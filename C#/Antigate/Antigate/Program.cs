﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using SimpleAntiGate;

namespace Antigate
{
    class Program
    {
        static void Main(string[] args)
        {

            AntiGate.AntiGateKey = "62b2cda009b3002143e3888840fbb1a9";

            if (!args.Any())
            {
                Console.Write("You should specify any parameter");
                return;
            }

            if (!args.Any())
            {
                Console.Write("You should specify any parameter");
                return;
            }

            //string args1 = "imagePathc:\\programming\\_SELENIUM\\avito\\captcha.png";

            if (args[0].Contains("imagePath"))
            //if (args1.Contains("imagePath"))
            {
                string captchaId;
                string answer = AntiGate.Recognize(args[0].Replace("imagePath", ""), out captchaId, isPhrase: true, isRussian: true);
                Encoding iso = Encoding.GetEncoding("cp1251");
                Encoding utf8 = Encoding.UTF8;
                byte[] utfBytes = utf8.GetBytes(answer);
                byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
                string msg = iso.GetString(isoBytes);
                Console.WriteLine(msg + " " + captchaId);
            }

            if (args[0].Contains("complaint"))
            {
                string captchaId = args[0].Replace("complaint", "");
                string answer = AntiGate.ReportBad(captchaId);
                //string answer = AntiGate.GetBalance();
                Console.WriteLine(answer);
            }

            
        }
    }
}

