﻿from UBaseObjects import *

class URegRuObjects(UBaseObjects):

    EnterLink = ["xpath", "//a[contains(., '" + u"Вход" + "')]"]
    Login = ["name", "login"]
    Password = ["name", "password"]
    Submit = GetButtonByText(u"Войти")

    #DomainsNamesLink = ["xpath", "//a[contains(., '" + u"Доменные имена" + "')]"]
    AddRecordA = ["xpath", "//div[@class = 'b-text b-text_margin_none addition-popup__item'][1]"]
    AddRecord = ["xpath", "//div[@class = 'domain-zone__add-record-item b-account-flex']"]
    SubdomainName = ["id", "subdomain"]
    IPAddress = ["id", "ipaddr"]
    ReadyButton = GetButtonByText(u"Готово")
    CloseButton = ["xpath", "//div[@class = 'toast-item__button toast-item__button--ds-cross']"]





