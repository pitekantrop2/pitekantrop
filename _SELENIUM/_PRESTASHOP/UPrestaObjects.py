﻿from UBaseObjects import *

class UPrestaObjects(UBaseObjects):

    Login = ["id", "email"]
    Password = ["id", "passwd"]
    Submit = ["class", "ladda-label"]
    UserAvatar = ["class", "employee_avatar_small"]

    continueButton = ["xpath", "//a[@class = 'btn btn-continue']"]

##    shopLink = ["linktext", u"Novall"]
    shopLink = ["linktext", u"Safetus.Ru"]
##    otpugLink = ["linktext", u"otpugiwatel"]

    addShopLink = ["class", "process-icon-new"]

    shopName = ["id", "name"]
    saveButton = ["class", "process-icon-save"]
    addUrlLink = ["xpath", "//a[contains(., '" + u"Щелкните здесь, чтобы установить URL для этого магазина" + "')]"]
##    addUrlLink = ["xpath", "//a[contains(., '" + u"Click here to set a URL for this shop." + "')]"]

    domenName = ["name", "domain"]
    sslDomainName = ["name", "domain_ssl"]

    ShopNumberButton = ["xpath", "//button[@class = 'btn btn-default dropdown-toggle' and contains(., '50')]"]
    Numslist = ["class", "dropdown-menu"]

    shopNameList = ["xpath", "//input[@name = 'shopFilter_a!name']"]
    findButton = ["id", "submitFilterButtonshop"]




