﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess
import win32api
import win32com.client

############################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()
logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "snippets")
logging = log.Log(logFileName)
objects = UYandexObjects.UYandexObjects
shell = win32com.client.Dispatch("WScript.Shell")

machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    yandex_ = yandex.Yandex(True)
    driver = yandex_.selenium
    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1])

    httpsUrls = ["http://otpugivateli-sobak.ru",
    "http://otpugivateli-ptic.ru",
    "http://otpugivateli-krotov.ru",
    "http://otpugivateli-grizunov.ru",
    "http://glushilki.ru.com",
    "http://mini-camera.ru.com",
    "http://insect-killers.ru"
    ]

    ULData = {}
    if site["url"] in ["http://omsk.knowall.ru.com", "http://tomsk.otpugivatel.com"]:
        ULData["form"] = "ИП"
        ULData["name"] = u"Усов Иван Викторович"
        ULData["ogrn"] = "313554317000016"
        ULData["phone"] = "+7(800)707-81-55"
        ULData["street"] = u"Бабушкина"
        ULData["home"] = "11"
        ULData["flat"] = "54"
    else :
        ULData["form"] = "ООО"
        ULData["name"] = u"Новалл"
        ULData["ogrn"] = "1177847257523"
        ULData["phone"] = "+7(800)707-81-35"
        ULData["street"] = u"Лиговский проспект"
        ULData["home"] = "87"
        ULData["flat"] = "621"


##    driver.MaximizeWindow()
    driver.OpenUrl("https://webmaster.yandex.ru/sites/")
    driver.WaitForText("Мои сайты")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        print shopId
##        if shopId < 126:
##            continue

        shopName = commonLibrary.GetShopName(shopName)

        if site["url"] in httpsUrls:
            domain = "https://" + shopDomen
            port = 443
        else:
            domain = "http://" + shopDomen
            port = 80

        driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/info/market/".format(domain.replace("//",""), port))
        time.sleep(1)
        if driver.IsTextsPresents(["вам не принадлежит"]):
            continue

        driver.SwitchToFrame("iFrameResizer0")

        driver.WaitForText("Товары и цены")
        if driver.IsVisible(objects.market_deactivateShopButton):
            continue

        if driver.IsVisible(objects.market_activateShopButton):
            driver.Click(objects.market_activateShopButton)
            time.sleep(2)
            if driver.IsVisible(objects.market_dataNextButton) == True:
                driver.Click(objects.market_dataNextButton)
                logging.LogInfoMessage(str(shopId))
                continue
            else:
                if driver.IsTextPresent(ULData["ogrn"]):
                    continue
        if driver.IsVisible(objects.market_agreeButton):
            driver.Click(objects.market_agreeButton)

        #Магазин
        driver.WaitFor(objects.market_changeLink)
        driver.Click(objects.market_changeLink)
        driver.WaitFor(objects.market_shopRegionName)
        driver.SendKeys(objects.market_shopRegionName, shopName)
        time.sleep(1)
        if driver.IsVisible(objects.market_shopRegionVariant) == False:
            continue
        driver.Click(objects.market_shopRegionVariant)
        driver.SendKeys(objects.market_shopShippingRegion, shopName)
        driver.SendKeys(objects.market_shopShippingInfo, u"Доставка курьером, на пункт выдачи или почтой России.")
        driver.SendKeys(objects.market_shopPhone, ULData["phone"])
        driver.SetSelectOptionByIndex(objects.market_shopOnlineOrderSelect, 2)
        id = prestashop_api.GetIdCmsByLinkRewrite(shopName)
        pageUrl = u"{0}/content/{1}-about-us-{2}".format(domain,id, urllib.quote(shopName.encode("cp1251")).decode("cp1251"))
        driver.SendKeys(objects.market_shopPageUrl, pageUrl)
        driver.Click(objects.market_shopNextButton)
        driver.WaitFor(objects.market_ULForm)

        #Юридическое лицо
        driver.SetSelectOptionByText(objects.market_ULForm, ULData["form"])
        driver.SendKeys(objects.market_ULName, ULData["name"])
        driver.SendKeys(objects.market_ULOGRN, ULData["ogrn"])
        for i in [0,1]:
            driver.SendKeys(driver.FindElements( objects.market_ULPostAddressCountry)[i], u"Россия")
            driver.SendKeys(driver.FindElements( objects.market_ULPostAddressCity)[i], u"Санкт-Петербург")
            driver.SendKeys(driver.FindElements( objects.market_ULPostAddressStreet)[i], ULData["street"] )
            driver.SendKeys(driver.FindElements( objects.market_ULPostAddressHome)[i], ULData["home"])
            driver.SendKeys(driver.FindElements( objects.market_ULPostAddressOffice)[i], ULData["flat"])
        driver.SendKeys(objects.market_ULPageUrl, pageUrl)
        driver.Click(objects.market_shopNextButton)
        driver.WaitFor(objects.market_dataUrl)
        time.sleep(3)

        #Источники данных
        if driver.IsTextPresent("Используется") == False:
            try:
                ymlFileName = "{0}.yml".format(shopDomen.split(".")[0])
                driver.SendKeys(objects.market_dataUrl, domain + "/" + ymlFileName)
                driver.Click(objects.market_dataAddButton)
                driver.WaitForText("Ожидает проверки")
                time.sleep(1)
                driver.Click(objects.market_dataRefresh)
                driver.WaitFor(objects.market_dataActivateFile)
                driver.Click(objects.market_dataActivateFile)
                driver.WaitForText("Используется")
            except:
                logging.LogErrorMessage(str(shopId))
                continue
        driver.Click(objects.market_dataSaveButton)
        time.sleep(2)
        if driver.IsVisible(objects.market_dataNextButton) == False:
            logging.LogErrorMessage(str(shopId))
            continue
        driver.WaitFor(objects.market_dataNextButton)
        driver.Click(objects.market_dataNextButton)
        try:
            driver.WaitFor(objects.market_activateShopButton)
            driver.Click(objects.market_activateShopButton)
            driver.WaitFor(objects.market_dataNextButton)
            driver.Click(objects.market_dataNextButton)
            logging.LogInfoMessage(str(shopId))
        except:
            logging.LogErrorMessage(str(shopId))

    if machineName.find("hp") != -1:
        ssh.kill()

    yandex_.selenium.CleanUp()
