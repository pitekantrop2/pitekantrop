﻿# -*- coding: utf-8 -*-
import myLibrary
import UPrestaObjects
import datetime
import log
import PrestashopAPI
import yandex
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess
import win32api
import win32com.client
import time

############################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()
objects = UYandexObjects.UYandexObjects
shell = win32com.client.Dispatch("WScript.Shell")
todayDate = datetime.date.today()

message = ""
machineName = commonLibrary.GetMachineName()

reindexData = prestashop_api.GetReindexData()

quotas = {}

for reindex in reindexData:
    id, site, shopname, categories, products, otherPages, lastShopId, lastLaunchDate = reindex
    if lastLaunchDate not in [None, ""]:
        pastDays = (todayDate - lastLaunchDate).days
        if pastDays == 0:
            continue

    if site not in quotas.keys():
        quotas[site] = commonLibrary.GetReindexQuota(site)

    if quotas[site] == 0:
        continue

    try:
        categories = [x.strip() for x in categories.split(",")]
    except:
        categories = []
    try:
        products = [x.strip() for x in products.split(",")]
    except:
        products = []
    try:
        otherPages = [x.strip() for x in otherPages.split("|")]
    except:
        otherPages = []

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site)
    bSubfolders = prestashop_api.IsSubfolderSite()

    message += "================= " + prestashop_api.siteUrl + " ===============\n\n"

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    site_ = commonLibrary.GetSiteParams(site)
    yandex_ = yandex.Yandex(True, site_["ip"])
    driver = yandex_.selenium
    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1], False)

    driver.OpenUrl("https://webmaster.yandex.ru/sites/")
    driver.WaitForTexts(["Мои сайты", "Информация о сайте"])

    shops = prestashop_api.GetAllShops()
    bNeedContinue = False
    lastShopId_ = -1

    scheme = prestashop_api.GetSiteUrlScheme()
    if scheme == "https://":
        port = 443
    else:
        port = 80

    domain = site.replace("http://", scheme)
    driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/indexing/reindex/".format(domain.replace("//",""), port))
    driver.WaitFor(objects.reindex_quota)
    qText = driver.GetText(objects.reindex_quota)
    if qText.find(u"Сегодня можно отправить") != -1:
        quotas[site] = int(qText.split(u"отправить еще ")[-1].split(" ")[0])

    shopId = 1

    for shop in shops:
        if driver.IsTextPresent("Сегодня вся квота израсходована."):
            quotas[site] = 0
            bNeedContinue = True
            if shopId != 1:
                lastShopId_ = shopId
            break

        shopId, shopDomen, shopName = shop
        shopName = commonLibrary.GetShopName(shopName)

        if shopname not in [None, ""]:
            if shopname != shopName:
                continue
        else:
            if len(otherPages) > 0 and shopId != 1:
                continue
            if shopId <= lastShopId or shopName not in (commonLibrary.mainCities1 + commonLibrary.mainCities2):
                continue

        print shopId

        urls = []
        mainDomain = prestashop_api.GetShopMainDomain(shopId)
        try:
            for categoryId in categories:
                if not bSubfolders:
                    url = "/{0}".format(prestashop_api.GetCategoryUrl(categoryId, shopId))
                else:
                    url = "{0}/{1}".format(mainDomain, prestashop_api.GetCategoryUrl(categoryId, shopId))
                urls.append(url)

            for productId in products:
                if not bSubfolders:
                    url = "/{0}".format(prestashop_api.GetProductUrl(productId, shopId))
                else:
                    url = "{0}/{1}".format(mainDomain, prestashop_api.GetProductUrl(productId, shopId))
                urls.append(url)

            for otherPage in otherPages:
                if otherPage.find("/") == -1:
                    artId = prestashop_api.GetArticleId(otherPage, shopId)
                    views = prestashop_api.GetArticleViews(artId)
                    otherPage = prestashop_api.GetArticleUrlById(artId)
                    if otherPage == -1:
                        continue
                if not bSubfolders:
                    url = "/{0}".format( otherPage)
                else:
                    url = "{0}/{1}".format(mainDomain, otherPage)
                urls.append(url)
        except:
            continue

        if len(urls) == 0:
            continue

        if bSubfolders == False:
            domain = scheme + shopDomen
            driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/indexing/reindex/".format(domain.replace("//",""), port))
            time.sleep(1)
            if driver.IsTextsPresents(["вам не принадлежит", "Подтверждение прав"]):
                continue

        driver.WaitFor(objects.reindex_urls)
        if driver.IsVisible(objects.reindex_closeButton):
            driver.Click(objects.reindex_closeButton)
            time.sleep(1)

        quotas[site] -= len(urls)

        try:
            driver.Click(objects.reindex_urls)
            for url in urls:
                for c in url:
                    shell.SendKeys(c)
                time.sleep(1)
                shell.SendKeys("{ENTER}")
                time.sleep(1)

            driver.Click(objects.reindex_sendButton)
            driver.WaitForText("В очереди")
        except:
            message += "[Error] Ошибка при отправке урлов на переиндексацию\n"
            bNeedContinue = True
            break

        if quotas[site] < len(urls):
            bNeedContinue = True
            lastShopId_ = shopId
            break

    if bNeedContinue:
        prestashop_api.SetReindexLastLaunchDate(id, todayDate)
        if lastShopId_ not in [-1, 1]:
            prestashop_api.SetReindexLastShopId(id, lastShopId_)
    else:
        prestashop_api.DeleteReindex(id)
        message += "Урлы отправлены на переиндексацию, запись удалена\n\n"

    if machineName.find("hp") != -1:
        ssh.kill()

    yandex_.selenium.CleanUp()

if message != "":
    result = mails.SendInfoMail("Переиндексация", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))