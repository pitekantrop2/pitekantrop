﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import UGoogleObjects
import ftplib
import google
import commonLibrary
import subprocess

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "deleteUrlsGoogle")

logging = log.Log(logFileName)
objects = UGoogleObjects.UGoogleObjects
google_ = google.Google(True)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

    loginData = commonLibrary.GetGoogleLoginData(site["url"])

    google_.Login(loginData[0], loginData[1])

    google_.selenium.OpenUrl("https://www.google.com/webmasters/tools/home")
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        id_shop, domain, name = shop

        domain = commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl) + domain
        if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
            startShopId = 233
        if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
            startShopId = 272
        if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
            startShopId = 8

        if id_shop <= startShopId:
            continue

        print domain, id_shop

        url = "https://www.google.com/webmasters/tools/url-removal?hl=ru&authuser=5&siteUrl={0}".format(domain)
        google_.selenium.OpenUrl(url)
        time.sleep(2)
        if google_.selenium.IsTextPresent ("Права на ресурс не подтверждены"):
            continue
        google_.selenium.WaitFor(objects.deleteUrls_hideButton)

        categories = prestashop_api.GetShopActiveNotGoogleCategories(id_shop)
        for category in categories:
            catId, name, link_rewrite = category
            catUrl = prestashop_api.GetCategoryUrl(catId,id_shop )
            try:
                if google_.selenium.IsTextPresent(catUrl):
                    continue
            except:
                continue
            google_.selenium.Click(objects.deleteUrls_hideButton)
            google_.selenium.WaitFor(objects.deleteUrls_continueButton)
            google_.selenium.SendKeys(objects.deleteUrls_urlEdit, catUrl)
            google_.selenium.Click(objects.deleteUrls_continueButton)
            google_.selenium.WaitFor(objects.deleteUrls_sendRequestButton)
            google_.selenium.Click(objects.deleteUrls_sendRequestButton)
            google_.selenium.WaitForTexts(["был внесен в список удаляемых.", "Запрос на удаление этого URL уже отправлен"])

    google_.selenium.CleanUp()
    if machineName.find("hp") != -1:
        ssh.kill()