﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import UGoogleObjects
import ftplib
import google
import commonLibrary
import subprocess

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "reindexGoogle")

logging = log.Log(logFileName)
objects = UGoogleObjects.UGoogleObjects
google_ = google.Google(True)
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

    if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
        categories = [140]
        products = []
        lastShopId = 7
        sitiesList = commonLibrary.mainCities2 + commonLibrary.mainCities1

    loginData = commonLibrary.GetGoogleLoginData(site["url"])

    google_.Login(loginData[0], loginData[1])

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        if shopId <= lastShopId:
            continue

        shopName = commonLibrary.GetShopName(shopName)

        if shopName not in sitiesList:
            continue

        domainScheme = commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl)
        siteUrl = domainScheme + shopDomen

        print siteUrl, shopId
        urls = []
        try:
            for categoryId in categories:
                urls.append(prestashop_api.GetCategoryUrl(categoryId, shopId))

            for productId in products:
                urls.append(prestashop_api.GetProductUrl(productId, shopId))
        except:
            continue
##
##        if shop[0] < 53: continue

        google_.selenium.OpenUrl("https://www.google.com/webmasters/tools/googlebot-fetch?hl=ru&authuser=7&siteUrl={0}".format(siteUrl))
        if google_.selenium.IsTextPresent("Вам недоступна информация о сайте"):
            continue

        for url in urls:
            if google_.selenium.IsTextPresent (url):
                continue
            google_.selenium.WaitFor(objects.reindex_scanButton)
            google_.selenium.SendKeys(objects.reindex_urlEdit, url)
            google_.selenium.Click(objects.reindex_scanButton)
            google_.selenium.WaitFor(objects.reindex_requestButton)
            google_.selenium.Click(objects.reindex_requestButton)
            google_.selenium.SwitchToFrame(google_.selenium.MyFindElement(objects.reindex_recaptchaIframe))
            google_.selenium.WaitFor(objects.captchaCh)
            google_.selenium.Click(objects.captchaCh)
            google_.selenium.WaitFor(objects.reindex_recaptchaDiv)
            google_.selenium.WaitForNotVisible(objects.reindex_recaptchaDiv)
            google_.selenium.SwitchToBaseWindow()
            google_.selenium.Click(objects.reindex_allLinksCh)
            google_.selenium.Click(objects.reindex_sendButton)
            google_.selenium.WaitForTexts(["Отправлен запрос на индексирование URL", "Произошла ошибка. Повторите попытку позже."])

    google_.selenium.CleanUp()
    if machineName.find("hp") != -1:
        ssh.kill()