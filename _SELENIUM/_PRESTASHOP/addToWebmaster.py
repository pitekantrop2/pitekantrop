﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import commonLibrary
import SendMails
import sys
import subprocess
import UYandexObjects


logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "webmaster")
logging = log.Log(logFileName)

objects = UYandexObjects.UYandexObjects
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen([commonLibrary.puttyPath, '-v', '-ssh', '-2', '-P', '22', '-C', '-l', commonLibrary.sshLogin, '-pw', commonLibrary.sshPassword, '-L', '5555:127.0.0.1:3306', site["ip"]])

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    yandex_ = yandex.Yandex(True, site["ip"])

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1])
    yandex_.selenium.OpenUrl("https://webmaster.yandex.ru/sites/add/")
    yandex_.selenium.WaitFor(objects.addSite_addSiteButton)

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
##        if shopId < 64:
##            continue

        logging.Log( shopId)
        shopName = commonLibrary.GetShopName(shopName)

        scheme = commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl)

        result = yandex_.AddSiteToWebmaster(scheme + shopDomen, site["ip"],"/home/" + site["siteDir"] + "/www")
        if result != -1:
            logging.LogInfoMessage("{0} ({1})".format(shopDomen, shopId))
        else:
            logging.LogErrorMessage("{0} ({1})".format(shopDomen, shopId))

    if machineName.find("hp") != -1:
        ssh.kill()

    yandex_.Destructor()