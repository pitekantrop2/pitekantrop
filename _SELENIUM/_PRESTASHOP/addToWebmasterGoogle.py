﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import UGoogleObjects
import ftplib
import google
import commonLibrary
import subprocess

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "addToWebmasterGoogle")

logging = log.Log(logFileName)
objects = UGoogleObjects.UGoogleObjects
google_ = google.Google(True)
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

    httpsUrls = ["http://otpugivateli-sobak.ru",
    "http://otpugivateli-ptic.ru",
    "http://otpugivateli-krotov.ru",
    "http://otpugivateli-grizunov.ru",
    "http://glushilki.ru.com",
    "http://mini-camera.ru.com",
    "http://insect-killers.ru"
    ]

    if site["url"] in ["http://sushilki.ru.com"]:
        google_.Login("knowall.ru.com@gmail.com", "19842001")
    elif site["url"] in ["http://otpugivateli-grizunov.ru"]:
        google_.Login("otpugivateligrizunov@gmail.com", "19842001")
    elif site["url"] in ["http://otpugivateli-ptic.ru"]:
        google_.Login("otpugivateliptic@gmail.com", "19842001")
    elif site["url"] in ["http://insect-killers.ru"]:
        google_.Login("insectkillersru@gmail.com", "19842001")
    elif site["url"] in ["http://otpugivateli-sobak.ru", "http://mini-camera.ru.com"]:
        google_.Login("otpugivatelisobak@gmail.com", "19842001")
    elif site["url"] in ["http://glushilki.ru.com", "http://otpugivateli-krotov.ru"]:
        google_.Login("glushilkirucom@gmail.com", "19842001")
    else:
        google_.Login("pitekantrop55@gmail.com", "Tabulfsam198")
##        google_.Login("feedback@knowall.ru.com", "Tabulfsam")

    google_.selenium.OpenUrl("https://www.google.com/webmasters/tools/home")
    google_.selenium.WaitFor(objects.addResource_addUrlButton)
    time.sleep(15)

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        domain = shop[1]
        if site["url"] in httpsUrls:
            domain = "https://" + domain
        print domain, shop[0]

##        if shop[0] < 213: continue

        if google_.selenium.IsTextPresent(domain):
            continue


        google_.selenium.Click(objects.addResource_addUrlButton)
        google_.selenium.WaitFor(objects.addResource_urlEdit)
        google_.selenium.SendKeys(objects.addResource_urlEdit, domain)
        google_.selenium.WaitFor(objects.addResource_nextButton)
        google_.selenium.Click(objects.addResource_nextButton)
        google_.selenium.WaitForText("Рекомендуемый способ")
        google_.selenium.SwitchToFrame(google_.selenium.MyFindElement(objects.recaptchaIframe))
        google_.selenium.Click(objects.captchaCh)
        google_.selenium.SwitchToBaseWindow()
        google_.selenium.WaitFor(objects.confirmButton)
        time.sleep(1)
        google_.selenium.Click(objects.confirmButton)
    ##    google_.selenium.Click(objects.addResource_confirmButton)
    ##    google_.selenium.WaitForText("Вы успешно подтвердили право собственности на сайт")
        google_.selenium.WaitForTexts(["Ресурс добавлен", "Вы успешно подтвердили право собственности на сайт", "не подтверждены", "Не удалось подтвердить"])
        logging.Log(domain)
        google_.selenium.OpenUrl("https://www.google.com/webmasters/tools/home")
        google_.selenium.WaitFor(objects.addResource_addUrlButton)

    google_.selenium.CleanUp()
    if machineName.find("hp") != -1:
        ssh.kill()