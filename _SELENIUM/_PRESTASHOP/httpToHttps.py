﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import commonLibrary
import SendMails
import sys
import subprocess
import UYandexObjects


logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "httpToHttps")
logging = log.Log(logFileName)

objects = UYandexObjects.UYandexObjects
machineName = commonLibrary.GetMachineName()

yandex_ = yandex.Yandex(True)
driver = yandex_.selenium

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1])
##    yandex_.selenium.OpenUrl("https://webmaster.yandex.ru/sites/add/")
##    yandex_.selenium.WaitFor(objects.addSite_addSiteButton)

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
##        if shopId < 337:
##            continue
##        shopName =

        driver.OpenUrl("https://webmaster.yandex.ru/site/http:{0}:80/indexing/mirrors/".format(shopDomen))
        time.sleep(1)
        if driver.IsTextsPresents(["это может занять некоторое время", "вместо домена", "вам не принадлежит.", "Отклеить неглавное зеркало", "Подтверждение прав"]) == True:
            continue

        driver.Click(objects.mirrors_HttpsCh)
        time.sleep(1)
        driver.Click(objects.mirrors_SaveButton)
        time.sleep(1)
        logging.Log(str(shopId))

    if machineName.find("hp") != -1:
        ssh.kill()

yandex_.selenium.CleanUp()

mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (webmaster)", "", [logFileName])