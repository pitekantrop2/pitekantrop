﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import UGoogleObjects
import ftplib
import google
import commonLibrary
import subprocess

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "reindexSitemapGoogle")

logging = log.Log(logFileName)
objects = UGoogleObjects.UGoogleObjects
google_ = google.Google(True)
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

    loginData = commonLibrary.GetGoogleLoginData(site["url"])

    google_.Login(loginData[0], loginData[1])

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        domain = shop[1]
        domainScheme = commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl)
        siteUrl = domainScheme + domain
        city = domain.split(".")[0]
        sitemapName = "google-sitemap-{0}.xml".format(city)

        print domain, shop[0]
##
##        if shop[0] < 53: continue

        google_.selenium.OpenUrl("https://www.google.com/webmasters/tools/sitemap-list?hl=ru&authuser=5&siteUrl={0}/#MAIN_TAB=1&CARD_TAB=-1".format(siteUrl))
        if google_.selenium.IsTextPresent("Вам недоступна информация о сайте"):
            continue

        google_.selenium.WaitFor(objects.sitemaps_addButton)
        sitemapLink = ["xpath", "//a[. = '{0}']".format("/" + sitemapName)]
        google_.selenium.Click(sitemapLink)
        google_.selenium.WaitFor(objects.sitemaps_resendButton)
        google_.selenium.Click(objects.sitemaps_resendButton)
        google_.selenium.WaitForTexts(["Данные отправлены."])
        logging.Log(domain)

    google_.selenium.CleanUp()
    if machineName.find("hp") != -1:
        ssh.kill()