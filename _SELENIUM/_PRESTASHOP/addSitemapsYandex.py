﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess

############################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "sitemaps")
logging = log.Log(logFileName)
objects = UYandexObjects.UYandexObjects

machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    yandex_ = yandex.Yandex(True, site["ip"])
    driver = yandex_.selenium
    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1])

    siteUrlScheme = prestashop_api.GetSiteUrlScheme()

##    driver.MaximizeWindow()
    driver.OpenUrl("https://webmaster.yandex.ru/sites/")
    driver.WaitForText("Мои сайты")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
##        if shopId < 300:
##            continue

        logging.Log( shopId)

        shopName = commonLibrary.GetShopName(shopName)
        domain = siteUrlScheme + shopDomen

        if siteUrlScheme == "https://":
            port = 443
        else:
            port = 80

        driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/indexing/sitemap/".format(domain.replace("//",""), port))
        time.sleep(3)
        if driver.IsTextsPresents(["вам не принадлежит", "что не нашли сайт в DNS.","Подтвердите права на"]):
            logging.LogErrorMessage("{0}, {1}".format( str(shopId),shopDomen ))
            continue

        driver.WaitFor(objects.sitemaps_sitemapEdit)
        if driver.IsVisible(objects.sitemaps_deleteButton):
            continue

        city = shopDomen.split(".")[0]
        sitemapUrl = "{0}/sitemap-{1}.xml".format(domain, city)
        driver.SendKeys(objects.sitemaps_sitemapEdit, sitemapUrl )
        driver.Click(objects.sitemaps_Add)
        driver.WaitForText("Очередь на обработку")
        logging.LogInfoMessage("{0}, {1}".format( str(shopId),shopDomen ))

    if machineName.find("hp") != -1:
        ssh.kill()

    yandex_.selenium.CleanUp()
