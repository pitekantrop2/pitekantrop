﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess
import win32api
import win32com.client


def GetPvzAddress(shopId):
    sql = u"""SELECT name
			FROM `ps_carrier`
			LEFT JOIN `ps_carrier_shop` ON `ps_carrier`.id_carrier = `ps_carrier_shop`.id_carrier
			WHERE `ps_carrier`.active =1
			AND `ps_carrier`.deleted =0
			AND `ps_carrier_shop`.id_shop = {0}
			AND name LIKE  'На пункт выдачи%'""".format(shopId)

    try:
        carrier = prestashop_api.MakeGetInfoQueue(sql)[0][0]
    except:
        return -1

    address = carrier.split("(")[-1].strip(").")
    return address


def GetShopRegion(shopDomain):

    if shopDomain == "mineralnye-vody.otpugivatel.com":
        return u"Минеральные Воды"

    for shop in shops:
        if shop[1] == shopDomain:
            return shop[2]

############################################################################

shell = win32com.client.Dispatch("WScript.Shell")
logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "regions")
logging = log.Log(logFileName)
objects = UYandexObjects.UYandexObjects
yandex_ = yandex.Yandex(True)
driver = yandex_.selenium
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    if prestashop_api.siteUrl in ["http://omsk.knowall.ru.com"]:
        imPhone = "8(800)707-81-55"

    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1])

    httpsUrls = ["http://otpugivateli-sobak.ru",
    "http://otpugivateli-ptic.ru",
    "http://otpugivateli-krotov.ru",
    "http://otpugivateli-grizunov.ru",
    "http://glushilki.ru.com",
    "http://mini-camera.ru.com",
    "http://insect-killers.ru"
    ]

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        shopName = commonLibrary.GetShopName(shopName)
        address = GetPvzAddress(shopId)
        if address == -1:
            continue
        address = shopName + " " + address
        if prestashop_api.siteUrl in httpsUrls:
            siteUrl = "https://" + shopDomen
        else:
            siteUrl = "http://" + shopDomen

        driver.OpenUrl("https://yandex.ru/sprav/main")
        time.sleep(1)
        if driver.IsVisible(objects.directory_AddLink):
            driver.Click(objects.directory_AddLink)
            driver.WaitForText("Новая организация")
        else:
            driver.WaitForText("Дайте знать о своей организации")

        driver.SendKeys(objects.directory_name, imName)
        time.sleep(1)
        driver.SendKeys(objects.directory_addressString, address)
        time.sleep(1)
        driver.SendKeys(objects.directory_phones, imPhone)
        driver.Click(objects.directory_Add)
        driver.SendKeys(objects.directory_urls, siteUrl)
        driver.Click(objects.directory_Add)
        driver.Click(objects.directory_workingDays)
        driver.SendKeys(objects.directory_activity, u"Интернет-магазин")
        driver.WaitFor(objects.directory_menuItem)
        driver.Click(objects.directory_menuItem)
        time.sleep(1)
        while True:
            try:
                driver.Click(objects.directory_AddOrganization)
                break
            except:
                shell.SendKeys("{DOWN}")
                time.sleep(0.5)
        driver.WaitForText("Добавить компанию с моими данными")
        while True:
            try:
                driver.Click(objects.directory_ConfirmAdding)
                break
            except:
                shell.SendKeys("{DOWN}")
                time.sleep(0.5)
        driver.WaitFor(objects.directory_Follow)
        driver.Click(objects.directory_Follow)
        driver.WaitForText("Мои заявки")

        logging.LogInfoMessage("Region for {0} ({1}) has been added".format(shopDomen, shopId))

    if machineName.find("hp") != -1:
        ssh.kill()

yandex_.selenium.CleanUp()