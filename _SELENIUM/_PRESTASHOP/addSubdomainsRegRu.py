﻿import myLibrary
import URegRuObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP", "subdomains")

baseUrl = "https://www.reg.ru"
logging = log.Log(logFileName)

def Login(login, password):
    Driver.OpenUrl(baseUrl)
    Driver.Click(URegRuObjects.URegRuObjects.EnterLink)
    Driver.WaitFor(URegRuObjects.URegRuObjects.Login)
    Driver.SendKeys(URegRuObjects.URegRuObjects.Login, login)
    Driver.SendKeys(URegRuObjects.URegRuObjects.Password, password)
    Driver.Click(URegRuObjects.URegRuObjects.Submit)
    Driver.WaitForText("pitekantrop")


def AddSubdomain(name, ipAddress):

    Driver.OpenUrl(baseUrl + "/service/zone_manager?service_ids=" + service_ids)

    if Driver.IsTextPresent(name) == True:
        return

    Driver.WaitFor(URegRuObjects.URegRuObjects.AddDomainButton)
    Driver.Click(URegRuObjects.URegRuObjects.AddDomainButton)
    Driver.WaitFor(URegRuObjects.URegRuObjects.SubdomainName)
    Driver.SendKeys(URegRuObjects.URegRuObjects.SubdomainName, name)
    Driver.SendKeys(URegRuObjects.URegRuObjects.IPAddress, ipAddress)
    Driver.Click(URegRuObjects.URegRuObjects.AddEntryButton)
    Driver.WaitForText("Операция успешно")
    time.sleep(1)



Driver = myLibrary.Selenium()
Driver.SetUp()
Login("pitekantrop", "1qaz@WSX3edc")

ipAddress = "185.5.251.103"
domain = "otpugivatel.com"
service_ids = "4850663"
prestashop_api = PrestashopAPI.PrestashopAPI("http://tomsk.otpugivatel.com", 'QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6', "127.0.0.1", "otpugivatel", "otpugiv_user_ex", "1qaz@WSX", 5555)

##ipAddress = "185.58.207.82"
##service_ids =
##prestashop_api = PrestashopAPI.PrestashopAPI("http://omsk.knowall.ru.com", 'KAR7M17CPPALJY38NKI521NNP96IH3Z7', "127.0.0.1", "knowall", "knowall_user_ex", "1qaz@WSX", 5555)

shops = prestashop_api.GetAllShops()

for shop in shops:

    shopId, shopDomen, shopName = shop

    if shopId <= 200:continue

    try:
        AddSubdomain(shopDomen.replace("." + domain, ""), ipAddress)
        logging.LogInfoMessage(shopDomen+ " has been added")
    except Exception, e:
        logging.LogErrorMessage(shopDomen + " hasn't been added cause")


Driver.CleanUp()