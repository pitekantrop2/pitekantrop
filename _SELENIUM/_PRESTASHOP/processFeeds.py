﻿# -*- coding: utf-8 -*-
import myLibrary
import UPrestaObjects
import datetime
import log
import PrestashopAPI
import yandex
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess
import time
import win32api
import win32com.client

############################################################################

applications = {
"http://glushilki.ru.com":"160910",
"http://mini-camera.ru.com":"160616",
"http://otpugivateli-sobak.ru":"161030",
"http://insect-killers.ru":"158783",
"http://otpugivateli-grizunov.ru":"161031",
"http://tomsk.otpugivatel.com":"160886",
"http://otpugivateli-ptic.ru":"160685"
}

prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()
objects = UYandexObjects.UYandexObjects
todayDate = datetime.date.today()
shell = win32com.client.Dispatch("WScript.Shell")

message = ""
machineName = commonLibrary.GetMachineName()

feedsData = prestashop_api.GetFeedsData()

numOfAddingFeeds = 4

for feed in feedsData:
    id, site, city, date = feed
    if todayDate == date:
        continue

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site)
    bSubfolders = prestashop_api.IsSubfolderSite()

    message += "================= " + prestashop_api.siteUrl + " ===============\n"

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    site_ = commonLibrary.GetSiteParams(site)
    yandex_ = yandex.Yandex(True, site_["ip"])
    driver = yandex_.selenium
    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1], False)

    appNum = applications[prestashop_api.siteUrl]
    driver.OpenUrl("https://turboapps.yandex.ru/console/application/" + appNum)
    driver.WaitForTexts(["Добавление фидов"])

    scheme = prestashop_api.GetSiteUrlScheme()
    domain = site.replace("http://", scheme)

    #удаляем фиды, не прошедшие модерацию
    feedsStates = driver.MyFindElements(objects.feeds_feedState)
    statesTexts = [driver.GetElementText(i) for i in feedsStates]
    indexes = [i for i in range(0, len(statesTexts)) if statesTexts[i] == u"Отказано"]
    counter = 0
    for ind in indexes:
        removeButtons = driver.MyFindElements(objects.feeds_feedRemoveButton)
        while True:
            try:
                driver.MouseMoveAt(removeButtons[ind - counter], 5,5)
                driver.Click(removeButtons[ind - counter])
                break
            except:
                pass

        counter += 1

    if counter != 0:
        city = u"Москва"

    #добавляем новые фиды
    mainCities = commonLibrary.mainCities1 + commonLibrary.mainCities2
    if city == None:
        city = mainCities[0]

    for i in range(numOfAddingFeeds):
        ind = mainCities.index(city) + 1
        if ind == len(mainCities):
            continue
        feedsUrls = driver.MyFindElements(objects.feeds_feedUrl)
        addedUrls = [driver.GetElementAttribute(i, "value") for i in feedsUrls]
        while True:
            nextCity = mainCities[ind]
            shopId = prestashop_api.GetShopIdByName(nextCity)
            if bSubfolders:
                latinCityName = prestashop_api.GetShopVirtualUri(shopId).replace("/", "")
            else:
                domain = prestashop_api.GetShopDomain(shopId)
                latinCityName = domain.split(".")[0]
            newFeedUrl = "{}/{}.yml".format(prestashop_api.GetShopMainDomain(shopId),latinCityName)
            if newFeedUrl not in addedUrls:
                break
            ind += 1
        city = nextCity
        while True:
            try:
                driver.Click(objects.feeds_addFeedButton)
                break
            except:
                shell.SendKeys("{DOWN}")
                shell.SendKeys("{DOWN}")
                time.sleep(0.5)

        if city == u"Королев":
            city == u"Королёв"

        time.sleep(1)
        region = u"{}, Россия".format(nextCity)
        driver.SendKeys(driver.MyFindElements(objects.feeds_feedRegion)[-1], region)
        time.sleep(1.5)
        try:
            driver.WaitFor(objects.feeds_ListItem)
        except:
            message += "[Error] Ошибка добавления фида для города {}\n".format(nextCity.encode("utf-8"))
            prestashop_api.SetFeedCity(id, nextCity)
            continue
        while True:
            try:
                driver.Click(objects.feeds_ListItem)
                break
            except:
                shell.SendKeys("{DOWN}")
                shell.SendKeys("{DOWN}")
                time.sleep(0.5)
        driver.SendKeys(driver.MyFindElements(objects.feeds_feedUrl)[-1], newFeedUrl)
        driver.Click(driver.MyFindElements(objects.feeds_checkFeedButton)[-1])
        driver.WaitForText("Проверка")
        time.sleep(1)
        prestashop_api.SetFeedCity(id, nextCity)

        message += "Добавлен фид для города {}\n".format(nextCity.encode("utf-8"))

    prestashop_api.SetFeedDate(id, todayDate)

    if machineName.find("hp") != -1:
        ssh.kill()

    yandex_.selenium.CleanUp()

if message != "":
    result = mails.SendInfoMail("Фиды", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))