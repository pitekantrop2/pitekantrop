﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess
import win32api
import win32com.client

############################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()
logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "reindexSitemapsY")
logging = log.Log(logFileName)
objects = UYandexObjects.UYandexObjects

machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    yandex_ = yandex.Yandex(True)
    driver = yandex_.selenium
    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1])

##    driver.MaximizeWindow()
    driver.OpenUrl("https://webmaster.yandex.ru/sites/")
    driver.WaitForText("Мои сайты")


    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        logging.Log(str(shopId))

        print shopId
##        if shopId < 287:
##            continue

        scheme = commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl)
        if scheme == "https://":
            port = 443
        else:
            port = 80

        domain = scheme + shopDomen

        driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/indexing/sitemap/".format(domain.replace("//",""), port))
        time.sleep(1)
        if driver.IsTextsPresents(["вам не принадлежит"]):
            continue

        driver.WaitFor(objects.sitemaps_reindex)
        driver.Click(objects.sitemaps_reindex)
        time.sleep(1)
##        driver.WaitForText("был отправлен")


    if machineName.find("hp") != -1:
        ssh.kill()

    yandex_.selenium.CleanUp()
