﻿import myLibrary
import UBegetObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP", "subdomains")

baseUrl = "https://cp.beget.ru"
logging = log.Log(logFileName)

def Login(login, password):
    Driver.OpenUrl(baseUrl)
    Driver.SendKeys(UBegetObjects.UBegetObjects.Login, login)
    Driver.SendKeys(UBegetObjects.UBegetObjects.Password, password)
    Driver.Click(UBegetObjects.UBegetObjects.Submit)
    Driver.WaitForText("Иванов")


def AddSubdomain(name, mainDomain, directory):

    Driver.OpenUrl(baseUrl + "/subdomains")
    Driver.SendKeys(UBegetObjects.UBegetObjects.SubdomenName, name)
    Driver.Click(UBegetObjects.UBegetObjects.SelectDomainButton)
    Driver.SetListIndexByText(UBegetObjects.UBegetObjects.ResultsList, mainDomain)
    Driver.Click(UBegetObjects.UBegetObjects.ChooseDirRadioButton)
    Driver.Click(UBegetObjects.UBegetObjects.SelectDirButton)
    Driver.SetListIndexByText(UBegetObjects.UBegetObjects.ResultsList, directory)
    Driver.Click(UBegetObjects.UBegetObjects.AddSubdomainButton)
    time.sleep(1)

mainDomain = "amazing-things.ru"
directory = "amazing-things.ru/public_html"

Driver = myLibrary.Selenium()
Driver.SetUp()
Login("lfb7967i", "2G6O3cRC")

with open("cities.txt", "r") as myfile:
    for line in myfile.readlines() :
        try:
            subdomainName = line.split(";")[1].lower().replace("\n","")
            AddSubdomain(subdomainName, mainDomain, directory)
            logging.LogInfoMessage(subdomainName+ " has been added")
        except Exception, e:
            logging.LogErrorMessage(subdomainName + " hasn't been added cause")


Driver.CleanUp()