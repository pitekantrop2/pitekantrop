﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess


logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "regions")
logging = log.Log(logFileName)
objects = UYandexObjects.UYandexObjects


machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    loginData = commonLibrary.GetYandexLoginData(prestashop_api.siteUrl)

    time.sleep(1)

    yandex_ = yandex.Yandex(True, site["ip"])
    yandex_.Login(loginData[0], loginData[1])
    driver = yandex_.selenium

##    driver.MaximizeWindow()
    driver.OpenUrl("https://webmaster.yandex.ru/sites/")
    driver.WaitForText("Мои сайты")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
##        if shopId < 440:
##            continue
        logging.Log(shopId)
        scheme = prestashop_api.GetSiteUrlScheme()
        shopDomen = scheme + shopDomen

        shopName = commonLibrary.GetShopName(shopName)

        if shopDomen.find("https") != -1:
            port = 443
        else:
            port = 80

        driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/index-setup/regions/".format(shopDomen.replace("//",""), port))
        if driver.IsTextsPresents(["вам не принадлежит.", "Заявка принята в обработку", "Подтвердите права на"]):
            continue

        driver.WaitFor(objects.region_editRegionButton)
        driver.Click(objects.region_editRegionButton)
        time.sleep(0.5)
        if driver.IsVisible(objects.region_addRegionButton) == False:
            continue

        driver.Click(objects.region_addRegionButton)

        if driver.IsTextsPresents(["Заявка принята в обработку", "Изменения будут применены"]):
            continue

        driver.WaitForTextNotVisible("подозрительная")
        driver.WaitFor(objects.region_RegionEdit)

        try:
            pageId = prestashop_api.GetIdCmsByLinkRewrite(shopName.replace(" ", "-"))
            pageUrl = shopDomen + "/content/" + str(pageId) + "-delivery-" + shopName.replace(" ", "-")
        except:
            continue
        driver.WaitFor(objects.region_RegionEdit)
        driver.SendKeys(objects.region_RegionEdit, shopName)
        time.sleep(1)
        if driver.IsVisible(objects.region_NoRegion) == False:
            driver.WaitFor(objects.region_RegionVariant)
            try:
                driver.Click(objects.region_RegionVariant)
            except:
                continue
            driver.WaitForNotVisible(objects.region_RegionVariant)
            driver.SendKeys(objects.region_UrlEdit, pageUrl)
            driver.Click(objects.region_SetRegionButton)
            time.sleep(1)
##            driver.WaitForText("Заявка принята в обработку")
##            logging.LogInfoMessage("Region for {0} ({1}) has been added".format(shopDomen, shopId))

    if machineName.find("hp") != -1:
        ssh.kill()

yandex_.selenium.CleanUp()
mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (regions)", "", [logFileName])