﻿# -*- coding: utf-8 -*-
import myLibrary
import datetime
import log
import WebasystAPI
import yandex
import UYandexObjects
import commonLibrary
import SendMails
import sys
import subprocess
import win32api
import win32com.client
import time

############################################################################

webasyst_api = WebasystAPI.WebasystAPI()
mails = SendMails.SendMails()
objects = UYandexObjects.UYandexObjects
shell = win32com.client.Dispatch("WScript.Shell")
todayDate = datetime.date.today()

message = ""
machineName = commonLibrary.GetMachineName()

siteurls = [i["url"] for i in commonLibrary.wa_sitesParams]
reindexData = webasyst_api.GetReindexData()

quotas = {}

for reindex in reindexData:
    id, site, shopname, categories, products, otherPages, lastStorefrontId, lastLaunchDate = reindex
    if lastLaunchDate not in [None, ""]:
        pastDays = (todayDate - lastLaunchDate).days
        if pastDays == 0:
            continue

    site = site.replace("https", "http")
    site = site.strip("/")

    if site not in siteurls:
        continue

    if site not in quotas.keys():
        quotas[site] = commonLibrary.GetReindexQuota(site)

    if quotas[site] == 0:
        continue

    try:
        categories = [x.strip() for x in categories.split(",")]
    except:
        categories = []
    try:
        products = [x.strip() for x in products.split(",")]
    except:
        products = []
    try:
        otherPages = [x.strip() for x in otherPages.split("|")]
    except:
        otherPages = []

    webasyst_api = commonLibrary.CreateWebasystSession(site)
    if webasyst_api == -1:
        continue

    bSubfolders = webasyst_api.IsSubfolderSite()

    message += "================= " + webasyst_api.siteUrl + " ===============\n\n"

    loginData = commonLibrary.GetYandexLoginData(site)

    site_ = commonLibrary.GetWaSiteParams(site)
    yandex_ = yandex.Yandex(True, site_["ip"])
    driver = yandex_.selenium
    time.sleep(1)
    yandex_.Login(loginData[0], loginData[1], False)

    driver.OpenUrl("https://webmaster.yandex.ru/sites/")
    driver.WaitForTexts(["Мои сайты", "Информация о сайте"])

    bErrors = False
    bNeedContinue = False
    lastStorefrontId_ = -1

    port = 443

    domain = site.replace("http://", "https://")
    driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/indexing/reindex/".format(domain.replace("//",""), port))
    driver.WaitFor(objects.reindex_quota)
    qText = driver.GetText(objects.reindex_quota)
    if qText.find(u"Сегодня можно отправить") != -1:
        quotas[site] = int(qText.split(u"отправить еще ")[-1].split(" ")[0])

    storefrontId = 1
    storefronts = webasyst_api.GetStorefrontsIds()
    for storefrontId in storefronts:
        if driver.IsTextPresent("Сегодня вся квота израсходована."):
            quotas[site] = 0
            bNeedContinue = True
            if storefrontId != 1:
                lastStorefrontId_ = storefrontId
            break

        shopName = webasyst_api.GetStorefrontName(storefrontId)

        if shopname not in [None, ""]:
            if shopname != shopName:
                continue
        else:
            if len(otherPages) > 0 and storefrontId != 1:
                continue
            if storefrontId <= lastStorefrontId or shopName not in (commonLibrary.mainCities1 + commonLibrary.mainCities2):
                continue

        print storefrontId

        urls = []
        mainDomain = webasyst_api.GetStorefrontUrlById(storefrontId)
        try:
            for categoryId in categories:
                url = webasyst_api.GetCategoryFullUrl(categoryId, shopName)
                urls.append(url)

            for productId in products:
                url = webasyst_api.GetProductFullUrl(productId, shopName)
                urls.append(url)

##            for otherPage in otherPages:
##                if otherPage.find("/") == -1:
##                    artId = webasyst_api.GetArticleId(otherPage, shopId)
##                    views = webasyst_api.GetArticleViews(artId)
##                    otherPage = webasyst_api.GetArticleUrlById(artId)
##                    if otherPage == -1:
##                        continue
##                if not bSubfolders:
##                    url = "/{0}".format( otherPage)
##                else:
##                    url = "{0}/{1}".format(mainDomain, otherPage)
##                urls.append(url)
        except:
            continue

        if len(urls) == 0:
            continue

        if bSubfolders == False:
            domain = "https://" + mainDomain
            driver.OpenUrl("https://webmaster.yandex.ru/site/{0}:{1}/indexing/reindex/".format(domain.replace("//",""), port))
            time.sleep(1)
            if driver.IsTextsPresents(["вам не принадлежит", "Подтверждение прав"]):
                continue

        driver.WaitFor(objects.reindex_urls)
        if driver.IsVisible(objects.reindex_closeButton):
            driver.Click(objects.reindex_closeButton)
            time.sleep(1)

        quotas[site] -= len(urls)

        try:
            driver.Click(objects.reindex_urls)
            for url in urls:
                for c in url:
                    shell.SendKeys(c)
                time.sleep(1)
                shell.SendKeys("{ENTER}")
                time.sleep(1)

            driver.Click(objects.reindex_sendButton)
            while driver.GetText(objects.reindex_lines).strip() != "":
                time.sleep(1)

        except:
            message += "[Error] Ошибка при отправке урлов на переиндексацию\n"
            bNeedContinue = True
            bErrors = True
            break

        if quotas[site] < len(urls):
            bNeedContinue = True
            lastStorefrontId_ = storefrontId
            break

    if bNeedContinue:
        if bErrors == False:
            webasyst_api.SetReindexLastLaunchDate(id, todayDate)
            if lastStorefrontId_ not in [-1, 1]:
                webasyst_api.SetReindexLastShopId(id, lastStorefrontId_)
    else:
        webasyst_api.DeleteReindex(id)
        message += "Урлы отправлены на переиндексацию, запись удалена\n\n"

    yandex_.selenium.CleanUp()

if message != "":
    result = mails.SendInfoMail("Переиндексация", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))