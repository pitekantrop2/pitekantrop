﻿import myLibrary
import UAvitoObjects
import datetime
import time, threading
import urllib
import shutil
import requests
from PIL import Image
import subprocess
import log
from bs4 import BeautifulSoup
import re, urlparse
import PrestashopAPI
import win32api
import win32com.client
import html2text
from lxml import etree
import HTMLParser

logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\avito\\log")

baseUrl = "https://www.avito.ru"
logging = log.Log(logFileName)

def OpenChooseFileDialog():
    Driver.Click(UAvitoObjects.UAvitoObjects.AddImage)

def GetAvitoCategory(category):

    params = {}
    params["categories"] = []

    if category.find(u"тпуги") != -1:

        params["categories"].append(u"Для дома и дачи")
        params["categories"].append(u"Бытовая техника")

        params["parameters"] = "Другое"
        return params

    if category.find(u"кроскоп") != -1:

        params["categories"].append(u"Бытовая электроника")
        params["categories"].append(u"Товары для компьютера")

        params["parameters"] = "Аксессуары"
        return params

    if category.find(u"подавители") != -1 or category.find(u"переговоров") != -1:

        params["categories"].append(u"Бытовая электроника")
        params["categories"].append(u"Аудио и видео")

        params["parameters"] = "Аксессуары"
        return params

    if category.find(u"сигнал") != -1:

        params["categories"].append(u"Для дома и дачи")
        params["categories"].append(u"Бытовая техника")

        params["parameters"] = "Другое"
        return params

    params["categories"].append(u"Бытовая электроника")
    params["categories"].append(u"Аудио и видео")

    params["parameters"] = "Аксессуары"

    return params


def SaveCaptchaImage(afterError = False):
    im = Driver.MyFindElement(UAvitoObjects.UAvitoObjects.CaptchaImage)
    screen = Driver.GetScreenshotAsFile("screen.png")
    i = Image.open("screen.png")
    width, height = i.size
##    bbox = (int( im.location["x"]),int(im.location["y"]),int( im.location["x"]) + im.size["width"],int(im.location["y"]) + im.size["height"])
    if afterError == False:
        bbox = (435,678,530,707)
    else:
        bbox = (435,655,530,685)
    cap = i.crop(bbox)
    cap.save("captcha.png")

def GetCaptcha(pictureName):
    program = """c:\\programming\\C#\\Antigate\\Antigate\\bin\\Release\\Antigate.exe"""
    gate = subprocess.Popen(program + " " + "imagePath" + pictureName,  stdout=subprocess.PIPE, shell=True)
    (output, err) = gate.communicate()
    return output

def PostComplaint(captchaId):
    program = """c:\\programming\\C#\\Antigate\\Antigate\\bin\\Release\\Antigate.exe"""
    gate = subprocess.Popen(program + " " + "complaint" + captchaId,  stdout=subprocess.PIPE, shell=True)
    (output, err) = gate.communicate()
    return output

def AddImage(imagePath):
    loaded = False
    while loaded == False:
        t1 = threading.Thread( target=OpenChooseFileDialog)
        t1.start()
        time.sleep(4)
        for c in imagePath:
            shell.SendKeys(c)
            ##time.sleep(0.1)
        shell.SendKeys("{ENTER}")
        shell.SendKeys("{ENTER}")
        time.sleep(1)
        try:
            Driver.WaitFor(UAvitoObjects.UAvitoObjects.DeleteImage)
            loaded = True
        except:
            Driver.Click(UAvitoObjects.UAvitoObjects.ContinueButton)

def Login(login, password):
    Driver.OpenUrl(baseUrl + "/profile/login?next=%2Fprofile")
    Driver.SendKeys(UAvitoObjects.UAvitoObjects.Login, login)
    Driver.SendKeys(UAvitoObjects.UAvitoObjects.Password, password)
    Driver.Click(UAvitoObjects.UAvitoObjects.Submit)
    Driver.WaitFor(UAvitoObjects.UAvitoObjects.AddAdv)

def AddAd(item):
    Driver.OpenUrl(baseUrl + "/additem")


    for category in item["categories"]:
        Driver.Click( ["xpath", "//span[contains(., '" + category + "')]"])
        time.sleep(1)
    Driver.SetSelectOptionByText(UAvitoObjects.UAvitoObjects.Params, item["params"])
    Driver.SendKeys(UAvitoObjects.UAvitoObjects.Title, item["title"])
    Driver.SendKeys(UAvitoObjects.UAvitoObjects.Description, item["description"])
    Driver.SendKeys(UAvitoObjects.UAvitoObjects.Price, item["price"])
    if item["image"] <> "":
        AddImage(item["image"])

################################################################################

##prestashop_api = PrestashopAPI.PrestashopAPI("http://otpugiwatel.com", 'QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6', "127.0.0.1", "otpug", "otpug_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://tomsk.otpugivatel.com", 'QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6', "127.0.0.1", "otpugivatel", "otpugiv_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://amazing-things.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", "amazing-things", "am_th_user_ex", "1qaz@WSX", 5555)
prestashop_api = PrestashopAPI.PrestashopAPI("http://omsk.knowall.ru.com", 'KAR7M17CPPALJY38NKI521NNP96IH3Z7', "127.0.0.1", "knowall", "knowall_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://safetus.ru", 'BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU', "127.0.0.1", "safetus", "safetus_user_ex", "1qaz@WSX", 5555)

Driver = myLibrary.Selenium()
Driver.SetUp()

shell = win32com.client.Dispatch("WScript.Shell")

##Login("sevastopol@knowall.ru.com", "1qaz@WSX")
Login("rostov-na-donu@knowall.ru.com", "1qaz@WSX")

products = prestashop_api.GetShopProducts(10)

for product in products:

    id_product = product[0]

    if id_product <= 1334 :
        continue

    if prestashop_api.IsProductActive(id_product) == 0:
        continue

    if prestashop_api.IsProductInBackoffice(id_product) == False:
        continue

    if prestashop_api.GetProductShopQuantity(id_product, 10) == 0:
        continue

    print str(id_product)
    try:
        productCategoryId = prestashop_api.GetProductCategoryDefault(id_product)
        productCategory = prestashop_api.GetShopCategoryDetails(10, productCategoryId)[0][4]
    except:
        continue

##    if productCategoryId in [16, 56, 57, 58, 59, 183, 184, 186, 187, 188, 189 ]:
    if productCategoryId in [16, 183, 184, 186, 187, 188,189,104, 105, 106, 96]:
        continue

    if productCategory.find(u"камер") != -1:
        continue

    if prestashop_api.IsCategoryActive( productCategoryId) != 1:
        continue

    productArticle = prestashop_api.GetProductArticle(id_product)

    if productArticle.startswith("sv") == True:
        continue

    if productArticle == "" and productCategory.find(u"тпугив") == -1:
        continue

    name, price, wholesale_price, description, link_rewrite, meta_title = prestashop_api.GetShopProductDetailsById(id_product, 10)

    item = {}

    params = GetAvitoCategory(productCategory)

    item["categories"] = params["categories"]
    item["params"] = params["parameters"]

    item["title"] = name

##    productDescription = etree.HTML(description)
##    elements = productDescription.xpath("//a")
##
##    for element in elements:
##        element.getparent().remove(element)
##
##    elements = productDescription.xpath("//img")
##
##    for element in elements:
##        element.getparent().remove(element)
##
##    description = etree.tostring(productDescription)
##    regexp = "&.+?;"
##    list_of_html = re.findall(regexp, description) #finds all html entites in page
##    for e in list_of_html:
##        h = HTMLParser.HTMLParser()
##        unescaped = h.unescape(e) #finds the unescaped value of the html entity
##        description = description.replace(e, unescaped) #replaces html entity with unescaped value
##
##    description = description.replace("<html>","")
##    description = description.replace("</html>","")
##    description = description.replace("<body>","")
##    description = description.replace("</body>","")
##
##    description = html2text.html2text(description)
##
##    tIndex = description.rfind(u'Технические характеристики')
##    cIndex = description.rfind(u'Комплектация')
##
##    if cIndex == tIndex:
##        continue
##    else:
##
##        if tIndex == -1:
##            index = cIndex
##        else:
##            if cIndex == -1:
##                index = tIndex
##            else:
##                if cIndex > tIndex:
##                    index = tIndex
##                else:
##                    index = cIndex
##
##    description = description[index:]
##
##    description = description.replace("###", "")
##    description = description.replace("**", "")

    item["description"] = name
    item["price"] = str(int(price))
    result = prestashop_api.GetImage("products", id_product)
    if result == True:
        item["image"] = "c:\\programming\\_SELENIUM\\avito\\pic.jpg"
    else:
        item["image"] = ""

    if productArticle.startswith("ts") == True and item["image"] == "":
        continue

    AddAd(item)
    Driver.Click(UAvitoObjects.UAvitoObjects.RegularSale)
    Driver.Click(UAvitoObjects.UAvitoObjects.ContinueButton)

    afterError = False
    while True:
        Driver.SendKeys(UAvitoObjects.UAvitoObjects.CaptchaInput, "123")
        SaveCaptchaImage(afterError)
        captcha = GetCaptcha("captcha.png").split(" ")
        captchaText = captcha[0]
        captchaId = captcha[1].replace("\r\n", "")
        try:
            Driver.SendKeys(UAvitoObjects.UAvitoObjects.CaptchaInput, captchaText.decode("utf-8"))
        except:
            Driver.Click(UAvitoObjects.UAvitoObjects.ForwardButton)
            afterError = True
            continue

        Driver.Click(UAvitoObjects.UAvitoObjects.ForwardButton)
        time.sleep(1)
        if Driver.IsTextPresent("Текст не совпадает с картинкой") == True or \
            Driver.IsTextPresent("Введите текст с картинки") == True:
            afterError = True
            if captchaText.find("NO_SLOT") == -1:
                PostComplaint(captchaId)
                continue

        Driver.WaitForText("Подключение")
        break


    logging.Log(str(id_product) + " has been added")

Driver.CleanUp()