﻿from UBaseObjects import *

class UAvitoObjects(UBaseObjects):

    Login = ["name", "login"]
    Password = ["name", "password"]
    Submit = ["xpath", "//button[@type = 'submit']"]

    AddAdv = ["xpath", "//*[@class = 'btn-plain btn-plain_blue']"]
    AddImage = ["xpath", "//div[contains(@class, 'js-uploader-add')]"]
    DeleteImage = ["xpath", "//i[contains(@class, 'js-uploader-item-delete')]"]

    Category = ["id", "fld_category_id"]
    Title = ["id", "item-edit__title"]
    Description = ["id", "item-edit__description"]
    Price = ["id", "item-edit__price"]
    NextButton = ["id", "form_submit"]
    Params = ["xpath", "//select[contains(@id, 'flt_param')]"]
    RegularSale = ["xpath", "//span[contains(., '" + u"Обычная продажа" + "')]"]
    ContinueButton = ["xpath", "//button[contains(., '" + u"Продолжить с пакетом «Обычная продажа»" + "')]"]
    CaptchaInput = ["name", "captcha"]
    CaptchaImage = ["class", "form-captcha-image"]
    ForwardButton = ["xpath", "//button[contains(., '" + u"Продолжить" + "')]"]

    #обновление
    Page = ["class", "pagination__page"]
    UpdateLink = ["xpath", "//a[contains(., '" + u"Активировать" + "') and not(contains(., '60'))]"]



