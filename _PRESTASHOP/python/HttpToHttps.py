﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import PrestashopAPI
import io
import shutil
import ftplib
import os
import commonLibrary
import subprocess
##from yattag import Doc
import re



def ChangeArticlesImg():
    city = shopDomen.split(".")[0]

    print shopDomen
    shopName = commonLibrary.GetShopName(shopName)

    articles = prestashop_api.GetShopArticles(shopId)
    for articleId in articles:
        bNeedUpdate = False
        content = prestashop_api.GetArticleContent(articleId)
        ldElement = etree.HTML(content)
        elements = ldElement.xpath("//img")
        for element in elements:
            src = element.attrib["src"]
            src = src.replace("http://", "https://")
            element.attrib["src"] = src
            content = commonLibrary.GetStringDescription(ldElement)
            bNeedUpdate = True
        if bNeedUpdate == True:
            prestashop_api.SetArticleContent(articleId, content)


def UpdateLongDescr():
    for catid in cats:
        descr = prestashop_api.GetShopCategoryLongDescription(shopId, catid)
        if descr.find("http://") != -1:
            descr = descr.replace("http://", "https://")
            prestashop_api.SetShopCategoryLongDescription(shopId, catid, descr)


def UpdateProductsDescr():
    for prodid in prods:
        descr = prestashop_api.GetShopProductDescription(shopId, prodid)
        if descr != -1 and descr.find("http://") != -1:
            descr = descr.replace("http://", "https://")
            prestashop_api.SetShopProductDescription(shopId, prodid, descr)


############################################################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "HttpToHttps")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()

prestashop_api = PrestashopAPI.PrestashopAPI()

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    shops = prestashop_api.GetAllShops()
    cats = prestashop_api.GetActiveCategoriesIds()
    ##    prods = prestashop_api.GetActiveProductsIDs()
    prods = prestashop_api.GetContractorProductsIds("st")

    for shop in shops:
        shopId, shopDomen, shopName = shop
        if shopId < 417:
            continue
        logging.Log(str(shopId))

        try:
##            ChangeArticlesLinksSubdomains()
##            UpdateLongDescr()
            UpdateProductsDescr()
        except:
            prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])



    if machineName.find("hp") != -1:
        ssh.kill()



