﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess
import SendMails


def ConfigureLayredBlock(shopId,categoriesIds, featuresIds):

    global last_insert_id

    if len(featuresIds) == 0:

        sql = """INSERT INTO ps_layered_filter_shop(id_layered_filter, id_shop)
                 VALUES (1, %(shopId)s)"""%{"shopId":shopId}

        prestashop_api.MakeUpdateQueue(sql)

    for id_category in categoriesIds:

        if len (featuresIds) != 0:

            for featureId in featuresIds:

                id_value = featureId
                type_ = "id_feature"
                filter_type = 0
                idv = 0
                last_insert_id += 1

                sql = """INSERT INTO ps_layered_category(id_layered_category, id_shop, id_category, id_value, type, position, filter_type, filter_show_limit)
                            VALUES (%(last_insert_id)s,%(shopId)s,%(id_category)s,%(id_value)s,'%(type_)s',%(position)s,%(filter_type)s,0)"""%{"shopId":shopId,
                            "id_category":id_category,"id_value":id_value,"type_":type_, "position":featureId, "filter_type":filter_type, "last_insert_id":last_insert_id}

                prestashop_api.MakeUpdateQueue(sql)

        else:#добавление фильтра цены

            id_value = "NULL"
            type_ = "price"
            filter_type = 1
            last_insert_id += 1

            sql = """INSERT INTO ps_layered_category(id_layered_category, id_shop, id_category, id_value, type, position, filter_type, filter_show_limit)
                        VALUES (%(last_insert_id)s,%(shopId)s,%(id_category)s,%(id_value)s,'%(type_)s',%(position)s,%(filter_type)s,0)"""%{"shopId":shopId,
                        "id_category":id_category,"id_value":id_value,"type_":type_, "position":"0", "filter_type":filter_type, "last_insert_id":last_insert_id}

            prestashop_api.MakeUpdateQueue(sql)

#########################################################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "layeredFilter")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()

for site in commonLibrary.sitesParams:
    if machineName == "hp":
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    startShopId = 0

    shops = prestashop_api.GetAllShops()
    featuresIds = prestashop_api.GetAllFeaturesIds()
    categoriesIds = [x[0] for x in prestashop_api.GetShopActiveCategories(1)]

    prestashop_api.DeleteLayeredCategories()
    prestashop_api.DeleteLayeredFilterShops()

    last_insert_id = prestashop_api.GetLastInsertedId("ps_layered_category", "id_layered_category")

    for shop in shops:
        shopId, shopDomen, shopName = shop

        if shopId < startShopId: continue

        try:
            ConfigureLayredBlock(shopId, categoriesIds, [])
            ConfigureLayredBlock(shopId, categoriesIds, featuresIds)
        except:
            startShopId = shopId + 1
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))
            prestashop_api = commonLibrary.GetPrestashopInstance(site)
            if prestashop_api == False:
                mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
                try:
                    ssh.kill()
                except:
                    pass
                continue

        logging.LogInfoMessage(u"Filter for shop {0} has been updated".format(shopId))

    if machineName == "hp":
        ssh.kill()


mails.SendInfoMail("Обновление фильтров", "", [logfile])

