﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import PrestashopAPI
import io
import shutil
import ftplib
import os
import commonLibrary
import subprocess
##from yattag import Doc
import re

def UrlsToVirtualUri():
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        if shopId == 1:
            continue

        parts = shopDomen.split(".")
        mainDomain = shopDomen.replace(parts[0] + ".","")
        virtualUri = parts[0]+"/"

        sql = """UPDATE ps_shop_url
               SET domain = '{0}', domain_ssl = '{0}', virtual_uri = '{1}'
               WHERE id_shop = {2}""".format(mainDomain, virtualUri, shopId)

        prestashop_api.MakeUpdateQueue(sql)

def UrlsToSubdomains():
    shops = prestashop_api.GetShopsUrls()

    for shop in shops:
        if shop[5] == "":
            continue
        shopId = shop[1]
        mainDomain = shop[2]
        city = shop[5].replace("/","")
        if shopId == 1:
            continue

        subDomain = "{0}.{1}".format(city,mainDomain)

        sql = """UPDATE ps_shop_url
               SET domain = '{0}', domain_ssl = '{0}', virtual_uri = ''
               WHERE id_shop = {1}""".format(subDomain, shopId)

        prestashop_api.MakeUpdateQueue(sql)

def GenerateHtaccess():
    if os.path.isfile("rules.txt"):
        os.remove("rules.txt")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        parts = shopDomen.split(".")
        if len(parts) == 2:
            continue

        mainDomain = "{0}.{1}".format(parts[1],parts[2])
        city = parts[0]

        part = """
RewriteRule ^{0}$ /{0}/ [L,R]
RewriteRule ^{0}/(.*) /$1 [L]
""".format(city, mainDomain)

        with open("rules.txt", "a") as myfile:
            myfile.write(part)

def GenerateHtaccess2():
    shops = prestashop_api.GetShopsUrls()

    for shop in shops:
        mainDomain = shop[2]
        city = shop[5].replace("/","")

        part = """
RewriteRule ^{0}$ /{0}/ [L,R]
RewriteRule ^{0}/(.*) /$1 [L]
""".format(city, mainDomain)

##RewriteCond %{{HTTP_HOST}} ^{0}.{1}$
##RewriteRule ^(.*)$  https://{1}/{0}/$1 [R=301,L]

        with open("rules.txt", "a") as myfile:
            myfile.write(part)


def EditCategoryArticlesSubfolders():
    shops = prestashop_api.GetShopsUrls()

    for shop in shops:
        shopId = shop[1]
        print shopId
        city = shop[5].replace("/","")
        if city == "":
            continue

        shopArticles = prestashop_api.GetShopCategoriesArticles(shopId)
        for article in shopArticles:
            id_category, articles = article
            if articles == "":
                continue
            newArticles = articles.replace("/smartblog/","/{0}/smartblog/".format(city))
            prestashop_api.SetCategoryArticles(shopId, id_category, newArticles)

def EditCategoryArticlesSubdomains():
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        if shopId == 1:
            continue

        city = shopDomen.split(".")[0]

        shopArticles = prestashop_api.GetShopCategoriesArticles(shopId)
        for article in shopArticles:
            id_category, articles = article
            if articles == "":
                continue
            newArticles = articles.replace("/{0}/smartblog/".format(city),"/smartblog/")
            prestashop_api.SetCategoryArticles(shopId, id_category, newArticles)

def ChangeArticlesLinksSubfolders(step):
    shops = prestashop_api.GetShopsUrls()

    for shop in shops:
        shopId = shop[1]
        city = shop[5].replace("/","")
        if city == "":
            continue

        print city
        shopName = prestashop_api.GetShopName(shopId)

        articles = prestashop_api.GetShopArticles(shopId)
        for articleId in articles:
            content = prestashop_api.GetArticleContent(articleId)
            if step == 2:
                ldElement = etree.HTML(content)
                elements = ldElement.xpath("//a")
                for element in elements:
                    element.attrib["href"] = prestashop_api.Transliterate(element.attrib["href"])

                content = commonLibrary.GetStringDescription( ldElement)
            else:
                content = content.replace(u"-купить-" + shopName, "").replace(u"-купить-" + shopName.lower(), "")
                content = content.replace("""href=\"""","""href=\"/{0}""".format(city))
            prestashop_api.SetArticleContent(articleId, content)


def ChangeArticlesLinksSubdomains(step):
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        city = shopDomen.split(".")[0]

        print shopDomen
        shopName = commonLibrary.GetShopName(shopName)

        articles = prestashop_api.GetShopArticles(shopId)
        for articleId in articles:
            bNeedUpdate = False
            content = prestashop_api.GetArticleContent(articleId)
            if step == 2:
                ldElement = etree.HTML(content)
                elements = ldElement.xpath("//img")
                for element in elements:
                    src = element.attrib["src"]
                    src = src.replace("http://", "https://")
                    element.attrib["src"] = src
                    content = commonLibrary.GetStringDescription(ldElement)
                    bNeedUpdate = True
##                    p = re.compile('[0-9]+')
##                    id = p.findall(element.attrib["href"])[0]
##                    if prestashop_api.IsProductActive(id) == 0:
##                        print id
##                        element.getparent().remove(element)
##                        bNeedUpdate = True
##                if bNeedUpdate == True:
##                    content = commonLibrary.GetStringDescription(ldElement)
##                    element.attrib["href"] = prestashop_api.Transliterate(element.attrib["href"])
            else:
                bNeedUpdate = True
                content = content.replace(u"-купить-" + shopName, "").replace(u"-купить-" + shopName.lower(), "")

            if bNeedUpdate == True:
                prestashop_api.SetArticleContent(articleId, content)

def GenerateSitemap():
    doc, tag, text = Doc().tagtext()

    shops = prestashop_api.GetShopsUrls()

    for shop in shops:
        shopId = shop[1]
        city = shop[5].replace("/","")

        if city == "":
            continue

        protocol = commonLibrary.GetSiteUrlScheme(site)

        shopUrl = protocol + shop[2] + "/" + shop[5]
        shopName = prestashop_api.GetShopName(shopId)

        with tag('h4'):
            with tag('a', href = shopUrl):
                text(shopName)

        categories = prestashop_api.GetShopCategories(shopId)
        with tag('ul'):
            for category in categories:
                id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = category
                if prestashop_api.IsCategoryActive(id_category) == 0 or id_category in [1, 2]:
                    continue

                categoryUrl = shopUrl + u"{0}-{1}".format(id_category, link_rewrite)
                with tag('li'):
                    with tag('a', href = categoryUrl):
                        text(name)

    with open("sitemap.htm", "a") as htm:
        htm.write (doc.getvalue().encode('cp1251'))

############################################################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "configuration")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()

prestashop_api = PrestashopAPI.PrestashopAPI()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

##    GenerateSitemap()
##    GenerateHtaccess2()
##    UrlsToSubdomains()
##    UrlsToVirtualUri()
##    EditCategoryArticlesSubfolders()
##    EditCategoryArticlesSubdomains()
##    ChangeArticlesLinksSubfolders(1)
    ChangeArticlesLinksSubdomains(2)

    if machineName.find("hp") != -1:
        ssh.kill()



