﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import SMSAPI
import psutil
import commonLibrary

states = [
u"Отправлен",
u"Вручен"
]

now = datetime.datetime.now()
today = datetime.date.today()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "checkOrdersPayment")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()

mails = SendMails.SendMails()
prestashop_api = PrestashopAPI.PrestashopAPI()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=190)

sendLetter = False

message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    bTitle = False

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue


    logging.Log("==================================  " + site["url"] + "  ==================================")

    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:
        orderId = order[0]
        boUrl = "{0}?controller=AdminOrders&id_order={1}&vieworder".format(backofficeUrl, orderId)
        trackingNumber = prestashop_api.GetOrderTrackingNumber(orderId)

        if trackingNumber in [None ,"", -1]:
            continue

        trackingNumber = trackingNumber.strip()
        orderState = prestashop_api.GetOrderState(orderId)
        orderStateDate = prestashop_api.GetOrderStateDate(orderId, orderState)

        if orderState == u"Отправлен":
            days = -1
            pastDays = (today - orderStateDate.date()).days
            if len(trackingNumber) == 10 and pastDays > 10:
                days = 10
            if len(trackingNumber) == 14 and pastDays > 20:
                days = 20

            if days != -1:
                sendLetter = True
                if bTitle == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    bTitle = True
                message += "Заказ <a target='blank_' href='{0}'>{1}</a> (<a target='blank_' href='https://www.cdek.ru/track.html?order_id={2}'>{2}</a>) не доставлен более {3} дней\n".format(boUrl, orderId, trackingNumber, days)
                logging.Log(u"Заказ {0} ({1}) не доставлен более {2} дней".format(orderId, trackingNumber, days))

        if orderState == u"Вручен":
            days = -1
            pastDays = (today - orderStateDate.date()).days
            if len(trackingNumber) == 10 and pastDays > 25:
                days = 25
            if len(trackingNumber) == 14 and pastDays > 10:
                days = 10
            if days != -1:
                if days == 25:
                    trackUrl = "https://www.cdek.ru/track.html?order_id="
                else:
                    trackUrl = "https://www.pochta.ru/tracking#"
                sendLetter = True
                if bTitle == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    bTitle = True
                message += "Заказ <a target='blank_' href='{0}'>{1}</a> (<a target='blank_' href='{4}{2}'>{2}</a>) не оплачен более {3} дней\n".format(boUrl, orderId, trackingNumber, days, trackUrl)
                logging.Log(u"Заказ {0} ({1}) не оплачен более {2} дней".format(orderId, trackingNumber, days))

    if machineName.find("hp") != -1:
        ssh.kill()

if sendLetter == True:
    result = mails.SendInfoMail("Проверка статусов заказов", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(50)