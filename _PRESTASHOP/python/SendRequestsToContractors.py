﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary
import ModulbankAPI

##prestashop_api = PrestashopAPI.PrestashopAPI()

def GetAllItems(city):
    prestashop_api = PrestashopAPI.PrestashopAPI()

    if city == u"Москва":
        sql = u"""SELECT items FROM deliveries
                 WHERE recipientCity = '{0}'""".format(u"Москва")
    else:
        sql = u"""SELECT items FROM deliveries
                 WHERE recipientCity <> '{0}'""".format(u"Москва")

    data = prestashop_api.MakeLocalDbGetInfoQueue(sql)
    if len(data) == 0:
        return ""

    return "/".join( [x[0] for x in data])

def GetExpectedProductQty(city, name, quantity):
    global mskAllItems
    global spbAllItems

    expectedQty = 0

    if city == u"Москва":
        allItems = mskAllItems
    else:
        allItems = spbAllItems

    if allItems == "":
        return 0

    itemsNames, itemsQuantities = commonLibrary.ParseItems(allItems)
    allItems = u""
    for i in range(0, len(itemsNames)):
        itemName = itemsNames[i]
        itemQty = int(itemsQuantities[i])
        if itemName == name:
            if itemQty >= quantity:
                expectedQty = quantity
                itemsQuantities[i] = itemQty - quantity
            else:
                expectedQty = itemQty
                itemsQuantities[i] = 0

        allItems += u"{0}_{1}/".format(itemsNames[i], itemsQuantities[i])

    allItems = allItems.strip("/")
    if city == u"Москва":
        mskAllItems = allItems
    else:
        spbAllItems = allItems

    return expectedQty

#################################################################################

states = [
u"В процессе подготовки"]

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "sendRequestsToContractors")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

now = datetime.datetime.now()
if now.hour == 19:
    prestashop_api = PrestashopAPI.PrestashopAPI()
    prestashop_api.DeleteAllRequests()
    sys.exit()

contractorsItems = {}
additionalItems = {}
warehouseItems = {}

mskAllItems = GetAllItems(u"Москва")
spbAllItems = GetAllItems(u"Спб")

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=190)

#получаем платежи
operations = []
for flag in [True, False]:
    mb = ModulbankAPI.ModulbankAPI(flag)
    operations += mb.GetOperationsHistory(datetime.date.today().strftime('%Y-%m-%d'), "Credit")


innList = [x["contragentInn"] for x in operations]
paidContractors = [commonLibrary.contractorsInn[x] for x in innList if x in commonLibrary.contractorsInn.keys() ]

message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))
    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:
        orderId = order[0]
        orderNote = prestashop_api.GetOrderNote(orderId)
        bIsOrderAgreed = commonLibrary.GetOrderIsAgreed(orderNote)
        if bIsOrderAgreed == False:
            continue

        bNovallOrder = commonLibrary.GetNovallOrder(orderNote)
        orderCarrier = prestashop_api.GetOrderCarrierName(orderId).lower()
        orderItems = prestashop_api.GetOrderItems(orderId)
        orderShop = prestashop_api.GetOrderShopName(orderId)
        if orderCarrier.find("\"") != -1:
            orderCarrier = orderCarrier.split("\"")[0].strip()
        else:
            orderCarrier = orderCarrier.split(".")[0].strip()

        if (orderShop in [u"Санкт-Петербург", u"Спб"] and orderCarrier.find(u"пункт") == -1) or orderCarrier.find(u"очт") != -1:
            warehouse = "warehouse_spb"
            city = u"Спб"
        else:
            warehouse = "warehouse_moscow"
            city = u"Москва"

        for orderItem in orderItems:
            product_quantity = orderItem[3]
            productId = orderItem[0]
            productDetails = prestashop_api.GetShopProductDetailsById(productId, 10)
            productName = prestashop_api.GetProductOriginalName(productId)
            if productName == "":
                productName = orderItem[1]

            productName = commonLibrary.CleanItemName(productName)
            warehouseProductQuantity = prestashop_api.GetWarehouseProductQuantity(warehouse, productName)
            if warehouseProductQuantity == -1:
                warehouseProductQuantity = 0

            if warehouseProductQuantity >= product_quantity:
                continue

            deficitProductQty = product_quantity - warehouseProductQuantity

            expectedProductQty = GetExpectedProductQty(city, productName, deficitProductQty)
            if expectedProductQty >= deficitProductQty:
                continue

            deficitProductQty = deficitProductQty - expectedProductQty

            productArticle = prestashop_api.GetProductArticle(productId)
            contractor = commonLibrary.GetContractorName(productArticle)
            if contractor in [u"Берег мечты"]:
                continue

            if contractor == -1:
                continue

            if contractorsItems.has_key(contractor) == False:
                contractorsItems[contractor] = []

            if bNovallOrder == True:
                productName += u" (Новалл)"

            itemIndex = -1

            try:
                itemIndex = [x[0] for x in contractorsItems[contractor]].index(productName)
            except:
                pass

            if itemIndex != -1:
                contractorsItems[contractor][itemIndex][1] += deficitProductQty
            else:
                itemTuple = [productName, deficitProductQty, productDetails[2]]
                contractorsItems[contractor].append(itemTuple)

    if machineName.find("hp") != -1:
        ssh.kill()

#получаем список дополнительных товаров
requests = commonLibrary.GetRequests()

for request in requests:
    id, contractor, additionalProducts, emailSended, paid, doNotProcess = request

    if now.hour >= 15 and contractor not in ["Polikon"]:
        prestashop_api.DeleteRequest(id)
        continue

    if doNotProcess == 1 or (emailSended == 1 and paid == 1):
        continue

    if contractor in paidContractors :
        prestashop_api.SetRequestPaid(contractor, 1)
        continue

    contractor = commonLibrary.contractorsLabels[contractor]

    if additionalProducts not in ["", None]:
        itemsNames, itemsQuantities = commonLibrary.ParseItems(additionalProducts)
        for i in range(0,len(itemsNames)):
            itemName = itemsNames[i]
            itemQuantity = itemsQuantities[i]
            deficitProductQty = int(itemQuantity)

            if contractorsItems.has_key(contractor) == False:
                contractorsItems[contractor] = []

            itemIndex = -1

            try:
                itemIndex = [x[0] for x in contractorsItems[contractor]].index(itemName)
            except:
                pass

            if itemIndex != -1:
                contractorsItems[contractor][itemIndex][1] += int(deficitProductQty)
            else:
                itemTuple = [itemName, deficitProductQty, 0]
                contractorsItems[contractor].append(itemTuple)

for contractorName in contractorsItems.keys():
    label = commonLibrary.GetContractorLabel(contractorName)
    if label == -1:
        continue

    if label not in [x[1] for x in requests] and now.hour < 15:
        prestashop_api.AddRequest(label)

    bSendRequest = False
    name = ""

    email = "feedback@knowall.ru.com"
    if contractorName == u"Торнадо":
        if now.hour == 11:
            bSendRequest = True
        email = "cnt-tlt@yandex.ru"
        name = ", Галина"
    if contractorName == u"Атек":
        if now.hour == 13:
            bSendRequest = True
        email = "atekmks@mail.ru"
    if contractorName == u"Телесистемы":
        if now.hour == 14:
            bSendRequest = True
        email = "sales6@telesys.ru"
    if contractorName == u"Кварц":
        if now.hour == 13:
            bSendRequest = True
        email = "aleksei@konnektim.ru"
    if contractorName == u"Сититек":
        if now.hour == 13:
            bSendRequest = True
        email = "opt2@ruopt1.ru"
    if contractorName == u"Поликон":
        if now.hour == 15:
            bSendRequest = True
        email = "9917959@mail.ru"
        name = ", Елена"
    if contractorName == u"31 Век" and now.hour == 10:
        bSendRequest = True
    if contractorName == u"Логос" and now.hour == 13:
        bSendRequest = True

    email = "feedback@knowall.ru.com"

    if bSendRequest == False:
        continue

    if contractorName == u"Сититек":
        letterText = """Приветствую!

Выставь, пожалуйста, счет на:

{itemsList}

С уважением,
Иван"""
    elif contractorName in [u"31 Век", u"Логос"]:
        letterText = """
Нужно сделать заказ в '{contractor}':

{itemsList}"""
    else:
        letterText = """Здравствуйте{name}!

Выставьте, пожалуйста, счет на:

{itemsList}

С уважением,
Иван"""

    itemsList = ""

    for item in contractorsItems[contractorName]:
        itemsList += u"{0} - {1} шт.\n".format(item[0], item[1])

    letterText = letterText.replace("{itemsList}", itemsList.encode("utf-8"))
    letterText = letterText.replace("{name}", name)
    letterText = letterText.replace("{contractor}", contractorName.encode("utf-8"))
    result = mails.SendMailToPartner(email, "заказ", letterText)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма в {0}: {1}".format(contractorName, result.decode("cp1251")))
        message += "[Error] Ошибка при отправке письма в {0}: {1}\n".format(contractorName.encode("utf-8"), str(result))
    else:
        if contractorName not in [u"31 Век", u"Логос"]:
            logging.LogInfoMessage(u"Отправлен запрос в '{0}' на: {1}".format(contractorName, itemsList))
            message += "Отправлен запрос в '{0}' на:\n{1}\n\n".format(contractorName.encode("utf-8"), itemsList.encode("utf-8"))
        prestashop_api.SetRequestSended(label, 1)

if message != "":
    result = mails.SendInfoMail("Отправка запросов поставщикам", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(50)
