﻿import log
import datetime
import sys
import PrestashopAPI
import SendMails
import io
import subprocess
import time
import psutil
import commonLibrary
import TextruAPI
from lxml import etree

def GetAllItems():
    prestashop_api = PrestashopAPI.PrestashopAPI()
    sql = u"""SELECT items FROM deliveries"""
    data = prestashop_api.MakeLocalDbGetInfoQueue(sql)
    if len(data) == 0:
        return ""

    items = [x[0] for x in data]

    data_ = prestashop_api.GetAliexpressOrders()
    if len(data_) != 0:
        items += [x[2] for x in data_]

    return "/".join(items)

def GetExpectedProductQty(name):
    for i in range(0, len(itemsNames)):
        itemName = itemsNames[i]
        itemQty = int(itemsQuantities[i])
        if itemName == name:
            return itemQty

    return 0


################################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

message = ""

allItems = GetAllItems()
itemsNames, itemsQuantities = commonLibrary.ParseItems(allItems)

products = {}

warehouse_moscow = prestashop_api.GetWarehouseProducts("warehouse_moscow")
warehouse_spb = prestashop_api.GetWarehouseProducts("warehouse_spb")

for product in warehouse_moscow:
    id, ItemName, ItemQuantity, WarehouseName, Sku_id, MinQty = product
    contractor = prestashop_api.GetProductContractor(ItemName)
    if contractor == -1:
        contractor = u"Неизвестный поставщик"
    try:
        spbItemQuantity = [x[2] for x in warehouse_spb if x[1] == ItemName][0]
    except:
        spbItemQuantity = 0
    totalQuantity = ItemQuantity + spbItemQuantity + GetExpectedProductQty(ItemName)
    if totalQuantity < MinQty:
        if contractor not in products.keys():
            products[contractor] = [[ItemName, MinQty, totalQuantity]]
        else:
            products[contractor].append([ItemName, MinQty, totalQuantity])

for contractor in products.keys():
    message += "\n=== {0} ===\n".format(contractor.encode("utf-8"))
    for product in products[contractor]:
        ItemName, MinQty, ItemQuantity = product
        message += "{0} - мин. кол-во {1}, в наличии {2} шт.\n".format(ItemName.encode("utf-8"), MinQty, ItemQuantity)

if message != "":
    result = mails.SendInfoMail("Заканчивающиеся товары", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(50)
