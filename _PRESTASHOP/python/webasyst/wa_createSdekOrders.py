﻿# -*- coding: utf-8 -*-
import log
import datetime
import sys
import WebasystAPI
import SdekAPI
import io
import SendMails
import subprocess
import time
import commonLibrary

states = [u"Оплачен", u"Подтвержден"]

################################################################################

def SetDoNotSendNote(orderNote):
    orderSender = commonLibrary.GetOrderSender(orderNote)
    if orderSender != -1:
        orderNote = orderNote.replace(orderSender,"")
    orderNote += "\ndonotsend"
    webasyst_api.SetOrderComment(orderId, orderNote)

def GetOrderDataForSDEK(orderNumber, orderNote):
    global data

    data["orderNote"] = orderNote
    orderSender = commonLibrary.GetOrderSender(orderNote)
    if orderSender == -1:
        return -1
    data["orderSender"] = orderSender
    data["orderPaid"] = webasyst_api.IsOrderPaid(orderNumber)
    bSingleOrder = commonLibrary.GetOrderIsSingle(orderNote)
    data["bSingleOrder"] = bSingleOrder
    customerData = webasyst_api.GetOrderCustomerData(orderNumber)
    data["customerId"] = customerData[4]
    places = commonLibrary.GetOrderPlaces(orderNote)
    if places != -1:
        data["Places"] = places
    data["bNovallOrder"] = commonLibrary.GetNovallOrder(orderNote)
    pvzCode = commonLibrary.GetPvzCode(orderNote)
    if pvzCode != -1:
        data["PvzCode"] = pvzCode
    tariffCode = commonLibrary.GetTariffCode(orderNote)
    if tariffCode != -1:
        data["TariffCode"] = tariffCode

    today = datetime.date.today()
    now = datetime.datetime.now()

    if bSingleOrder == False:
        data["Tariff"] = "S"
    else:
        data["Tariff"] = "D"
        courierRequestParams = commonLibrary.GetCourierRequestParams(orderSender)
        data["SendCityCode"] = courierRequestParams["SendCityCode"]
        data["SendStreet"] = courierRequestParams["SendStreet"]
        data["SendHouse"] = courierRequestParams["SendHouse"]
        data["SendFlat"] = courierRequestParams["SendFlat"]
        data["SendPhone"] = courierRequestParams["SendPhone"]
        data["SenderName"] = courierRequestParams["SenderName"]
        data["CourierTimeBeg"] = courierRequestParams["CourierTimeBeg"]
        data["CourierTimeEnd"] = courierRequestParams["CourierTimeEnd"]
        data["CourierDate"] = courierRequestParams["CourierDate"]

    try:
        data["Number"] = today.strftime('%d%m') + str(orderNumber)
        if orderSender == "SDEKMoscow":
            orderStates = webasyst_api.GetOrderStatesNames(orderNumber)
            num = orderStates.count(u"zhdet-otpravki")
            if num != 0:
                data["Number"] += str(num)

        data["SendCityCode"] = commonLibrary.GetCourierRequestParams(orderSender)["SendCityCode"]

        deliveryCost = int(webasyst_api.GetOrderDeliveryCost(orderNumber))
        if data["orderPaid"] == True:
            deliveryCost = 0
        data["DeliveryRecipientCost"] = str(deliveryCost)
        data["RecipientName"] = u"{} {}".format( customerData[1], customerData[0])
        data["RecipientPhone"] = customerData[2]

        deliveryData = webasyst_api.GetOrderDeliveryData(orderNumber)
        if len(deliveryData) == 3:
            city, shipping_id, shipping_name = deliveryData
        else:
            city, address, shipping_id, shipping_name = deliveryData
        data["shipping_name"] = shipping_name
        if shipping_name in [u"Курьером до двери", u"Курьером до двери (Курьер)"] or shipping_name.find(u"ПВЗ") != -1:
            bSdekPlugin = True
        else:
            bSdekPlugin = False

##        print shipping_name
        if shipping_name in [u"До двери", u"Курьером до двери", u"Курьером до двери (Курьер)"]:
            data["Tariff"] += "D"
            addressParts = address.split(",")
            data["RecStreet"] = addressParts[0].strip()
            data["RecHouse"] = addressParts[1].strip()
            data["RecFlat"] = addressParts[2].strip()
            if city.isdigit() == False:
                sdekCityCode = webasyst_api.GetSdekCityCode(city)
                if len(sdekCityCode) != 1:
                    message += "[Error] Нужно вручную установить код города по заказу {0}\n".format(orderNumber)
                    return -2
                else:
    ##                    prestashop_api.SetOrderAddressCity(orderId, sdekCityCode[0])
                    data["RecCityName"] = sdekCityCode[0]
            else:
                data["RecCityName"] = city
        else:
            data["Tariff"] += "S"
            data["RecCityName"] = city
            if "PvzCode" not in data.keys():
                if bSdekPlugin:
                    data["PvzCode"] = webasyst_api.GetOrderParamValue(orderNumber, "shipping_rate_id").split(":")[0]
                    data["RecPointName"] = shipping_name.split(":")[-1].strip(")").strip()
                else:
                    data["PvzCode"] = webasyst_api.GetPluginValue(shipping_id, "additional")
                    data["RecPointName"] = shipping_name.split("\"")[-1]

        data["Items"] = []
        orderItems = webasyst_api.GetOrderItems(orderNumber)
        if "Places" not in data.keys():#если нет разбивки по местам
            orderDiscount = webasyst_api.GetOrderDiscountSum(orderNumber)
            discountPerItem = int(orderDiscount / sum((x[3] for x in orderItems)))
            for orderItem in orderItems:
                product_id, sku_id, product_name, unit_price_tax_incl, product_quantity, purchase_supplier_price = orderItem
                item = {}
                item["WareKey"] = str(product_id)
                item["Comment"] = product_name
                item["Cost"] = str(purchase_supplier_price)
                if data["orderPaid"] == True:
                    item["Payment"] = "0"
                else:
                    if unit_price_tax_incl < discountPerItem:
                        return -3
                    item["Payment"] = str(unit_price_tax_incl - discountPerItem)
                item["Amount"] = str(product_quantity)
                on = webasyst_api.GetSkuOriginalName(sku_id)
                item["Name"] = on
                if orderSender == "SDEKMoscow":
                    item["Name"] = webasyst_api.GetSkuIdByItemName(on)
                    if item["Name"] == -1:
                        return -6

                data["Items"].append(item)
        else:#есть разбивка по местам
            data["Contractor"] = -1
            orderWarehouseCost = float(sum(i[-1]* i[4] for i in orderItems))
            orderCustomerCost = float(sum(i[3]* i[4] for i in orderItems))
            placesNum = len(data["Places"].split("/"))
            placeWarehouseCost = int (float(orderWarehouseCost)/float(placesNum))
            placeCustomerCost = int (float(orderCustomerCost)/float(placesNum))
            for i in range(1, placesNum + 1):
                item = {}
                item["WareKey"] = str(i)
                item["Comment"] = u"Техника"
                item["Cost"] = str(placeWarehouseCost)
                if data["orderPaid"] == True:
                    item["Payment"] = "0"
                else:
                    item["Payment"] = str(placeCustomerCost)
                item["Amount"] = "1"
                weight = data["Places"].split("/")[i-1].split("_")[0]
    ##                if tariffCode in [136, 62]:
                weight = int(weight) * 1000
                item["Weight"] = str(weight)
                item["Name"] = u"Техника"
                data["Items"].append(item)

    except:
        return -2

    return data

##############################################################################################################################
webasyst_api = WebasystAPI.WebasystAPI()
logfile = log.GetLogFileName(commonLibrary.basePath + "log", "sdek")
logging = log.Log(logfile)

testMode = False
##testMode = True

message = ""

sdekIP = SdekAPI.SdekAPI(testMode)
sdekNovall = SdekAPI.SdekAPI(testMode, "b9171b1ca91597383ab3043a218b6883", "de53f4a57568665605a76e21d9f100e1")

mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=200)

printFormsSititekIzhevsk = []
printFormsSititekMoscow = []
printFormsSititekSpb = []
printFormsLogos = []
printFormsCNT = []
printFormsATEK = []
printFormsTelesys = []
printFormsKvarts = []
printForms31Vek = []
printFormsPolikon = []
printFormsChiston = []
printFormsBereg = []
printFormsWell = []
printFormsUst = []
printFormsKvazar = []

ordersItemsSititekIzhevsk = ""
ordersItemsSititekMoscow = ""
ordersItemsSititekSpb = ""
ordersItemsLogos = ""
ordersItemsCNT = ""
ordersItemsATEK = ""
ordersItemsTelesys = ""
ordersItemsKvarts = ""
ordersItems31Vek = ""
ordersItemsPolikon = ""
ordersItemsBeevid = ""
ordersItemsChiston = ""
ordersItemsBereg = ""
ordersItemsWell = ""
ordersItemsUst = ""
ordersItemsKvazar = ""

labels = []
files = None

totalSuccess = True
wasDeliveries = False

for site in commonLibrary.wa_sitesParams:
    webasyst_api = commonLibrary.GetWebasystInstance(site)

    orders = webasyst_api.GetOrdersIds(states, startDate, endDate)
    bOrders = False

    for orderId in orders:
        data = {}
        orderNote = webasyst_api.GetOrderComment(orderId)
##        if commonLibrary.GetOrderLocked(orderNote):
##            continue

        result = GetOrderDataForSDEK(orderId, orderNote)
        if result == -1:
            continue

        if bOrders == False:
            message += ("\n=========  " + site["url"] + "  =========\n\n")
            bOrders = True

        if result == -2:
            additionalInfo = ""
            if data["shipping_name"] in [u"До двери", u"Курьером до двери", u"Курьером до двери (Курьер)"]:
                additionalInfo = ", ВОЗМОЖНО, НЕКОРРЕКНО УКАЗАН АДРЕС КУРЬЕРСКОЙ ДОСТАВКИ"
            message += "[Error] Ошибка при создании данных накладной по заказу " + str(orderId) + additionalInfo + "\n"
            totalSuccess = False
            continue
        if result == -6:
            message += "[Error] На складе не найден товар по заказу " + str(orderId) + "\n"
            SetDoNotSendNote(orderNote)
            totalSuccess = False
            continue
        if result == -3:
            message += "[Error] Невозможно применить скидку к заказу {0}\n".format(str(orderId))
            SetDoNotSendNote(orderNote)
            totalSuccess = False
            continue

        if data["bNovallOrder"] == False:
            sdek = sdekIP
        else:
            sdek = sdekNovall

        bSingleOrder = data["bSingleOrder"]
##        if bSingleOrder and machineName.find("hp") == -1 and machineName.find("pitekantrop") == -1:
##            continue
##        webasyst_api.SetOrderComment(orderId, orderNote + " locked")
        if bSingleOrder == False:
            if data["orderSender"] == "SDEKMoscow":
                number, deliveryCost = sdek.CreateOrderApi(data)
            else:
                number, deliveryCost = sdek.CreateOrder(data)
        else:
            number, deliveryCost, courierRequestNumber = sdek.CreateOrder(data)
##            tariff, number, courierRequestNumber = sdek.CreateInvoice(data)
            deliveryCost = 0

        wasDeliveries = True
        if number in [-1, None]:
            number = "ERROR"

        if number.find("ERR") == -1 and len(number) == 10:
            orderDeliveryCost = int(data["DeliveryRecipientCost"])
            if orderDeliveryCost != 0 and int(deliveryCost) > orderDeliveryCost:
                totalSuccess = False
                message += "[Error] Стоимость доставки по накладной {} ({} руб.) превышает стоимость, указанной в заказе {} ({} руб.)\n".format(number, deliveryCost, orderId, data["DeliveryRecipientCost"])

            if bSingleOrder == False:
                message += "По заказу {} ({}) создана накладная {}\n".format(orderId,data["orderSender"], number)
            else:
                message += "По заказу {} ({}) создана накладная {}, номер заявки на вызов курьера {}\n".format(orderId,data["orderSender"], number, courierRequestNumber)

            warningMessage = ""
            hasFragileItems = False

            if data["orderSender"] in ["SDEKMoscow", "SDEKSpb", "SDEKPushkin"]:
                if data["orderSender"] == "SDEKMoscow":
                    warehouseName = "warehouse_moscow"
                else:
                    warehouseName = "warehouse_spb"

                #уменьшаем количество на складе
                for item in data["Items"]:
                    if data["orderSender"] == "SDEKMoscow":
                        productOriginalName = webasyst_api.GetItemNameBySkuId(item["Name"])
                    else:
                        productOriginalName = item["Name"]
                    if productOriginalName in commonLibrary.fragileItems or productOriginalName.find(u"лампа") != -1 or productOriginalName.find(u"ветерок") != -1:
                        hasFragileItems = True

                    try:
                        webasyst_api.DecreaseWarehouseProductQuantity(warehouseName, productOriginalName, int(item["Amount"]))
                        productQty = webasyst_api.GetWarehouseProductQuantity(warehouseName, productOriginalName)
                        message += "Остаток товара '{0}' на складе {1} - {2} шт.\n".format(productOriginalName.encode("utf-8"), warehouseName, str(productQty))
                    except:
                        mails.SendInfoMail("Списание товаров вручную", "'" + productOriginalName.encode("utf-8") + "' - " + str(item["Amount"]) + " шт.\n")

                #отправка уведомления о хрупком товаре
                if data["orderSender"] == "SDEKMoscow":
                    if hasFragileItems == True:
                        result = mails.SendMailToPartner("support@cowms.ru", "Накладная " + str(number), "\nПросим доупаковать груз по данной накладной и отправить его с пометкой 'Хрупко'")

                        if result != True:
                            message += "[Error] Ошибка при отправке уведомления о хрупком товаре " + result + "\n"
                            totalSuccess = False
                        else:
                            message += "Отправлено уведомление о хрупком товаре по накладной {0}\n".format(number)

            #печатная форма
            if data["orderSender"] not in ["SDEKMoscow", "SDEKSpb", "SDEKPushkin"]:
                printFormPath = sdek.GetPrintForm(number)
                if printFormPath == -1 or printFormPath.find("ERR") != -1 :
##                    message += "[Error] Ошибка при создании печатной формы по заказу " + str(orderId) + "\n"
##                    totalSuccess = False
                    pass
                else:
                    message += "Создана печатная форма по заказу " + str(orderId) + "\n"
                    if data["orderSender"] == "SititekMoscow":
                        printFormsSititekMoscow.append(printFormPath)
                    if data["orderSender"] == "SititekIzhevsk":
                        printFormsSititekIzhevsk.append(printFormPath)
                    if data["orderSender"] == "SititekSpb":
                        printFormsSititekSpb.append(printFormPath)
                    if data["orderSender"] == "Logos":
                        printFormsLogos.append(printFormPath)
                    if data["orderSender"] == "CNT":
                        printFormsCNT.append(printFormPath)
                    if data["orderSender"] == "ATEK":
                        printFormsATEK.append(printFormPath)
                    if data["orderSender"] == "Telesys":
                        printFormsTelesys.append(printFormPath)
                    if data["orderSender"] == "Kvarts":
                        printFormsKvarts.append(printFormPath)
                    if data["orderSender"] == "31Vek":
                        printForms31Vek.append(printFormPath)
                    if data["orderSender"] == "Polikon":
                        printFormsPolikon.append(printFormPath)
                    if data["orderSender"] == "Chiston":
                        printFormsChiston.append(printFormPath)
                    if data["orderSender"] == "Bereg":
                        printFormsBereg.append(printFormPath)
                    if data["orderSender"] == "Well":
                        printFormsWell.append(printFormPath)
                    if data["orderSender"] == "Ust":
                        printFormsUst.append(printFormPath)
                    if data["orderSender"] == "Kvazar":
                        printFormsKvazar.append(printFormPath)

            if data["orderSender"] not in ["SDEKMoscow", "SDEKSpb", "SDEKPushkin"]:
                #добавляем товары в список
                items = []
                for item in data["Items"]:
                    entry = []
                    productOriginalName = item["Name"]
                    entry.append(productOriginalName)
                    entry.append(item["Amount"])
                    items.append(entry)

                shopname = data["RecCityName"]
                if data["Tariff"][1] == "D":
                    if shopname[0].isdigit() == True:
                        shopname =  webasyst_api.GetCityBySdekCode(shopname)

                if data["orderSender"] == "SititekMoscow":
                    for item in items:
                        ordersItemsSititekMoscow += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsSititekMoscow = ordersItemsSititekMoscow.strip(", ")
                    ordersItemsSititekMoscow += u" - {0}\n".format(shopname)

                if data["orderSender"] == "SititekIzhevsk":
                    for item in items:
                        ordersItemsSititekIzhevsk += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsSititekIzhevsk = ordersItemsSititekIzhevsk.strip(", ")
                    ordersItemsSititekIzhevsk += u" - {0}\n".format(shopname)

                if data["orderSender"] == "SititekSpb":
                    for item in items:
                        ordersItemsSititekSpb += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsSititekSpb = ordersItemsSititekSpb.strip(", ")
                    ordersItemsSititekSpb += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Logos":
                    for item in items:
                        ordersItemsLogos += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsLogos = ordersItemsLogos.strip(", ")
                    ordersItemsLogos += u" - {0}\n".format(shopname)

                if data["orderSender"] == "CNT":
                    for item in items:
                        ordersItemsCNT += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsCNT = ordersItemsCNT.strip(", ")
                    ordersItemsCNT += u" - {0}\n".format(shopname)

                if data["orderSender"] == "ATEK":
                    for item in items:
                        ordersItemsATEK += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsATEK = ordersItemsATEK.strip(", ")
                    ordersItemsATEK += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Telesys":
                    for item in items:
                        ordersItemsTelesys += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsTelesys = ordersItemsTelesys.strip(", ")
                    ordersItemsTelesys += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Kvarts":
                    for item in items:
                        ordersItemsKvarts += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsKvarts = ordersItemsKvarts.strip(", ")
                    ordersItemsKvarts += u" - {0}\n".format(shopname)

                if data["orderSender"] == "31Vek":
                    for item in items:
                        ordersItems31Vek += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItems31Vek = ordersItems31Vek.strip(", ")
                    ordersItems31Vek += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Polikon":
                    for item in items:
                        ordersItemsPolikon += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsPolikon = ordersItemsPolikon.strip(", ")
                    ordersItemsPolikon += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Chiston":
                    for item in items:
                        ordersItemsChiston += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsChiston = ordersItemsChiston.strip(", ")
                    ordersItemsChiston += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Bereg":
                    for item in items:
                        ordersItemsBereg += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsBereg = ordersItemsBereg.strip(", ")
                    ordersItemsBereg += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Well":
                    for item in items:
                        ordersItemsWell += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsWell = ordersItemsWell.strip(", ")
                    ordersItemsWell += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Ust":
                    for item in items:
                        ordersItemsUst += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsUst = ordersItemsUst.strip(", ")
                    ordersItemsUst += u" - {0}\n".format(shopname)

                if data["orderSender"] == "Kvazar":
                    for item in items:
                        ordersItemsKvazar += item[0] + u" ({0} шт.), ".format(item[1])
                    ordersItemsKvazar = ordersItemsKvazar.strip(", ")
                    ordersItemsKvazar += u" - {0}\n".format(shopname)

            webasyst_api.ApplyActionToOrder(orderId, u"gotov-k-otpravke")
            webasyst_api.SetOrderTrackingNumber(orderId, number)

            #удаляем данные об отправке из "Служебных записок"
            orderNote = orderNote.replace(" locked","")
            webasyst_api.SetOrderComment(orderId, orderNote)

        else:
            errorDescr = number
            if number == "ERR_RECCITYCODE":
                errorDescr = u"Код города получателя отсутствует в базе СДЭК"
            if number == "ERR_PVZCODE":
                errorDescr = u"Код ПВЗ отсутствует в базе СДЭК"
            message += "[Error] Ошибка при создании накладной по заказу {0}: '{1}'\n".format(str(orderId), errorDescr)
            SetDoNotSendNote(orderNote)
            totalSuccess = False

    ordersIds = webasyst_api.GetOrdersIds([u"Ждет отправки"], startDate, endDate)
    labels += [commonLibrary.GetOrderSender(webasyst_api.GetOrderComment(orderId)) for orderId in ordersIds]

##    if machineName.find("hp") != -1 or machineName.find("pitekantrop") != -1 :
##        ssh.kill()


################################################################################
webasyst_api = WebasystAPI.WebasystAPI()
##sdek = SdekAPI.SdekAPI(testMode, "iix55tq8ldhr9fysxg2380l84hpzxsy4", "ddt6w6jfkps7tdzvljq52v2x3ulbexre")
sdek = SdekAPI.SdekAPI(testMode)
date = datetime.date.today() - datetime.timedelta(days=5)
deliveries = commonLibrary.GetDeliveriesByDate(date)
paidContractors = [x[1] for x in commonLibrary.GetRequests() if x[4] == 1]
for delivery in deliveries:
    id, date, contractor, recipientCity, asIM, items, places, invoice, postingid, currentState = delivery
    if currentState not in [None, u"Создан", ""]:
        continue
    if invoice not in [None, ""]:
        continue
    contractor = contractor.strip()
    if contractor not in paidContractors and contractor not in ["SDEKMoscow", "SDEKSpb", "SDEKPushkin"]:
        continue

    #проверка мест
    _places = [x.strip() for x in places.split("/")]
    lens = [len(x.split("_")) for place in _places]
    if lens.count(4) != len(lens):
        totalSuccess = False
        message += "Ошибка указания мест по доставке '{0}'\n".format(id)
        continue
    #проверка товаров
    try:
        itemsNames, itemsQuantities = commonLibrary.ParseItems(items)
    except:
        totalSuccess = False
        message += "Ошибка парсинга товаров по доставке '{0}'\n".format(id)
        continue

    if recipientCity.lower() == u"москва":
        deliveryItemsList = u"\nв Москву:\n"
    else:
        deliveryItemsList = u"\nв Санкт-Петербург:\n"

    #стоимость товаров
    cost = 0
    for i in range(len(itemsNames)):
        price = webasyst_api.GetProductWholesalePrice(itemsNames[i])
        if price == -1:
            totalSuccess = False
            message += "Не найдена стоимость товара '{0}'\n".format(itemsNames[i].encode("utf-8"))
            i = 0
            break
        deliveryItemsList += u"{0} - {1} шт.\n".format(itemsNames[i], itemsQuantities[i])
        cost += price * int(itemsQuantities[i])

    if i < len(itemsNames) - 1 or cost == 0:#не найдена стоимость какого-то товара
        continue

    contractorDeliveriesNumber = len([x[0] for x in deliveries if x[2] == contractor and x[9] in [None, "", u"Создан"]])
    if contractor in labels:
        contractorDeliveriesNumber += 1

    data = commonLibrary.GetCourierRequestParams(contractor)
    data["Contractor"] = contractor
    if recipientCity.lower() == u"москва":
        data["RecipientName"] = u"Фулфилмент"
        data["RecPVZName"] = u"MSK123"
        data["RecipientPhone"] = "88002500405"
        data["RecCityCode"] = "44"
        data["RecCityName"] = u"Москва"
        data["RecipientCompany"] = u"СДЭК"
        data["ClientSide"] = "other"

    else:
        data["RecipientName"] = u"Усов Иван"
        data["RecPVZName"] = u"SPB156"
        data["RecipientPhone"] = "89697333555"
        data["RecCityCode"] = "137"
        data["RecCityName"] = "137"
        data["RecipientCompany"] = u"Усов Иван Викторович"
        data["ClientSide"] = "receiver"
##        data["RecipientName"] = u"Усов Иван"
##        data["RecPVZName"] = u"SVT18"
##        data["RecipientPhone"] = "89697333555"
##        data["RecCityCode"] = "15256"
##        data["RecCityName"] = "15256"
##        data["RecipientCompany"] = u"Усов Иван Викторович"
##        data["ClientSide"] = "receiver"

    data["Places"] = places
    data["itemsCost"] = cost
    data["orderPaid"] = False

    data["Number"] = datetime.date.today().strftime('%d%m') + str(id)

    if asIM:
        if data["NeedPickup"] == False:
            if contractor == "Bereg":
                tariffCode = 62
            else:
                tariffCode = 136
##                tariffCode = 234
        elif contractorDeliveriesNumber > 1:
            tariffCode = 136
        else:
            tariffCode = 138
        data["TariffId"] = tariffCode
        data["Items"] = []
        placesNum = len(places.split("/"))
        placeCost = int (float(cost)/float(placesNum))
        cost = placeCost * placesNum
        if contractor != "SDEKMoscow":
            for i in range(1, placesNum + 1):
                item = {}
                item["WareKey"] = str(i)
                item["Comment"] = u"Техника"
                item["Cost"] = str(placeCost)
                item["Payment"] = "0"
                item["Amount"] = "1"
                weight = places.split("/")[i-1].split("_")[0]
##                if tariffCode in [136, 62]:
##                    weight = int(weight) * 1000
                item["Weight"] = str(weight)
                data["Items"].append(item)
        else:
            data["Tariff"] = "SS"
            data["PvzCode"] = data["RecPVZName"]
            for i in range(len(itemsNames)):
                price = webasyst_api.GetProductWholesalePrice(itemsNames[i])
                item = {}
                item["WareKey"] = str(i)
                item["Name"] = webasyst_api.GetSkuIdByItemName(itemsNames[i])
                item["Cost"] = str(price)
                item["Payment"] = "0"
                item["Amount"] = str(itemsQuantities[i])
                item["Weight"] = "300"
                data["Items"].append(item)
        data["DeliveryRecipientCost"] = "0"
    else:
        data["TariffId"] = commonLibrary.GetSdekTariffId( commonLibrary.GetTariff(contractorDeliveriesNumber, recipientCity, contractor))
        if data["TariffId"] == -1:
            message += "Ошибка выбора тарифа по доставке '{0}'\n".format(id)
            totalSuccess = False
            continue

        tariffCode = data["TariffId"]

    wasDeliveries = True

    if contractor == "SDEKMoscow":
        invoice, deliveryCost = sdek.CreateOrderApi(data)
    else:
        if data["NeedPickup"] == True and contractorDeliveriesNumber == 1:
##            returnData = sdek.CreateInvoice(data)
            returnData = sdek.CreateOrder(data)
            invoice, tariff, courierRequestNumber = returnData
            deliveryCost = 0
        else:
            returnData = sdek.CreateOrder(data)
            invoice, deliveryCost = returnData

    if invoice == -1 or invoice.lower().find("err") != -1:
        message += "[Error] Ошибка {1} при создании накладной по доставке {0}\n".format(id, invoice)
        totalSuccess = False
        continue

    if contractor in ["SDEKMoscow", "SDEKSpb", "SDEKPushkin"]:
        if contractor == "SDEKMoscow":
            warehouseName = "warehouse_moscow"
        else:
            warehouseName = "warehouse_spb"
        #уменьшаем количество на складе
        for i in range(len(itemsNames)):
            itemName = itemsNames[i]
            itemAmount = itemsQuantities[i]
            webasyst_api.DecreaseWarehouseProductQuantity(warehouseName, itemName, int(itemAmount))
            productQty = webasyst_api.GetWarehouseProductQuantity(warehouseName, itemName)
            message += "Остаток товара '{0}' на складе {1} - {2} шт.\n".format(itemName.encode("utf-8"), warehouseName, str(productQty))

    webasyst_api.SetDeliveryInvoice(id, invoice)
    message += "Создана накладная {0} по доставке {1}\n".format(invoice, id)
    if data["NeedPickup"] == True and contractorDeliveriesNumber == 1:
        message += ". Номер заявки на вызов курьера {0}\n".format(courierRequestNumber)
    printFormPath = sdek.GetPrintForm(invoice)
    if printFormPath.find("ERR") != -1:
        message += "[Error] Ошибка при создании печатной формы по доставке " + str(id) + "\n"
        printFormPath = "error"
##            totalSuccess = False
    else:
        message += "Создана печатная форма по доставке " + str(id) + "\n"
        files = [printFormPath]

    if contractor == "SititekMoscow":
        printFormsSititekMoscow.append(printFormPath)
        ordersItemsSititekMoscow += deliveryItemsList
    if contractor == "SititekIzhevsk":
        printFormsSititekIzhevsk.append(printFormPath)
        ordersItems += deliveryItemsList
    if contractor == "Logos":
        printFormsLogos.append(printFormPath)
        ordersItemsLogos += deliveryItemsList
    if contractor == "CNT":
        printFormsCNT.append(printFormPath)
        ordersItemsCNT += deliveryItemsList
    if contractor == "ATEK":
        printFormsATEK.append(printFormPath)
        ordersItemsATEK += deliveryItemsList
    if contractor == "Telesys":
        printFormsTelesys.append(printFormPath)
        ordersItemsTelesys += deliveryItemsList
    if contractor == "Kvarts":
        printFormsKvarts.append(printFormPath)
        ordersItemsKvarts += deliveryItemsList
    if contractor == "31Vek":
        printForms31Vek.append(printFormPath)
        ordersItems31Vek += deliveryItemsList
    if contractor == "Polikon":
        printFormsPolikon.append(printFormPath)
        ordersItemsPolikon += deliveryItemsList
    if contractor == "Chiston":
        printFormsChiston.append(printFormPath)
        ordersItemsChiston += deliveryItemsList
    if contractor == "Bereg":
        printFormsBereg.append(printFormPath)
        ordersItemsBereg += deliveryItemsList
    if contractor == "Well":
        printFormsWell.append(printFormPath)
        ordersItemsWell += deliveryItemsList
    if contractor == "Ust":
        printFormsUst.append(printFormPath)
        ordersItemsUst += deliveryItemsList
    if contractor == "Kvazar":
        printFormsKvazar.append(printFormPath)
        ordersItemsKvazar += deliveryItemsList

################################################################################

lettersInfos = []

if testMode == False:
    if len(printFormsSititekIzhevsk) != 0:
        info = ["opt@sititek.ru", u"Балтачеву Рустаму" ,printFormsSititekIzhevsk, ordersItemsSititekIzhevsk]
        lettersInfos.append(info)

    if len(printFormsSititekMoscow) != 0:
        info = ["opt@sititek.ru", u"Балтачеву Рустаму" ,printFormsSititekMoscow, ordersItemsSititekMoscow]
        lettersInfos.append(info)

    if len(printFormsSititekSpb) != 0:
        info = ["na@spb812.com", u"Нагибину Александру" ,printFormsSititekSpb, ordersItemsSititekSpb]
        lettersInfos.append(info)

    if len(printFormsLogos) != 0:
        info = ["100@455.ru", u"Викулину Александру" ,printFormsLogos, ordersItemsLogos]
        lettersInfos.append(info)

    if len(printFormsCNT) != 0:
        info = ["cnt-tlt@yandex.ru", u"Пановой Наталье" ,printFormsCNT, ordersItemsCNT]
        lettersInfos.append(info)

    if len(printFormsATEK) != 0:
        info = ["atekmks@mail.ru", u"Воронкову Виталию" ,printFormsATEK, ordersItemsATEK]
        lettersInfos.append(info)

    if len(printFormsTelesys) != 0:
        info = ["sales4@telesys.ru", u"Маздыкову Аркадию" ,printFormsTelesys, ordersItemsTelesys]
        lettersInfos.append(info)

    if len(printFormsKvarts) != 0:
        info = ["aleksei@konnektim.ru", u"Шигорину Алексею" ,printFormsKvarts, ordersItemsKvarts]
        lettersInfos.append(info)

    if len(printForms31Vek) != 0:
        info = ["men10@a-el.ru", u"Горбуновой Эльвире" ,printForms31Vek, ordersItems31Vek]
        lettersInfos.append(info)

    if len(printFormsPolikon) != 0:
        info = ["9917959@mail.ru", u"Елене" ,printFormsPolikon, ordersItemsPolikon]
        lettersInfos.append(info)

    if len(printFormsChiston) != 0:
        info = ["lilia555@mail.ru", u"Лилии" ,printFormsChiston, ordersItemsChiston]
        lettersInfos.append(info)

    if len(printFormsBereg) != 0:
        info = ["soltterra.com@mail.ru", u"Комлевой Ольге" ,printFormsBereg, ordersItemsBereg]
        lettersInfos.append(info)

    if len(printFormsWell) != 0:
        info = ["manager1@well-we.ru", u"Александру",printFormsWell, ordersItemsWell]
        lettersInfos.append(info)

    if len(printFormsUst) != 0:
        info = ["otpugivatel-yust@yandex.ru", u"Людмиле",printFormsUst, ordersItemsUst]
        lettersInfos.append(info)

    if len(printFormsKvazar) != 0:
        info = ["ab@birdrepeller.ru", u"Алексею",printFormsKvazar, ordersItemsKvazar]
        lettersInfos.append(info)

    for letterInfo in lettersInfos:
        email = letterInfo[0]
        contactPerson = letterInfo[1]
        invoices = letterInfo[2]
        while invoices.count("error") != 0:
            invoices.remove("error")
        orderItems = letterInfo[3]

        email = "feedback@knowall.ru.com"

        if totalSuccess == False:
            email = "feedback@knowall.ru.com"

        messageToContractor = "\nВо вложении накладные для отгрузки:\n\n"
        messageToContractor += orderItems.encode("utf-8")
        messageToContractor += "\n\nС уважением,\nИван"

        result = mails.SendMailToPartner(email, "Накладные для отгрузки", messageToContractor, invoices)
        if result != True:
            message += "[Error] Ошибка при отправке письма {0}: {1}\n".format(contactPerson.encode("utf-8"), str(result))
        else:
            message += "Отправлено письмо {0}\n".format(contactPerson.encode("utf-8"))

################################################################################



theme = "Отчет по отправкам СДЭК"
if totalSuccess == False:
    theme += " (есть ошибки!)"

if (wasDeliveries == True or totalSuccess == False):
    result = mails.SendInfoMail(theme, message, files)


