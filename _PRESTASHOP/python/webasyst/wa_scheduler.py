﻿import time
import datetime
import subprocess
import commonLibrary

scriptsPath = commonLibrary.wa_basePath

nowMinute = 59

while True:
    now = datetime.datetime.now()
    if now.minute == nowMinute:
        time.sleep(30)
        now = datetime.datetime.now()

    nowMinute = now.minute

################################################################################

    if now.minute % 2 == 0:
        try:
            daemon = subprocess.Popen(["python", "wa_processOrganizationsOrders.py"])
            daemon.wait()
        except:
            daemon.kill()

    if now.hour in [9, 12, 16, 20] and now.minute == 2:
        subprocess.Popen(["python", "wa_processOrdersStates.py"])

    if now.hour in [23] and now.minute == 2:
        subprocess.Popen(["python", "..\..\..\_SELENIUM\\webasyst\\reindexYandex.py"])

    time.sleep(55)