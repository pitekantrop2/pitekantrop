﻿# -*- coding: utf-8 -*-
import log
import datetime
import sys
import WebasystAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import SMSAPI
import psutil
import commonLibrary

states = [
u"Ждет отправки"
]

def GetPvz():
    if label in ["SititekMoscow", "Logos", "Polikon", "31Vek", "Almaz", "Kvazar"]:
        return u"MSK370"
    if label == "Telesys":
        return u"ZLN3"
    if label == "CNT":
        return u"TLT12"
    if label == "ATEK":
        return u"LBN1"
    if label == "Well":
        return u"KHMK6"
    if label in ["SititekSpb", "Kvarts", "Palmira"]:
        return u"SPB4"
    if label == "SititekIzhevsk":
        return u"IZHV3"

test = False
##test = True

logfile = log.GetLogFileName("log", "callCouriers")
logging = log.Log(logfile)

mails = SendMails.SendMails()
sdek = SdekAPI.SdekAPI(test)
machineName = commonLibrary.GetMachineName()
paidContractors = [x[1] for x in commonLibrary.GetRequests() if x[4] == 1]

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=190)
webasyst_api = WebasystAPI.WebasystAPI()

wasCouriersCalls = False
bErrors = False
message = ""

today = datetime.date.today()
now = datetime.datetime.now()

labels = {}

for site in commonLibrary.wa_sitesParams:
    webasyst_api = commonLibrary.GetWebasystInstance(site)

    orders = webasyst_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:
        orderId = order[0]
##        if orderId != 160:
##            continue
        orderNote = webasyst_api.GetOrderComment(orderId)
        invoiceNumber = webasyst_api.GetOrderTrackingNumber(orderId)

        if commonLibrary.GetOrderIsSingle(orderNote) == True:
            continue

        label = commonLibrary.GetOrderSender(orderNote)

        if label != -1:
            if label not in labels.keys():
                labels[label] = []

            labels[label].append(invoiceNumber)

#получаем список доставок
date = datetime.date.today() - datetime.timedelta(days=10)
deliveries = webasyst_api.GetDeliveriesByDate(date)
for delivery in deliveries:
    id, date, contractor, recipientCity, asIM, items, places, invoice, postingid, currentState = delivery
    if contractor in [u"Chiston"]:
        continue
    if currentState not in [None, u"Создан", ""]:
        continue
    if contractor in labels.keys():
        contractorOrders = len(labels[contractor])
    else:
        contractorOrders = 0
    contractorDeliveriesNumber = len([x[0] for x in deliveries if x[2] == contractor and x[9] in [None, "", u"Создан"]]) + contractorOrders
    tariff = commonLibrary.GetTariff(contractorDeliveriesNumber, recipientCity, contractor)
    if tariff == -1 or tariff.find(u"дверь") != -1:
        continue
    if contractor not in labels.keys():
        labels[contractor] = []
    labels[contractor].append(invoice)

printForms = []

for label in labels.keys():
    if label in [
"SDEKMoscow",
"Chiston",
"Korshun",
"Bereg",
##"CNT",
"Spectr",
"Ust",
"Polikon",
"Alex",
"SDEKSpb"]:continue

##    if label not in paidContractors:
##        continue
##
##    if label in ["SititekIzhevsk", "CNT"] and now.hour not in [13]:
##        continue
##
##    if label not in ["SititekIzhevsk", "CNT"] and now.hour not in [14]:
##        continue

    wasCouriersCalls = True

    data = {}
    courierRequestParams = commonLibrary.GetCourierRequestParams(label)
    if courierRequestParams == -1:
        continue

    data["SendCityCode"] = courierRequestParams["SendCityCode"]
    data["SendStreet"] = courierRequestParams["SendStreet"]
    data["SendHouse"] = courierRequestParams["SendHouse"]
    data["SendFlat"] = courierRequestParams["SendFlat"]
    data["SendPhone"] = courierRequestParams["SendPhone"]
    data["SenderName"] = courierRequestParams["SenderName"]
    data["CourierDate"] = courierRequestParams["CourierDate"]
    data["CourierTimeBeg"] = courierRequestParams["CourierTimeBeg"]
    data["CourierTimeEnd"] = courierRequestParams["CourierTimeEnd"]

    data["SendComment"] = ", ".join(labels[label])
    data["Places"] = ""
    for i in range(0, len(labels[label])):
        data["Places"] += "1_10_10_10/"

    data["Places"] = data["Places"].strip("/")

    if label in ["31Vek"]:
##        continue
##        pass
        callCourierNumber = sdek.CallCourier(data)
    else:
##        data["Places"] = "5_20_20_20"
        data["RecCityName"] = data["SendCityCode"]
        data["Tariff"] = u"Забор груза дверь-склад"
        data["itemsCost"] = 0
        data["RecipientName"] = u"Расконсолидация"
        data["RecipientPhone"] = u"88002500405"
        data["RecPVZName"] = GetPvz()
##        try:
        res = sdek.CreateInvoice(data)
##        except:
##            res = -1
        if res == -1:
            callCourierNumber = "ERROR"
        else:
            callCourierNumber = res[2]
    if callCourierNumber.find("ERR") != -1 or len(callCourierNumber) != 8:
        if callCourierNumber.find("ERR_CALLCOURIER_DATE_EXISTS") != -1:
            logging.LogInfoMessage(u"Заявка на вызов курьера в {0} уже существует".format(label))
            message += "Заявка на вызов курьера в {0} уже существует\n".format(label)
        else:
            bErrors = True
            wasCouriersCalls = True
            logging.LogErrorMessage(u"Ошибка при создании заявки на вызов курьера ({0}): {1}".format(label, callCourierNumber))
            message += "[Error] Ошибка при создании заявки на вызов курьера ({0}): {1}\n".format(label, callCourierNumber.encode("utf-8"))
    else:
        logging.LogInfoMessage(u"Создана заявка на вызов курьера ({0}): {1}".format(label, callCourierNumber))
        message += "Создана заявка на вызов курьера ({0}): {1}\n".format(label, callCourierNumber.encode("utf-8"))
        if "res" in locals():
            dispatchNumber = res[1]
            printFormPath = sdek.GetPrintForm(dispatchNumber)
            if printFormPath == -1 or printFormPath.find("ERR") != -1:
                try:
                    logging.LogErrorMessage(u"Ошибка при создании печатной формы по заявке на вызов курьера {0}: {1}".format(dispatchNumber, printFormPath))
                    message += "Ошибка при создании печатной формы по заявке на вызов курьера {0}\n".format(dispatchNumber )
                except:
                    pass
            else:
                printForms.append(printFormPath)

if wasCouriersCalls == True:
    theme = "Отчет по заявкам на вызов курьера"

    if bErrors == True:
        theme += " (есть ошибки!)"

    result = mails.SendInfoMail(theme, message, printForms)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))


time.sleep(30)
