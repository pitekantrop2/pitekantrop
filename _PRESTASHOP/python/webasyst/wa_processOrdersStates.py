﻿# -*- coding: utf-8 -*-
import log
import datetime
import sys
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import SMSAPI
import psutil
import commonLibrary
import WebasystAPI

states = [
u"Ждет отправки",
u"Отправлен",
u"Доставлен в почтовое отделение",
u"Доставлен в пункт выдачи",
u"Доставлен в город-получатель",
u"Вручен"
]

def decreaseWarehouseProductQuantity(orderId, warehouseName):
    global message

    orderItems = webasyst_api.GetOrderItems(orderId)
    for orderItem in orderItems:
        product_id, sku_id, product_name, unit_price_tax_incl, product_quantity, purchase_supplier_price = orderItem
        productOriginalName = webasyst_api.GetSkuOriginalName(sku_id)
        try:
            webasyst_api.DecreaseWarehouseProductQuantity(warehouseName, productOriginalName, int(product_quantity))
            productQty = webasyst_api.GetWarehouseProductQuantity(warehouseName, productOriginalName)
            message += "Остаток товара '{0}' на складе {1} - {2} шт.\n".format(productOriginalName.encode("utf-8"), warehouseName, str(productQty))
        except:
            mails.SendInfoMail("Списание товаров вручную", "'" + productOriginalName.encode("utf-8") + "' - " + str(orderItem[3]) + " шт.\n")


def SendSMSToCustomer(site, orderId, mode, trackingNumber):
    global message

    if mode not in ["shipped", "reminder", "in_transit"]:
##    if mode not in [ "reminder", "in_transit"]:
        return

    if now.hour < 9 or now.hour > 14:
        return

    smsMessage = u"Здравствуйте! Ваш заказ"
    companyName = u"«{0}»".format(commonLibrary.GetCompanyName(site["url"]))

    firstname, lastname, phone, email, id = webasyst_api.GetOrderCustomerData(orderId)
    try:
        deliveryData = webasyst_api.GetOrderDeliveryData(orderId)
        if len(deliveryData) == 3:
            city, shipping_id, shipping_name = deliveryData
        else:
            city, address, shipping_id, shipping_name = deliveryData
    except:
        if bOrders == False:
            message += "\n=========  " + webasyst_api.siteUrl + "  =========\n\n"
        message += "[Error] Ошибка получения данных доставки по заказу {}\n".format(orderId)
        return

    if shipping_name.find(u"(") != -1:
        bSdekPlugin = True
    else:
        bSdekPlugin = False

    if mode == "in_transit":
        smsMessage += u" уже в пути"
    else:
        smsMessage += u" доставлен"

    if shipping_name.find(u"Почта") != -1:
        if mode == "in_transit":
            smsMessage += u", трекинг-код для отслеживания посылки {}.".format(trackingNumber)
        else:
            smsMessage += u" в почтовое отделение. "
    elif bCourierDelivery:
        if mode == "in_transit":
            smsMessage += u", номер накладной {}.".format(trackingNumber)
        else:
            return
    else:
        comment = webasyst_api.GetOrderComment(orderId)
        pvzCode = commonLibrary.GetPvzCode(comment)
        if pvzCode == -1:
            if bSdekPlugin:
                pvzCode = webasyst_api.GetOrderParamValue(orderId, "shipping_rate_id").split(":")[0]
            else:
                pvzCode = webasyst_api.GetPluginValue(shipping_id, "additional")
        pointDetails = sdek.GetPVZDetails("", pvzCode)
        if pointDetails != -1:
            pointName = pointDetails[1]
        if mode == "in_transit":
            smsMessage += u", номер накладной {}.".format(trackingNumber)
        else:
            if pointDetails == -1:
                message += "[Error] Не найден пункт выдачи {0}, заказ {1}\n".format(pointName.encode("utf-8"), orderId)
                smsMessage += u" в пункт выдачи."
            else:
                smsMessage += u" в пункт выдачи '{}', адрес: {}, телефоны: {}, режим работы: {}.".format(pointName, pointDetails[5], pointDetails[6], pointDetails[4] )

    smsMessage += u" Интернет-магазин {0}.".format(companyName)
    smsMessage = smsMessage.encode("cp1251")

    result = -1
    lastSMSDate = webasyst_api.GetOrderSMSDate(orderId, mode)
    if mode in ["shipped", "in_transit"]:
        if lastSMSDate == -1:
            result = sms.SendSMS(phone, smsMessage)
        else:
            return
    if mode == "reminder":
        if lastSMSDate == -1:
            shippedSMSDate = webasyst_api.GetOrderSMSDate(orderId, "shipped")
            if shippedSMSDate == -1:
                result = sms.SendSMS(phone, smsMessage)
            else:
                pastDays = (datetime.date.today() - shippedSMSDate.date()).days
                if pastDays > 3:
                    result = sms.SendSMS(phone, smsMessage)
                else:
                    return
        else:
            pastDays = (datetime.date.today() - lastSMSDate.date()).days
            if pastDays > 3:
                result = sms.SendSMS(phone, smsMessage)
            else:
                return

    result = result.replace("\n", "")

    if result.find("100") == -1:
        message += "[Error] Ошибка {0} при отправке sms клиенту по заказу <a target='blank_' href='{1}'>{2}</a>\n".format(result, boUrl, orderId)
    else:
        if test == False:
            webasyst_api.SetOrderSMSDate(orderId, mode, datetime.date.today())

#############################################################################################

now = datetime.datetime.now()
today = datetime.date.today()

test = False
##test = True

logfile = log.GetLogFileName(commonLibrary.basePath + "log", "processOrders")
logging = log.Log(logfile)

machineName = commonLibrary.GetMachineName()
sms = SMSAPI.SMSAPI()
webasyst_api = WebasystAPI.WebasystAPI()

sdekIP = SdekAPI.SdekAPI()
sdekNovall = SdekAPI.SdekAPI(False, "b9171b1ca91597383ab3043a218b6883", "de53f4a57568665605a76e21d9f100e1")
##sdek = sdekIP

rp = RussianPostAPI.RussianPostAPI()
mails = SendMails.SendMails()
notShippedOrders = {}
notDeliveredCouriersOrders = {}

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=180)

message = ""

for site in commonLibrary.wa_sitesParams:
    webasyst_api = commonLibrary.GetWebasystInstance(site)

    notShippedOrders[site["url"]] = []
    notDeliveredCouriersOrders[site["url"]] = []
    orders = webasyst_api.GetOrdersIds(states, startDate, endDate)
    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]
##    orders = prestashop_api.GetOrdersIds(states, startDate, endDate, True)

    bOrders = False

    for orderId in orders:
##        if orderId != 3184:
##            continue
        # print orderId
        mode = None
        orderState = webasyst_api.GetOrderStateName(orderId)
        orderNote = webasyst_api.GetOrderComment(orderId)
        bNovallOrder = commonLibrary.GetNovallOrder(orderNote)
        orderPaid = webasyst_api.IsOrderPaid(orderId)
        deliveryData = webasyst_api.GetOrderDeliveryData(orderId)
        if len(deliveryData) == 0:
            continue
        shipping_name = deliveryData[-1]
        if shipping_name.find(u"Почта") == -1:
            if shipping_name.find(u"выдачи") != -1 or shipping_name.find(u"ПВЗ") != -1:
                bCourierDelivery = False
            else:
                bCourierDelivery = True
        customerData = webasyst_api.GetOrderCustomerData(orderId)
        orderCity =  webasyst_api.GetOrderShippingCity(orderId).strip()
        boUrl = "{}?action=orders#/order/{}/".format(backofficeUrl, orderId)

        trackingNumber = webasyst_api.GetOrderTrackingNumber(orderId)
        if orderState == u"Вручен":
            if orderCity in [u"Спб", u"Санкт-Петербург"] and bCourierDelivery and shipping_name.find(u"Почта") == -1:
                if trackingNumber in [None, "", -1]:#доставка нами
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    webasyst_api.ApplyActionToOrder(orderId, u"complete")
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен и оплачен'\n".format(boUrl, orderId)

                    #уменьшаем количество на складе
                    decreaseWarehouseProductQuantity(orderId, "warehouse_spb")
                continue

        if orderState == u"Вручен" and orderPaid == True:
            if bOrders == False:
                message += ("\n=========  " + site["url"] + "  =========\n\n")
                bOrders = True
            webasyst_api.ApplyActionToOrder(orderId, u"complete")
            message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен и оплачен'\n".format(boUrl, orderId)
            continue

        if trackingNumber in [None, "", -1]:
            continue
        trackingNumber = trackingNumber.strip()
        if len(trackingNumber) not in [10, 13, 14]:
            message += "[Error] Неверно указан трекинг-код {} по заказу заказа <a target='blank_' href='{}'>{}</a>\n".format(trackingNumber, boUrl, orderId)
            continue


        ######################### доставка СДЭКом ########################
        if len(trackingNumber) == 10:
            continue
##            print orderId
            if bNovallOrder == False:
                sdek = sdekIP
            else:
                sdek = sdekNovall

            orderSdekState = sdek.GetOrderState(trackingNumber)
            #костыль для заказов, оформленных на ИП
            if orderSdekState == -1:
                orderSdekState = sdekIP.GetOrderState(trackingNumber)

            if orderSdekState == -1:
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    bOrders = True
                message += "[Error] Не удалось определить статус доставки заказа <a target='blank_' href='{0}'>{1}</a>\n".format(boUrl, orderId)
                continue

            if orderSdekState == "order_not_found":
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    bOrders = True
                message += "[Error] Не найдена накладная {0} по заказу <a target='blank_' href='{1}'>{2}</a>\n".format(trackingNumber, boUrl, orderId)
                continue

            if orderState == u"Ждет отправки":
                if orderSdekState in ["in_transit", "shipped"]:
                    if orderSdekState == "in_transit":
                        wa_state = u"Отправлен"
                        orderState = u"Отправлен"
                        action = "ship"
                    else:
                        if not bCourierDelivery:
                            wa_state = u"Доставлен в пункт выдачи"
                            action = "dostavlen-v-punkt-vydachi"
                        if bCourierDelivery:
                            wa_state = u"Доставлен в город-получатель"
                            action = "dostavlen-v-gorod-poluchatel"

                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    webasyst_api.ApplyActionToOrder(orderId, action)
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на '{2}'\n".format(boUrl, orderId, wa_state.encode("utf-8"))
                    mode = orderSdekState
                    #удаляем данные об отправке из "Служебных записок"
                    orderSender = commonLibrary.GetOrderSender(orderNote)
                    if orderSender != -1:
                        orderNote = orderNote.replace(orderSender,"")
                    orderNote += "\ndonotsend"
                    webasyst_api.SetOrderComment(orderId, orderNote)
                else:
                    awaitingTransitStateDate = webasyst_api.GetOrderStateDate(orderId, u"Ждет отправки")
                    if today.day != awaitingTransitStateDate.date().day:
                        label = commonLibrary.GetOrderSender(orderNote)
                        notShippedOrders[site["url"]].append("<a target='blank_' href='{3}'>{4}</a> ({1}, <a target='blank_' href='https://www.cdek.ru/track.html?order_id={2}'>{2}</a>)".format(orderId, label, trackingNumber, boUrl, orderId))
                        pastDays = (today - awaitingTransitStateDate.date()).days
                        if now.hour == 9 :
                            if (awaitingTransitStateDate.time().hour + 3 < 15 and pastDays == 1) or (awaitingTransitStateDate.time().hour + 3 > 15 and pastDays == 2):
                                # отправка письма менеджеру СДЭК
                                courierRequestNumber = commonLibrary.GetParamValueFromComment(orderNote, "courierRequestNumber")
                                if courierRequestNumber != -1:
                                    mes = "Здравствуйте!\n\nУточните, пожалуйста, почему не выполнен забор груза по заявке {0}?\n\nС уважением,\nИван".format(courierRequestNumber)
                                    result = mails.SendMailToPartner("support_msk@cdek.ru", courierRequestNumber, mes)
                                    if result:
                                        message += "Отправлено письмо менеджеру СДЭК по заявке {}\n".format(courierRequestNumber)
                                    else:
                                        message += "[Error] Ошибка отправки письма менеджеру СДЭК по заявке {}\n".format(courierRequestNumber)


            if orderState == u"Отправлен":
                if orderSdekState == "shipped":
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    if not bCourierDelivery:
                        wa_state = u"Доставлен в пункт выдачи"
                        action = "dostavlen-v-punkt-vydachi"
                    if bCourierDelivery:
                        wa_state = u"Доставлен в город-получатель"
                        action = "dostavlen-v-gorod-poluchatel"
                    if 'wa_state' not in locals():
                        message += "[Error] Неверно указан способ доставки в заказе <a target='blank_' href='{0}'>{1}</a>\n".format(boUrl, orderId)
                        continue
                    webasyst_api.ApplyActionToOrder(orderId, action)
                    message += "Статус заказа <a target='blank_' href='{}'>{}</a> изменен на '{}'\n".format(boUrl, orderId, wa_state.encode("utf-8"))
                    mode = "shipped"
                else:
                    SendSMSToCustomer(site, orderId, "in_transit", trackingNumber)

            if orderState in [u"Доставлен в пункт выдачи", u"Доставлен в город-получатель"]:
                if orderSdekState == "delivered":
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    webasyst_api.ApplyActionToOrder(orderId, "vruchen")
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Вручен'\n".format(boUrl, orderId)
                else:
                    mode = "reminder"
                    if 'bCourierDelivery' in locals():
                        if not bCourierDelivery:
                            wa_state = u"Доставлен в пункт выдачи"
                        else:
                            wa_state = u"Доставлен в город-получатель"
                    else:
                        message += "[Error] Неверно указан способ доставки в заказе <a target='blank_' href='{0}'>{1}</a>\n".format(boUrl, orderId)
                        continue
                    try:
                        shippedDate = webasyst_api.GetOrderStateDate(orderId, wa_state)
                        pastDays = (datetime.date.today() - shippedDate.date()).days
                    except:
                        continue

                    if pastDays >= 1 and bCourierDelivery:
                        if sdek.GetOrderStatesCodes(trackingNumber)[-1] == 11:
                            continue
                        if now.hour == 9 and orderNote.find("requestSended") == -1:
                            mes = "Здравствуйте!\n\nУточните, пожалуйста, почему не доставлен заказ по накладной {0}?\n\nС уважением,\nИван".format(trackingNumber)
                            result = mails.SendMailToPartner("support_msk@cdek.ru", trackingNumber, mes)
                            if result:
                                message += "Отправлено письмо менеджеру СДЭК по накладной {}\n".format(trackingNumber)
                                orderNote += "\nrequestSended"
                                webasyst_api.SetOrderComment(orderId, orderNote)
                            else:
                                message += "[Error] Ошибка отправки письма менеджеру СДЭК по накладной {}\n".format(trackingNumber)

                        notDeliveredCouriersOrders[site["url"]].append("\n<a target='blank_' href='{2}'>{0}</a> ( https://www.cdek.ru/track.html?order_id={1} )".format(orderId, trackingNumber, boUrl))

                    if pastDays > 3:
                        if bOrders == False:
                            message += ("\n=========  " + site["url"] + "  =========\n\n")
                            bOrders = True
                        message += "ЗАКАЗ <a target='blank_' href='{0}'>{1}</a>, ОТПРАВЛЕННЫЙ СДЭК'ОМ, НЕ ВРУЧЕН (ДНЕЙ): {2}\n".format(boUrl, orderId, pastDays)

                    SendSMSToCustomer(site, orderId, "shipped", trackingNumber)

        ######################### доставка почтой России ########################
        else:
            rpOrdersStates = rp.GetOrderStates(trackingNumber)

            if rpOrdersStates == False:
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    bOrders = True
                message += "[Error] Не удалось получить статус почтового заказа <a target='blank_' href='{0}'>{1}</a>\n".format(boUrl, orderId)
                continue

            if orderState == u"Ждет отправки":
                minStatesQty = 0
                if trackingNumber.startswith("801") or trackingNumber.startswith("800"):
                    minStatesQty = 1

                if len(rpOrdersStates) > minStatesQty:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    webasyst_api.ApplyActionToOrder(orderId, "ship")
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Отправлен', трекинг-код {2}\n".format(boUrl, orderId, trackingNumber)
                    decreaseWarehouseProductQuantity(orderId, "warehouse_spb")
                    mode = "in_transit"

            if orderState == u"Отправлен":
                if u"Прибыло в место вручения" in rpOrdersStates:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    action = "dostavlen-v-pochtovoe-otdelenie"
                    webasyst_api.ApplyActionToOrder(orderId, action)
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен в почтовое отделение'\n".format(boUrl, orderId)
                    mode = "shipped"
                else:
                    SendSMSToCustomer(site, orderId, "in_transit", trackingNumber)

            if orderState == u"Доставлен в почтовое отделение":
                if u"Вручение адресату" in rpOrdersStates:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    webasyst_api.ApplyActionToOrder(orderId, "vruchen")
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Вручен'\n".format(boUrl, orderId)

                elif u"Возврат - Истек срок хранения" in rpOrdersStates:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        bOrders = True
                    webasyst_api.ApplyActionToOrder(orderId, "refund")
                    mes = u"Статус заказа " + str(orderId) + u" изменен на 'Возврат'"
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Возврат'\n".format(boUrl, orderId)
                else:
                    mode = "reminder"
                    shippedDate = webasyst_api.GetOrderStateDate(orderId, u"Доставлен в почтовое отделение")

                    pastDays = (datetime.date.today() - shippedDate.date()).days
                    if pastDays > 3:
                        if bOrders == False:
                            message += ("\n=========  " + site["url"] + "  =========\n\n")
                            bOrders = True
                        message += "ЗАКАЗ <a target='blank_' href='{0}'>{1}</a>, ОТПРАВЛЕННЫЙ ПОЧТОЙ, НЕ ВРУЧЕН (ДНЕЙ): {2}\n".format(boUrl, orderId, pastDays)

                    SendSMSToCustomer(site, orderId, "shipped", trackingNumber)

        if mode != None:
            SendSMSToCustomer(site, orderId, mode, trackingNumber)


if message != "":
    result = mails.SendInfoMail("Отчет изменения статусов заказов", message)

notShippedOrdersMessage = ""
notDeliveredCouriersOrdersMessage = ""
now = datetime.datetime.now()

for site in notShippedOrders.keys():
    if len(notShippedOrders[site]) != 0:
        orders = ", ".join(str(orderId) for orderId in notShippedOrders[site])
        notShippedOrdersMessage += "<p>{0}: {1}</p><br/>".format(site, orders)

for site in notDeliveredCouriersOrders.keys():
    if len(notDeliveredCouriersOrders[site]) != 0:
        orders = ", ".join(str(orderId) for orderId in notDeliveredCouriersOrders[site])
        notDeliveredCouriersOrdersMessage += "{0}: {1}\n\n".format(site, orders)

if now.hour == 9:
    if notShippedOrdersMessage != "" and now.weekday() not in [0, 6]:
        mails.SendInfoMail("Неотправленные заказы", notShippedOrdersMessage)

    if notDeliveredCouriersOrdersMessage != "" and now.weekday() not in [5, 6]:
        mails.SendInfoMail("Недоставленные курьерские заказы", notDeliveredCouriersOrdersMessage)

