# -*- coding: utf-8 -*-

import log
import datetime
import sys
import WebasystAPI
import SendMails
import SdekAPI
import time
import commonLibrary
import SrvOnline
import ModulbankAPI
import math

def GetCoefficient(contractor):
    if orderData["Novall"] == False or (orderData["Novall"] == True and bWithoutIncrease):
        return 1
    if contractor in [u"Велл",u"Поликон", u"Алекс"]:
        return float(1.05)
    else:
        return float(1.2)

def GetOrderSum():
    rows = note.split("\n")
    for row in rows:
        if row.find(u"orderSum") != -1:
            try:
                return int(float( row.replace(u"orderSum", "").strip()))
            except:
                return "Некорректное значение orderSum в заказе {}\n\n".format(orderId)

    return -1

def GetContractorData(note, bCheck = True):
    global message

    contractorData = {}

    rows = note.split("\n")
    for row in rows:
        if row.find(u"ИНН") != -1:
            contractorData["INN"] = row.replace(u"ИНН", "").replace(" ","").strip()
        if row.find(u"КПП") != -1:
            contractorData["KPP"] = row.replace(u"КПП", "").replace(" ","").strip()
        if row.find(u"БИК") != -1:
            contractorData["BIK"] = row.replace(u"БИК", "").replace(" ","").strip()
        if row.find(u"ОГРН") != -1:
            contractorData["OGRN"] = row.replace(u"ОГРН", "").replace(" ","").strip()
        if row.find(u"РС") != -1:
            contractorData["RS"] = row.replace(u"РС", "").replace(" ","").strip()

    if bCheck == False:
        return contractorData

    data = []
    if "INN" not in contractorData.keys() and "OGRN" not in contractorData.keys():
        data.append(u"ИНН")
        data.append(u"ОГРН")
##    if "BIK" not in contractorData.keys():
##        data.append(u"БИК")
##    if "RS" not in contractorData.keys():
##        data.append(u"РС")

    if len(data) != 0:
        message += "[Error] Недостаточно данных для создания счета по заказу {0}: {1}\n".format(orderId, ", ".join(data).encode("utf-8"))
        logging.LogErrorMessage(u"Недостаточно данных для создания счета по заказу {0}: {1}".format(orderId, ", ".join(data)))
        return -1

    return contractorData


def GetOrderDeliveryDescription(orderId):
    deliveryData = webasyst_api.GetOrderDeliveryData(orderId)
    if len(deliveryData) == 3:
        city, shipping_id, shipping_name = deliveryData
    else:
        city, address, shipping_id, shipping_name = deliveryData
    if shipping_name in [u"До двери", u"Курьером до двери", u"Курьером до двери (Курьер)"]:
        if city[0].isdigit() == True:
            city =  webasyst_api.GetCityBySdekCode(city)
            if city == -1:
                return -1
        return u"курьером до двери в г. {0}".format(city)
    elif shipping_name.find(u"выдачи") != -1:
        return u"на пункт выдачи в г. {0}".format(city)
    else:
        return u"почтой России."

def GetInvoiceNumber(note):
    try:
        return [x.replace(u"№", "") for x in note.split(" ") if x.startswith(u"№")][0]
    except:
        return -1


def GetOrderPaid(note):
    if operationsHistoryNovall == -1 or operationsHistory == -1:
        return False

    global sendLetter
    global message

    invoiceNumber = GetInvoiceNumber(note)
    if invoiceNumber == -1:
        return False

##    orderCost = webasyst_api.GetOrderTotalCost(orderId)
    orderCost = GetOrderSum()
    if type( orderCost) != type(1):
        sendLetter = True
        message += "[Error] {}".format(orderCost)
        return False
    if orderCost == -1:
        orderCost = webasyst_api.GetOrderTotalCost(orderId)
    contractorData = GetContractorData(note, False)
    if "INN" not in contractorData.keys():
        sendLetter = True
        return -1

    if commonLibrary.GetNovallOrder(note) == True:
        operationsHistory_ = operationsHistoryNovall
    else:
        operationsHistory_ = operationsHistory

    for operation in operationsHistory_:
        if operation["contragentInn"] == contractorData["INN"]:
            operationAmount = int(operation["amount"])
            if int(operationAmount) == int(orderCost):
                sendLetter = True
                return operation["paymentPurpose"]
            else:
                bOtherOrder = False
                customersOrders = webasyst_api.GetCustomerOrdersIds(customerId)
                if len(customersOrders) > 1:
                    for customerOrderId in customersOrders:
                        orderCosts = webasyst_api.GetOrderTotalCost(customerOrderId)
                        if operationAmount == int(orderCosts):
                            bOtherOrder = True
                            sOrders = ",".join([str(x) for x in customersOrders])
                            message += "У контрагента {0} несколько заказов: {1}\n".format(int(contractorData["INN"]), sOrders)
                            break

                operationDate = [int(x) for x in operation["created"].split("T")[0].split("-")]
                operationDate = datetime.date(operationDate[0],operationDate[1], operationDate[2])
                todayDate = datetime.date.today()
                delta = (todayDate - operationDate).days
                if bOtherOrder == False and delta < 2:
                    if note.find("letterSended") == -1:
                        sendLetter = True
                        message += "[Error] Стоимость заказа {0} {1} руб. отличается от суммы {2} руб., оплаченной по счету №{3}\n" \
                        .format(orderLink, int(orderCost), operationAmount, invoiceNumber)
                        webasyst_api.SetOrderComment(orderId, webasyst_api.GetOrderComment(orderId) + "\nletterSended")

    return False

################################################################################

states = [u"Новый", u"В ожидании оплаты по счету"]

webasyst_api = WebasystAPI.WebasystAPI()

mails = SendMails.SendMails()
srvOnline = SrvOnline.SrvOnline()
mb = ModulbankAPI.ModulbankAPI()
mbNovall = ModulbankAPI.ModulbankAPI(True)
##sdek = SdekAPI.SdekAPI()
machineName = commonLibrary.GetMachineName()

##q = mb.GetAccountInfo()

operationsHistory = mb.GetOperationsHistory((datetime.date.today() - datetime.timedelta(days=15)).strftime('%Y-%m-%d'), "Debet")
operationsHistoryNovall = mbNovall.GetOperationsHistory((datetime.date.today() - datetime.timedelta(days=15)).strftime('%Y-%m-%d'), "Debet")

message = ""

if operationsHistoryNovall == -1 or operationsHistory == -1:
    message = "[Error] Ошибка сервера Модульбанка\n"
sendLetter = False

todayData = datetime.date.today().strftime('%d.%m.%Y')
endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=190)

for site in commonLibrary.wa_sitesParams:
    webasyst_api = commonLibrary.GetWebasystInstance(site)
    if webasyst_api == False:
        message += "Ошибка соединения с сайтом '{}'".format(site["url"])
        continue

    orders = webasyst_api.GetOrdersIds(states, startDate, endDate)
    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]
    bOrders = False

    for orderId in orders:
        # if orderId != 13046:
        #     continue

        orderData = {}
        note = webasyst_api.GetOrderComment(orderId)
        if commonLibrary.GetOrderLocked(note):
            continue

        boUrl = "{}?action=orders#/order/{}/".format(backofficeUrl, orderId)
        orderLink = "<a target='blank_' href='{0}'>{1}</a>".format(boUrl, orderId)

        try:
            customerId = webasyst_api.GetOrderCustomerData(orderId)[-1]
        except:
            message += "[Error] Ошибка создания счета по заказу {0}: не найден покупатель\n".format(orderLink)
            continue
        orderData["Novall"] = commonLibrary.GetNovallOrder(note)
        bRounding = note.find("withoutRounding") == -1
        bWithoutIncrease = note.find("withoutIncrease") != -1
        postageIncluded = note.find("postageIncluded") >= 0
        orderDeliveryCost = int(webasyst_api.GetOrderDeliveryCost(orderId))
        if orderData["Novall"] == True:
            orderDeliveryCost = int(float(orderDeliveryCost) * 1.2)

        ################ создание счета ################
        if note.find("createInvoice") != -1:
            webasyst_api.SetOrderComment(orderId, note + " locked")
            sendLetter = True
            contractorData = GetContractorData(note)
            if contractorData == -1:
                continue

            if bOrders == False:
                message += "\n=========  " + site["url"] + "  =========\n\n"
                bOrders = True

            orderData["date"] = todayData
            orderData["orderDeliveryCost"] = orderDeliveryCost
            orderData["postageIncluded"] = postageIncluded
            postagePerItem = 0
            orderItems = webasyst_api.GetOrderItems(orderId)
            itemsAmount = sum(x[4] for x in orderItems)
            if orderData["postageIncluded"] == True:
                postagePerItem = int(orderData["orderDeliveryCost"] / itemsAmount)
                orderDeliveryCost = postagePerItem * itemsAmount
##                if orderData["Novall"] == False:
##                    webasyst_api.SetOrderDeliveryCost(orderId, orderDeliveryCost)

            orderData["orderDeliveryDescription"] = GetOrderDeliveryDescription(orderId)
            if orderData["orderDeliveryDescription"] == -1:
                message += "[Error] Ошибка создания счета по заказу {0}: не найден город с указанным кодом\n".format(orderLink)
                webasyst_api.SetOrderComment(orderId, note)
                continue
            orderData["orderItems"] = []
            if postagePerItem == 0:
                orderSum = orderDeliveryCost
            else:
                orderSum = 0

            if bWithoutIncrease == False:
                orderDiscountPercent = commonLibrary.GetDiscount(note)
                orderDiscountSum = webasyst_api.GetOrderDiscountSum(orderId)
                discountPerItem = int(float(orderDiscountSum) / float(itemsAmount))
            else:
                orderDiscountPercent = 0
                discountPerItem = 0
            for orderItem in orderItems:
                product_id, sku_id, name, price, quantity, purchase_price = orderItem
                item = {}
                item["Name"] = name
                if orderDiscountPercent != 0:
                    price = webasyst_api.GetProductSkuDetails(sku_id)[2]
                itemContractor = commonLibrary.GetContractorName(webasyst_api.GetSkuArticle(sku_id))
                coefficient = GetCoefficient(itemContractor)
                if orderDiscountPercent != 0:
                    itemPrice = round(float(price) * float(1 - orderDiscountPercent/100) * coefficient, 2)
                else:
                    itemPrice = round(float(price - discountPerItem) * coefficient, 2)
                if bRounding:
                    itemPrice = int(10 * math.ceil (float(itemPrice)/10))
                itemPrice = itemPrice + postagePerItem
                orderSum += itemPrice * float(quantity)
                item["Price"] = str(itemPrice)
                item["Amount"] = str(quantity)
                orderData["orderItems"].append(item)

            srvOnline.Login()
            res = srvOnline.CreateInvoice(orderData, contractorData)
            if type(res) == type(1):
                error = ""
                if res == -2:
                    error = ": не найден БИК"
                if res == -3:
                    error = ": не найден контрагент по ИНН"
                message += "[Error] Ошибка создания счета по заказу {0}{1}\n".format(orderLink, error)
                webasyst_api.SetOrderComment(orderId, note)
                continue
            else:
                invoiceNumber = res[0]
                docNumber = res[1]
                href = "https://srv-online.ru/desktop/docs/Счет%20№%20{0}%20от%20{1}.pdf?mod=docs.print&prm[]=2013&q[action]=preview&q[docs]=2-{2}&q[stamp]=1".format(invoiceNumber, todayData, docNumber)
                message += "<p>Выставлен <a href='{3}'>счет №{0} от {1}</a> по заказу {2}</p>".format(invoiceNumber, todayData, orderLink,href)
                note = note.replace("\ncreateInvoice", "")
                note += u"\nВыставлен счет №{0} от {1}".format(invoiceNumber, orderData["date"])
                note += u"\norderSum{0}".format(str(int(orderSum)))
                webasyst_api.SetOrderComment(orderId, note)
                webasyst_api.ApplyActionToOrder(orderId, u"v-ozhidanie-oplaty-po-schetu")

        ################ создание договора ###################
        if note.find("createContract") != -1:
            sendLetter = True

            webasyst_api.SetOrderComment(orderId, note + " locked")

            if postageIncluded == False and orderDeliveryCost == 0:
                orderDeliveryDescription = ""
            else:
                orderDeliveryDescription = GetOrderDeliveryDescription(orderId)

            if orderDeliveryDescription == -1:
                message += "[Error] Ошибка создания договора по заказу {0}: не найден город с указанным кодом\n".format(orderLink)
                webasyst_api.SetOrderComment(orderId, note)
                continue

            if 'invoiceNumber' not in locals():
                invoiceNumber = GetInvoiceNumber(note)

            if invoiceNumber == -1:
                webasyst_api.SetOrderComment(orderId, note)
                continue

            if bOrders == False:
                message += "\n=========  " + site["url"] + "  =========\n\n"
                bOrders = True

            srvOnline.Login()
            contractNumber = srvOnline.CreateContract(invoiceNumber, orderDeliveryDescription, commonLibrary.GetNovallOrder(note))
            if contractNumber == -1:
                message += "[Error] Ошибка создания договора по заказу {0}\n".format(orderLink)
                webasyst_api.SetOrderComment(orderId, note)
                continue
            else:
                message += "Создан договор №{0} от {1} по заказу {2}\n".format(contractNumber, todayData, orderLink)
                note = note.replace("\ncreateContract", "")
                note += u"\nСоздан договор {0} от {1}".format(contractNumber, todayData)
                webasyst_api.SetOrderComment(orderId, note)

        ################ создание товарной накладной ################
        orderPaid = GetOrderPaid(note)
        if orderPaid not in [-1, False]:
            if bOrders == False:
                message += "\n=========  " + site["url"] + "  =========\n\n"
                bOrders = True

            nds = ""
            if orderData["Novall"] == False:
                entries = [u"т.ч.", u"20%", u"в том числе", u"20.00%"]
                for entry in entries:
                    if orderPaid.lower().find(entry) != -1:
                        nds = u"!!!ВОЗМОЖНО, ВЫДЕЛЕН НДС!!!\n"

            message += """Заказ {0} оплачен ("{1}")\n{2}""".format(orderLink, orderPaid.encode("utf-8"), nds.encode("utf-8"))
            if nds != "":
                if note.find("letterSended") == -1:
                    sendLetter = True
                    webasyst_api.SetOrderComment(orderId, webasyst_api.GetOrderComment(orderId) + "\nletterSended")
                else:
                    sendLetter = False
                continue
            if orderDeliveryCost == 0 or orderData["Novall"] == True:
                note += u"\ndonotsend"
            webasyst_api.SetOrderComment(orderId, note)
            firstname, lastname, phone, email, contact_id = webasyst_api.GetOrderCustomerData(orderId)
            if email != None:
                email = email.strip("_")
                webasyst_api.SetCustomerEmail(contact_id,email)
            webasyst_api.ApplyActionToOrder(orderId, u"pay")

            deliveryData = webasyst_api.GetOrderDeliveryData(orderId)
            if len(deliveryData) == 3:
                city, shipping_id, shipping_name = deliveryData
            else:
                city, address, shipping_id, shipping_name = deliveryData
            if shipping_name in [u"До двери", u"Курьером до двери", u"Курьером до двери (Курьер)"]:
                if city[0].isdigit() == False:
                    sdekCityCode = webasyst_api.GetSdekCityCode(city)
                    if len(sdekCityCode) != 1:
                        message += "НУЖНО ВРУЧНУЮ УСТАНОВИТЬ КОД ГОРОДА\n"
##                    else:
##                        webasyst_api.SetOrderAddressCity(orderId, sdekCityCode[0])

            if commonLibrary.GetNovallOrder(note) == False:
                invoiceNumber = GetInvoiceNumber(note)

                srvOnline.Login()
                result = srvOnline.CreatePackingList(invoiceNumber, orderData["Novall"])
                if result == -1:
                    message += "[Error] Ошибка создания товарной накладной по заказу {0}\n".format(orderLink)
                    continue
                else:
                    message += "Создана товарная накладная по заказу {0}\n".format(orderLink)
        elif orderPaid == -1:
            message += "[Error] Недостаточно данных для проверки оплаты по заказу {0}\n".format(orderLink)
            continue
        else:
            pass

    if srvOnline.selenium != None:
        srvOnline.CloseSelenium()

if sendLetter != False:
    result = mails.SendInfoMail("Обработка заказов юрлиц", message)
