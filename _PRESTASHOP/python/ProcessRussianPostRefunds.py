﻿import urllib
import log
import datetime
import PrestashopAPI
import os
import re, urlparse
from selenium import webdriver
import myLibrary
import SendMails
import RussianPostAPI
import time
import commonLibrary
import sys
import subprocess
from lxml import etree


################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processRPRefunds")
logging = log.Log(logfile)

today = datetime.date.today()
now = datetime.datetime.now()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=2)
machineName = commonLibrary.GetMachineName()

message = ""
sendLetter = False

rp = RussianPostAPI.RussianPostAPI()
mails = SendMails.SendMails()
prestashop_api = PrestashopAPI.PrestashopAPI()
refunds = prestashop_api.GetRussianPostRefunds()
refundTrackingNumbers = [x[3] for x in refunds]

for site in commonLibrary.sitesParams:

    message += ("\n=========  " + site["url"] + "  =========\n\n")
    logging.Log("=========  " + site["url"] + "  =========")

    if machineName == "hp":
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            continue

    orders = prestashop_api.GetOrdersIds([u"Отклонен"], startDate, endDate)
    for order in orders:
        orderId = order[0]
        trackNum = prestashop_api.GetOrderTrackingNumber(orderId)
        if len(trackNum) == 14 and trackNum not in refundTrackingNumbers:
            data = {}
            data["state"] = commonLibrary.GetPostRefundState(rp.GetOrderStates(trackNum))
            if data["state"] in [-1]:
                continue
            data["date"] = prestashop_api.GetOrderStateDate(orderId, u"Отклонен")
            data["orderReference"] = prestashop_api.GetOrderReference(orderId)
            data["trackingNumber"] = trackNum
            items = []
            orderItems = prestashop_api.GetOrderItems(orderId)
            for orderItem in orderItems:
                name = prestashop_api.CleanItemName(prestashop_api.GetProductOriginalName(orderItem[0]))
                items.append( u"{0}_{1}, ".format(name, orderItem[3]))
            data["items"] = ", ".join(items).strip(", ")
            prestashop_api.AddRussianPostRefund(data)
            message += "Добавлена информация по возврату заказа {0}\n".format(data["orderReference"])
            logging.Log(u"Добавлена информация по возврату заказа {0}".format(data["orderReference"]))
            sendLetter = True

    if machineName == "hp":
        ssh.kill()

for refund in refunds:
    id, date, orderReference, items, trackingNumber, state = refund
    if state != u"Вручен":
        newState = commonLibrary.GetPostRefundState(rp.GetOrderStates(trackingNumber))
        if newState == -1:
            continue
        if newState != state:
            prestashop_api.SetRussianPostRefundState(trackingNumber, newState)
            message += "Изменен статус возврата заказа {0} на '{1}'\n".format(orderReference, state.encode("utf-8"))
            logging.Log(u"Изменен статус возврата заказа {0} на '{1}'\n".format(orderReference, state))
            sendLetter = True

if sendLetter == True:
    mails.SendInfoMail("Обработка почтовых возвратов", message)
