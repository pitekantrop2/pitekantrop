﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import re
import SendMails
import subprocess
import os


seoBlock = u"""<div style="text-align:left;"><p><br /><br /></p>
<h3 style="font-size:14px;">Купить {name} в {city}</h3>
<p>Купить {name} можно, например, с доставкой на пункт выдачи:</p>
{points}
<p><a href="/content/2-delivery" rel="nofollow" target="_blank">все способы доставки</a></p></div>"""

pointSource = u"""
<p><span style="text-decoration:underline;">{name}</span> - г. {city}, {address}, телефоны: {phones}, время работы: {hours}</p>
"""


def GetProductSeoBlock(name, shopName):
    pvzNodes = root.xpath("//Pvz[contains(@City, '" + shopName + "')]")
    city = commonLibrary.GetCity(shopName)
    tmpSeoBlock = seoBlock.replace("{name}", downcase(name)).replace("{city}", city)

    #формирование страницы
    if len(pvzNodes) == 0:
        return tmpSeoBlock.replace("{points}", "")

    pointsSource = ""

    if len(pvzNodes) < 4:
        pointsNum = len(pvzNodes)
    else:
        pointsNum = 3

    for i in range(0, pointsNum):

        pvzNode = pvzNodes[i]
        pointSource_tmp = pointSource
        pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
        pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
        pointSource_tmp = pointSource_tmp.replace("{phones}", pvzNode.attrib["Phone"])
        pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])
        pointSource_tmp = pointSource_tmp.replace("{city}", shopName)

##        if i != len(pvzNodes) - 1:
##            pointSource_tmp += "<p></p>"

        pointsSource += pointSource_tmp

    tmpSeoBlock = tmpSeoBlock.replace("{points}", pointsSource)

    return tmpSeoBlock


def DeleteSeoBlock(descr):

    ind = descr.rfind("""<p><br/><br/></p>""")

    if ind != -1:
        descr = descr[0:ind]

    return descr

def UpdateProductsSeoParameters(shopId):
    shopProducts = prestashop_api.GetShopProducts(shopId)

    if shopProducts == -1:
        return

    for shopProduct in shopProducts:

        productID, link_rewrite, meta_title, description, name, meta_keywords, meta_description = shopProduct

        if meta_title.find(city) != -1:
            continue

        if prestashop_api.IsProductActive(productID) == 0:
            continue

##        if productID not in [30, 62, 164, 165, 167, 355]:
##            continue

##        print productID

##        if link_rewrite.find(shopName.replace(" ", "-")) != -1:
##            continue
##
##        if link_rewrite.find(shopName.encode("cp1251").replace("\xa0", "-").decode("cp1251")) != -1:
##            continue

        if meta_title == None:
            return

##        meta_title = meta_title.replace(u" в Москве", "")
##        meta_title = meta_title.replace(u" в Омске", "")
##        meta_title = meta_title.replace(u" в " + city, "")
####        meta_title = meta_title.replace(u"купить", "")
##        meta_title = meta_title.replace(u"  ", u" ")
##        meta_title = meta_title.replace(u"otpug - ", "")
##        meta_title = meta_title.replace(u"Отпугиватель.COM - ", "")
##        meta_title = meta_title.replace(u"Amazing Things - ", "")
##        meta_title = meta_title.replace(u"Новалл - ", "")
##        meta_title = meta_title.replace(u"Safetus - ", "")
##        meta_title = meta_title.replace(u"Отпугиватель.Com - ", "")
##        meta_title = meta_title.strip()

##        if prestashop_api.siteUrl not in ["http://omsk.knowall.ru.com", "http://tomsk.otpugivatel.com"]:
        if meta_title.lower().find(u"купить") == -1:
            meta_title += u" купить"

        if meta_title.find(city) == -1:
            if meta_title.find("{0}") != -1:
                meta_title = meta_title.replace("{0}", u"в " + city)
            else:
                meta_title += u" в " + city

        description = description.replace("'","")
        meta_title = meta_title.replace("'","")

##        if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
##            shortDescription = prestashop_api.GetShopProductShortDescription(shopId, productID)
##            if shortDescription.find("<p>") == -1:
##                shortDescription = "<p>" + shortDescription + "</p>"
##                prestashop_api.SetShopProductShortDescription(shopId, productID, shortDescription)

##        print meta_title
##        meta_keywords = u", ".join([u"{0} купить в {1}".format(name, city), u"{0} отзывы".format(name), u"{0} цена".format(name)])
##        categoryId = prestashop_api.GetProductCategoryDefault(productID)
##        categoryDetails = prestashop_api.GetShopCategoryDetails(shopId, categoryId)
##        if categoryDetails == -1:
##            continue
##        if prestashop_api.siteUrl in ["http://safetus.ru"]:
##            meta_description = u"Качественные и недорогие " + downcase(categoryDetails[0][4]) + u" в " + city + u". Купить " + downcase(name) + u" в " + city + u" с гарантией. Скидки. Удобные способы доставки и оплаты."
##        elif prestashop_api.siteUrl in ["http://otpugiwatel.com"]:
##            meta_description = u"Эффективные и недорогие " + downcase(categoryDetails[0][4]) + u" в " + city + u". Купить " + downcase(name) + u" в " + city + u" с гарантией. Скидки. Удобные способы доставки и оплаты."
##        else:
##            if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
##                imName = u"«Отпугиватель.Com»"
##                items = u"эффективные средства от вредителей."
##            if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
##                imName = u"«Новалл»"
##                items = u"качественные {0} в {1}".format(downcase(categoryDetails[0][4]), city)
##            if prestashop_api.siteUrl == "http://saltlamp.su":
##                imName = u"«SaltLamp.Su»"
##                items = u"изысканная польза для вашего дома!"
##            if prestashop_api.siteUrl == "http://sushilki.ru.com":
##                imName = u"«Сушилки»"
##                items = u"лучшие модели сушилок по низким ценам!"
##            if prestashop_api.siteUrl == "https://glushilki.ru.com":
##                imName = u"«Глушилки»"
##                items = u"эффективные средства защиты информации"
##            if prestashop_api.siteUrl == "http://insect-killers.ru":
##                imName = u"«Insect Killers»"
##                items = u"избавьтесь от летающих насекомых!"
##            if prestashop_api.siteUrl == "http://mini-camera.ru.com":
##                imName = u"«Мини камера»"
##                items = u"лучшие модели скрытых камер по низким ценам!"
##            if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
##                imName = u"«Отпугиватели грызунов»"
##                items = u"эффективные средства защиты от крыс и мышей."
##            if prestashop_api.siteUrl == "http://otpugivateli-krotov.ru":
##                imName = u"«Отпугиватели кротов»"
##                items = u"эффективные средства защиты от садовых вредителей."
##            if prestashop_api.siteUrl == "http://otpugivateli-ptic.ru":
##                imName = u"«Отпугиватели птиц»"
##                items = u"эффективные средства защиты от пернатых вредителей."
##            if prestashop_api.siteUrl == "http://otpugivateli-sobak.ru":
##                imName = u"«Отпугиватели собак»"
##                items = u"эффективные средства защиты от агрессивных животных."
##            meta_description = name + u" купить в " + city + u" по низкой цене. Интернет-магазин {0} - {1}".format(imName, items)
##        print meta_keywords
        print link_rewrite
        params = [link_rewrite, meta_title, description, meta_keywords, meta_description]
        prestashop_api.UpdateShopProductData(shopId, productID, params)


def UpdateProductsDescriptions(shopId):
    shopProducts = prestashop_api.GetShopProducts(shopId)

    for shopProduct in shopProducts:
        id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description = shopProduct
        if description.find("allowfullscreen") == -1:
            continue

        productDescription = etree.HTML(description)
        productDescriptionDiv = productDescription.xpath("//div")
        if len(productDescriptionDiv) != 0:
            productDescriptionDiv = productDescription[0]
            element = productDescriptionDiv.xpath(".//p[contains(., '" + u"Цена с максимальной скидкой"  + "')]")

            if len(element) != 0:
                element[0].getparent().remove(element[0])

        description = commonLibrary.GetStringDescription(productDescription)

        description = DeleteSeoBlock(description)
##        print description
##        seoblock = GetProductSeoBlock(name, shopName)
##        description += seoblock
##        print description

##        print link_rewrite
        logging.Log(link_rewrite)
        params = [link_rewrite, meta_title, description, meta_keywords, meta_description]
        prestashop_api.UpdateShopProductData(shopId, id_product, params)

def DownloadProductImages(productInfo):
    picsForDownload = []
    productDescr = productInfo[3].replace(u"\u02da","").replace(u"\xb2","").replace(u"\xba","").replace(u"\xb3","")
    ldElement = etree.HTML(productDescr)
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(site["url"].replace("http://", commonLibrary.GetSiteUrlScheme(site["url"]))) == -1:
            print picUrl
            picsForDownload.append(picUrl)

    if len(picsForDownload) == 0:
        return

    productName = prestashop_api.CleanItemName(productInfo[0])
    productName = productName.replace("/", " ")
    print productName
    imDir = imagesDir + productName
    if not os.path.exists(imDir):
        os.mkdir(imDir)


    for picUrl in picsForDownload:
        prestashop_api.DownloadImage(picUrl, imDir + "\\")

def UpdateProductPictures(productID, productInfo):
    productDescr = productInfo[3].replace(u"\u02da","").replace(u"\xb2","").replace(u"\xba","").replace(u"\xb3","")
    productName = prestashop_api.CleanItemName(productInfo[0])
    productName = productName.replace("/", " ")
    productDir = "/img/cms/" + productName + "/"
    ldElement = etree.HTML(productDescr)

    bNeedUpdate = False

    productArticle = prestashop_api.GetProductArticle(productID)
    contractor = commonLibrary.GetContractorName(productArticle)
    if contractor != u"Логос":
        elements = ldElement.xpath("//img")
        for element in elements:
            picUrl = element.attrib["src"]
            if picUrl.find(site["url"]) == -1:
                bNeedUpdate = True
                picName = picUrl.split("/")[-1]
                element.attrib["src"] = productDir + picName

        elements = ldElement.xpath("//a//img")
        for element in elements:
            bNeedUpdate = True
            a = element.getparent()
            pos = a.getparent().index(a)
            a.getparent().insert(pos, element)
            element.getparent().remove(a)
    else:
        elements = ldElement.xpath("//img")
        for element in elements:
            bNeedUpdate = True
            element.getparent().remove(element)

    elements = ldElement.xpath("//a")
    for element in elements:
        bNeedUpdate = True
        element.getparent().remove(element)

    description = commonLibrary.GetStringDescription(ldElement)

    if bNeedUpdate == True:
        print productName
        prestashop_api.SetProductDescription(productID, description)


############################################################################################################################################

downcase = lambda s: s[:1].lower() + s[1:] if s else ''

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "updateProducts")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()

##resp = requests.get("http://gw.edostavka.ru:11443/pvzlist.php")
##pvzXml = etree.XML(resp._content)
##root =  pvzXml.getroottree().getroot()
imagesDir = u"c:\\temp\\img\\"

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

##    shops = prestashop_api.GetAllShops()
##
##    for shop in shops:
##        shopId, shopDomen, shopName = shop
##
####        if shopId < 61:
####            continue
##
##        print shopName, shopId
##    ##    print shopId
##        logging.Log(str(shopId))
##
##        shopName = commonLibrary.GetShopName(shopName)
##
##        city = commonLibrary.GetCity(shopName)
##        if city == -1:
##            continue
##
##        UpdateProductsSeoParameters(shopId)
####        UpdateProductsDescriptions(shopId)


    productsIDs = prestashop_api.GetActiveProductsIDs()
##    productsIDs = prestashop_api.GetContractorProductsIds("st")
    ##productsIDs = prestashop_api.GetCategoryProductsIds(258)
    for productID in productsIDs:

##        productID = productID[0]

        productInfo = prestashop_api.GetProductDetailsById(productID)
##        productCategoryDefaultId = prestashop_api.GetProductCategoryDefault(productID)
        print productID

        if productInfo == -1:
            continue

        if productInfo[3].strip() == "":
            continue

##        if prestashop_api.IsProductActive(productID) == 0:
##            continue

        UpdateProductPictures(productID, productInfo)
##        DownloadProductImages(productInfo)
##        UpdateProductsDescriptions()
##
    if machineName.find("hp") != -1:
        ssh.kill()


mails.SendInfoMail("Обновление товаров", "", [logfile])