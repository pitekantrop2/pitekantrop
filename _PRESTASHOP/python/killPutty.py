import psutil
import datetime
import time
import os
import subprocess
import commonLibrary

def KillPython():
    for process in psutil.process_iter():
        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "python.exe" and process.pid != pid:
            process.kill()

def KillPlink():
    for process in psutil.process_iter():
        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "plink.exe":
            process.kill()

def GetLastLogTime():
    files = os.listdir("log")
    times  = [os.path.getctime("log\\" + file) for file in files]
    return datetime.datetime.fromtimestamp(list( reversed (sorted(times)))[0])

#################################################################################

pid = commonLibrary.GetPid()

while True:
    now = datetime.datetime.now()
    if now.hour >= 7 and now.hour < 20:
        if now.minute == 0:
            commonLibrary.KillChromedriver()
            KillPlink()
            now = datetime.datetime.now()
            pastMinutes = int((now - GetLastLogTime()).seconds / 60)
            if pastMinutes > 12:
                KillPython()
                subprocess.Popen(["python", "scheduler.py"])

    time.sleep(50)
