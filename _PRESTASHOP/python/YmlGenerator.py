﻿import requests
import lxml
from lxml import etree
import urllib
import json
import datetime
import sys
import re, urlparse
import PrestashopAPI
import io
import commonLibrary
import subprocess
import ftplib
import os
import log
from yattag import Doc

urlPart = """
<url>
   <loc>{url}</loc>
   <lastmod>{lastmod}</lastmod>
   <changefreq>daily</changefreq>
   <priority>{priority}</priority>
</url>"""

prestashop_api = PrestashopAPI.PrestashopAPI()
machineName = commonLibrary.GetMachineName()
logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\python\\log", "yml")
logging = log.Log(logFileName)

date = datetime.date.today().strftime('%Y-%m-%d')

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen([commonLibrary.puttyPath, '-v', '-ssh', '-2', '-P', '22', '-C', '-l', 'root', '-pw', "IiRMBuAiJTLv", '-L', '5555:127.0.0.1:3306', site["ip"]])

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("=============== {0} ===============".format(prestashop_api.siteUrl))

    bDescriptions = True

##    if site["url"] in ["http://omsk.knowall.ru.com", "http://tomsk.otpugivatel.com"]:
##        companyName = u"ИП Усов Иван Викторович"
##    else:
##        companyName = u"Новалл"

    shopName = commonLibrary.GetCompanyName(prestashop_api.siteUrl)
    companyName = shopName

    ssh_, sftp = commonLibrary.CreateSshSession(site["ip"])
    shops = prestashop_api.GetAllShops()
    domainScheme = prestashop_api.GetSiteUrlScheme()
    for shop in shops:
        doc, tag, text = Doc().tagtext()
        shopId = shop[0]
##        if shopId < 153:
##            continue
        print shopId
        logging.Log(str(shopId))
        shopDomain = domainScheme + shop[1]

        content = """<?xml version="1.0" encoding="UTF-8"?>"""
        with tag('yml_catalog', date=datetime.datetime.now().strftime('%Y-%m-%d')):
            with tag('shop'):
                with tag('name'):
                    text(shopName)
                with tag('company'):
                    text(companyName)
                with tag('url'):
                    text(shopDomain)
                with tag('currencies'):
                    with tag('currency', id="RUR", rate="1"):
                        text("")
                productsIds = []
                with tag('categories'):
                    categories = prestashop_api.GetShopMainCategories(1)
                    for category in categories:
                        categoryId = category[0]
                        with tag('category', id=int(categoryId) ):
                            text(category[1])
                        productsIds += prestashop_api.GetCategoryActiveProductsIds(categoryId)
                        subcategories = prestashop_api.GetCategoryActiveSubcategoriesIds(categoryId)
                        for subcategory in subcategories:
                            categoryName = prestashop_api.GetCategoryName(subcategory)
                            productsIds += prestashop_api.GetCategoryActiveProductsIds(subcategory)
                            with tag('category', id=int(subcategory), parentId= int(categoryId)):
                                text(categoryName)
                            subsubcategories = prestashop_api.GetCategoryActiveSubcategoriesIds(subcategory)
                            for subsubcategory in subsubcategories:
                                productsIds += prestashop_api.GetCategoryActiveProductsIds(subsubcategory)
                                categoryName = prestashop_api.GetCategoryName(subsubcategory)
                                with tag('category', id=int(subsubcategory), parentId= int(subcategory)):
                                    text(categoryName)

                productsIds = list(set(productsIds))
                with tag('offers'):
                    for productId in productsIds:
                        productUrl = prestashop_api.GetProductUrl(productId, shopId)
                        if productUrl == -1:
                            continue
                        url = shopDomain + "/" + productUrl
                        name, price, wholesale_price, description, link_rewrite, meta_title = prestashop_api.GetShopProductDetailsById(productId, shopId)
                        categoryId = prestashop_api.GetProductCategoryDefault(productId)

                        images = prestashop_api.GetProductImagesIds(productId)
                        if len(images) > 0:
                            imgUrl = "{0}/{1}-large_default/{2}.jpg".format(shopDomain, images[0],link_rewrite )
                        else:
                            imgUrl = shopDomain + "/1-large_default/1.jpg"
                        with tag('offer', id=int(productId)):
                            with tag('url'):
                                text(url)
                            with tag('price'):
                                text(float(price))
                            with tag('currencyId'):
                                text("RUR")
                            with tag('picture'):
                                text(imgUrl)
                            with tag('name'):
                                text(name)
                            if bDescriptions:
                                with tag('description'):
                                    text(description)
                            with tag('categoryId'):
                                text(int(categoryId))

        content = """<?xml version="1.0" encoding="UTF-8"?>""" + doc.getvalue()
        ymlFileName = "{0}.yml".format(shop[1].split(".")[0])
        with open(ymlFileName, "w") as file:
            file.write(content.encode("utf-8"))

        sftp.put(ymlFileName, '/home/{0}/www/{1}'.format(site["siteDir"], ymlFileName))
        os.remove(ymlFileName)

    ssh_.close()

    if machineName.find("hp") != -1:
        ssh.kill()