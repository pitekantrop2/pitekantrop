﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import SendMails
import subprocess
import random
import re

saleInfoTemplates = [
u"<p>{Купить|Приобрести} {0} в [city_P] {вы можете|можно}, {оформив|сделав|оставив} заказ {в нашем магазине|в нашем интернет-магазине|в нашем онлайн-магазине|на нашем сайте|в {онлайн-|интернет-}магазине «cname»}. {1}: цена, отзывы, подробное описание.</p>",
u"<p>{В нашем {|онлайн-|интернет-}магазине|На нашем сайте|В {онлайн-|интернет-}магазине «cname»} вы можете {ознакомиться с отзывами|найти отзывы} на {0}, а также {купить|приобрести} {данную|эту} модель по самой {выгодной|доступной|низкой} цене в [city_P].</p>",
u"<p>{Купить|Приобрести} {0} по {выгодной|доступной|низкой} цене в [city_P] очень {просто|легко}: для этого достаточно {оформить заказ {в нашем {|онлайн-|интернет-}магазине|на нашем сайте|в {онлайн-|интернет-}магазине «cname»}|оставить заявку нашим операторам {по телефону или в онлайн-чате|в онлайн-чате или по email|по телефону или email}}.</p>",
]

def GetSaleInfo(name):
    changedName = commonLibrary.ChangeCase(commonLibrary.CleanItemName(commonLibrary.Dowmcase(name)))
    template = random.choice(saleInfoTemplates).replace("{0}", changedName).replace("{1}", commonLibrary.CleanItemName(name)).replace("cname",commonLibrary.GetCompanyName(prestashop_api.siteUrl))
    saleInfo = commonLibrary.GetVariantFromTemplate(shopName, template)
    return saleInfo

def UpdateProductsDescriptions():
    name = prestashop_api.GetProductName(id_product)
    if name == -1:
        logging.LogErrorMessage(str(id_product))
        return
    productDescription = prestashop_api.GetProductDescription(id_product, shopId)
    if productDescription == -1:
        return
    if productDescription.find(commonLibrary.GetVariantFromTemplate(shopName, "[city_P]")) != -1:
        return


##        bUpdate = False
##        try:
##            description = etree.HTML(productDescription)
##        except:
##            continue

##        if bUpdate == True:
##            print id_product
##            productDescription = commonLibrary.GetStringDescription(description)
##    print GetSaleInfo(name)
##    productDescription += GetSaleInfo(name)
##    prestashop_api.SetProductDescription(id_product, productDescription)
    prestashop_api.SetShopProductDescription(shopId, id_product, productDescription)

##        if productDescription.find(u"<p/>") != -1:
##            print id_product
##            productDescription = productDescription.replace("<p/>", "")
####            print productDescription
##            prestashop_api.SetProductDescription(id_product, productDescription)

def GetMetaDescriptionTemplate(name):
    commonTamplates = [
u"""{У нас вы можете|Предлагаем{| вам}} {недорого|выгодно} {приобрести|купить} {0} с доставкой по [city_D].""",
u"""{У нас вы можете|Предлагаем{| вам}} {приобрести|купить} {0} в [city_P] по {оптимальной|выгодной|доступной} цене."""
    ]
    if prestashop_api.siteUrl == "http://safetus.ru":
        templates = [ u"В {наличии|каталоге|ассортименте} мини камеры, глушилки сотовых, видеоглазки, детекторы \"жучков\" и другие полезные товары."]
    if prestashop_api.siteUrl == "http://otpugiwatel.com":
        templates = [u"Интернет-магазин «cname» - быстро и эффективно избавляем от вредителей!"]
    if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
        templates = [u"Интернет-магазин «Отпугиватель.ком» - эффективные средства от вредителей в [city_P]."]
    if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
        templates = [u"Интернет-магазин Новалл - знать всё!"]
    if prestashop_api.siteUrl == "http://saltlamp.su":
        return u" Соляные (солевые) лампы, солевые (соляные) светильники в " + city + "."
    if prestashop_api.siteUrl == "http://sushilki.ru.com":
        return u" Электросушилки для рыбы, грибов, ягод и других продуктов в " + city + "."
    if prestashop_api.siteUrl == "http://glushilki.ru.com":
        templates = [u"В {продаже имеются|наличии|каталоге представлены|ассортименте} {качественные и недорогие|качественные и надежные|надежные и недорогие} подавители сотовой связи и глонасс, глушилки gsm и gps."]
    if prestashop_api.siteUrl == "http://insect-killers.ru":
        templates = [ u"Интернет-магазин «Insect Killers» - ловушки, уничтожители комаров и другие средства от насекомых в [city_P]."]
    if prestashop_api.siteUrl == "http://mini-camera.ru.com":
        templates = [ u"Интернет-магазин «Мини камера» - это большой выбор {миниатюрных видеокамер|миникамер}, {опытные консультанты|компетентные менеджеры} и гарантия на {всю продукцию|все товары}."]
    if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
        return u" Интернет-магазин «Отпугиватели грызунов» - избавьтесь от крыс и мышей!"
    if prestashop_api.siteUrl == "http://otpugivateli-krotov.ru":
        words = [u"кротов", u"медведок", u"змей"]
        random.shuffle(words)
        phrase = ", ".join(words)
        templates = [u"Интернет-магазин «cname» - {продажа|реализация} {надежных|эффективных} {средств от|отпугивателей} {0} и {других|прочих} вредителей в [city_P].".replace("{0}", phrase)]
    if prestashop_api.siteUrl == "http://otpugivateli-ptic.ru":
        cinds = [u"биоакустические", u"визуальные", u"ультразвуковые"]
        catName = prestashop_api.GetCategoryName(prestashop_api.GetProductCategoryDefault(productId)).lower()
        for cind in cinds:
            if catName.lower().find(cind) != -1:
                cinds.remove(cind)
        random.shuffle(cinds)
        sCinds = ", ".join(cinds)
        templates = [u"Также в наличии {0}, механические отпугиватели и другие средства от птиц.".format(sCinds)]
    if prestashop_api.siteUrl == "http://otpugivateli-sobak.ru":
        templates = [  u"Интернет-магазин «Отпугиватели собак» - Когда собаки под контролем"]
    if prestashop_api.siteUrl == "http://incubators.shop":
        cinds = [u"куриных", u"утиных", u"перепелиных", u"гусиных", u"китайских"]
        catName = prestashop_api.GetCategoryName(prestashop_api.GetProductCategoryDefault(productId)).lower()
        for cind in cinds:
            if catName.lower().find(cind) != -1:
                cinds.remove(cind)
        cinds = random.sample(cinds, 3)
        sCinds1 = ", ".join(cinds)
        cinds = [u"Золушка", u"Sititek", u"Блиц"]
        for cind in cinds:
            if catName.lower().find(cind) != -1:
                cinds.remove(cind)
        random.shuffle(cinds)
        sCinds2 = ", ".join(cinds)
        templates = [
u"{Интернет-|Онлайн-}магазин «cname» - {широкий выбор|большой ассортимент} {0} инкубаторов с {гарантией качества|проверенной надежностью|высокой выводимостью яиц}!".replace("{0}", sCinds1),
u"{Интернет-|Онлайн-}магазин «cname» - {широкий выбор|большой ассортимент} инкубаторов {0} с {гарантией качества|проверенной надежностью|высокой выводимостью яиц}!".replace("{0}", sCinds2)]
    if prestashop_api.siteUrl == "http://usiliteli-svyazi.ru":
        templates = [u"{Интернет-|Онлайн-}магазин «cname» - качественные и недорогие {1} в [city_P]."]
    if prestashop_api.siteUrl == "http://gemlux-shop.ru":
        templates = [u"{{Интернет-|Онлайн-}магазин|Магазин} «cname» - {качественные и недорогие|качественные и надежные|надежные и недорогие} {1} в [city_P]."]
    if prestashop_api.siteUrl == "http://otpugivatel.spb.ru":
        templates = [u"{{Интернет-|Онлайн-}магазин|Магазин} «cname» - {эффективные и недорогие|качественные и надежные|недорогие и надежные} {1} в [city_P]."]
    temp = random.choice(commonTamplates) + u" " + random.choice(templates)

    return  temp
##    templates = [
##u"""Предлагаем{| вам} {недорого|выгодно} {приобрести|купить} {0} в [city_P]. {В нашем {интернет|онлайн}-магазине|У нас в каталоге|В ассортименте нашего магазина} {представлены{| только} {качественные|надежные} модели|{имеется|можно найти} множество{| популярных} моделей} {по {доступным|низким} ценам|с гарантией и положительными отзывами}. {Осуществляем оперативную доставку по [city_D]|Быстрая доставка по [city_D]|Доставим заказ} {до двери|курьером{| на дом}} или {на пункт выдачи|в точки самовывоза}."""
##u"""Ищете, где{| недорого| выгодно} {приобрести|купить} {0} в [city_P]? {В нашем {интернет|онлайн}-магазине|У нас в каталоге|В ассортименте нашего магазина} {представлены{| только} {качественные|надежные} модели|{имеется|можно найти} множество{| популярных} моделей} {по {доступным|низким} ценам|с гарантией и положительными отзывами}. {Компетентные консультанты|Вежливые и опытные операторы|Грамотные и отзывчивые менеджеры} {помогут вам подобрать {оптимальный|подходящий} вариант|всегда готовы помочь вам с выбором|с радостью помогут сделать {лучший|оптимальный} выбор}."""
##u"""Если {вы хотите|вам нужно} {приобрести|купить} {0} в [city_P] по {оптимальной|выгодной|доступной} цене - {обращайтесь|заходите} в {интернет|онлайн}-магазин "cname"! Все{| наши} товары {имеют сертификаты соответствия и {гарантию|гарантийный срок} от {{шести|6} месяцев|полугода}|сертифицированы и имеют {гарантию|гарантийный срок} от {{шести|6} месяцев|полугода}}. {Предлагаем множественные|Доступны различные} способы оплаты: наличными, {на расчетный счет|безналичным расчетом}, {в платежной системе|онлайн-деньгами} или {с мобильного|через СМС}.""".replace("cname", commonLibrary.GetCompanyName(prestashop_api.siteUrl))
##]


def SetMetatags():
    name = prestashop_api.GetProductName(productId)
##    print name
##    currentMt = prestashop_api.GetShopProductMetaTitle(shopId, productId)
##    if currentMt.find(u" в ") != -1:
##        return
##    name = prestashop_api.GetProductOriginalName(productId)
    categoryName = commonLibrary.Dowmcase( prestashop_api.GetProductCategoryName(productId))
    sfName = commonLibrary.ChangeCase(name)
    dowmcasedName = commonLibrary.Dowmcase(sfName)
    if prestashop_api.siteUrl in ["http://gemlux-shop.ru"]:
        link_rewrite = prestashop_api.GetProductLinkRewrite(productId)
    elif prestashop_api.siteUrl in ["http://otpugivatel.spb.ru"]:
        link_rewrite = commonLibrary.GetLinkRewriteFromName( commonLibrary.CleanItemName(name) )
    else:
        link_rewrite = prestashop_api.Transliterate(prestashop_api.GetProductLinkRewrite(productId))
    meta_title = commonLibrary.GetVariantFromTemplate(shopName, u"Купить {0} в [city_P]".format(dowmcasedName))
    if len(meta_title) + 17 < 70:
        meta_title += commonLibrary.GetVariantFromTemplate(shopName, u" по {низкой|доступной|выгодной} цене")
    elif len(meta_title) + 8 < 70:
        word = random.choice([u"недорого", u"дешево"])
        meta_title += u" {0}".format(word)
    meta_description = commonLibrary.GetVariantFromTemplate(shopName, GetMetaDescriptionTemplate(name).replace("{0}",dowmcasedName).replace("{1}",categoryName).replace("cname",commonLibrary.GetCompanyName(prestashop_api.siteUrl)))
    meta_keywords = commonLibrary.GetVariantFromTemplate(shopName, u"{0} купить цена в [city_P]".format(sfName))

    params = [link_rewrite, meta_title, meta_keywords, meta_description]
##    for param in params:
##        print param

    prestashop_api.SetProductData(shopId, productId, params)

#############################################################################################################


prestashop_api = PrestashopAPI.PrestashopAPI()

machineName = commonLibrary.GetMachineName()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "updateProducts")
logging = log.Log(logfile)
mails = SendMails.SendMails()

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    logging.Log("==================================  " + site["url"] + "  ==================================")

##    categoryId = 172
##    prestashop_api.SetCategoryUpdateDate(categoryId, prestashop_api.GetTodayDate())

    shops = prestashop_api.GetAllShops()
##    products = prestashop_api.GetActiveProductsIDs()
    products = prestashop_api.GetContractorProductsIds("ww") + prestashop_api.GetContractorProductsIds("me")
##    products = []
##    cats = [171,172,173,174]
##    for cat in cats:
##        products += prestashop_api.GetCategoryActiveProductsIds(cat)

    for shop in shops:
        shopId, shopDomen, shopName = shop
        if shopId < 3:
            continue

##        print shopId
        logging.Log(str(shopId))
        shopName = commonLibrary.GetShopName(shopName)
        for productId in products:
            print productId
##            if prestashop_api.IsProductActive(productId) == 0:
##                continue
            if shopId == 1:
                prestashop_api.SetProductUpdateDate(productId, prestashop_api.GetTodayDate())
##            if productId not in [500]:
##                continue
            try:
                SetMetatags()
            except Exception as e:
                prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])


    if machineName.find("hp") != -1 or machineName.find("pitekantrop") != -1:
        ssh.kill()


