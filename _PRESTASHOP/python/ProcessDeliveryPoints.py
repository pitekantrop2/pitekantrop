﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import SdekAPI
import RussianPostAPI
import shutil
import ftplib
import os
import SendMails
import commonLibrary
import subprocess

def AddCourierCarrier():
    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)
    if u"Курьером до двери." in shopCarriersNames:
        return

    pvzNodes = root.xpath("//Pvz[(starts-with(@City, '" + shopName + "') and contains(@City, ',')) or (@City = '" + shopName + "' and not(contains(@City, ',')))]")

    #если нет курьерской доставки - выходим
    if len(pvzNodes) == 0:
        return

    pvzNode = pvzNodes[0]

    tariffs = [137, 233, 11]
    minCost = 100000
    calcResult = None

    for tariffId in tariffs:
        requestData = {}
        requestData['version'] = "1.0"
        requestData['dateExecute'] = datetime.date.today().strftime('%Y-%m-%d')
        requestData['senderCityId'] = 44
        requestData['receiverCityId'] = int(pvzNodes[0].attrib["CityCode"])
        requestData['tariffId'] = tariffId
        if prestashop_api.siteUrl == "http://saltlamp.su":
            requestData['goods'] = [{'weight':4, 'length':20, 'width':20,'height':20}]
        elif prestashop_api.siteUrl == "http://sushilki.ru.com":
            requestData['goods'] = [{'weight':4, 'length':30, 'width':30,'height':40}]
        elif prestashop_api.siteUrl == "https://incubators.shop":
            requestData['goods'] = [{'weight':5, 'length':50, 'width':40,'height':30}]
        elif prestashop_api.siteUrl == "http://gemlux-shop.ru":
            requestData['goods'] = [{'weight':4, 'length':30, 'width':30,'height':30}]
        else:
            requestData['goods'] = [{'weight':1, 'length':20, 'width':20,'height':20}]
        calcResult_ = sdek.Calculate(requestData)

        if "error" in calcResult_.keys():
            continue

        if int(calcResult_["result"]["price"]) < minCost:
            minCost = int(calcResult_["result"]["price"])
            calcResult = calcResult_
##            print tariffId

    id_carrier = prestashop_api.GetLastInsertedId("ps_carrier","id_carrier") + 1

    pointName = u"Курьером до двери."

    sql = """INSERT INTO ps_carrier(id_carrier, id_reference, id_tax_rules_group, name, url, active, deleted, shipping_handling, range_behavior, is_module, is_free, shipping_external, need_range, external_module_name, shipping_method, position, max_width, max_height, max_depth, max_weight, grade)
            VALUES (%(id_carrier)s,%(id_reference)s,0,'%(pointName)s','',1,0,0,0,0,0,0,0,'',2,3,0,0,0,0,0)"""%{"id_carrier":id_carrier,
            "id_reference":id_carrier, "pointName":pointName}

    prestashop_api.MakeUpdateQueue(sql)

    for j in range(1,4):

        sql = """INSERT INTO ps_carrier_group(id_carrier, id_group)
                VALUES (%(id_carrier)s,%(id_group)s)"""%{"id_carrier":id_carrier,"id_group":j}

        prestashop_api.MakeUpdateQueue(sql)

    delay = u"Срок доставки (рабочих дней): "
    if calcResult["result"]["deliveryPeriodMin"] == calcResult["result"]["deliveryPeriodMax"]:
        delay += str(calcResult["result"]["deliveryPeriodMax"] + 1)
    else:
        delay += str(calcResult["result"]["deliveryPeriodMin"] + 1) \
                + " - " + str(calcResult["result"]["deliveryPeriodMax"] + 1)

    delay += "."

    sql = """INSERT INTO ps_carrier_lang(id_carrier, id_shop, id_lang, delay)
             VALUES (%(id_carrier)s,%(id_shop)s,1,'%(delay)s')"""%{"id_carrier":id_carrier,
             "id_shop":shopId,"delay":delay}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_shop(id_carrier, id_shop)
             VALUES (%(id_carrier)s,%(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_tax_rules_group_shop(id_carrier, id_tax_rules_group, id_shop)
             VALUES (%(id_carrier)s, 0, %(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_zone(id_carrier, id_zone)
             VALUES (%(id_carrier)s, %(zoneId)s)"""%{"id_carrier":id_carrier, "zoneId":zoneId}

    prestashop_api.MakeUpdateQueue(sql)

    id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

    rangesIds = []
    for k in range(0, 21):
        rangesIds.append(id_range_price + k)

    counter = 0

    for id_range_price in rangesIds:
        delimiter1 = 1000 * counter + 1

        if counter == 20:
            delimiter2 = 1000001
        else: delimiter2 = 1000 * (counter + 1)

        sql = """INSERT INTO ps_range_price(id_carrier, delimiter1, delimiter2)
                 VALUES (%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

        prestashop_api.MakeUpdateQueue(sql)

        if counter == 20:
            delimiter2 = 21000

        price = int(calcResult["result"]["price"])
        price = price + delimiter2 * 0.03
        price = int(10 * round(float(price)/10))

        sql = """INSERT INTO ps_delivery(id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                 VALUES (%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        counter += 1

    logging.LogInfoMessage(shopName + " " + pointName + " has been added")

def AddRussianPostCarrier(shopId, shopName , zoneId ):
    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    if u"Почтой России." in shopCarriersNames:
        return

    id_carrier = prestashop_api.GetLastInsertedId("ps_carrier","id_carrier") + 1

    pointName = u"Почтой России."

    sql = """INSERT INTO ps_carrier(id_carrier, id_reference, id_tax_rules_group, name, url, active, deleted, shipping_handling, range_behavior, is_module, is_free, shipping_external, need_range, external_module_name, shipping_method, position, max_width, max_height, max_depth, max_weight, grade)
            VALUES (%(id_carrier)s,%(id_reference)s,0,'%(pointName)s','',1,0,0,0,0,0,0,0,'',2,10004,0,0,0,0,0)"""%{"id_carrier":id_carrier,
            "id_reference":id_carrier, "pointName":pointName}

    prestashop_api.MakeUpdateQueue(sql)

    for j in range(1,4):

        sql = """INSERT INTO ps_carrier_group(id_carrier, id_group)
                VALUES (%(id_carrier)s,%(id_group)s)"""%{"id_carrier":id_carrier,"id_group":j}

        prestashop_api.MakeUpdateQueue(sql)

    delay = u"Срок доставки (дней): "
    minDays = 12

##    shopPostcode = rusPostApi.GetIndex( shopName, u"ленин")
##    if shopPostcode != -1:
##        minDays = rusPostApi.GetDays(shopPostcode) + 2

    delay += str(minDays) + " - " + str(minDays + 3)
    delay += "."

    sql = """INSERT INTO ps_carrier_lang(id_carrier, id_shop, id_lang, delay)
             VALUES (%(id_carrier)s,%(id_shop)s,1,'%(delay)s')"""%{"id_carrier":id_carrier,
             "id_shop":shopId,"delay":delay}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_shop(id_carrier, id_shop)
             VALUES (%(id_carrier)s,%(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_tax_rules_group_shop(id_carrier, id_tax_rules_group, id_shop)
             VALUES (%(id_carrier)s, 0, %(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_zone(id_carrier, id_zone)
             VALUES (%(id_carrier)s, %(zoneId)s)"""%{"id_carrier":id_carrier, "zoneId":zoneId}

    prestashop_api.MakeUpdateQueue(sql)

    id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

    rangesIds = []
    for k in range(0, 21):
        rangesIds.append(id_range_price + k)

    counter = 0

    for id_range_price in rangesIds:
        delimiter1 = 1000 * counter + 1

        if counter == 20:
            delimiter2 = 1000001
        else: delimiter2 = 1000 * (counter + 1)

        sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                 VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                 "id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

        prestashop_api.MakeUpdateQueue(sql)

        if counter == 20:
            delimiter2 = 21000

        id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
        if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
            price = 200
        else:
            price = 250
        price = price + delimiter2 * 0.04

        price = int(10 * round(float(price)/10))

        sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                 VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                 "id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        counter += 1

    logging.LogInfoMessage(str(id_carrier) + " has been added")

def AddPointCarrier():
    pvzNodes = root.xpath("//Pvz[(starts-with(@City, '" + shopName + "') and contains(@City, ',')) or (@City = '" + shopName + "' and not(contains(@City, ',')))]")

    #если нет пунктов - выходим
    if len(pvzNodes) == 0:
        return

    pvzNames = [u"На пункт выдачи \"" + pvzNode.attrib["Name"] + "\" (" + pvzNode.attrib["Address"] + ")." for pvzNode in pvzNodes]
    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    #удаляем неактивные пункты
    if shopCarriersNames != -1:
        for shopCarriersName in shopCarriersNames:
            if shopCarriersName.find(u"На пункт выдачи") != -1:
                try:
                    itemIndex = pvzNames.index(shopCarriersName)
                except:
                    prestashop_api.DeactivateCarrier(shopCarriersName)
                    logging.LogInfoMessage(shopName + " " + shopCarriersName + " has been deactivated")

    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    for i in range(0, len(pvzNodes)):
        pvzNode = pvzNodes[i]
        pvzName = pvzNode.attrib["Name"]
        pointName = u"На пункт выдачи \"" + pvzName + "\" (" + pvzNode.attrib["Address"] + ")."

        bExists = False
        if shopCarriersNames != -1:
            for shopCarriersName in shopCarriersNames:
                if shopCarriersName == pointName:
                    bExists = True
                    break

        if bExists == True:
            continue

        tariffs = [234, 136, 10, 62]
        minCost = 100000
        calcResult = None

        for tariffId in tariffs:
            requestData = {}
            requestData['version'] = "1.0"
            requestData['dateExecute'] = datetime.date.today().strftime('%Y-%m-%d')
            requestData['senderCityId'] = 44
            requestData['receiverCityId'] = int(pvzNodes[0].attrib["CityCode"])
            requestData['tariffId'] = tariffId
            if prestashop_api.siteUrl == "http://saltlamp.su":
                requestData['goods'] = [{'weight':4, 'length':20, 'width':20,'height':20}]
            elif prestashop_api.siteUrl == "http://sushilki.ru.com":
                requestData['goods'] = [{'weight':4, 'length':30, 'width':30,'height':40}]
            elif prestashop_api.siteUrl == "https://incubators.shop":
                requestData['goods'] = [{'weight':5, 'length':50, 'width':40,'height':30}]
            elif prestashop_api.siteUrl == "http://gemlux-shop.ru":
                requestData['goods'] = [{'weight':4, 'length':30, 'width':30,'height':30}]
            else:
                requestData['goods'] = [{'weight':1, 'length':20, 'width':20,'height':20}]
            calcResult_ = sdek.Calculate(requestData)

            if "error" in calcResult_.keys():
                continue

            if int(calcResult_["result"]["price"]) < minCost:
                minCost = int(calcResult_["result"]["price"])
                calcResult = calcResult_
                print tariffId

        id_carrier = prestashop_api.GetLastInsertedId("ps_carrier","id_carrier") + 1

        sql = """INSERT INTO ps_carrier(id_carrier, id_reference, id_tax_rules_group, name, url, active, deleted, shipping_handling, range_behavior, is_module, is_free, shipping_external, need_range, external_module_name, shipping_method, position, max_width, max_height, max_depth, max_weight, grade)
                VALUES (%(id_carrier)s,%(id_reference)s,0,'%(pointName)s','',1,0,0,0,0,0,0,0,'',2,2,0,0,0,0,0)"""%{"id_carrier":id_carrier,
                "id_reference":id_carrier, "pointName":pointName}

        prestashop_api.MakeUpdateQueue(sql)

        for j in range(1,4):

            sql = """INSERT INTO ps_carrier_group(id_carrier, id_group)
                    VALUES (%(id_carrier)s,%(id_group)s)"""%{"id_carrier":id_carrier,"id_group":j}

            prestashop_api.MakeUpdateQueue(sql)

        delay = u"Срок доставки (рабочих дней): "
        if calcResult["result"]["deliveryPeriodMin"] == calcResult["result"]["deliveryPeriodMax"]:
            delay += str(calcResult["result"]["deliveryPeriodMax"] + 1)
        else:
            delay += str(calcResult["result"]["deliveryPeriodMin"] + 1) \
                    + " - " + str(calcResult["result"]["deliveryPeriodMax"] + 1)

        delay += "."

        sql = """INSERT INTO ps_carrier_lang(id_carrier, id_shop, id_lang, delay)
                 VALUES (%(id_carrier)s,%(id_shop)s,1,'%(delay)s')"""%{"id_carrier":id_carrier,
                 "id_shop":shopId,"delay":delay}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_carrier_shop(id_carrier, id_shop)
                 VALUES (%(id_carrier)s,%(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_carrier_tax_rules_group_shop(id_carrier, id_tax_rules_group, id_shop)
                 VALUES (%(id_carrier)s, 0, %(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_carrier_zone(id_carrier, id_zone)
                 VALUES (%(id_carrier)s, %(zoneId)s)"""%{"id_carrier":id_carrier, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

        rangesIds = []
        for k in range(0, 21):
            rangesIds.append(id_range_price + k)

        counter = 0

        for id_range_price in rangesIds:
            delimiter1 = 1000 * counter + 1

            if counter == 20:
                delimiter2 = 1000001
            else: delimiter2 = 1000 * (counter + 1)

            sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                     VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                     "id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

            prestashop_api.MakeUpdateQueue(sql)

            if counter == 20:
                delimiter2 = 21000

            id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
            price = int(calcResult["result"]["price"])
            price = price + delimiter2 * 0.03

            price = int(10 * round(float(price)/10))

            sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                     VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                     "id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

            prestashop_api.MakeUpdateQueue(sql)

            counter += 1

##        filename = str(id_carrier) + ".jpg"
##        shutil.copyfile(u"point.jpg", filename)
##        try:
##            session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
##            session.set_pasv(False)
##            session.cwd("/home/" + site["siteDir"] + "/www/img/s")
##            file = open(filename,'rb')                  # file to send
##            session.storbinary('STOR ' + filename, file)     # send the file
##            file.close()                                    # close file and FTP
##            session.quit()
##            os.remove(filename)
##        except:
##            logging.LogErrorMessage(u"Fail to load image: {0}, id {1}".format(shopName, id_carrier))

        logging.LogInfoMessage(shopName + " " + pointName + " has been added")



############################################################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "pvz")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()

sdek = SdekAPI.SdekAPI()
mails = SendMails.SendMails()

resp = requests.get("https://integration.cdek.ru/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    if prestashop_api.siteUrl in ["http://safetus.ru", "http://omsk.knowall.ru.com"]:
        zoneId = 7
    else:
        zoneId = 9

    logging.Log("==================================  " + site["url"] + "  ==================================")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
##        if shopId < 300:
##            continue
##        print shopDomen
        shopName = commonLibrary.GetShopName(shopName)
##        AddPointCarrier()
        AddCourierCarrier()
##        AddRussianPostCarrier()

    if machineName.find("hp") != -1:
        ssh.kill()


mails.SendInfoMail("Обновление пунктов выдачи", "", [logfile])
