﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import PrestashopAPI
import commonLibrary
import SendMails
import subprocess
import random
import re


def GetDescriptionTemplate():
    templates = [
u"""<p>{Выгодно {купить|приобрести} {0} {1}|Купить {0} {1}{| по {низкой|доступной} цене}} в [city_P] {вы можете|можно}, {оформив|сделав|оставив} заказ {в нашем магазине|в нашем интернет-магазине|в нашем онлайн-магазине|на нашем сайте|в {онлайн-|интернет-}магазине «cname»}.</p>""",
u"""<p>Чтобы {купить|приобрести} {1} в [city_P], достаточно {оформить|сделать} заказ {в нашем магазине|в нашем интернет-магазине|в нашем онлайн-магазине|на нашем сайте|в {онлайн-|интернет-}магазине «cname»}.</p>""",
##u"""<p>{На данной странице|В данном разделе} нашего  {купить|приобрести} {1} в [city_P], достаточно {оформить|сделать} заказ {в нашем магазине|в нашем интернет-магазине|в нашем онлайн-магазине|на нашем сайте|в {онлайн-|интернет-}магазине «cname»}.</p>""",
]
    temp = random.choice(templates)
##    print temp
    return temp

def GetLongDescriptionTemplate():
    templates = [
u"""<p>{Купить {0} в [city_P] можно|{1} в [city_P] можно купить|{Предлагаем вам|Мы предоставляем возможность} купить {0} в [city_P]} с доставкой [{почтой России, |почтой, }|{курьером до двери, |курьером, |до двери, |курьерской службой, }]а также в [city_pr_m_V] {пункты выдачи|точки выдачи}{| товаров| заказов}.</p>""",
u"""<p>Заказать {0} в [city_V] вы можете {у наших операторов|по телефону} или{| самостоятельно} {на сайте|через сайт}, а {получить|забрать} - в {одном из пунктов выдачи|одной из точек доставки}.</p>""",
u"""<p>{Наши покупатели могут|Вы можете|У вас есть возможность} приобрести {0} в [city_P], оплатив {товар|заказ|покупку}{| при получении} в одном из [city_pr_m_R] пунктов {выдачи|доставки}.</p>""",
u"""<p>На все {0} {предоставляется гарантия|дается гарантия|мы предоставляем гарантию} от {{шести|6} месяцев|полугода}. Доставка{| заказов} в [city_V] {возможна|осуществляется}{| любым удобным вам способом -| различными способами -} [{почтой России, |почтой, }|{курьером до двери, |курьером, |до двери, |курьерской службой, }]а также в [city_pr_m_V] {пункты|точки} выдачи.</p>""",
u"""<p>Получить {профессиональную консультацию по приборам|совет по выбору устройства} и {оформить заказ на|заказать} {0} можно {у наших менеджеров|обратившись к консультантам нашего магазина} {по телефону или в онлайн-чате|в онлайн-чате или по email|по телефону или email}. Выдача {заказов|товаров} в [city_P] {производится|осуществляется} по нескольким адресам.</p>""",
u"""<p>Мы {продаем|реализуем|предлагаем} только {проверенные|надежные|хорошо зарекомендовавшие себя} {0}, {которые получили|получившие} от наших [city_pr_m_R] покупателей {{большое количество|множество} положительных отзывов|исключительно высокие оценки}. {Понравившийся|Любой} товар {вы можете|можно} получить при доставке {курьером до двери|курьером|до двери|курьерской службой} или самостоятельно в {пунктах|точках} выдачи.</p>""",
u"""<p>{Заказывая|Покупая|Приобретая} {0} {в нашей компании|в нашем {{|онлайн-|интернет-}магазине}|у нас} вы получаете не только {гарантии качества|высокий уровень сервиса|профессиональное обслуживание}, но и возможность {рассчитаться за|оплатить} {покупку|заказ|товар} {при получении в [city_pr_m_P] {точках|пунктах} выдачи|в {точках|пунктах} выдачи в [city_P]}.</p>""",
u"""<p>Все {предлагаемые к покупке|реализуемые нами} {0} имеют {необходимые сертификаты|сертификаты соответствия} и {длительный срок службы|гарантию от {{шести|6} месяцев|полугода}}. {Интересующий вас|Понравившийся вам|Заказанный вами} товар {мы оперативно доставим|наша курьерская служба может оперативно доставить|будет в течение нескольких дней доставлен} в [city_pr_m_I] {точки|пункты} выдачи.</p>""",
]
    temp = random.choice(templates)
##    print temp
    return temp

def GetMetaDescriptionTemplate(name):
    commonTamplates = [
u"""{У нас вы можете|Предлагаем{| вам}} {недорого|выгодно} {приобрести|купить} {0} с доставкой по [city_D].""",
u"""{У нас вы можете|Предлагаем{| вам}} {приобрести|купить} {0} в [city_P] по {оптимальной|выгодной|доступной} цене."""
    ]
    if prestashop_api.siteUrl == "http://safetus.ru":
        templates = [ u"В {наличии|каталоге|ассортименте} мини камеры, глушилки сотовых, видеоглазки, детекторы \"жучков\" и другие полезные товары."]
    if prestashop_api.siteUrl == "http://otpugiwatel.com":
        templates = [u"Интернет-магазин «cname» - быстро и эффективно избавляем от вредителей!"]
    if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
        templates = [u"Интернет-магазин «cname» - большой выбор эффективных ультразвуковых средств от вредителей в [city_P]."]
    if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
        return u" Интернет-магазин Новалл - знать всё!"
    if prestashop_api.siteUrl == "http://saltlamp.su":
        templates = [u"Интернет-магазин «cname» - изысканная польза для вашего дома!"]
    if prestashop_api.siteUrl == "http://sushilki.ru.com":
        return u" Электросушилки для рыбы, грибов, ягод и других продуктов в " + city + "."
    if prestashop_api.siteUrl == "http://glushilki.ru.com":
        templates = [u"В {продаже имеются|наличии|каталоге представлены|ассортименте} {эффективные и недорогие|качественные и надежные|надежные и недорогие} подавители сотовой связи и глонасс, глушилки gsm и gps."]
    if prestashop_api.siteUrl == "http://insect-killers.ru":
        cinds =[u"ловушки для комаров", u"инсектицидные лампы", u"уничтожители насекомых"]
        random.shuffle(cinds)
        sCinds = ", ".join(cinds)
        templates = [ u"В {продаже имеются|наличии|каталоге представлены|ассортименте имеются} {эффективные и недорогие|качественные и надежные|надежные и недорогие} sCinds.".replace("sCinds", sCinds)]
    if prestashop_api.siteUrl == "http://mini-camera.ru.com":
        templates = [ u"Интернет-магазин «cname» - качественные и недорогие миниатюрные видеокамеры с гарантией."]
    if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
        templates = [u"Интернет-магазин «Отпугиватели грызунов» - избавьтесь от крыс и мышей быстро и эффективно!"]
    if prestashop_api.siteUrl == "http://otpugivateli-krotov.ru":
        templates = [u"{{Интернет|Онлайн}-магазин|Магазин} «Отпугиватели кротов» - средства от кротов, медведок и змей в [city_P]."]
    if prestashop_api.siteUrl == "http://otpugivateli-ptic.ru":
        templates = [u"Интернет-магазин «cname» - эффективные средства от голубей, ворон, сорок, воробьев и других птиц."]
    if prestashop_api.siteUrl == "http://otpugivateli-sobak.ru":
        templates = [u"Интернет-магазин «Отпугиватели собак» - Когда собаки под контролем."]
    if prestashop_api.siteUrl == "http://otpugivatel.spb.ru":
        cinds = [u"грызунов", u"птиц", u"кротов", u"тараканов", u"комаров", u"собак", u"змей"]
        for cind in cinds:
            if name.lower().find(cind) != -1:
                cinds.remove(cind)
        random.shuffle(cinds)
        cinds = cinds[0:3]
        sCinds = ", ".join(cinds)
        templates = [u"В {продаже также имеются|наличии также|каталоге также представлены|ассортименте также имеются} {эффективные и недорогие|качественные и надежные|надежные и недорогие} отпугиватели [0] и других вредителей.".replace("[0]", sCinds)]
    if prestashop_api.siteUrl == "http://incubators.shop":
        cinds = [u"гусиные", u"утиные", u"куриные", u"перепелиные"]
        also = ""
        for cind in cinds:
            if name.lower().find(cind) != -1:
                cinds.remove(cind)
                also = u" также"
        random.shuffle(cinds)
        sCinds = ", ".join(cinds)
        templates = [u"В {продаже{1} имеются|наличии{1}|нашем каталоге{1} представлены|ассортименте} {качественные и недорогие|качественные и надежные|надежные и недорогие} {0} инкубаторы с высокой выводимостью.".replace("{0}", sCinds).replace("{1}", also)]
    if prestashop_api.siteUrl == "http://usiliteli-svyazi.ru":
        templates = [u"Интернет-магазин «Усилители связи» - Отличная связь всегда!"]
    if prestashop_api.siteUrl == "http://gemlux-shop.ru":
        cinds = [x[1].lower() for x in prestashop_api.GetShopActiveCategories(shopId)]
        for cind in cinds:
            if name.lower().find(cind) != -1 or cind.lower() in [u"другие товары", u"кухонная посуда"]:
                cinds.remove(cind)
        random.shuffle(cinds)
        cinds = cinds[0:3]
        sCinds = ", ".join(cinds)
        templates = [u"Также в {продаже имеются|наличии|нашем каталоге представлены|ассортименте} {качественные и недорогие|качественные и надежные|надежные и недорогие} {0} и {другая продукция|другие товары|другое оборудование} {производства компании|торговой марки|под брендом|фирмы} {«Гемлюкс»|«Gemlux»}.".replace("{0}", sCinds)]

    temp = random.choice(commonTamplates) + u" " + random.choice(templates)

    return  temp


def SetDescription():
    template = u"""<p>GSM-сигнализации используются для обеспечения безопасности на объектах посредством сотовой связи или Wi-Fi. Cигнализации c GSM-модулем охраняют частные дома, предприятия, производственные помещения. Их задача — определить факт вторжения на контролируемую территорию и оповестить об этом владельцев при помощи SMS-сообщения.</p>"""
    name = prestashop_api.GetCategoryName(categoryId)

    newDescription = commonLibrary.GetVariantFromTemplate(shopName, template)
    if newDescription == False:
        print u"ошибка шаблона"
        sys.exit(0)

    gender = 2
    name = commonLibrary.GetSingleForm(name)
    name = commonLibrary.Dowmcase(name)
    if gender == 1:
        part = u"{качественный и недорогой|качественный и надежный|надежный и недорогой}"
    elif gender == 2:
        part = u"{качественную и недорогую|качественную и надежную|надежную и недорогую}"
##        part = u"{качественную и недорогую}"
    elif gender == 3:
        part = u"{качественные и недорогие|качественные и надежные|надежные и недорогие}"
    else:
        part = u"{качественное и недорогое|качественное и надежное|надежное и недорогое}"
    template = GetDescriptionTemplate().replace("{0}", part).replace("{1}", name).replace("cname", commonLibrary.GetCompanyName(prestashop_api.siteUrl))
    newDescription += commonLibrary.GetVariantFromTemplate(shopName, template)
##    print newDescription
##    print "description " + str(categoryId)
    prestashop_api.SetShopCategoryDescription(shopId, categoryId, newDescription)

def SetLongDescription():
    name = prestashop_api.GetCategoryName(categoryId)
    downcasedName = commonLibrary.Dowmcase(name)
    uppercasedName = commonLibrary.Uppercase(name)
    if shopName in (commonLibrary.mainCities1 + commonLibrary.mainCities2):
        template = u"""<p>Фотоловушки разработаны для ведения съемки в сложных условиях. Автономная камера работает от батареек. Есть модели со встроенной солнечной батареей.</p>
<p>Устройство в герметичном корпусе обычно бывает окрашено в неброский, маскирующий цвет, что делает его невидимым на фоне природных объектов. Класс защиты IP54-IP66 обеспечивает надежную работу в условиях температурных перепадов, низких температур, 100 % влажности воздуха. </p>
<p>Конструкция предполагает наличие широкоугольного объектива, фотоматрицы, карты памяти, электронных блоков обработки данных. На каждой фотографии обязательно отображаются метаданные, включающие время и дату съемки. Съемка местности ведется автоматическим способом, после срабатывания встроенного датчика движения. Запись видео осуществляется со звуком. Готовые файлы представляют собой короткие видеоролики в разрешении Full HD. Длина видеороликов задается заранее.</p>
<p>Невидимая для животных ночная подсветка от ИК модуля позволяет вести круглосуточную съемку, даже в полной темноте. Дальность обнаружения движущегося в темноте объекта достигает 30 м. Поддержка 4G/LTE обеспечивает скоростную отправку данных на сервер или другое устройство. В виде MMS фотографии приходят на мобильный телефон.</p>
<p>Благодаря ЖК-дисплею можно быстро посмотреть данные, сохраненные на карте памяти, прямо на устройстве. Модуль microSD позволяет использовать внешние карты памяти разного объема.</p>"""

    else:
        return
##        template = u""""""
    newDescription = commonLibrary.GetVariantFromTemplate(shopName, template)
##    newDescription = DeletePointsInfo(newDescription, downcasedName)

    h2 = random.choice( [u"<h2>Купить {0} в [city_P]</h2>".format(commonLibrary.GetSingleForm(downcasedName)),
          u"<h2>{0} купить в [city_P]</h2>".format(commonLibrary.GetSingleForm(uppercasedName))])

    newDescription = commonLibrary.GetVariantFromTemplate(shopName, h2) + newDescription
    if newDescription == False:
        print u"ошибка шаблона"
        sys.exit(0)

    products = prestashop_api.GetCategoryActiveProductsIds(categoryId)
    random.shuffle(products)
    count = len(products)
    if len(products) > 3:
        count = 3
    if len(products) != 0:
        if prestashop_api.siteUrl not in ["http://gemlux-shop.ru"]:
            sProducts = ", ".join([u"\"{0}\"".format(commonLibrary.CleanItemName(prestashop_api.GetProductOriginalName(x))) for x in products[0:count]])
        else:
            sProducts = ", ".join([u"\"{0}\"".format(commonLibrary.CleanItemName(prestashop_api.GetProductName(x))) for x in products[0:count]])
        newDescription += commonLibrary.GetVariantFromTemplate(shopName, u"""<p>В нашем {каталоге|ассортименте|магазине|онлайн-магазине|интернет-магазине} {представлены|присутствуют|вы можете найти|есть|можно найти} такие {популярные|проверенные|надежные} модели, как %s и{| многие} другие.<p>""" % sProducts)
    newDescription += commonLibrary.GetVariantFromTemplate(shopName, GetLongDescriptionTemplate().replace("{0}", downcasedName).replace("{1}",name))
##    print newDescription
##    print "long_description " + str(categoryId)
    prestashop_api.SetShopCategoryLongDescription(shopId, categoryId, newDescription)


def SetMetatags():
    name = prestashop_api.GetCategoryName(categoryId)
    sfName = commonLibrary.ChangeCase(commonLibrary.GetSingleForm(name))
    dowmcasedName = commonLibrary.Dowmcase(sfName)
    metaTitles = [
    u"Купить {} в [city_P]".format(dowmcasedName),
    u"{} купить в [city_P]".format(commonLibrary.GetSingleForm(name))
    ]
    link_rewrite = prestashop_api.Transliterate(prestashop_api.GetCategoryLinkRewrite(categoryId))
    meta_title = commonLibrary.GetVariantFromTemplate(shopName, random.choice(metaTitles))
    if len(meta_title) + 17 < 70:
        meta_title += commonLibrary.GetVariantFromTemplate(shopName, u" по {низкой|доступной|выгодной} цене")
    elif len(meta_title) + 8 < 70:
        word = random.choice([u"недорого", u"дешево"])
        meta_title += u" {0}".format(word)
    meta_description = commonLibrary.GetVariantFromTemplate(shopName, GetMetaDescriptionTemplate(name).replace("{0}",dowmcasedName).replace("cname", commonLibrary.GetCompanyName(prestashop_api.siteUrl)))
    meta_keywords = commonLibrary.GetVariantFromTemplate(shopName, u"{0} купить цена в [city_P]".format(sfName))

    params = [link_rewrite, meta_title, meta_keywords, meta_description]
##    for param in params:
##        print param

    prestashop_api.SetCategoryData(shopId, categoryId, params)

#############################################################################################################
machineName = commonLibrary.GetMachineName()

logfile = log.GetLogFileName("log", "updateCategories")
logging = log.Log(logfile)
mails = SendMails.SendMails()

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    logging.Log("============  " + site["url"] + "  ================")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        shopName = commonLibrary.GetShopName(shopName)
##        if shopId not in [180]:
##            continue
##        if shopId < 104:
##            continue
##        print shopId, shopName
        logging.Log(str(shopId))

        cats_ = prestashop_api.GetShopActiveCategories(shopId)
        cats = [x[0] for x in cats_]
        for cat in cats:
            categoryId = cat
            if categoryId not in [29]:
                continue
            if shopId == 1:
                prestashop_api.SetCategoryUpdateDate(categoryId, prestashop_api.GetTodayDate())

            SetDescription()
            SetLongDescription()
            SetMetatags()


    if machineName.find("hp") != -1 or machineName.find("pitekantrop") != -1 :
        ssh.kill()


