﻿import PrestashopAPI
import ContentmonsterApi
import SendMails
import log
import commonLibrary
import TextruAPI
import lxml
from lxml import etree
import subprocess
import datetime

template = u"""Здравствуйте!

Нужны описания следующих категории товаров:

[cats]

Для каждой категории необходимы 2 текста:
- 3-4 предложения для размещения над списком товаров
- текст 800-1100 символов для размещения под списком товаров

Общие требования:
- Призывы к действию и общую информацию о магазине включать не нужно
- В готовом тексте названия категорий нужно выделять заголовком h2
- Уникальность 100% в сервисе http://text.ru/antiplagiat .

По окончанию работы прошу прикладывать скриншоты с результатами проверок.

С уважением,
Иван"""

def CorrectLongDescr(text):
    tree = etree.HTML(text)
    elements = tree.xpath("//h2")
    for element in elements:
        element.getparent().remove(element)
    return commonLibrary.GetTextFromHtml(commonLibrary.GetStringDescription(tree))

def GetOrderDescription(cityId, categories):
    catsDescr = u""
    for catId in categories:
        catName = prestashop_api.GetCategoryName(catId)
        catUrl = prestashop_api.GetShopMainDomain(cityId) + "/" + prestashop_api.GetCategoryUrl(catId, cityId)
        catsDescr += u"{0} ( {1} )\n".format(catName, catUrl)

    descr = template.replace("[cats]", catsDescr)
    return descr

def GetNextCity(categories, cityName):
    categoryId = [x.strip() for x in categories.split(",")][0]
    shops = prestashop_api.GetAllShops()
    mainShopsIds = []

    for i in range(0,len( shops)):
        shop = shops[i]
        shopId, shopDomen, shopName = shop
        shopName = commonLibrary.GetShopName(shopName)

        if shopName in commonLibrary.mainCities1:
            mainShopsIds.append(shopId)

    for i in range(0,len( mainShopsIds)):
        shopId = mainShopsIds[i]
        longDescr = prestashop_api.GetShopCategoryLongDescription(shopId, categoryId)
        longDescr = CorrectLongDescr(longDescr) [0:100]
        for k in range(0,len( mainShopsIds)):
            if k <= i:
                continue
            shopId1 = mainShopsIds[k]
            longDescr1 = prestashop_api.GetShopCategoryLongDescription(shopId1, categoryId)
            longDescr1 = CorrectLongDescr(longDescr1) [0:100]
            if longDescr == longDescr1:
                if shopName == cityName:#заказ с таким городом уже в работе
                    continue
                return [shopId, prestashop_api.GetShopName(shopId)]
            else:
                break

    return -1

def GetTexts(source):
    titleTag = "h2"
    texts = []
    tree = etree.HTML(source)
    titles = tree.xpath("//{0}".format(titleTag))

    for title in titles:
        text = u""
        title = title.getnext()
        bContinue = True
        while bContinue:
            text += etree.tostring(title)
            title = title.getnext()
            if title == None or title.tag == titleTag:
                bContinue = False

        text = commonLibrary.GetTextFromHtml(text)
        texts.append(text)

    return texts

def GetDataForOrder(cityId, cityName, categories):
    cats = [x.strip() for x in categories.split(",")]
    data = {}
    data["site_id"] = contentmonster_api.GetSiteId(prestashop_api.siteUrl)
    data["task"] = 1
    data["subject_id"] = 30
    data["write_time_limit"] = 24 + len(cats) * 5
    data["len_min"] = len(cats) * 1100
    data["len_max"] = len(cats) * 1400
    data["write_pay"] = 80
    data["wmtype"] = 1
    data["uniq_min"] = 100
    data["name"] = u"Описания категорий товаров ({0})".format(cityName)
    data["description"] = GetOrderDescription(cityId, cats)
    data["tender_type"] = 2
    data["tender_group"] = 32050
    data["send"] = 1
    return data


###############################################################################

contentmonster_api = ContentmonsterApi.ContentmonsterApi()
prestashop_api = PrestashopAPI.PrestashopAPI()
textru_api = TextruAPI.TextruAPI()
mails = SendMails.SendMails()
todayDate = datetime.date.today()

orders = prestashop_api.GetContentmonsterCategoriesOrders()
message = ""

#обновление статусов существующих заказов
for order in orders:
    id, site, cityId, cityName, categories, orderId, text, state, uniqueness, date = order
    if orderId in [None, ""]:
        continue
    if state in [u"проверен", u"добавлен", u"закончен"]:
        continue
    orderState = contentmonster_api.GetOrderStatus(orderId)
    if state != orderState:
        prestashop_api.SetContentmonsterOrderState(orderId, orderState)
        message += "Изменен статус заказа {0} на '{1}'\n".format(orderId, orderState.encode("utf-8"))
        if orderState == u"закончен":
            orderText = contentmonster_api.GetOrderText(orderId)
            prestashop_api.SetContentmonsterOrderText(orderId, orderText)
            message += "Добавлен текст заказа {0}\n".format(orderId)

#проверка уникальности существующих заказов
orders = prestashop_api.GetContentmonsterCategoriesOrders()
for order in orders:
    id, site, cityId, cityName, categories, orderId, text, state, uniqueness, date = order
    if state == u"добавлен" and uniqueness in [-1, 100]:
        if (todayDate - date).days < 3:
            continue
        texts = commonLibrary.GetContentmonsterCategoriesTexts(text)
        for text_ in texts:
            uniqueness_ = textru_api.GetTextUniqueness(text_)
            if int(float( uniqueness_)) != 100:
                prestashop_api.SetContentmonsterOrderUniqueness(orderId, uniqueness_)
                message += "Изменена уникальность по заказу {0} - {1}%\n".format(orderId,uniqueness_ )
                break

#создание новых заказов
orders = prestashop_api.GetContentmonsterCategoriesOrders()
for order in orders:
    id, site, cityId, cityName, categories, orderId, text, state, uniqueness, date = order
    numOfSiteOrders = len([x for x in orders if x[1] == site])
    if orderId in [None, ""] or (uniqueness >= 0 and uniqueness < 100 and numOfSiteOrders == 1):#если новый домен или нужно добавить заказ для следующего города
        prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site)
    if orderId in [None, ""]:#если новый домен
        data = GetDataForOrder(cityId, cityName, categories)
        orderId = contentmonster_api.CreateOrder(data)
        if orderId != -1:
            prestashop_api.SetContentmonsterOrderId(id, orderId)
            message += "Создан заказ {0} по записи {1}\n".format(orderId, id)
        else:
            message += "[Error] Ошибка создания заказа по записи {0}\n".format(id )
    else:#если нужно добавить заказ для следующего города
        if uniqueness >= 0 and uniqueness < 100 and numOfSiteOrders == 1:
            result = GetNextCity(categories, cityName)
            if result != -1:
                cityId, shopName = result
                data = GetDataForOrder(cityId, shopName, categories)
                orderid = contentmonster_api.CreateOrder(data)
                if orderid != -1:
                    data = {}
                    data["site"] = site
                    data["cityId"] = cityId
                    data["cityName"] = shopName
                    data["orderId"] = orderid
                    data["categories"] = categories
                    prestashop_api.AddContentmonsterOrder(data)
                    message += "Создан заказ для города {0} по записи {1}\n".format(shopName.encode("utf-8"), id)
                else:
                    message += "[Error] Ошибка создания заказа по записи {0}\n".format(id )

            prestashop_api.DeleteContentmonsterOrder(orderId)

    if 'ssh' in locals():
        ssh.kill()


if message != "":
    result = mails.SendInfoMail("Обработка contentmonster", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
