﻿import urllib
import log
import datetime
import PrestashopAPI
import os
import re, urlparse
from selenium import webdriver
import myLibrary
import SendMails
import SdekAPI
import time
import commonLibrary
import sys
import subprocess
from lxml import etree

def GetRefundParams(fileName):
    paramsArr = []
    content = etree.HTML(open(fileName).read())
    statementCode = content.xpath("//table[1]//tr//td[1]")[0].text.split(" ")[1].replace(u"№", "")
    rows = content.xpath("//table[2]//tr[not(.//th)]")
    for i in range(1, len(rows)):
        columns = content.xpath("//table[2]//tr[{0}]/td".format(i + 1))
        params = {}
        try:
            invoice = columns[0].text
            if invoice in ["", None]:
                paramsArr[-1]["items"] = paramsArr[-1]["items"] + ", " + commonLibrary.CleanItemName(columns[12].text.strip())
            else:
                if i > 2:
                    statementCode += "_{0}".format(i-2)
                params["invoice"] = invoice
                params["statementCode"] = statementCode
                params["items"] =  commonLibrary.CleanItemName(columns[12].text.strip())
                paramsArr.append(params)
        except:
            pass

    return paramsArr

################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processRefunds")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()

today = datetime.date.today()
now = datetime.datetime.now()

message = ""
paramsArr = []

sdek = SdekAPI.SdekAPI()
mails = SendMails.SendMails()
knowallParams = {"FromAddress" : u"feedback@knowall.ru.com", "Password" : "tabulfsam198"}

prestashop_api = PrestashopAPI.PrestashopAPI()

if now.weekday() == 0:

    emails = mails.GetMails(knowallParams, u"Письмо сопровождение возвратной ведомости")
    if len(emails) != 0:
        filesNames = mails.GetMailsAttachments(emails, "refunds")
        for fileName in filesNames:
            params = GetRefundParams("refunds\\" + fileName)
            paramsArr += params

    paramsArrTmp = paramsArr
    for site in commonLibrary.sitesParams:
        logging.Log("=========  " + site["url"] + "  =========")

        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            try:
                ssh.kill()
            except:
                continue

        bTitle = False

        for params in paramsArr:
            if prestashop_api.GetIsRefundProcessed(params["statementCode"]):
                continue
            orderId = prestashop_api.GetOrderIdByTrackingNumber(params["invoice"])
            if orderId != -1:
                if bTitle == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    bTitle = True
                prestashop_api.SetOrderState(orderId, u"Отклонен")
                message += "Статус заказа {0} изменен на 'Отклонен'\n".format(orderId)
                logging.Log(u"Статус заказа {0} изменен на 'Отклонен'".format(orderId))
                items = u""
                orderItems = prestashop_api.GetOrderItems(orderId)
                products = u", ".join([u"{0} ({1} шт.)".format(prestashop_api.CleanItemName(prestashop_api.GetProductOriginalName(x[0])), x[3]) for x in orderItems])
                message += "в заказе: {0}\n".format(products.encode("utf-8"))
                message += "в ведомости: {0}\n".format(params["items"].encode("utf-8"))
                for orderItem in orderItems:
                    name = prestashop_api.CleanItemName(prestashop_api.GetProductOriginalName(orderItem[0]))
                    items += u"{0}_{1}/".format(name, orderItem[3])
                items = items.strip("/")
                params["items"] = items
                params["date"] = today
                prestashop_api.AddRefund(params)
                paramsArrTmp.pop(paramsArrTmp.index(params))

        if machineName.find("hp") != -1:
            ssh.kill()

    for params in paramsArrTmp:
        params["date"] = today
        prestashop_api.AddRefund(params)

################################################################################

if now.weekday() == 1:
    date = datetime.date.today() - datetime.timedelta(days=2)
    refunds = prestashop_api.GetRefundsParams(date)
    for refund in refunds:
        statementCode = refund[1]
        if refund[7] == 1:
            continue

        #рассылаем уведомления
        emails = mails.GetMails(knowallParams, statementCode)
        if len(emails) == 0:
            continue
        if refund[3] in [u"Москва", "", None]:
            text = u"Прошу вернуть товар на склад фулфилмента (пос. Малаховка, Касимовское шоссе, 3Б литера О)"
        else:
            text = u"Прошу вернуть товар на пункт выдачи по адресу: г. Санкт-Петербург, Лиговский пр., д.50, корп.13.."
        result = mails.ReplyToEmail(knowallParams, emails[0], text)
        if result:
            prestashop_api.SetRefundReplySended(statementCode, 1)
            message += "Отправлено письмо на возврат для ведомости '{0}'\n".format(statementCode.encode("utf-8"))
            logging.Log(u"Отправлено письмо на возврат для ведомости '{0}'".format(statementCode))
        else:
            message += "[Error] Ошибка при отправке письма на возврат для ведомости '{0}'\n".format(statementCode.encode("utf-8"))
            logging.LogErrorMessage(u"Ошибка при отправке письма на возврат для ведомости '{0}'".format(statementCode))


################################################################################

refundsToPosting = []

if now.weekday() not in [0,1]:
    date = datetime.date.today() - datetime.timedelta(days=120)
    refunds = prestashop_api.GetRefundsParams(date)
    for refund in refunds:
        date, statementCode, items, toCity, invoice, returnInvoice, currentState, replySended, postingId  = refund
        if returnInvoice in ["", None]:
            returnInvoice = sdek.GetOrderReturnInvoiceNumber(refund[4])
            if returnInvoice == -1:
                continue
            else:
                prestashop_api.SetRefundReturnInvoice(statementCode, returnInvoice)
                if postingId == None:
                    #создаем заявку на приемку
                    itemsData = []
                    try:
                        itemsNames, itemsQuantities = commonLibrary.ParseItems(items)
                    except:
                        message += "[Error] Не указано количество товара для ведомости '{0}'\n".format(statementCode.encode("utf-8"))
                        logging.LogErrorMessage(u"Не указано количество товара для ведомости '{0}'".format(statementCode))
                        continue
                    for i in range(0,len(itemsNames)):
                        itemName = itemsNames[i]
                        itemQuantity = itemsQuantities[i]
                        shortItemName = prestashop_api.CleanItemName(itemName)
                        SkuId = prestashop_api.GetSkuIdByItemName(shortItemName)
                        if SkuId == -1 and toCity in [u"Москва", "", None]:
                            data = {"Article": commonLibrary.GetVendorCode(), "Name":shortItemName}
                            SkuId = sdek.UpdateProduct(data)
                            if SkuId != -1:
                                message += "Товар '{0}' добавлен в справочник с артикулом {1}\n".format(shortItemName.encode("utf-8"), SkuId)
                                logging.Log(u"Товар '{0}' добавлен в справочник с артикулом {1}".format(shortItemName, SkuId))
                                data["Sku_id"] = SkuId
                                commonLibrary.AddProductToWarehouse("warehouse_moscow", data)
                            else:
                                message += "[Error] Ошибка при добавлении товара '{0}' в справочник\n".format(shortItemName.encode("utf-8"))
                                logging.LogErrorMessage(u"Ошибка при добавлении товара '{0}' в справочник".format(shortItemName))
                                break
                        itemsData.append({"SkuId":SkuId, "Amount":itemQuantity})

                    result = sdek.CreatePostingApi(itemsData, returnInvoice)
                    if result != -1:
                        message += "Создана заявка на приемку {0} по возвратной ведомости {1}\n".format(result, statementCode.encode("utf-8"))
                        logging.Log(u"Создана заявка на приемку {0} по возвратной ведомости {1}".format(result, statementCode))
                        prestashop_api.SetRefundPostingId(statementCode, result)
                    else:
                        message += "[Error] Ошибка при cоздании заявки на приемку по возвратной ведомости {1}\n".format(statementCode.encode("utf-8"))
                        logging.LogErrorMessage(u"Ошибка при cоздании заявки на приемку по возвратной ведомости {1}".format(statementCode))


        if currentState != u"Вручен":
            state = sdek.GetOrderState(returnInvoice, True)
            state = commonLibrary.GetInvoiceState(state)
            if state == -1:
                continue

            if currentState != state:
                prestashop_api.SetRefundState(statementCode, state)
                message += "Ведомость '{0}': изменен статус возвратной накладной на '{1}'\n".format(statementCode.encode("utf-8"), state.encode("utf-8"))
                logging.Log(u"Ведомость '{0}': изменен статус возвратной накладной на '{1}'".format(statementCode, state))

            currentState = state

        if currentState == u"Вручен":
            if toCity not in [u"Москва", "", None]:
                message += "\nПриняты возврат на склад в Спб:\n"
                deliveryItemsNames, deliveryItemsQuantities = commonLibrary.ParseItems(items)
                for i in range(0, len(deliveryItemsNames)):
                    itemName = deliveryItemsNames[i]
                    itemQty = int(deliveryItemsQuantities[i])
                    if prestashop_api.GetWarehouseProductQuantity("warehouse_spb", itemName) == -1:
                        commonLibrary.AddProductToWarehouse("warehouse_spb", itemName)
                    prestashop_api.IncreaseWarehouseProductQuantity("warehouse_spb", itemName, itemQty)
                    actualProductQty = prestashop_api.GetWarehouseProductQuantity("warehouse_spb", itemName)
                    message += ("{0} - {1} шт. (остаток на складе {2} шт.)\n".format(itemName.encode("utf-8"),itemQty, actualProductQty))
                    logging.Log(u"{0} - {1} шт. (остаток на складе {2} шт.)\n".format(itemName,itemQty, actualProductQty))
                prestashop_api.DeleteRefund(statementCode)
            else:
                postingItems = sdek.GetPosting(postingId, invoice)
                if len(postingItems) != 0:
                    message += "Ведомость '{0}': приняты на склад:\n".format(statementCode.encode("utf-8"))
                    logging.Log(u"Ведомость '{0}': приняты на склад:".format(statementCode))
                    bErrors = False
                    bPostingExist = False
                    postingItemsNames = []
                    postingItemsQuantities = []
                    for postingItem in postingItems:
                        productItemName = prestashop_api.GetItemNameBySkuId(postingItem[0])
                        productQty = int(float(postingItem[1]))
                        prestashop_api.IncreaseWarehouseProductQuantity("warehouse_moscow", productItemName, productQty)
                        actualProductQty = prestashop_api.GetWarehouseProductQuantity("warehouse_moscow", productItemName)
                        postingItemsNames.append(productItemName)
                        postingItemsQuantities.append(productQty)
                        message += "{0} (артикул {1}) - {2} шт. (остаток на складе {3} шт.)\n".format(productItemName.encode("utf-8"), postingItem[0],productQty, actualProductQty)
                        logging.Log(u"{0} (артикул {1}) - {2} шт. (остаток на складе {3} шт.)\n".format(productItemName, postingItem[0],productQty, actualProductQty))
                        try:
                            deliveryItemsNames, deliveryItemsQuantities = commonLibrary.ParseItems(items)
                        except:
                            message += "Ведомость '{0}': ошибка парсинга товаров\n".format(statementCode.encode("utf-8"))
                            logging.LogErrorMessage(u"Ведомость '{0}': ошибка парсинга товаров:".format(statementCode))
                            continue
                        for i in range(0, len(deliveryItemsNames)):
                            itemName = deliveryItemsNames[i]
                            itemQty = deliveryItemsQuantities[i]
                            index = -1
                            try:
                                index = postingItemsNames.index(itemName)
                            except:
                                pass
                            if index == -1:
                                message += "[Error] Ведомость '{2}': отсутствует товар '{0}' в приемке №{1}\n".format(itemName.encode("utf-8"), postingId, statementCode.encode("utf-8"))
                                logging.LogErrorMessage(u"Ведомость '{2}': отсутствует товар '{0}' в приемке №{1}".format(itemName, postingId, statementCode))
                                bErrors = True
                                continue
                            if int(postingItemsQuantities[index]) != int(itemQty):
                                message += "[Error] Ведомость '{4}': не совпадает количество товара '{0}': принято {1}, должно быть {2} (приемка №{3})\n".format(itemName.encode("utf-8"), postingItemsQuantities[index], itemQty, postingId,statementCode.encode("utf-8"))
                                logging.LogErrorMessage(u"Ведомость '{4}': не совпадает количество товара '{0}': принято {1}, должно быть {2} (приемка №{3})".format(itemName, postingItemsQuantities[index], itemQty, postingId,statementCode))
                                bErrors = True
                            postingItemsNames.pop(index)
                            postingItemsQuantities.pop(index)

                        items = ""
                        for i in range(0, len(postingItemsNames)):
                            itemName = postingItemsNames[i]
                            itemQty = postingItemsQuantities[i]
                            items += u"{0} ({1} шт.), ".format(itemName, itemQty)
                        if items != "":
                            message += "[Info] Дополнительно приняты по приемке {0} товары: {1}\n".format(posting[1], items.encode("utf-8"))
                            logging.LogInfoMessage(u"Дополнительно приняты по приемке {0} товары: {1}\n".format(posting[1], items))

                        if bErrors == False:
                            prestashop_api.DeleteRefund(statementCode)
                            message += "Ведомость '{0}': товары успешно оприходованы\n".format(statementCode.encode("utf-8"))
                            logging.LogInfoMessage(u"Ведомость '{0}': товары успешно оприходованы".format(statementCode))

                else:
                    refundsToPosting.append(returnInvoice)

    if len(refundsToPosting) > 4 and now.weekday() not in [5, 6] :
        invoicesStr = ", ".join(refundsToPosting)
        mes = "Здравствуйте!\n\nПримите, пожалуйста, возвраты по накладным {0}..\n\nС уважением,\nИван".format(invoicesStr)
        result = mails.SendMailToPartner("support@cowms.ru", "возвраты", mes)
        if result != True:
            logging.LogErrorMessage(u"Ошибка {0} при отправке письма Поповой Елизавете".format(result.decode("cp1251")))
            message += "[Error] Ошибка {0} при отправке письма Поповой Елизавете\n".format(str(result))
        else:
            logging.LogInfoMessage(u"Отправлено письмо на склад о приемке возвратов {0}".format(invoicesStr))
            message += "Отправлено письмо на склад о приемке возвратов {0}\n".format(invoicesStr)



if message != "":
    mails.SendInfoMail("Обработка возвратов СДЭК", message)

time.sleep(50)
