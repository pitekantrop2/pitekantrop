﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import html2text
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess
import SendMails


def UpdateProduct(name, price):
    sql = u"""UPDATE products
             SET wholesale_price = {0}
             WHERE name = '{1}'""".format(price, name)
    prestashop_api.MakeLocalDbUpdateQueue(sql)

def AddProduct(name, price, contractor):
    sql = u"""INSERT INTO products(name, wholesale_price, contractor)
             VALUES ('{0}',{1}, '{2}')""".format(name, price, contractor)
    prestashop_api.MakeLocalDbUpdateQueue(sql)

#########################################################################################################################

mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()

sql = """SELECT * FROM products"""
products = prestashop_api.MakeLocalDbGetInfoQueue(sql)
productsNames = [x[1] for x in products]

updatedProducts = []

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    shopProducts = prestashop_api.GetShopProducts(10)

    for product in shopProducts:
        productId = product[0]
        name = prestashop_api.CleanItemName(prestashop_api.GetProductOriginalName(productId))
        if name not in productsNames:
            contractor = commonLibrary.GetContractorName(prestashop_api.GetProductArticle(productId))
            productCost = prestashop_api.GetProductDetailsById(productId)[2]
            AddProduct(name, productCost, contractor)

    if machineName.find("hp") != -1:
        ssh.kill()



