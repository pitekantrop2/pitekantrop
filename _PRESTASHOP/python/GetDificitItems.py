﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary


states = [
u"Данного товара нет на складе"]

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "deficitItems")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

items = []

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=45)

message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)
    bOrdersExists = False

    for order in orders:
        if bOrdersExists == False:
            message += ("\n=========  " + site["url"] + "  =========\n\n")
            logging.Log("=========  " + site["url"] + "  =========")
            bOrdersExists = True

        orderId = order[0]
        orderItems = prestashop_api.GetOrderItems(orderId)

        mes = u"Заказ " + str(orderId)
        logging.LogInfoMessage(mes)
        message += "Заказ " + str(orderId) + "\n"

        for orderItem in orderItems:
            productName = orderItem[1]
            productOriginalName = prestashop_api.GetProductOriginalName(orderItem[0])
            if productOriginalName != "":
                productName = productOriginalName

            product_quantity = orderItem[3]

            productName = commonLibrary.CleanItemName(productName)

            itemIndex = -1

            try:
                itemIndex = [x[0] for x in items].index(productName)
            except:
                pass

            if itemIndex != -1:
                items[itemIndex][1] += product_quantity
            else:
                itemTuple = [productName, product_quantity]
                items.append(itemTuple)

            logging.LogInfoMessage(productName + " - " + str(product_quantity) + u" шт.")
            message += productName.encode("utf-8") + " - " + str(product_quantity) + " шт.\n"

        message += "\n"

    if machineName.find("hp") != -1:
        ssh.kill()


message += ("====================================================================\n\n")
logging.Log("====================================================================")

for item in items:
    message += (item[0].encode("utf-8") + ": " + str(item[1]) + " шт.\n")
    logging.LogInfoMessage(item[0] + " - " + str(item[1]) + u" шт.")

if bOrdersExists == True:
    result = mails.SendInfoMail("Список отсутствующих товаров", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(50)
