﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess
import SendMails

pointSource = u"""
<p style="text-align: left;"><span style="text-decoration:underline;">{name}</span></p>
<p>Адрес: {address}</p>
<p>Телефоны: {phones}</p>
<p>Время работы: {hours}</p>
"""

def GetPageSource(shopName):
    pvzNodes = root.xpath("//Pvz[(starts-with(@City, '" + shopName + "') and contains(@City, ',')) or (@City = '" + shopName + "' and not(contains(@City, ',')))]")

    #формирование страницы
    if len(pvzNodes) != 0:

        pointsSource = ""

        for i in range(0, len(pvzNodes)):

            pvzNode = pvzNodes[i]
            pointSource_tmp = pointSource
            pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
            pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
            pointSource_tmp = pointSource_tmp.replace("{phones}", pvzNode.attrib["Phone"])
            pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])

            if i != len(pvzNodes) - 1:
                pointSource_tmp += "<p><br/></p>"

            pointsSource += pointSource_tmp

    else:
        return -1

    return pointsSource


#########################################################################################################################


logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "companyPages")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

resp = requests.get("https://integration.cdek.ru/pvzlist.php")

pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        shopName = commonLibrary.GetShopName(shopName)

        cityName = commonLibrary.GetCityDeclension(shopName)[5]
        points = GetPageSource(shopName)

        if points == -1:
            continue

        link = u"delivery-" + shopName.replace(" ", "-")
        content = prestashop_api.GetShopPageContent(shopId, link)

        if content == -1 or cityName == -1:
            continue

        ind1 = content.find(u"""<h3><a name="pvz">""")
        ind2 = content.find(u"""<h3><a name="courier">""")
        newContent = commonLibrary.GetVariantFromTemplate(shopName, u"""<h3><a name="pvz"></a>Пункты выдачи заказов в [city_P]</h3>""")

        if ind1 == -1 or ind2 == -1:
            logging.LogErrorMessage(u"Unable to update page for {0}".format(shopName))
            continue

        newContent += points
        newContent += "<p><br /><br /></p>"
        content = content.replace(content[ind1:ind2], newContent)

        prestashop_api.SetShopPageContent(shopId, link, content)

        logging.LogInfoMessage(u"Page for {0} has been updated".format(shopName))

    if machineName.find("hp") != -1:
        ssh.kill()


mails.SendInfoMail("Обновление страниц о компании", "", [logfile])

