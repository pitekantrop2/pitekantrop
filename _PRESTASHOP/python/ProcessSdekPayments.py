﻿# -*- coding: utf-8 -*-
import log
import datetime
import PrestashopAPI
from selenium import webdriver
import myLibrary
import SendMails
import SdekAPI
import time
import commonLibrary
import sys
import ModulbankAPI
import win32api
import win32com.client


def CheckRegistryPayment(registryParams):
    global message

    for operation in operationsHistory:
        if operation["contragentName"] == u'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СДЭК-ГЛОБАЛ"':
            if operation["paymentPurpose"].find(registryParams[0]) != -1:
                operationAmount = int(float(operation["amount"]))
                print registryParams[3]
                registryAmount = int(float(registryParams[3].replace("\n","")))
                if registryAmount == operationAmount:
                    return True
                else:
                    message += "[Error] Различия в сумме реестра: {0} руб. поступило на р/с, {1} руб. в лк".format(registryParams, operationAmount)
                    logging.LogErrorMessage(u"Различия в сумме реестра: {0} руб. поступило на р/с, {1} руб. в лк".format(registryParams, operationAmount))

    return False

################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processSdekPayments")
logging = log.Log(logfile)

message = ""

sdek = SdekAPI.SdekAPI()
mails = SendMails.SendMails()
objects = sdek.fulfilmentObjects
prestashop_api = PrestashopAPI.PrestashopAPI()
shell = win32com.client.Dispatch("WScript.Shell")

registriesIds = prestashop_api.GetRegistries()

mb = ModulbankAPI.ModulbankAPI()
operationsHistory = mb.GetOperationsHistory((datetime.date.today() - datetime.timedelta(days=100)).strftime('%Y-%m-%d'), "Debet")

sdek.LoginInLK()
sdek.selenium.OpenUrl( sdek.LKUrl + "docs/registries-np")
sdek.selenium.WaitFor(objects.registries_searchButton)
sdek.selenium.Click(objects.registries_paidFilter)
sdek.selenium.Click(objects.registries_dateFrom)
sdek.selenium.WaitFor(objects.registries_previousMonth)
sdek.selenium.Click(objects.registries_previousMonth)
sdek.selenium.Click(objects.registries_previousMonth)
time.sleep(1)
sdek.selenium.Click(objects.registries_firstDay)
sdek.selenium.WaitForNotVisible(objects.registries_firstDay)
sdek.selenium.Click(objects.registries_searchButton)
sdek.selenium.WaitFor(objects.registries_registryRow)

paymentsDetails = {}
processedRegistries = []

while True:
    registryRows = sdek.selenium.MyFindElements(objects.registries_registryRow)
    registryRows.pop(0)
    numOfRows = len(registryRows)
    time.sleep(5)

    for registryRow in registryRows:
        columns = sdek.selenium.MyFindElementsInElement(registryRow, ["tagname", "td"])

        registryParams = [ sdek.selenium.GetText(td) for td in columns ]
        registryId = registryParams[0]

        if registryId in registriesIds:
            continue

        if CheckRegistryPayment(registryParams) == False:
            continue

        logging.Log(u"Реестр №" + registryId)
        message += "Реестр №{0}<br/>".format(registryId)

        while True:
            try:
                sdek.selenium.Click(registryRow)
                break
            except:
                shell.SendKeys("{DOWN}")
                time.sleep(0.5)

        sdek.selenium.WaitForText("Таблица накладных")
        time.sleep(2)

        registrySum = 0
        pagesNum = len(sdek.selenium.MyFindElements(objects.invoices_page)) - 1
        if pagesNum == -1:
            pagesNum = 1

        for i in range(0, pagesNum):

            paymentsRows = sdek.selenium.MyFindElements(objects.invoices_rows)
            paymentsRows.pop(0)

            for paymentRow in paymentsRows:

                columns = sdek.selenium.MyFindElementsInElement(paymentRow, ["tagname", "td"])

                paymentParams = [ sdek.selenium.GetText(td) for td in columns ]
                invoiceId = paymentParams.pop(0)

                registrySum += float(paymentParams[5])

                paymentsDetails[invoiceId] = paymentParams

            if i != pagesNum - 1:
                sdek.selenium.Click(objects.registries_nextPage)
                time.sleep(2)

        if int(float(registrySum)) != int(float(registryParams[3])):
            message += "Различия в сумме платежа реестра {0}: {1} vs {2}\n\n".format(registryId, registryParams[3], registrySum)
            logging.Log(u"Различия в сумме платежа реестра {0}: {1} vs {2}\n\n".format(registryId, registryParams[3], registrySum))

        for i in range(0, 20):
            try:
                sdek.selenium.Click(objects.invoices_back)
                break
            except:
                shell.SendKeys("{UP}")
                time.sleep(0.5)

        sdek.selenium.WaitFor(objects.registries_registryRow)
        processedRegistries.append(registryId)

    if numOfRows < 15:
        break

    try:
        sdek.selenium.Click(objects.registries_nextPage)
    except:
        break
    sdek.selenium.WaitFor(objects.registries_registryRow)
    time.sleep(2)

sdek.selenium.CleanUp()

################################################################################

if len(paymentsDetails) == 0:
    sys.exit()

states = [
u"Отправлен",
u"Доставлен",
u"Вручен",
u"Отклонен"
]

machineName = commonLibrary.GetMachineName()
endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=90)

today = datetime.date.today()

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    message += "\n=========  " + site["url"] + "  ===========\n\n"
    logging.Log("===========  " + site["url"] + "  ===========")

    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:
        orderId = order[0]
        boUrl = "{0}?controller=AdminOrders&id_order={1}&vieworder".format(backofficeUrl, orderId)
        trackingNumber = prestashop_api.GetOrderTrackingNumber(orderId)
        if len(str(trackingNumber)) != 10:
            continue

        orderState = prestashop_api.GetOrderState(orderId)
        orderNote = prestashop_api.GetOrderNote(orderId)
        orderPaid = commonLibrary.GetOrderPaid(orderNote)

        if trackingNumber in paymentsDetails.keys():
            orderDate, deliveryDate, deliveryCosts, agentsCommission, cod, sumToPay, sumType = paymentsDetails[trackingNumber]
            deliveryCosts = int(float(deliveryCosts))
            agentsCommission = int(float(agentsCommission))
            cod = int(float(cod))
            sumToPay = int(float(sumToPay))

            if orderPaid == False:
                #проверка суммы НП
                orderTotalSum = int(float(prestashop_api.GetOrderTotalCost(orderId)))

                if orderTotalSum != cod and orderState != u"Отклонен":
                    message += "Не совпадают суммы НП по заказу <a target='blank_' href='{0}'>{1}</a> (<a target='blank_' href='https://www.cdek.ru/track.html?order_id={4}'>{4}</a>): {2} у нас, {3} у СДЭК\n".format(boUrl, orderId, int(orderTotalSum), int(cod), trackingNumber)
                    logging.LogErrorMessage(u"Не совпадают суммы НП по заказу {0} ({3}): {1} у нас, {2} у СДЭК".format(orderId, int(orderTotalSum), int(cod), trackingNumber))

                #проверка стоимости доставки
                orderDeliverySum = int(float(prestashop_api.GetOrderDeliveryCost(orderId)))
                if deliveryCosts > orderDeliverySum + 150:
                    message += "Фактическая стоимость доставки по заказу <a target='blank_' href='{0}'>{1}</a> (<a target='blank_' href='https://www.cdek.ru/track.html?order_id={4}'>{4}</a>) превышает стоимость доставки для клиента: {2} фактическая, {3} в заказе\n".format(boUrl, orderId, int(deliveryCosts), int(orderDeliverySum), trackingNumber)
                    logging.LogWarningMessage(u"Фактическая стоимость доставки по заказу {0} ({3}) превышает стоимость доставки для клиента: {1} фактическая, {2} в заказе".format(orderId, int(deliveryCosts), int(orderDeliverySum), trackingNumber))

                if cod > 0:
                    prestashop_api.SetOrderState(orderId, u"Доставлен и оплачен")
                    message += "Изменен статус заказа {0} на 'Доставлен и оплачен'\n".format(orderId)
                    logging.Log(u"Изменен статус заказа {0} на 'Доставлен и оплачен'".format(orderId))
                    paymentsDetails.pop(trackingNumber)

##    if machineName.find("hp") != -1:
##        ssh.kill()

################################################################################

message += "\n\n"

for invoiceId in paymentsDetails.keys():
    orderDate, deliveryDate, deliveryCosts, agentsCommission, cod, sumToPay, sumType = paymentsDetails[invoiceId]

    #проверка стоимости заявок на вызов курьера
    if len(invoiceId) != 10:

        if int(float(deliveryCosts)) != 300:
            message += "Некорректная стоимость {0} заявки на вызов курьера {1}\n".format(deliveryCosts, invoiceId)
            logging.LogErrorMessage(u"Некорректная стоимость {0} заявки на вызов курьера {1}".format(deliveryCosts, invoiceId))

    if sumType.find(u"Сторно") != -1:
        message += "Операция '{0}' по накладной {1}, сумма к начислению {2}\n".format(sumType.encode("utf-8"), invoiceId, str(sumToPay))
        logging.LogErrorMessage(u"Операция '{0}' по накладной {1}, сумма к начислению {2}\n".format(sumType, invoiceId, str(sumToPay)))

################################################################################

for registryId in processedRegistries:
    prestashop_api.AddRegistryId(registryId)

if message != "":
    mails.SendInfoMail("Обработка платежей", message)
