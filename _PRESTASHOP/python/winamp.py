# -*- coding: utf-8 -*-

import psutil
import datetime
import time
import os
import subprocess
import commonLibrary
import PrestashopAPI
import random
import log

def GetFlag():
    presta = PrestashopAPI.PrestashopAPI()
    sql = """SELECT flag FROM winamp"""
    try:
        return presta.MakeLocalDbGetInfoQueue(sql)[0][0]
    except:
        return 0

def KillWinamp():
    for process in psutil.process_iter():
        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "winamp.exe":
            process.kill()

def IsWinampRunning():
    for process in psutil.process_iter():
        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "winamp.exe":
            return True

    return False

#################################################################################

logging = log.Log("log\\winamp.log")

ultraListPath = u'f:\\music\\ultra\\ultra.m3u'
soundListPath = u'c:\\music\\sounds\\1.m3u'

flag_ = -1
while True:
    minute = random.randint(1, 40)
    flag = GetFlag()
    if flag == flag_:
        continue
    else:
        if flag_ in [1,2]:
            KillWinamp()
        flag_ = flag
    if flag in [1, 2]:
        if flag == 1:
            listPath = ultraListPath
        else:
            listPath = soundListPath
        if IsWinampRunning() == False:
            try:
                daemon = subprocess.Popen(["C:\\Program Files (x86)\\Winamp\\winamp.exe", '/play', listPath])
            except Exception as e:
                logging.LogErrorMessage(str(e))

    elif flag == 0:
        KillWinamp()
    else:
        now = datetime.datetime.now()

        if (now.hour == 8 and now.minute > minute) or (now.hour >= 9 and now.hour < 22) or (now.hour == 21 and now.minute < minute):
            if IsWinampRunning() == False:
                try:
                    daemon = subprocess.Popen(["C:\\Program Files (x86)\\Winamp\\winamp.exe", '/play', ultraListPath])
                except:
                    daemon.kill()

        else:
            KillWinamp()

    time.sleep(5)
