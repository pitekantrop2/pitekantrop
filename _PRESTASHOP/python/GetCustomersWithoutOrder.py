﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary


##prestashop_api = PrestashopAPI.PrestashopAPI()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "clientsWithoutOrder")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

now = datetime.datetime.now()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = datetime.date.today()

bIsCustomers = False
message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    bTitle = False

    customers = prestashop_api.GetCustomersForPeriod(startDate, endDate)

    for customer in customers:
        customerId = customer[0]
        customerFirstname = customer[1]
        customerLastname = customer[2]
        shopName = prestashop_api.GetShopName(customer[3])

        orders = prestashop_api.GetCustomerOrdersIds(customerId)

        if len(orders) > 0:
            continue

        addressData = prestashop_api.GetCustomerAddressData(customerId)
        if addressData == -1:
            continue

        id_address, city, phone, phone_mobile = addressData
        if phone_mobile in ["", None]:
            continue

        bIsCustomers = True

        if bTitle == False:
            message += ("\n=========  " + site["url"] + "  =========\n\n")
            bTitle = True

        productsString = ""
        products = prestashop_api.GetCartProductsIdsByAddressId(id_address)

        for product in products:
            productsString += prestashop_api.GetProductOriginalName(product[0]) + u" ({0} шт.), ".format(product[1])

        productsString = productsString.strip(", ")

        message += "{0} {1}, г. {2}, тел. {3}, товары: {4}\n".format(customerLastname.encode("utf-8"),
        customerFirstname.encode("utf-8"), shopName.encode("utf-8"), phone_mobile, productsString.encode("utf-8"))

    if machineName.find("hp") != -1:
        ssh.kill()

if bIsCustomers == True:
    result = mails.SendInfoMail("Незавершившие заказ покупатели", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(30)
