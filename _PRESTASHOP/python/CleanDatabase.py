﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary


prestashop_api = PrestashopAPI.PrestashopAPI()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "cleanDatabase")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

now = datetime.datetime.now()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = '2012-01-01 00:00:00'

message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    customersCounter = 0
    message += ("\n=========  " + site["url"] + "  =========\n\n")

    customers = prestashop_api.GetCustomersForPeriod(startDate, endDate)

    for customer in customers:
        customerId = customer[0]

        orders = prestashop_api.GetCustomerOrdersIds(customerId)

        if len(orders) == 0:
            prestashop_api.DeleteCustomer(customerId)
            customersCounter += 1

    message += "Удалено покупателей: {0}\n".format(customersCounter)
    threads = prestashop_api.GetCustomerThreads(0, datetime.date.today() - datetime.timedelta(days=1))
    for thread in threads:
        prestashop_api.DeleteCustomerThread(thread[0])

    message += "Удалено сообщений: {0}\n".format(len(threads))

    prestashop_api.DeleteGuests()

    if machineName.find("hp") != -1:
        ssh.kill()

result = mails.SendInfoMail("Очистка БД", message)
if result != True:
    logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(30)
