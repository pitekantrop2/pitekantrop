﻿# -*- coding: utf-8 -*-

import log
import datetime
import sys
import WebasystAPI
import SendMails
import time
import commonLibrary
import Aliexpress
import UAliexpressObjects
import OzonAPI

def GetOrderDetails(trackNum):
##    print trackNum
    for x in aliexpressOrders:
##        print x[3]
        if x[3] == trackNum:
            return x
    return -1

def TakeToWarehouse(trackNum, items):
    global message
    message += "Отправление {0} ({1}) вручено\n".format(trackNum.encode("utf-8"), items.encode("utf-8"))
    message += "Приняты товары:\n"
    deliveryItemsNames, deliveryItemsQuantities = commonLibrary.ParseItems(items)
    for i in range(0, len(deliveryItemsNames)):
        itemName = deliveryItemsNames[i]
        itemQty = deliveryItemsQuantities[i]
        if webasyst_api.GetWarehouseProductQuantity("warehouse_spb", itemName) == -1:
            commonLibrary.AddProductToWarehouse("warehouse_spb", {"name" : itemName})
        webasyst_api.IncreaseWarehouseProductQuantity("warehouse_spb", itemName, int(itemQty))
        actualProductQty = webasyst_api.GetWarehouseProductQuantity("warehouse_spb", itemName)
        message += "{0} - {1} шт. (остаток в офисе {2} шт.)\n".format(itemName.encode("utf-8"),itemQty, actualProductQty)

        # обновляем остатки товара на складе Озона
        sku = webasyst_api.ozon_GetProductSku(itemName)
        if sku == -1:
            continue

        ozonQuantity = ozon.GetProductStocks(sku, ozonWarehouseName)
        ozonQuantity += productQty
        result = ozon.SetProductStocks(sku, ozonWarehouseName, ozonQuantity)
        if result != True:
            message += "[ERROR] Ошибка обновления остатков товара на складе '{}': '{}'\n".format(ozonWarehouseName.encode("utf-8"), result.encode("utf-8"))
        else:
            message += "остаток на складе '{}' - {} шт.\n".format(ozonWarehouseName.encode("utf-8"), ozonQuantity)

    webasyst_api.DeleteAliexpressOrder(trackNum)

################################################################################

webasyst_api = WebasystAPI.WebasystAPI()

mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
aliexpressOrders = webasyst_api.GetAliexpressOrders()
ozon = OzonAPI.OzonAPI()
objects = UAliexpressObjects.UAliexpressObjects
bOnlyTakeToWarehouse = True
deliveredOrders = []
message = ""
ozonWarehouseName = u'Офис Спб'

if not bOnlyTakeToWarehouse:
    ali = Aliexpress.Aliexpress()
    ali.Login("feedback@knowall.ru.com", "19842001")
    ali.OpenSendedOrderList()
    ali.selenium.MaximizeWindow()
    time.sleep(2)
    if ali.selenium.IsVisible(objects.closeChatButton):
        ali.selenium.Click(objects.closeChatButton)
        time.sleep(1)

    pageNum = 2
    while True:
        buttons = ali.selenium.MyFindElements(objects.ordersList_CheckOrderTrackButton)
        buttonsNum = len(buttons)
        for i in range(0, buttonsNum):
            ali.selenium.Sleep(5)
            if ali.selenium.IsTextPresent( "Service is unavailable"):
                ali.selenium.Back()
            trButton = ali.selenium.MyFindElements(objects.ordersList_CheckOrderTrackButton)[i]
            ali.selenium.Click(trButton)
            ali.selenium.WaitForText("Отслеживание заказа")
            if ali.selenium.IsVisible(objects.order_TrackNum):
                trackNum = ali.selenium.GetText(objects.order_TrackNum)
            elif ali.selenium.IsVisible(objects.order_TrackNum2):
                trackNum = ali.selenium.GetText(objects.order_TrackNum2)
            else:
                message += "[Error] Не найден трек-код заказа в строке {}\n".format(i)
                ali.selenium.Back()
                continue
            orderDetails = GetOrderDetails(trackNum)
            if orderDetails == -1:
                ali.selenium.Back()
                continue
            orderState = orderDetails[-1]
            items = orderDetails[2]
            if orderState == u"Вручен":
                TakeToWarehouse(trackNum, items)
                deliveredOrders.append(trackNum)
                ali.selenium.Back()
                continue
            if not ali.selenium.IsVisible(objects.order_StatesList):
                state = u"Заказ отправлен"
                if orderState != state:
                    webasyst_api.SetAliexpressOrderState(trackNum, state)
                ali.selenium.Back()
                continue
            states = [ali.selenium.GetText(x) for x in ali.selenium.MyFindElements(objects.order_States)]
            dates = [ali.selenium.GetText(x) for x in ali.selenium.MyFindElements(objects.order_Dates)]
            if len(states) == 0:
                states = [ali.selenium.GetText(x) for x in ali.selenium.MyFindElements(objects.order_States2)]
                dates = [ali.selenium.GetText(x) for x in ali.selenium.MyFindElements(objects.order_Dates2)]
            if len(states) == 0:
                ali.selenium.Back()
                continue
            currentState = states[0]
            if currentState in [u"[RU]Delivery successful",
                            u"Delivery successful",
                            u"[RU]Delivered",
                            u"[unknown,RU]Выдан клиенту",
                            u"[RU]Успешно доставлено",
                            u"Вручение,Вручение адресату",
                            u"Вручен", u"[unknown]Выдан клиенту",
                            u"Delivered",
                            u"Успешно доставлено",
                            u"Delivery",
                            u"Товар был успешно доставлен получателю. Спасибо что воспользовались нашими услугами",
                            u"【Russia】 Delivered. Thank you for using SF Express. Looking forward to serving you again",
                            u"Delivery,Delivery to the addressee",
                            u"Successful delivery"] or orderState == u"Вручен":
                try:
                    TakeToWarehouse(trackNum, items)
                    deliveredOrders.append(trackNum)
                except:
                    message += "[Error] Ошибка парсинга товаров: {0}\n".format(items)
                ali.selenium.Back()
                continue
            currentStateWithDate = u"{0}: {1}".format(dates[0],currentState)
            if orderState.find(currentState) == -1:
                webasyst_api.SetAliexpressOrderState(trackNum,currentStateWithDate)
                message += "Изменен статус отправления {0} ({2}) на '{1}'\n".format(trackNum, currentState.encode("utf-8"), items.encode("utf-8"))
            ali.selenium.Back()

        if ali.selenium.IsTextPresent( "Service is unavailable"):
            ali.selenium.Back()
        nextPage = ["xpath", "//a[@pageno = '{0}' and contains(@class, 'ui-goto-page')]".format(pageNum)]
        if ali.selenium.IsVisible(nextPage):
            ali.selenium.Click(nextPage)
            pageNum += 1
        else:
            break

    if ali.selenium != None:
        ali.CloseSelenium()

aliexpressOrders = webasyst_api.GetAliexpressOrders()
for order in aliexpressOrders:
    id, date, product, trackingCode, state = order
    if trackingCode in deliveredOrders:
        continue
    if state == u"Вручен":
        TakeToWarehouse(trackingCode, product)


if message != "":
    result = mails.SendInfoMail("Обработка заказов aliexpress", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
