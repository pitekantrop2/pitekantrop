﻿import log
import datetime
import sys
import PrestashopAPI
import SendMails
import io
import subprocess
import time
import psutil
import commonLibrary
import TextruAPI
from lxml import etree

prestashop_api = PrestashopAPI.PrestashopAPI()

def ClearText(long_description):
    ldElement = etree.HTML(long_description)
    elements = ldElement.xpath(u".//p[contains(., '{0}') or contains(., '{1}') or contains(., '{2}') or contains(., '{3}') or contains(., '{4}')]".format(u"время работы",u"г.", u" модели, как ", u"выдачи", u"выдача"))

    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath(".//h2")
    for element in elements:
        element.getparent().remove(element)

    long_description = commonLibrary.GetStringDescription(ldElement)
    long_description = long_description.replace("<p>","")
    long_description = long_description.replace("</p>","")

    return long_description

################################################################################

mails = SendMails.SendMails()
textru_api = TextruAPI.TextruAPI()
machineName = commonLibrary.GetMachineName()

message = ""
texts = prestashop_api.GetTexts()

indexedTexts = []

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    indexedCats = []

    for textData in texts:
        siteUrl, shopId, categories, id = textData
        if siteUrl == prestashop_api.siteUrl:
            for catId in categories.split(","):
                catId = catId.strip()
                descr = ClearText(prestashop_api.GetShopCategoryLongDescription(shopId, catId))
                uniqueness = textru_api.GetTextUniqueness(descr)
                if float(uniqueness) != 100:
                    indexedCats.append(catId)

            city = commonLibrary.GetShopName(prestashop_api.GetShopName(shopId))

        if len(indexedCats) != 0:
            indexedTexts.append([siteUrl, city, indexedCats])

    if machineName.find("hp") != -1:
         ssh.kill()

for entry in indexedTexts:
    siteUrl, city, indexedTexts = entry
    message += "<p>{0} ({1}): {2}</p><br/>".format(siteUrl, city.encode("utf-8"), ", ".join(indexedTexts))


if message != "":
    result = mails.SendInfoMail("Проиндексированные тексты", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
