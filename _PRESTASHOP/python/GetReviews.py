﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys
import xlrd
import os
import myLibrary
import UBaseObjects

def CorrectUrl(url):
    if url == "https://www.ozon.ru/context/detail/id/145022170/":
        return "https://www.ozon.ru/context/detail/id/140002759/"
    if url == "https://www.ozon.ru/context/detail/id/140002761/":
        return "https://www.ozon.ru/reviews/149994209/"

    return url

def CheckName(site, source, url):
    if site == "ozon.ru":
        exprs = ["//div[@class= '_8627a4']", "//span[@class = '_086f9b']", "//div[@class = 'b0o1']"]
        name = u""
        for expr in exprs:
            try:
                name = source.xpath(expr)[0].text.strip().lower()
                if name == "":
                    continue
                break
            except:
                continue
        print site, name
        logging.Log(u"{0}: '{1}', {2}".format(site, name, url))
        if bNameCheck == True:
            if name.find(productName.lower()) == -1:
                return -1
    if site == "onlinetrade.ru":
        name = source.xpath("//h1")[0].text.lower()
        print site, name
        logging.Log(u"{0}: '{1}', {2}".format(site, name, url))
        if bNameCheck == True:
            if name.find(productName.lower()) == -1:
                return -1
    if site == "market.yandex.ru":
        try:
            name = source.xpath("//h1/a")[0].text.lower()
        except:
            return -1
        print site, name
        logging.Log(u"{0}: '{1}', {2}".format(site, name, url))
        if bNameCheck == True:
            if name.find(productName.lower()) == -1:
                return -1
    if site == "mvideo.ru":
        name = source.xpath("//h1")[0].text.lower()
        print site, name
        logging.Log(u"{0}: '{1}', {2}".format(site, name, url))
        if bNameCheck == True:
            if name.find(productName.lower()) == -1:
                return -1
    if site == "wildberries.ru":
        try:
            name = source.xpath("//span[@class = 'name']")[0].text.strip().lower()
        except:
            return -1
        print site, name
        logging.Log(u"{0}: '{1}', {2}".format(site, name, url))
        if bNameCheck == True:
            if name.find(productName.lower()) == -1:
                return -1
    return 0


def ValidateDate(date):
    try:
        datetime.datetime.strptime(date, '%Y-%m-%d')
    except ValueError:
        return False

    return True

def GetProductLocalReviews(name):
    reviews = []
    for localReview in localReviews:
        id, productName, site, author, text,grade, title, date = localReview
        if productName == name:
            return True

    return False

def ExistingLocalReview(review):
    for localReview in localReviews:
        id, productName, site, author, text,grade, title, date = localReview
        print localReview
        try:
##            if date.strftime('%Y-%m-%d') == review["date"] and site == review["site"] and author == review["author"]:
            if productName == review["name"] and site == review["site"] and author == review["author"]:
                return True
        except:
            pass

    return False

def SiteVisited(site, reviews):
    for review in reviews:
        if review["site"] == site:
            return True

    return False


def GetOzonReviews(url):
    reviews = []
    productId = url.split("/")[-2]
    url = "https://www.ozon.ru/reviews/{0}/".format(productId)
    selenium = myLibrary.Selenium()
    selenium.SetUp()
    selenium.OpenUrl(url)
    source =  selenium.GetSource()
    pageSource = etree.HTML(source)
    if CheckName("ozon.ru", pageSource, url) == -1:
        selenium.CleanUp()
        return []
    pages = pageSource.xpath("//a[contains(@class, 'pagintaion-item')]")
    pagesNum = len(pages)
    if pagesNum == 0:
        pagesNum = 1
    for i in range(1, pagesNum + 1):
        selenium.OpenUrl(url + "?page=" + str(i))
        source = selenium.GetSource()
        pageSource = etree.HTML(source)
        reviewsElems = pageSource.xpath("//div[@itemprop = 'review']")
        for reviewsElem in reviewsElems:
            review = {}
            review["title"] = productName
            review["name"] = productName
            review["site"] = "ozon.ru"
            text = reviewsElem.xpath(".//meta[@itemprop='reviewBody']")[0].attrib["content"]
            text = text.replace(u"Достоинства", u"<b>Достоинства</b><br>").replace(u"Недостатки", u"<br><br><b>Недостатки</b><br>").replace(u"Комментарий", u"<br><br><b>Комментарий</b><br>")
            review["content"] = text
            review["author"] = reviewsElem.xpath(".//meta[@itemprop='author']")[0].attrib["content"]
            review["grade"] = reviewsElem.xpath(".//meta[@itemprop='ratingValue']")[0].attrib["content"]
            review["date"] = reviewsElem.xpath(".//meta[@itemprop='datePublished']")[0].attrib["content"].replace("00", "01")
            if ValidateDate(review["date"]) == False:
                review["date"] = "2019-04-25"

            reviews.append(review)

    selenium.CleanUp()
    return reviews


def GetOnlinetradeReviews(url):
    reviews = []
    selenium = myLibrary.Selenium()
    selenium.SetUp()
    selenium.OpenUrl(url.replace("m.onlinetrade.ru", "onlinetrade.ru") + "#tabs_feedbacks")
    source = selenium.GetSource()
    selenium.CleanUp()
    pageSource = etree.HTML(source)
    if CheckName("onlinetrade.ru", pageSource, url) == -1:
        return []
    reviewsElems = pageSource.xpath("//div[@itemprop = 'review']")
    for reviewsElem in reviewsElems:
        review = {}
        review["name"] = productName
        review["site"] = "onlinetrade.ru"
        review["title"] = reviewsElem.xpath(".//span[@itemprop='name']")[0].text
        textElem = reviewsElem.xpath(".//div[@itemprop='reviewBody']")[0]
        textElem.attrib.pop("itemprop")
        review["content"] = commonLibrary.GetStringDescription(textElem)
        review["author"] = reviewsElem.xpath(".//b[@itemprop='author']")[0].text
        review["grade"] = reviewsElem.xpath(".//div[contains(@class, 'stars stars')]")[0].attrib["title"].replace(u"Оценка: ", "")
        review["date"] = reviewsElem.xpath(".//span[@itemprop='datePublished']")[0].attrib["content"]
        if ValidateDate(review["date"]) == False:
            review["date"] = "2019-04-25"

        reviews.append(review)

    return reviews

def GetYandexMarketReviews(url):
    reviews = []
    if url.find("reviews") == -1:
        url = url + "/reviews"
    source = requests.get(url).text
    pageSource = etree.HTML(source)
    if CheckName("market.yandex.ru", pageSource, url) == -1:
        return []
    reviewsElems = pageSource.xpath("//div[contains(@class, 'n-product-review-item i-bem')]")
    for reviewsElem in reviewsElems:
        review = {}
        review["name"] = productName
        review["site"] = "market.yandex.ru"
        review["title"] = productName
        titles = reviewsElem.xpath(".//dt[@class='n-product-review-item__title']")
        texts = reviewsElem.xpath(".//dd[@class='n-product-review-item__text']")
        text = u""
        if len(titles) == len(texts):
            for i in range(0, len(titles)):
                title = titles[i].text
                text_ = texts[i].text
                if title == u"Достоинства: ":
                    text += u"<b>Достоинства:</b><br>"
                else:
                    text += u"<br><br><b>{0}</b><br>".format(title)
                text += text_
        else:
            text += texts[0].text

        if text == u"":
            continue

        review["content"] = text
        try:
            review["author"] = reviewsElem.xpath(".//meta[@itemprop='author']")[0].attrib["content"]
            review["grade"] = reviewsElem.xpath(".//meta[@itemprop='ratingValue']")[0].attrib["content"]
            review["date"] = reviewsElem.xpath(".//meta[@itemprop='datePublished']")[0].attrib["content"]
            if ValidateDate(review["date"]) == False:
                review["date"] = "2019-04-25"
        except:
            continue

        reviews.append(review)

    return reviews

def GetMvideoReviews(url):
    reviews = []
    selenium = myLibrary.Selenium()
    selenium.SetUp()
    if url.find("reviews") == -1:
        url = url + "/reviews"
    selenium.OpenUrl(url)
    source = selenium.GetSource()
    selenium.CleanUp()
    pageSource = etree.HTML(source)
    if CheckName("mvideo.ru", pageSource, url) == -1:
        return []
    reviewsElems = pageSource.xpath("//div[@class = 'review-ext-item-content']")
    for reviewsElem in reviewsElems:
        review = {}
        review["name"] = productName
        review["site"] = "mvideo.ru"
        review["title"] = reviewsElem.xpath(".//meta[@itemprop='itemReviewed']")[0].attrib["content"]
        text = reviewsElem.xpath(".//p[@itemprop='description']/span")[0].text
        review["content"] = text
        review["author"] = reviewsElem.xpath(".//span[@itemprop='author']")[0].text
        review["grade"] = reviewsElem.xpath(".//span[@itemprop='ratingValue']")[0].text
        review["date"] = reviewsElem.xpath(".//span[@itemprop='datePublished']")[0].attrib["content"]
        if ValidateDate(review["date"]) == False:
            review["date"] = "2019-04-25"

        reviews.append(review)

    return reviews

def GetWildberriesReviews(url):
    reviews = []
    url += "?targetUrl=GO#Comments"
    selenium = myLibrary.Selenium()
    selenium.SetUp()
    selenium.OpenUrl(url)
    source = selenium.GetSource()
    pageSource = etree.HTML(source)
    if bDebug == True:
        with open("wildberries.htm", "w") as f:
            f.write(commonLibrary.GetStringDescription( pageSource).encode("utf-8"))
    if CheckName("wildberries.ru", pageSource, url) == -1:
        return []
    moreReviews = UBaseObjects.GetElementByText("span", u"Показать еще отзывы")
    while selenium.IsVisible(moreReviews):
        selenium.Click(moreReviews)
        selenium.WaitForTextNotVisible(u"Загрузка...")

    source = selenium.GetSource()
    selenium.CleanUp()
    pageSource = etree.HTML(source)
    reviewsElems = pageSource.xpath("//div[@itemprop = 'review']")
    for reviewsElem in reviewsElems:
        review = {}
        review["name"] = productName
        review["site"] = "wildberries.ru"
        review["title"] = productName
        text = reviewsElem.xpath(".//p[@itemprop='reviewBody']")[0].text
        review["content"] = text
        try:
            review["author"] = reviewsElem.xpath(".//meta[@itemprop='author']")[0].attrib["content"]
            review["grade"] = reviewsElem.xpath(".//div[@itemprop='ratingValue']")[0].text
            review["date"] = reviewsElem.xpath(".//div[@itemprop='datePublished']")[0].attrib["content"].split("T")[0]
            if ValidateDate(review["date"]) == False:
                review["date"] = "2019-04-25"
        except:
            continue

        reviews.append(review)



    return reviews

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
localReviews = prestashop_api.GetLocalReviews()
bNameCheck = True
bDebug = False

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    if prestashop_api.siteUrl in ["http://rose-of-bulgaria.ru"]:
        bNameCheck = False

    products = prestashop_api.GetActiveProducts()

    for product in products:
        id_product, name, original_name = product
        reviews = []
        productName = commonLibrary.DeleteQuotes(name)
        if len(prestashop_api.GetProductLocalReviews(productName)) != 0:#чтобы избежать дублей
            continue

        print productName
        logging.Log(u"============ {0} ( {1} ) =============".format( productName, id_product))

        searchUrl = u"https://www.google.ru/search?q={0}".format(productName.replace(" ", "+"))
        resp = requests.get(searchUrl)
        googleSearchResults = etree.HTML(resp.text)
        if bDebug == True:
            print searchUrl
            with open("gog.htm", "w") as f:
                f.write(commonLibrary.GetStringDescription( googleSearchResults).encode("utf-8"))
        sitesLinks = googleSearchResults.xpath("//a[@rel='noopener']")
        for link in sitesLinks:
            reviews_ = []
            url = link.attrib["href"].split("url?q=")[1].split("&sa")[0]
            url = CorrectUrl(url)
            if url.find("ozon.ru") != -1:
                if SiteVisited("ozon.ru",reviews) == False:
                    reviews_ = GetOzonReviews(url)
            if url.find("onlinetrade.ru") != -1:
                if SiteVisited("onlinetrade.ru",reviews) == False:
                    reviews_ = GetOnlinetradeReviews(url)
            if url.find("market.yandex.ru") != -1:
                if SiteVisited("market.yandex.ru",reviews) == False:
                    reviews_ = GetYandexMarketReviews(url)
            if url.find("mvideo.ru") != -1:
                if SiteVisited("mvideo.ru",reviews) == False:
                    reviews_ = GetMvideoReviews(url)
            if url.find("wildberries.ru") != -1:
                if SiteVisited("wildberries.ru",reviews) == False:
                    reviews_ = GetWildberriesReviews(url)

            reviews += reviews_

        reviews = [x for x in reviews if int(x["grade"]) > 2]

        if len(reviews) == 0:#если отзывов нет, добавляем фейковую запись, чтобы повторно не собирать отзывы на данный товар
            review = {}
            review["name"] = productName
            review["site"] = ""
            review["title"] = ""
            review["content"] = ""
            review["author"] = ""
            review["grade"] = 0
            review["date"] = "0000-00-00"
            prestashop_api.AddLocalReview(review)
            continue

        for review in reviews:
            if prestashop_api.IsExistingLocalProductReview(review) == False:
                prestashop_api.AddLocalReview(review)

        #добавляем на сайт
        locReviews = prestashop_api.GetProductLocalReviews(productName)
        reviewsNum = 0
        for locReview in locReviews:
            if prestashop_api.IsExistingProductReview(id_product, locReview) == False:
                id, productName, site, author, text,grade, title, date = locReview
                if site != "":
                    prestashop_api.AddProductReview(id_product , locReview)
                    reviewsNum += 1

        logging.Log(u"Добавлено {0} отзывов для товара '{1}' ( {2} )".format(reviewsNum, productName, id_product))

    if machineName.find("hp") != -1:
        ssh.kill()

