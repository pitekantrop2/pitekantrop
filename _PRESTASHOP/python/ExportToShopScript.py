﻿# -*- coding: utf-8 -*-


import lxml
from lxml import etree
import log
import datetime
import PrestashopAPI
import SdekAPI
import commonLibrary
import time
import random
import WebasystAPI
import os
import re
import io
import sys

def GetDoubleCharacteristics():
    return [u"Угол обзора",
           u"Продолжительность автономной работы",
           u"Эффективная площадь",
           u"Суммарная мощность ламп"
            ]

def GetMultipleCharacteristics():
    return [u"Питание",
            u"Тип питания",
            u"Объект применения",
            u"Эффективен против",
            u"Тип воздействия"
            ]

def GetPageSortIndex(meta_title):
    if meta_title == u"О компании":
        return 1
    if meta_title == u"Оплата":
        return 2
    if meta_title == u"Доставка":
        return 3
    if meta_title in [u"Информация для юрлиц", u"Для юрлиц"]:
        return 4
    if meta_title == u"Гарантии и правила возврата":
        return 5
    if meta_title == u"Политика конфиденциальности":
        return 6

    return 0

def DownloadArticleImages(id_shop, content):
    sourceDomain = prestashop_api.GetShopMainDomain(1)
    picsForDownload = []
    ldElement = etree.HTML(content)
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(sourceArticlesDir) == -1:
            picsForDownload.append( picUrl)

    if len(picsForDownload) == 0:
        return []

    successDownloadImages = []
    for picUrl in picsForDownload:
        picUrl_ = picUrl
        if picUrl_.find(sourceDomain) == -1:
            picUrl_ = sourceDomain + picUrl
        if commonLibrary.DownloadImage(picUrl_, sourceArticlesDir):
            successDownloadImages.append(picUrl)
    return successDownloadImages

def UpdateArticlePictures(content, successDownloadImages):
    if len(successDownloadImages) == 0:
        return content

    ldElement = etree.HTML(content)

    bNeedUpdate = False
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl in successDownloadImages:
            bNeedUpdate = True
            picName = commonLibrary.GetPictureNameFromUrl(picUrl)
            element.attrib["src"] = u"{}/{}".format( destArticlesDir, picName)

    content = commonLibrary.GetStringDescription(ldElement)
    return content

def CorrectArticle(id_shop, content, mainDomain):
    productLinks = []
    successDownloadImages = DownloadArticleImages(id_shop, content)
    content = UpdateArticlePictures(content, successDownloadImages)
##    print content
    tree = etree.HTML(content)
    elements = tree.xpath(".//a")
    for element in elements:
        href = element.attrib["href"]
        href = href.replace(mainDomain, "")
        p = re.compile('\/(\d+)\-')
        try:
            id = p.findall(href)[0]
            count = href.count("\\")
            if href.find("html") != -1 :#товар
                aHtml = commonLibrary.GetStringDescription(element)
                aText = element.text
                if aText == None:
                    spans = element.xpath(".//span")
                    try:
                        aText = spans[0].text
                    except:
                        elHtml = commonLibrary.GetStringDescription(element)
                        ind = elHtml.rfind("</a>")
                        elHtml = elHtml[:ind]
                        ind = elHtml.rfind(">")
                        aText = elHtml[ind + 1:]
                aHtml = aHtml[aHtml.index("<a"):aHtml.rindex("</a>") + 4]
                productLinks.append([aHtml, aText])
                continue
##                name = prestashop_api.GetProductOriginalName(id)
##                productId = webasyst_api.GetProductIdQuick(name)
##                if productId == -1:
##                    element.getparent().remove(element)
##                    continue
##                productUrl = webasyst_api.GetProductUrl(productId, u"Санкт-Петербург")
##                element.attrib["href"] = productUrl

            else:#категория
                name = prestashop_api.GetCategoryName(id)
                catId = webasyst_api.GetCategoryId(name)
                catUrl = webasyst_api.GetCategoryUrl(catId, u"Санкт-Петербург")
                element.attrib["href"] = catUrl
        except:
            continue

    content_ = commonLibrary.GetStringDescription(tree)
    for link in productLinks:
        aHtml, aText = link
        content_ = content_.replace(aHtml, aText)
##    print content_
    return content_


def CorrectPageContent(id_shop, meta_title, content):
    content = content.replace("<h3><br /><br /></h3>","<p><br/></p>")
    content = content.replace("<h3><br/></h3>","<p><br/></p>")
    content = content.replace("<p><br /><br /></p>","<p><br /></p>")
    if content.startswith("<p><br/></p>"):
        content = content[12:]
    if content.startswith("<p></p>"):
        content = content[7:]

    if meta_title == u"Оплата":
        content = content.replace(u"<h2>Оплата</h2>","")
        content = content.replace(u"<h1>Оплата</h1>","")
        return content

    successDownloadImages = DownloadPageImages(id_shop, content)
    content = UpdatePagePictures(content, successDownloadImages)
    tree = etree.HTML(content)
    elements = tree.xpath("//td")
    for element in elements:
        style = ""
        if "style" in element.attrib.keys():
            style = element.attrib["style"]
        style += "border: 0px;padding: 0px;"
        element.attrib["style"] = style
    tags = ["h2", "h1", "style"]
    for tag in tags:
        elements = tree.xpath("//{}".format( tag))
        for element in elements:
            element.getparent().remove(element)

    content = commonLibrary.GetStringDescription(tree)
    if meta_title == u"Доставка":
        content = content.replace("/widget/scripts/", "/wa-content/js/sdekwidget/scripts/")
        content = content.replace("""height:600px;">""", """height: 600px;"> </div>""")
        content = content.replace("""</p></div>""", """</p>""")
        content = """<script id="ISDEKscript" src="/wa-content/js/sdekwidget/widjet.js"></script>""" + content

    return content

def DownloadPageImages(id_shop, content):
    sourceDomain = prestashop_api.GetShopMainDomain(1)
    picsForDownload = []
    ldElement = etree.HTML(content)
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(sourcePagesDir) == -1:
            picsForDownload.append( picUrl)

    if len(picsForDownload) == 0:
        return []

    if id_shop > 1:
        return picsForDownload

    successDownloadImages = []
    for picUrl in picsForDownload:
        picUrl_ = picUrl
        if picUrl_.find("https://") == -1 and picUrl_.find("http://") == -1:
            picUrl_ = sourceDomain + picUrl
        if commonLibrary.DownloadImage(picUrl_, sourcePagesDir):
            successDownloadImages.append(picUrl)

    return successDownloadImages

def UpdatePagePictures(content, successDownloadImages):
    if len(successDownloadImages) == 0:
        return content

    ldElement = etree.HTML(content)

    bNeedUpdate = False
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(destPagesDir) == -1 and picUrl in successDownloadImages:
            bNeedUpdate = True
            picName = commonLibrary.GetPictureNameFromUrl(picUrl)
            element.attrib["src"] = u"{}/{}".format( destPagesDir, picName)

    content = commonLibrary.GetStringDescription(ldElement)
    return content

def DownloadCategoryImages(long_description):
    sourceDomain = prestashop_api.GetShopMainDomain(1)
    picsForDownload = []
    ldElement = etree.HTML(long_description)
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(destCatsDir) == -1:
            picsForDownload.append( picUrl)

    if len(picsForDownload) == 0:
        return []

    successDownloadImages = []
    for picUrl in picsForDownload:
        picUrl_ = picUrl
        if picUrl_.find(sourceDomain) == -1:
            picUrl_ = sourceDomain + picUrl
        if commonLibrary.DownloadImage(picUrl_, sourceCatsDir):
            successDownloadImages.append(picUrl)

    return successDownloadImages

def UpdateCatsPictures(long_description, successDownloadImages):
    if len(successDownloadImages) == 0:
        return long_description

    ldElement = etree.HTML(long_description)

    bNeedUpdate = False
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(destCatsDir) == -1 and picUrl in successDownloadImages:
            bNeedUpdate = True
            picName = commonLibrary.GetPictureNameFromUrl(picUrl)
            element.attrib["src"] = u"{}/{}".format( destCatsDir, picName)

    description = commonLibrary.GetStringDescription(ldElement)
    return description

def DownloadProductImages(productName, productDescr):
    sourceDomain = prestashop_api.GetShopMainDomain(1)
    picsForDownload = []
    productName = productName.replace(" ", "_")
    productDir = u"/wa-data/public/shop/img/" + productName + "/"
    ldElement = etree.HTML(productDescr)
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(productDir) == -1:
            picsForDownload.append( picUrl)

    if len(picsForDownload) == 0:
        return []

    if machineName == "hp":
        imDir = u"{}\\{}".format( imagesDir, commonLibrary.ReplaceSpecialCharacters( productName))
    else:
        imDir = u"{}/{}".format( imagesDir, commonLibrary.ReplaceSpecialCharacters( productName))
    if not os.path.exists(imDir):
        os.mkdir(imDir)

    successDownloadImages = []
    for picUrl in picsForDownload:
        picUrl_ = picUrl
        if picUrl_.find(sourceDomain) == -1:
            picUrl_ = sourceDomain + picUrl
        if commonLibrary.DownloadImage(picUrl_, imDir):
            successDownloadImages.append(picUrl)

    return successDownloadImages

def UpdateProductPictures(productName, productDescr, successDownloadImages):
    if len(successDownloadImages) == 0:
        return productDescr
    productDir = u"/wa-data/public/shop/img/" + productName.replace(" ", "_") + "/"
    ldElement = etree.HTML(productDescr)

    bNeedUpdate = False
    elements = ldElement.xpath("//img")
    for element in elements:
        picUrl = element.attrib["src"]
        if picUrl.find(productDir) == -1 and picUrl in successDownloadImages:
            bNeedUpdate = True
            picName = commonLibrary.GetPictureNameFromUrl(picUrl)
            element.attrib["src"] = productDir + picName

    description = commonLibrary.GetStringDescription(ldElement)
    return description

def CreateRoutingFile():
    content = """<?php
return array (
  'auto-hands.ru' =>
  array (
    {entries}

  ),
);"""

    entries = ""
    shops = list( prestashop_api.GetAllShopsFull())
    f = shops.pop(0)
    shops.append(f)
    i = 1
    for shop in shops:
        id_shop, domain, name, virtual_uri = shop
        points = [k[5] for k in commonLibrary.GetCityPvz(name)]
        if len(points) == 0:
            address = u"г. {}".format(name)
        else:
            random.shuffle(points)
            address = u"г. {}, {}".format(name, points[0])
        entry = u"""
    {} =>
    array (
      'url' => '{}*',
      'app' => 'shop',
      '_name' => '{}',
      'city' => ' в {}',
      'city2' => '{}',
      'pointAddress' => 'Адрес пункта выдачи: {}',
      'theme' => 'mastershopop',
      'theme_mobile' => 'mastershopop',
      'checkout_version' => '2',
      'locale' => 'ru_RU',
      'title' => '',
      'meta_keywords' => '',
      'meta_description' => '',
      'og_title' => '',
      'og_image' => '',
      'og_video' => '',
      'og_description' => '',
      'og_type' => '',
      'og_url' => '',
      'url_type' => '1',
      'products_per_page' => '',
      'type_id' =>
      array (
        0 => '6',
      ),
      'currency' => 'RUB',
      'public_stocks' => '0',
      'drop_out_of_stock' => '1',
      'payment_id' =>
      array (
        0 => '1',
        1 => '3',
        2 => '4077',
        3 => '4',
      ),
      'shipping_id' => '0',
      'ssl' => '1',
      'checkout_storefront_id' => 'f204ecd22662d673e53c6e7bd35df7eb',
    ),
    """.format(i, virtual_uri, commonLibrary.GetCompanyName(prestashop_api.siteUrl),commonLibrary.GetVariantFromTemplate(name, "[city_P]"),commonLibrary.GetVariantFromTemplate(name, "[city_V]"), address )
        entries += entry
        i += 1

    content = content.replace("{entries}", entries)
    with open("routing.php", "w") as f:
        f.write(content.encode("cp1251"))

def CreateRoutingFileSubdomains():
    entries = ""
    shops = list( prestashop_api.GetAllShopsFull())
    f = shops.pop(0)
    shops.append(f)
    for shop in shops:
        id_shop, domain, name, virtual_uri = shop
        points = [k[5] for k in commonLibrary.GetCityPvz(name)]
        if len(points) == 0:
            address = u"г. {}".format(name)
        else:
            random.shuffle(points)
            address = u"г. {}, {}".format(name, points[0])
        entry = u"""
  '{}' =>
    array (
    2 =>
    array (
      'url' => 'blog/*',
      'app' => 'blog',
      'theme' => 'mastershopog',
      'theme_mobile' => 'mastershopog',
      'locale' => 'ru_RU',
      'blog_url_type' => '9',
      'post_url_type' => '0',
      'title_type' => 'post',
      'title' => 'Отпугиватели грызунов',
      'meta_keywords' => '',
      'meta_description' => '',
      'rss_title' => 'Отпугиватели грызунов',
    ),
    1 =>
    array (
      'url' => '*',
      'app' => 'shop',
      '_name' => '{}',
      'city' => ' в {}',
      'city2' => '{}',
      'pointAddress' => 'Адрес пункта выдачи: {}',
      'theme' => 'mastershopog',
      'theme_mobile' => 'mastershopog',
      'checkout_version' => '2',
      'locale' => 'ru_RU',
      'title' => '',
      'meta_keywords' => '',
      'meta_description' => '',
      'og_title' => '',
      'og_image' => '',
      'og_video' => '',
      'og_description' => '',
      'og_type' => '',
      'og_url' => '',
      'url_type' => '1',
      'products_per_page' => '',
      'type_id' =>
      array (
        0 => '7',
      ),
      'currency' => 'RUB',
      'public_stocks' => '0',
      'drop_out_of_stock' => '1',
      'payment_id' =>
      array (
        0 => '1',
        1 => '3',
        2 => '4079',
        3 => '4',
      ),
      'shipping_id' => '0',
      'ssl' => '1',
      'checkout_storefront_id' => '38ef757cc1417912de490b60e1c8e977',
    ),
  ),""".format(domain, commonLibrary.GetCompanyName(prestashop_api.siteUrl),commonLibrary.GetVariantFromTemplate(name, "[city_P]"),commonLibrary.GetVariantFromTemplate(name, "[city_V]"), address )
        entries += entry

    with open("routing.php", "w") as f:
        f.write(entries.encode("cp1251"))

def AddCategoryToWebasyst(name, link_rewrite, description, active, parentCatName = None):
    if webasyst_api.GetCategoryId(name) != -1:
        return
    bAddCategory = True
    if active == 0:
        id = webasyst_api.GetCommonCategoryId(name)
        if id != -1:
            bAddCategory = False
    if bAddCategory == True:
        data = {}
        data["name"] = name
        if parentCatName != None:
            data["parentName"] = parentCatName
        data["url"] = link_rewrite
        data["description"] = description
        data["include_sub_categories"] = "1"
        data["status"] = active
        id = webasyst_api.AddCategory(data)
        if id == -1:
            return
        webasyst_api.SetCategorySortProducts(id, "total_sales DESC")
        webasyst_api.AddCategoryParams(id, "enable_sorting", 1)

    #привязываем категорию к витринам
    id_ = webasyst_api.GetCategoryId(name)
    if id_ == -1:
        shops = prestashop_api.GetAllShopsFull()
        for shop in shops:
            id_shop, domain, name, virtual_uri = shop
            domain = domain.replace("otpugivateli-grizunov.ru", "auto-hands.ru")
            if bSubdomains:
                route = "{}/*".format(domain)
            else:
                route = "{}/{}*".format(webasyst_api.siteUrl, virtual_uri)
            webasyst_api.AddCategoryRoute(id, route)

def CopyCategories(catId):
    subcats = prestashop_api.GetCategorySubcategoriesIds(catId)
    if catId != 2:
        parentCatName = prestashop_api.GetCategoryName(catId)
    for subcat in subcats:
        id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = prestashop_api.GetShopCategoryDetails(1, subcat)[0]
        active = prestashop_api.IsCategoryActive(id_category)
        if catId == 2:
            AddCategoryToWebasyst(name, link_rewrite, description, active)
        else:
            AddCategoryToWebasyst(name, link_rewrite, description, active, parentCatName)
        CopyCategories(id_category)

def CopyFeatures():
    ignoredFeatures = []
    addedFeaturesIds = []
    features = prestashop_api.GetFeaturesNames()
    wa_features = webasyst_api.GetFeaturesNames()
    for featureName in features:
        values = prestashop_api.GetFeatureValues(featureName)
        if len(values) < 2:
            continue
        print featureName
        if len(values) == 2 and values[0] in [u"да", u"нет"]:
            type_ = "boolean"
            if featureName in wa_features:
                id = webasyst_api.GetFeatureId(featureName)
                addedFeaturesIds.append(str(id))
                continue
        elif featureName in GetDoubleCharacteristics():
            type_ = "double"
        else:
            type_ = "varchar"
        if featureName not in wa_features + ignoredFeatures:#добавляем хар-ку
            ignoredFeatures.append(featureName)
            data = {}
            data["name"] = featureName
            data["code"] = commonLibrary.GetLinkRewriteFromName(featureName).replace("-","_")
            if featureName in GetDoubleCharacteristics():
                type_ = "double"
            data["type"] = type_
            if type_ in ["boolean", "double"]:
                data["selectable"] = 0
            else:
                data["selectable"] = 1
            if featureName in GetMultipleCharacteristics():
                data["multiple"] = 1
            else:
                data["multiple"] = 0
            id = webasyst_api.AddFeature(data)
            addedFeaturesIds.append(str(id))
            if type_ not in ["boolean"]:
                if type_ == "double":
                    values = [commonLibrary.GetIntValue(i) for i in values]
                for value_ in values:
                    webasyst_api.AddFeatureValue2(featureName, value_)
        else:#добавляем отсутствующие значения
            id = webasyst_api.GetFeatureId(featureName)
            addedFeaturesIds.append(str(id))
            wa_values = webasyst_api.GetFeatureValues(featureName)
            if type_ == "double":
                wa_values = [ int(i) for i in wa_values]
                values = [commonLibrary.GetIntValue(i) for i in values]
            for value_ in values:
                if value_ not in wa_values:
                    webasyst_api.AddFeatureValue2(featureName, value_)

    for id_ in addedFeaturesIds:
        if not webasyst_api.IsFeatureAddedToSite(id_):
            webasyst_api.AddFeatureToSiteById(id_)

    #активируем фильтры для всех категорий
    filter_ = "price," + ",".join(addedFeaturesIds)
    cats = webasyst_api.GetCategoriesIds(1)
    for catId in cats:
        webasyst_api.SetCategoryFilter(catId, filter_)

def CopyPages():
    shops = prestashop_api.GetAllShopsFull()
    for shop in shops:
        id_shop, domain, name, virtual_uri = shop
        print id_shop
        if id_shop < 2:
            continue
        pages = prestashop_api.GetShopPages(id_shop)
        for page in pages:
            meta_title, meta_description, meta_keywords, content, link_rewrite = page
            if meta_title in [u"Как оформить заказ", u"Скидки"]:
                continue
            if meta_title == u"Для юрлиц":
                meta_title = u"Информация для юрлиц"
            pageData = []
            if bSubdomains:
                domain = domain.replace("otpugivateli-grizunov.ru", "auto-hands.ru")
                pageData.append(domain)
                pageData.append("*")
            else:
                pageData.append( webasyst_api.siteUrl)
                pageData.append( "{}*".format(virtual_uri))
            pageData.append(  meta_title)
            if meta_title == u"Доставка":
                link_rewrite = "delivery"
            pageData.append( u"{}/".format(link_rewrite))
            pageData.append( CorrectPageContent(id_shop, meta_title, content))
            pageData.append( GetPageSortIndex(meta_title))
            webasyst_api.AddPageToDomain(pageData)

def CopyProducts():
    webasyst_api.InitAllProducts()
    products = prestashop_api.GetActiveProductsIDs()
    for productId in products:
        name, price, wholesale_price, description, link_rewrite, meta_title = prestashop_api.GetProductDetailsById(productId)
        originalName = commonLibrary.CleanItemName( prestashop_api.GetProductOriginalName(productId))
        if webasyst_api.GetProductIdQuick(originalName) != -1:
            continue
        data= {}
        data["name"] = name
        print name
        data["summary"] = prestashop_api.GetProductShortDescription(productId)
        description = commonLibrary.DeleteSalesData(description)
        successDownloadImages = DownloadProductImages(originalName, description)
        description = UpdateProductPictures(originalName, description, successDownloadImages)
        data["description"] = description
        data["url"] = link_rewrite
        data["price"] = price
        data["status"] = 1
        data["sku_type"] = 1
        id_ = webasyst_api.AddProduct(data)
        if id_ == -1:
            print u"ошибка добавления товара " + originalName
            continue

        if prestashop_api.IsProductInStock(productId) == False:
            webasyst_api.SetProductAvailableForOrder(id_, False)

        webasyst_api.SetProductOriginalName(id_, originalName)
        webasyst_api.SetProductTypeId(id_)

        #цены
        webasyst_api.SetProductPrices(id_, wholesale_price, price)

        #артикул
        article = prestashop_api.GetProductArticle(productId)
        p = re.compile('\D+')
        try:
            art = p.findall(article)[0] + str(id_)
        except:
            print u"отсутствует артикул"
##            continue
        webasyst_api.SetProductArticle(id_, art)

        #категории
        productCats = [prestashop_api.GetCategoryName(i) for i in prestashop_api.GetProductCategories(productId)]
        while -1 in productCats:
            productCats.remove(-1)
        wa_productCats = [webasyst_api.GetCategoryId(i) for i in productCats]
        webasyst_api.SetProductCategories(id_, wa_productCats)

        #фотографии
        productImages = webasyst_api.GetProductImagesIds(id_)
        if len(productImages) == 0:
            images = prestashop_api.GetProductImagesIds(productId)
            if len(images) > 0:
                imDir = u"{}\\{}".format( imagesDir, commonLibrary.ReplaceSpecialCharacters( originalName)).strip()
                if not os.path.exists(imDir):
                    os.mkdir(imDir)
            for image in images:
                imgUrl = "{0}/{1}-large_default/{2}.jpg".format(prestashop_api.GetShopMainDomain(1), image,link_rewrite )
                result = commonLibrary.DownloadImage(imgUrl, imDir)
                if result:
                    picName = commonLibrary.GetPictureNameFromUrl(imgUrl)
                    imgFilePath = u"{}\\{}".format(imDir, picName)
                    pictureId = webasyst_api.AddProductImage(id_, imgFilePath)
                    os.unlink(imgFilePath)

            if len(images) > 0:
                if len(os.listdir(imDir)) == 0:
                    os.rmdir(imDir)

        #характеристики
        features = prestashop_api.GetProductFeatures(productId)
        waProductFeatures = webasyst_api.GetProductFeatures(id_)
        for featureName in features.keys():
##            print featureName
            for featureValue in features[featureName]:
                if featureName in GetDoubleCharacteristics():
                    featureValue = commonLibrary.GetIntValue(featureValue)
                if featureName in waProductFeatures.keys():
                    if featureValue not in waProductFeatures[featureName]:
                        webasyst_api.AddProductFeatureValue(id_, featureName, featureValue)
                else:
                    webasyst_api.AddProductFeatureValue(id_, featureName, featureValue)


        #отзывы
        reviews = prestashop_api.GetProductReviewsById(productId)
        for review in reviews:
            webasyst_api.AddProductReview(id_, review)
        if len(reviews) > 0:
            rates = [float(i[7]) for i in reviews]
            rating = round(float(sum(rates)) / float(len(rates)), 2)
            webasyst_api.SetProductRating(id_, rating)

        #видео
        video = prestashop_api.GetProductVideo(productId)
        if video not in ["", None]:
            try:
                videoId = video.split("/embed/")[1].split('"')[0]
                videoUrl = """https://www.youtube.com/watch?v={}""".format(videoId)
                webasyst_api.SetProductVideo(id_, videoUrl)
            except:
                pass

        #хиты
        if prestashop_api.GetProductOnSale(productId):
            webasyst_api.SetProductBadge(id_, 'bestseller')

def CopyAccessoires():
    webasyst_api.InitAllProducts()
    products = prestashop_api.GetActiveProductsIDs()
    for productId in products:
##        if prestashop_api.IsProductInStock(productId) == False:
##            continue
        accIds = prestashop_api.GetProductAccessory(productId)
        accIds = list(set(accIds))
        if len(accIds) > 0:
            originalName = commonLibrary.CleanItemName(prestashop_api.GetProductOriginalName(productId))
            id_ = webasyst_api.GetProductIdQuick(originalName)
            relProducts = webasyst_api.GetProductRelatedProducts(id_, "cross_selling")
            webasyst_api.SetProductCrossSelling(id_, 2)
            for accId in accIds:
                accOriginalName = commonLibrary.CleanItemName(prestashop_api.GetProductOriginalName(accId))
                accId_ = webasyst_api.GetProductIdQuick(accOriginalName)
                if id_!= -1 and accId_ != -1 and accId_ not in relProducts:
                    webasyst_api.SetProductRelatedProducts(id_, "cross_selling", accId_)

def CopySeoTexts():
    shops = prestashop_api.GetAllShopsFull()
    webasyst_api.InitAllProducts()
    for shop in shops:
        id_shop, domain, name, virtual_uri = shop
        print id_shop
        logging.Log(id_shop)
        if id_shop != 1:
            continue
##        storefrontId = webasyst_api.GetStorefrontId(webasyst_api.siteUrl, name)
        #создаем витрину
        if bSubdomains == False:
##            url = "auto-hands.ru/{}*".format(virtual_uri)
            url = "otpugivateli-sobak.ru/{}*".format(virtual_uri)
        else:
            domain = domain.replace("otpugivateli-grizunov.ru", "auto-hands.ru")
            url = "{}/*".format(domain)
        storefrontId = webasyst_api.AddStorefront(name, url, "INCLUDE" )

        #город в заголовке
        webasyst_api.AddStorefrontSettings(storefrontId, "category_is_enabled", 1)
        city_P = commonLibrary.GetVariantFromTemplate(name, "[city_P]")
        webasyst_api.AddStorefrontSettings(storefrontId, "category_h1", u"{$category.name} в city_P".replace("city_P", city_P))

        #категории
        cats = prestashop_api.GetActiveCategoriesIds()
        for catId in cats:
            catName = prestashop_api.GetCategoryName(catId)
            waCatId = webasyst_api.GetCategoryId(catName)
            id_category, link_rewrite, meta_title, description, catname, long_description, meta_keywords, meta_description = prestashop_api.GetShopCategoryDetails(id_shop, catId)[0]
            descr = description.replace("<p/><p>","")
            webasyst_api.AddStorefrontCategorySettings(storefrontId, waCatId, "description", descr)
            webasyst_api.AddStorefrontCategorySettings(storefrontId, waCatId, "meta_title", meta_title)
            webasyst_api.AddStorefrontCategorySettings(storefrontId, waCatId, "meta_keywords", meta_keywords)
            webasyst_api.AddStorefrontCategorySettings(storefrontId, waCatId, "meta_description", meta_description)
            if long_description in [None, ""]:
                continue
            if webasyst_api.GetStorefrontCategorySettings(storefrontId, waCatId, "additional_description") == -1:
                pics = DownloadCategoryImages(long_description)
                long_description = UpdateCatsPictures(long_description, pics)
                webasyst_api.AddStorefrontCategorySettings(storefrontId, waCatId, "additional_description", long_description)

        #товары
        webasyst_api.AddStorefrontSettings(storefrontId, "product_is_enabled", 1)
        products = prestashop_api.GetActiveProductsIDs()
        for productId in products:
            name, price, wholesale_price, description, link_rewrite, meta_title = prestashop_api.GetProductDetailsById(productId)
            originalName = commonLibrary.CleanItemName( prestashop_api.GetProductOriginalName(productId))
            waProductId = webasyst_api.GetProductIdQuick(originalName)
            if waProductId == -1:
                continue
            meta_title, meta_description, meta_keywords = prestashop_api.GetShopProductMetaTagsById(productId, id_shop)
            webasyst_api.AddStorefrontProductSettings(storefrontId, waProductId, "meta_title", meta_title)
            webasyst_api.AddStorefrontProductSettings(storefrontId, waProductId, "meta_keywords", meta_keywords)
            webasyst_api.AddStorefrontProductSettings(storefrontId, waProductId, "meta_description", meta_description)

        #главная
        title, description, keywords, url_rewrite = prestashop_api.GetShopDefaultPageTags(id_shop, "index")
        webasyst_api.AddStorefrontSettings(storefrontId, "home_page_is_enabled", 1)
        webasyst_api.AddStorefrontSettings(storefrontId, "home_page_meta_description", description)
        webasyst_api.AddStorefrontSettings(storefrontId, "home_page_meta_title", title)
        webasyst_api.AddStorefrontSettings(storefrontId, "home_page_meta_keywords", keywords)

def CopyArticles():
    blog_id = 9
    contact_id = 2360
    contact_name = u"Отпугиватели грызунов"
    webasyst_api.InitAllProducts()
    articlesTitles = [i[6] for i in webasyst_api.GetBlogArticles(blog_id) ]
    id_shop = prestashop_api.GetShopIdByName(u"Санкт-Петербург")
    mainDomain = prestashop_api.GetShopMainDomain(id_shop)
    ids = prestashop_api.GetShopArticles(id_shop)
    for id in ids:
        id_smart_blog_post, meta_title, content, link_rewrite, short_description, created = prestashop_api.GetArticleData(id)
        if meta_title in articlesTitles:
            continue
        content = CorrectArticle(id_shop, content, mainDomain)
##        print content
        data = {}
        data["blog_id"] = blog_id
        data["contact_id"] = contact_id
        data["contact_name"] = contact_name
        data["datetime"] = created
        data["title"] = meta_title
        data["text"] = content
        p = re.compile('(\(id_cat\d+\))')
        links = p.findall(short_description)
        for link in links:
            short_description = short_description.replace(link, u"")
        data["text_before_cut"] = short_description
        data["link_rewrite"] = u"{}_{}".format(id_smart_blog_post, link_rewrite.replace(u"-sankt-peterburg", "").replace(u"-Санкт-Петербург", ""))
        data["meta_title"] = meta_title
        data["meta_keywords"] = ""
        data["meta_description"] = ""
        webasyst_api.AddArticle(data)

def CopyCustomers():
    ids = prestashop_api.GetAllCustomers()
    for id in ids:
        if len(prestashop_api.GetCustomerOrdersIds(id)) == 0:
            continue
        firstname, lastname, email, phone_mobile, city = prestashop_api.GetCustomerData(id)
        if webasyst_api.GetCustomerId(phone_mobile, email) != -1:
            continue
        data = {}
        data["firstname"] = firstname
        data["lastname"] = lastname
        data["email"] = email
        data["phone_mobile"] = phone_mobile
        data["city"] = city
        webasyst_api.AddCustomer(data)

def AddCustomersToShop():
    customersIds = webasyst_api.GetContactsIds("shop")
    for customerId in customersIds:
        if webasyst_api.IsCustomerInShop(customerId):
            continue
        orders = webasyst_api.GetCustomerOrdersIds(customerId)
        if len(orders) == 0:
            continue
        total_spent = 0
        for orderId in orders:
            orderCost = webasyst_api.GetOrderTotalCost(orderId)
            total_spent += orderCost

        data = {}
        data["total_spent"] = total_spent
        data["number_of_orders"] = len(orders)
        data["last_order_id"] = orders[-1]
        webasyst_api.AddCustomerToShop(customerId, data)

def CopyOrders():
    endDate = datetime.date.today() + datetime.timedelta(days=1)
    webasyst_api.InitAllProducts()
    states = [u"В процессе подготовки", u"Данного товара нет на складе", u"Доставлен и оплачен", u"Вручен", u"Отправлен", u"Доставлен"]
    orders = [x[0] for x in prestashop_api.GetOrdersIds(states, '2017-06-23', endDate)]
    for orderId in orders:
        orderData = {}

        #дата
        orderData["datetime"] = prestashop_api.GetOrderCreationDate(orderId)

        #покупатель
        id_customer = prestashop_api.GetOrderCustomer(orderId)[-1]
        firstname, lastname, email, phone_mobile, name = prestashop_api.GetCustomerData(id_customer)
        waCustomerId = webasyst_api.GetCustomerId(phone_mobile, email)
        if waCustomerId == -1:
            continue

        if len(webasyst_api.GetCustomerOrdersIds(waCustomerId)) != 0:#если заказы уже добавлены
            continue

        orderData["contact_id"] = waCustomerId

        #статус
        ps_state = prestashop_api.GetOrderState(orderId)
        trackCode = prestashop_api.GetOrderTrackingNumber(orderId)
        if ps_state == u"Доставлен":
            if len(trackCode) == 10:
                wa_state = "dostavlen"
            else:
                wa_state = "dostavlen-v-poch"
        elif ps_state == u"В процессе подготовки":
            wa_state = "processing"
        else:
            wa_state = webasyst_api.ordersStates[ps_state]
        orderData["state_id"] = wa_state

        #товары
        totalCost = 0
        products = prestashop_api.GetOrderItems(orderId)
        orderData["products"] = []
        for product in products:
            product_id, product_name, unit_price_tax_incl, product_quantity, purchase_supplier_price, product_attribute_id = product
            totalCost += product_quantity * unit_price_tax_incl
            waProductId = webasyst_api.GetProductIdQuick(prestashop_api.GetProductOriginalName(product_id))
            if waProductId == -1:
                continue

            name, sku_id, sku_code = webasyst_api.GetProductData(waProductId)
            orderData["products"].append([waProductId, product_name, sku_id, sku_code, product_quantity, unit_price_tax_incl, purchase_supplier_price])

        if len(orderData["products"]) == 0:
            continue

        orderData["total"] = totalCost

        #параметры
        shopId = prestashop_api.GetOrderShopId(orderId)
        if shopId == -1:
            continue
        virtualUri = prestashop_api.GetShopVirtualUri(shopId)
        shopName = prestashop_api.GetShopName(shopId)

        orderParams = {}
        orderParams["shipping_address.country"] = "rus"
        orderParams["shipping_address.city"] = shopName
        try:
            region = commonLibrary.GetCityPvz(shopName)[0][-3]
            regionCode = webasyst_api.GetRegionCode(region)
        except:
            regionCode = "78"
        orderParams["shipping_address.region"] = regionCode
        if bSubdomains == False:
            storefront = "{}/{}".format(prestashop_api.siteUrl, virtualUri)
        else:
            storefront = prestashop_api.GetShopDomain(shopId)
        orderParams["sales_channel"] = "storefront:{}".format(storefront)
        orderParams["storefront_decoded"] = orderParams["storefront"] = storefront
        orderParams["departure_datetime"] = orderData["datetime"]
        params = [
        "shipping_address.street",
        "shipping_address.zip",
        "billing_address.street",
        "billing_address.city",
        "shipping_address.lng",
        "shipping_address.lat",
        "shipping_tax_id",
        "referer_host",
        "billing_address.lat",
        "coupon_id"]
        orderParams["shipping_currency_rate"] = "1"
        orderParams["shipping_currency"] = "RUB"
        orderParams["shipping_plugin"] = "courier"
        orderParams["shipping_name"] = u"Курьер"
        orderParams["shipping_id"] = 8
        orderParams["shipping_rate_id"] = 1
        orderParams["payment_id"] = 1
        orderParams["payment_name"] = u"Наличные"
        for param in params:
            orderParams[param] = ""

        wa_orderId = webasyst_api.AddOrder(orderData, orderParams )
        if wa_state == "completed" or commonLibrary.GetOrderPaid(prestashop_api.GetOrderNote(orderId)):
            webasyst_api.SetOrderPaid(wa_orderId, orderData["datetime"])

        #трекинг-код
        if trackCode not in ["", -1, None]:
            webasyst_api.SetOrderTrackingNumber(wa_orderId, trackCode)

        #комментарии
        comment = prestashop_api.GetOrderNote(orderId)
        webasyst_api.SetOrderComment(wa_orderId, comment)

    AddCustomersToShop()

def CopyNotifications():
    urls = webasyst_api.GetStorefrontsUrls()
    notifications = webasyst_api.GetActiveNotificationsIds("mini-camera.ru.com")
    for id in notifications:
        if id == 2:
            continue
        name, event, transport, status = webasyst_api.GetNotificationData(id)
        name = name + u" ({})".format("otpugivateli-grizunov.ru")
        id_ = webasyst_api.AddNotification(name, event)

        #параметры
        params = webasyst_api.GetNotificationParams(id)
        for param in params:
            name, value = param
            #изменение домена
            value = value.replace("mini-camera.ru.com", "otpugivateli-grizunov.ru")
            value = value.replace('{$wa->shop->settings("name")}', u"Отпугиватели грызунов")
            value = value.replace(u'\xd7', "x")
            value = value.replace(u'\u2212', "-")
            value = value.replace(u"'", '"')
            #изменение номера

            webasyst_api.AddNotificationParam(id_, name, value)

        #привязка к витринам
        for url in urls:
            webasyst_api.AddNotificationSource(id_, url)

def CopyProductBadges():
    webasyst_api.InitAllProducts()
    products = prestashop_api.GetActiveProductsIDs()
    for productId in products:
        originalName = commonLibrary.CleanItemName( prestashop_api.GetProductOriginalName(productId))
        id = webasyst_api.GetProductIdQuick(originalName)
        if id == -1:
            continue
        if prestashop_api.GetProductOnSale(productId):
            webasyst_api.SetProductBadge(id, 'bestseller')

def CreateSiteDirs():
    shops = prestashop_api.GetAllShopsFull()
    for shop in shops:
        id_shop, domain, name, virtual_uri = shop
        ftp.mkd('wa-data/public/site/data/{}'.format(domain))

def CreateRobotsTxt():
    robots = u"""
User-agent: *

Sitemap: https://{0}/wa-data/public/site/data/{1}/sitemap-{2}.xml

# wa shop *
Disallow: /webasyst*
Disallow: /index.php/my/
Disallow: /index.php/checkout/
Disallow: /for-organizations/
Disallow: /payment/
Disallow: /about-us/
Disallow: /garantii-i-pravila-vozvrata/
Disallow: /politika-konfidencialnosti/
Disallow: /delivery/
Disallow: /order/
Disallow: /checkout/
Disallow: /?sort=*
Disallow: /?page=*
Disallow: /compare/
Disallow: /my/
Disallow: /search*
Disallow: /login/
Disallow: /signup/
Disallow: /blog/author/*
Disallow: /posadochnye/"""
##    sitemap-abakan.xml
    shops = prestashop_api.GetAllShopsFull()
    for shop in shops:
        id_shop, domain, name, virtual_uri = shop
        if domain == "otpugivateli-grizunov.ru":
            city = ""
        else:
            city = domain.split(".")[0]
        robots_ = robots.format(domain, domain, city)
        if id_shop == 1:
            ftp.cwd('wa-data/public/site/data/{}'.format(domain))
        else:
            ftp.cwd('../{}'.format(domain))
        with open("robots.txt", "w") as f:
            f.write(robots_.encode("utf-8"))
        ftp.storlines('STOR robots.txt', open("robots.txt", "r"))

############################################################################################################################

logfile = log.GetLogFileName("log", "export")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()
bSubdomains = True

if machineName == "hp":
    imagesDir = "img\\products"
    sourceCatsDir = "img\\cats"
    sourcePagesDir = "img\\pages"
    sourceArticlesDir = "img\\artiles"
else:
    imagesDir = "pictures/products"
    sourceCatsDir = "pictures/cats"
    sourcePagesDir = "pictures/pages"
    sourceArticlesDir = "pictures/artiles"
destCatsDir = "/wa-data/public/shop/img/cats"
destPagesDir = "/wa-data/public/shop/img/pages"
destArticlesDir = "/wa-data/public/blog/img"

webasyst_api = WebasystAPI.WebasystAPI()
prestashop_api = PrestashopAPI.PrestashopAPI()
webasyst_api = commonLibrary.CreateWebasystSession("http://auto-hands.ru")
prestashop_api = commonLibrary.CreatePrestashopSession("http://otpugivateli-grizunov.ru")

##if bSubdomains:
##    CreateRoutingFileSubdomains()
##else:
##    CreateRoutingFile()
##CopyCategories(2)
##CopyFeatures()
##CopyPages()
##CopyProducts()
##CopyAccessoires()
##CopySeoTexts()
##CopyArticles()
##CopyCustomers()
##CopyOrders()
##CopyNotifications()
if bSubdomains:
    ftp = commonLibrary.CreateFtpSession("95.217.23.106")
##    CreateSiteDirs()
    CreateRobotsTxt()
