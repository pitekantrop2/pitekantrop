﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess
import SendMails

def GetShopPvzList(shopName):
    pvzList = []
    pvzNodes = pvzNodes = root.xpath("//Pvz[(starts-with(@City, '" + shopName + "') and contains(@City, ',')) or (@City = '" + shopName + "' and not(contains(@City, ',')))]")
    for pvzNode in pvzNodes:
        pvzData = {}
        pvzData["WorkTime"] = pvzNode.attrib["WorkTime"]
        pvzData["Address"] = pvzNode.attrib["Address"]
        pvzData["coordX"] = pvzNode.attrib["coordX"]
        pvzData["coordY"] = pvzNode.attrib["coordY"]
        pvzData["Phone"] = pvzNode.attrib["Phone"]
        pvzData["Name"] = pvzNode.attrib["Name"]
        pvzList.append(pvzData)

    return pvzList

#########################################################################################################################

mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "updateStores")
logging = log.Log(logfile)

resp = requests.get("https://integration.cdek.ru/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

now = datetime.datetime.now()

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        logging.Log(str(shopId))

        print shopId

        shopName = commonLibrary.GetShopName(shopName)
        shopStores = prestashop_api.GetShopStores(shopName)
        shopStoresNames = [x[0] for x in shopStores]
        shopStoresAddresses = [x[1] for x in shopStores]

        pvzList = GetShopPvzList(shopName)
        for pvz in pvzList:
            try:
                index = shopStoresAddresses.index(pvz["Address"])
            except:
                index = -1
            if index == -1:
                #добавляем пункт
                sql = u"""INSERT INTO ps_store( id_country, id_state, name, address1, address2, city, postcode, latitude, longitude, hours, phone, fax, email, note, active, date_add, date_upd)
                          VALUES (177,0,'{0}','{1}','','{2}','',{3},{4},'{5}','{6}','','','',1,'{7}','{7}')""" \
                          .format(pvz["Name"], pvz["Address"],shopName, pvz["coordY"], pvz["coordX"], pvz["WorkTime"], pvz["Phone"], now)

                prestashop_api.MakeUpdateQueue(sql)

            else:
                shopStoresAddresses.pop(index)

        #удаляем устаревшие пункты выдачи
        for shopStoreAddress in shopStoresAddresses:
            prestashop_api.DeleteStore(shopName, shopStoreAddress)

    if machineName.find("hp") != -1:
        ssh.kill()



