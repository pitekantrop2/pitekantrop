﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import html2text
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess
import SendMails
import xlrd


#########################################################################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()

sql = """SELECT * FROM sdek_cities_codes"""
citiesTable = prestashop_api.MakeLocalDbGetInfoQueue(sql)
citiesIds = [str(int(x[0])) for x in citiesTable]

fileName = "City_RUS_20200119.xls"

workbook = xlrd.open_workbook(fileName, formatting_info=True)
sheet = workbook.sheets()[0]
num_rows = sheet.nrows - 1
for i in range(1, num_rows):
    rowValues = sheet.row_values(i, start_colx=0, end_colx=4)
    id, full_name, city_name, obl_name = rowValues
    id = str(int(id))
    if id not in citiesIds:
        sql = u"""INSERT INTO sdek_cities_codes(id, full_name, city_name, obl_name)
                 VALUES ({0},'{1}','{2}','{3}')""".format( id, full_name, city_name, obl_name)
        prestashop_api.MakeLocalDbUpdateQueue(sql)


