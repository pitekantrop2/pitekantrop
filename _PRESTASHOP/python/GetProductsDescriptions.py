﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import html2text
import PrestashopAPI
import os
import re, urlparse
import HTMLParser
import commonLibrary
import subprocess


def AddProductDescription(productID, productInfo):
    global textLength
    global listCounter

    productDescr = productInfo[3].replace(u"\u02da","").replace(u"\xb2","").replace(u"\xba","").replace(u"\xb3","")
    productTitle = productInfo[0]
    print productTitle

    try:
        ldElement = etree.HTML(productDescr)
    except:
        return

    elements = ldElement.xpath("//div[contains(@style, 'background') and contains(., '" + u"Требования к каналу Интернет"  + "')]")
    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath("//div[contains(@style, 'background') and contains(., '" + u"Выпущена новая версия прошивки"  + "')]")
    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath("//div[contains(@style, 'background') and contains(., '" + u"Как увеличить расстояние"  + "')]")
    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath("//div[contains(@style, 'background') and contains(., '" + u"подарок"  + "')]")
    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath("//img")
    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath("//a")
    for element in elements:
        element.getparent().remove(element)

    elements = ldElement.xpath("//h3")
    elements += ldElement.xpath("//h3/strong")
    elements += ldElement.xpath("//h2")
    for element in elements:
        if element.text not in [None,""]:
            element.text = "{h3} " + element.text + " {h3}"
##    elements = ldElement.xpath("//table")
##    for element in elements:
##        element.getparent().remove(element)

    productDescr = commonLibrary.GetStringDescription(ldElement)

##    index = productDescr.find(u'Технические характеристики')
##    if index != -1:
##        productDescr = productDescr[0:index]
##
##    index = productDescr.find(u'Технические  характеристики')
##    if index != -1:
##        productDescr = productDescr[0:index]

    index = productDescr.find(u'Комплектация')
    if index != -1:
        productDescr = productDescr[0:index]

    index = productDescr.find(u'Особенности и преимущества')
    if index != -1:
        productDescr = productDescr[0:index]

    try:
        productDescr = html2text.html2text (productDescr)
    except:
        return

    chars = ["###", "_", "**", u'\u25cf', u'\u2264', u'\u2265', u'\xd7', u'\uff08', u'\u200b']
    for char in chars:
        productDescr = productDescr.replace(char, "")

    try:
        textLength += len(productDescr.encode("cp1251"))
    except:
        return

    if textLength > maxTextLength:
        listCounter += 1
        textLength = 0

    with open( "{0}_{1}.txt".format(u"описания".encode("cp1251"), str(listCounter)), "a") as myfile:
        myfile.write("=================== " + productTitle.encode("cp1251") + " (" + str(productID) +  ") ===================\n")
        myfile.write(productDescr.encode("cp1251"))


def AddCategoryDescription(id_category, description, name, long_description):
    if description != "":
        ldElement = etree.HTML(description)
        elements = ldElement.xpath("//p[contains(., '" + u"оформив заказ в "  + "') ]")

        for element in elements:
            element.getparent().remove(element)

        description = commonLibrary.GetStringDescription(ldElement)

    if long_description != "":
        ldElement = etree.HTML(long_description)
        elements = ldElement.xpath("//p[ contains(., '" + u"г. Липецк"  + "') or contains(., '" + u"доставкой на пункт"  + "')]")

        for element in elements:
            element.getparent().remove(element)

        long_description = commonLibrary.GetStringDescription(long_description)

    description = html2text.html2text (description).replace("###", "").replace("**", "").replace("[", "").replace("]", "").replace("\\", "")
    long_description = html2text.html2text (long_description).replace(u"\xd7","").replace("###", "").replace("**", "").replace("[", "").replace("]", "").replace("\\", "")

    description = re.sub(r'\([^)]*\)', '', description)
    long_description = re.sub(r'\([^)]*\)', '', long_description)

    description = description.replace(u"\u2192","")
    long_description = long_description.replace(u"\u2192","")

    with open(descrFile, "a") as myfile:

        myfile.write( name.encode("cp1251") + "({0})\n\n".format(id_category))

        if len(description) > 150:
            myfile.write(u"-====== Основное описание ======-\n".encode("cp1251"))
            myfile.write(description.encode("cp1251") + "\n\n")

        if len(long_description) > 500:
            myfile.write(u"-====== Дополнительное описание ======-\n".encode("cp1251"))
            myfile.write(long_description.encode("cp1251"))

        if len(description) > 150 or len(long_description) > 500:
            myfile.write( "\n\n******************************************************\n\n")


def MakePrice(shopId):
    descrFile = u"""price.txt"""

    if os.path.exists(descrFile):
        os.unlink(descrFile)

    shopCategories = prestashop_api.GetShopCategories(shopId)

    for shopCategory in shopCategories:

        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory

        if categoryID in [1,2]:
            continue

        if prestashop_api.IsCategoryActive(categoryID) == 0:
            continue

        with open(descrFile, "a") as myfile:
            myfile.write("\n\n=================== " + name.encode("cp1251") + " ===================\n\n")

        products = prestashop_api.GetCategoryProductsIds(categoryID)

        for productID in products:

            productId = productID[0]

            if prestashop_api.IsProductActive(productId) == 0:
                continue

            if prestashop_api.IsProductInStock(productId) == 0:
                continue

            details = prestashop_api.GetShopProductDetailsById(productId, 1)

            with open(descrFile, "a") as myfile:

                myfile.write(details[0].encode("cp1251") + " - " + str(int(details[1])) + u" руб.\n".encode("cp1251"))


def SaveProductDescriptionAsHtml(productId, productInfo):
    name = productInfo[0].replace("\"","").replace("/","").replace("\r\n","")
    print name
    path = folderPath + name + "_{0}.html".format(productId)
    if os.path.exists(path):
        return

    descr = productInfo[3]
    chars = [u"\xd8",u"\xba",u"\u2264",u"\u2212",u"\u02da",u"\xd7", u"\xf7", u"\u2219"]
    for char in chars:
        descr = descr.replace(char, "")

    with open(path, "w") as myfile:
        myfile.write(descr.encode("cp1251"))

################################################################################


textLength = 0
maxTextLength = 10000
listCounter = 0
machineName = commonLibrary.GetMachineName()

folderPath = u"c:\\work\\for safetus\\сео\\товары\\"

descrFile = u"""descr.txt"""

if os.path.exists(descrFile):
    os.unlink(descrFile)

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen([commonLibrary.puttyPath, '-v', '-ssh', '-2', '-P', '22', '-C', '-l', 'root', '-pw', "IiRMBuAiJTLv", '-L', '5555:127.0.0.1:3306', site["ip"]])

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

##    productsIDs = prestashop_api.GetProductsIDs()
    productsIDs = prestashop_api.GetContractorProductsIds("v")
    ##productsIDs = prestashop_api.GetCategoryProductsIds(258)
    for productID in productsIDs:

##        productID = productID[0]

        productInfo = prestashop_api.GetProductDetailsById(productID)
        productCategoryDefaultId = prestashop_api.GetProductCategoryDefault(productID)

    ##    if productCategoryDefaultId not in [99, 102]:
    ##        continue

        if prestashop_api.IsProductInStock(productID) == False:
            continue

        if prestashop_api.IsProductActive(productID) == 1:
            continue

        AddProductDescription(productID, productInfo)
##        SaveProductDescriptionAsHtml(productID, productInfo)

    if machineName.find("hp") != -1:
        ssh.kill()

##    shopCategories = prestashop_api.GetShopCategories(51)
##
##    for shopCategory in shopCategories:
##
##        id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory
##
##        if description == None:
##            description = ""
##        if long_description == None:
##            long_description = ""
##
##        if id_category in (1,2):
##            continue
##
##        if prestashop_api.IsCategoryActive(id_category) == 0:
##            continue
##
##        print id_category
####        print name
##
##        AddCategoryDescription(id_category, description, name, long_description)
##
##    if machineName.find("hp") != -1:
##        ssh.kill()