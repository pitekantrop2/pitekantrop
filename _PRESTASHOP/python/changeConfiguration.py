﻿# -*- coding: utf-8 -*-


import lxml
from lxml import etree
import log
import datetime
import PrestashopAPI
import SdekAPI
import commonLibrary
import time
import random
import TextruAPI

def ConfigureMenu(shopId, value, update = False):
    if update == False:
        sql = """INSERT INTO ps_configuration (id_shop_group, id_shop, name, value, date_add, date_upd)
                 VALUES (1, %(shopID)s,'MOD_BLOCKTOPMENU_ITEMS', '%(value)s', '2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"shopID":shopId,
                 "value":value}

        prestashop_api.MakeUpdateQueue(sql)

    else:

        sql = """UPDATE ps_configuration
                 SET value = '%(value)s'
                 WHERE id_shop = %(shopID)s AND name = 'MOD_BLOCKTOPMENU_ITEMS'"""%{"shopID":shopId,
                 "value":value}

        prestashop_api.MakeUpdateQueue(sql)


def ConfigurePhone(shopId, shopName):
    city = commonLibrary.GetCity(shopName,2)

    value = u"(бесплатно для " + city + u")"

    sql = """INSERT INTO ps_configuration (id_shop_group, id_shop, name, value, date_add, date_upd)
             VALUES (1, %(shopID)s,'BLOCKCONTACT_TELNUMBER', '%(value)s', '2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"shopID":shopId,
             "value":value}

    prestashop_api.MakeUpdateQueue(sql)

def AddSeoText(shopId, shopName):
    city = commonLibrary.GetCity(shopName)
    seoText = u"""<h2 class="top_text">Удивительные подарки для мужчин, удивительные подарки для женщин купить в {city}, удивительные вещи для детей, интерактивные игрушки, игрушки-роботы купить в {city}, электронные светодиодные свечи, проекторы-ночники, беспроводные бэби-мониторы купить в {city}, популярные товары, новинки, удивительные вещи для безопасности купить в {city}, охранное видеонаблюдение, системы охраны с gsm-дозвонщиком, беспроводные охранные системы купить в {city}, персональные металлодетекторы, автономные системы безопасности, персональные и антикражные сирены купить в {city}, удивительные сувениры, часы, мышки купить в {city}, usb-хабы, колонки, ручки купить в {city}, лампы, будильники, mp3-плееры купить в {city}, винные аксессуары, наушники, копилки купить в {city}, прочие сувениры, товары на главной, удивительные вещи для дачников и путешественников купить в {city}, удивительные вещи для автомобилистов, борьба с вредителями, уничтожители грызунов купить в {city}, отпугиватели птиц, уничтожители насекомых, отпугиватели собак купить в {city}, отпугиватели насекомых, отпугиватели змей, универсальные отпугиватели купить в {city}, отпугиватели кротов, спецпредложения, удивительные вещи для дома купить в {city}, паровая техника, роботы-пылесосы, приборы для измерений купить в {city}, вакуумные пакеты, часы и метеостанции, удивительные вещи для развлечения купить в {city}, небесные фонарики, удивительные вещи для кухни, удивительные вещи для спорта купить в {city}, удивительные вещи для красоты и здоровья, массажеры, корректирующее белье купить в {city}, волшебные бигуди, разные товары для детей, разные товары для дома купить в {city}</h2>"""
    seoText = seoText.replace("{city}", city)

    last_insert_id = prestashop_api.GetLastInsertedId("ps_configuration","id_configuration") + 1

    sql = """INSERT INTO ps_configuration (id_configuration, id_shop_group, id_shop, name, value, date_add, date_upd)
             VALUES (%(last_insert_id)s, 1, %(shopID)s,'blockhtml_body', '%(seoText)s', '2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"shopID":shopId,
             "seoText":seoText, "last_insert_id":last_insert_id}

    prestashop_api.MakeUpdateQueue(sql)

def ConfigureReinsurance(shopId, filesNames, texts):
    global id
    for i in range(0, len(filesNames)):

        fileName = filesNames[i]
        text = texts[i]

        sql = """INSERT INTO ps_reinsurance (id_reinsurance, id_shop, file_name)
                 VALUES ({0}, {1}, '{2}')""".format( id, shopId, fileName)

        prestashop_api.MakeUpdateQueue(sql)

        sql = u"""INSERT INTO ps_reinsurance_lang (id_lang, id_reinsurance, text)
                 VALUES (1, {0}, '{1}')""".format( id, text)

        prestashop_api.MakeUpdateQueue(sql)
        id += 1


def AddPageToShop(shopId, pageId):
    ps_cms_block_page_query = """INSERT INTO ps_cms_block_page( id_cms_block, id_cms, is_category)
                                VALUES (%(shopId)s,%(pageId)s,0)"""%{"shopId":shopId, "pageId":pageId}

    prestashop_api.MakeUpdateQueue(ps_cms_block_page_query)

def UpdateRussianPostDeliveryCost():
    pointName = u"Почтой России."

    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    id_carrier = prestashop_api.GetShopCarrierIdByName(shopId, pointName)
    if id_carrier == -1:
        return

     #удаляем старые записи
    sql = """DELETE FROM ps_range_price
             WHERE id_carrier = %(carrierId)s"""%{"carrierId":id_carrier}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """DELETE FROM ps_delivery
             WHERE id_carrier = %(carrierId)s"""%{"carrierId":id_carrier}

    prestashop_api.MakeUpdateQueue(sql)

    id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

    rangesIds = []
    for k in range(0, 21):
        rangesIds.append(id_range_price + k)

    counter = 0

    for id_range_price in rangesIds:
        delimiter1 = 1000 * counter + 1

        if counter == 20:
            delimiter2 = 1000001
        else: delimiter2 = 1000 * (counter + 1)

        sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                 VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                 "id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

        prestashop_api.MakeUpdateQueue(sql)

        if counter == 20:
            delimiter2 = 21000

        id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
##            if prestashop_api.siteUrl in ["https://otpugiwatel.com", "http://tomsk.otpugivatel.com", ]:
##                price = 250
##            else:
        price = 400

        price = price + delimiter2 * 0.03

        price = int(10 * round(float(price)/10))

        sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                 VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                 "id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        counter += 1


def UpdateCarriersCost( carrierType):
    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)
    if shopCarriersNames == -1:
        return

    for carrierName in shopCarriersNames:
        if carrierName.find(carrierType) == -1:
            continue

        carrierId = prestashop_api.GetShopCarrierIdByName(shopId, carrierName)
##        print carrierName

        pvzNodes = root.xpath("//Pvz[(starts-with(@City, '" + shopName + "') and contains(@City, ',')) or (@City = '" + shopName + "' and not(contains(@City, ',')))]")

        if len(pvzNodes) == 0:
            return

        if carrierType == u"На пункт выдачи":
            tariffs = [234, 136, 10, 62]
        else:
            tariffs = [137, 233, 11]
        minCost = 100000
        calcResult = None

        for tariffId in tariffs:
            requestData = {}
            requestData['version'] = "1.0"
            requestData['dateExecute'] = datetime.date.today().strftime('%Y-%m-%d')
            requestData['senderCityId'] = 44
            requestData['receiverCityId'] = int(pvzNodes[0].attrib["CityCode"])
            requestData['tariffId'] = tariffId
            if prestashop_api.siteUrl == "http://saltlamp.su":
                requestData['goods'] = [{'weight':4, 'length':20, 'width':20,'height':20}]
            elif prestashop_api.siteUrl == "http://sushilki.ru.com":
                requestData['goods'] = [{'weight':4, 'length':30, 'width':30,'height':40}]
            elif prestashop_api.siteUrl == "https://incubators.shop":
                requestData['goods'] = [{'weight':5, 'length':50, 'width':40,'height':30}]
            elif prestashop_api.siteUrl == "http://gemlux-shop.ru":
                requestData['goods'] = [{'weight':4, 'length':30, 'width':30,'height':30}]
            else:
                requestData['goods'] = [{'weight':1, 'length':20, 'width':20,'height':20}]
            calcResult_ = sdek.Calculate(requestData)

            if "error" in calcResult_.keys():
                continue

            if int(calcResult_["result"]["price"]) < minCost:
                minCost = int(calcResult_["result"]["price"])
                calcResult = calcResult_
                print tariffId

        if calcResult == None:
            print -1
            continue

        delay = u"Срок доставки (рабочих дней): "
        if calcResult["result"]["deliveryPeriodMin"] == calcResult["result"]["deliveryPeriodMax"]:
            delay += str(calcResult["result"]["deliveryPeriodMax"] + 1)
        else:
            delay += str(calcResult["result"]["deliveryPeriodMin"] + 1) \
                    + " - " + str(calcResult["result"]["deliveryPeriodMax"] + 1)

        delay += "."

        sql = u"""UPDATE ps_carrier_lang
                 SET delay = '{0}'
                 WHERE id_carrier = {1}""".format(delay, carrierId)

        prestashop_api.MakeUpdateQueue(sql)

        #удаляем старые записи
        sql = """DELETE FROM ps_range_price
                 WHERE id_carrier = %(carrierId)s"""%{"carrierId":carrierId}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """DELETE FROM ps_delivery
                 WHERE id_carrier = %(carrierId)s"""%{"carrierId":carrierId}

        prestashop_api.MakeUpdateQueue(sql)

        #добавляем новые записи
        id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

        rangesIds = []
        for k in range(0, 21):
            rangesIds.append(id_range_price + k)

        counter = 0

        for id_range_price in rangesIds:
            delimiter1 = 1000 * counter + 1

            if counter == 20:
                delimiter2 = 1000001
            else:
                delimiter2 = 1000 * (counter + 1)

            sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                     VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                     "id_carrier":carrierId, "delimiter1":delimiter1, "delimiter2":delimiter2}

            prestashop_api.MakeUpdateQueue(sql)

            if counter == 20:
                delimiter2 = 21000

            id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
            price = int(calcResult["result"]["price"])
##            if prestashop_api.siteUrl in ["https://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
##                if shopId == 1 or shopId == 7:
##                    price = price + delimiter2 * 0.03
##                else:
##                    price = price + delimiter2 * 0.06
##            else:
##                price = price + delimiter2 * 0.03

            price = price + delimiter2 * 0.03

            price = int(10 * round(float(price)/10))

            sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                     VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                     "id_shop":shopId, "id_carrier":carrierId, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

            prestashop_api.MakeUpdateQueue(sql)

            counter += 1

def ConfigureMainPageText(shopId):
    sql = """SELECT id_info FROM ps_info
             WHERE id_shop = %(shopId)s"""%{"shopId":shopId}

    data = prestashop_api.MakeGetInfoQueue(sql)

    id_info = data[0][0]

    city = commonLibrary.GetCity(shopName)

    infoContentTemp = infoContent.replace("{city}", city)

    sql = """UPDATE ps_info_lang
             SET text = '%(infoContent)s'
             WHERE id_info = %(id_info)s"""%{"id_info":id_info, "infoContent":infoContentTemp}

    prestashop_api.MakeUpdateQueue(sql)


def ChangeDomains(replacedDomain, newDomain):
    sql = """SELECT * FROM ps_shop_url"""
    data = prestashop_api.MakeGetInfoQueue(sql)
    for row in data:
        domain = row[2].replace(replacedDomain, newDomain)
        sql = """UPDATE ps_shop_url
                 SET domain = '{0}', domain_ssl = '{0}'
                 WHERE id_shop_url = {1}""".format(domain, row[0])
        prestashop_api.MakeUpdateQueue(sql)

def DeleteAllOrders():
    orders = prestashop_api.GetAllOrdersIds()
    for orderId in orders:
        prestashop_api.DeleteOrder(orderId)


def CategoriesToHttps():
    categories = prestashop_api.GetShopCategories(1)
    for cat in categories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = cat
        if categoryID in [1,2]:
            continue
        description = description.replace("http://","https://")
        long_description = long_description.replace("http://","https://")
        prestashop_api.SetCategoryDescription(categoryID, description)
        prestashop_api.SetCategoryLongDescription(categoryID, long_description)

def ProductsToHttps():
    products = [x[0] for x in prestashop_api.GetProductsIDs()]
    for productId in products:
        descr = prestashop_api.GetProductDescription(productId)
        if descr.find("http://") == -1:
            continue
        print productId
        descr = descr.replace("http://","https://")
        prestashop_api.SetProductDescription(productId, descr)

def FillImagesAltTag(shopId, shopName):
    categories = prestashop_api.GetShopCategories(shopId)
    for category in categories:
        id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = category
        if id_category in [1,2]:
            continue
        if description.find("alt") == -1 and long_description.find("alt") == -1:
            continue
        description = description.replace(u'alt=""', u'alt="{0}"'.format(u"{0} в {1}".format(name.replace('"',''), commonLibrary.GetCity(shopName))))
        long_description = long_description.replace(u'alt=""', u'alt="{0}"'.format(u"{0} в {1}".format(name.replace('"',''), commonLibrary.GetCity(shopName))))
        params = [link_rewrite, meta_title, description, long_description, meta_keywords, meta_description ]
        prestashop_api.UpdateCategoryData(shopId, id_category, params)

def FillVideoTab():
    shopProducts = prestashop_api.GetShopProducts(10)

    for shopProduct in shopProducts:
        id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description = shopProduct

##        if id_product <> 370:
##            continue

        if prestashop_api.IsProductActive(id_product) == 0 or description == "":
            continue

        video = u""
        characteristics = u""
        equipment = u""

        productDescription = etree.HTML(description)
        if len(productDescription) != 0:
            videos = productDescription.xpath(".//div//p[contains(@class, 'videos')]")
            for v in videos:
                video += etree.tostring(v) + "<p><br/></p>"
                v.getparent().remove(v)

            videos = productDescription.xpath(".//iframe")
            for v in videos:
                video += etree.tostring(v) + "<p><br/></p>"
                v.getparent().remove(v)

##            characteristics = productDescriptionDiv.xpath(".//p[contains(., '" + u"арактеристики"  + "')]")
##
##            if len(element) != 0:
##                element[0].getparent().remove(element[0])

        if video == u"":
            continue

        print id_product

        description = commonLibrary.GetStringDescription(productDescription)
        video = video.replace("""allowfullscreen"/>""","""allowfullscreen"></iframe>""")
        video = video.replace("""allowfullscreen=\"\""/>""","""allowfullscreen"></iframe>""")
        video = video.replace("""\"/>""","""\"></iframe>""")
        prestashop_api.SetProductDescription(id_product, description)
        prestashop_api.SetProductVideo(id_product, video)

def CleanH3Tags():
    shopProducts = prestashop_api.GetShopProducts(10)

    for shopProduct in shopProducts:
        id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description = shopProduct
        print id_product

        if prestashop_api.IsProductActive(id_product) == 0 or description == "":
            continue

        productDescription = etree.HTML(description)
        if len(productDescription) != 0:
            h3tags = productDescription.xpath(".//h3")
            for h3tag in h3tags:
                text = h3tag.text
                if text == None:
                    try:
                        text = h3tag.xpath(".//span")[0].text
                        if text == None:
                            continue
                    except:
                        continue
                if text.find(u"Технические") != -1:
                    text = u"Технические характеристики"
                if text.find(u"Особенности") != -1:
                    text = u"Особенности и преимущества"
                if text.find(u"Комплект") != -1:
                    text = u"Комплектация"
                if text.find(u"Спецификация") != -1:
                    text = u"Спецификация"
                if text.find(u"Принцип работы") != -1:
                    text = u"Принцип работы"
                if text.find(u"Принцип действия") != -1:
                    text = u"Принцип действия"
                if text.find(u"Область") != -1:
                    text = u"Область применения"
                if text.find(u"Использование") != -1:
                    text = u"Использование"
                if text.find(u"Правила использования") != -1:
                    text = u"Правила использования"
                if text.find(u"Преимущества") != -1:
                    text = u"Преимущества"
                if text.find(u"Функции") != -1:
                    text = u"Функции"

                if h3tag.text == None:
                    h3tag.xpath(".//span")[0].text = text
                else:
                    h3tag.text = text

            description = etree.tostring(productDescription)
            description = commonLibrary.UnescapeText(description)

            description = description.replace("<html>","")
            description = description.replace("</html>","")
            description = description.replace("<body>","")
            description = description.replace("</body>","")
            prestashop_api.SetProductDescription(id_product, description)

def DeleteImagesFromCategoriesDescriptions(shopId):
    shopCategories = prestashop_api.GetShopCategories(shopId)

    for shopCategory in shopCategories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory
        if categoryID in [1,2]:
            continue

        if prestashop_api.IsCategoryActive(categoryID) == 0:
            continue

##        print description

        bNeedUpdate1 = False
        bNeedUpdate2 = False

        ldElement = etree.HTML(description)
        elements = ldElement.xpath("//img")
        for element in elements:
            parent = element.getparent()
            text = commonLibrary.UnescapeText(etree.tostring(parent))
##            print text
            ind1 = text.find("height")
            ind2 = text.find("</p>")
            if ind1 == -1 or ind2 == -1 or ind2 - ind1 < 20:
                bNeedUpdate1 = True
                if parent.tag == "strong":
                    parent.getparent().remove(parent)
                else:
                    parent.remove(element)
            else:
                bNeedUpdate2 = True
                ind1 = description.find("<img ")
                ind2 = description.find("\" />")
                img = description[ind1:ind2 + 4]
                description = description.replace(img, "")

        if bNeedUpdate1 == True or bNeedUpdate2 == True:
            if bNeedUpdate1 == True:
                description = commonLibrary.GetStringDescription(ldElement)
##            print description
            prestashop_api.SetShopCategoryDescription(shopId, categoryID, description)

def TransliterateProductsLinkRewrite():
    products = prestashop_api.GetProductsIDs()
    for productId in products:
        productId = productId[0]
        linkRewrite = prestashop_api.GetProductLinkRewrite(productId)
        linkRewrite = prestashop_api.Transliterate(linkRewrite)
        prestashop_api.SetProductLinkRewrite(productId, linkRewrite)

def TransliterateCategoriesLinkRewrite():
    shopCategories = prestashop_api.GetShopCategories(10)
    for shopCategory in shopCategories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory
        if categoryID in [1,2]:
            continue

        if prestashop_api.IsCategoryActive(categoryID) == 0:
            continue

        linkRewrite = prestashop_api.Transliterate(link_rewrite)
        prestashop_api.SetCategoryLinkRewrite(categoryID, linkRewrite)

def GetDuplicateProductTitles():
    products = prestashop_api.GetShopProducts(10)
    city =  commonLibrary.GetCity(prestashop_api.GetShopName(10))

    titles = []
    for product in products:
        id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description = product
        if prestashop_api.IsProductActive(id_product) == 0:
            continue

        with open("{0}.txt".format(prestashop_api.siteUrl.replace("http://", "")), "a") as file:
            labels = ""
            if name + u" в " + city == meta_title:
                labels += "[DUPLICATES_H1] "
##            meta_title = meta_title[0:meta_title.find(u" в ")]
            if meta_title in titles:
                labels += "[DUPLICATE] "
            if len(meta_title) < 40 or len(meta_title) > 50:
                labels += "[{0}] ".format(len(meta_title))

            if labels == "":
                continue
            file.write("{0}{1} , {2}\n".format(labels, id_product, meta_title.encode('cp1251')))

        titles.append(meta_title)

def GetDuplicateCategoryTitles():
    shopCategories = prestashop_api.GetShopCategories(10)
    city =  commonLibrary.GetCity(prestashop_api.GetShopName(10))

    titles = []
    for shopCategory in shopCategories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory

        if categoryID in [1,2]:
            continue

        if prestashop_api.IsCategoryActive(categoryID) == 0:
            continue

        with open("{0}_categories.txt".format(prestashop_api.siteUrl.replace("http://", "")), "a") as file:
            labels = ""
            if meta_title == name + u" в " + city:
                labels += "[DUPLICATES_H1] "
##            meta_title = meta_title[0:meta_title.find(u" в ")]
            if meta_title in titles:
                labels += "[DUPLICATE] "
            if len(meta_title) < 40 or len(meta_title) > 50:
                labels += "[{0}] ".format(len(meta_title))

            file.write("{0}{1} , {2}\n".format(labels, categoryID, meta_title.encode('cp1251')))

        titles.append(meta_title)

def DeleteCityFromProductsUrlsAndTransliterate():
    products = prestashop_api.GetProductsIDs()
    for productId in products:
        productId = productId[0]
        linkRewrite = prestashop_api.GetProductLinkRewrite(productId)
        linkRewrite = linkRewrite.replace(u"-купить-Москва", "")
        linkRewrite = prestashop_api.Transliterate(linkRewrite)
        print linkRewrite
        prestashop_api.SetProductLinkRewrite(productId, linkRewrite)

def DeleteCityFromCategoriesUrlsAndTransliterate():
    shopCategories = prestashop_api.GetShopCategories(11)
    shopName = prestashop_api.GetShopName(11)
    for shopCategory in shopCategories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory
        if categoryID in [1,2]:
            continue

        if prestashop_api.IsCategoryActive(categoryID) == 0:
            continue

        link_rewrite = link_rewrite.replace(u"-купить-{0}".format(shopName), "")
        link_rewrite = prestashop_api.Transliterate(link_rewrite)
        print link_rewrite
        prestashop_api.SetCategoryLinkRewrite(categoryID, link_rewrite)

def SetDefaultPagesTags():
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        shopName = commonLibrary.GetShopName(shopName)

        cinds = [x[1].lower() for x in prestashop_api.GetShopActiveCategories(shopId)]
        for cind in cinds:
            if cind.lower() in [u"другие товары", u"кухонная посуда"]:
                cinds.remove(cind)
            if len([x for x in cinds if x.find(u"глушилки") != -1]) > 2 and cind.find(u"глушилки") != -1:
                cinds.remove(cind)
        random.shuffle(cinds)
        cinds = cinds[0:3]
        sCinds = ", ".join(cinds)

        title, description, keywords, url_rewrite = prestashop_api.GetShopDefaultPageTags(shopId, "index")
        title = commonLibrary.GetVariantFromTemplate(shopName, u"{{Интернет|Онлайн}-магазин|Магазин} «cname» - подавители сигналов, глушилки сотовой связи и навигации в [city_P] ".replace("cname", commonLibrary.GetCompanyName(prestashop_api.siteUrl)))
        description = commonLibrary.GetVariantFromTemplate(shopName, u"Наш {{интернет|онлайн}-магазин|магазин} предлагает по {доступным|низким|выгодным} ценам в [city_P] {0} и другие товары для обеспечения защиты информации.".replace("{0}", sCinds))
        keywords = commonLibrary.GetVariantFromTemplate(shopName, u"Продажа средств защиты безопасности в [city_P]")
##        print title, description, keywords

        prestashop_api.SetShopDefaultPageTag(shopId, "index","title", title)
        prestashop_api.SetShopDefaultPageTag(shopId, "index","description", description)
        prestashop_api.SetShopDefaultPageTag(shopId, "index","keywords", keywords)



def UpdateCategoriesDescriptions():
    shopCategories = prestashop_api.GetShopCategories(1)
    for shopCategory in shopCategories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory

        if categoryID in [1,2]:
            continue

        if prestashop_api.IsCategoryActive(categoryID) == 0:
            continue

        if long_description.find(u"</iframe>") != -1:
            print categoryID
            long_description = long_description.replace("</iframe>", "")
            prestashop_api.SetCategoryLongDescription(categoryID, long_description)



def GetProductsUrls():
    products = [x[0] for x in prestashop_api.GetProductsIDs()]

    for productId in products:
        if prestashop_api.IsProductActive(productId) == 0:
            continue

        catId = prestashop_api.GetProductCategoryDefault(productId)
        catLR = prestashop_api.GetShopCategoryDetails(1, catId)[0][1]
        link_rewrite = prestashop_api.GetProductLinkRewrite(productId)

        try:
            with open("safetus.txt", "a") as myfile:
                myfile.write( "https://safetus.ru/{0}/{1}-{2}.html\n".format(catLR, productId, link_rewrite).encode("cp1251"))
        except:
            pass

def GetCategoriesUrlsSubdomains():
    shops = prestashop_api.GetShopsUrls()

    for shop in shops:
        if shop[1] not in [9]:
            continue
        cats = prestashop_api.GetShopActiveCategories(shop[1])
        for cat in cats:
            id_category, name, link_rewrite = cat
            if id_category not in []:
                continue
            print "{4}{0}/{3}{1}-{2}".format(shop[2], id_category, link_rewrite, shop[5], commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl))

def AddNofollowToArticles():
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        print shopId

        shopCategories = prestashop_api.GetShopsActiveCategories(shopId)

        for shopCategory in shopCategories:
            categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory

            if categoryID in [1,2]:
                continue

            articles = prestashop_api.GetCategoryArticles(shopId, categoryID)
            if articles in ["", None]:
                continue

            if articles.find("nofollow") != -1:
                continue
            articles = articles.replace("href=","rel=\"nofollow\" href=")
            print categoryID

            prestashop_api.SetCategoryArticles(shopId, categoryID, articles)

def GetProductsWithOneImage():
##    cats = [144, 145, 146]
    cats = [56,57,59,16, 352,23]
    for catId in cats:
        catName = prestashop_api.GetCategoryName(catId)
        ids = prestashop_api.GetCategoryActiveProductsIds(catId)
        for id in ids :
            images = prestashop_api.GetProductImagesIds(id)
            name = prestashop_api.GetProductOriginalName(id)
            if len(images) == 0:
                with open (u"{} (нет картинок).txt".format(catName), "a") as file:
                    file.write("{} ( {} )\n".format(name.encode('utf-8'), id))
##            if len(images) == 1:
##                with open (u"{} (1 картинка).txt".format(catName), "a") as file:
##                    file.write("{} ( {} )\n".format(name.encode('utf-8'), id))


def UpdateThemeConfigurator():
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        print shopId

        content = u"""<div class="title">Почему мы?</div>
<div class="adv first">Большой ассортимент товара</div>
<div class="adv">Оперативная доставка курьером, почтой России или на пункт выдачи</div>
<div class="adv">Множество вариантов оплаты</div>
<div class="adv">Опытные консультанты</div>
<div class="adv last">Оповещение о статусе заказа по email и через СМС</div>"""
        data = [1, "", 0, "top", "", 0, "", 0, 0, content, 1]
        prestashop_api.AddItemToThemeConfigurator(shopId, data)

        content = u"""<div class="ctgry snd"><h2>Отпугиватели насекомых</h2>
<a href="/92-Otpugivateli-komarov" class="subcat"><div>Отпугиватели комаров</div><img src="/img/cms/Icons/mosqiuto.png" alt="Комар"></a><a href="/136-otpugivateli-tarakanov" class="subcat"><div>Отпугиватели тараканов</div><img src="/img/cms/Icons/roach.png" alt="Таракан"></a>
<a href="/137-otpugivateli-kleschey" class="subcat"><div>Отпугиватели клещей</div><img src="/img/cms/Icons/mite.png" alt="Клещ"></a>
</div>"""
        data = [6, "", 0, "top", "", 0, "", 0, 0, content, 1]
        prestashop_api.AddItemToThemeConfigurator(shopId, data)

        content = u"""<div class="ctgry"><h2>Отпугиватели животных</h2>
<a href="/144-ultrazvukovye-otpugivateli-krys" class="subcat"><div>Отпугиватели крыс</div><img src="/img/cms/Icons/rat.png" alt="Крыса"></a><a href="/143-ultrazvukovye-otpugivateli-myshey" class="subcat"><div>Отпугиватели мышей</div><img src="/img/cms/Icons/mouse.png" alt="Мышь"></a>
<a href="/96-Otpugivateli-sobak" class="subcat"><div>Отпугиватели собак</div><img src="/img/cms/Icons/dog.png" alt="Собака"></a>
<a href="/91-Otpugivateli-krotov" class="subcat"><div>Отпугиватели кротов</div><img src="/img/cms/Icons/krot.png" alt="Крот"></a><a href="/90-Otpugivateli-zmey" class="subcat"><div>Отпугиватели змей</div><img src="/img/cms/Icons/snake.png" alt="Змея"></a>
<a href="/138-otpugivateli-golubey" class="subcat"><div>Отпугиватели голубей</div><img src="/img/cms/Icons/dove.png" alt="Голубь"></a><a href="/105-Otpugivateli-ptits" class="subcat"><div>Отпугиватели ворон</div><img src="/img/cms/Icons/raven.png" alt="Ворона"></a>
<a href="/105-Otpugivateli-ptits" class="subcat"><div>Отпугиватели сорок</div><img src="/img/cms/Icons/magpie.png" alt="Сорока"></a></div>"""
        data = [5, "", 0, "top", "", 0, "", 0, 0, content, 1]
        prestashop_api.AddItemToThemeConfigurator(shopId, data)

        content = u"""<div class="title">Статьи по темам</div><a href="/smartblog/category/7_Отпугиватели-птиц.html" class="art"><div>Отпугивание птиц</div></a><a href="/smartblog/category/6_Отпугиватели-ползающих-насекомых.html" class="art"><div>Отпугивание ползающих насекомых</div></a>
<a href="/smartblog/category/5_Отпугиватели-кротов-змей-землероек-медведок.html" class="art"><div>Отпугивание кротов, землероек, медведок</div></a>
<a href="/smartblog/category/4_Отпугиватели-и-уничтожители-летающих-насекомых.html" class="art"><div>Отпугивание летающих насекомых</div></a>
<a href="/smartblog/category/3_Ультразвуковые-отпугиватели-собак.html" class="art"><div>Отпугивание собак</div></a>
<a href="/smartblog/category/2_Ультразвуковые-отпугиватели-грызунов.html" class="art"><div>Отпугивание грызунов</div></a>
<a href="/smartblog/category/9_Общая-информация.html" class="art"><div>Общая информация</div></a>"""
        data = [3, "", 0, "top", "", 0, "", 0, 0, content, 1]
        prestashop_api.AddItemToThemeConfigurator(shopId, data)

def UpdateCitiesTable():
    pvzNodes = root.xpath("//Pvz[ not(contains (@Name, '" + u"Фиктивный" + "')) and @CountryCode = 1 and not(contains (@Name, ','))] ")
    sdekCities = []
    for node in pvzNodes:
        city = node.attrib["City"].split(",")[0].split(" (")[0]
        sdekCities.append(city)

    myset = set(sdekCities)
    uniqueSdekCities = list(myset)

    existingCities = [x[0] for x in commonLibrary.GetCitiesTable()]
    for sdekCity in uniqueSdekCities:
        if sdekCity in existingCities:
            continue

        print sdekCity
        try:
            resp = requests.get(u"https://ws3.morpher.ru/russian/declension?s=" + sdekCity, verify=False)
            xml = etree.XML(resp._content)
            city_I = sdekCity

            city_R = xml.xpath(u"//Р")[0].text
            city_D = xml.xpath(u"//Д")[0].text
            city_V = xml.xpath(u"//В")[0].text
            city_T = xml.xpath(u"//Т")[0].text
            city_P = xml.xpath(u"//П")[0].text

            resp = requests.get(u"https://ws3.morpher.ru/russian/adjectivize?s=" + sdekCity, verify=False)
            city_pr_s_I = etree.XML(resp._content).xpath(u"//string")[0].text
            resp = requests.get(u"https://ws3.morpher.ru/russian/declension?s=" + city_pr_s_I, verify=False)

            xml = etree.XML(resp._content)
            city_pr_s_R = xml.xpath(u"//Р")[0].text
            city_pr_s_D = xml.xpath(u"//Д")[0].text
            city_pr_s_V = xml.xpath(u"//В")[0].text
            city_pr_s_T = xml.xpath(u"//Т")[0].text
            city_pr_s_P = xml.xpath(u"//П")[0].text

            city_pr_m_I = xml.xpath(u"//множественное//И")[0].text
            city_pr_m_R = xml.xpath(u"//множественное//Р")[0].text
            city_pr_m_D = xml.xpath(u"//множественное//Д")[0].text
            city_pr_m_V = xml.xpath(u"//множественное//В")[0].text
            city_pr_m_T = xml.xpath(u"//множественное//Т")[0].text
            city_pr_m_P = xml.xpath(u"//множественное//П")[0].text
        except:
            continue

        sql = u"""INSERT INTO cities(city_I, city_R, city_D, city_V, city_T, city_P, city_pr_s_I, city_pr_s_R, city_pr_s_D, city_pr_s_V, city_pr_s_T, city_pr_s_P, city_pr_m_I, city_pr_m_R, city_pr_m_D, city_pr_m_V, city_pr_m_T, city_pr_m_P)
                 VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}')""" \
                 .format(city_I, city_R, city_D, city_V, city_T, city_P, city_pr_s_I, city_pr_s_R, city_pr_s_D, city_pr_s_V, city_pr_s_T, city_pr_s_P, city_pr_m_I, city_pr_m_R, city_pr_m_D, city_pr_m_V, city_pr_m_T, city_pr_m_P)

        commonLibrary.MakeLocalDbUpdateQueue(sql)
        time.sleep(1)

def UpdateProductsMetatitles():
    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        print shopId

        prods = prestashop_api.GetShopProducts(shopId)
        for prod in prods:
            id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description = prod

            try:
                city = commonLibrary.GetCityDeclension(commonLibrary.GetShopName(shopName))[5]
            except:
                continue

            meta_title = u"{0} купить в {1}".format(commonLibrary.ChangeCase( name), city)
            if len(meta_title) < 63:
                meta_title += u" недорого"

            print meta_title
            prestashop_api.SetShopProductMetaTitle(shopId, id_product,meta_title)

def GetCategoriesUrlsSubfolders():
    shops = prestashop_api.GetShopsUrls()
    cats = prestashop_api.GetShopActiveCategories(1)
    domain = shops[0][2]
    for shop in shops:
        shopId = shop[1]
        subfolder = shop[5].replace("/","")
        shopName = commonLibrary.GetShopName(prestashop_api.GetShopName(shopId))
        if shopName not in (commonLibrary.mainCities1 + commonLibrary.mainCities2):
            continue
        for catId in [x[0] for x in cats]:
            if catId not in [169, 170, 171, 172, 173, 174]:
                continue
            catLinkRewrite = prestashop_api.GetCategoryUrl(catId,shopId)
            if subfolder == "":
                url = "{0}{1}/{2}".format(commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl) , domain, subfolder, catLinkRewrite)
            else:
                url = "{0}{1}/{2}/{3}".format(commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl) , domain, subfolder, catLinkRewrite)
            print url

def GetProductsUrlsSubfolders():
    shops = prestashop_api.GetShopsUrls()
    domain = shops[0][2]
    for shop in shops:
        shopId = shop[1]
        subfolder = shop[5].replace("/","")
        shopName = commonLibrary.GetShopName(prestashop_api.GetShopName(shopId))
        if shopName not in (commonLibrary.mainCities1 + commonLibrary.mainCities2):
            continue
        products = prestashop_api.GetActiveProductsIDs()
        for productId in products:
            if productId not in [442]:
                continue
            if shopId in [1]:
                continue
            linkRewrite = prestashop_api.GetProductUrl(productId,shopId)
            if subfolder == "":
                url = "{0}{1}/{2}".format(commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl) , domain, subfolder, catLinkRewrite)
            else:
                url = "{0}{1}/{2}/{3}".format(commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl) , domain, subfolder, linkRewrite)
            print url

def CustomizeProduct():
    name1 = u"Отпугиватель птиц"
##    prestashop_api.InitAllProducts()
    article = u"Купол-Био"
    name = u"{0} «{1}»".format(name1, article)
    i = 410
##    if i == -1:
##        return
    descr = u""""""

    print i
    if i == False:
        return
##    if name1 == u"":
##        return

    catid = [143,144]

##    print i, name
##    prestashop_api.SetProductLinkRewrite(i, commonLibrary.GetLinkRewriteFromName(article))
##    prestashop_api.SetProductName(i, name)
    prestashop_api.SetProductOriginalName(i,u"Антилай HKS99")
    prestashop_api.SetProductArticle(i, "nt{0}".format(i))
##    prestashop_api.SetProductCategories(i, catid)
##    prestashop_api.SetProductShortDescription(i, name)
##    prestashop_api.SetProductMetaTitle(i, name)
##    prestashop_api.SetProductPriceForAllShops(i, 2850,5760)
##    prestashop_api.SetProductDescription(i, descr)
##    prestashop_api.MakeProductEnabledForAllShops(i )
##    prestashop_api.SetProductQuantityForAllShops(i , 30)
    prestashop_api.SetProductActive(i, 1)
##    if imageId != -1:
##        prestashop_api.SetProductMainImage(i, imageId)
##    prestashop_api.SetProductOnSale(i, 0)


def UpdateCitiesLocalTable():
    sql = """SELECT * FROM cities"""

    siteCities = [x[0] for x in prestashop_api.MakeGetInfoQueue(sql)]

    for city in cities:
        city_I = city[0]
        city_P = city[5]

        if city_I not in siteCities:
##            print city_I
            sql = u"INSERT INTO cities VALUES ('{0}','{1}')".format(city_I, city_P)
            prestashop_api.MakeUpdateQueue(sql)

def AddProductsToRootCat():
    subcats = [92, 132, 133, 134]
    catId = 152
    for subcat in subcats:
        products = prestashop_api.GetCategoryActiveProductsIds(subcat)
        for id_product in products:
            prCats = prestashop_api.GetProductCategories(id_product)
            if catId not in prCats:
                prestashop_api.AddProductToCategory(id_product, catId)

def SetProductPriceForAllShops():
    newWholesalePrice = 950
    newPrice = 2630
##    newPrice = int(10 * round(float(float(newWholesalePrice) / 0.7)/10))
##    if newPrice % 1000 == 0:
##        newPrice -= 10
    prestashop_api.InitAllProducts()
    name = u"MD81"
    i = prestashop_api.GetProductIdQuick(name)
    if i != -1:
        print name
        prestashop_api.SetProductPriceForAllShops(i, newWholesalePrice, newPrice)

def MakeProductUnavailableForAllShops():
    prestashop_api.InitAllProducts()
    names = [u"SITITEK Москито-MV-11"]
    for name in names:
        i = prestashop_api.GetProductIdQuick(name)
        if i != -1:
            print name
            prestashop_api.MakeProductUnavailableForAllShops(i)
##            prestashop_api.SetProductActive(i, 0)
##            prestashop_api.SetProductOriginalName(i, u"SITITEK Москито MV-11")

def DeleteProducts():
    prestashop_api.InitAllProducts()
    names = [u"Скорпион 5XL Интернет", u"Терминатор-40 Кейс", u"Терминатор-15V (Видеокамеры)"]
    for name in names:
        i = prestashop_api.GetProductIdQuick(name)
        if i != -1:
            print name
            prestashop_api.DeleteProduct(i)
##            prestashop_api.SetProductActive(i, 0)

def ConfigureMegaMenu():
    tabs = prestashop_api.GetMegaMenuTabsIds()
    tabs = [6]
    shops = prestashop_api.GetAllShops()

##    session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
##    session.set_pasv(False)
##    session.cwd("/home/" + site["siteDir"] + "/www/modules/iqitmegamenu/views/css")

##    sql = """DELETE FROM ps_iqitmegamenu_tabs_shop"""
##    prestashop_api.MakeUpdateQueue(sql)

    for shop in shops:
        shopId, shopDomen, shopName = shop
        print shopId
        for tabId in tabs:
            sql = """INSERT INTO ps_iqitmegamenu_tabs_shop(id_tab, id_shop)
                     VALUES ({0},{1})""".format(tabId, shopId)

            prestashop_api.MakeUpdateQueue(sql)

##        cssFileName = "iqitmegamenu_s_{0}.css".format(shopId)
##        shutil.copyfile("iqitmegamenu.css", cssFileName)
##        try:
##            session.delete(cssFileName)
##        except:
##            pass
##        file = open(cssFileName,'rb')                      # file to send
##        session.storbinary('STOR {0}'.format(cssFileName), file)     # send the file
##        file.close()
##        os.remove(cssFileName)

def MakeMegaMenuConf():
    id_tab = 5
    width_ = 12
    string = "{"
    colsNum = 1
##    cats = [x[0] for x in prestashop_api.GetShopActiveCategories(1) if prestashop_api.GetCategoryParent(x[0]) in [1,2]]
##    cats.remove(227)
    cats = [140, 139, 145]
    mmcats = [140, 139, 145]
    if len(cats) % colsNum == 0:
        linesNum = int(len(cats) / colsNum)
    else:
        linesNum = int(len(cats) / colsNum) + 1
    catPerLine = int(len(cats) / colsNum)
    index = 0
    for lineNum in range(1, linesNum + 1):
        parent_id = lineNum + (lineNum - 1) * colsNum
        index += 1
        position = (index + 1) / 2
        string += """"index":{"elementId":index,"type":1,"depth":0,"position":pos_,"parentId":0},""".replace("index", str(index)).replace("pos_", str(position))

        for i in range(0, colsNum):
            if len(cats) == 0:
                break
            catId = cats.pop(0)
            index += 1
            string += """"index":{"elementId":index,"type":2,"depth":1,"width":width_,"contentType":2,"position":pos_,"parentId":parent_id,"content":{"ids":["catId"],"treep":0,"thumb":1,"depth":4,"line":12,"sublimit":15},"content_s":{"title":{"1":""},"href":{"1":""},"legend":{"1":""}}},""".replace("index", str(index)).replace("catId", str(catId)).replace("parent_id", str(parent_id)).replace("pos_", str(position)).replace("width_", str(width_))

    string = string.strip(",")
    string += "}"
    sql = """UPDATE ps_iqitmegamenu_tabs
             SET submenu_content = '{0}'
             WHERE id_tab = {1}""".format(string, id_tab)
    prestashop_api.MakeUpdateQueue(sql)

    #mobile menu
    mmstring = ""
    for cat in mmcats:
        mmstring += "CAT{},".format(cat)
    mmstring = mmstring.strip(",")
    sql = """UPDATE ps_configuration
             SET value = '{}'
             WHERE name = 'iqitmegamenu_mobile_menu'""".format(mmstring)
    prestashop_api.MakeUpdateQueue(sql)

def MakeDestUrl(destDomain, destCat):
    destSiteScheme = prestashop_api.GetSiteUrlScheme()
    isDestSiteSubfolders = "http://" + destDomain in commonLibrary.GetSubfoldersSites()
    id_shop, domain, name, virtual_uri = shop
    if isSubfolders:
        city = virtual_uri.replace("/","")
    else:
        city = domain.replace("." + site["domain"], "")

    if id_shop == 1:
        destUrl = "{0}{1}/{2}".format(destSiteScheme, destDomain, destCat )
    else:
        if isDestSiteSubfolders:
            destUrl = "{0}{1}/{2}/{3}".format(destSiteScheme, destDomain,city, destCat )
        else:
            destUrl = "{0}{1}.{2}/{3}".format(destSiteScheme,city, destDomain,destCat )

    if prestashop_api.IsUrlExists(destUrl) == False:
        destUrl = "{0}{1}".format(destSiteScheme, destDomain )

    return destUrl

def AddCustomLinkToMegaMenu():
    destUrl = MakeDestUrl("glushilki.ru.com", "145-glushilki-podaviteli-gsm-gps-glonass")
    label = u"Мини камеры видеонаблюдения"

    #добавляем ссылку
    linkParam = {}
    linkParam["id_shop"] = id_shop
    linkParam["new_window"] = 1
    linkParam["label"] = label
    linkParam["link"] = destUrl
    if prestashop_api.IsMegaMenuCustomLinkExists(linkParam):
        return
    linkId = prestashop_api.AddMegaMenuCustomLink(linkParam)

    #мобильное меню
    mobileMenuValue = prestashop_api.GetMegaMenuMobileMenuValue(id_shop)
    if mobileMenuValue == -1:
        mobileMenuValue = "CAT144,CAT145,CAT146,CAT147"
        mobileMenuValue += ",LNK{0}".format(linkId)
        prestashop_api.AddMegaMenuMobileMenuValue(id_shop, mobileMenuValue)
    else:
        entry = ",LNK{0}".format(linkId)
        if mobileMenuValue.find(entry) == -1:
            mobileMenuValue += entry
            prestashop_api.SetMegaMenuMobileMenuValue(id_shop, mobileMenuValue)

    #обновляем/добавляем таб
    tab = prestashop_api.GetMegaMenuTab(id_shop, 'HOME0')
    if tab == -1:
        tabContent = """{"1":{"elementId":1,"type":1,"depth":0,"position":1,"parentId":0},"2":{"elementId":2,"type":2,"depth":1,"width":12,"contentType":2,"position":3,"parentId":1,"content":{"ids":["144"],"treep":0,"thumb":1,"depth":4,"line":12,"sublimit":15},"content_s":{"title":{"1":""},"href":{"1":""},"legend":{"1":""}}},"3":{"elementId":3,"type":1,"depth":0,"position":2,"parentId":0},"4":{"elementId":4,"type":2,"depth":1,"width":12,"contentType":2,"position":3,"parentId":3,"content":{"ids":["145"],"treep":0,"thumb":1,"depth":4,"line":12,"sublimit":15},"content_s":{"title":{"1":""},"href":{"1":""},"legend":{"1":""}}},"5":{"elementId":5,"type":1,"depth":0,"position":3,"parentId":0},"6":{"elementId":6,"type":2,"depth":1,"width":12,"contentType":2,"position":3,"parentId":5,"content":{"ids":["146"],"treep":0,"thumb":1,"depth":4,"line":12,"sublimit":15},"content_s":{"title":{"1":""},"href":{"1":""},"legend":{"1":""}}},"7":{"elementId":7,"type":1,"depth":0,"position":4,"parentId":0},"8":{"elementId":8,"type":2,"depth":1,"width":12,"contentType":2,"position":3,"parentId":7,"content":{"ids":["147"],"treep":0,"thumb":1,"depth":4,"line":12,"sublimit":15},"content_s":{"title":{"1":""},"href":{"1":""},"legend":{"1":""}}}}"""
    else:
        tabContent = tab[1]

    tabContent = tabContent[1:len(tabContent) - 1]

    elementId = tabContent.count("elementId") + 1
    position = (elementId + 1) / 2
    tabContent += ""","index":{"elementId":index,"type":1,"depth":0,"position":pos_,"parentId":0},""".replace("index", str(elementId)).replace("pos_", str(position))
    tabContent += """"index":{"elementId":index,"type":2,"depth":1,"width":12,"contentType":3,"position":3,"parentId":parentId_,"content_s":{"title":{"1":""},"href":{"1":""},"legend":{"1":""}},"content":{"ids":["LNKlinkId"],"view":2}}""".replace("index", str(elementId + 1)).replace("parentId_", str(elementId)).replace("pos_", str(position)).replace("linkId", str(linkId))

    tabContent = "{cont}".replace("cont", tabContent)

    if tab == -1:
        tabParam = {}
        tabParam["menu_type"] = 1
        tabParam["position"] = 1
        tabParam["submenu_content"] = tabContent
        tabParam["submenu_type"] = 2
        tabParam["submenu_width"] = 5
        tabParam["title"] = u"Каталог товаров"
        tabId = prestashop_api.AddMegaMenuTab(tabParam)
        prestashop_api.AddMegaMenuTabToShop(tabId,id_shop )
        logging.Log(u"Добавлена ссылка для магазина {0} ({1})".format(id_shop, city))
    else:
        tabId = tab[0]
        prestashop_api.SetMegaMenuTabSubmenuContent(tabId, tabContent )
        logging.Log(u"Обновлена ссылка для магазина {0} ({1})".format(id_shop, city))




def DeleteArticle():
    theme = u"""Отпугиватели птиц"""
    shops = prestashop_api.GetAllShops()
    for shop in shops:
        shopId, shopDomen, shopName = shop
        artId = prestashop_api.GetArticleId(theme, shopId)
        if artId != -1:
            prestashop_api.DeleteArticle(artId)
####        UpdateRussianPostDeliveryCost()
####        shopName = commonLibrary.GetShopName(prestashop_api.GetShopName(shopId))
##        shopId, shopDomen, shopName = shop

def CleanCategoriesTexts():
    cats = prestashop_api.GetActiveCategoriesIds()
    for catId in cats:
        shops = prestashop_api.GetAllShops()
        for shop in shops:
            shopId, shopDomen, shopName = shop
            descr = prestashop_api.GetShopCategoryDescription(shopId, catId)
            if descr != "" and len(descr) < 300:
                prestashop_api.SetShopCategoryDescription(shopId, catId, u"")
            longDescr = prestashop_api.GetShopCategoryLongDescription(shopId, catId)
            if longDescr != "" and len(longDescr) < 300:
                prestashop_api.SetShopCategoryLongDescription(shopId, catId, u"")

def GetCategoriesWithoutDescr():
    cats = prestashop_api.GetActiveCategoriesIds()
    for catId in cats:
        catName = prestashop_api.GetCategoryName(catId)
        catUrl = prestashop_api.GetCategoryFullUrl( catId, 1)
        logging.Log(u"======= {} ( {} ) =========".format(catName, catUrl))
        shops = prestashop_api.GetAllShops()
        for shop in shops:
            shopId, shopDomen, shopName = shop
            shopName = commonLibrary.GetShopName(shopName)
            if shopName in (commonLibrary.mainCities1 + commonLibrary.mainCities2):
                descr = prestashop_api.GetShopCategoryDescription(shopId, catId)
                longDescr = prestashop_api.GetShopCategoryLongDescription(shopId, catId)
##                if descr in ["", None] and longDescr in ["", None]:
##                if descr in ["", None] and longDescr not in ["", None]:
                if descr not in ["", None] and longDescr in ["", None]:
                    logging.Log(u"{}, {}".format(shopId, shopName))
##                if longDescr in ["", None]:
##                    logging.Log("shopId {}, catId {}".format(shopId, catId))

def AddSmartCategoryToAllShops():
    catId = 1
    shops = prestashop_api.GetAllShops()
    for shop in shops:
        shopId, shopDomen, shopName = shop
        sql = """INSERT INTO ps_smart_blog_category_shop(id_smart_blog_category, id_shop)
                 VALUES ({},{})""".format(catId, shopId)
        prestashop_api.MakeUpdateQueue(sql)

def DeleteSpecificPrice():
##    products = prestashop_api.GetActiveProductsIDs()
    accessoires = []
    cats = [i[0] for i in prestashop_api.GetShopCategories(1)]
    for catid in cats:
        name = prestashop_api.GetCategoryName(catid)
        if name.find(u"шипы") != -1:
            accessoires += prestashop_api.GetCategoryActiveProductsIds(catid)
    for id in accessoires:
        prestashop_api.SetProductUnitPriceRatio(id, 0)
        prestashop_api.SetProductUnity(id, u"")
        prestashop_api.DeleteProductSpecificPrice(id)

def SetSpecificPrice():
    accessoires = []
    cats = [i[0] for i in prestashop_api.GetShopCategories(1)]
    for catid in cats:
        name = prestashop_api.GetCategoryName(catid)
        if name.find(u"сессуа") != -1:
            accessoires += prestashop_api.GetCategoryActiveProductsIds(catid)
    products = prestashop_api.GetActiveProductsIDs()
    for id in products:
        if id in accessoires:
            continue
        price = prestashop_api.GetShopProductPrice(id, 1)
        if price < 2500:
            productUnitPriceRatio = 1.075260
            reduction = 0.07
        elif price < 5000:
            productUnitPriceRatio = 1.052632
            reduction = 0.05
        else:
            productUnitPriceRatio = 1.030928
            reduction = 0.03
        prestashop_api.SetProductUnitPriceRatio(id, productUnitPriceRatio)
        prestashop_api.SetProductUnity(id, u"2 шт.")
        prestashop_api.AddProductSpecificPrice(id, 1, 2, reduction)

def SetProductMinimumQuantity():
    minimalOrder = 1700
    accessoires = []
    cats = [i[0] for i in prestashop_api.GetShopCategories(1)]
    for catid in cats:
        name = prestashop_api.GetCategoryName(catid)
        if name.find(u"силит") != -1:
            continue
        if name.find(u"сессуа") != -1:
            accessoires += prestashop_api.GetCategoryActiveProductsIds(catid)
    for id in accessoires:
        name = prestashop_api.GetProductName(id)
        if len([i for i in [u"лагшток", u"даптер", u"лотков"] if name.lower().find(i) != -1]) > 0:
            continue

        price = prestashop_api.GetShopProductPrice(id, 1)
        if price >= minimalOrder:
            continue
        print id, name
        number = int (minimalOrder / price) + 1
        prestashop_api.SetProductMinQuantity (id, number)

def AddProductAccessory():
    prestashop_api.InitAllProducts()
    prodId = prestashop_api.GetProductIdQuick(u"Град-А16")
    if prodId == -1:
        return
    accId1 = prestashop_api.GetProductIdQuick(u"Внешний динамик ГРАД А-16 100Вт (30м)")
    accId2 = prestashop_api.GetProductIdQuick(u"Внешний динамик к Град А-16 (5 м)")
    if accId1 != -1:
        prestashop_api.AddProductAccessory(prodId, accId1)
    if accId2 != -1:
        prestashop_api.AddProductAccessory(prodId, accId2)

def DeactivateProduct():
    prestashop_api.InitAllProducts()
##    prodId = prestashop_api.GetProductIdQuick(u"Барьер")
    prodId = prestashop_api.GetProductIdQuick(u"SITITEK КМ-01")
    prodId2 = prestashop_api.GetProductIdQuick(u"Торнадо ОЗВ.01")
    if prodId == -1:
        return
    prestashop_api.SetProductActive(prodId, 0)
    prestashop_api.SetProductRedirectType(prodId, '301')
    prestashop_api.SetProductRedirectedId(prodId, prodId2)

def AddNewProduct():
    data = {}
    data["name"] = u"Ультразвуковой стационарный антилай \"HKS99\""
    data["categories"] = u"Ультразвуковые стационарные антилаи"
    data["active"] = 0
    data["description_short"] = u"Аккумуляторный стационарный антилай для собак"
    data["description"] = u"""<p>Ультразвуковой прибор HKS99 предназначен для борьбы с лаем домашних и уличных собак. Он поможет вам успокоить даже очень шумное животное и добиться тишины без ущерба для питомца. Антилай HKS99 не вызывает проблем с настройкой и может использоваться как дома, так и на приусадебном участке.</p>
<p> </p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="https://otpugivateli-sobak.ru//img/cms/HKS99/5.jpg" alt="" width="600" height="451" /></p>
<p> </p>
<h3>Особенности антилая HKS99</h3>
<p>К достоинствам этого прибора следует отнести:</p>
<ol>
<li><strong>Полную безвредность для собак и людей.</strong> Устройство издает ультразвук, который не слышен человеческому уху. Для животных же подобные колебания являются сильным раздражителем. Собаки сразу перестают лаять, как только слышат ультразвук.<br /><br /><br /><img style="display: block; margin-left: auto; margin-right: auto;" src="https://otpugivateli-sobak.ru//img/cms/HKS99/6.jpg" alt="" width="500" height="430" /></li>
<li><strong>Миниатюрные размеры</strong>, позволяющие разместить антилай на любой поверхности. Глушитель можно закрепить на стене дома, заборе, дереве, собачьей будке или другой основе. Он не пострадает от дождя или ультрафиолета, так как имеет защищенный от погодных условий корпус.</li>
<li><strong>Наличие датчика определения лая.</strong> Прибор не работает круглосуточно. Он включается только при обнаружении лающей собаки (в пределах охватываемой аппаратом территории). Такой принцип действия позволяет устройству заметно экономить электроэнергию и работать от одной зарядки до 30 дней (длительность работы зависит от частоты включений).</li>
<li><strong>Возможность настройки чувствительности.</strong> Вы сможете самостоятельно указать дальность действия «Антилая». Максимальная дистанция составляет 15 метров.</li>
<li><strong>Универсальность.</strong> HKS99 имеет четыре частоты звучания. От выбора диапазона зависит влияние прибора на разные породы собак. </li>
<li><strong>Привлекательный дизайн.</strong> Корпус легко впишется в интерьер вашей квартиры или в стиль оформления приусадебного участка. Он не выгорит под воздействием солнечных лучей, так как имеет защитное покрытие от ультрафиолета.</li>
<li><strong>Цифровой дисплей.</strong> Наглядно отображает текущие настройки антилая.<br /><br /><br /><img style="display: block; margin-left: auto; margin-right: auto;" src="https://otpugivateli-sobak.ru//img/cms/HKS99/4.jpg" alt="" width="500" height="358" /><br /><br /></li>
<li><strong>Возможность зарядки через USB.</strong> Аппарат можно заряжать через обычный USB-шнур, например, от компьютера. Для зарядки от обычной розетки вам понадобится блок питания на 2А или меньше.</li>
</ol>
<p> </p>
<h3><img style="display: block; margin-left: auto; margin-right: auto;" src="https://otpugivateli-sobak.ru//img/cms/HKS99/2.jpg" alt="" width="499" height="460" /></h3>
<h3>Комплектация</h3>
<ul>
<li>Стационарный антилай HKS99 </li>
<li>Инструкция</li>
</ul>"""
    data["price"] = 3970
    data["wholesale_price"] = 1500
    data["images"] = []

    print prestashop_api.AddProductAllShops(data, "")

def AddPaymentMethod():
    name = u"Оплата на карту Сбербанка"
    descr = u"скидка 5%"
    state = u"Ожидание оплаты на карту Сбербанка"
    id_order_state = prestashop_api.GetOrderStateId(state)

    id_currency = str(prestashop_api.GetCurrencyId("Ruble"))
    id_country = str(prestashop_api.GetActiveCountryId())

    carriers = prestashop_api.GetActiveCarriersIds()
    excludedCarriers = prestashop_api.GetCarriersIdsByName(u"Самовывоз из нашего офиса:")
    for id in excludedCarriers:
        try:
            carriers.remove(id)
        except:
            pass
    carriers = [str(i) for i in carriers]
    available_carriers = ",".join(carriers)

    sql = u"""INSERT INTO ps_custom_payment_method( logo, id_order_state, type_commission,confirmation_page, 	apply_commission, discount_percent,available_groups, available_carriers, available_currencies, available_countries, cart_total_to, is_send_mail, commission_use_tax_on_products, discount_use_tax_on_products)
              VALUES ('1.png', {}, 0, 0, 0, 5,'1,2,3', '{}', '{}', '{}', 1000000, 0, 0, 0)""".format(id_order_state, available_carriers, id_currency, id_country)
    prestashop_api.MakeUpdateQueue(sql)
##    id_custom_payment_method = prestashop_api.GetLastInsertedId("ps_custom_payment_method", "id_custom_payment_method")
    id_custom_payment_method = 1
    sql = u"""INSERT INTO ps_custom_payment_method_lang(id_custom_payment_method, id_lang, name, details, description, description_short, name_message_field, error_message_field)
              VALUES ({}, 1, '{}', '', '{}', '{}', '', '')""".format(id_custom_payment_method, name, descr, descr)
    prestashop_api.MakeUpdateQueue(sql)
##    for shop in shops:
##        shopId = shop[0]
##        sql = u"""INSERT INTO ps_custom_payment_method_shop(id_custom_payment_method, id_shop)
##              VALUES ({},{})""".format(id_custom_payment_method, shopId)
##        prestashop_api.MakeUpdateQueue(sql)

def GetProductsUniqueness():
    global prestashop_api
    textru_api = TextruAPI.TextruAPI()
    prods = prestashop_api.GetShopActiveProducts(1)
    contentmonsterProducts = prestashop_api.GetProductsUniqueness()
    for prod in prods:
        try:
            id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description = prod
            if len([i for i in contentmonsterProducts if i[1] == prestashop_api.siteUrl and i[2] == id_product]) > 0:
                continue
            prodCats = prestashop_api.GetProductCategories(id_product)
            if 125 in prodCats:
                continue
            productDescription = commonLibrary.GetProductTextDescription(description)
            if productDescription != False:
                res = textru_api.GetTextUniqueness(productDescription)
                if res == -1:
                    continue
                unique, urls = res
                uniquenessInfo = ""
                domain = prestashop_api.GetShopMainDomain(1).replace("https://","")
                urls_ = [i for i in urls if i["url"].find(domain) == -1 and float(i["plagiat"]) > 15]
                if len(urls_) > 0:
                    entries = [u"{} : {}".format(i["url"], i["plagiat"]) for i in urls_]
                    uniquenessInfo = "\n".join(entries)
                data = {}
                data["site"] = prestashop_api.siteUrl
                data["productId"] = id_product
                data["name"] = name
                data["description"] = productDescription
                data["uniquenessInfo"] = uniquenessInfo
                data["url"] = prestashop_api.GetProductFullUrl(id_product, 1)
                prestashop_api.AddProductUniqueness(data)
        except:
            prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

def GetNonuniqueTextLen():
    ids = [i[2] for i in prestashop_api.GetNonuniqueSiteProducts(site["url"])]
    length = 0
    for id in ids:
        htmlDescription = prestashop_api.GetProductDescription(id)
        if htmlDescription != -1:
            descr = commonLibrary.GetProductTextDescription(htmlDescription)
            length += len(descr)

    print "length {}, cost {}".format( length, int(length * 0.075))

############################################################################################################################

logfile = log.GetLogFileName("log", "configuration")
logging = log.Log(logfile)
machineName = commonLibrary.GetMachineName()
##FillProductLang()
##cities = commonLibrary.GetCitiesTable()
prestashop_api = PrestashopAPI.PrestashopAPI()
##ids = prestashop_api.GetProductsIDs()
##sdek = SdekAPI.SdekAPI()
##rusPostApi = RussianPostAPI.RussianPostAPI()

productsFeatures = {}
features = {}
##UpdateCitiesTable()

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    if prestashop_api.siteUrl in ["http://safetus.ru", "http://omsk.knowall.ru.com"]:
        zoneId = 7
    else:
        zoneId = 9

    logging.Log("========={0}==========".format(prestashop_api.siteUrl))

    isSubfolders = prestashop_api.IsSubfolderSite()

    GetNonuniqueTextLen()
##    GetProductsUniqueness()


##    AddPaymentMethod()
##    AddNewProduct()
##    CustomizeProduct()
##    GetNonuniqueProducts()
##    DeactivateProduct()
##    MakeMegaMenuConf()
##    AddProductAccessory()
##    SetProductMinimumQuantity()
##    DeleteNotRussianShops()
##    AddSmartCategoryToAllShops()
##    GetCategoriesWithoutDescr()
##    CleanCategoriesTexts()
##    DeleteSpecificPrice()
##    SetSpecificPrice()
##    GetProductsWithOneImage()
##    CopyNames2()
##    AddProductsToRootCat()
##    DeleteArticle()

##    MakeIqitMenuConf()
##    ConfigureMegaMenu()
##    DeleteProducts()
##    CopyFeatures()
##    CopyProductFeatures()
##    MakeProductUnavailableForAllShops()
##    SetProductPriceForAllShops()

##    FillProductLang()
##    FillCitiesTable()
##    GetCategoriesUrls()
##    GetCategoriesUrlsSubfolders()
##    GetProductsUrlsSubfolders()
##    GetCategoriesUrlsSubdomains()
##    GetShopCategoriesUrlsSubdomains()
##    UpdateProductsMetatitles()
##    SetDefaultPagesTags()
##    id = 1
##
##    shops = prestashop_api.GetAllShops()
####    shops = prestashop_api.GetAllShopsFull()
##    for shop in shops:
##        shopId = shop[0]
##        print shopId
####        if shopId != 2:
####            continue
####        AddCustomLinkToMegaMenu()
##
##        try:
##            UpdateRussianPostDeliveryCost()
##        except:
##            prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])
####        shopName = commonLibrary.GetShopName(prestashop_api.GetShopName(shopId))
##        shopId, shopDomen, shopName = shop
##        logging.Log(str(shopId))
##        shopName = commonLibrary.GetShopName(shopName)
##        UpdateCarriersCost(u"урьером")
##        UpdateCarriersCost(u"На пункт выдачи")
##        print shopId
##        if shopId <= 160:
##            continue
##
##        AddRussianPostCarrier(shopId, shopName, zoneId)

##        if shopId < 2:
##            continue

##        ConfigureReinsurance(shopId, [u"reinsurance-3-1.png", "reinsurance-1-1.png", "reinsurance-2-1.png", "reinsurance-4-1.png", "reinsurance-5-1.png","reinsurance-1416-1.png"],
##        [u"Оплата при получении", u"Гарантия на все товары", u"Быстрая замена брака", u"Удобная и быстрая доставка", u"Безопасная оплата онлайн", u"Профессиональные консультации"])

##        ConfigureReinsurance(shopId, [u"reinsurance-1-1.png", "reinsurance-5-1.png", "reinsurance-4-1.png", "reinsurance-2-1.png", "reinsurance-1416-1.jpg","reinsurance-3-1.png"],
##        [u"Гарантия на все товары", u"Безопасная оплата онлайн", u"Удобная и быстрая доставка", u"Быстрая замена брака",  u"Профессиональные консультации", u"Оплата при получении"])
##        UpdateCarriersCost(shopId, u"Курьер")

##        logging.Log(str(shopId))
##        FillImagesAltTag(shopId, shopName)
##        DeleteImagesFromCategoriesDescriptions(shopId)

    ##    if shopId not in [27]:
    ##        continue

##        if shopId < 2:
##            continue

    ##    shopName = u"Москва"
##        print shopId
##        UpdateCarriersCost( u"урьер")

##        varsalue = "CAT143,CAT145,CAT146"
##        ConfigureMenu(shopId, varsalue)
##        ConfigurePhone(shopId, shopName)
        #x for x in range(1,20)
##        categoriesIds = [x[0] for x in prestashop_api.GetShopActiveCategories(shopId)]
##        ConfigureLayredBlock(shopId, categoriesIds,[x for x in range(1,9)])
##        ConfigureLayredBlock(shopId, categoriesIds,[9,10,11])

    if machineName.find("hp") != -1:
        ssh.kill()


