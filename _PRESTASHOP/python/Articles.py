﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import html2text
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import random
import subprocess
from datetime import date

def DeleteCategoryArticles(categoryId, excludedArticlesIds):
    query = """SELECT  id_smart_blog_post FROM ps_smart_blog_post
               WHERE id_category = %(categoryId)s """%{"categoryId":categoryId}

    data = prestashop_api.MakeGetInfoQueue(query)

    ids = [x[0] for x in data]

    for id_smart_blog_post in ids:
        if id_smart_blog_post in excludedArticlesIds:
            continue

        query = """DELETE FROM ps_smart_blog_post
                   WHERE id_smart_blog_post = %(id_smart_blog_post)s """%{"id_smart_blog_post":id_smart_blog_post}
        prestashop_api.MakeUpdateQueue(query)

        query = """DELETE FROM ps_smart_blog_post_lang
                   WHERE id_smart_blog_post = %(id_smart_blog_post)s """%{"id_smart_blog_post":id_smart_blog_post}
        prestashop_api.MakeUpdateQueue(query)

        query = """DELETE FROM ps_smart_blog_post_shop
                   WHERE id_smart_blog_post = %(id_smart_blog_post)s """%{"id_smart_blog_post":id_smart_blog_post}
        prestashop_api.MakeUpdateQueue(query)


def AddArticleToShops():
    prestashop_api.DeleteArticleFromShop(articleId, shopId)

    #добавляем статьи
    link_rewrite = linkRewrite + "-" + shopName
    link_rewrite = link_rewrite.replace(" ", "-")

    #ps_smart_blog_post
    id_smart_blog_post = prestashop_api.GetLastInsertedId("ps_smart_blog_post","id_smart_blog_post")
    id_smart_blog_post += 1

    query = """INSERT INTO ps_smart_blog_post(id_smart_blog_post, id_author, id_category, position, active, available, created, modified, viewed, is_featured, comment_status, post_type, image)
                      VALUES ({0},1,{1},0,1,1,'{2}','{2}',0,0,1,0,'')""".format(id_smart_blog_post,id_category,todayFormatDate)

    prestashop_api.MakeUpdateQueue(query)

    #ps_smart_blog_post_lang
    query = """INSERT INTO ps_smart_blog_post_lang(id_smart_blog_post, id_lang, meta_title, meta_keyword, meta_description, short_description, content, link_rewrite)
                VALUES (%(id_smart_blog_post)s,1,'%(meta_title)s','','','%(short_description)s','%(content)s','%(link_rewrite)s')"""%{"id_smart_blog_post":id_smart_blog_post,
                      "meta_title":meta_title,
                      "short_description":short_description,
                      "content":content,
                      "link_rewrite":link_rewrite
                      }

    prestashop_api.MakeUpdateQueue(query)

    #ps_smart_blog_post_shop
    query = """INSERT INTO ps_smart_blog_post_shop(id_smart_blog_post, id_shop)
                VALUES (%(id_smart_blog_post)s,%(shopId)s)"""%{"id_smart_blog_post":id_smart_blog_post,
                      "shopId":shopId}

    prestashop_api.MakeUpdateQueue(query)

    #добавление статьи в список статей категории
    articleTemplate = """<p><a target="_blank" href="/smartblog/id_link_rewrite.html">meta_title</a></p>"""
    articleTemplate = articleTemplate.replace("id", str(id_smart_blog_post)) \
                                        .replace("link_rewrite",link_rewrite ) \
                                        .replace("meta_title",meta_title )

    for categoryId in categories:
        articles = prestashop_api.GetCategoryArticles(shopId, categoryId)
        articles += articleTemplate
        prestashop_api.SetCategoryArticles(shopId, categoryId, articles)


def UpdateArticleContent():
    article_id = prestashop_api.GetArticleId(articleTitle, shopId)
    if article_id != -1:
        prestashop_api.SetArticleContentById(article_id, content)
        prestashop_api.SetArticleUpdateDate(article_id, todayFormatDate)

#############################################################################################################

prestashop_api = PrestashopAPI.PrestashopAPI()
machineName = commonLibrary.GetMachineName()

##DeleteCategoryArticles(4, [524, 1043, 1908])
todayFormatDate = date.today().strftime('%Y-%m-%d')

articleId = 8407
articleTitle = u"Средства и методы борьбы с крысами"
categories = [105, 107]

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

##    meta_title, short_description, Content, linkRewrite, id_category = prestashop_api.GetArticle(articleId)
    Content = u"""<p><img style="float: left; margin: 10px;" src="http://tomsk.otpugivatel.com//img/cms/ультразвуковые отпугиватели грызунов.jpg" alt="" width="220" height="156" /></p>
<p>Живущие около гаражей и подземных стоянок мыши и крысы настолько всеядны, что в отсутствие пищи легко могут перейти даже на автомобильную проводку или на внутренний интерьер автомобиля.</p>
<p>Для устранения такой угрозы для автоимущества придуманы и широко используются специальные средства от крыс и мышей, которые излучают ультразвук. Их установка предельно проста: они просто помещаются в двигательный отсек вашего автомобиля, когда тот не используется в течение длительного времени. Питание такое устройство получает от автомобильного аккумулятора, к которому его легко подсоединить с помощью специальных креплений-зажимов. </p>
<p>Говоря о конкретных марках таких отпугивателей, в первую очередь необходимо упомянуть модели «Цунами-4», а также «Тайфун ЛС-700». Все перечисленные модели предназначены для эксплуатации в летнее время (если машина ставится на открытую стоянку) или в отапливаемом гараже, где столбик термометра не падает до минусовых значений.</p>
<h3>Заголовок 1</h3>
<p>Если существует необходимость в приборе, который бы работал круглогодично, то есть зимой и летом, то стоит обратить внимание на ультразвуковой отпугиватель крыс и мышей «Торнадо 200-12К», сохраняющий свою работоспособность даже в сильный холод (-40 C) и изнуряющую жару (+80 C).</p>
<p>Все эти приборы отличаются друг от друга способом крепления к аккумулятору. Модель «Торнадо 200-12К» оснащена контактами, которые крепко соединяются с аккумулятором, обеспечивая надёжный контакт. Крепления «Тайфун ЛС-700» имеют меньшие размеры, а значит занимают меньше места под капотом. Самое надёжное соединение у ультразвукового отпугивателя грызунов «Цунами-4» — к аккумулятору он подсоединяется при помощи болтов. Это единственная модель из перечисленных, которая допускает стационарное использование.</p>
<p>В поисках пищи грызуны способны пробраться не только в моторный отсек, но и гораздо дальше — внутрь салона или даже в багажник. Особенно этого стоит опасаться, если внутри автомобиля вы храните съестные припасы. Следовательно, эти внутриавтомобильные пространства также нуждаются в защите. Её легко организовать с помощью ультразвукового отпугивателя мышей «Торнадо 200-12», которое включается в стандартное гнездо прикуривателя. К сожалению, не все автомобили поддерживают питание от прикуривателя при заглушенном двигателе. Тем, кто беспокоится о том, что при работе от прикуривателя устройство отпугивания очень быстро посадит аккумулятор, не стоит волноваться. Отпугиватель потребляет настолько мало энергии, что способен работать в активном режиме месяцами.</p>
<h3>Специфика гаражных отпугивателей</h3>
<p>Обычно гаражи используются не только для хранения автомобиля, но и как небольшая мастерская или кладовка. Следовательно, в гараже есть множество предметов, которые будут существенным образом ограничивать радиус эффективного действия любого отпугивателя. В капитальных гаражах может быть несколько уровней и раздельных помещений, что ещё более усложняет задачу избавления от грызунов. В таких гаражах, как правило, существует множество укрытий, где могут гнездиться и размножаться вредители. Чтобы гарантированно «дотянутся» до любого уголка гаражного помещения, целесообразно покупать сразу наиболее мощный из ультразвуковых отпугиватель крыс. К числу таких моделей относятся «Цунами» и «Цунами-2», предназначенные для «зачистки» от мышей и крыс пространств до 250-400 кв. м. Кроме того, они излучают сигнал внутри очень широкого сектора, а поэтому укрыться от него у вредителей вряд ли получится.</p>
<h3>Заголовок 2</h3>
<p> </p>
<p>В гараже без отопления лучше всего установить ультразвуковые отпугиватели «Торнадо-300» или «Торнадо-400», поскольку благодаря своей широкоугольности, они также охватывают излучением всё помещение, которое может быть площадью вплоть до 300-400 кв., а самое главное — эти модели способны работать даже при больших отрицательных температурах окружающего воздуха. Несмотря на их большую мощность, человеческое ухо не воспринимает излучаемый ими ультразвук.</p>
<p>Если планировка гаража позволяет, то можно установить в его центральной части ультразвуковой отпугиватель «Чистон-2». Особенность данной модели в том, что она излучает ультразвук вокруг себя и способна изгнать мышей и крыс из помещений площадью до 300 кв. м.</p>
<p>Тем кто стремится гибко регулировать уровень звука, предлагаем ознакомиться с моделью TransonicPro, которая эффективна на площади до 325 кв. м. Она позволяет владельцу гаража легко переключаться между различными режимами эффективными в тех или иных ситуациях.</p>
<p>К вопросу выбора места для установки отпугивателя необходимо подойти предельно внимательно. Самым оптимальным вариантом будет его установка над уровнем пола. Если вы знаете откуда появляются вредители, то направьте излучатель в это место. Установка отпугивателя прямо на полу является не самым лучшим вариантом, так как кабель питания может быть легко повреждён зубами крыс.</p>
<h3>Заголовок 3</h3>
<p> </p>
<p>В случае, если в гараже нет электропитания и по каким-либо причинам подключение к аккумулятору автомобиля также невозможно, то выход из такой ситуации заключается в покупке ультразвукового отпугивателя крыс и мышей «Град А-500», который способен работать от обычных батареек.</p>
<p>Чтобы предельно обезопасить и автомобиль и гараж, используйте сразу 2 отпугивателя. Один из них установите внутрь автомобиля, а второй поместите снаружи в помещении гаража. Использовать устройства для отпугивания необходимо беспрерывно до того момента, когда грызуны уйдут из гаража окончательно. После этого устройства следует включать лишь время от времени в целях профилактики.</p>"""

    shops = prestashop_api.GetAllShops()
    bSubfolders = prestashop_api.siteUrl in commonLibrary.GetSubfoldersSites()

    for shop in shops:
        shopId, shopDomen, shopName = shop
##        if shopId < 2:continue

        shopName = commonLibrary.GetShopName(shopName)
        print shopName

        #составляем список ссылок
        p = re.compile('id[0-9]+')
        productsIds = [x.replace("id", "") for x in p.findall(Content)]
        p = re.compile('id_cat[0-9]+')
        categoriesIds = [x.replace("id_cat", "") for x in p.findall(Content)]

        content = Content

        if bSubfolders:
            subfolder = "/" + prestashop_api.GetShopVirtualUri(shopId)
        else:
            subfolder = "/"

        for productId in productsIds:
            productUrl = subfolder + prestashop_api.GetProductUrl(productId, shopId)
            content = content.replace("id" + productId, productUrl)
        for catId in categoriesIds:
            catUrl = subfolder + prestashop_api.GetCategoryUrl(catId, shopId)
            content = content.replace("id_cat" + catId, catUrl)

##        AddArticleToShops()
        UpdateArticleContent()

    if machineName.find("hp") != -1:
        ssh.kill()
