﻿import log
import datetime
import sys
import PrestashopAPI
import io
import subprocess
import time
import ftplib
import os
import psutil
import shutil
import commonLibrary
import YandexDiskApi
import SendMails
import paramiko

commands = """
cd /home/{dirName}
mysqldump -uroot -p1qaz@WSX1qaz@WSX {database} > {database}.sql
zip {database}.zip {database}.sql
rm {database}.sql
rm -rf www/cache/smarty/cache/*
rm -rf www/cache/smarty/compile/*
rm -rf www/themes/default-bootstrap/cache/*
rm -rf www/cache/cachefs/*
zip -r files.zip www
"""

days = 2

ya_api = YandexDiskApi.YandexDiskApi()
mails = SendMails.SendMails()

message = ""

bErrors = False

for site in commonLibrary.sitesParams:
    ssh_, sftp = commonLibrary.CreateSshSession(site["ip"])
    message += ("\n=========  " + site["url"] + "  =========\n\n")
    comm = commands.replace("{database}", site["dbName"]).replace("{dirName}", site["siteDir"])
    fileName = "commands\\comm_" + site["siteDir"] + ".txt"

    with open(fileName, "w") as vhostsfile:
        vhostsfile.write(comm)

    ssh = subprocess.Popen([commonLibrary.puttyPath, '-load', site["sessionName"], '-l', commonLibrary.sshLogin, '-pw', commonLibrary.sshPassword, '-m', fileName])
    ssh.wait()

##    yaDiskDir = "/backups/" + site["url"].replace("http://", "")

    #удаляем бэкапы старше {days} дней
    dirs = os.listdir(site["backupDir"])
    for dir_ in dirs:
        date_object = datetime.datetime.strptime(dir_, '%d_%m_%Y')
        pastDays = (datetime.date.today() - date_object.date()).days
        if pastDays >= days:
            shutil.rmtree(site["backupDir"] + "\\" + dir_)

    backupDir = site["backupDir"] + "\\" + datetime.date.today().strftime('%d_%m_%Y')
    if not os.path.exists(backupDir):
        os.makedirs(backupDir)

##    todayYaBackupDir = "{0}/{1}".format( yaDiskDir, datetime.date.today().strftime('%d_%m_%Y'))
##    resp = ya_api.CreateDir(todayYaBackupDir)
##    if resp in [True, 'DiskPathPointsToExistentDirectoryError']:
##        message += "Создана папка {0}\n".format(todayYaBackupDir)
##    else:
##        message += "[Error] Ошибка создания папки {0}: {1}\n".format(todayYaBackupDir, resp)
##        bErrors = True
##        continue

    bSuccess = True

    filesFileName = backupDir + "\\{0}_files.zip".format(site["dbName"])
    databaseFileName = backupDir + "\\{0}_database.zip".format(site["dbName"])
    sourceDbFile = '/home/{0}/{1}.zip'.format(site["siteDir"], site["dbName"])
    try:
        sftp.get('/home/{0}/files.zip'.format(site["siteDir"]), filesFileName)
        sftp.get(sourceDbFile, databaseFileName)
        message += "Файлы успешно скачаны\n".format(fileName)
    except:
        message += "Ошибка при скачавании файлов\n".format(fileName)
##    for fileName in [filesFileName, databaseFileName]:
##        resp = ya_api.SendFile(fileName, todayYaBackupDir)
##        if resp == True:
##            message += "Файл {0} успешно отправлен\n".format(fileName)
##        else:
##            message += "[Error] Ошибка отправки файла {0}: {1}\n".format(fileName, resp)
##            bErrors = True

##    if bSuccess == True:
##        shutil.rmtree(backupDir)
##        #удаляем бэкапы старше {days} дней
##        dirs = ya_api.GetSubdirectories(yaDiskDir)
##        for dir_ in dirs:
##            date_object = datetime.datetime.strptime(dir_, '%d_%m_%Y')
##            pastDays = (datetime.date.today() - date_object.date()).days
##            if pastDays >= days:
##                dirName = "{0}/{1}".format( yaDiskDir, dir_)
##                resp = ya_api.DeleteResource(dirName)
##                if resp == True:
##                    message += "Удалена папка {0}\n".format(dirName)
##                else:
##                    message += "[Error] Ошибка удаления папки {0}: {1}\n".format(dirName, resp)
##                    bErrors = True

#prestashop
commands = """
cd /home
mysqldump -uroot -p1qaz@WSX1qaz@WSX {database} > {database}.sql
zip {database}.zip {database}.sql
rm {database}.sql"""

comm = commands.replace("{database}", "prestashop")
fileName = "commands\\comm_prestashop.txt"

with open(fileName, "w") as vhostsfile:
    vhostsfile.write(comm)

ssh = subprocess.Popen([commonLibrary.puttyPath, '-load', "95.216.196.140", '-l', commonLibrary.sshLogin, '-pw', commonLibrary.sshPassword, '-m', fileName])
ssh.wait()

ssh_ = paramiko.SSHClient()
ssh_.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_.connect("95.216.196.140", port=22, username=commonLibrary.sshLogin, password=commonLibrary.sshPassword)
sftp = ssh_.open_sftp()

sftp.get('/home/prestashop.zip', "c:\\backups\\prestashop.zip")
##resp = ya_api.SendFile("c:\\\\backups\\prestashop.zip","/backups")
##if resp == True:
##    message += "Файл {0} успешно отправлен\n".format("prestashop.zip")
##else:
##    message += "[Error] Ошибка отправки файла {0}: {1}\n".format("prestashop.zip", resp)
##    bErrors = True

ssh_.close()

theme = "Бэкапы"
if bErrors != False:
    theme += " (есть ошибки!)"
mails.SendInfoMail(theme, message)