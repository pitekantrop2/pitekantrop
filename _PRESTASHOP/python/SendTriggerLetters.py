﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary

states = [
u"Вручен", u"Доставлен и оплачен"]

downcase = lambda s: s[:1].lower() + s[1:] if s else ''

def SendMailToCustomer(site, orderId, mode, couponData = []):

##    if site["url"] in ["http://safetus.ru", "http://omsk.knowall.ru.com"]:
##        return

    global lettersCounter

    reviewDiscount = 150

    data = mails.GetSenderData(site["url"])

    dateEnd = today + datetime.timedelta(days=90)

    pageId = prestashop_api.GetIdCmsByTitle(u"Скидки")
    shopUrl = commonLibrary.GetSiteUrlScheme(site["url"]) + prestashop_api.GetOrderShopUrl(orderId)
    data["pageUrl"] = shopUrl + "/content/" + str(pageId) + "-skidki"
    data["order_name"] = prestashop_api.GetOrderReference(orderId)

##    customer = prestashop_api.GetOrderCustomer(orderId)
##    idCustomer = customer[3]

    cartRuleParams = {}
    cartRuleParams["id_customer"] = idCustomer
    cartRuleParams["date_from"] =  today
    cartRuleParams["date_to"] = dateEnd

    if mode == "coupon":
        percent = 7

        coupons = prestashop_api.GetCustomerCoupons(idCustomer, u"за заказ")

        code = data["order_name"] + "D"

        for coupon in coupons:
            if coupon[1] == code:
                return

        percent += len(coupons)

        cartRuleParams["code"] = code
        cartRuleParams["reduction_percent"] = str(percent)
        cartRuleParams["active"] = "1"
        cartRuleParams["name"] = u"Скидка " + str(percent) + u"% за заказ"

        data["code"] = cartRuleParams["code"]
        data["reduction_percent"] = cartRuleParams["reduction_percent"]


    if mode == "review":
        deliveryDate = prestashop_api.GetOrderStateDate(orderId, u"Вручен")
        if deliveryDate == -1:
            prestashop_api.AddCustomerToIgnoreList(idCustomer)
            return
        data["date"] = deliveryDate.strftime('%d.%m.%Y')
##        coupons = prestashop_api.GetCustomerCoupons(idCustomer, u"тзыв")
##
##        code = data["order_name"] + "R"
##
##        for coupon in coupons:
##            if coupon[1] == code:
##                return
##
##        cartRuleParams["code"] = code
##        cartRuleParams["reduction_amount"] = str(reviewDiscount)
##        cartRuleParams["active"] = "0"
##        cartRuleParams["name"] = u"Скидка за отзыв"
##
##        data["code"] = cartRuleParams["code"]
##        data["reduction_amount"] = cartRuleParams["reduction_amount"]

    if mode == "coupon_activation":
        coupons = prestashop_api.GetCustomerCoupons(idCustomer, u"тзыв")
        couponCode = coupons[-1][1]
        data["code"] = couponCode
        data["reduction_amount"] = str(reviewDiscount)
        dateEnd = coupons[-1][5]

    if mode == "coupon_reminder":
        data["dateTo"] = couponData[2].strftime('%d.%m.%Y')
        data["code"] = couponData[0]
        data["reduction_percent"] = str(int(couponData[4]))
        data["reduction_amount"] = str(int(couponData[3]))
        data["categories"] = "<ul>"
        mainCategories = prestashop_api.GetShopMainCategories(prestashop_api.GetOrderShopId(orderId))
        for mainCategory in mainCategories:
            li = "<li><a href = '{href}'>{name}</a></li>"
            href = shopUrl + "/" + str(mainCategory[0]) + "-" + mainCategory[2]
            data["categories"] += li.replace("{href}", href).replace("{name}",mainCategory[1])
        data["categories"] += "</ul>"


    if test == False:
        if mode == "coupon":
            prestashop_api.CreateCartRule(cartRuleParams)

    data["shop_url"] = shopUrl

    data["dateEnd"] = dateEnd.strftime('%d.%m.%Y')
    data["firstname"] = firstname
    data["lastname"] = lastname
    data["email"] = email
    if test == True:
        data["email"] = "pitekantrop@list.ru"

    orderItems = prestashop_api.GetOrderItems(orderId)
    data["item_name"] = orderItems[0][1]
    itemUrl = prestashop_api.GetProductUrl(orderItems[0][0], prestashop_api.GetOrderShopId(orderId))
    if itemUrl == -1:
        return

    data["itemUrl"] = data["shop_url"].strip("/") + "/" + itemUrl

    #отправка письма
    result = mails.SendMailToCustomer(mode, data)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма клиенту: " + result.decode("cp1251"))
    else:
        logging.LogInfoMessage(u"Отправлено письмо '" + mode + u"' по заказу " + str(orderId) )
        lettersCounter += 1

    prestashop_api.AddCustomerToIgnoreList(idCustomer)
    return result


####################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "reviews")
logging = log.Log(logfile)

test = False
##test = True

machineName = commonLibrary.GetMachineName()
sdek = SdekAPI.SdekAPI()
mails = SendMails.SendMails()

today = datetime.date.today()

endDate = datetime.date.today() - datetime.timedelta(days=50)
startDate = endDate - datetime.timedelta(days=360)

logging.Log(u"Период: " + startDate.strftime('%d.%m.%Y') + " - " + endDate.strftime('%d.%m.%Y'))


for site in commonLibrary.sitesParams:
    if site["url"] in ["http://amazing-things.ru"]:
        continue

    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    #получение неопубликованных отзывов
    customers = prestashop_api.GetUnvalidatedReviewsCustomers()
    customersNames = []
    for customer in customers:
        if customer[0] == 0:
            customerFIO = u"Неопознанный покупатель"
        else:
            custInfo = prestashop_api.GetCustomerInfo(customer[0])
            customerFIO = custInfo[0] + " " + custInfo[1]

        logging.LogInfoMessage(u"Неопубликован отзыв от покупателя " + customerFIO)
        customersNames.append(site["url"] + ": " + customerFIO)

    if len(customersNames) != 0:
        mails.SendInfoMail("Неопубликованные отзывы от покупателей", '\n'.join(customersNames).encode("utf-8"))

##    if site["url"] in ["http://safetus.ru", "http://omsk.knowall.ru.com"]:
##    if machineName.find("hp") != -1:
##        ssh.kill()
##    continue

    ignoredCustomers = prestashop_api.GetMailingIgnoreCustomers()
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)
    lettersCounter = 0

    for order in orders:
        if lettersCounter > 15:
            break

        orderId = order[0]
        customer = prestashop_api.GetOrderCustomer(orderId)
        if customer == -1:
            continue
        firstname, lastname, email, idCustomer  = customer
        if idCustomer == 0 or email.find("@") == -1 or idCustomer in ignoredCustomers:
            continue

##        deliveryDate = prestashop_api.GetOrderStateDate(orderId, u"Вручен")
        SendMailToCustomer(site, orderId, "review")

##        if deliveryDate != -1:
##            if pastDays > 6:
##                SendMailToCustomer(site, orderId, "coupon")
##            SendMailToCustomer(site, orderId, "review")

    #активация купонов за опубликованные отзывы
##    customers = prestashop_api.GetValidatedReviewsCustomers()
##    for customer in customers:
##        idCustomer = customer[0]
##        if idCustomer == 0:
##            continue
##        coupons = prestashop_api.GetCustomerCoupons(idCustomer, u"тзыв")
##        if len(coupons) != 0:
##            couponCode = coupons[-1][1]
##            if prestashop_api.GetIsCouponActivated(couponCode) != True:
##                prestashop_api.ActivateCoupon(couponCode)
##                SendMailToCustomer(site, prestashop_api.GetCustomerOrdersIds(idCustomer)[0], "coupon_activation")
##        else:
##            custInfo = prestashop_api.GetCustomerInfo(customer[0])
##            customerFIO = custInfo[0] + " " + custInfo[1]
##            mails.SendInfoMail("Создание купона", "Необходимо вручную создать купон за отзыв для покупателя " + customerFIO.encode("utf-8"))
##
##    #рассылка напоминаний
##    activeCoupons = prestashop_api.GetActiveCoupons()
##    ignoredCustomers = prestashop_api.GetMailingIgnoreCustomers()
##
##    for activeCoupon in activeCoupons:
##
##        code, id_customer, date_to, reduction_amount, reduction_percent, date_from = activeCoupon
##        if id_customer in ignoredCustomers:
##            continue
##
##        if id_customer == 0 or date_to.date() <= today or (today - date_from.date()).days < 20 :
##            continue
##
##        couponDate = prestashop_api.GetCouponReminderDate(code)
##        try:
##            customerOrderId = prestashop_api.GetCustomerOrdersIds(id_customer)[0]
##        except:
##            continue
##        if couponDate == -1:
##            result = SendMailToCustomer(site, customerOrderId, "coupon_reminder", activeCoupon)
##        else:
##            pastDays = (datetime.date.today() - couponDate.date()).days
##            if pastDays > 19:
##                result = SendMailToCustomer(site, customerOrderId, "coupon_reminder", activeCoupon)
##            else:
##                continue
##
##        if test == False:
##            prestashop_api.SetCouponReminderDate(code, today)

    if machineName.find("hp") != -1:
        ssh.kill()

time.sleep(50)
##logging.LogInfoMessage(u"Отправлено писем: " + str(lettersCounter))
result = mails.SendInfoMail("Отчет по отправке писем", "", [logfile])