﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import html2text
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess
import SendMails
import xlrd


#########################################################################################################################
logfile = log.GetLogFileName("log", "updateCdekPvz")
logging = log.Log(logfile)
prestashop_api = PrestashopAPI.PrestashopAPI()

sql = """SELECT * FROM sdek_pvz"""
pvzTable = prestashop_api.MakeLocalDbGetInfoQueue(sql)
pvzCodes = [x[0] for x in pvzTable]

resp = requests.get("https://integration.cdek.ru/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

pvzNodes = root.xpath("//Pvz[@CountryCode = '1']")
actualPvzCodes = []


for pvzNode in pvzNodes:
    pvzCode = pvzNode.attrib["Code"]
    pvzName = pvzNode.attrib["Name"]
    cityCode = pvzNode.attrib["CityCode"]
    cityName = pvzNode.attrib["City"]
    WorkTime = pvzNode.attrib["WorkTime"]
    Address = pvzNode.attrib["Address"]
    Phone = pvzNode.attrib["Phone"]
    coordX = pvzNode.attrib["coordX"]
    coordY = pvzNode.attrib["coordY"]
    Type = pvzNode.attrib["Type"]
    Site = pvzNode.attrib["Site"]
    actualPvzCodes.append(pvzCode)
    if pvzCode not in pvzCodes:
        sql = u"""INSERT INTO sdek_pvz(pvzCode, pvzName, cityCode, cityName, WorkTime, Address, Phone, coordX, coordY, Type, Site)
                  VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')""" \
                  .format(pvzCode, pvzName, cityCode, cityName, WorkTime, Address, Phone, coordX, coordY, Type, Site)
        prestashop_api.MakeLocalDbUpdateQueue(sql)
        logging.Log(u"{0} добавлен".format(pvzCode))

for pvzCode in pvzCodes:
    if pvzCode not in actualPvzCodes:
        sql = """DELETE FROM sdek_pvz
                 WHERE pvzCode = '{0}'""".format(pvzCode)
        prestashop_api.MakeLocalDbUpdateQueue(sql)
        logging.Log(u"{0} удален".format(pvzCode))