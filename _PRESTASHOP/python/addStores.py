﻿import requests
import lxml
from lxml import etree
import urllib
import json
import datetime
import sys
import re, urlparse
import PrestashopAPI
import io
import RussianPostAPI

##prestashop_api = PrestashopAPI.PrestashopAPI("http://otpugiwatel.com", 'QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6', "lfb7967i_pres", "yx8WJNFBB")
##prestashop_api = PrestashopAPI.PrestashopAPI("http://omsk.knowall.ru.com", 'KAR7M17CPPALJY38NKI521NNP96IH3Z7', "127.0.0.1", "knowall", "knowall_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://safetus.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", "safetus", "safetus_user_ex", "1qaz@WSX", 5555)
prestashop_api = PrestashopAPI.PrestashopAPI("http://amazing-things.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", "amazing_things", "am_th_user_ex", "1qaz@WSX", 5555)

rusPostApi = RussianPostAPI.RussianPostAPI()

resp = requests.get("http://gw.edostavka.ru:11443/pvzlist.php")

pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

shops = prestashop_api.GetAllShops()

for shop in shops:

    shopId, shopDomen, shopName = shop

    if shopId == 1:
        continue

    print shopName

    pvzNodes = root.xpath("//Pvz[contains(@City, '" + shopName.encode("cp1251").replace("\xa0", " ").decode("cp1251") + "')]")

    if len(pvzNodes) == 0:
        continue

    for pvzNode in pvzNodes:

        storeID = prestashop_api.GetStoreID(pvzNode.attrib["Address"])

        if storeID != -1:
            continue

        street =  pvzNode.attrib["Address"].split(",")[0][0:4]
        index = rusPostApi.GetIndex(shopName, street)
        if index == -1: index = "000000"

        hours = """a:7:{i:0;s:13:"09:00 - 18:00";i:1;s:13:"09:00 - 18:00";i:2;s:13:"09:00 - 18:00";i:3;s:13:"09:00 - 18:00";i:4;s:13:"09:00 - 18:00";i:5;s:13:"10:00 - 16:00";i:6;s:0:"";}"""

        last_insert_id = prestashop_api.GetLastInsertedId("ps_store","id_store") + 1

        sql = """INSERT INTO ps_store (id_store, id_country, id_state, name, address1, address2, city, postcode, latitude, longitude, hours, phone, fax, email, note, active, date_add, date_upd)
             VALUES (%(last_insert_id)s, 177, 0,'%(name)s', '%(address1)s', '','%(city)s', '%(postcode)s', %(latitude)s, %(longitude)s,'%(hours)s', '%(phone)s', '', '','',1,'2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"name":pvzNode.attrib["Name"],
             "address1":pvzNode.attrib["Address"],
             "city":shopName,
             "postcode":index,
             "latitude":pvzNode.attrib["coordY"],
             "longitude":pvzNode.attrib["coordX"],
             "hours":hours,
             "phone":pvzNode.attrib["Phone"],
             "last_insert_id":last_insert_id
             }

        prestashop_api.MakeUpdateQueue(sql)

        for shop in prestashop_api.GetAllShops():

            shopID = shop[0]

            sql = """INSERT INTO ps_store_shop (id_store, id_shop)
                        VALUES (%(id_store)s, %(id_shop)s)"""%{"id_store":last_insert_id,"id_shop":shopID}

            prestashop_api.MakeUpdateQueue(sql)

