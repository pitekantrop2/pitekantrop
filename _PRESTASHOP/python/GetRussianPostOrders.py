﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import commonLibrary

states = [
u"В процессе подготовки",
u"Данного товара нет на складе"
]

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "postOrders")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
bOrders = False

missingItems = {}
message = ""

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=45)

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    message += ("\n===========  " + site["url"] + "  ===========\n\n")

    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:
        orderId = order[0]
        carrierName = prestashop_api.GetOrderCarrierName(orderId)
        if carrierName == -1:
            continue

        if carrierName.find(u"Почтой") != -1:
            bOrders = True
            note = prestashop_api.GetOrderNote(orderId)
            status = ""
            if commonLibrary.GetOrderIsAgreed(note) == False:
                status = " - НЕ СОГЛАСОВАН"
            message += "\nЗаказ {0} {1}:\n".format(orderId, status)

            orderItems = prestashop_api.GetOrderItems(orderId)
            for orderItem in orderItems:
                productQty = orderItem[3]
                productId = orderItem[0]
                productName = prestashop_api.GetProductOriginalName(productId)
                if productName == "":
                    productName = orderItem[1]

                productName = commonLibrary.CleanItemName(productName)

                warehouseProductQuantity = prestashop_api.GetWarehouseProductQuantity("warehouse_spb", productName)
                exist = ""
                if warehouseProductQuantity < productQty:
                    exist = "(отсутствует в Спб)"
                    itemIndex = -1
                    try:
                        itemIndex = missingItems.keys().index(productName)
                    except:
                        pass

                    if itemIndex != -1:
                        missingItems[productName] += productQty
                    else:
                        missingItems[productName] = productQty
                message += "{0} - {1} шт. {2}\n".format(productName.encode("utf-8"), productQty, exist)

    if machineName.find("hp") != -1:
        ssh.kill()

if len(missingItems.keys()) != 0:
    message += "\n\n=========================================\n"
    for productName in missingItems.keys():
        message += "{0} - {1} шт.\n".format(productName.encode("utf-8"), missingItems[productName])

if bOrders == True:
    result = mails.SendInfoMail("Информация по почтовым заказам", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(50)
