﻿
# -*- coding: utf-8 -*-
import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import time
import commonLibrary

def GetIndex(address):
    parts = address.split(" ")
    for part in parts:
        if len(part) == 6 and part[0].isdigit() == True:
            return part

    return False

################################################################################

states = [
u"В процессе подготовки"]

prestashop_api = PrestashopAPI.PrestashopAPI()



logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processRussianPostOrders")
logging = log.Log(logfile)
mails = SendMails.SendMails()
rp = RussianPostAPI.RussianPostAPI()
machineName = commonLibrary.GetMachineName()

message = ""
sendLetter = False

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=90)

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:
        orderId = order[0]

        carrierName = prestashop_api.GetOrderCarrierName(orderId)
        if carrierName == -1:
            continue
        if carrierName.find(u"Почтой") == -1:
            continue

        note = prestashop_api.GetOrderNote(orderId)

        if note.find("toRussianPost") == -1:
            continue

        sendLetter = True

        message += "\n=========  " + site["url"] + "  =========\n"
        logging.Log("=========  " + site["url"] + "  =========")

        deliveryAddressData = prestashop_api.GetOrderDeliveryData(orderId)
        customer = prestashop_api.GetOrderCustomer(orderId)
        customerFio = customer[0] + " " + customer[1]
        if len(customerFio.strip().split(" ")) < 3:
            message += "[Error] Не полностью указаны ФИО клиента в заказе {0}\n".format(orderId)
            logging.LogErrorMessage(u"Не полностью указаны ФИО клиента в заказе {0}".format(orderId))
            continue

        deliveryAddressData = prestashop_api.GetOrderDeliveryData(orderId)
        index = GetIndex(deliveryAddressData[1])
        if index == False:
            message += "[Error] Не указан индекс в заказе {0}\n".format(orderId)
            logging.LogErrorMessage(u"Не указан индекс в заказе {0}".format(orderId))
            continue

        orderPaid = commonLibrary.GetOrderPaid(note)
        if orderPaid == False:
            cod = str(int(prestashop_api.GetOrderTotalCost(orderId)))
            orderCost = cod
        else:
            orderCost = "50"
            cod = "0"

        orderData = {}
        orderData["from_fio"] = u"Усов Иван Викторович"
        orderData["from_addr"] = u"Бабушкина, 11, кв. 54"
        orderData["from_index"] = u"192029"
##        orderData["from_addr"] = u"Лиговский проспект, 87, офис 621"
##        orderData["from_index"] = u"191040"
        orderData["p_seriya"] = u"5212"
        orderData["p_nomer"] = u"163428"
        orderData["p_vidal"] = u"Отделом №2 УФМС России в Советском АО г. Омска"
        orderData["p_vidan"] = u"11.01.2013"
##        orderData["from_fio"] = u"Коробкин Кирилл Сергеевич"
##        orderData["from_addr"] = u"Лиговский проспект, 87, офис 621"
##        orderData["from_index"] = u"191040"
##        orderData["p_seriya"] = u"4009"
##        orderData["p_nomer"] = u"892213"
##        orderData["p_vidal"] = u"ТП №51 отдела УФМС России в Московском р-не гор. Санкт-Петербурга"
##        orderData["p_vidan"] = u"05.11.2009"
        orderData["to_fio"] = customerFio
        orderData["to_tel"] = deliveryAddressData[4]
        orderData["to_index"] = index
        orderData["to_addr"] = deliveryAddressData[0]
        orderData["ob_cennost_rub"] = orderCost
        orderData["nalogka_rub"] = cod

        blanksUrls = rp.GetBlanks(orderData)
        if blanksUrls != -1:
            message += "\nСозданы бланки по заказу {0}: {1}\n".format(str(orderId), ", ".join(blanksUrls))
            logging.LogInfoMessage(u"Созданы бланки по заказу {0}: {1}\n".format(str(orderId), ", ".join(blanksUrls)))

            items = prestashop_api.GetOrderItems(orderId)
            prestashop_api.SetOrderState(orderId, u"Ждет отправки")
            note = note.replace("toRussianPost","")
            prestashop_api.SetCustomerNote(customer[3], note)

            for item in items:
                productOriginalName = prestashop_api.GetProductOriginalName(item[0])
                try:
                    prestashop_api.DecreaseWarehouseProductQuantity("warehouse_spb", productOriginalName, item[3])
                    productQty = prestashop_api.GetWarehouseProductQuantity("warehouse_spb", productOriginalName)
                    logging.LogInfoMessage(u"Остаток товара '{0}' - {1} шт.".format(productOriginalName, str(productQty)))
                    message += "Остаток товара '{0}' - {1} шт.\n".format(productOriginalName.encode("utf-8"), str(productQty))
                except:
                    mails.SendInfoMail("Списание товаров вручную", "'{0}' - {1} шт.\n".format(productOriginalName.encode("utf-8"), str(item[3])))

        else:
            message += "[Error] Ошибка создания бланков по заказу {0}\n".format(orderId)
            logging.LogErrorMessage(u"Ошибка создания бланков по заказу {0}".format(orderId))


    if rp.selenium != None:
        rp.CloseSelenium()

    if machineName.find("hp") != -1:
        ssh.kill()


if sendLetter != False:
    result = mails.SendInfoMail("Обработка почтовых заказов", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
