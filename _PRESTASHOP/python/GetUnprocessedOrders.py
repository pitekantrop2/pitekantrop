﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary
import time

def IsOrderAgreed(note, orderId):
    if len(orderNote.strip()) < 2:
        if orderState != u"Доставлен":
            return False
    else:
        if orderNote.find(u"дозвон") == -1 and orderState not in [u"Доставлен", u"В ожидании оплаты банком"]:
            return True

    today = datetime.date.today()

    maxNumberOfCalls = 3

    if orderState == u"Доставлен":
        shippedDate = prestashop_api.GetOrderStateDate(orderId, u"Доставлен")

        if orderCarrier.find(u"почтой") != -1:
            pauseDays = 10
        else:
            pauseDays = 7

        pastDays = (today - shippedDate.date()).days
        if pastDays < pauseDays:
            return True

    if orderState == u"В ожидании оплаты банком":
        maxNumberOfCalls = 1
        creationDate = prestashop_api.GetOrderStateDate(orderId, u"В ожидании оплаты банком")
        pastDays = (today - creationDate.date()).days
        if pastDays < 10:
            return True

    numOfFailedCalls = 0

    rows = orderNote.split("\n")
    for row in rows:
        parts = [x for x in row.split(" ") if len(x.strip()) > 0]
        for part in parts:
            if part[0].isdigit() == True:
                part = part.replace(",",".")
                part = part.replace("/",".")
                part = part.strip(".")
                try:
                    date_object = datetime.datetime.strptime(part + "." + str(today.year), '%d.%m.%Y')
                except:
                    continue

                commentDate = date_object.date()
                pastDays = (today - commentDate).days
                if pastDays == 0:
                    return True
                else:
                    if row.find(u"звон") != -1:
                        numOfFailedCalls += 1
                        if numOfFailedCalls == maxNumberOfCalls:
                            return True
                    else:
                        return True

    return False

################################################################################


states = [
u"В процессе подготовки",
u"Awaiting PayAnyWay payment",
u"Данного товара нет на складе",
u"Доставлен",
u"В ожидании оплаты банком"
]

prestashop_api = PrestashopAPI.PrestashopAPI()
machineName = commonLibrary.GetMachineName()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "unprocessedOrders")
logging = log.Log(logfile)
mails = SendMails.SendMails()

now = datetime.datetime.now()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=40)

message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue


    message += ("<br/><p>=========  " + site["url"] + "  =========<p>")
    logging.Log("=========  " + site["url"] + "  =========")
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]

    for order in orders:
        orderId = order[0]

        orderState = prestashop_api.GetOrderState(orderId)
        shopname =  prestashop_api.GetOrderShopName(orderId)

        if shopname == -1:
	       shopname = u"неизвестно"

        if orderState == -1:
            continue

        orderNote = prestashop_api.GetOrderNote(orderId)
        orderCarrier = prestashop_api.GetOrderCarrierName(orderId).lower()
        if orderCarrier == -1:
            orderCarrier = ""

        if orderCarrier.find("\"") != -1:
            orderCarrier = orderCarrier.split("\"")[0].strip()
        else:
            orderCarrier = orderCarrier.split(".")[0].strip()

        orderCreationDate = prestashop_api.GetOrderCreationDate(orderId)
        pastHours = int((now - orderCreationDate).total_seconds() / 3600) - 3

        boUrl = "{0}?controller=AdminOrders&id_order={1}&vieworder".format(backofficeUrl, orderId)
        orderProducts = ""
        orderItems = prestashop_api.GetOrderItems(orderId)
        for orderItem in orderItems:
            itemId = orderItem[0]
            upc = prestashop_api.GetProductUpc(itemId)
            if upc in ["", None]:
                upc = "http://" + prestashop_api.GetOrderShopUrl(orderId) + "/" + prestashop_api.GetProductUrl(itemId, prestashop_api.GetOrderShopId(orderId))
            orderProducts += "<a href='{2}'>{0}</a> - {1} шт.<br/>".format(prestashop_api.GetProductOriginalName(itemId).encode("utf-8"), orderItem[3],upc.encode("utf-8"))

        if IsOrderAgreed(orderNote, orderId) == False:
            if pastHours > 100:
                color = "darkgoldenrod"
                orderProducts = ""
            else: color = "red"
            logging.LogInfoMessage(u"Заказ " + str(orderId) + " (" + orderCarrier + u") НЕ ОБРАБОТАН (часов): " + str(pastHours))
            message += "<p style='color:{4};'><a style='color:{4};' href='{0}'>Заказ {1} ({5}, {2}) не обработан {3} час(а, ов)</a><br/><span style='color:grey;'>{6}</span></p>".format(boUrl, orderId,orderCarrier.encode("utf-8"),pastHours, color, shopname.encode("utf-8"), orderProducts)

        else:
            if orderState == u"В процессе подготовки":
                logging.LogInfoMessage(u"Заказ {0} ({1}) в процессе подготовки (часов): {2}".format(str(orderId), orderCarrier, str(pastHours)))
                message += "<p>Заказ <a href='{0}'>{1}</a> ({5}, {2}) в процессе подготовки {3} час(а, ов) [ {4} ]<br/><span style='color:grey;'>{6}</span></p>".format(boUrl, orderId,orderCarrier.encode("utf-8"),pastHours, orderNote.encode("utf-8"), shopname.encode("utf-8"), orderProducts)

    if machineName.find("hp") != -1:
        ssh.kill()


result = mails.SendInfoMail("Заказы \"В процессе подготовки\"", message)
if result != True:
    logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

time.sleep(30)