﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import SMSAPI
import psutil
import commonLibrary

states = [
u"Ждет отправки",
u"Отправлен",
u"Доставлен",
u"Вручен",
]

def SendMailToCustomer(site, orderId, mode, trackingNumber):
    global message

    data = mails.GetSenderData(site["url"])

    if mode == "in_transit":
        data["tracking_number"] = trackingNumber
        if len(trackingNumber) == 10:
            data["followup"] = "https://www.cdek.ru/track.html?order_id="
        if len(trackingNumber) == 14:
            data["followup"] = "https://www.pochta.ru/tracking/#"

    if mode == "shipped":
        if carrier.find(u"Почтой") != -1:
            description = " в Ваше почтовое отделение."
        elif carrier.find(u"Курьер") != -1:
            description = " в Ваш город. В ближайшее время с Вами свяжется курьер для уточнения удобного времени доставки."
        else:
            pointName = carrier[carrier.index("\"") + 1:carrier.rindex("\"")]
            pvzCode = commonLibrary.GetPvzCode(prestashop_api.GetOrderNote(orderId))
            if pvzCode == -1:
                pointDetails = sdek.GetPVZDetails(shopname, pointName)
            else:
                pointDetails = sdek.GetPVZDetails("", pvzCode)
                if pointDetails != -1:
                    pointName = pointDetails[3]
            if pointDetails == -1:
                description = " в пункт выдачи."
            else:
                description = " в пункт выдачи '" + pointName.encode("utf-8") + "': адрес: " + pointDetails[0].encode("utf-8") + \
                    ", телефоны: " + pointDetails[1].encode("utf-8") + ", режим работы: " + pointDetails[2].encode("utf-8")
        data["description"] = description

    shopUrl = commonLibrary.GetSiteUrlScheme(prestashop_api.siteUrl) + prestashop_api.GetOrderShopUrl(orderId)
    data["shop_url"] = shopUrl

    data["firstname"] = customer[0]
    data["lastname"] = customer[1]
    data["email"] = customer[2]
    if test == True:
        data["email"] = "pitekantrop@list.ru"
    data["order_name"] = prestashop_api.GetOrderReference(orderId)

    if data["email"].find("@") == -1:
        return
    result = mails.SendMailToCustomer(mode, data)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма клиенту: " + result.decode("cp1251"))
        message += "[Error] Ошибка при отправке письма клиенту\n"


def SendSMSToCustomer(site, orderId, mode, trackingNumber):
    global message

    if mode not in ["shipped", "reminder", "in_transit"]:
##    if mode not in [ "reminder", "in_transit"]:
        return

    if now.hour < 9 or now.hour > 14:
        return

    smsMessage = u"Здравствуйте, "

    shop_name = u"«{0}»".format(commonLibrary.GetCompanyName(site["url"]))

    customer = prestashop_api.GetOrderCustomer(orderId)
    orderReference = prestashop_api.GetOrderReference(orderId)
    deliveryData = prestashop_api.GetOrderDeliveryData(orderId)

    smsMessage += customer[0] + u"! Ваш заказ"
    if mode == "in_transit":
        smsMessage += u" уже в пути"
    else:
        smsMessage += u" доставлен"

    if carrier.find(u"Почтой") != -1:
        if mode == "in_transit":
            smsMessage += u", трекинг-код для отслеживания посылки " + trackingNumber + "."
        else:
            smsMessage += u" в Ваше почтовое отделение. "
    elif carrier.find(u"Курьер") != -1:
        if mode == "in_transit":
            smsMessage += u", номер накладной " + trackingNumber + "."
        else:
            return
    else:
        pvzCode = commonLibrary.GetPvzCode(prestashop_api.GetOrderNote(orderId))
        pointName = carrier[carrier.index("\"") + 1:carrier.rindex("\"")]
        if pvzCode == -1:
            pointDetails = sdek.GetPVZDetails(shopname, pointName)
        else:
            pointDetails = sdek.GetPVZDetails("", pvzCode)
            if pointDetails != -1:
                pointName = pointDetails[3]
        if mode == "in_transit":
            smsMessage += u", номер накладной " + trackingNumber + u". Пожалуйста, сохраняйте данный номер до получения заказа в пункте выдачи."
        else:
            if pointDetails == -1:
                logging.LogErrorMessage(u"Не найден пункт выдачи '" + pointName + "'")
                message += "[Error] Не найден пункт выдачи {0}, заказ {1}\n".format(pointName.encode("utf-8"), orderId)
                smsMessage += u" в пункт выдачи."
            else:
                smsMessage += u" в пункт выдачи '" + pointName + u"', адрес: " + pointDetails[0] + \
                        u", телефоны: " + pointDetails[1] + u", режим работы: " + pointDetails[2] + u"."

##    if mode in ["shipped"]:
##        smsMessage += u" По всем вопросам, связанным с заказом, обращайтесь по телефону 8(800)707-81-55."

    phone = deliveryData[4]
    if test == True:
##        return
        phone = "79620590262"

    smsMessage += u"Интернет-магазин {0}.".format(shop_name)

    smsMessage = smsMessage.encode("cp1251")

    result = -1
    lastSMSDate = prestashop_api.GetOrderSMSDate(orderId, mode)
    if mode in ["shipped", "in_transit"]:
        if lastSMSDate == -1:
            result = sms.SendSMS(phone, smsMessage)
        else:
            return
    if mode == "reminder":
        if lastSMSDate == -1:
            shippedSMSDate = prestashop_api.GetOrderSMSDate(orderId, "shipped")
            if shippedSMSDate == -1:
                result = sms.SendSMS(phone, smsMessage)
            else:
                pastDays = (datetime.date.today() - shippedSMSDate.date()).days
                if pastDays > 2:
                    result = sms.SendSMS(phone, smsMessage)
                else:
                    return
        else:
            pastDays = (datetime.date.today() - lastSMSDate.date()).days
            if pastDays > 2:
                result = sms.SendSMS(phone, smsMessage)
            else:
                return

    result = result.replace("\n", "")

    if result.find("100") == -1:
        logging.LogErrorMessage(u"Ошибка " + str(result)  + u" при отправке sms по заказу " + str(orderId) )
        message += "[Error] Ошибка {0} при отправке sms клиенту по заказу <a target='blank_' href='{1}'>{2}</a>\n".format(result, boUrl, orderId)
    else:
        if test == False:
            prestashop_api.SetOrderSMSDate(orderId, mode, datetime.date.today())

#############################################################################################



now = datetime.datetime.now()
today = datetime.date.today()

test = False
##test = True

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processOrders")
logging = log.Log(logfile)

machineName = commonLibrary.GetMachineName()
sms = SMSAPI.SMSAPI()
prestashop_api = PrestashopAPI.PrestashopAPI()

sdekIP = SdekAPI.SdekAPI()
sdekNovall = SdekAPI.SdekAPI(False, "b9171b1ca91597383ab3043a218b6883", "de53f4a57568665605a76e21d9f100e1")
sdek = sdekIP

rp = RussianPostAPI.RussianPostAPI()
mails = SendMails.SendMails()
notShippedOrders = {}
notDeliveredCouriersOrders = {}

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=60)

message = ""

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    notShippedOrders[site["url"]] = []
    notDeliveredCouriersOrders[site["url"]] = []
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)
    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]
##    orders = prestashop_api.GetOrdersIds(states, startDate, endDate, True)

    bOrders = False

    for order in orders:
        orderId = order[0]
        print orderId

##        if orderId != 6396:
##            continue

        mode = None

        orderState = prestashop_api.GetOrderState(orderId)
        orderNote = prestashop_api.GetOrderNote(orderId)
        bNovallOrder = commonLibrary.GetNovallOrder(orderNote)
        orderPaid = commonLibrary.GetOrderPaid(orderNote)
        carrier = prestashop_api.GetOrderCarrierName(orderId)
        if carrier == -1:
            continue
        customer = prestashop_api.GetOrderCustomer(orderId)
        shopname =  prestashop_api.GetOrderShopName(orderId)
        if shopname == -1:
            continue
        shopname = commonLibrary.GetShopName(shopname)
        boUrl = "{0}?controller=AdminOrders&id_order={1}&vieworder".format(backofficeUrl, orderId)

        if orderState == u"Вручен" and orderPaid == True:
            if bOrders == False:
                message += ("\n=========  " + site["url"] + "  =========\n\n")
                logging.Log("==================================  " + site["url"] + "  ==================================")
                bOrders = True
            prestashop_api.SetOrderState(orderId, u"Доставлен и оплачен")
            logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Доставлен и оплачен'".format(orderId))
            message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен и оплачен'\n".format(boUrl, orderId)
            continue

        if orderState == u"Вручен":
            if shopname in [u"Спб", u"Санкт-Петербург"] and carrier.find(u"выдачи") == -1 and carrier.find(u"Почтой") == -1:
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    logging.Log("==================================  " + site["url"] + "  ==================================")
                    bOrders = True
                warehouseName = "warehouse_spb"
                prestashop_api.SetOrderState(orderId, u"Доставлен и оплачен")
                logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Доставлен и оплачен'".format(orderId))
                message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен и оплачен'\n".format(boUrl, orderId)

                #уменьшаем количество на складе
                orderItems = prestashop_api.GetOrderItems(orderId)
                for orderItem in orderItems:
                    productOriginalName = prestashop_api.GetProductOriginalName(orderItem[0])
                    cleanProductOriginalName = prestashop_api.CleanItemName(productOriginalName)
                    try:
                        prestashop_api.DecreaseWarehouseProductQuantity(warehouseName, productOriginalName, int(orderItem[3]))
                        productQty = prestashop_api.GetWarehouseProductQuantity(warehouseName, productOriginalName)
                        logging.LogInfoMessage(u"Остаток товара '{0}' на складе {1} - {2} шт.".format(productOriginalName, warehouseName, str(productQty)))
                        message += "Остаток товара '{0}' на складе {1} - {2} шт.\n".format(productOriginalName.encode("utf-8"), warehouseName, str(productQty))
                    except:
                        mails.SendInfoMail("Списание товаров вручную", "'" + productOriginalName.encode("utf-8") + "' - " + str(orderItem[3]) + " шт.\n")
            continue

        trackingNumber = prestashop_api.GetOrderTrackingNumber(orderId)

        if trackingNumber == None or trackingNumber == "":
            continue

        trackingNumber = trackingNumber.strip()

        if len(trackingNumber) == 10:#доставка СДЭКом
            if bNovallOrder == False:
                sdek = sdekIP
            else:
                sdek = sdekNovall

            orderDeliveryState = sdek.GetOrderState(prestashop_api.GetOrderTrackingNumber(orderId), True)
##            orderDeliveryState = sdek.GetOrderState(prestashop_api.GetOrderTrackingNumber(orderId))

            if orderDeliveryState == -1:
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    logging.Log("==================================  " + site["url"] + "  ==================================")
                    bOrders = True
                logging.LogErrorMessage(u"Не удалось определить статус доставки заказа {0}".format(orderId))
                message += "[Error] Не удалось определить статус доставки заказа <a target='blank_' href='{0}'>{1}</a>\n".format(boUrl, orderId)
                continue

            if orderDeliveryState == "order_not_found":
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    logging.Log("==================================  " + site["url"] + "  ==================================")
                    bOrders = True
                logging.LogErrorMessage(u"Не найдена накладная {0} по заказу {1}".format(trackingNumber, orderId))
                message += "[Error] Не найдена накладная {0} по заказу <a target='blank_' href='{1}'>{2}</a>\n".format(trackingNumber, boUrl, orderId)
                continue

            if orderState == u"Ждет отправки":
                if orderDeliveryState in ["in_transit", "shipped"]:
                    if orderDeliveryState == "in_transit":
                        prestashopState = u"Отправлен"
                    else:
                        prestashopState = u"Доставлен"

                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, prestashopState)
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на '{1}'".format(orderId, prestashopState))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на '{2}'\n".format(boUrl, orderId, prestashopState.encode("utf-8"))
                    mode = orderDeliveryState
                    #удаляем данные об отправке из "Служебных записок"
                    orderSender = commonLibrary.GetOrderSender(orderNote)
                    invoiceNumber = commonLibrary.GetInvoiceNumber(orderNote)
                    for text in [orderSender, invoiceNumber]:
                        if text != -1:
                            orderNote = orderNote.replace(text,"")
                            prestashop_api.SetCustomerNote(customer[3], orderNote)

                else:
                    awaitingTransitStateDate = prestashop_api.GetOrderStateDate(orderId, u"Ждет отправки")
                    if today.day != awaitingTransitStateDate.date().day:
                        label = commonLibrary.GetOrderSender(orderNote)
                        notShippedOrders[site["url"]].append("<a target='blank_' href='{3}'>{4}</a> ({1}, <a target='blank_' href='https://www.cdek.ru/track.html?order_id={2}'>{2}</a>)".format(orderId, label, trackingNumber, boUrl, orderId))


            if orderState == u"Отправлен":
                if orderDeliveryState == "shipped":
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, u"Доставлен")
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Доставлен'".format(orderId))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен'\n".format(boUrl, orderId)
                    mode = "shipped"
                else:
                    SendSMSToCustomer(site, orderId, "in_transit", trackingNumber)

            if orderState == u"Доставлен":
                if orderDeliveryState == "delivered":
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, u"Вручен")
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Вручен'".format(orderId))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Вручен'\n".format(boUrl, orderId)
                else:
                    mode = "reminder"
                    shippedDate = prestashop_api.GetOrderStateDate(orderId, u"Доставлен")
                    pastDays = (datetime.date.today() - shippedDate.date()).days

                    if pastDays >= 1 and carrier.find(u"Курьер") != -1:
                        notDeliveredCouriersOrders[site["url"]].append("\n<a target='blank_' href='{2}'>{0}</a> ( https://www.cdek.ru/track.html?order_id={1} )".format(orderId, trackingNumber, boUrl))

                    if pastDays > 3:
                        if bOrders == False:
                            message += ("\n=========  " + site["url"] + "  =========\n\n")
                            logging.Log("==================================  " + site["url"] + "  ==================================")
                            bOrders = True
                        logging.LogInfoMessage(u"Клиент не забирает заказ {0} больше {1} дней".format(orderId, pastDays))
                        message += "ЗАКАЗ <a target='blank_' href='{0}'>{1}</a>, ОТПРАВЛЕННЫЙ СДЭК'ОМ, НЕ ВРУЧЕН (ДНЕЙ): {2}\n".format(boUrl, orderId, pastDays)

                    SendSMSToCustomer(site, orderId, "shipped", trackingNumber)

        if len(trackingNumber) == 14:#доставка почтой России
            ordersStates = rp.GetOrderStates(prestashop_api.GetOrderTrackingNumber(orderId))

            if ordersStates == False:
                if bOrders == False:
                    message += ("\n=========  " + site["url"] + "  =========\n\n")
                    logging.Log("==================================  " + site["url"] + "  ==================================")
                    bOrders = True
                message += "[Error] Не удалось получить статус почтового заказа <a target='blank_' href='{0}'>{1}</a>\n".format(boUrl, orderId)
                logging.LogErrorMessage(u"Не удалось получить статус почтового заказа {0}".format(orderId))
                continue

            if orderState == u"Ждет отправки":
                if len(ordersStates) > 0:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, u"Отправлен")
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Отправлен', трекинг-код {1}".format(orderId, trackingNumber))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Отправлен', трекинг-код {2}\n".format(boUrl, orderId, trackingNumber)
                    mode = "in_transit"

            if orderState == u"Отправлен":
                if u"Прибыло в место вручения" in ordersStates:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, u"Доставлен")
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Доставлен'".format(orderId))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Доставлен'\n".format(boUrl, orderId)
                    mode = "shipped"
                else:
                    SendSMSToCustomer(site, orderId, "in_transit", trackingNumber)

            if orderState == u"Доставлен":
                if u"Вручение адресату" in ordersStates:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, u"Вручен")
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Вручен'".format(orderId))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Вручен'\n".format(boUrl, orderId)

                elif u"Возврат - Истек срок хранения" in ordersStates:
                    if bOrders == False:
                        message += ("\n=========  " + site["url"] + "  =========\n\n")
                        logging.Log("==================================  " + site["url"] + "  ==================================")
                        bOrders = True
                    if test == False:
                        prestashop_api.SetOrderState(orderId, u"Отклонен")
                    mes = u"Статус заказа " + str(orderId) + u" изменен на 'Отклонен'"
                    logging.LogInfoMessage(u"Статус заказа {0} изменен на 'Отклонен'".format(orderId))
                    message += "Статус заказа <a target='blank_' href='{0}'>{1}</a> изменен на 'Отклонен'\n".format(boUrl, orderId)
                else:
                    mode = "reminder"
                    shippedDate = prestashop_api.GetOrderStateDate(orderId, u"Доставлен")

                    pastDays = (datetime.date.today() - shippedDate.date()).days
                    if pastDays > 3:
                        if bOrders == False:
                            message += ("\n=========  " + site["url"] + "  =========\n\n")
                            logging.Log("==================================  " + site["url"] + "  ==================================")
                            bOrders = True
                        logging.LogInfoMessage(u"Клиент не забирает заказ {0} больше {1} дней".format(orderId, pastDays))
                        message += "ЗАКАЗ <a target='blank_' href='{0}'>{1}</a>, ОТПРАВЛЕННЫЙ ПОЧТОЙ, НЕ ВРУЧЕН (ДНЕЙ): {2}\n".format(boUrl, orderId, pastDays)

                    SendSMSToCustomer(site, orderId, "shipped", trackingNumber)

        if mode != None:
            if mode != "reminder":
                SendMailToCustomer(site, orderId, mode, trackingNumber)
            SendSMSToCustomer(site, orderId, mode, trackingNumber)

    if machineName.find("hp") != -1:
        ssh.kill()

if sdek.selenium != None:
    sdek.selenium.CleanUp()

result = mails.SendInfoMail("Отчет изменения статусов заказов", message)
if result != True:
    logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))

notShippedOrdersMessage = ""
notDeliveredCouriersOrdersMessage = ""
now = datetime.datetime.now()

for site in notShippedOrders.keys():
    if len(notShippedOrders[site]) != 0:
        orders = ", ".join(str(orderId) for orderId in notShippedOrders[site])
        notShippedOrdersMessage += "<p>{0}: {1}</p><br/>".format(site, orders)

for site in notDeliveredCouriersOrders.keys():
    if len(notDeliveredCouriersOrders[site]) != 0:
        orders = ", ".join(str(orderId) for orderId in notDeliveredCouriersOrders[site])
        notDeliveredCouriersOrdersMessage += "{0}: {1}\n\n".format(site, orders)

if now.hour == 9:
    if notShippedOrdersMessage != "" and now.weekday() not in [0, 6]:
        mails.SendInfoMail("Неотправленные заказы", notShippedOrdersMessage)

    if notDeliveredCouriersOrdersMessage != "" and now.weekday() not in [5, 6, 0]:
        mails.SendInfoMail("Недоставленные курьерские заказы", notDeliveredCouriersOrdersMessage)

