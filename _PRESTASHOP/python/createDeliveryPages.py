﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import html2text
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import subprocess

pointSource = u"""
<p style="text-align: left;"><span style="text-decoration:underline;">{name}</span></p>
<p>Адрес: {address}</p>
<p>Телефоны: {phones}</p>
<p>Время работы: {hours}</p>
"""

def GetPageSource(shopName):
    pvzNodes = root.xpath("//Pvz[(starts-with(@City, '" + shopName + "') and contains(@City, ',')) or (@City = '" + shopName + "' and not(contains(@City, ',')))]")

    #формирование страницы
    if len(pvzNodes) != 0:
        pageSource_tmp = pageSource
        pointsSource = ""

        for i in range(0, len(pvzNodes)):
            pvzNode = pvzNodes[i]
            pointSource_tmp = pointSource
            pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
            pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
            pointSource_tmp = pointSource_tmp.replace("{phones}", pvzNode.attrib["Phone"])
            pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])

            if i != len(pvzNodes) - 1:
                pointSource_tmp += "<p><br/></p>"

            pointsSource += pointSource_tmp

        pageSource_tmp = pageSource_tmp.replace("{pointsList}", pointsSource)

    else:
        pageSource_tmp = defaultPageSource

    return commonLibrary.GetVariantFromTemplate(shopName, pageSource_tmp)


#########################################################################################################################
machineName = commonLibrary.GetMachineName()
pageSource = open("delivery.txt","r").read().decode("utf-8")
defaultPageSource = open("deliveryDefault.txt","r").read().decode("utf-8")

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue


    resp = requests.get("https://integration.cdek.ru/pvzlist.php")

    pvzXml = etree.XML(resp._content)
    root =  pvzXml.getroottree().getroot()

    logFile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "create_pages")

    logging = log.Log(logFile)
    logging.LogInfoMessage("")

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        shopName = commonLibrary.GetShopName(shopName)

        link_rewrite = "delivery-" + shopName
        link_rewrite = link_rewrite.replace(" ", "-")

        if prestashop_api.IsPageExists(link_rewrite):
            continue

        pageSource_tmp = GetPageSource(shopName)
        if pageSource_tmp == -1:
            continue

        print shopId,shopDomen

        #добавление в БД
        ps_cms_query = """INSERT INTO ps_cms( id_cms_category, position, active, indexation)
                            VALUES (1,0,1,0)"""

        prestashop_api.MakeUpdateQueue(ps_cms_query)
        id_cms = prestashop_api.GetLastInsertedId("ps_cms","id_cms")

        sql = """SELECT id_cms_block FROM ps_cms_block_shop
                WHERE id_shop = %(id_shop)s"""%{"id_shop":shopId}

        try:
            id_cms_block = prestashop_api.MakeGetInfoQueue(sql)[0][0]
        except:
            continue

        ps_cms_block_page_query = """INSERT INTO ps_cms_block_page(id_cms_block_page, id_cms_block, id_cms, is_category)
                                    VALUES (0,%(id_cms_block)s,%(id_cms)s,0)"""%{"id_cms_block":id_cms_block,
                                    "id_cms":id_cms}

        prestashop_api.MakeUpdateQueue(ps_cms_block_page_query)

        ps_cms_lang_query = u"""INSERT INTO ps_cms_lang(id_cms, id_lang, meta_title, meta_description, meta_keywords, content, link_rewrite)
                                VALUES (%(id_cms)s,1,'Доставка','Информация о доставке','Информация о доставке','%(content)s','%(link_rewrite)s')"""%{"id_cms":id_cms,
                 "content":pageSource_tmp, "link_rewrite":link_rewrite}

        prestashop_api.MakeUpdateQueue(ps_cms_lang_query)

        ps_cms_shop_query = """INSERT INTO ps_cms_shop (id_cms ,id_shop)
                                VALUES(%(id_cms)s,%(id_shop)s)"""%{"id_shop":shopId,"id_cms":id_cms}

        prestashop_api.MakeUpdateQueue(ps_cms_shop_query)

        logging.LogInfoMessage(u"Добавлен город: " + shopDomen)

    if machineName.find("hp") != -1:
        ssh.kill()