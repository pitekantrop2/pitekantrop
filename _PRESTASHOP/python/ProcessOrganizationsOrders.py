# -*- coding: utf-8 -*-

import log
import datetime
import sys
import PrestashopAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import commonLibrary
import SrvOnline
import ModulbankAPI

def GetCoefficient(contractor):
    if orderData["Novall"] == False:
        return 1
    if contractor in [u"Атек", u"Велл",u"Поликон", u"Алекс"]:
        return float(1.05)
    elif contractor == u"Чистон":
        return float(1.25)
    else:
        return float(1.2)

def GetOrderSum():
    rows = note.split("\n")
    for row in rows:
        if row.find(u"orderSum") != -1:
            try:
                return int(float( row.replace(u"orderSum", "").strip()))
            except:
                return "Некорректное значение orderSum в заказе {}\n\n".format(orderId)

    return -1

def GetContractorData(note, bCheck = True):
    global message

    contractorData = {}

    rows = note.split("\n")
    for row in rows:
        if row.find(u"ИНН") != -1:
            contractorData["INN"] = row.replace(u"ИНН", "").replace(" ","").strip()
        if row.find(u"КПП") != -1:
            contractorData["KPP"] = row.replace(u"КПП", "").replace(" ","").strip()
        if row.find(u"БИК") != -1:
            contractorData["BIK"] = row.replace(u"БИК", "").replace(" ","").strip()
        if row.find(u"ОГРН") != -1:
            contractorData["OGRN"] = row.replace(u"ОГРН", "").replace(" ","").strip()
        if row.find(u"РС") != -1:
            contractorData["RS"] = row.replace(u"РС", "").replace(" ","").strip()

    if bCheck == False:
        return contractorData

    data = []
    if "INN" not in contractorData.keys() and "OGRN" not in contractorData.keys():
        data.append(u"ИНН")
        data.append(u"ОГРН")
##    if "BIK" not in contractorData.keys():
##        data.append(u"БИК")
##    if "RS" not in contractorData.keys():
##        data.append(u"РС")

    if len(data) != 0:
        message += "[Error] Недостаточно данных для создания счета по заказу {0}: {1}\n".format(orderId, ", ".join(data).encode("utf-8"))
        logging.LogErrorMessage(u"Недостаточно данных для создания счета по заказу {0}: {1}".format(orderId, ", ".join(data)))
        return -1

    return contractorData


def GetOrderDeliveryDescription(orderId):
    carrierName = prestashop_api.GetOrderCarrierName(orderId)
    if carrierName.find(u"Курьер") != -1:
        city = prestashop_api.GetOrderDeliveryData(orderId)[2]
        if city[0].isdigit() == True:
            city =  prestashop_api.GetCityBySdekCode(city)
            if city == -1:
                return -1
        return u"курьером до двери в г. {0}".format(city)
    elif carrierName.find(u"выдачи") != -1:
        shopname =  prestashop_api.GetOrderShopName(orderId)
        shopname = commonLibrary.GetShopName(shopname)
        return u"на пункт выдачи в г. {0}".format(shopname)
    else:
        return u"почтой России."

def GetInvoiceNumber(note):
    try:
        return [x.replace(u"№", "") for x in note.split(" ") if x.startswith(u"№")][0]
    except:
        return -1


def GetOrderPaid(note):
    if operationsHistoryNovall == -1 or operationsHistory == -1:
        return False

    global sendLetter
    global message

    invoiceNumber = GetInvoiceNumber(note)
    if invoiceNumber == -1:
        return False

##    orderCost = prestashop_api.GetOrderTotalCost(orderId)
    orderCost = GetOrderSum()
    if type( orderCost) != type(1):
        sendLetter = True
        message += "[Error] {}".format(orderCost)
        return False
    if orderCost == -1:
        orderCost = prestashop_api.GetOrderTotalCost(orderId)
    contractorData = GetContractorData(note, False)
    if "INN" not in contractorData.keys():
        sendLetter = True
        return -1

    if commonLibrary.GetNovallOrder(note) == True:
        operationsHistory_ = operationsHistoryNovall
    else:
        operationsHistory_ = operationsHistory

    for operation in operationsHistory_:
        if operation["contragentInn"] == contractorData["INN"]:
            operationAmount = int(operation["amount"])
            if int(operationAmount) == int(orderCost):
                sendLetter = True
                return operation["paymentPurpose"]
            else:
                bOtherOrder = False
                customersOrders = prestashop_api.GetCustomerOrdersIds(customerId)
                if len(customersOrders) > 1:
                    for customerOrderId in customersOrders:
                        orderCosts = prestashop_api.GetOrderTotalCost(customerOrderId)
                        if operationAmount == int(orderCosts):
                            bOtherOrder = True
                            sOrders = ",".join([str(x) for x in customersOrders])
                            message += "У контрагента {0} несколько заказов: {1}\n".format(int(contractorData["INN"]), sOrders)
                            break

                operationDate = [int(x) for x in operation["created"].split("T")[0].split("-")]
                operationDate = datetime.date(operationDate[0],operationDate[1], operationDate[2])
                todayDate = datetime.date.today()
                delta = (todayDate - operationDate).days
                if bOtherOrder == False and delta < 2:
                    sendLetter = True
                    message += "[Error] Стоимость заказа {0} {1} руб. отличается от суммы {2} руб., оплаченной по счету №{3}\n" \
                    .format(orderId, int(orderCost), operationAmount, invoiceNumber)
                    logging.LogErrorMessage(u"Стоимость заказа {0} {1} руб. отличается от суммы {2} руб., оплаченной по счету №{3}"
                    .format(orderId, int(orderCost), operationAmount, invoiceNumber))

    return False

################################################################################

states = [
u"В ожидании оплаты банком"]

prestashop_api = PrestashopAPI.PrestashopAPI()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "createDocumentsForOrganizations")
logging = log.Log(logfile)
mails = SendMails.SendMails()
srvOnline = SrvOnline.SrvOnline()
mb = ModulbankAPI.ModulbankAPI()
mbNovall = ModulbankAPI.ModulbankAPI(True)
##sdek = SdekAPI.SdekAPI()
machineName = commonLibrary.GetMachineName()

##q = mb.GetAccountInfo()

operationsHistory = mb.GetOperationsHistory((datetime.date.today() - datetime.timedelta(days=15)).strftime('%Y-%m-%d'), "Debet")
operationsHistoryNovall = mbNovall.GetOperationsHistory((datetime.date.today() - datetime.timedelta(days=15)).strftime('%Y-%m-%d'), "Debet")

message = ""

if operationsHistoryNovall == -1 or operationsHistory == -1:
    message = "[Error] Ошибка сервера Модульбанка\n"
sendLetter = False

todayData = datetime.date.today().strftime('%d.%m.%Y')
endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=190)

for site in commonLibrary.sitesParams:
    prestashop_api = commonLibrary.CreatePrestashopSession(site["url"])
    if prestashop_api == -1:
        continue

    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)
    bOrders = False

    for order in orders:
        orderData = {}
        orderId = order[0]
##        if orderId != 8478:
##            continue
        note = prestashop_api.GetOrderNote(orderId)
        if commonLibrary.GetOrderLocked(note):
            continue

        orderData["Novall"] = commonLibrary.GetNovallOrder(note)
        customerId = prestashop_api.GetOrderCustomer(orderId)[3]
        postageIncluded = note.find("postageIncluded") >= 0
        orderDeliveryCost = int(prestashop_api.GetOrderDeliveryCost(orderId))
        if orderData["Novall"] == True:
            orderDeliveryCost = int(float(orderDeliveryCost) * 1.2)

        ################ создание счета ################
        if note.find("createInvoice") != -1:
            prestashop_api.AddToOrderNote(orderId, " locked")

            sendLetter = True

            contractorData = GetContractorData(note)
            if contractorData == -1:
                continue

            if bOrders == False:
                message += "\n=========  " + site["url"] + "  =========\n\n"
                logging.Log("=========  " + site["url"] + "  =========")
                bOrders = True

            orderData["date"] = todayData
            orderData["orderDeliveryCost"] = orderDeliveryCost
            orderData["postageIncluded"] = postageIncluded
            postagePerItem = 0
            orderItems = prestashop_api.GetOrderItems(orderId)
            if orderData["postageIncluded"] == True:
                itemsAmount = sum(x[3] for x in orderItems)
                postagePerItem = int(orderData["orderDeliveryCost"] / itemsAmount)
                orderDeliveryCost = postagePerItem * itemsAmount
##                if orderData["Novall"] == False:
##                    prestashop_api.SetOrderDeliveryCost(orderId, orderDeliveryCost)

            orderData["orderDeliveryDescription"] = GetOrderDeliveryDescription(orderId)
            if orderData["orderDeliveryDescription"] == -1:
                message += "[Error] Ошибка создания счета по заказу {0}: не найден город с указанным кодом\n".format(orderId)
                logging.LogErrorMessage(u"Ошибка создания счета по заказу {0}: не найден город с указанным кодом".format(orderId))
                prestashop_api.DeleteFromOrderNote(orderId, " locked")
                continue
            orderData["orderItems"] = []

            orderDiscount = commonLibrary.GetDiscount(note)
            if postagePerItem == 0:
                orderSum = orderDeliveryCost
            else:
                orderSum = 0

            for orderItem in orderItems:
                item = {}
                itemId = orderItem[0]
                item["Name"] = orderItem[1]
                if orderDiscount != 0:
                    price = prestashop_api.GetProductDetailsById(itemId)[1]
                else:
                    price = orderItem[2]
                itemContractor = commonLibrary.GetContractorName(prestashop_api.GetProductArticle(itemId))
                coefficient = GetCoefficient(itemContractor)
                itemPrice = round(float(price) * float(1 - orderDiscount/100) * coefficient, 2) + postagePerItem
                orderSum += itemPrice * orderItem[3]
                item["Price"] = str(itemPrice)
                item["Amount"] = str(orderItem[3])
                orderData["orderItems"].append(item)

            srvOnline.Login()
            res = srvOnline.CreateInvoice(orderData, contractorData)
            if res in [-1, -2]:
                error = ""
                if res == -2:
                    error = ": не найден БИК"
                message += "[Error] Ошибка создания счета по заказу {0}{1}\n".format(orderId, error)
                logging.LogErrorMessage(u"Ошибка создания счета по заказу {0}".format(orderId))
                prestashop_api.DeleteFromOrderNote(orderId, " locked")
                continue
            else:
                invoiceNumber = res[0]
                docNumber = res[1]
                href = "https://srv-online.ru/desktop/docs/Счет%20№%20{0}%20от%20{1}.pdf?mod=docs.print&prm[]=2013&q[action]=preview&q[docs]=2-{2}&q[stamp]=1".format(invoiceNumber, todayData, docNumber)
                message += "<p>Выставлен <a href='{3}'>счет №{0} от {1}</a> по заказу {2}</p>".format(invoiceNumber, todayData, orderId,href)
                logging.Log(u"Выставлен счет №{0} от {1} по заказу {2}".format(invoiceNumber, todayData, orderId))
                note = note.replace("\ncreateInvoice", "")
                note += u"\nВыставлен счет №{0} от {1}".format(invoiceNumber, orderData["date"])
                note += u"\norderSum{0}".format(str(int(orderSum)))
                note = note.replace( " locked", "")
                prestashop_api.SetCustomerNote(customerId, note)


        ################ создание договора ###################
        if note.find("createContract") != -1:
            sendLetter = True

            prestashop_api.AddToOrderNote(orderId, " locked")

            if postageIncluded == False and orderDeliveryCost == 0:
                orderDeliveryDescription = ""
            else:
                orderDeliveryDescription = GetOrderDeliveryDescription(orderId)

            if orderDeliveryDescription == -1:
                message += "[Error] Ошибка создания договора по заказу {0}: не найден город с указанным кодом\n".format(orderId)
                logging.LogErrorMessage(u"Ошибка создания договора по заказу {0}: не найден город с указанным кодом".format(orderId))
                prestashop_api.DeleteFromOrderNote(orderId, " locked")
                continue

            if 'invoiceNumber' not in locals():
                invoiceNumber = GetInvoiceNumber(note)

            if invoiceNumber == -1:
                prestashop_api.DeleteFromOrderNote(orderId, " locked")
                continue

            if bOrders == False:
                message += "\n=========  " + site["url"] + "  =========\n\n"
                logging.Log("=========  " + site["url"] + "  =========")
                bOrders = True

            srvOnline.Login()
            contractNumber = srvOnline.CreateContract(invoiceNumber, orderDeliveryDescription, commonLibrary.GetNovallOrder(note))
            if contractNumber == -1:
                message += "[Error] Ошибка создания договора по заказу {0}\n".format(orderId)
                logging.LogErrorMessage(u"Ошибка создания договора по заказу {0}".format(orderId))
                prestashop_api.DeleteFromOrderNote(orderId, " locked")
                continue
            else:
                message += "Создан договор №{0} от {1} по заказу {2}\n".format(contractNumber, todayData, orderId)
                logging.Log(u"Создан договор №{0} от {1} по заказу {2}".format(contractNumber, todayData, orderId))
                note = note.replace("\ncreateContract", "")
                note += u"\nСоздан договор {0} от {1}".format(contractNumber, todayData)
                prestashop_api.SetCustomerNote(customerId, note)
                prestashop_api.DeleteFromOrderNote(orderId, " locked")

        ################ создание товарной накладной ################
        orderPaid = GetOrderPaid(note)
        print orderId
        if orderPaid not in [-1, False]:
            if bOrders == False:
                message += "\n=========  " + site["url"] + "  =========\n\n"
                logging.Log("=========  " + site["url"] + "  =========")
                bOrders = True

            nds = ""
            if orderData["Novall"] == False:
                entries = [u"т.ч.", u"20%", u"в том числе", u"20.00%"]
                for entry in entries:
                    if orderPaid.lower().find(entry) != -1:
                        nds = u"!!!ВОЗМОЖНО, ВЫДЕЛЕН НДС!!!\n"

            message += """Заказ {0} оплачен ("{1}")\n{2}""".format(orderId, orderPaid.encode("utf-8"), nds.encode("utf-8"))
            logging.Log(u"""Заказ {0} оплачен ("{1}")""".format(orderId, orderPaid))
            prestashop_api.SetOrderState(orderId, u"В процессе подготовки")
            note += u"\nоплачен"
            if orderDeliveryCost == 0 or orderData["Novall"] == True:
                note += u"\ndonotsend"
            prestashop_api.SetCustomerNote(customerId, note)
            email = prestashop_api.GetCustomerData(customerId)[2]
            email = email.strip("_")
            prestashop_api.SetCustomerEmail(customerId,email)

            carrierName = prestashop_api.GetOrderCarrierName(orderId)
            if carrierName.find(u"Курьер") != -1:
                deliveryData = prestashop_api.GetOrderDeliveryData(orderId)
                if deliveryData[2][0].isdigit() == False:
                    sdekCityCode = prestashop_api.GetSdekCityCode(deliveryData[2])
                    if len(sdekCityCode) != 1:
                        message += "НУЖНО ВРУЧНУЮ УСТАНОВИТЬ КОД ГОРОДА\n"
                    else:
                        prestashop_api.SetOrderAddressCity(orderId, sdekCityCode[0])

            if commonLibrary.GetNovallOrder(note) == False:
                invoiceNumber = GetInvoiceNumber(note)

                srvOnline.Login()
                result = srvOnline.CreatePackingList(invoiceNumber, orderData["Novall"])
                if result == -1:
                    message += "[Error] Ошибка создания товарной накладной по заказу {0}\n".format(orderId)
                    logging.LogErrorMessage(u"Ошибка создания товарной накладной по заказу {0}".format(orderId))
                    continue
                else:
                    message += "Создана товарная накладная по заказу {0}\n".format(orderId)
                    logging.Log(u"Создана товарная накладная по заказу {0}".format(orderId))
        elif orderPaid == -1:
            message += "[Error] Недостаточно данных для проверки оплаты по заказу {0}\n".format(orderId)
            logging.LogErrorMessage(u"Недостаточно данных для проверки оплаты по заказу {0}".format(orderId))
            continue
        else:
            pass

    if srvOnline.selenium != None:
        srvOnline.CloseSelenium()


if sendLetter != False:
    result = mails.SendInfoMail("Обработка заказов юрлиц", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
