﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import SendMails
import subprocess
import random

##seoBlock = u"""
##<p><a href="_href_">Купить {name} в {city}</a> можно в нашем интернет-магазине - например, с доставкой на пункт выдачи:</p>
##{points}"""
seoBlock = u"""
<p>Купить {name} в {city} можно в нашем интернет-магазине - например, с доставкой на пункт выдачи:</p>
{points}"""

pointSource = u"""
<p><span style="text-decoration:underline;">{name}</span> - г. {city}, {address}, время работы: {hours}</p>
"""

def GetSeoBlock(shopName):
    pvzNodes = root.xpath("//Pvz[@City = '" + shopName + "']")
    random.shuffle(pvzNodes)
    city = commonLibrary.GetCity(shopName)
##    tmpSeoBlock = seoBlock.replace("{name}", downcase(name)).replace("{city}", city)
    tmpSeoBlock = "{points}"

    #формирование страницы
    if len(pvzNodes) == 0:
        return tmpSeoBlock.replace("{points}", "")

    pointsSource = ""

    if len(pvzNodes) < 6:
        pointsNum = len(pvzNodes)
    else:
        pointsNum = 5

    for i in range(0, pointsNum):
        pvzNode = pvzNodes[i]
        pointSource_tmp = pointSource
        pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
        pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
        pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])
        pointSource_tmp = pointSource_tmp.replace("{city}", shopName)

        pointsSource += pointSource_tmp

    tmpSeoBlock = tmpSeoBlock.replace("{points}", pointsSource)

    return tmpSeoBlock

def GetSeoBlockTmp(name, shopName):
    pvzNodes = root.xpath("//Pvz[ @City = '" + shopName + "']")
    random.shuffle(pvzNodes)
    city = commonLibrary.GetCity(shopName)
    tmpSeoBlock = seoBlock.replace("{name}", downcase(name)).replace("{city}", city)
##    tmpSeoBlock = "{points}"

    #формирование страницы
    if len(pvzNodes) == 0:
        return tmpSeoBlock.replace("{points}", "")

    pointsSource = ""

    if len(pvzNodes) < 6:
        pointsNum = len(pvzNodes)
    else:
        pointsNum = 5

    for i in range(0, pointsNum):
        pvzNode = pvzNodes[i]
        pointSource_tmp = pointSource
        pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
        pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
        pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])
        pointSource_tmp = pointSource_tmp.replace("{city}", shopName)

        pointsSource += pointSource_tmp

    tmpSeoBlock = tmpSeoBlock.replace("{points}", pointsSource)

    return tmpSeoBlock

def UpdateCategoriesParams(shopId):
    shopCategories = prestashop_api.GetShopCategories(shopId)

    for shopCategory in shopCategories:
        categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory

        if categoryID in [1,2]:
            continue

        if categoryID not in [99]:
            continue

        if description.find(city) != -1:
            if prestashop_api.siteUrl in ["http://tomsk.otpugivatel.com",
                                            "http://otpugiwatel.com",
                                            "http://otpugivateli-grizunov.ru",
                                            "http://otpugivateli-ptic.ru",
                                            "http://otpugivateli-krotov.ru",
                                            "http://otpugivateli-sobak.ru",
                                            "http://insect-killers.ru"
                                            ]:
                ind = description.find(u"<p>Эффективные")
            else:
                ind = description.find(u"<p>Качественные")
            if ind != -1:
                description = description[0:ind]
        if prestashop_api.siteUrl in ["http://tomsk.otpugivatel.com",
                                        "http://otpugiwatel.com",
                                        "http://otpugivateli-grizunov.ru",
                                        "http://otpugivateli-ptic.ru",
                                        "http://otpugivateli-krotov.ru",
                                        "http://otpugivateli-sobak.ru",
                                        "http://insect-killers.ru"
                                        ]:
            descseoBlock = u"""<p>Эффективные и недорогие %(name)s в %(city)s Вы можете купить, оформив заказ в нашем интернет-магазине.</p>"""%{"name":downcase(name),
            "city":city}
        else:
            descseoBlock = u"""<p>Качественные и недорогие %(name)s в %(city)s Вы можете купить, оформив заказ в нашем интернет-магазине.</p>"""%{"name":downcase(name),
            "city":city}

        description += descseoBlock

####        ###########################
####        if long_description != "":
####            ldElement = etree.HTML(long_description)
####            elements = ldElement.xpath(".//p[contains(., '" + u"например, с доставкой на пункт выдачи"  + "') or contains(., '" + u"время работы"  + "')]")
####
####            for element in elements:
####                element.getparent().remove(element)
####
####            long_description = commonLibrary.GetStringDescription(ldElement)
####        else:
####            continue
####
####        ###########################
####
######            if long_description.find(city) == -1:
####        seoblock = GetSeoBlockTmp(name, shopName)
####        long_description += seoblock
####            long_description = long_description.replace("_href_", href)
##
##        description = description.replace("'","")
##        description = description.replace(u"\u2014"," ")
##        meta_keywords = u", ".join([u"{0} купить в {1}".format(downcase(name), city), u"{0} отзывы".format(downcase(name)), u"{0} цены".format(downcase(name))])
##        print link_rewrite
##        if prestashop_api.siteUrl in ["http://safetus.ru", "http://amazing-things.ru"]:
##            meta_description = u"Качественные и недорогие " + downcase(name) + u" в " + city + u"."
##            if prestashop_api.siteUrl == "http://safetus.ru":
##                meta_description += u" В наличии мини камеры, глушилки сотовых, видеоглазки, детекторы \"жучков\" и другие полезные товары."
##        elif prestashop_api.siteUrl in ["http://otpugiwatel.com"]:
##            meta_description = u"Эффективные и недорогие " + downcase(name) + u" купить в " + city + u". В наличии средства от крыс, мышей, кротов, птиц, кошек, насекомых и других вредителей."
##        else:
##            meta_description = name + u" купить в " + city + u" по низкой цене."
##            if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
##                meta_description += u" Большой выбор эффективных ультразвуковых средств от вредителей в " + city + "."
##            if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
##                meta_description += u" Интернет-магазин Новалл - знать всё!"
##            if prestashop_api.siteUrl == "http://saltlamp.su":
##                meta_description += u" Соляные (солевые) лампы, солевые (соляные) светильники в " + city + "."
##            if prestashop_api.siteUrl == "http://sushilki.ru.com":
##                meta_description += u" Электросушилки для рыбы, грибов, ягод и других продуктов в " + city + "."
##            if prestashop_api.siteUrl == "http://glushilki.ru.com":
##                meta_description += u" Эффективные подавители сотовой связи и глонасс, глушилки gsm и gps в " + city + "."
##            if prestashop_api.siteUrl == "http://insect-killers.ru":
##                meta_description += u" Средства от комаров, мошек, мух, ловушки и уничтожители насекомых в " + city + "."
##            if prestashop_api.siteUrl == "http://mini-camera.ru.com":
##                meta_description += u" Устройства для скрытого видеонаблюдения в " + city + "."
##            if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
##                meta_description += u" Интернет-магазин «Отпугиватели грызунов» - избавьтесь от крыс и мышей!"
##            if prestashop_api.siteUrl == "http://otpugivateli-krotov.ru":
##                meta_description += u" Интернет-магазин «Отпугиватели кротов» - средства от кротов, медведок и змей в " + city + "."
##            if prestashop_api.siteUrl == "http://otpugivateli-ptic.ru":
##                meta_description += u" Отпугиватели и другие эффективные средства от птиц в " + city + "."
##            if prestashop_api.siteUrl == "http://otpugivateli-sobak.ru":
##                meta_description += u" Интернет-магазин «Отпугиватели собак» - Когда собаки под контролем"

##        print meta_title
##        print meta_description
##        print description
##        print long_description

        params = [link_rewrite, meta_title, description, long_description, meta_keywords, meta_description ]
        prestashop_api.UpdateCategoryData(shopId, categoryID, params)




#############################################################################################################

downcase = lambda s: s[:1].lower() + s[1:] if s else ''
prestashop_api = PrestashopAPI.PrestashopAPI()

resp = requests.get("https://integration.cdek.ru/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()
machineName = commonLibrary.GetMachineName()

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "updateCategories")
logging = log.Log(logfile)
mails = SendMails.SendMails()



for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    shops = prestashop_api.GetAllShops()
    categoryId = 143

    for shop in shops:
        shopId, shopDomen, shopName = shop

        logging.Log(str(shopId))

        print shopName
        print shopId

        shopName = commonLibrary.GetShopName(shopName)
        city =  commonLibrary.GetCity(shopName)
        if city == -1:
            continue

        ConfigureNewCategory()
##        UpdateCategoriesParams(shopId)

    if machineName.find("hp") != -1:
        ssh.kill()


##mails.SendInfoMail("Обновление категорий", "", [logfile])
