﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary
import DostavkaGuruAPI

states = [
u"В процессе подготовки"
]

riskyItems = [
u"ГРАД А-500",
u"Подавитель Скорпион GSM+GPS",
u"Торнадо - 115"
]

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processOrders")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=45)

now = datetime.datetime.now()

message = ""

warehouseItems = {}
ordersToSend = {}

warehouseProducts = []

date = datetime.date.today() - datetime.timedelta(days=3)
deliveries = commonLibrary.GetDeliveriesByDate(date)
paidContractors = [x[1] for x in commonLibrary.GetRequests() if x[4] == 1]

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    if len(warehouseProducts) == 0:
        warehouseProducts = prestashop_api.GetWarehouseProducts("warehouse_moscow")
        for warehouseProduct in warehouseProducts:
            warehouseItems[warehouseProduct[1]] = warehouseProduct[2]

    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    ordersAndFIOs = {}

    for order in orders:
        orderId = order[0]

##        if orderId != 3754:
##            continue
##        print orderId

        shopname =  prestashop_api.GetOrderShopName(orderId)
        if shopname in [u"Спб", u"Санкт-Петербург"]:
            continue

        orderState = prestashop_api.GetOrderState(orderId)
        note = prestashop_api.GetOrderNote(orderId)
        isOrderAgreed = commonLibrary.GetOrderIsAgreed(note)
        customer = prestashop_api.GetOrderCustomer(orderId)
        customerFIO = customer[0].lower() + " " + customer[1].lower()
        customerFIO2 = customer[1].lower() + " " + customer[0].lower()
        customerId = customer[3]
        customerEmail = customer[2]
        carrierName = prestashop_api.GetOrderCarrierName(orderId)
        if carrierName == -1:
            continue
        deliveryCost = prestashop_api.GetOrderDeliveryCost(orderId)
        itemsCost = prestashop_api.GetOrderTotalCost(orderId) - deliveryCost

        if isOrderAgreed == False:
            continue

        if deliveryCost > 450 and itemsCost < 1500:
            continue

        if customerFIO in ordersAndFIOs.keys() :
            ordersToSend[prestashop_api.siteUrl].remove([ordersAndFIOs[customerFIO],"SDEKMoscow"])
            continue

        if customerFIO2 in ordersAndFIOs.keys() :
            ordersToSend[prestashop_api.siteUrl].remove([ordersAndFIOs[customerFIO2],"SDEKMoscow"])
            continue

        if carrierName.find(u"пункт") == -1 and carrierName.find(u"рьером до двери") == -1:
            continue

        if carrierName.find(u"рьером до двери") != -1:
            if isOrderAgreed == False:
                continue

        if customerEmail.find("@") == -1 and isOrderAgreed == False:
            continue

        if note.lower().find("donotsend") != -1:
            continue

        orderItems = prestashop_api.GetOrderItems(orderId)
        orderProducts = []

        bSingleItemsContractor = True
        contractors = []

        for orderItem in orderItems:
            productId = orderItem[0]
            productArticle = prestashop_api.GetProductArticle(productId)
            contractor = commonLibrary.GetContractorLabel(commonLibrary.GetContractorName(productArticle))
            contractors.append( contractor)
            if len(contractors) > 1:
                if contractors[-2] != contractor:
                    bSingleItemsContractor = False
            productName = orderItem[1]
            productOriginalName = prestashop_api.GetProductOriginalName(productId)
            if productOriginalName != "":
                productName = productOriginalName

            product_quantity = orderItem[3]

            productName = commonLibrary.CleanItemName(productName)

            if productName in riskyItems and isOrderAgreed == False:
                continue

##            if orderItem[1].lower().find(u"шипы") != -1 or orderItem[1].lower().find(u"мунд") != -1:
##                continue

            if productName in warehouseItems.keys():
                if warehouseItems[productName] < product_quantity:
                    continue
                else:
                    warehouseItems[productName] -= product_quantity
            else:
                continue

            itemTuple = [productName, product_quantity]
            orderProducts.append(itemTuple)

        contractorLabel = ""
        if len(orderProducts) == len(orderItems):#проверка достаточности товаров на складе
            contractorLabel = "SDEKMoscow"
##            continue
        else:
            if bSingleItemsContractor == True and contractors[0] in paidContractors:
                contractorLabel = contractors[0]

        if contractorLabel not in ["",  "Chiston", "Bereg", "Spectr", "SititekMoscow"]:
            if contractorLabel == "31Vek" and now.hour >= 11:
                continue
            if prestashop_api.siteUrl not in ordersToSend.keys():
                ordersToSend[prestashop_api.siteUrl] = []
            ordersToSend[prestashop_api.siteUrl].append([orderId,contractorLabel])
            ordersAndFIOs[customerFIO] = orderId

    if machineName.find("hp") != -1:
        ssh.kill()

values = ordersToSend.values()
allLabels = []
for i in range (0, len(values)):
    for j in range(0, len(values[i])):
        allLabels.append(values[i][j][1])

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    if prestashop_api.siteUrl in ordersToSend.keys():
        for order in ordersToSend[prestashop_api.siteUrl]:
            orderId = order[0]
            contractor = order[1]
            customerId = prestashop_api.GetOrderCustomer(orderId)[3]
            note = prestashop_api.GetOrderNote(orderId)
            if commonLibrary.GetOrderSender(note) == -1:
                note += "\n{0}".format(contractor)
                if contractor != "SDEKMoscow":
                    contractorDeliveriesNumber = len([x[0] for x in deliveries if x[2] == contractor and x[6] in [None, ""]])
                    contractorOrdersNumber = contractorDeliveriesNumber + allLabels.count(contractor)
                    if contractorOrdersNumber == 1 and contractor not in ["Chiston", "Bereg", "Ust"]:
                        note += "\nsingleorder"
                prestashop_api.SetCustomerNote(customerId, note)

    if machineName.find("hp") != -1:
        ssh.kill()