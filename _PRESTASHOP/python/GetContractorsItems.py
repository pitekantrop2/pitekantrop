﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary

##prestashop_api = PrestashopAPI.PrestashopAPI()

def GetAllItems(city):
    prestashop_api = PrestashopAPI.PrestashopAPI()

    if city == u"Москва":
        sql = u"""SELECT items FROM deliveries
                 WHERE recipientCity = '{0}'""".format(u"Москва")
    else:
        sql = u"""SELECT items FROM deliveries
                 WHERE recipientCity <> '{0}'""".format(u"Москва")

    data = prestashop_api.MakeLocalDbGetInfoQueue(sql)
    if len(data) == 0:
        return ""

    return "/".join( [x[0] for x in data])

def GetExpectedProductQty(city, name, quantity):
    global mskAllItems
    global spbAllItems

    expectedQty = 0

    if city == u"Москва":
        allItems = mskAllItems
    else:
        allItems = spbAllItems


    if allItems == "":
        return 0

    itemsNames, itemsQuantities = commonLibrary.ParseItems(allItems)
    allItems = u""
    for i in range(0, len(itemsNames)):
        itemName = itemsNames[i]
        itemQty = int(itemsQuantities[i])
        if itemName == name:
            expectedQty = itemQty
            if itemQty >= quantity:
                itemsQuantities[i] = itemQty - quantity
            else:
                itemsQuantities[i] = 0

        allItems += u"{0}_{1}/".format(itemsNames[i], itemsQuantities[i])

    allItems = allItems.strip("/")
    if city == u"Москва":
        mskAllItems = allItems
    else:
        spbAllItems = allItems

    return expectedQty

################################################################################



states = [
u"В процессе подготовки", u"Данного товара нет на складе"]

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "contractorsItems")
logging = log.Log(logfile)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

message = ""

cities = [u"Москва", u"Спб"]

items = []
contractorsItems = {}

mskAllItems = GetAllItems(u"Москва")
spbAllItems = GetAllItems(u"Спб")

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=25)

for site in commonLibrary.sitesParams:

    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue


    message += ("<br/><p>=========  " + site["url"] + "  =========<p>")
    logging.Log("=========  " + site["url"] + "  =========")
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)
    backofficeUrl = commonLibrary.backofficeUrls[site["url"]]

    for order in orders:
        orderId = order[0]

        orderItems = prestashop_api.GetOrderItems(orderId)
        orderShop = prestashop_api.GetOrderShopName(orderId)
        orderNote = prestashop_api.GetOrderNote(orderId)
        orderCarrier = prestashop_api.GetOrderCarrierName(orderId).lower()
        bIsAgreed = commonLibrary.GetOrderIsAgreed(orderNote)
        if orderCarrier.find("\"") != -1:
            orderCarrier = orderCarrier.split("\"")[0].strip()
        else:
            orderCarrier = orderCarrier.split(".")[0].strip()

        if orderShop in [u"Санкт-Петербург", u"Спб"] and orderCarrier.find(u"пункт") == -1:
            warehouse = "warehouse_spb"
            city = u"Спб"
        else:
            warehouse = "warehouse_moscow"
            city = u"Москва"

        boUrl = "{0}?controller=AdminOrders&id_order={1}&vieworder".format(backofficeUrl, orderId)

        mes = u"Заказ " + str(orderId)
        logging.LogInfoMessage(mes)
        status = ""
        if bIsAgreed == False:
            status = " - НЕ СОГЛАСОВАН"

        bMessageOrderInfo = False

        for orderItem in orderItems:
            product_quantity = orderItem[3]
            productId = orderItem[0]
            productDetails = prestashop_api.GetShopProductDetailsById(productId, 10)
            productName = prestashop_api.GetProductOriginalName(productId)
            if productName == "":
                productName = orderItem[1]

            productName = commonLibrary.CleanItemName(productName)

            warehouseProductQuantity = prestashop_api.GetWarehouseProductQuantity(warehouse, productName)

            if warehouseProductQuantity >= product_quantity:
                continue

            deficitProductQty = product_quantity - warehouseProductQuantity

            productArticle = prestashop_api.GetProductArticle(productId)
            contractor = commonLibrary.GetContractorName(productArticle)
            expectedProductQty = GetExpectedProductQty(city, productName, deficitProductQty)
            if expectedProductQty >= deficitProductQty:
                continue

            if contractor == -1:
                contractor = u"Без артикула"

            itemIndex = -1

            try:
                itemIndex = [x[0] for x in contractorsItems[contractor]].index(productName)
            except:
                pass

            deficitProductQty = deficitProductQty - expectedProductQty

            if bIsAgreed == True:
                if contractorsItems.has_key(contractor) == False:
                    contractorsItems[contractor] = []
                if itemIndex != -1:
                    contractorsItems[contractor][itemIndex][1] += deficitProductQty
                else:
                    itemTuple = [productName, deficitProductQty, productDetails[2], city]
                    contractorsItems[contractor].append(itemTuple)

            if bMessageOrderInfo == False:
                message += "<p>Заказ <a href='{0}'>{1}</a> ({2}){3}<br/>".format(boUrl, orderId,orderCarrier.lower().encode("utf-8"),status)
                bMessageOrderInfo = True

            logging.LogInfoMessage(productName + ", " + str(product_quantity) + u" шт. - " + orderShop)
            message += "<span style='color:grey;'>{0}, {1} шт. - {2}<br/>".format(productName.encode("utf-8"), str(product_quantity), orderShop.encode("utf-8"))

        message += "</p>"

    if machineName.find("hp") != -1:
        ssh.kill()


message += ("====================================================================<br/>")
logging.Log("====================================================================")

for contractorName in contractorsItems.keys():
    message += "<span style='font-weight:900'> ********************** {0} ".format(contractorName.encode("utf-8")) + " **********************</span>"
    logging.LogInfoMessage(contractorName)
    totalSum = 0

    for city in cities:
        items = [item for item in contractorsItems[contractorName] if item[3] == city]
        if len (items) != 0:
            message += "<p><span style = 'font-weight:700;text-decoration:underline;'>{0}</span><br/>".format(city.encode("utf-8"))

            for item in items:
                itemSum = int(item[1] * item[2])
                totalSum += itemSum
                message += "{0} - {1} шт.<br/>".format(item[0].encode("utf-8"), str(item[1]))
                logging.LogInfoMessage(item[0] + " - " + str(item[1]) + u" шт. - " + str(itemSum) + u" руб.\n")

        message += "</p>"

    message += "========================\nОбщая сумма: " + str(totalSum) + " руб.\n\n"


result = mails.SendInfoMail("Товары по поставщикам", message)
if result != True:
    logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
