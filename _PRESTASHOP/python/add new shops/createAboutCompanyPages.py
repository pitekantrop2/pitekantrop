﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import SendMails


pointSource = u"""
<p style="text-align: left;"><span style="text-decoration:underline;">{name}</span></p>
<p>Адрес: {address}</p>
<p>Телефоны: {phones}</p>
<p>Время работы: {hours}</p>
"""

def GetPageSource(shopName):
    pvzNodes = root.xpath("//Pvz[contains(@City, '" + shopName + "')]")

    #формирование страницы
    if len(pvzNodes) != 0:

        pageSource_tmp = pageSource

        if len(pvzNodes) > 1:
            pageSource_tmp = pageSource_tmp.replace("{points}", u"Пункты")
        else:
            pageSource_tmp = pageSource_tmp.replace("{points}", u"Пункт")

        cityName = commonLibrary.GetCity(shopName)
        pageSource_tmp = pageSource_tmp.replace("{city}", cityName)
        pointsSource = ""

        for i in range(0, len(pvzNodes)):

            pvzNode = pvzNodes[i]
            pointSource_tmp = pointSource
            pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
            pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
            pointSource_tmp = pointSource_tmp.replace("{phones}", pvzNode.attrib["Phone"])
            pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])

            if i != len(pvzNodes) - 1:
                pointSource_tmp += "<p><br/></p>"

            pointsSource += pointSource_tmp

        pageSource_tmp = pageSource_tmp.replace("{pointsList}", pointsSource)

    else:
        pageSource_tmp = defaultPageSource

    return pageSource_tmp


#########################################################################################################################

sites = [
{"url": "http://safetus.ru", "ip" : "185.5.249.9", "prestashopKey" : "BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU", "dbName" : "safetus", "dbUser" : "safetus_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "safetus", "domain":"safetus.ru"},
{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "knowall", "domain":"knowall.ru.com"},
{"url": "http://otpugiwatel.com", "ip" : "185.5.251.99", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpug", "dbUser" : "otpug_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugiwatel", "domain":"otpugiwatel.com"},
{"url": "http://tomsk.otpugivatel.com", "ip" : "185.5.251.103", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpugivatel", "dbUser" : "otpugiv_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugivatel", "domain":"otpugivatel.com"}
]

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "create_pages")
logging = log.Log(logfile)

resp = requests.get("http://gw.edostavka.ru:11443/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

for site in sites:

    if site["url"] != sys.argv[1]:
        continue

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
        pageSource = open("about company pages\\aboutCompany_otpug.txt","r").read()
        defaultPageSource = open("about company pages\\aboutCompany_otpug_default.txt","r").read()

    if prestashop_api.siteUrl in ["http://safetus.ru"]:
        pageSource = open("about company pages\\aboutCompany_safetus.txt","r").read()
        defaultPageSource = open("about company pages\\aboutCompany_safetus_default.txt","r").read()

    if prestashop_api.siteUrl in ["http://omsk.knowall.ru.com"]:
        pageSource = open("about company pages\\aboutCompany_knowall.txt","r").read()
        defaultPageSource = open("about company pages\\aboutCompany_knowall_default.txt","r").read()

    pageSource = pageSource.decode("cp1251")
    defaultPageSource = defaultPageSource.decode("cp1251")

    shops = prestashop_api.GetAllShops()

    for shop in shops:

        shopId, shopDomen, shopName = shop

        if shopId < int(sys.argv[2]):
            continue

##        if shopId < 207:
##            continue


        link_rewrite = "about-us-" + shopName.encode("cp1251").replace("\xa0", "-").decode("cp1251")
        link_rewrite = link_rewrite.replace(" ", "-")

        if prestashop_api.IsPageExists(link_rewrite):
            continue

        pageSource_tmp = GetPageSource(shopName)

        #добавление в БД
        id_cms = prestashop_api.GetLastInsertedId("ps_cms","id_cms")
        id_cms += 1
        ps_cms_query = """INSERT INTO ps_cms(id_cms, id_cms_category, position, active, indexation)
                            VALUES (%(id_cms)s,1,0,1,0)"""%{"id_cms":id_cms}

        prestashop_api.MakeUpdateQueue(ps_cms_query)

        sql = """SELECT id_cms_block FROM ps_cms_block_shop
                WHERE id_shop = %(id_shop)s"""%{"id_shop":shopId}

        id_cms_block = prestashop_api.MakeGetInfoQueue(sql)[0][0]

        ps_cms_block_page_query = """INSERT INTO ps_cms_block_page(id_cms_block_page, id_cms_block, id_cms, is_category)
                                    VALUES (0,%(id_cms_block)s,%(id_cms)s,0)"""%{"id_cms_block":id_cms_block,
                                    "id_cms":id_cms}

        prestashop_api.MakeUpdateQueue(ps_cms_block_page_query)

        ps_cms_lang_query = u"""INSERT INTO ps_cms_lang(id_cms, id_lang, meta_title, meta_description, meta_keywords, content, link_rewrite)
                                VALUES (%(id_cms)s,1,'О компании','Информация о компании','о нас, информация','%(content)s','%(link_rewrite)s')"""%{"id_cms":id_cms,
                 "content":pageSource_tmp, "link_rewrite":link_rewrite}

        prestashop_api.MakeUpdateQueue(ps_cms_lang_query)

        ps_cms_shop_query = """INSERT INTO ps_cms_shop (id_cms ,id_shop)
                                VALUES(%(id_cms)s,%(id_shop)s)"""%{"id_shop":shopId,"id_cms":id_cms}

        prestashop_api.MakeUpdateQueue(ps_cms_shop_query)


    logging.LogInfoMessage(u"Добавлена страница для города: " + shopDomen)
    print "page for shop {0} has been added".format(shopId)

mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (createAboutCompanyPages)", "", [logfile])