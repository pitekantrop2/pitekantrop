﻿import requests
import lxml
from lxml import etree
import urllib
import json
import datetime
import sys
import re, urlparse
import PrestashopAPI
import io
import commonLibrary
import os
import log
import subprocess
import ftplib

logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "createVhosts")
logging = log.Log(logFileName)
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if site["url"] != sys.argv[1]:
        continue

##    if machineName.find("hp") != -1:
##        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)

    if prestashop_api == False:
        continue

    session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
    session.set_pasv(False)

    bChanges = False

    session.cwd("/home/{0}/www".format(site["siteDir"]))
    file = open(".htaccess",'wb')
    session.retrbinary('RETR %s' % ".htaccess", file.write)
    file.close()

    htaccessFileContent = ""
    with open(".htaccess", "r") as vhostsfile:
        htaccessFileContent = vhostsfile.read()

    shops = prestashop_api.GetInactiveShops()
##    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, cityName = shop
        tranliteratedName = prestashop_api.Transliterate(cityName).lower().replace(" ", "-")

        if htaccessFileContent.find("/{0}/".format(tranliteratedName)) == -1:
            bChanges = True
            logging.LogInfoMessage("htaccess: " + tranliteratedName)
            #new_entry
            entry = \
"""RewriteRule ^{0}$ /{0}/ [L,R]
RewriteRule ^{0}/(.*) /$1 [L]

#new_entry""".format(tranliteratedName)
            htaccessFileContent = htaccessFileContent.replace("#new_entry", entry)
            with open(".htaccess", "w") as vhostsfile:
                vhostsfile.write(htaccessFileContent.encode("cp1251"))


    if bChanges:
        session.delete(".htaccess")
        file = open(".htaccess",'rb')
        session.storbinary('STOR ' + ".htaccess", file)
        file.close()
        os.remove(".htaccess")

    session.close()

