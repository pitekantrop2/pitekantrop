﻿import PrestashopAPI
import commonLibrary
import SendMails
import subprocess
import requests
from lxml import etree



mails = SendMails.SendMails()

sites = [
{"url": "http://tomsk.otpugivatel.com", "ip" : "185.5.251.103", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpugivatel", "dbUser" : "otpugiv_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugivatel", "domain":"otpugivatel.com","baseUrl":"http://tomsk.otpugivatel.com/admin9945/index.php"}
]

resp = requests.get("https://integration.cdek.ru/pvzlist.php")
pvzXml = etree.XML(resp._content)

root =  pvzXml.getroottree().getroot()
pvzNodes = root.xpath("//Pvz[ not(contains (@Name, '" + u"Фиктивный" + "')) and @CountryCode = 1 and not(contains (@Name, ','))] ")

sdekCities = []

for node in pvzNodes:
    city = node.attrib["City"].split(",")[0].split(" (")[0]
    if  commonLibrary.excludedCities.find(city) == -1:
        sdekCities.append(city)

myset = set(sdekCities)
uniqueSdekCities = list(myset)
newCities = []

for site in sites:

    newDomainsList = []

    ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        ssh.kill()
        continue

    baseUrl = site["baseUrl"]

    shops = prestashop_api.GetAllShops()
    shopsNames = [x[2] for x in shops]

    for newCity in uniqueSdekCities:

        if newCity in shopsNames:
            continue

        if newCity in [u"Набережные Челны",
        u"Великие Луки", u"Новый Уренгой",
        u"Минеральные Воды", u"Нижний Тагил", u"Сосновый Бор", u"Атырау (Гурьев)", u"Москва", u"Великий Новгород",
        u"Старый Оскол"]:
            continue

        newCities.append(newCity)

    ssh.kill()

if len(newCities) > 0:
    mails.SendInfoMail("Новые города", ", ".join(newCities))