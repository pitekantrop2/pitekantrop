﻿from UBaseObjects import *

class UIhorObjects(UBaseObjects):

    Login = ["id", "username"]
    Password = ["id", "password"]
    Submit = ["id", "submit"]

    DomainsNamesLink = ["xpath", "//a[contains(., '" + u"Доменные имена" + "')]"]
    MainLink = ["xpath", "//a[contains(., '" + u"Главное" + "')]"]
    AddDomainButton = ["xpath", "//div[contains(@class, 's-icon s24x24 t-new')]"]
    MasterRadio = ["xpath", "//div[@data-val = 'master']"]
    DomainName = ["name", "name"]
    IpAddress = ["name", "ip"]
    OkButton = ["xpath", "//div[@data-act = 'ok']"]
    CancelButton = ["xpath", "//div[@data-act = 'cancel']"]





