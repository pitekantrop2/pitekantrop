﻿from UBaseObjects import *

class URegRuObjects(UBaseObjects):

    EnterLink = ["xpath", "//a[contains(., '" + u"Войти" + "')]"]
    Login = ["xpath", "//div[@class = 'b-popup__content']//input[contains(@placeholder, '" + u"Электронная почта или логин" + "')]"]
    Password = ["xpath", "//div[@class = 'b-popup__content']//input[contains(@placeholder, '" + u"Пароль" + "')]"]
    Submit = GetButtonByText(u"Войти")

    #DomainsNamesLink = ["xpath", "//a[contains(., '" + u"Доменные имена" + "')]"]
    AddDomainButton = ["xpath", "//ul[@class = 'glamor_list mark_down']//a[contains(., '" + u"A" + "')]"]
    SubdomainName = ["name", "rr_subname_0"]
    IPAddress = ["name", "rr_ipaddr_0"]
    AddEntryButton = ["xpath", "//button[contains(., '" + u"Добавить запись A" + "')]"]
    SubdomainLink = ["xpath", "//abbr[@class = 'tooltip dotted-bottom']"]
    RemoveButton = ["xpath", ".//input[@class = 'remove_but i-data-confirm']"]
    RecordRow = ["xpath", "//tr[@style = 'color: black']"]
    AcceptPolicy = GetButtonByText(u"Принять")
    RemoveSubdomainsButtons = ["xpath", "//table[@class = 'rrs_table']//tr[contains(., '95.216.161.87')]//input[@class = 'remove_but i-data-confirm']"]
    YesButton = GetButtonByText(u"Да")



