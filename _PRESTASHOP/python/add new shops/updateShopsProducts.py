﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import re
import commonLibrary
import SendMails


def UpdateProductsSeoParameters(shopId):
    shopProducts = prestashop_api.GetShopProducts(shopId)

    if shopProducts == -1:
        return

    for shopProduct in shopProducts:

        productID, link_rewrite, meta_title, description, name, meta_keywords, meta_description = shopProduct
##        print productID

        if link_rewrite.find(shopName.replace(" ", "-")) != -1:
            continue

        if link_rewrite.find(shopName.encode("cp1251").replace("\xa0", "-").decode("cp1251")) != -1:
            continue

        city = commonLibrary.GetCity(shopName)

        meta_title = meta_title.replace(u" в Москве", "")
        meta_title = meta_title.replace(u" в Омске", "")
        meta_title = meta_title.replace(u" в " + city, "")
        meta_title = meta_title.replace(u"купить", "")
        meta_title = meta_title.replace(u"  ", u" ")
        meta_title = meta_title.replace(u"недорого", "")
        meta_title = meta_title.replace(u"otpug - ", "")
        meta_title = meta_title.replace(u"Отпугиватель.COM - ", "")
        meta_title = meta_title.strip()

        if prestashop_api.siteUrl in ["http://safetus.ru", "http://otpugiwatel.com"]:
            if meta_title.lower().find(u"купить") == -1:
                meta_title += u" купить"

        meta_title += u" в " + city

        link_rewrite = link_rewrite.replace(u"-купить-Москва", "")
        link_rewrite = link_rewrite.replace(u"-купить-москва", "")
        link_rewrite = link_rewrite.replace(u"-купить-Омск", "")
        link_rewrite = link_rewrite.replace(u"-купить-омск", "")
        link_rewrite = link_rewrite.replace(u"-купить-Novall", "")
        link_rewrite = link_rewrite.replace(u"-купить-" + shopName.replace(" ", "-"), "")
        link_rewrite = link_rewrite.replace(u"-купить-" + shopName.encode("cp1251").replace("\xa0", "-").decode("cp1251"), "")
        link_rewrite += u"-купить-" + shopName

        link_rewrite = link_rewrite.encode("cp1251").replace("\xa0", "-").decode("cp1251")
        link_rewrite = link_rewrite.replace(" ", "-")

        description = description.replace("'","")
        meta_title = meta_title.replace("'","")
##        print description

##        print link_rewrite
##        print meta_title
        meta_keywords = name.replace(" ", ",") + u", купить, " + shopName
        categoryId = prestashop_api.GetProductCategoryDefault(productID)
        categoryDetails = prestashop_api.GetShopCategoryDetails(shopId, categoryId)
        if categoryDetails == -1:
            continue
        if prestashop_api.siteUrl in ["http://tomsk.otpugivatel.com", "http://omsk.knowall.ru.com"]:
            meta_description = name + u" купить в " + city + u" по низкой цене. Гарантия на всю продукцию. Скидочная система. " + categoryDetails[0][4] + u" в " + city + "."
        if prestashop_api.siteUrl in ["http://safetus.ru"]:
            meta_description = u"Качественные и недорогие " + downcase(categoryDetails[0][4]) + u" в " + city + u". Купить " + downcase(name) + u" в " + city + u" с гарантией. Скидки. Удобные способы доставки и оплаты."
        if prestashop_api.siteUrl in ["http://otpugiwatel.com"]:
            meta_description = u"Эффективные и недорогие " + downcase(categoryDetails[0][4]) + u" в " + city + u". Купить " + downcase(name) + u" в " + city + u" с гарантией. Скидки. Удобные способы доставки и оплаты."
##        print meta_keywords
##        print meta_description
        params = [link_rewrite, meta_title, description, meta_keywords, meta_description]
        prestashop_api.UpdateShopProductData(shopId, productID, params)



############################################################################################################################################

sites = [
{"url": "http://safetus.ru", "ip" : "185.5.249.9", "prestashopKey" : "BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU", "dbName" : "safetus", "dbUser" : "safetus_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "safetus", "domain":"safetus.ru", "baseUrl":"http://safetus.ru/admin0581/index.php"},
{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "knowall", "domain":"knowall.ru.com","baseUrl":"http://omsk.knowall.ru.com/admin1301/index.php"},
{"url": "http://otpugiwatel.com", "ip" : "185.5.251.99", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpug", "dbUser" : "otpug_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugiwatel", "domain":"otpugiwatel.com","baseUrl":"http://otpugiwatel.com/admin9945/index.php"},
{"url": "http://tomsk.otpugivatel.com", "ip" : "185.5.251.103", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpugivatel", "dbUser" : "otpugiv_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugivatel", "domain":"otpugivatel.com","baseUrl":"http://tomsk.otpugivatel.com/admin9945/index.php"}
]

downcase = lambda s: s[:1].lower() + s[1:] if s else ''

logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "updateProducts")
logging = log.Log(logFileName)

resp = requests.get("http://gw.edostavka.ru:11443/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

for site in sites:

    if site["url"] != sys.argv[1]:
        continue

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        continue

    ipAddress = site["ip"]

    logging.Log("==================================  " + site["url"] + "  ==================================")

    shops = prestashop_api.GetAllShops()

    for shop in shops:

        shopId, shopDomen, shopName = shop

        if shopId < int(sys.argv[2]):
            continue

##        if shopId < 207:
##            continue

        UpdateProductsSeoParameters(shopId)

        if prestashop_api.siteUrl in ["http://tomsk.otpugivatel.com", "http://otpugiwatel.com"]:

            products = prestashop_api.GetShopProducts(10)

            for product in products:

                productID, link_rewrite, meta_title, description, name, meta_keywords, meta_description = product

                productDetails = prestashop_api.GetShopProductDetailsById(productID, 10)
                prestashop_api.SetProductPriceForShop(productID,shopId, productDetails[2], productDetails[1])


mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (updateShopsProducts)", "", [logFileName])
