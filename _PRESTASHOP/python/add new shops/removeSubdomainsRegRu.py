﻿import myLibrary
import URegRuObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import commonLibrary
import SendMails
import sys
import subprocess

def Login(login, password):
    Driver.OpenUrl(baseUrl)
    Driver.Click(URegRuObjects.URegRuObjects.EnterLink)
    Driver.WaitFor(URegRuObjects.URegRuObjects.Login)
    Driver.SendKeys(URegRuObjects.URegRuObjects.Login, login)
    Driver.SendKeys(URegRuObjects.URegRuObjects.Password, password)
    Driver.Click(URegRuObjects.URegRuObjects.Submit)
    Driver.WaitForText("pitekantrop")


def RemoveSubdomain(button):
    Driver.Click(button)
    Driver.AcceptAlert()
    Driver.WaitForText("Операция проведена успешно")
    time.sleep(1)

##################################################################################


logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "addSubdomainsRegRu")
logging = log.Log(logFileName)

baseUrl = "https://www.reg.ru"

Driver = myLibrary.Selenium()
Driver.SetUp()
Login("pitekantrop", "1qaz@WSX3edc")

for site in commonLibrary.sitesParams:
##    ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))
##
##    logging.Log("==================================  " + site["url"] + "  ==================================")
##
##    prestashop_api = commonLibrary.GetPrestashopInstance(site)
##
##    if prestashop_api == False:
####        ssh.kill()
##        continue

    ipAddress = site["ip"]
    domain = site["domain"]
    service_ids = "4850663"

##    if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
##        service_ids = "1908357"
##    if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
##        service_ids = "4850663"
##    if prestashop_api.siteUrl == "http://saltlamp.su":
##        service_ids = "27797981"
##    if prestashop_api.siteUrl == "http://insect-killers.ru":
##        service_ids = "27980473"
##    if prestashop_api.siteUrl == "http://sushilki.ru.com":
##        service_ids = "27844785"
##    if prestashop_api.siteUrl == "http://glushilki.ru.com":
##        service_ids = "27849155"
##    if prestashop_api.siteUrl == "http://mini-camera.ru.com":
##        service_ids = "27895743"
##    if prestashop_api.siteUrl == "http://otpugivateli-grizunov.ru":
##        service_ids = "27972971"
##    if prestashop_api.siteUrl == "http://otpugivateli-krotov.ru":
##        service_ids = "27972773"
##    if prestashop_api.siteUrl == "http://otpugivateli-ptic.ru":
##        service_ids = "27972805"
##    if prestashop_api.siteUrl == "http://otpugivateli-sobak.ru":
##        service_ids = "30825329"

    url = baseUrl + "/service/zone_manager?service_ids=" + service_ids
    Driver.OpenUrl(url)

    buttons = Driver.MyFindElements(URegRuObjects.URegRuObjects.RemoveSubdomainsButtons)
    while (len(buttons)) != 0:
        RemoveSubdomain(buttons[0])
        buttons = Driver.MyFindElements(URegRuObjects.URegRuObjects.RemoveSubdomainsButtons)



Driver.CleanUp()

mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (addSubdomainsRegRu)", "", [logFileName])