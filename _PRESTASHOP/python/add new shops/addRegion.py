﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import yandex
import ftplib
import UYandexObjects

def HasBeenProcessed(siteDomain):
    with open(logFileName, "r") as myfile:
        if myfile.read().find(siteDomain) != -1:
            return True

    return False

def GetShopRegion(shopDomain):

    if shopDomain == "mineralnye-vody.otpugivatel.com":
        return u"Минеральные Воды"

    for shop in shops:
        if shop[1] == shopDomain:
            return shop[2]


logFileName = log.GetLogFileName("c:\\programming\\_SELENIUM\\_PRESTASHOP\\log", "regions")
##logFileName = "c:\\programming\\_SELENIUM\\_PRESTASHOP\\regions_8_3_18_9.log"
logging = log.Log(logFileName)
objects = UYandexObjects.UYandexObjects

##prestashop_api = PrestashopAPI.PrestashopAPI("http://otpugiwatel.com", 'QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6', "127.0.0.1", "otpug", "otpug_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://amazing-things.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", #"amazing_things", "am_th_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://tomsk.otpugivatel.com", 'QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6', "127.0.0.1", "otpugivatel", "otpugiv_user_ex", "1qaz@WSX", 5555)
##prestashop_api = PrestashopAPI.PrestashopAPI("http://omsk.knowall.ru.com", 'KAR7M17CPPALJY38NKI521NNP96IH3Z7', "127.0.0.1", "knowall", "knowall_user_ex", "1qaz@WSX", 5555)
prestashop_api = PrestashopAPI.PrestashopAPI("http://safetus.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", "safetus", "safetus_user_ex", "1qaz@WSX", 5555)



yandex_ = yandex.Yandex(True)
driver = yandex_.selenium
##yandex_.Login("pitekantrop5", "2wsx#EDC")
yandex_.Login("pitekantrop5-safetus", "3edc$RFV")
##yandex_.Login("pitekantrop-knowall", "3edc$RFV")
##yandex_.Login("pitekantrop-amth", "2wsx#EDC")
##yandex_.Login("pitekantrop-otpugivatel", "2wsx#EDC")
##yandex_.Login("pitekantrop5", "2wsx#EDC")
driver.OpenUrl("https://webmaster.yandex.ru/sites/")
driver.WaitFor(objects.deleteSiteButton)

shops = prestashop_api.GetAllShops()

while True:

    sitesLinks = driver.MyFindElements(objects.siteLink)

    for i in range(0, len(sitesLinks)):

        siteLink = driver.MyFindElements(objects.siteLink)[i]
        siteDomain = driver.GetText(siteLink)


        logging.Log(siteDomain)

        driver.Click(siteLink)
        driver.WaitForText("ТИЦ")

        driver.Click(["xpath", "//span[contains(.,'" + u"География сайта" + "') and @class = 'b-pseudo-link']"])
        time.sleep(1)
        driver.Click(["xpath", "//a[contains(.,'" + u"Регион сайта" + "')]"])
        time.sleep(2)
        if driver.IsTextPresent("Сайт  отнесён к следующим регионам:") \
        or driver.IsTextPresent("Изменения вступят в силу при следующем обновлении поисковой базы Яндекса") \
        or driver.IsTextPresent("Вы установили регион") \
        or driver.IsTextPresent("В данный момент права на управление не подтверждены") \
        or driver.IsTextPresent("Раздел недоступен, так как сайт не проиндексирован"):
            driver.Back()
            driver.Back()
            continue

        region = GetShopRegion(siteDomain)
        try:
            pageId = prestashop_api.GetIdCmsByLinkRewrite(region)
            pageUrl = "http://" + siteDomain + "/content/" + str(pageId) + "-about-us-" + region
        except:
            driver.Back()
            driver.Back()
            continue
        driver.WaitFor(objects.region_RegionEdit)
        driver.SendKeys(objects.region_RegionEdit, region)
        if driver.IsTextPresent("Такого региона нет"):
            driver.Back()
            driver.Back()
            driver.Back()
        driver.WaitFor(objects.region_RegionVariant)
        driver.Click(objects.region_RegionVariant)
        driver.SendKeys(objects.region_UrlEdit, pageUrl)
        driver.Click(objects.region_SetRegionButton)
        driver.WaitForText("Вы установили регион")
        driver.Back()
        driver.Back()
        driver.Back()

    if driver.IsVisible(objects.nextPageLink):
        driver.Click(objects.nextPageLink)
    else:
        break




yandex_.selenium.CleanUp()