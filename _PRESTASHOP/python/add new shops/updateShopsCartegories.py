﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import commonLibrary
import SendMails

seoBlock = u"""
<p><a href="_href_">Купить {name} в {city}</a> можно в нашем интернет-магазине - например, с доставкой на пункт выдачи:</p>
{points}"""
##seoBlock = u"""
##<p>Купить {name} в {city} можно в нашем интернет-магазине - например, с доставкой на пункт выдачи:</p>
##{points}"""

pointSource = u"""
<p><span style="text-decoration:underline;">{name}</span> - г. {city}, {address}, время работы: {hours}</p>
"""

def GetTmpBlock(name, shopName):
    pvzNodes = root.xpath("//Pvz[contains(@City, '" + shopName.encode("cp1251").replace("\xa0", " ").decode("cp1251") + "')]")
    city = commonLibrary.GetCity(shopName)
    tmpSeoBlock = seoBlock.replace("{name}", downcase(name)).replace("{city}", city).replace("{points}","")
    return tmpSeoBlock

def GetSeoBlock(name, shopName):

    pvzNodes = root.xpath("//Pvz[contains(@City, '" + shopName.encode("cp1251").replace("\xa0", " ").decode("cp1251") + "')]")
    city = commonLibrary.GetCity(shopName)
    tmpSeoBlock = seoBlock.replace("{name}", downcase(name)).replace("{city}", city)

    #формирование страницы
    if len(pvzNodes) == 0:
        return tmpSeoBlock.replace("{points}", "")

    pointsSource = ""

    for i in range(0, len(pvzNodes)):

        pvzNode = pvzNodes[i]
        pointSource_tmp = pointSource
        pointSource_tmp = pointSource_tmp.replace("{name}", pvzNode.attrib["Name"])
        pointSource_tmp = pointSource_tmp.replace("{address}", pvzNode.attrib["Address"])
        pointSource_tmp = pointSource_tmp.replace("{hours}", pvzNode.attrib["WorkTime"])
        pointSource_tmp = pointSource_tmp.replace("{city}", shopName)

        pointsSource += pointSource_tmp

    tmpSeoBlock = tmpSeoBlock.replace("{points}", pointsSource)

    return tmpSeoBlock


#############################################################################################################

sites = [
{"url": "http://safetus.ru", "ip" : "185.5.249.9", "prestashopKey" : "BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU", "dbName" : "safetus", "dbUser" : "safetus_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "safetus", "domain":"safetus.ru", "baseUrl":"http://safetus.ru/admin0581/index.php"},
{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "knowall", "domain":"knowall.ru.com","baseUrl":"http://omsk.knowall.ru.com/admin1301/index.php"},
{"url": "http://otpugiwatel.com", "ip" : "185.5.251.99", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpug", "dbUser" : "otpug_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugiwatel", "domain":"otpugiwatel.com","baseUrl":"http://otpugiwatel.com/admin9945/index.php"},
{"url": "http://tomsk.otpugivatel.com", "ip" : "185.5.251.103", "prestashopKey" : "QFG44Y1J7WMAY167FKA6ZFFYBQ5DJHY6", "dbName" : "otpugivatel", "dbUser" : "otpugiv_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "otpugivatel", "domain":"otpugivatel.com","baseUrl":"http://tomsk.otpugivatel.com/admin9945/index.php"}
]

downcase = lambda s: s[:1].lower() + s[1:] if s else ''

logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "updateCategories")
logging = log.Log(logFileName)

resp = requests.get("http://gw.edostavka.ru:11443/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

for site in sites:

    if site["url"] != sys.argv[1]:
        continue

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        continue

    ipAddress = site["ip"]

    logging.Log("==================================  " + site["url"] + "  ==================================")

    shops = prestashop_api.GetAllShops()

    for shop in shops:

        shopId, shopDomen, shopName = shop

        if shopId < int(sys.argv[2]):
            continue
##
##
##        if shopId < 265:
##            continue

        shopCategories = prestashop_api.GetShopCategories(shopId)

        for shopCategory in shopCategories:

            categoryID, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = shopCategory

            if categoryID in [1,2]:
                continue

            if prestashop_api.IsCategoryActive(categoryID) == 0:
                continue

            if description == None:
                description = ""
            if long_description == None:
                long_description = ""

            if link_rewrite.find(shopName.replace(" ", "-")) != -1:
                continue

            if link_rewrite.find(shopName.encode("cp1251").replace("\xa0", "-").decode("cp1251")) != -1:
                continue
##
            city =  commonLibrary.GetCity(shopName)

            meta_title = meta_title.replace(u" в Москве", "")
            meta_title = meta_title.replace(u" в Омске", "")
            meta_title = meta_title.replace(u"купить", "")
            meta_title = meta_title.replace(u"недорого", "")
            meta_title = meta_title.replace(u"Новалл - ", "")
            meta_title = meta_title.replace(u"Safetus - ", "")
            meta_title = meta_title.replace(u"Отпугиватель.Com - ", "")
            meta_title = meta_title.strip()

            if prestashop_api.siteUrl in ["http://safetus.ru", "http://otpugiwatel.com"]:
                if meta_title.lower().find(u"купить") == -1:
                    meta_title += u" купить"

            meta_title += u" в " + city

            link_rewrite = link_rewrite.replace(u"-купить-Москва", "")
            link_rewrite = link_rewrite.replace(u"-купить-москва", "")
            link_rewrite = link_rewrite.replace(u"-купить-Омск", "")
            link_rewrite = link_rewrite.replace(u"-купить-омск", "")

            link_rewrite += u"-купить-" + shopName
            link_rewrite = link_rewrite.encode("cp1251").replace("\xa0", "-").decode("cp1251")
            link_rewrite = link_rewrite.replace(" ", "-")

            href = "/" + str(categoryID) + "-" + link_rewrite

            if description != None:

                if description.find(city) == -1:
                    ind = description.rfind(u"Качественные")
                    if ind != -1:
                        description = description[0:ind]
                    ind = description.rfind(u"Эффективные")
                    if ind != -1:
                        description = description[0:ind]
                    if prestashop_api.siteUrl not in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
                        descseoBlock = u"""<p>Качественные и недорогие <strong>%(name)s в %(city)s</strong> Вы можете купить, оформив заказ в нашем интернет-магазине.</p>"""%{"name":downcase(name),
                        "city":city}
                    else:
                        descseoBlock = u"""<p>Эффективные и недорогие <strong>%(name)s в %(city)s</strong> Вы можете купить, оформив заказ в нашем интернет-магазине.</p>"""%{"name":downcase(name),
                        "city":city}
                description += descseoBlock

            description = description.replace("_href_", href)
            description = description.replace(u"Москва", shopName)
            description = description.replace(u"Омск", shopName)

            ###########################
            if long_description != "":
                ldElement = etree.HTML(long_description)
                elements = ldElement.xpath(".//p[contains(., '" + u"например, с доставкой на пункт выдачи"  + "') or contains(., '" + u"время работы"  + "')]")

                for element in elements:
                    element.getparent().remove(element)

                long_description = etree.tostring(ldElement)
                regexp = "&.+?;"
                list_of_html = re.findall(regexp, long_description) #finds all html entites in page
                for e in list_of_html:
                    h = HTMLParser.HTMLParser()
                    unescaped = h.unescape(e) #finds the unescaped value of the html entity
                    long_description = long_description.replace(e, unescaped) #replaces html entity with unescaped value
                long_description = long_description.replace("<html>","")
                long_description = long_description.replace("</html>","")
                long_description = long_description.replace("<body>","")
                long_description = long_description.replace("</body>","")

            ###########################

            if long_description.find(city) == -1:
                seoblock = GetSeoBlock(name, shopName)
                long_description += seoblock
                long_description = long_description.replace("_href_", href)

            description = description.replace("'","")
            meta_keywords = link_rewrite.replace("-", ",")
            if prestashop_api.siteUrl in ["http://tomsk.otpugivatel.com", "http://omsk.knowall.ru.com"]:
                meta_description = name + u" купить в " + city + u" по низкой цене. Гарантия на всю продукцию. Скидочная система."
                if prestashop_api.siteUrl == "http://tomsk.otpugivatel.com":
                    meta_description += u" Ультразвуковые отпугиватели крыс, мышей, кротов, собак, птиц, комаров, ползающих насекомых и других вредителей в " + city + "."
                else:
                    meta_description += u" Камеры видеонаблюдения, видеоглазки, обнаружители \"жучков\", глушилки сотовых, GSM-сигнализации, видеодомофоны, усилители сигналов в " + city + "."
            elif prestashop_api.siteUrl in ["http://safetus.ru", "http://amazing-things.ru"]:
                meta_description = u"Качественные и недорогие " + downcase(name) + u" в " + city + u". Гарантия. Скидки. Удобные способы доставки и оплаты."
                if prestashop_api.siteUrl == "http://safetus.ru":
                    meta_description += u" Купить в " + city + u" камеру видеонаблюдения, глушилку сотовых, видеоглазок, обнаружитель \"жучков\" и другие полезные товары."
            else:
                meta_description = u"Эффективные и недорогие " + downcase(name) + u" в " + city + u". Гарантия. Скидки. Удобные способы доставки и оплаты."
                meta_description += u" Купить в " + city + u" отпугиватели ультразвуковые мышей, крыс, собак, кротов, птиц, комаров, ползающих насекомых и других вредителей."
            print meta_title
    ##        print meta_description
    ##        print description
    ##        print long_description
            params = [link_rewrite, meta_title, description, long_description, meta_keywords, meta_description ]
            prestashop_api.UpdateCategoryData(shopId, categoryID, params)


        logging.LogInfoMessage("Categories of " + shopDomen + " has been updated")


mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (updateShopsCartegories)", "", [logFileName])