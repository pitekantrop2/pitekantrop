﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import io
import SdekAPI
import RussianPostAPI
import shutil
import ftplib
import commonLibrary
import SendMails
import os


def ConfigureMenu(shopId, value, update = True):

    if update == False:

        last_insert_id = prestashop_api.GetLastInsertedId("ps_configuration","id_configuration") + 1

        sql = """INSERT INTO ps_configuration (id_configuration, id_shop_group, id_shop, name, value, date_add, date_upd)
                 VALUES (%(last_insert_id)s, 1, %(shopID)s,'MOD_BLOCKTOPMENU_ITEMS', '%(value)s', '2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"shopID":shopId,
                 "last_insert_id":last_insert_id,
                 "value":value}

        prestashop_api.MakeUpdateQueue(sql)

    else:

        sql = """UPDATE ps_configuration
                 SET value = '%(value)s'
                 WHERE id_shop = %(shopID)s AND name = 'MOD_BLOCKTOPMENU_ITEMS'"""%{"shopID":shopId,
                 "value":value}

        prestashop_api.MakeUpdateQueue(sql)


def ConfigurePhone(shopId, shopName):
    city = commonLibrary.GetCity(shopName,2)

    value = u"(бесплатно для " + city + u")"
    last_insert_id = prestashop_api.GetLastInsertedId("ps_configuration","id_configuration") + 1

    sql = """INSERT INTO ps_configuration (id_configuration, id_shop_group, id_shop, name, value, date_add, date_upd)
             VALUES (%(last_insert_id)s, 1, %(shopID)s,'BLOCKCONTACT_TELNUMBER', '%(value)s', '2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"shopID":shopId,
             "value":value, "last_insert_id":last_insert_id}

    prestashop_api.MakeUpdateQueue(sql)

def AddSeoText(shopId, shopName):

    city = commonLibrary.GetCity(shopName)
    seoText = u"""<h2 class="top_text">Удивительные подарки для мужчин, удивительные подарки для женщин купить в {city}, удивительные вещи для детей, интерактивные игрушки, игрушки-роботы купить в {city}, электронные светодиодные свечи, проекторы-ночники, беспроводные бэби-мониторы купить в {city}, популярные товары, новинки, удивительные вещи для безопасности купить в {city}, охранное видеонаблюдение, системы охраны с gsm-дозвонщиком, беспроводные охранные системы купить в {city}, персональные металлодетекторы, автономные системы безопасности, персональные и антикражные сирены купить в {city}, удивительные сувениры, часы, мышки купить в {city}, usb-хабы, колонки, ручки купить в {city}, лампы, будильники, mp3-плееры купить в {city}, винные аксессуары, наушники, копилки купить в {city}, прочие сувениры, товары на главной, удивительные вещи для дачников и путешественников купить в {city}, удивительные вещи для автомобилистов, борьба с вредителями, уничтожители грызунов купить в {city}, отпугиватели птиц, уничтожители насекомых, отпугиватели собак купить в {city}, отпугиватели насекомых, отпугиватели змей, универсальные отпугиватели купить в {city}, отпугиватели кротов, спецпредложения, удивительные вещи для дома купить в {city}, паровая техника, роботы-пылесосы, приборы для измерений купить в {city}, вакуумные пакеты, часы и метеостанции, удивительные вещи для развлечения купить в {city}, небесные фонарики, удивительные вещи для кухни, удивительные вещи для спорта купить в {city}, удивительные вещи для красоты и здоровья, массажеры, корректирующее белье купить в {city}, волшебные бигуди, разные товары для детей, разные товары для дома купить в {city}</h2>"""
    seoText = seoText.replace("{city}", city)

    last_insert_id = prestashop_api.GetLastInsertedId("ps_configuration","id_configuration") + 1

    sql = """INSERT INTO ps_configuration (id_configuration, id_shop_group, id_shop, name, value, date_add, date_upd)
             VALUES (%(last_insert_id)s, 1, %(shopID)s,'blockhtml_body', '%(seoText)s', '2015-01-01 20:41:10', '2015-01-01 20:41:10')"""%{"shopID":shopId,
             "seoText":seoText, "last_insert_id":last_insert_id}

    prestashop_api.MakeUpdateQueue(sql)

def ConfigureReinsurance(shopId):

    filesNames = ["reinsurance-3-1.jpg", "reinsurance-1-1.jpg", "reinsurance-2-1.jpg", "reinsurance-4-1.jpg", "reinsurance-5-1.jpg"]
    texts = [u"Оплата при получении", u"Гарантия на все товары",u"Быстрая замена брака",u"Удобная и быстрая доставка",u"Безопасная оплата онлайн" ]

    for i in range(0, len(filesNames)):

        last_insert_id = prestashop_api.GetLastInsertedId("ps_reinsurance","id_reinsurance") + 1
        fileName = filesNames[i]
        text = texts[i]

        sql = """INSERT INTO ps_reinsurance (id_reinsurance, id_shop, file_name)
                 VALUES (%(id)s, %(shopID)s, '%(fileName)s')"""%{"id":last_insert_id,
                 "shopID":shopId, "fileName":fileName}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_reinsurance_lang (id_reinsurance, id_lang, text)
                 VALUES (%(id)s, 1, '%(text)s')"""%{"id":last_insert_id,
                 "text":text}

        prestashop_api.MakeUpdateQueue(sql)


def AddCourierCarrier(shopId, shopName, zoneId):

    logging.LogInfoMessage(shopName)
    pvzNodes = root.xpath("//Pvz[starts-with(@City, '" + shopName.encode("cp1251").replace("\xa0", " ").decode("cp1251") + "')]")

    logging.LogInfoMessage(str(len(pvzNodes)))

    #если нет курьерской доставки - выходим
    if len(pvzNodes) == 0:
        return

    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    #удаляем неактивные пункты
    if shopCarriersNames != -1:
        for shopCarriersName in shopCarriersNames:
            if shopCarriersName.find(u"Курьером") != -1:
                return

    pvzNode = pvzNodes[0]

    requestData = {}
    requestData['version'] = "1.0"
    requestData['dateExecute'] = "2015-04-20"
    requestData['senderCityId'] = 44
    requestData['receiverCityId'] = int(pvzNode.attrib["CityCode"])
    requestData['tariffId'] = 137
    requestData['goods'] = [{'weight':1, 'length':20, 'width':20,'height':20}]
    calcResult = sdek.Calculate(requestData)



    #если ошибка - выходим
    if "error" in calcResult.keys():
        return

    id_carrier = prestashop_api.GetLastInsertedId("ps_carrier","id_carrier") + 1

    pointName = u"Курьером до двери."

    sql = """INSERT INTO ps_carrier(id_carrier, id_reference, id_tax_rules_group, name, url, active, deleted, shipping_handling, range_behavior, is_module, is_free, shipping_external, need_range, external_module_name, shipping_method, position, max_width, max_height, max_depth, max_weight, grade)
            VALUES (%(id_carrier)s,%(id_reference)s,0,'%(pointName)s','',1,0,0,0,0,0,0,0,'',2,3,0,0,0,0,0)"""%{"id_carrier":id_carrier,
            "id_reference":id_carrier, "pointName":pointName}

    prestashop_api.MakeUpdateQueue(sql)

    for j in range(1,4):

        sql = """INSERT INTO ps_carrier_group(id_carrier, id_group)
                VALUES (%(id_carrier)s,%(id_group)s)"""%{"id_carrier":id_carrier,"id_group":j}

        prestashop_api.MakeUpdateQueue(sql)

    delay = u"Срок доставки (дней): "
    if calcResult["result"]["deliveryPeriodMin"] == calcResult["result"]["deliveryPeriodMax"]:
        delay += str(calcResult["result"]["deliveryPeriodMax"])
    else:
        delay += str(calcResult["result"]["deliveryPeriodMin"]) \
                + " - " + str(calcResult["result"]["deliveryPeriodMax"])

    delay += "."

    sql = """INSERT INTO ps_carrier_lang(id_carrier, id_shop, id_lang, delay)
             VALUES (%(id_carrier)s,%(id_shop)s,1,'%(delay)s')"""%{"id_carrier":id_carrier,
             "id_shop":shopId,"delay":delay}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_shop(id_carrier, id_shop)
             VALUES (%(id_carrier)s,%(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_tax_rules_group_shop(id_carrier, id_tax_rules_group, id_shop)
             VALUES (%(id_carrier)s, 0, %(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_zone(id_carrier, id_zone)
             VALUES (%(id_carrier)s, %(zoneId)s)"""%{"id_carrier":id_carrier, "zoneId":zoneId}

    prestashop_api.MakeUpdateQueue(sql)

    id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

    rangesIds = []
    for k in range(0, 21):
        rangesIds.append(id_range_price + k)

    counter = 0

    for id_range_price in rangesIds:

        delimiter1 = 1000 * counter + 1

        if counter == 20:
            delimiter2 = 1000001
        else: delimiter2 = 1000 * (counter + 1)

        sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                 VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                 "id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

        prestashop_api.MakeUpdateQueue(sql)

        if counter == 20:
            delimiter2 = 21000

        id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
        price = int(calcResult["result"]["price"])

        if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com", "http://amazing-things.ru"]:
            price = price + delimiter2 * 0.06
        else:
            price = price + delimiter2 * 0.03

        price = int(10 * round(float(price)/10))

        sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                 VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                 "id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        counter += 1

    filename = str(id_carrier) + ".jpg"
    shutil.copyfile(u"c:\\programming\\_PRESTASHOP\\python\\courier.jpg", filename)

    session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
    session.set_pasv(False)
    session.cwd("/home/" + site["siteDir"] + "/www/img/s")
    file = open(filename,'rb')                  # file to send
    session.storbinary('STOR ' + filename, file)     # send the file
    file.close()                                    # close file and FTP
    session.quit()
    os.remove(filename)
    logging.LogInfoMessage("Courier carrier has been added")

def AddRussianPostCarrier(shopId, shopName , zoneId ):

    print shopName

    id_carrier = prestashop_api.GetLastInsertedId("ps_carrier","id_carrier") + 1

    pointName = u"Почтой России."

    sql = """INSERT INTO ps_carrier(id_carrier, id_reference, id_tax_rules_group, name, url, active, deleted, shipping_handling, range_behavior, is_module, is_free, shipping_external, need_range, external_module_name, shipping_method, position, max_width, max_height, max_depth, max_weight, grade)
            VALUES (%(id_carrier)s,%(id_reference)s,0,'%(pointName)s','',1,0,0,0,0,0,0,0,'',2,4,0,0,0,0,0)"""%{"id_carrier":id_carrier,
            "id_reference":id_carrier, "pointName":pointName}

    prestashop_api.MakeUpdateQueue(sql)

    for j in range(1,4):

        sql = """INSERT INTO ps_carrier_group(id_carrier, id_group)
                VALUES (%(id_carrier)s,%(id_group)s)"""%{"id_carrier":id_carrier,"id_group":j}

        prestashop_api.MakeUpdateQueue(sql)

    delay = u"Срок доставки (дней): "
    minDays = 8

    shopPostcode = rusPostApi.GetIndex( shopName, u"ленин")
    if shopPostcode != -1:
        minDays = rusPostApi.GetDays(shopPostcode) + 2

    delay += str(minDays) + " - " + str(minDays + 3)
    delay += "."

    sql = """INSERT INTO ps_carrier_lang(id_carrier, id_shop, id_lang, delay)
             VALUES (%(id_carrier)s,%(id_shop)s,1,'%(delay)s')"""%{"id_carrier":id_carrier,
             "id_shop":shopId,"delay":delay}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_shop(id_carrier, id_shop)
             VALUES (%(id_carrier)s,%(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_tax_rules_group_shop(id_carrier, id_tax_rules_group, id_shop)
             VALUES (%(id_carrier)s, 0, %(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

    prestashop_api.MakeUpdateQueue(sql)

    sql = """INSERT INTO ps_carrier_zone(id_carrier, id_zone)
             VALUES (%(id_carrier)s, %(zoneId)s)"""%{"id_carrier":id_carrier, "zoneId":zoneId}

    prestashop_api.MakeUpdateQueue(sql)

    id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

    rangesIds = []
    for k in range(0, 21):
        rangesIds.append(id_range_price + k)

    counter = 0

    for id_range_price in rangesIds:

        delimiter1 = 1000 * counter + 1

        if counter == 20:
            delimiter2 = 1000001
        else: delimiter2 = 1000 * (counter + 1)

        sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                 VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                 "id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

        prestashop_api.MakeUpdateQueue(sql)

        if counter == 20:
            delimiter2 = 21000

        id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
        if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com", "http://amazing-things.ru"]:
            price = 200
        else:
            price = 250
        price = price + delimiter2 * 0.04

        price = int(10 * round(float(price)/10))

        sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                 VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                 "id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        counter += 1

    filename = str(id_carrier) + ".jpg"
    shutil.copyfile(u"c:\\programming\\_PRESTASHOP\\python\\post.jpg", filename)

    session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
    session.set_pasv(False)
    session.cwd("/home/" + site["siteDir"] + "/www/img/s")
    file = open(filename,'rb')                  # file to send
    session.storbinary('STOR ' + filename, file)     # send the file
    file.close()                                    # close file and FTP
    session.quit()
    os.remove(filename)
    logging.LogInfoMessage(shopName + " " + pointName + " has been added")


def AddPointCarrier(shopId, shopName, zoneId ):

    pvzNodes = root.xpath("//Pvz[starts-with(@City, '" + shopName.encode("cp1251").replace("\xa0", " ").decode("cp1251") + "')]")

    logging.LogInfoMessage(str(len(pvzNodes)))

    #если нет пунктов - выходим
    if len(pvzNodes) == 0:
        return

    pvzNames = [u"На пункт выдачи \"" + pvzNode.attrib["Name"] + "\" (" + pvzNode.attrib["Address"] + ")." for pvzNode in pvzNodes]
    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    #удаляем неактивные пункты
    if shopCarriersNames != -1:
        for shopCarriersName in shopCarriersNames:
            if shopCarriersName.find(u"На пункт выдачи") != -1:
                try:
                    itemIndex = pvzNames.index(shopCarriersName)
                except:
                    print shopCarriersName
                    prestashop_api.DeactivateCarrier(shopCarriersName)
                    logging.LogInfoMessage(shopName + " " + shopCarriersName + " has been deactivated")

    shopCarriersNames = prestashop_api.GetShopActiveCarriersNames(shopId)

    for i in range(0, len(pvzNodes)):

        pvzNode = pvzNodes[i]
        pvzName = pvzNode.attrib["Name"]
        pointName = u"На пункт выдачи \"" + pvzName + "\" (" + pvzNode.attrib["Address"] + ")."

        bExists = False
        if shopCarriersNames != -1:
            for shopCarriersName in shopCarriersNames:
                if shopCarriersName == pointName:
                    bExists = True
                    break

        if bExists == True:
            logging.LogInfoMessage("exists")
            continue

        logging.LogInfoMessage(pointName)
        requestData = {}
        requestData['version'] = "1.0"
        requestData['dateExecute'] = "2015-04-20"
        requestData['senderCityId'] = 44
        requestData['receiverCityId'] = int(pvzNode.attrib["CityCode"])
        requestData['tariffId'] = 136
        requestData['goods'] = [{'weight':1, 'length':20, 'width':20,'height':20}]
        calcResult = sdek.Calculate(requestData)

        #если ошибка - выходим
        if "error" in calcResult.keys():
            return

        id_carrier = prestashop_api.GetLastInsertedId("ps_carrier","id_carrier") + 1

        sql = """INSERT INTO ps_carrier(id_carrier, id_reference, id_tax_rules_group, name, url, active, deleted, shipping_handling, range_behavior, is_module, is_free, shipping_external, need_range, external_module_name, shipping_method, position, max_width, max_height, max_depth, max_weight, grade)
                VALUES (%(id_carrier)s,%(id_reference)s,0,'%(pointName)s','',1,0,0,0,0,0,0,0,'',2,2,0,0,0,0,0)"""%{"id_carrier":id_carrier,
                "id_reference":id_carrier, "pointName":pointName}

        prestashop_api.MakeUpdateQueue(sql)

        for j in range(1,4):

            sql = """INSERT INTO ps_carrier_group(id_carrier, id_group)
                    VALUES (%(id_carrier)s,%(id_group)s)"""%{"id_carrier":id_carrier,"id_group":j}

            prestashop_api.MakeUpdateQueue(sql)

        delay = u"Срок доставки (дней): "
        if calcResult["result"]["deliveryPeriodMin"] == calcResult["result"]["deliveryPeriodMax"]:
            delay += str(calcResult["result"]["deliveryPeriodMax"])
        else:
            delay += str(calcResult["result"]["deliveryPeriodMin"]) \
                    + " - " + str(calcResult["result"]["deliveryPeriodMax"])

        delay += "."

        sql = """INSERT INTO ps_carrier_lang(id_carrier, id_shop, id_lang, delay)
                 VALUES (%(id_carrier)s,%(id_shop)s,1,'%(delay)s')"""%{"id_carrier":id_carrier,
                 "id_shop":shopId,"delay":delay}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_carrier_shop(id_carrier, id_shop)
                 VALUES (%(id_carrier)s,%(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_carrier_tax_rules_group_shop(id_carrier, id_tax_rules_group, id_shop)
                 VALUES (%(id_carrier)s, 0, %(id_shop)s)"""%{"id_carrier":id_carrier,"id_shop":shopId}

        prestashop_api.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_carrier_zone(id_carrier, id_zone)
                 VALUES (%(id_carrier)s, %(zoneId)s)"""%{"id_carrier":id_carrier, "zoneId":zoneId}

        prestashop_api.MakeUpdateQueue(sql)

        id_range_price = prestashop_api.GetLastInsertedId("ps_range_price","id_range_price") + 1

        rangesIds = []
        for k in range(0, 21):
            rangesIds.append(id_range_price + k)

        counter = 0

        for id_range_price in rangesIds:

            delimiter1 = 1000 * counter + 1

            if counter == 20:
                delimiter2 = 1000001
            else: delimiter2 = 1000 * (counter + 1)

            sql = """INSERT INTO ps_range_price(id_range_price, id_carrier, delimiter1, delimiter2)
                     VALUES (%(id_range_price)s,%(id_carrier)s,%(delimiter1)s,%(delimiter2)s)"""%{"id_range_price":id_range_price,
                     "id_carrier":id_carrier, "delimiter1":delimiter1, "delimiter2":delimiter2}

            prestashop_api.MakeUpdateQueue(sql)

            if counter == 20:
                delimiter2 = 21000

            id_delivery = prestashop_api.GetLastInsertedId("ps_delivery","id_delivery") + 1
            price = int(calcResult["result"]["price"])
            if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
                if shopId == 1 or shopId == 7:
                    price = price + delimiter2 * 0.03
                else:
                    price = price + delimiter2 * 0.06
            else:
                price = price + delimiter2 * 0.03

            price = int(10 * round(float(price)/10))

            sql = """INSERT INTO ps_delivery(id_delivery, id_shop, id_shop_group, id_carrier, id_range_price, id_range_weight, id_zone, price)
                     VALUES (%(id_delivery)s,%(id_shop)s,1,%(id_carrier)s,%(id_range_price)s,NULL,%(zoneId)s,%(price)s)"""%{"id_delivery":id_delivery,
                     "id_shop":shopId, "id_carrier":id_carrier, "id_range_price":id_range_price, "price":price, "zoneId":zoneId}

            prestashop_api.MakeUpdateQueue(sql)

            counter += 1

        filename = str(id_carrier) + ".jpg"
        shutil.copyfile(u"c:\\programming\\_PRESTASHOP\\python\\point.jpg", filename)

        session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
        session.set_pasv(False)
        session.cwd("/home/" + site["siteDir"] + "/www/img/s")
        file = open(filename,'rb')                  # file to send
        session.storbinary('STOR ' + filename, file)     # send the file
        file.close()                                    # close file and FTP
        session.quit()
        os.remove(filename)
        logging.LogInfoMessage(shopName + " " + pointName + " has been added")

def AddPageToShop(shopId, pageId):

    ps_cms_block_page_query = """INSERT INTO ps_cms_block_page(id_cms_block_page, id_cms_block, id_cms, is_category)
                                VALUES (0,%(shopId)s,%(pageId)s,0)"""%{"shopId":shopId, "pageId":pageId}

    prestashop_api.MakeUpdateQueue(ps_cms_block_page_query)

############################################################################################################################



logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "configuration")
logging = log.Log(logfile)

sdek = SdekAPI.SdekAPI()
rusPostApi = RussianPostAPI.RussianPostAPI()

resp = requests.get("http://gw.edostavka.ru:11443/pvzlist.php")
pvzXml = etree.XML(resp._content)
root =  pvzXml.getroottree().getroot()

for site in commonLibrary.sitesParams:

    if site["url"] != sys.argv[1]:
        continue

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    shops = prestashop_api.GetAllShops()

    if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com", "http://amazing-things.ru"]:
        zoneId = 9
    else:
        zoneId = 7

    if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
        topMenuValue = "CAT99,CAT96,CAT92,CAT91,CAT105,CAT95,CAT90,CAT104,CAT112,CAT114"

    if prestashop_api.siteUrl == "http://safetus.ru":
        topMenuValue = "CAT263,CAT201,CAT256,CAT303,CAT250,CAT207"

    if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
        topMenuValue = "CAT18,CAT12,CAT17,CAT13,CAT16,CAT15"

    if prestashop_api.siteUrl == "http://amazing-things.ru":
        topMenuValue = "CAT83,CAT84,CAT85,CAT94,CAT101,CAT115,CAT116,CAT117,CAT127,CAT133,CAT135,CAT136,CAT139"

    for shop in shops:

        shopId, shopDomen, shopName = shop

        if shopId < int(sys.argv[2]):
            continue

##        if shopId != 202:
##            continue

        #телефон
        ConfigurePhone(shopId, shopName)
        logging.LogInfoMessage("Phones has been configured")

        #меню
        ConfigureMenu(shopId, topMenuValue, False)
        logging.LogInfoMessage("Menu has been configured")

        #гарантии
        ConfigureReinsurance(shopId)
        logging.LogInfoMessage("Reinsurance has been configured")

        #доставка
        prestashop_api.DeleteShopsCarriers(shopId)
        AddCourierCarrier(shopId, shopName, zoneId)
        AddPointCarrier(shopId, shopName, zoneId)
        print "shop {0} configurated".format(shopId)



    ##    AddPageToShop(shopId, 172)
    ##shopCategories = prestashop_api.GetShopCategories(3)
    ##
    ##i = 0
    ##
    ##for shopCategory in shopCategories:
    ##
    ##    categoryID, link_rewrite, meta_title, description, name = shopCategory
    ##
    ##    entry = ""
    ##
    ##    if i % 3 == 0:
    ##        entry += name.lower() + u" купить в {city}, "
    ##    else:
    ##        entry = name.lower() + ", "
    ##
    ##    with open("seotext", "a") as vhostsfile:
    ##            vhostsfile.write(entry.encode("cp1251"))
    ##
    ##    i += 1

mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (changeConfiguration)", "", [logfile])