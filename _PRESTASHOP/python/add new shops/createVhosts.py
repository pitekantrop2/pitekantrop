﻿import requests
import lxml
from lxml import etree
import urllib
import json
import datetime
import sys
import re, urlparse
import PrestashopAPI
import io
import commonLibrary
import os
import log
import subprocess
import ftplib

logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "createVhosts")
logging = log.Log(logFileName)
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if site["url"] != sys.argv[1]:
        continue

##    if machineName.find("hp") != -1:
##        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)

    if prestashop_api == False:
        continue

    session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
    session.set_pasv(False)

    bChanges = False
    bHttps = commonLibrary.GetSiteUrlScheme(site["url"]) == "https://"
    if bHttps == False:
        vhostsFile = site["dbName"] + ".conf"
    else:
        vhostsFile = "ssl.conf"

    session.cwd("/etc/httpd/conf.d")
    file = open(vhostsFile,'wb')
    session.retrbinary('RETR %s' % vhostsFile, file.write)
    file.close()

    vhostsFileContent = ""
    with open(vhostsFile, "r") as vhostsfile:
        vhostsFileContent = vhostsfile.read()

    shops = prestashop_api.GetInactiveShops()

    for shop in shops:
        shopId, shopDomen, cityName = shop

        if vhostsFileContent.find("\"" + shopDomen) == -1:
            bChanges = True
            logging.LogInfoMessage("Vhosts: " + shopDomen)

            if bHttps:
                if site["url"] == "http://insect-killers.ru":
                    SSLCertificateFile = "/root/STAR_insect-killers_ru.crt"
                    SSLCertificateChainFile = "/root/STAR_insect-killers_ru.ca-bundle"

                if site["url"] == "http://otpugivateli-grizunov.ru":
                    SSLCertificateFile = "/root/STAR_otpugivateli-grizunov_ru.crt"
                    SSLCertificateChainFile = "/root/STAR_otpugivateli-grizunov_ru.ca-bundle"

                if site["url"] == "http://otpugivateli-krotov.ru":
                    SSLCertificateFile = "/root/STAR_otpugivateli-krotov_ru.crt"
                    SSLCertificateChainFile = "/root/STAR_otpugivateli-krotov_ru.ca-bundle"

                entry = """
<VirtualHost *:443>
ServerName "[domen]"
SSLEngine on
SSLCertificateFile [SSLCertificateFile]
SSLCertificateKeyFile /root/private.key
SSLCertificateChainFile [SSLCertificateChainFile]
	DocumentRoot "/home/[siteDir]/www"
	<Directory /home/[siteDir]/www>
		Options FollowSymLinks
		AllowOverride All
	</Directory>

	ServerAdmin admin@localserver12.ru
    	php_admin_value short_open_tag on
	ErrorLog /home/[siteDir]/logs/error.log
</VirtualHost>""".replace("[domen]", shopDomen).replace("[siteDir]", site["siteDir"]).replace("[SSLCertificateFile]", SSLCertificateFile).replace("[SSLCertificateChainFile]", SSLCertificateChainFile)
                with open(vhostsFile, "a") as vhostsfile:
                    vhostsfile.write(entry.encode("cp1251") + "\n")

            else:#не https
                entry = """
<VirtualHost *:80>
	ServerName "[domen]"
	ServerAlias "[domen]" "www.[domen]"

	DocumentRoot "/home/[siteDir]/www"
	<Directory /home/[siteDir]/www>
		Options FollowSymLinks
		AllowOverride All
	</Directory>

	ServerAdmin admin@localserver12.ru
    	php_admin_value short_open_tag on
	ErrorLog /home/[siteDir]/logs/error.log

</VirtualHost>""".replace("[domen]", shopDomen).replace("[siteDir]", site["siteDir"])
            with open(vhostsFile, "a") as vhostsfile:
                vhostsfile.write(entry.encode("cp1251") + "\n")

    if bChanges:
        session.delete(vhostsFile)
        file = open(vhostsFile,'rb')
        session.storbinary('STOR ' + vhostsFile, file)
        file.close()
        os.remove(vhostsFile)

    session.close()

