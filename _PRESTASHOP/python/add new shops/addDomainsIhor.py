﻿import myLibrary
import UIhorObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import commonLibrary
import sys
import subprocess
import SendMails

def Login(login, password):
    Driver.OpenUrl(baseUrl + "/dnsmgr?func=logon")
    Driver.WaitFor(UIhorObjects.UIhorObjects.Login)
    Driver.SendKeys(UIhorObjects.UIhorObjects.Login, login)
    Driver.SendKeys(UIhorObjects.UIhorObjects.Password, password)
    Driver.Click(UIhorObjects.UIhorObjects.Submit)
    Driver.WaitFor(UIhorObjects.UIhorObjects.MainLink)


def AddSubdomain(name):

    if Driver.IsTextPresent(name) == True:
        return

    while Driver.IsVisible(UIhorObjects.UIhorObjects.MasterRadio) == False:
        Driver.Click(UIhorObjects.UIhorObjects.AddDomainButton)
        time.sleep(1)

    Driver.WaitFor(UIhorObjects.UIhorObjects.MasterRadio)
    Driver.Click(UIhorObjects.UIhorObjects.MasterRadio)
    Driver.SendKeys(UIhorObjects.UIhorObjects.DomainName, name)
    Driver.SendKeys(UIhorObjects.UIhorObjects.IpAddress, ipAddress)
    Driver.Click(UIhorObjects.UIhorObjects.OkButton)
    time.sleep(1)
    if Driver.IsTextPresent("уже существует"):
        Driver.Click(UIhorObjects.UIhorObjects.CancelButton)
    Driver.WaitFor(UIhorObjects.UIhorObjects.AddDomainButton)
    time.sleep(2)

################################################################################



logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "addDomainsIhor")

baseUrl = "https://dns-master.marosnet.net"
logging = log.Log(logFileName)

Driver = myLibrary.Selenium()
Driver.SetUp()
Login("ih39655", "szNyOObiUNFJB3qD")

Driver.Click(UIhorObjects.UIhorObjects.MainLink)
Driver.WaitFor(UIhorObjects.UIhorObjects.DomainsNamesLink)
Driver.Click(UIhorObjects.UIhorObjects.DomainsNamesLink)
Driver.WaitFor(UIhorObjects.UIhorObjects.AddDomainButton)

for site in commonLibrary.sitesParams:
    logging.Log(sys.argv[1])

    if site["url"] != sys.argv[1]:
        continue

    logging.Log("==================================  " + site["url"] + "  ==================================")

    prestashop_api = commonLibrary.GetPrestashopInstance(site)

    if prestashop_api == False:
##        ssh.kill()
        continue

    ipAddress = site["ip"]

    shops = prestashop_api.GetInactiveShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop

        try:
            AddSubdomain(shopDomen)
            logging.LogInfoMessage(shopDomen + " has been added")
        except Exception, e:
            logging.LogErrorMessage(shopDomen + " hasn't been added")


Driver.CleanUp()

mails = SendMails.SendMails()
mails.SendInfoMail("Добавление магазинов (addDomainsIhor)", "", [logFileName])