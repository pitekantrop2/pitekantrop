﻿import myLibrary
import UPrestaObjects
import datetime
import time, threading
import urllib
import shutil
import requests
import log
import PrestashopAPI
import requests
from lxml import etree
import commonLibrary
import SendMails
import subprocess
import ftplib
import os
import sys

prestashop_api = PrestashopAPI.PrestashopAPI()

def AddShop(ruName):
    shops = prestashop_api.GetShops()
    shopsUrls = prestashop_api.GetShopsUrls()
    id_shop = max([x[1] for x in shopsUrls]) + 1

    sql = u"""INSERT INTO ps_shop(id_shop, id_shop_group, name, id_category, id_theme, active, deleted)
              VALUES ({0},{1},'{2}',{3},{4},0,0)""".format(id_shop, shops[0][1], ruName, shops[0][3],shops[0][4])
    prestashop_api.MakeUpdateQueue(sql)

    if bSubFolders:
        folder = transliteratedName + "/"
        sql = u"""INSERT INTO ps_shop_url( id_shop, domain, domain_ssl, physical_uri, virtual_uri, main, active)
                  VALUES ({0},'{1}','{1}','/','{2}',1,1)""".format(id_shop, shopsUrls[0][2], folder)
    else:
        domain = transliteratedName + "." + commonLibrary.GetShopMainDomain( shopsUrls[0][2])
        sql = u"""INSERT INTO ps_shop_url( id_shop, domain, domain_ssl, physical_uri, virtual_uri, main, active)
                  VALUES ({0},'{1}','{1}','/','',1,0)""".format(id_shop, domain)

    prestashop_api.MakeUpdateQueue(sql)

    return id_shop

def AddRecordsToTables():
    #ps_category_lang
    for row in ps_category_lang:
        description = commonLibrary.GetStringDescription(commonLibrary.ReplaceCityInText(templateShopName, newCity, row[4]))
        long_description = commonLibrary.GetStringDescription(commonLibrary.ReplaceCityInText(templateShopName, newCity, row[5]))
        link_rewrite = row[7].replace(u"tomsk", transliteratedName)
        meta_title = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[8])
        meta_keywords = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[9])
        meta_description = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[10])
        sql = u"""INSERT INTO ps_category_lang(id_category, id_shop, id_lang, name, description, long_description, articles, link_rewrite, meta_title, meta_keywords, meta_description)
        VALUES ({0},{1},{2},'{3}','{4}','{5}','','{6}','{7}','{8}','{9}')""".format(row[0], id_shop, row[2], row[3],description, long_description, link_rewrite, meta_title, meta_keywords, meta_description )
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_category_lang")

    #ps_category_shop
    for row in ps_category_shop:
        sql = u"""INSERT INTO ps_category_shop(id_category, id_shop, position)
            VALUES ({0},{1},1)""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_category_shop")

    #ps_cms_block_shop
    for row in ps_cms_block_shop:
        sql = u"""INSERT INTO ps_cms_block_shop(id_cms_block, id_shop)
            VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_cms_block_shop")

    #ps_cms_shop
    for row in ps_cms_shop:
        params = prestashop_api.GetPageParams(row[0])
        if params[6].find(templateShopName) != -1:
            continue
        sql = u"""INSERT INTO ps_cms_shop(id_cms, id_shop)
            VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_cms_shop")

    #ps_configuration
    for row in ps_configuration:
        sql = u"""INSERT INTO ps_configuration( id_shop_group, id_shop, name, value, date_add, date_upd)
                 VALUES ({0},{1},'{2}','{3}','{4}','{5}')""".format(row[1], id_shop, row[3], row[4], row[5], row[6])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_configuration")

    #ps_country_shop
    for row in ps_country_shop:
        sql = u"""INSERT INTO ps_country_shop(id_country, id_shop)
                 VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_country_shop")

    #ps_currency_shop
    for row in ps_currency_shop:
        sql = u"""INSERT INTO ps_currency_shop(id_currency, id_shop, conversion_rate)
                 VALUES ({0},{1},{2})""".format(row[0], id_shop, row[2])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_currency_shop")

    #ps_employee_shop
    for row in ps_employee_shop:
        sql = u"""INSERT INTO ps_employee_shop(id_employee, id_shop)
                 VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_employee_shop")

    #ps_feature_shop
    for row in ps_feature_shop:
        sql = u"""INSERT INTO ps_feature_shop(id_feature, id_shop)
                 VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_feature_shop")

    #ps_group_shop
    for row in ps_group_shop:
        sql = u"""INSERT INTO ps_group_shop(id_group, id_shop)
                 VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_group_shop")

    #ps_hook_module
    for row in ps_hook_module:
        sql = u"""INSERT INTO ps_hook_module(id_module, id_shop, id_hook, position)
                 VALUES ({0},{1}, {2},{3})""".format(row[0], id_shop, row[2], row[3])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_hook_module")

    #ps_image_shop
    for row in ps_image_shop:
        sql = u"""INSERT INTO ps_image_shop(id_image, id_shop, cover)
                 VALUES ({0},{1}, {2})""".format(row[0], id_shop, row[2])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_image_shop")

    #ps_info
    for row in ps_info:
        sql = u"""SELECT text FROM ps_info_lang
                WHERE id_info = {0}""".format(row[0])
        text = commonLibrary.ReplaceCityInText(templateShopName, newCity, prestashop_api.MakeGetInfoQueue(sql)[0][0])
        sql = u"""INSERT INTO ps_info_lang( id_lang, text)
                  VALUES (1,'{0}')""".format(text)
        prestashop_api.MakeUpdateQueue(sql)
        id_info = prestashop_api.GetLastInsertedId("ps_info_lang", "id_info")
        sql = u"""INSERT INTO ps_info(id_info, id_shop)
                  VALUES ({0},{1})""".format(id_info, id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_info")

    #ps_iqitmegamenu_tabs_shop
    for row in ps_iqitmegamenu_tabs_shop:
        sql = u"""INSERT INTO ps_iqitmegamenu_tabs_shop(id_tab, id_shop)
                  VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_iqitmegamenu_tabs_shop")

    #ps_lang_shop
    for row in ps_lang_shop:
        sql = u"""INSERT INTO ps_lang_shop(id_lang, id_shop)
                 VALUES (1,{0})""".format(id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_lang_shop")

    #ps_layered_category
    for row in ps_layered_category:
        if row[3] == None:
            value = "NULL"
        else:
            value = row[3]
        sql = u"""INSERT INTO ps_layered_category( id_shop, id_category, id_value, type, position, filter_type, filter_show_limit)
                 VALUES ({0},{1},{2},'{3}',{4},{5},{6})""".format(id_shop, row[2],value,row[4],row[5],row[6],row[7])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_layered_category")

    #ps_layered_filter_shop
    for row in ps_layered_filter_shop:
        sql = u"""INSERT INTO ps_layered_filter_shop(id_layered_filter, id_shop)
                  VALUES ({0},{1})""".format(row[0], id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_layered_filter_shop")

    #ps_layered_price_index
    for row in ps_layered_price_index:
        sql = u"""INSERT INTO ps_layered_price_index(id_product, id_currency, id_shop, price_min, price_max)
                  VALUES ({0},{1},{2},{3},{4})""".format(row[0], row[1], id_shop, row[3], row[4])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_layered_price_index")

    #ps_meta_lang
    for row in ps_meta_lang:
        title = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[3])
        description = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[4])
        keywords = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[5])
        sql = u"""INSERT INTO ps_meta_lang(id_meta, id_shop, id_lang, title, description, keywords, url_rewrite)
                  VALUES ({0},{1},{2},'{3}','{4}','{5}','{6}')""".format(row[0], id_shop, row[2],title,description,keywords,row[6])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_meta_lang")

   #ps_module_country
    for row in ps_module_country:
        sql = u"""INSERT INTO ps_module_country(id_module, id_shop, id_country)
                  VALUES ({0},{1},{2})""".format(row[0], id_shop, row[2])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_module_country")

    #ps_module_currency
    for row in ps_module_currency:
        sql = u"""INSERT INTO ps_module_currency(id_module, id_shop, id_currency)
                  VALUES ({0},{1},{2})""".format(row[0], id_shop, row[2])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_module_currency")

    #ps_module_group
    for row in ps_module_group:
        sql = u"""INSERT INTO ps_module_group(id_module, id_shop, id_group)
                 VALUES ({0},{1},{2})""".format(row[0], id_shop, row[2])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_module_group")

    #ps_module_shop
    for row in ps_module_shop:
        sql = u"""INSERT INTO ps_module_shop(id_module, id_shop, enable_device)
                  VALUES ({0},{1},{2})""".format(row[0], id_shop,row[2])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_module_shop")

    #ps_product_lang
    for row in ps_product_lang:
        link_rewrite = row[5].replace("labinsk", transliteratedName)
        meta_description = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[6])
        meta_keywords = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[7])
        meta_title = commonLibrary.ReplaceCityInText(templateShopName, newCity, row[8])
        description = commonLibrary.GetStringDescription(row[3])
        sql = u"""INSERT INTO ps_product_lang(id_product, id_shop, id_lang, description, description_short, link_rewrite, meta_description, meta_keywords, meta_title, name, original_name, available_now, available_later, video, characteristics, equipment)
                    VALUES ({0},{1},{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}')""".format(row[0], id_shop, row[2], description, row[4], link_rewrite, meta_description, meta_keywords, meta_title, row[9],row[10],row[11],row[12],row[13],row[14],row[15]  )
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_product_lang")

    #ps_product_media
    try:
        for row in ps_product_media:
            sql = u"""INSERT INTO ps_product_media(id_product, id_shop, label, url_media, type)
            VALUES ({0},{1},'{2}','{3}','{4}')""".format(row[1], id_shop, row[3],row[4],row[5])
            prestashop_api.MakeUpdateQueue(sql)
        logging.Log("ps_product_media")
    except:
        pass

    #ps_product_shop
    for row in ps_product_shop:
        sql = u"""INSERT INTO ps_product_shop(id_product, id_shop, id_category_default, id_tax_rules_group, on_sale, online_only, ecotax, minimal_quantity, price, wholesale_price, unity, unit_price_ratio, additional_shipping_cost, customizable, uploadable_files, text_fields, active, redirect_type, id_product_redirected, available_for_order, available_date, `condition`, show_price, indexed, visibility, cache_default_attribute, advanced_stock_management, date_add, date_upd)
                 VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},'{10}',{11},{12},{13},{14},'{15}','{16}','{17}',{18},{19},'{20}','{21}',{22},{23},'{24}',{25},{26},'{27}','{28}')""" \
                 .format(row[0], id_shop, row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], row[19], row[20], row[21], row[22], row[23], row[24], row[25], row[26], row[27], row[28] )
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_product_shop")

    #ps_stock_available
    for row in ps_stock_available:
        sql = u"""INSERT INTO ps_stock_available(id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)
                 VALUES ({0},{1},{2},{3},{4},{5},{6})""".format( row[1], row[2], id_shop, row[4], row[5], row[6], row[7])
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_stock_available")

    #ps_tax_rules_group_shop
    for row in ps_tax_rules_group_shop:
        sql = u"""INSERT INTO ps_tax_rules_group_shop(id_tax_rules_group, id_shop)
                  VALUES ({0},{1})""".format(row[0],id_shop )
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_tax_rules_group_shop")

    #ps_webservice_account_shop
    for row in ps_webservice_account_shop:
        sql = u"""INSERT INTO ps_webservice_account_shop(id_webservice_account, id_shop)
                 VALUES ({0},{1})""".format(row[0],id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_webservice_account_shop")

    #ps_zone_shop
    for row in ps_zone_shop:
        sql = u"""INSERT INTO ps_zone_shop(id_zone, id_shop)
                  VALUES ({0},{1})""".format(row[0],id_shop)
        prestashop_api.MakeUpdateQueue(sql)
    logging.Log("ps_zone_shop")


##################################################################################################################################

logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\add new shops\\log", "shops")
logging = log.Log(logFileName)
mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

templateShopName = u"Лабинск"

sdekCities = [x[0] for x in commonLibrary.GetCitiesTable()]

myset = set(sdekCities)
uniqueSdekCities = list(myset)

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1 or machineName.find("pitekantrop") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    sourceCss = "iqitmegamenu_s_1.css"

    templateShopId = prestashop_api.GetShopIdByName(templateShopName)
    ps_category_lang = prestashop_api.GetTableData("ps_category_lang", templateShopId)
    ps_category_shop = prestashop_api.GetTableData("ps_category_shop", templateShopId)
    ps_cms_block_shop = prestashop_api.GetTableData("ps_cms_block_shop", templateShopId)
    ps_cms_shop = prestashop_api.GetTableData("ps_cms_shop", templateShopId)
    ps_configuration = prestashop_api.GetTableData("ps_configuration", templateShopId)
    ps_country_shop = prestashop_api.GetTableData("ps_country_shop", templateShopId)
    ps_currency_shop = prestashop_api.GetTableData("ps_currency_shop", templateShopId)
    ps_employee_shop = prestashop_api.GetTableData("ps_employee_shop", templateShopId)
    ps_feature_shop = prestashop_api.GetTableData("ps_feature_shop", templateShopId)
    ps_group_shop = prestashop_api.GetTableData("ps_group_shop", templateShopId)
    ps_hook_module = prestashop_api.GetTableData("ps_hook_module", templateShopId)
    ps_image_shop = prestashop_api.GetTableData("ps_image_shop", templateShopId)
    ps_info = prestashop_api.GetTableData("ps_info", templateShopId)
    ps_iqitmegamenu_tabs_shop = prestashop_api.GetTableData("ps_iqitmegamenu_tabs_shop", templateShopId)
    ps_lang_shop = prestashop_api.GetTableData("ps_lang_shop", templateShopId)
    ps_layered_category = prestashop_api.GetTableData("ps_layered_category", templateShopId)
    ps_layered_filter_shop = prestashop_api.GetTableData("ps_layered_filter_shop", templateShopId)
    ps_layered_price_index = prestashop_api.GetTableData("ps_layered_price_index", templateShopId)
    ps_meta_lang = prestashop_api.GetTableData("ps_meta_lang", templateShopId)
    ps_module_country = prestashop_api.GetTableData("ps_module_country", templateShopId)
    ps_module_currency = prestashop_api.GetTableData("ps_module_currency", templateShopId)
    ps_module_group = prestashop_api.GetTableData("ps_module_group", templateShopId)
    ps_module_shop = prestashop_api.GetTableData("ps_module_shop", templateShopId)
    ps_product_lang = prestashop_api.GetTableData("ps_product_lang", templateShopId)
    ps_product_media = prestashop_api.GetTableData("ps_product_media", templateShopId)
    ps_product_shop = prestashop_api.GetTableData("ps_product_shop", templateShopId)
    ps_stock_available = prestashop_api.GetTableData("ps_stock_available", templateShopId)
    ps_tax_rules_group_shop = prestashop_api.GetTableData("ps_tax_rules_group_shop", templateShopId)
    ps_webservice_account_shop = prestashop_api.GetTableData("ps_webservice_account_shop", templateShopId)
    ps_zone_shop = prestashop_api.GetTableData("ps_zone_shop", templateShopId)

    logging.Log("==================================  " + site["url"] + "  ==================================")

    bSubFolders = prestashop_api.siteUrl in commonLibrary.GetSubfoldersSites()

    shops = prestashop_api.GetAllShops()
    shopsNames = [x[2].lower() for x in shops]

    for newCity in uniqueSdekCities:
        if newCity.lower() in shopsNames or commonLibrary.GetCityDeclension(newCity) == -1 or commonLibrary.excludedCities.find(newCity) != -1:
            continue

##        newCity = u"Красноперекопск"
        logging.Log("===============" + newCity + "===============")
##        print newCity
        transliteratedName = prestashop_api.Transliterate(newCity).replace(" ","-").lower()

        id_shop = AddShop(newCity)
        logging.LogInfoMessage("urls for {0} ({1}) has been added".format(transliteratedName, id_shop))

        AddRecordsToTables()


        ################## добавление поддоменов в dns #############################
        if bSubFolders == False:
            if site["url"] in [ "http://safetus.ru", "http://otpugiwatel.com", "http://amazing-things.ru"]:
                process = subprocess.Popen(["python", "addDomainsIhor.py", site["url"]])
            else:
                process = subprocess.Popen(["python", "addSubdomainsRegRu.py", site["url"]])
            process.wait()

        #добавление в vhosts и .htaccess
        if bSubFolders == False:
##            process = subprocess.Popen(["python", "add new shops\\createVhosts.py", site["url"]])
            process = subprocess.Popen(["python", "createVhosts.py", site["url"]])
            process.wait()

        else:
            process = subprocess.Popen(["python", "createHtaccess.py", site["url"]])
            process.wait()


        ##################### конфигурирование ############################

        #mega menu
        session = ftplib.FTP(site["ip"], 'FTP_user', 'user_FTP123')
        session.set_pasv(False)
        destCss = "iqitmegamenu_s_{0}.css".format(id_shop)
        path = "/home/{0}/www/modules/iqitmegamenu/views/css".format(site["siteDir"])
        session.cwd(path)
        if os.path.isfile(sourceCss) == False:
            file = open(sourceCss,'wb')
            session.retrbinary('RETR %s' % sourceCss, file.write)
            file.close()
        file = open(sourceCss,'rb')                      # file to send
        session.storbinary('STOR {0}'.format(destCss), file)     # send the file
        file.close()
        session.close()

##        #страницы "О компании"
##        process = subprocess.Popen(["python", "add new shops\\createAboutCompanyPages.py", site["url"], str(startShopId)])
##        process.wait()


        prestashop_api.SetShopActive(id_shop, 1)
        prestashop_api.SetShopDomainActive(id_shop, 1)

        os.remove(sourceCss)
    if machineName.find("hp") != -1 or machineName.find("pitekantrop") != -1:
        ssh.kill()

##Driver.CleanUp()