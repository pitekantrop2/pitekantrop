﻿import PrestashopAPI
import ContentmonsterApi
import SendMails
import log
import commonLibrary
import TextruAPI
import lxml
from lxml import etree
import subprocess
import datetime

template = u"""Здравствуйте!

Требуется статья о категории товаров "[cat]" ( [url] ).

Статья может содержать:[themes]

Условия:
1) наличие заголовка
2) текст должен быть разбит на 2-3 части
3) должен присутствовать хотя бы один список
4) уникальность 100% в сервисе text.ru (прошу прикладывать скриншот с результом проверки).

С уважением,
Иван"""

def GetOrderDescription(category):
    themes = u"""
- описание принципов действия/особенностей работы устройств
- рекомендации по использованию приборов
- описание/сравнение наиболее популярных марок
- рекомендации по подбору подходящей модели
"""
    if category.lower().find(u"отпугива") != -1 or category.lower().find(u"уничтож") != -1:
        themes += u"- сравнение с другими способами борьбы с вредителями"
    url = prestashop_api.GetSmartBlogCategoryUrl(category)
    descr = template.replace("[cat]", category).replace("[themes]", themes).replace("[url]", url)
    return descr

def GetNextCategory(category):
    catNames = prestashop_api.GetSmartBlogCategoriesNames()
    try:
        catNames.remove(u"Общая информация")
        catNames.remove(u"Универсальные отпугиватели")
    except:
        pass
    for i in range(0, len(catNames)):
        if catNames[i] == category:
            if i + 1 <= len(catNames) - 1:
                return catNames[i+1]
            else:
                return catNames[0]

def GetDataForOrder(category):
    data = {}
    data["site_id"] = contentmonster_api.GetSiteId(prestashop_api.siteUrl)
    data["task"] = 1
    data["subject_id"] = 30
    data["write_time_limit"] = 48
    data["len_min"] = 2400
    data["len_max"] = 3000
    data["write_pay"] = 100
    data["wmtype"] = 1
    data["uniq_min"] = 100
    data["name"] = u"Статья о категории товаров \"{0}\"".format(category)
    data["description"] = GetOrderDescription(category)
    data["tender_type"] = 2
    data["tender_group"] = 32050
    data["send"] = 1
    return data


###############################################################################

contentmonster_api = ContentmonsterApi.ContentmonsterApi()
prestashop_api = PrestashopAPI.PrestashopAPI()
textru_api = TextruAPI.TextruAPI()
mails = SendMails.SendMails()
todayDate = datetime.date.today()

orders = prestashop_api.GetContentmonsterArticlesOrders()
message = ""

#обновление статусов существующих заказов
for order in orders:
    id, site, category, theme, orderId, state, text, date = order
    if orderId in [None, ""]:
        continue
    if state in [u"проверен", u"добавлен", u"закончен"]:
        continue
    orderState = contentmonster_api.GetOrderStatus(orderId)
    if state != orderState:
        prestashop_api.SetContentmonsterArticleState(orderId, orderState)
        message += "Изменен статус заказа {0} на '{1}'\n".format(orderId, orderState.encode("utf-8"))
        if orderState == u"закончен":
            orderText = contentmonster_api.GetOrderText(orderId)
            orderText = orderText.replace("'", "")
            prestashop_api.SetContentmonsterArticleText(orderId, orderText)
            message += "Изменен текст заказа {0}\n".format(orderId)


#создание новых заказов
orders = prestashop_api.GetContentmonsterArticlesOrders()
for order in orders:
    id, site, category, theme, orderId, state, text, date = order
    numOfSiteOrders = len([x for x in orders if x[1] == site])
    pastDays = (todayDate - date).days
    if orderId in [None, ""] or (pastDays > 6 and numOfSiteOrders == 1):#если новый домен или прошло более 6 дней
        prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site)
    if orderId in [None, ""]:#если новый домен
        data = GetDataForOrder(category)
        orderId = contentmonster_api.CreateOrder(data)
        if orderId != -1:
            prestashop_api.SetContentmonsterArticleOrderId(id, orderId)
            message += "Создан заказ {0} по записи {1}\n".format(orderId, id)
        else:
            message += "[Error] Ошибка создания заказа по записи {0}\n".format(id )
    else:#если нужно добавить следующий заказ
        if pastDays > 6 and numOfSiteOrders == 1:
            nextCategory = GetNextCategory(category)
            data = GetDataForOrder(nextCategory)
            orderid = contentmonster_api.CreateOrder(data)
            if orderid != -1:
                data = {}
                data["site"] = site
                data["cityId"] = cityId
                data["cityName"] = shopName
                data["orderId"] = orderid
                data["categories"] = categories
                prestashop_api.AddContentmonsterOrder(data)
                message += "Создан заказ для категории {0} по записи {1}\n".format(nextCategory.encode("utf-8"), id)
            else:
                message += "[Error] Ошибка создания заказа по записи {0}\n".format(id )

            prestashop_api.DeleteContentmonsterOrder(orderId)

    if 'ssh' in locals():
        ssh.kill()


if message != "":
    result = mails.SendInfoMail("Обработка contentmonster_articles", message)
    if result != True:
        logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
