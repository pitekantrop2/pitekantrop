﻿import log
import datetime
import sys
import PrestashopAPI
import io
import subprocess
import time
import commonLibrary
import xlrd
import xlwt
import SendMails

states = [
u"Ожидание платежа по квитанции",
u"В процессе подготовки",
u"Отправлен",
u"Доставлен",
u"Ошибка оплаты",
u"Данного товара нет на складе",
u"В ожидании оплаты банком"
]

notInStates = [
#u"Отклонен",
u"Возврат денег"
]

def GetCoefficient(contractor):
    if contractor == u"Логос":
        return float(1.15)
    elif contractor in [u"Сититек", u"Телесистемы", u"Кварц", u"Торнадо",  u"ЮСТ", u"Биос", u"Алекс"]:
        return float(1.2)
    elif contractor == u"Чистон":
        return float(1.3)
    else:
        return float(1)

def WarehouseProductsCosts():
    allItems = {}

    for site in commonLibrary.sitesParams:
        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
            try:
                ssh.kill()
            except:
                pass
            continue

        products = prestashop_api.GetShopProducts(10)

        for product in products:

            productId = product[0]
            name = prestashop_api.CleanItemName(prestashop_api.GetProductOriginalName(productId))
            productCost = prestashop_api.GetProductDetailsById(productId)[2]
            if name not in allItems.keys():
                allItems[name] = productCost

        if machineName.find("hp") != -1:
            ssh.kill()

    site = sites[0]
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))
    prestashop_api = commonLibrary.GetPrestashopInstance(site)

    warehouseProducts = prestashop_api.GetWarehouseProducts("warehouse_moscow")
    warehouseProductsCosts = 0

    for warehouseProduct in warehouseProducts:
        if warehouseProduct[1] in allItems.keys():
            warehouseProductsCosts += allItems[warehouseProduct[1]]

    if machineName.find("hp") != -1:
        ssh.kill()

    print "===================="
    print warehouseProductsCosts

def GetSDEKArrears():
    states = [
    u"Вручен"
    ]

    endDate = datetime.date.today() + datetime.timedelta(days=1)
    startDate = endDate - datetime.timedelta(days=90)

    itemsProfit = 0
    numOfOrders = 0

    for site in commonLibrary.sitesParams:
        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
            try:
                ssh.kill()
            except:
                pass
            continue

        orders = prestashop_api.GetOrdersIds(states, startDate, endDate)
        numOfOrders += len(orders)

        for order in orders:

            orderId = order[0]
            orderCarrierName = prestashop_api.GetOrderCarrierName(orderId)
            if orderCarrierName.find(u"очтой") != -1:
                continue

            orderItems = prestashop_api.GetOrderItems(orderId)

            #статистика по товарам
            for item in orderItems:

                product_quantity = item[3]
                unit_price_tax_incl = item[2]
                purchase_supplier_price = item[4]

                itemProfit = product_quantity * unit_price_tax_incl
                itemsProfit += itemProfit

        if machineName.find("hp") != -1:
            ssh.kill()

    print "=============="
    print itemsProfit * 0.97
    print numOfOrders


def GetStatistics(dateStart, dateStop, products = []):
    items = []
    shops = []
    contractors = []
    carriers = []
    payments =[]
    shopsProfits = []
    notDeliveredOrders = 0
    legalEntityProfit = 0

    for site in commonLibrary.sitesParams:
        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            continue

        orders = prestashop_api.GetOrdersIds(notInStates , dateStart, dateStop, True)

        #суммарный зароботок магазина
        shopProfit = 0

        for order in orders:
            orderId = order[0]
            orderState  = prestashop_api.GetOrderState(orderId)
            orderStates  = prestashop_api.GetOrderStates(orderId)

            if orderState == u"Отклонен":
                if len(orderStates) > 2:
                    notDeliveredOrders += 1
                continue

            if orderState in [u"Ожидание платежа по квитанции", u"В ожидании оплаты банком"]:
                continue

            orderItems = prestashop_api.GetOrderItems(orderId)
            orderShop = prestashop_api.GetOrderShopName(orderId)
            if orderShop == -1:
                continue
            note = prestashop_api.GetOrderNote(orderId)
            orderCarrierName = prestashop_api.GetOrderCarrierName(orderId)
            orderPaymentMethod = prestashop_api.GetOrderPaymentMethod(orderId)
            bNovallOrder = commonLibrary.GetNovallOrder(note)
            discount = commonLibrary.GetDiscount(note)

            #статистика по товарам
            for item in orderItems:
                itemContractor = commonLibrary.GetContractorName( prestashop_api.GetProductArticle(item[0]))
                itemId = item[0]
                productName = prestashop_api.GetProductOriginalName(itemId)
                if productName == -1:
                    productName = item[1]
                product_quantity = item[3]
                unit_price_tax_incl = item[2]
                if discount != 0:
                    unit_price_tax_incl = prestashop_api.GetProductDetailsById(itemId)[1]
                    unit_price_tax_incl = int(float(unit_price_tax_incl) * float(1 - discount/float(100)))
                purchase_supplier_price = item[4]

                product_name = productName

                if productName.find(u"«") != -1:
                    product_name = productName[productName.index(u"«") + 1:productName.index(u"»")]

                if productName.find(u"\"") != -1:
                    product_name = productName.split(u"\"")[-2]

                if len(products) > 0:
                    if product_name not in products:
                        continue

                itemProfit = product_quantity * (unit_price_tax_incl - purchase_supplier_price)
                itemRevenue = product_quantity * unit_price_tax_incl
                shopProfit += itemProfit

                if orderPaymentMethod != u"Оплата наличными при получении":
                    legalEntityProfit += itemProfit

                itemIndex = -1

                try:
                    itemIndex = [x[0] for x in items].index(product_name)
                except:
                    pass

                if itemIndex != -1:
                    items[itemIndex][1] += product_quantity
                    items[itemIndex][2] += itemRevenue
                    items[itemIndex][3] += itemProfit
                else:
                    itemTuple = [product_name, product_quantity, itemRevenue, itemProfit]
                    items.append(itemTuple)

                #поставщик
                if itemContractor == -1:
                    continue

                itemIndex = -1

                try:
                    itemIndex = [x[0] for x in contractors].index(itemContractor)
                except:
                    pass

                if itemIndex != -1:
                    contractors[itemIndex][1] += product_quantity * purchase_supplier_price
                    contractors[itemIndex][2] += itemProfit
                else:
                    itemTuple = [itemContractor, product_quantity * purchase_supplier_price, itemProfit]
                    contractors.append(itemTuple)


            shopIndex = -1

            orderShop = commonLibrary.GetShopName(orderShop)

            #статистика по городам
            try:
                shopIndex = [x[0] for x in shops].index(orderShop)
            except:
                pass

            if shopIndex != -1:
                shops[shopIndex][1] += 1
            else:
                shopElement = [orderShop, 1]
                shops.append(shopElement)

            #статистика по способам доставки

            if orderCarrierName != -1:
                if orderCarrierName.find(u"пункт") != -1:
                    orderCarrierName = u"На пункты выдачи"

                carrierIndex = -1
                try:
                    carrierIndex = [x[0] for x in carriers].index(orderCarrierName)
                except:
                    pass

                if carrierIndex != -1:
                    carriers[carrierIndex][1] += 1
                else:
                    carrierElement = [orderCarrierName, 1]
                    carriers.append(carrierElement)

            #статистика по способам оплаты
            if orderPaymentMethod == -1:
                orderPaymentMethod = u"Другой способ"

            paymentIndex = -1
            try:
                paymentIndex = [x[0] for x in payments].index(orderPaymentMethod)
            except:
                pass

            if paymentIndex != -1:
                payments[paymentIndex][1] += 1
            else:
                paymentEntry = [orderPaymentMethod, 1]
                payments.append(paymentEntry)

        shopsProfits.append([site["url"], shopProfit])

        if machineName.find("hp") != -1:
            ssh.kill()

    dateStart = dateStart.split(" ")[0]
    dateStop = dateStop.split(" ")[0]

    logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "statistics (" + dateStart + " - " + dateStop + ")")
    logging = log.Log(logfile)

    sortedItems = []

    sortedItems = sorted(items, key=lambda item: item[3])

    for i in range(len(sortedItems) - 1, -1, -1):
        logging.Log(str(int(sortedItems[i][3])) + "   " + sortedItems[i][0] + " (" + str(sortedItems[i][1] ) + u" шт.)")

    #оборот
    totalProfit = int(sum(x[2] for x in sortedItems))

    #доход
    rev = int(sum(x[3] for x in sortedItems))

    marginPercent = int(float(rev) / float(totalProfit) * 100)

    #3% налогов
    tax = float( sum(x[1] for x in shopsProfits) * 3) / float(marginPercent)

    #прибыль
    realProfit = int(rev - totalProfit * 0.04 - tax)

    sortedCarriers = sorted(carriers, key=lambda carriers: carriers[1])
    ordersNum = sum(x[1] for x in sortedCarriers)

    logging.Log(u"====================== ФИНАНСЫ ======================")
    logging.Log(u"Оборот: " + str(totalProfit) + u" рублей")
    logging.Log(u"Доход: " + str(realProfit) + u" рублей")
    logging.Log(u"Средняя маржа: " + str(marginPercent) + u"%")
    logging.Log(u"Cредний чек: {0}".format( int(float(totalProfit) / float(ordersNum))))

    logging.Log(u"====================== ПРИБЫЛЬ ПО САЙТАМ ======================")
    sortedShopsProfits = sorted(shopsProfits, key=lambda shopsProfits: shopsProfits[1])
    for entry in reversed(sortedShopsProfits):

        shopProfit = int(entry[1])
        percent = float(shopProfit) / float(rev) * 100
        logging.Log( entry[0] + ": " + str(shopProfit) + " (" + str(int(percent)) + "%)")

    logging.Log(u"====================== ПОСТАВЩИКИ ======================")
    sortedContractors = sorted(contractors, key=lambda contractors: contractors[2])
    for entry in reversed(sortedContractors):

        logging.Log( u"{0}: оборот {1} руб., доход {2} руб.".format(entry[0], int(entry[1]), int(entry[2])))

    logging.Log(u"====================== СПОСОБЫ ДОСТАВКИ ======================")

    for entry in reversed(sortedCarriers):

        logging.Log(entry[0] + ": " + str(int(float(entry[1]) / float(ordersNum) * 100)) + "%")

    logging.Log(u"====================== СПОСОБЫ ОПЛАТЫ ======================")
    sortedPayments = sorted(payments, key=lambda payments: payments[1])
    for entry in reversed(sortedPayments):

        logging.Log(entry[0] + ": " + str(int(float(entry[1]) / float(ordersNum) * 100)) + "%")

    logging.Log(u"====================== ПРОЧЕЕ ======================")
    logging.Log(u"% возвратов: {0}".format(int ( float(notDeliveredOrders)/float(notDeliveredOrders + ordersNum) * 100)))
    legalEntityPercentage = int (float(legalEntityProfit)/float(totalProfit) * 100)
    logging.Log(u"% покупок юрлицами {0}, физлицами {1}".format(legalEntityPercentage, 100 - legalEntityPercentage))

    logging.Log(u"====================== ГОРОДА ======================")
    sortedShops = sorted(shops, key=lambda shop: shop[1])
    for entry in reversed(sortedShops):
        logging.Log(u"{0}\t{1}\t{2}".format( entry[0],entry[1], round(float(entry[1]) / float(ordersNum) * 100, 2)))


def GetManagerBonus(dateStart, dateStop):
    notInStates = [
    u"Отклонен",
    u"Возврат денег"
    ]

    managerPercent = 1
    sum = 0

    for site in commonLibrary.sitesParams:
        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            continue

        orders = prestashop_api.GetOrdersIds(notInStates , dateStart, dateStop, True)

        for order in orders:
            orderId = order[0]
            orderState  = prestashop_api.GetOrderState(orderId)

            if orderState in [u"Ожидание платежа по квитанции", u"В ожидании оплаты банком", u"Отклонен"]:
                continue

            message = prestashop_api.GetOrderCustomerMessage(orderId)
            if message.find("***") == -1:
                continue

            orderItems = prestashop_api.GetOrderItems(orderId)

            #статистика по товарам
            for item in orderItems:
                product_quantity = item[3]
                unit_price_tax_incl = item[2]
                purchase_supplier_price = item[4]

                itemRevenue = product_quantity * unit_price_tax_incl
                sum += int(itemRevenue)

        if machineName.find("hp") != -1:
            ssh.kill()


    logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "managerBonus")
    logging = log.Log(logfile)
    logging.Log( str(sum / 100 * managerPercent))

def GetStatisticsByItemsCategories(dateStart, dateStop, category, prestashop_api):
    sql = """SELECT SUM( product_price ) , SUM( product_quantity )
            FROM ps_order_detail
            LEFT JOIN ps_product ON ps_order_detail.product_id = ps_product.id_product
            LEFT JOIN ps_orders ON ps_order_detail.id_order = ps_orders.id_order
            WHERE ps_product.id_category_default
            IN ( {0} )
            AND ps_orders.date_add >  '{1}'
            AND ps_orders.date_add <  '{2}'""".format(category, dateStart, dateStop)

    return prestashop_api.MakeGetInfoQueue(sql)

def GetCategoriesStatisticsForPeriod():
    workbook = xlwt.Workbook("utf-8")

    data = {}

    for site in commonLibrary.sitesParams:
        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            continue

        categories = prestashop_api.GetShopActiveCategories(1)

        startDate = datetime.datetime(2017, 9, 1)
##        endDate = startDate + datetime.timedelta(days = 30)
        endDate = datetime.datetime(2018, 9, 1)
        while endDate.date() < datetime.date.today():
            period = startDate.strftime('%d.%m.%Y') + "-" + endDate.strftime('%d.%m.%Y')
            data[period] = []
            row = 1
            for category in categories:
                catData = []
                categoryId, name, link_rewrite = category
##                if name.find(u"тпугиват") == -1 and name.find(u"ничтож") == -1:
##                    continue
##                if name in [
##                            u"Ультразвуковые отпугиватели грызунов «Торнадо»",
##                            u"Ультразвуковые отпугиватели грызунов «Град»",
##                            u"Ультразвуковые отпугиватели грызунов «Чистон»",
##                            u"Ультразвуковые отпугиватели грызунов",
##                            u"Ультразвуковые отпугиватели грызунов «Экоснайпер»"
##                            ]:
##                    name = u"Отпугиватели грызунов"
##                if name in [
##                            u"Ультразвуковые и биоакустические отпугиватели птиц",
##                            u"Отпугиватели птиц",
##                            u"Визуальные отпугиватели птиц",
##                            u"Механические и электронные отпугиватели птиц"
##                            ]:
##                    name = u"Отпугиватели птиц"
##                if name in [
##                            u"Карманные отпугиватели комаров",
##                            u"Стационарные отпугиватели комаров"
##                            ]:
##                    name = u"Отпугиватели комаров"
##                if name in [
##                            u"Ультразвуковые отпугиватели собак",
##                            u"Стационарные ультразвуковые отпугиватели собак",
##                            u"Карманные ультразвуковые отпугиватели собак"
##                            ]:
##                    name = u"Отпугиватели собак"
##                if name in [
##                            u"Отпугиватели кротов и змей",
##                            u"Отпугиватели кротов",
##                            u"Отпугиватели змей"
##                            ]:
##                    name = u"Отпугиватели кротов и змей"
                sum, quantity = GetStatisticsByItemsCategories(startDate, endDate, categoryId, prestashop_api)[0]
                if sum == None: sum = 0
                if quantity == None: quantity = 0
                if sum == 0:
                    continue
                row += 1
                index = -1
                try:
                    index = [x[0] for x in data[period]].index(name)
                except:
                    pass

                if index != -1:
                    data[period][index][1] += sum
                    data[period][index][2] += quantity
                else:
                    catData = [name, int(sum), quantity]
                    data[period].append(catData)

            startDate = endDate
            endDate = startDate + datetime.timedelta(days = 30)

        if machineName.find("hp") != -1:
            ssh.kill()

    keys = data.keys()
    periods = sorted(keys, key=lambda keys: datetime.datetime.strptime(keys.split("-")[1],'%d.%m.%Y'))

    for period in periods:
        sheet = workbook.add_sheet(period)
        row = 1
        categories = data[period]
        categories = sorted(categories, key=lambda categories: categories[1], reverse=True)
        for categoryData in categories:
            name, sum, quantity = categoryData
            sheet.write(row, 1, name)
            sheet.write(row, 2, sum)
            sheet.write(row, 3, quantity)
            sheet.col(1).width = 12000
            row += 1

    workbook.save('output.xls')

def GetProductsOrders(names, dateStart, dateStop):
    logging = log.Log(u"обзвон1.txt")
    logging.Log(u"Период: {0} - {1}".format(dateStart.split(" ")[0], dateStop.split(" ")[0]), False)
    data = {}
    for site in commonLibrary.sitesParams:
        if machineName.find("hp") != -1:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

        prestashop_api = commonLibrary.GetPrestashopInstance(site)
        if prestashop_api == False:
            continue

        orders = prestashop_api.GetOrdersIds(notInStates , dateStart, dateStop, True)
        for order in orders:
            orderId = order[0]
            orderState  = prestashop_api.GetOrderState(orderId)
            if orderState != u"Доставлен и оплачен":
                continue

            orderItems = prestashop_api.GetOrderItems(orderId)

            #статистика по товарам
            for item in orderItems:
                productName = prestashop_api.GetProductOriginalName(item[0])
                if productName == -1:
                    productName = item[1]

                productName = commonLibrary.CleanItemName(productName)

                if productName in names:
                    if productName not in data.keys():
                        data[productName] = []

                    entry = [prestashop_api.siteUrl, orderId]
                    data[productName].append(entry)

        if machineName.find("hp") != -1:
            ssh.kill()

    message = ""
    for name in data.keys():
        orders = ""
        for entry in data[name]:
            orders += "{0} ({1})\n".format(entry[1], entry[0])
        logging.Log(u"""{0}:\n{1}""".format(name, orders), False)

##    mails.SendInfoMail("заказы для обзвона", message)


################################################################################################
machineName = commonLibrary.GetMachineName()
mails = SendMails.SendMails()
GetStatistics('2019-04-01 00:00:00', '2019-05-01 00:00:00')
##GetSDEKArrears()
##WarehouseProductsCosts()
##GetManagerBonus('2018-10-01 00:00:00', '2018-11-01 00:00:00')
##GetCategoriesStatisticsForPeriod()
##data = GetProductsOrders([u"Торнадо М", u"Торнадо 1200", u"ГРАД А-1000 ПРО", u"ГРАД А-1000 ПРО+", u"ГРАД А-500", u"ГРАД А-550УЗ"], '2017-09-01 00:00:00', '2018-01-01 00:00:00')
