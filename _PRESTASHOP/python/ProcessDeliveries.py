﻿import urllib
import log
import datetime
import PrestashopAPI
import os
import re, urlparse
from selenium import webdriver
import myLibrary
import SendMails
import SdekAPI
import time
import commonLibrary
import sys
import subprocess
from lxml import etree


################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "processDeliveries")
logging = log.Log(logfile)

today = datetime.date.today()
now = datetime.datetime.now()

notAcceptedInvoices = []
message = ""

prestashop_api = PrestashopAPI.PrestashopAPI()
sdek = SdekAPI.SdekAPI(False, "b9171b1ca91597383ab3043a218b6883", "de53f4a57568665605a76e21d9f100e1")
mails = SendMails.SendMails()

deliveries = prestashop_api.GetDeliveries()
for delivery in deliveries:
    id, date, contractor, recipientCity, asIM, items, places, invoice, postingid, currentState = delivery
    if contractor not in commonLibrary.labels:
        continue
    if postingid in [None, ""] and recipientCity == u"Москва":
        if invoice in [None, ""]:
            continue
        #создаем заявку на приемку
        bErrors = False
        itemsData = []
        try:
            itemsNames, itemsQuantities = commonLibrary.ParseItems(items)
        except:
            message += "Доставка {0} ({1}): ошибка парсинга товаров\n".format(id, contractor)
            logging.Log(u"Доставка {0} ({1}): ошибка парсинга товаров".format(id, contractor))
            continue
        for i in range(0,len(itemsNames)):
            itemName = itemsNames[i]
            itemQuantity = itemsQuantities[i]
            shortItemName = prestashop_api.CleanItemName(itemName)
            skuId = prestashop_api.GetSkuIdByItemName(shortItemName)
            if skuId == -1:
                data = {"Article" : commonLibrary.GetVendorCode(), "Name" : itemName}
                result = sdek.UpdateProduct(data)
                if result != -1:
                    message += "Товар '{0}' добавлен в справочник с артикулом {1}\n".format(shortItemName.encode("utf-8"), result)
                    logging.Log(u"Товар '{0}' добавлен в справочник с артикулом {1}".format(shortItemName, result))
                    skuId = result
                    data["Sku_id"] = skuId
                    commonLibrary.AddProductToWarehouse("warehouse_moscow", data)
                else:
                    message += "[Error] Ошибка при добавлении товара '{0}' в справочник\n".format(shortItemName.encode("utf-8"))
                    logging.LogErrorMessage(u"Ошибка при добавлении товара '{0}' в справочник".format(shortItemName))
                    bErrors = True
                    break
            itemsData.append({"SkuId":skuId, "Amount":itemQuantity})

        if bErrors == False:
            result = sdek.CreatePostingApi(itemsData, invoice)
            if result != -1:
                message += "Доставка {1} ({2}): создана заявка на приемку {0}\n".format(result, id, contractor)
                logging.Log(u"Доставка {1} ({2}): создана заявка на приемку {0}".format(result, id, contractor))
                prestashop_api.SetDeliveryPostingId(id, result)
            else:
                message += "[Error] Доставка {0} ({1}): ошибка при cоздании заявки на приемку\n".format(id, contractor)
                logging.LogErrorMessage(u"Доставка {0} ({1}): ошибка при cоздании заявки на приемку".format(id, contractor))
        else:
            message += "[Error] Доставка {0} ({1}): необходимо вручную создать заявку на приемку\n".format(id, contractor)
            logging.LogErrorMessage(u"Доставка {0} ({1}): необходимо вручную создать заявку на приемку".format(id, contractor))

    if invoice != None or currentState == u"Вручен":
        if currentState != u"Вручен":
            state = commonLibrary.GetInvoiceState(sdek.GetOrderState(invoice))
            if state == -1:
                state = commonLibrary.GetInvoiceState(SdekAPI.SdekAPI().GetOrderState(invoice))
                if state == -1:
                    continue
            if currentState != state:
                prestashop_api.SetDeliveryState(id, state)
                message += "Доставка {0} ({2}): изменен статус на '{1}'\n".format(id, state.encode("utf-8"), contractor)
                logging.Log(u"Доставка {0} ({2}): изменен статус на '{1}'".format(id, state, contractor))
                currentState = state

        if currentState == u"Создан" and now.hour == 8:
            message += "Доставка {0} ({1}): груз не отправлен\n".format(id, contractor)
            logging.Log(u"Доставка {0} ({1}): груз не отправлен".format(id, contractor))

        if currentState == u"Вручен":
            if recipientCity == u"Москва":
                if postingid in [None, ""]:
                    continue
                postingItems = sdek.GetPosting(postingid, invoice)
                if postingItems == -1:
                    postingItems = []
                if len(postingItems) != 0:
                    message += "Доставка {0} ({1}): приняты товары:\n".format(id, contractor)
                    bErrors = False
                    bPostingExist = False
                    postingItemsNames = []
                    postingItemsQuantities = []
                    for postingItem in postingItems:
                        productItemName = prestashop_api.GetItemNameBySkuId(postingItem[0])
                        productQty = int(float(postingItem[1]))
                        prestashop_api.IncreaseWarehouseProductQuantity("warehouse_moscow", productItemName, productQty)
                        actualProductQty = prestashop_api.GetWarehouseProductQuantity("warehouse_moscow", productItemName)
                        postingItemsNames.append(productItemName)
                        postingItemsQuantities.append(productQty)
                        message += "{0} (артикул {1}) - {2} шт. (остаток на складе {3} шт.)\n".format(productItemName.encode("utf-8"), postingItem[0],productQty, actualProductQty)
                        logging.Log(u"{0} (артикул {1}) - {2} шт. (остаток на складе {3} шт.)\n".format(productItemName, postingItem[0],productQty, actualProductQty))
                    try:
                        deliveryItemsNames, deliveryItemsQuantities = commonLibrary.ParseItems(items)
                    except:
                        message += "Доставка {0} ({1}): ошибка парсинга товаров по доставке '{0}'\n".format(id, contractor)
                        logging.Log(u"Доставка {0} ({1}): ошибка парсинга товаров по доставке '{0}'".format(id, contractor))
                        continue
                    for i in range(0, len(deliveryItemsNames)):
                        itemName = deliveryItemsNames[i]
                        itemQty = deliveryItemsQuantities[i]
                        index = -1
                        try:
                            index = postingItemsNames.index(itemName)
                        except:
                            pass
                        if index == -1:
                            message += "[Error] Доставка {2} ({3}): отсутствует товар '{0}' в приемке №{1}\n".format(itemName.encode("utf-8"), postingid, id, contractor)
                            logging.LogErrorMessage(u"Доставка {2} ({3}): отсутствует товар '{0}' в приемке №{1}\n".format(itemName, postingid, id, contractor))
                            bErrors = True
                            continue
                        if int(postingItemsQuantities[index]) != int(itemQty):
                            message += "[Error] Доставка {4} ({5}): не совпадает количество товара '{0}': принято {1}, должно быть {2} (приемка №{3})\n".format(itemName.encode("utf-8"), postingItemsQuantities[index], itemQty, postingid, id, contractor)
                            logging.LogErrorMessage(u"Доставка {4} ({5}): не совпадает количество товара '{0}': принято {1}, должно быть {2} (приемка №{3})".format(itemName, postingItemsQuantities[index], itemQty, postingid, id, contractor))
                            bErrors = True
                        postingItemsNames.pop(index)
                        postingItemsQuantities.pop(index)

                    items = ""
                    for i in range(0, len(postingItemsNames)):
                        itemName = postingItemsNames[i]
                        itemQty = postingItemsQuantities[i]
                        items += u"{0} ({1} шт.), ".format(itemName, itemQty)
                    if items != "":
                        message += "[Info] Дополнительно приняты по приемке {0} товары: {1}\n".format(postingid, items.encode("utf-8"))
                        logging.LogInfoMessage(u"Дополнительно приняты по приемке {0} товары: {1}\n".format(postingid, items))

                    if bErrors == False:
                        prestashop_api.DeleteDelivery(id)
                        message += "Доставка {0} ({1}): товары успешно оприходованы\n".format(id, contractor)
                        logging.LogInfoMessage(u"Доставка {0} ({1}): товары успешно оприходованы".format(id, contractor))
                    else:#есть расхождения
                        prestashop_api.SetDeliveryPostingId(id, None)

                else:#врученные товары неоприходованы
                    notAcceptedInvoices.append(invoice)

            else:#доставка в Спб
                try:
                    deliveryItemsNames, deliveryItemsQuantities = commonLibrary.ParseItems(items)
                except:
                    message += "Доставка {0} ({1}): ошибка парсинга товаров\n".format(id, contractor)
                    logging.Log(u"Доставка {0} ({1}): ошибка парсинга товаров".format(id, contractor))
                    continue
                message += "Приняты на склад в Спб:\n"
                for i in range(0, len(deliveryItemsNames)):
                    itemName = deliveryItemsNames[i]
                    itemQty = deliveryItemsQuantities[i]
                    if prestashop_api.GetWarehouseProductQuantity("warehouse_spb", itemName) == -1:
                        commonLibrary.AddProductToWarehouse("warehouse_spb", {"Name" : itemName})
                    prestashop_api.IncreaseWarehouseProductQuantity("warehouse_spb", itemName, int(itemQty))
                    actualProductQty = prestashop_api.GetWarehouseProductQuantity("warehouse_spb", itemName)
                    message += ("{0} - {1} шт. (остаток на складе {2} шт.)\n".format(itemName.encode("utf-8"),itemQty, actualProductQty))
                    logging.Log(u"{0} - {1} шт. (остаток на складе {2} шт.)\n".format(itemName,itemQty, actualProductQty))
                message += "\n"
                prestashop_api.DeleteDelivery(id)

if now.weekday() not in [5, 6] and now.hour == 9 and len(notAcceptedInvoices) > 0:
    if len(notAcceptedInvoices) > 1:
        inv = "накладным"
    else:
        inv = "накладной"
    invoices = ", ".join(notAcceptedInvoices)
    mes = "Здравствуйте, Людмила!\n\nОприходуйте, пожалуйста, товары по {0} {1}..\n\nС уважением,\nИван".format(inv, invoices )
    result = mails.SendMailToPartner("support@cowms.ru", "приемка", mes)
    if result != True:
        logging.LogErrorMessage(u"Ошибка {0} при отправке письма Людмиле Старовойтовой".format(result.decode("cp1251")))
        message += "[Error] Ошибка {0} при отправке письма Людмиле Старовойтовой\n".format(str(result))
    else:
        logging.LogInfoMessage(u"Отправлено письмо Людмиле Старовойтовой о приемке по накладной {0}".format(invoices))
        message += "Отправлено письмо Людмиле Старовойтовой о приемке по накладной {0}\n".format(invoices)

if message != "":
    mails.SendInfoMail("Обработка доставок", message)

time.sleep(50)
