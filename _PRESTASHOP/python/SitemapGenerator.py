﻿import requests
import lxml
from lxml import etree
import urllib
import json
import datetime
import sys
import re, urlparse
import PrestashopAPI
import io
import commonLibrary
import subprocess
import ftplib
import os
import log
import paramiko

urlPart = """
<url>
   <loc>{url}</loc>
   <lastmod>{lastmod}</lastmod>
   <changefreq>daily</changefreq>
   <priority>{priority}</priority>
</url>"""

prestashop_api = PrestashopAPI.PrestashopAPI()
machineName = commonLibrary.GetMachineName()
logFileName = log.GetLogFileName("c:\\programming\\_PRESTASHOP\python\\log", "sitemaps")
logging = log.Log(logFileName)

date = datetime.date.today().strftime('%Y-%m-%d')

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        try:
            ssh.kill()
        except:
            pass
        continue

    logging.Log("=============== {0} ===============".format(prestashop_api.siteUrl))

    ssh_, sftp = commonLibrary.CreateSshSession(site["ip"])

    shops = prestashop_api.GetShopsUrls()
    bSubfolders = prestashop_api.IsSubfolderSite()
    domain = shops[0][2]
    domainScheme = prestashop_api.GetSiteUrlScheme()
    sitemaps = []

    for shop in shops:
        try:
            shopId = shop[1]
##            if shopId < 64:
##                continue
            print shopId
            logging.Log(str(shopId))
            urls = ""
            if bSubfolders:
                city = shop[5].replace("/", "")
            else:
                city = shop[2].split(".")[0]
                domain = shop[2]

            content = """<?xml version="1.0" encoding="UTF-8"?>
    <urlset
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {urls}
    </urlset>"""

            #категории
            categories = prestashop_api.GetShopActiveCategories(shopId)
            for category in categories:
                urlPartTmp = urlPart
                id_category, name, link_rewrite = category
                url = domainScheme + domain
                if shopId != 1 and bSubfolders:
                    url += u"/{0}".format(city)
                url += u"/{0}-{1}".format(id_category, link_rewrite)
                urlPartTmp = urlPartTmp.replace("{url}", url)
                urlPartTmp = urlPartTmp.replace("{priority}", "0.75")
                urlPartTmp = urlPartTmp.replace("{lastmod}", date)
                urls += urlPartTmp

            #товары
            for productId in prestashop_api.GetActiveProductsIDs():
                if prestashop_api.IsProductInStock(productId) == 0:
                    continue

        ##            print productId

                urlPartTmp = urlPart
                url = domainScheme + domain
                if shopId != 1 and bSubfolders:
                    url += "/{0}".format(city)

                productUrl = prestashop_api.GetProductUrl(productId, shopId)
                if productUrl == -1:
                    continue
                url += "/" + prestashop_api.GetProductUrl(productId, shopId)
                urlPartTmp = urlPartTmp.replace("{url}", url)
                urlPartTmp = urlPartTmp.replace("{priority}", "0.5")
                urlPartTmp = urlPartTmp.replace("{lastmod}", date)
                urls += urlPartTmp

            #статьи
            articles = prestashop_api.GetShopArticles(shopId)
            if articles != -1:
                for articleId in articles:
                    meta_title, short_description, Content, linkRewrite, id_category = prestashop_api.GetArticle(articleId)
                    urlPartTmp = urlPart
                    url = domainScheme + domain
                    if shopId != 1 and bSubfolders:
                        url += "/{0}".format(city)

                    url += u"/smartblog/{0}_{1}.html".format(articleId, linkRewrite)
                    urlPartTmp = urlPartTmp.replace("{url}", url)
                    urlPartTmp = urlPartTmp.replace("{priority}", "0.4")
                    urlPartTmp = urlPartTmp.replace("{lastmod}", date)
                    urls += urlPartTmp

            content = content.replace("{urls}", urls)
            sitemapName = "sitemap-{0}.xml".format(city)

            with open(sitemapName, "w") as file:
                file.write(content.encode("utf-8"))

            sftp.put(sitemapName, '/home/{0}/www/{1}'.format(site["siteDir"], sitemapName))
            os.remove(sitemapName)

            sitemaps.append(sitemapName)


            if bSubfolders:
                parts = ""
                #создаем индекс XML Sitemap
                content = """<?xml version="1.0" encoding="UTF-8"?>
        <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        {sitemaps}
        </sitemapindex>
        """
                sitemapPart = """
        <sitemap>
        <loc>{loc}</loc>
        <lastmod>{lastmod}</lastmod>
        </sitemap>"""

                for sitemapName in sitemaps:
                    loc = domainScheme + domain + "/{0}".format(sitemapName)
                    sitemapPartTmp = sitemapPart
                    sitemapPartTmp = sitemapPartTmp.replace("{lastmod}",date)
                    sitemapPartTmp = sitemapPartTmp.replace("{loc}",loc)
                    parts += sitemapPartTmp

                content = content.replace("{sitemaps}", parts)
                sitemapIndex = "sitemap_index.xml"
                with open(sitemapIndex, "w") as file:
                    file.write(content.encode("utf-8"))

                sftp.put(sitemapIndex, '/home/{0}/www/{1}'.format(site["siteDir"], sitemapIndex))
                os.remove(sitemapIndex)

        except:
            ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))
            prestashop_api = commonLibrary.GetPrestashopInstance(site)

    ssh_.close()

    if machineName.find("hp") != -1:
        ssh.kill()