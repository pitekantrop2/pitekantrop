﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import sys
import re, urlparse
import HTMLParser
import PrestashopAPI
import os
import commonLibrary
import subprocess
import SendMails


def AddCategory(name, parentCategoryId, sourceCateforyId):
    data = {}
    data["name"] = name
    data["parent"] = parentCategoryId
    data["active"] = "1"
    data["is_root_category"] = "0"
    data["description"] = name
    data["meta_title"] = u""
    data["meta_description"] = u""
    data["meta_keywords"] = u""

    data["images"] = []
    imagesPath = "images//{0}".format(prestashop_api.siteUrl.replace("http://",""))
    for file in os.listdir(imagesPath):
        if file.find(str(sourceCateforyId)) != -1:
            path = "{0}//{1}".format(imagesPath, file)
            data["images"].append(path)

    id = prestashop_api.AddCategory(data)
    if len(id) > 3:
        return -1

    return id

#########################################################################################################################

mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "googlesCategories")
logging = log.Log(logfile)


for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    categoriesIds = {}

    parentCategoryId = prestashop_api.GetCategoryID(u"Гугл")

    #посадочные
    landingPagesId = prestashop_api.GetCategoryID(u"Посадочные")
    cats = prestashop_api.GetCategoryActiveSubcategoriesIds(landingPagesId)
    for catId in cats:
        name = prestashop_api.GetCategoryName(catId)
        categoryId = prestashop_api.GetCategoryId(name, parentCategoryId)
        if categoryId == -1:
            categoryId = AddCategory(name, parentCategoryId, catId)
        categoriesIds[catId] = categoryId

    #основные категории
    categories = prestashop_api.GetShopMainCategories(1)
    for category in categories:
        categoryId, categoryname, categorylink_rewrite = category
        parentCategoryId1 = prestashop_api.GetCategoryId(categoryname, parentCategoryId)
        if parentCategoryId1 == -1:
            parentCategoryId1 = AddCategory(categoryname, parentCategoryId, category)
        categoriesIds[categoryId] = parentCategoryId1
        subcategories = prestashop_api.GetCategoryActiveSubcategoriesIds(categoryId)
        for subcategoryId in subcategories:
            subname = prestashop_api.GetCategoryName(subcategoryId)
            parentCategoryId2 = prestashop_api.GetCategoryId(subname, parentCategoryId1)
            if parentCategoryId2 == -1:
                parentCategoryId2 = AddCategory(subname, parentCategoryId1, subcategoryId)
            categoriesIds[subcategoryId] = parentCategoryId2
            subsubcategories = prestashop_api.GetCategoryActiveSubcategoriesIds(subcategoryId)
            for subsubcategoryId in subsubcategories:
                subsubname = prestashop_api.GetCategoryName(subsubcategoryId)
                parentCategoryId3 = prestashop_api.GetCategoryId(subsubname, parentCategoryId2)
                if parentCategoryId3 == -1:
                    parentCategoryId3 = AddCategory(subsubname, parentCategoryId2, subsubcategoryId)
                categoriesIds[subsubcategoryId] = parentCategoryId3

    shops = prestashop_api.GetAllShops()

    for shop in shops:
        shopId, shopDomen, shopName = shop
        logging.Log(str(shopId))
        shopName = commonLibrary.GetShopName(shopName)

##        if shopId < 278:
##            continue

##        print shopId

        for catId in categoriesIds.keys():
            newCategoryId = categoriesIds[catId]
            id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description = prestashop_api.GetShopCategoryDetails(shopId, catId)[0]
            catDetails = prestashop_api.GetShopCategoryDetails(shopId, newCategoryId)
            if catDetails == -1:
                continue
            if catDetails[0][1].find("kupit") == -1:
                link_rewrite += commonLibrary.GetLinkRewriteFromName( u"-купить {0}".format(shopName))
                newCategoryData = [link_rewrite, meta_title, description, long_description, meta_keywords, meta_description]
                prestashop_api.UpdateCategoryData(shopId, newCategoryId, newCategoryData)
                products = prestashop_api.GetCategoryActiveProductsIds(catId)
                for productId in products:
                    prcategories = prestashop_api.GetProductCategories(productId)
                    if newCategoryId not in prcategories:
                        prestashop_api.AddProductToCategory(productId, newCategoryId)

    if machineName.find("hp") != -1:
        ssh.kill()



