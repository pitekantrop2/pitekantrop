﻿import urllib
import log
import datetime
import PrestashopAPI
import os
import re, urlparse
from selenium import webdriver
import myLibrary
import SendMails
import SdekAPI
import time
import commonLibrary
import sys

def GetSkuQty(sku, StockType_id):
    for product in products:
        if product[0] == sku and product[1] == StockType_id:
            return product[2]

    return 0

def GetDefectiveSkus():
    skus = []
    for product in products:
        if product[1] == 1 and product[2] > 0:
            skus.append([product[0], product[2]])

    return skus

################################################################################

logfile = log.GetLogFileName("c:\\programming\\_PRESTASHOP\\python\\log", "compareBalances")
logging = log.Log(logfile)

message = ""

sdek = SdekAPI.SdekAPI()
mails = SendMails.SendMails()
objects = sdek.fulfilmentObjects
prestashop_api = PrestashopAPI.PrestashopAPI()

products = sdek.GetBalance()
skus = [x[0] for x in products]

warehouseProducts = prestashop_api.GetWarehouseProducts("warehouse_moscow")

ourSkus = [x[4] for x in warehouseProducts]

for warehouseProduct in warehouseProducts:
    sku = warehouseProduct[4]
    qty = warehouseProduct[2]
    itemName = prestashop_api.GetItemNameBySkuId(sku)

    if sku not in skus:
        if qty == 0:
            continue
        else:
            products.append([sku, 0, 0 ])

    sdekQty = GetSkuQty(sku, 0)

    if qty != sdekQty:
        logging.LogInfoMessage(u"{0} (артикул {3}): {1} шт. у нас, {2} шт. у СДЭК".format(itemName, qty, sdekQty, sku))
        message += "{0} (артикул {3}): {1} шт. у нас, {2} шт. у СДЭК\n".format(itemName.encode("utf-8"), qty, sdekQty, sku)


for sku in skus:
    if sku not in ourSkus:
        sdekQty = GetSkuQty(sku, 0)
        logging.LogInfoMessage(u"артикул {2}: {0} шт. у нас, {1} шт. у СДЭК".format(0, sdekQty, sku))
        message += "артикул {2}: {0} шт. у нас, {1} шт. у СДЭК\n".format(0, sdekQty, sku)

message += "\n\nБракованные товары:\n"
for product in GetDefectiveSkus():
    itemName = prestashop_api.GetItemNameBySkuId(product[0])
    message += "{0}: {1} шт.\n".format(itemName.encode("utf-8"), product[1])


message += "\n\nСписок товаров:\n"

for warehouseProduct in warehouseProducts:
    sku = warehouseProduct[4]
    qty = warehouseProduct[2]
    if qty == 0:
        continue
    itemName = prestashop_api.GetItemNameBySkuId(sku)
    message += "{0}: {1} шт.\n".format(itemName.encode("utf-8"), qty)

if message != "":
    mails.SendInfoMail("Разница в остатках", message)

time.sleep(50)
