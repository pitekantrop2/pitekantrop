﻿import log
import datetime
import sys
import PrestashopAPI
import io
import subprocess
import time
import commonLibrary
import math

states = [
u"Доставлен и оплачен"
]

prestashop_api = PrestashopAPI.PrestashopAPI()

items = []

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=760)

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    orders = prestashop_api.GetOrdersIds(states , startDate, endDate)

    for order in orders:
        orderId = order[0]

        orderItems = prestashop_api.GetOrderItems(orderId)

        #статистика по товарам
        for item in orderItems:
            productName = prestashop_api.GetProductOriginalName(item[0])
            if productName == -1:
                productName = item[1]
            product_quantity = item[3]

            product_name = productName
            product_name = prestashop_api.CleanItemName(product_name)

            itemIndex = -1

            try:
                itemIndex = [x[0] for x in items].index(product_name)
            except:
                pass

            if itemIndex != -1:
                items[itemIndex][1] += product_quantity
            else:
                itemTuple = [product_name, product_quantity]
                items.append(itemTuple)

    ssh.kill()

prestashop_api = PrestashopAPI.PrestashopAPI()

for site in commonLibrary.sitesParams:
##    if site["url"] not in ["http://mini-camera.ru.com"]:
##        continue

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    sql = """UPDATE ps_product
            SET on_sale = 0"""
    prestashop_api.MakeUpdateQueue(sql)
    sql = """UPDATE ps_product_shop
            SET on_sale = 0"""
    prestashop_api.MakeUpdateQueue(sql)

    categories = prestashop_api.GetActiveCategoriesIds()

    for categoryId in categories:
        if len(prestashop_api.GetCategorySubcategoriesIds(categoryId)) != 0:
            continue

        products = prestashop_api.GetCategoryActiveProductsIds(categoryId)
        categoryProductsIds = [i for i in products if prestashop_api.IsProductInStock(i) == True]

        popularProductsNumber = int(round( float(len(categoryProductsIds)) / float(3)))
        itemsAmount = []

        for productId in categoryProductsIds:
            productName = prestashop_api.CleanItemName(prestashop_api.GetProductOriginalName(productId))
            try:
                itemIndex = [x[0] for x in items].index(productName)
            except:
                continue
            itemAmount = [productId, items[itemIndex][1]]
            itemsAmount.append(itemAmount)

        sortedItems = []
        sortedItems = list(reversed( sorted(itemsAmount, key=lambda itemsAmount: itemsAmount[1])))

        if popularProductsNumber > len(sortedItems):
            popularProductsNumber = len(sortedItems)

        if popularProductsNumber > 4:
            popularProductsNumber = 4

        for i in range(0, popularProductsNumber):
            productId = sortedItems[i][0]
            prestashop_api.SetProductOnSale(productId, 1)

    ssh.kill()

