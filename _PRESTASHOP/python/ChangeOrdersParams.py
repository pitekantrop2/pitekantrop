﻿import log
import datetime
import sys
import PrestashopAPI
import RussianPostAPI
import SendMails
import SdekAPI
import io
import subprocess
import time
import psutil
import commonLibrary


def GetCarrier(orderNote):
    try:
        parts = orderNote.split("\n")
        for part in parts:
            if part.find("deliveryMethod") != -1:
                return int(part.replace("deliveryMethod", ""))
    except:
        return -1

    return -1

def GetDeliveryCost(orderNote):
    try:
        parts = orderNote.split("\n")
        for part in parts:
            if part.find("deliveryCost") != -1:
                return int(part.replace("deliveryCost", ""))
    except:
        return -1

    return -1

def GetOrderCity(orderNote):
    try:
        parts = orderNote.split("\n")
        for part in parts:
            if part.find("orderCity") != -1:
                return part.replace("orderCity", "")
    except:
        return -1

    return -1

################################################################################


states = [
u"В процессе подготовки",
u"Awaiting PayAnyWay payment",
u"Данного товара нет на складе",
u"В ожидании оплаты банком"
]


prestashop_api = PrestashopAPI.PrestashopAPI()

mails = SendMails.SendMails()
machineName = commonLibrary.GetMachineName()

now = datetime.datetime.now()

endDate = datetime.date.today() + datetime.timedelta(days=1)
startDate = endDate - datetime.timedelta(days=40)

message = ""

for site in commonLibrary.sitesParams:
    if machineName == "hp":
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue


    message += ("=========  " + site["url"] + "  =========\n\n")
    orders = prestashop_api.GetOrdersIds(states, startDate, endDate)

    for order in orders:

        orderId = order[0]

        orderNote = prestashop_api.GetOrderNote(orderId)

        orderCity = GetOrderCity(orderNote)
        if orderCity != -1:
            result = prestashop_api.SetOrderShop(orderId, orderCity)
            if result == -1:
                message += "[Error] Ошибка изменения заказа {0}: не найден город '{1}'\n\n".format(orderId, orderCity.encode("utf-8"))
            else:
                message += "Изменение заказа {0}: установлен город доставки '{1}'\n\n".format(orderId, orderCity.encode("utf-8"))

        deliveryCost = GetDeliveryCost(orderNote)
        if deliveryCost != -1:
            prestashop_api.SetOrderDeliveryCost(orderId, deliveryCost)
            message += "Изменение заказа {0}: установлена стоимость доставки {1} руб.\n\n".format(orderId, deliveryCost)

        carrierId = GetCarrier(orderNote)
        if carrierId != -1:
            carrierName = prestashop_api.GetCarrierNameById(carrierId)
            orderShopId = prestashop_api.GetOrderShopId(orderId)
            activeCarriers = prestashop_api.GetShopActiveCarriersIds(orderShopId)
            if carrierId in activeCarriers:
                prestashop_api.SetOrderCarrier(orderId, carrierId)
                message += "Изменение заказа {0}: установлен способ доставки '{1}'\n\n".format(orderId, carrierName.encode("utf-8"))
            else:
                message += "[Error] Ошибка изменения заказа {0}: способ доставки '{1}' отключен или не относится к данному магазину\n\n".format(orderId, carrierId)

    if machineName == "hp":
        ssh.kill()


result = mails.SendInfoMail("Изменение заказов", message)
if result != True:
    logging.LogErrorMessage(u"Ошибка при отправке письма: " + result.decode("cp1251"))
