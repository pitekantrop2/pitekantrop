﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys
from datetime import date

def ProcessProductsOnPage(products, categoryName):
    for product in products:
        productUrl = product.attrib['href']
        print productUrl
        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, categoryName, productUrl)

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = productPageContent.xpath("//span[@class = 'amount']")[0].text.strip() \
    .replace(u" руб.", "").replace(u".00", "").replace(u" ", "").replace(u",", "")

    productParams["name"] = productPageContent.xpath("//h1[contains(@class, 'product_title')]")[0].text.strip()
    index = productParams["name"].find(u" — ")
    if index != -1:
        productParams["name"] = productParams["name"][0:index].strip()

##    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)

##    productParams["images"] = []
    productParams["images"] = [ x.attrib['href'] for x in productPageContent.xpath("//div[@class = 'images']//a")]

    price = int(10 * round(float(float(productPrice))/10))
    if price % 1000 == 0:
        price -= 10

    productParams["price"] = price
    productParams["wholesale_price"] = int(float(productPrice) * 0.7)
    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    descDiv = productPageContent.xpath("//div[@itemprop = 'description']")[0]
    productDescription = etree.tostring(descDiv)

    productDescription += "<br/><br/>"

    descDiv = productPageContent.xpath("//div[@id = 'tab-description']")[0]
    productDescription += etree.tostring(descDiv)

    productDescription = commonLibrary.GetStringDescription(productDescription)

    return productDescription


def ProcessProductPrestashop(productPageContent, categoryName, productUrl):
    if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com"]:
        if categoryName.find(u"Ловушки") == -1:
            return

    try:
        productParams = GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product")
        return

    logging.Log(productParams["name"])
    if categoryName.find(u"Ловушки") != -1:
        productParams["name"] = u"Ловушка-уничтожитель насекомых (мух, комаров, мошек) " + productParams["name"]
    else:
        productParams["name"] = u"Инфракрасный (ИК) обогреватель " + productParams["name"]

    productParams["active"] = "1"
    if shopName != "":
        productParams["meta_title"] = productParams["name"]
    else:
        productParams["meta_title"] = productParams["name"]
    productParams["article"] = "me"

    prestashop_api.Parser_ProcessProduct(productParams, categoryName, "Polikon", shopName, productUrl, False)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\polikon\\log", "polikon")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()

euroRate = commonLibrary.GetRate()
if euroRate == -1:
    logging.LogErrorMessage(u"Не удалось определить курс евро")
    exit(0)

items = {
"308A":[314, 220],
"7000":[46, 32],
"307A":[295, 206],
"308E":[293, 205],
"307E":[279, 195],
"305E":[253, 177],
"309":[178, 125],
"304":[156, 109],
"300N":[80,56],
"MOON":[168, 117],
"361B":[167,115 ],
"363G":[201,141 ],
"368G":[135,95 ],
"396":[96,67 ],
"372":[698,488 ],
"397":[526,368 ],
"Stick 700":[232,162 ],
"701":[206,145 ],
"399":[144,101 ],
"398":[178,125 ],
"7216":[86,61 ],
"7228":[99,69 ]
}

for site in commonLibrary.sitesParams:
    if site["url"] not in ["http://omsk.knowall.ru.com",
                            "http://otpugiwatel.com",
                            "http://tomsk.otpugivatel.com",
                            "http://insect-killers.ru"
                            ]:
        continue

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"])

    logging.LogInfoMessage("=============" + prestashop_api.siteUrl + "================")

    products = {}
    productsIDs = prestashop_api.GetContractorProductsIds("me")
    for productId in productsIDs:
        products[productId] = prestashop_api.GetProductOriginalName(productId)

    for productId in products.keys():
        name = products[productId]
        for itemKey in items.keys():
            if name.find(itemKey) != -1:
                price = int(10 * round(float(float(items[itemKey][0]*euroRate))/10))
                wholesale_price = int(items[itemKey][1]*euroRate)
                if price % 1000 == 0:
                    price -= 10
                productDetails = prestashop_api.GetShopProductDetailsById(productId, 10)
                name, old_price, old_wholesale_price, description, link_rewrite, meta_title = productDetails
                if int(wholesale_price) != int(old_wholesale_price):
                    prestashop_api.SetProductPriceForAllShops(productId, wholesale_price, price)
                    logging.Log(u"""Product '{0}' has been updated: old wholesale price = {1}, old price = {2}, new wholesale price = {3}, new price = {4}""".format( name,
                    old_wholesale_price,old_price,wholesale_price,price))

    if machineName.find("hp") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Поликон", "", [logFilePath])