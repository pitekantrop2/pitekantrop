﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys

def ProcessProductsOnPage(products):
    for product in products:

        productUrl = product.attrib['href']

        print productUrl

        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl)

def GetProductParams(productPageContent):
    productParams = {}
    isAvailable = productPageContent.xpath("//span[@class = 'warning_inline']")
    if len(isAvailable) > 0:
        raise Exception('unknown price')

    productPrice = productPageContent.xpath("//span[@id = 'our_price_display']")[0].text.strip().replace(u"ք", "").replace(u" ", "")
    productParams["name"] = productPageContent.xpath("//h1")[0].text.split(" - ")[0].strip()
    productParams["categories"] = [ x.text.strip() for x in productPageContent.xpath("//div[@class = 'breadcrumb']//a") if x.text != None]

    if productParams["categories"] in [u"Защита сотового, защита мобильного, кокон", u"Ультразвуковые подавители диктофонов Спайсоник, поколение 1", u"Ультразвуковые блокираторы диктофонов Спайсоник, поколение 2"]:
        price = int(10 * round(float(float(productPrice))/10))
        productParams["wholesale_price"] = int(float(productPrice) * 0.7)
    else:
        price = int(10 * round(float(float(productPrice) * 1.1)/10))
        productParams["wholesale_price"] = int(float(productPrice) * 0.85)

    if price % 1000 == 0:
        price -= 10

    productParams["price"] = price

    if prestashop_api.GetProductIdQuick(productParams["name"], False) == -1:
        productParams["description"] = MakeProductDescription(productPageContent, productParams["name"])
        images = productPageContent.xpath("//ul[@id = 'thumbs_list_frame']//li//a")
        productParams["images"] = [x.attrib['href'] for x in images]

    return productParams


def MakeProductDescription(productPageContent, productName):
    description = ""

    descr = productPageContent.xpath("//div[@id = 'more_info_sheets']")[0]

    iframes = descr.xpath("//p[.//iframe]")
    for iframe in iframes:
        iframe.getparent().remove(iframe)

    dirName = prestashop_api.Transliterate(productName.replace("/", "").replace("\"", "").replace("(", "").replace(")", ""))

    #скачиваем файлы
    elements = descr.xpath("//a")
    for element in elements:
        linkText = element.text
        if linkText != None and linkText.split(".")[-1] in ["doc", "pdf"]:
            prestashop_api.DownloadFile(element.attrib["href"], linkText)
            prestashop_api.SendFileToServer(linkText, "/home/" + site["siteDir"] + "/www/img/cms/" + dirName, site["ip"])
            element.attrib["href"] = site["url"] + "//img/cms/" + dirName + "/" + linkText

    description += etree.tostring(productPageContent.xpath("//div[@id = 'idTab1']")[0])
    attachmentTab = productPageContent.xpath("//ul[@id = 'idTab9']")
    if len(attachmentTab) > 0:
        description +=  u"<h3>Инструкции</h3>"
        description += etree.tostring(attachmentTab[0])

    description = commonLibrary.UnescapeText(description)
    description = PrestashopAPI.DeleteCommentsFromHtml(description)

    return description


def ProcessProductPrestashop(productPageContent, productUrl):

    try:
        productParams = GetProductParams(productPageContent)
    except Exception as exception:
        if exception.message == "unknown price":
            logging.LogErrorMessage("Product '{0}' has unknown price".format(productUrl))
        else:
            logging.LogErrorMessage("Unable to parse product {0}".format(productUrl))
        return

##    logging.Log(productParams["name"])

    productParams["active"] = "1"
    productParams["category"] = productParams["categories"][0]
    productParams["meta_title"] = productParams["name"]

    productParams["article"] = "sc"

    prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], "spycase", shopName, productUrl, False)

#===============================================================================

commonLibrary.KillPutty()


logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\spycase\\log", "spycase")
logging = log.Log(logFilePath)
h = HTMLParser.HTMLParser()
prestashop_api = PrestashopAPI.PrestashopAPI()

sites = [
{"url": "http://safetus.ru", "ip" : "185.5.249.9", "prestashopKey" : "BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU", "dbName" : "safetus", "dbUser" : "safetus_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "safetus"},
##{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX", "siteDir": "knowall"}
]

for site in sites:

    ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    shopName = ""
    if site["url"] == "http://omsk.knowall.ru.com":
        shopName = u"Новалл"
    else:
        shopName = u"Safetus"


    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    prestashop_api.InitAllProducts()

    baseUrl = "http://spycase.ru"

    resp = requests.get(baseUrl + "/ru/sitemap")
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//ul[@class = 'tree']/li")

    for category in categories:

        categoryUrl = category.xpath("./a")[0].attrib["href"]
        categoryName = category.xpath("./a")[0].text.strip()
        print categoryName

        if categoryName in [u"Распродажа, излишки производства", u"Металлодетекторы"]:
            continue

        resp = requests.get(categoryUrl)
        categoryContent = etree.HTML(resp._content)

        products = categoryContent.xpath("//a[@class = 'product_img_link']")

        ProcessProductsOnPage(products)

    ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: spycase", "", [logFilePath])