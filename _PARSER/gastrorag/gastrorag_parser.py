﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys
import xlrd
import os

def GetProductName(productPageContent):
    name = productPageContent.xpath("//h1")[0].text.strip()
    return name


def ProcessProductsOnPage(products, categoryName):
    for product in products:
        productUrl = baseUrl + product.attrib['href']

        print productUrl

        resp = requests.get(productUrl, verify=False)
        productPageContent = etree.HTML(resp.text)

        ProcessProductPrestashop(productPageContent, productUrl, categoryName)

def GetProductParams(productPageContent):
    productParams = {}
    productParams["name"] = GetProductName(productPageContent)
    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)
    productParams["images"] = [ "http://gastrorag.ru" + x.attrib['src'] for x in productPageContent.xpath("//div[@class = 'slides']//img")]

    #инструкция
    try:
        fileUrl = baseUrl + productPageContent.xpath("//div[@class = 'text_download']/a")[0].attrib["href"]
        name = fileUrl.split("/")[-1]
        prestashop_api.DownloadFile(fileUrl, None, "c:\\work\\gastrorag\\instructions\\")
        size = os.path.getsize("c:\\work\\gastrorag\\instructions\\" + name)
        id_attachment = prestashop_api.AddAttachment(name, name, size, "application/pdf")
        productParams["id_attachment"] = id_attachment
    except:
        pass

    return productParams


def MakeProductDescription(productPageContent):
    global productFeatures
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = ""

    try:
        descriptionElem = productPageContent.xpath("//div[contains(@class, 'detail_text')]")[0]
        imgs = descriptionElem.xpath(".//img")
        for img in imgs:
            img.attrib["src"] = baseUrl + img.attrib["src"]

        tags = ["h2", "a", "form", "iframe" ]
        for tag in tags:
            elems = descriptionElem.xpath(".//{0}".format(tag))
            for elem in elems:
                elem.getparent().remove(elem)

        productDescription += etree.tostring(descriptionElem)
        productDescription = commonLibrary.GetStringDescription(productDescription)
    except:
        productDescription = "Gastrorag"


    propertiesNames = [name.xpath(".//text()")[0].strip() for name in productPageContent.xpath("//td[contains(@class, 'char_name')]/span")]
    propertiesValues = [name.xpath(".//text()")[0].strip() for name in productPageContent.xpath("//td[contains(@class, 'char_value')]/span")]
    fNames = prestashop_api.GetFeaturesNames()
    for i in range(0, len(propertiesNames)):
        propertyName = propertiesNames[i]
        propertyValue = propertiesValues[i]
        if propertyName not in fNames:
            prestashop_api.AddFeature(propertyName)
        fValues = prestashop_api.GetFeatureValues(propertyName)
        if propertyValue not in fValues:
            prestashop_api.AddFeatureValue2(propertyName,propertyValue)

        productFeatures[propertyName] = propertyValue

    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl, categoryName):
    global productFeatures
##    if productUrl not in ["http://gemlux.ru/catalog/household-appliances/cooking/table-blenders/blender_gemlux_gl_pb_579d/"]:
##        return
    try:
        productParams = GetProductParams(productPageContent)
    except Exception as exception:
        productFeatures = {}
        if exception.message == "out of stock":
            logging.LogErrorMessage("Product '" + productUrl + "' out of stock")
        else:
            print exception.message
            logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        return

    article = productParams["name"].split("GASTRORAG")[-1].strip()

    if article in priceProducts.keys():
        productParams["wholesale_price"] = priceProducts[article][0]
        productParams["price"] = priceProducts[article][1]
        quantity = priceProducts[article][2]
    else:
        logging.LogErrorMessage("Price not found for product '" + productParams["name"] + "'")
        return

    productParams["active"] = "1"
    productParams["meta_title"] = productParams["name"]
    productParams["article"] = "gr"
    productParams["features"] = productFeatures
    productParams["quantity"] = quantity
    productFeatures = {}

    productId = prestashop_api.Parser_ProcessProduct(productParams, categoryName, u"Gastrorag", "", productUrl)

    if productId != False:
        updatedProducts.append(productId)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\gastrorag\\log")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()

priceProducts = {}
prices = []
productFeatures = {}

for file in os.listdir("."):
    if file.find(".xls") != -1:
        prices.append(file)

for priceName in prices:
    print priceName
    if priceName.find("HAKKA") != -1:
        wholesale_price_col = 8
        price_col = 10
    else:
        wholesale_price_col = 7
        price_col = 9
    rate = commonLibrary.GetRate("dollar")
    workbook = xlrd.open_workbook(priceName, formatting_info=True)
    sheet = workbook.sheets()[-1]
    num_rows = sheet.nrows - 1
    for i in range(11, num_rows):
        rowValues = sheet.row_values(i, start_colx=1, end_colx=14)
        try:
            name = rowValues[3].strip()
        except:
            continue
        wholesale_price = rowValues[wholesale_price_col]
        wholesale_price = int(10 * round(float(float(wholesale_price*rate))/10))
        price = rowValues[price_col]
        price = int(10 * round(float(float(price*rate))/10))
        price1 = int(10 * round(float(float(wholesale_price) / 0.75)/10))
        if price1 > price :
            price = price1
        if price % 1000 == 0:
            price -= 10
        quantity = rowValues[12]
        if quantity == "":
            quantity = 0
        else:
            quantity = int(quantity)

        name = name.replace("GEMLUX / ","").strip()
        name = name.replace("GASTRORAG / ","").strip()
        if name not in priceProducts.keys():
            priceProducts[name] = [wholesale_price, price, quantity]

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    updatedProducts = []

    prestashop_api.InitAllProducts()

    baseUrl = "http://gastrorag.ru"
    resp = requests.get(baseUrl + "/katalog-oborudovaniya", verify=False)
    tree = etree.HTML(resp.text)

    categories = tree.xpath("//li[@class = 'sect ']//a")

    for category in categories:
        categoryUrl = category.attrib['href']
        categoryName = category.xpath(".//text()")[0].strip()

        url = (baseUrl + categoryUrl).strip()
        resp = requests.get(url, verify=False)
        categoryContent = etree.HTML(resp.text)
        pages = categoryContent.xpath("//span[@class = 'nums']//a")
        products = categoryContent.xpath("//div[@class = 'item-title']/a")
        for page in pages:
            catUrl = page.attrib['href']
            resp = requests.get(baseUrl + catUrl, verify=False)
            categoryContent = etree.HTML(resp.text)
            products += categoryContent.xpath("//div[@class = 'item-title']/a")

        ProcessProductsOnPage(products, categoryName)

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("v")
    for id in ids:
        if id not in updatedProducts:
##            prestashop_api.SetProductActive(id, 0)
            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    if machineName.find("hp") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: 31 век", "", [logFilePath])