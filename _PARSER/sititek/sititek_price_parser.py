﻿import requests
from lxml import etree
import urllib
import log
import datetime
import xlrd
import sititek_parser
import os
import PrestashopAPI
import sys
import SendMails
import commonLibrary
import subprocess

def GetIncubatorName(name):
    nums = ["880", "528", "352", "176", "264", "1056"]
    for num in nums:
        if name.find(num) != -1:
            return u"Промышленный инкубатор на {0} яиц".format(num)

    marks = [u"SITITEK 32", u"SITITEK 56", u"SITITEK 96", u"SITITEK 48", u"SITITEK 112"]
    for mark in marks:
        if name.find(mark) != -1:
            return mark

    return name

def CorrectProductName(name, category, productUrl):
    if productUrl in [ "http://www.sititek.ru/optom/alcotest/mundshtuk_universalniy_krugliy.html",
                        "http://www.sititek.ru/optom/otpugivateli_zhivotnyh_i_nasekomyh/ultrazvukovye_otpugivateli_myshej_i_krys/adapter-pitanija-ot-akkumuljatora-12-v-dlja-otpugivatelejj-gryzunov-quotgradquot.html",
                        "https://www.sititek.ru/aksessuar-dlya-podavitelya-bughunter-daudio-bda-klatch.php",
                        "https://www.sititek.ru/optom/otpugivateli_zhivotnyh_i_nasekomyh/ultrazvukovye_otpugivateli_myshej_i_krys/adapter-pitanija-ot-prikurivatelja-avtomobilja-dlja-otpugivatelejj-gryzunov-quotgradquot.html",
                    "http://www.sititek.ru/optom/unichtozhiteli-komarov/attraktant-quotoktenolquot-dlja-ispolzovanija-s-unichtozhiteljami-quotmoskito-mv-1quotquotelectrotrap-v-16quotquotsadovyjjquot.html",
                    "http://www.sititek.ru/optom/otpugivateli-ptic-ultrazvukovye/vneshnij_dinamik_GRAD_A-16_100VT_30m.html",
                    "http://www.sititek.ru/optom/otpugivateli-ptic-ultrazvukovye/vneshnij_dinamik_k_Grad_A-16_na_7000m.html",
                    "http://www.sititek.ru/optom/otpugivateli_zhivotnyh_i_nasekomyh/ultrazvukovye_otpugivateli_myshej_i_krys/adapter-pitanija-ot-prikurivatelja-avtomobilja-dlja-otpugivatelejj-gryzunov-quotgradquot.html",
                    "http://www.sititek.ru/optom/podaviteli-diktofonov-i-mikrofonov/UZ_kolonka_BugHunter_DAudio_bda-3_Voices.html"
                    ]:
        name = name.replace("\"", "")
    if name.find(u"Виниловый 3D-шар с глазами хищника") != -1:
        name = u"Виниловый 3D-шар с глазами хищника \"Terror Eyes\""
    if name.find(u"3 виниловых шара с глазами") != -1:
        name = u"Комплект из 3 шаров с глазами хищника \"Scare-Eye\""
    if category == u"Инкубаторы автоматические":
        name = GetIncubatorName(name)
    if commonLibrary.CleanItemName(name) == u"Собак.Нет Вспышка+":
        name = u"Собакам.Нет Вспышка+"
    if name.find("Zon") != -1:
        if name.find(u"Стойка (тренога)") != -1:
            name = name.replace("\"", "")
        else:
            mark = name.split("\"")[1]
            if name.find(u"телескопическим") != -1:
                mark += " Telescope"
            else:
                mark += " Single Megaphone"
            name = mark

    return name

def GetProductId(name):
    for product in products:
        try:
            if product[4].find(name) != -1:
                return product[0]
        except:
            if product[4].find(str(name)) != -1:
                return product[0]

    return -1


def ProcessProductPrestashop(productUrl, price, ourPrice, category):
    global message
    if price == "" or ourPrice == "":
        return

    print productUrl

##    if productUrl != "http://www.sititek.ru/optom/otpugivateli-ptic-ultrazvukovye/vneshnij_dinamik_k_Grad_A-16_na_7000m.html":
##        return

    category = category.replace("\\"," - ")

    try:
        resp = requests.get(productUrl, verify=False)
        if resp.status_code == 404:
            raise Exception('out of stock')
        productPageContent = etree.HTML(resp.text)
        productParams = sititek_parser.GetProductParams(productPageContent)
    except Exception as exception:
        if exception.message == "out of stock":
            logging.LogInfoMessage(u"Товар '{}' отсутствует".format(unicode( productUrl)))
        else:
            message += "Ошибка парсинга товара '{}': {}\n".format(productUrl, str(exception))
            logging.LogErrorMessage(u"Ошибка парсинга товара '{}': {}".format(productUrl, str(exception)))
        return

    price = commonLibrary.GetIntPriceFromText(price)
    ourPrice = commonLibrary.GetIntPriceFromText(ourPrice)

    newPrice = int(10 * round(float(float(ourPrice) / 0.75)/10))
    if int(price) >  newPrice:
        newPrice = int(price)

    if prestashop_api.siteUrl not in ["http://glushilki.ru.com", "http://omsk.knowall.ru.com"]:
        if newPrice % 1000 == 0:
            newPrice -= 10

    if productParams["name"].find(u"Барьер-Премиум") != -1:
        productParams["name"] = commonLibrary.CleanItemName(productParams["name"]).strip() + u" (10 шт.)"
        newPrice *= 10
        ourPrice *= 10

    productParams["name"] = CorrectProductName(productParams["name"],category, productUrl)
    productParams["active"] = "0"
    productParams["price"] = newPrice
    productParams["wholesale_price"] = ourPrice
    productParams["meta_title"] = productParams["name"]

    productParams["article"] = "st"

    print productParams["name"]
    productId, mes = prestashop_api.Parser_ProcessProduct(productParams, category, "Sititek", shopName, productUrl, message=message)
    message = mes
    if productId != False:
        if prestashop_api.IsProductActive(productId) == False:
            result = commonLibrary.ProcessNewProduct(prestashop_api, productId, productParams["name"], category)
            if result != True:
                message += "Ошибка обработки нового товара '{}' ( {} ): {}\n".format(productParams["name"].encode("utf-8"), productId, result)
                logging.LogInfoMessage(u"Ошибка обработки нового товара '{}' ( {} )".format(productParams["name"], productId))
            else:
                productsForUpdate.append(str(productId))
                message += "Товар '{}' ( {} ) добавлен в обновление\n".format(productParams["name"].encode("utf-8"), productId)

        updatedProducts.append(int(productId))

#############################################################################################################
mails = SendMails.SendMails()
logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\sititek\\log", "sititek")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
message = ""

##for file in os.listdir("."):
##    if file.endswith(".xls"):
##        os.remove(file)
##
##priceFile = commonLibrary.GetContractorPrice("st")
##if priceFile == -1:
##    sys.exit(0)

priceFile = "price.xls"

products = []

workbook = xlrd.open_workbook(priceFile, formatting_info=True)
worksheet = workbook.sheet_by_index(0)
num_rows = worksheet.nrows - 1
for i in range(12, num_rows + 1):
    link = worksheet.hyperlink_map.get((i, 2))
    url = '(No URL)' if link is None else link.url_or_path
    if url is "(No URL)":
        cellValue = worksheet.row_values(i, start_colx=1, end_colx=6)
        if cellValue[0] != "" and cellValue[1] == "":
            category = cellValue[0]
        continue

    rowValues = worksheet.row_values(i, start_colx=9, end_colx=12)
    products.append([url, rowValues[1], rowValues[0], category])

category = ""

for site in commonLibrary.sitesParams:
    if site["url"] in ["http://amazing-things.ru", "http://saltlamp.su", "http://sushilki.ru.com", "http://mini-camera.ru.com", "http://usiliteli-svyazi.ru"]:
        continue

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"],logFilePath)

    updatedProducts = []
    productsForUpdate = []

    shopName = ""
    prestashop_api.InitAllProducts()

    message += "=========  " + prestashop_api.siteUrl + "  ===========\n\n"
    logging.LogInfoMessage("============== " + prestashop_api.siteUrl + " =================")

    for product in products:
        productUrl, price, wholesalePrice, category = product
        if prestashop_api.siteUrl == "http://safetus.ru":
            if category not in [u"Видеоаппаратура (видеонаблюдение)",
                                u"Видеодомофоны и видеоглазки",
                                u"Индикаторы поля",
                                u"Обнаружители скрытых видеокамер",
                                u"Подавители GPS\ГЛОНАСС",
                                u"Подавители микрофонов (диктофонов)",
                                u"Подавители сотовых телефонов"
                                ]:
                continue

        if prestashop_api.siteUrl == "http://glushilki.ru.com":
            if category not in [
                                u"Индикаторы поля",
                                u"Обнаружители скрытых видеокамер",
                                u"Подавители GPS\ГЛОНАСС",
                                u"Подавители микрофонов (диктофонов)",
                                u"Подавители сотовых телефонов"
                                ]:
                continue

        if prestashop_api.siteUrl == "http://omsk.knowall.ru.com":
            if category not in [
                                u"Видеоаппаратура (видеонаблюдение)",
                                u"Видеодомофоны и видеоглазки",
                                u"Индикаторы поля",
                                u"Микроскопы USB",
                                u"Инкубаторы автоматические",
                                u"Обнаружители скрытых видеокамер",
                                u"Подавители GPS\ГЛОНАСС",
                                u"Подавители микрофонов (диктофонов)",
                                u"Подавители сотовых телефонов",
                                u"Отпугиватели грызунов",
                                u"Отпугиватели клещей ультразвуковые",
                                u"Отпугиватели комаров",
                                u"Отпугиватели кротов и змей",
                                u"Отпугиватели птиц",
                                u"Отпугиватели собак",
                                u"Уничтожители комаров",
                                u"Алкотестеры",
                                u"Дозиметры",
                                u"Планетарии",
                                u"Нитратомер",
                                u"Ножеточки, ножи, ломтерезки"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://otpugiwatel.com", "http://tomsk.otpugivatel.com", "http://otpugivatel.spb.ru"]:
            if category not in [
                                u"Отпугиватели грызунов",
                                u"Отпугиватели клещей ультразвуковые",
                                u"Отпугиватели комаров",
                                u"Отпугиватели кротов и змей",
                                u"Отпугиватели птиц",
                                u"Отпугиватели собак",
                                u"Уничтожители комаров"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://insect-killers.ru"]:
            if category not in [
                                u"Уничтожители комаров",
                                u"Отпугиватели комаров"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://glushilki.ru.com"]:
            if category not in [u"Индикаторы поля",
                                u"Обнаружители скрытых видеокамер",
                                u"Подавители GPS\ГЛОНАСС",
                                u"Подавители микрофонов (диктофонов)",
                                u"Подавители сотовых телефонов"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://otpugivateli-grizunov.ru"]:
            if category not in [u"Отпугиватели грызунов",
                                u"Отпугиватели кротов и змей"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://otpugivateli-krotov.ru"]:
            if category not in [u"Отпугиватели кротов и змей"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://otpugivateli-ptic.ru"]:
            if category not in [u"Отпугиватели птиц"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://otpugivateli-sobak.ru"]:
            if category not in [u"Отпугиватели собак"
                                ]:
                continue

        if prestashop_api.siteUrl in ["http://incubators.shop"]:
            if category not in [u"Инкубаторы автоматические"]:
                continue

        ProcessProductPrestashop(productUrl, price, wholesalePrice, category)


    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("st")
    for id in ids:
        if id not in updatedProducts:
            name = prestashop_api.GetProductOriginalName(id)
            active = prestashop_api.IsProductActive(id)
            if active:
                prestashop_api.MakeProductUnavailableForAllShops(id)
            else:
                prestashop_api.DeleteProduct(id)
            message += "Товар '{}' ( {} ) деактивирован\n".format(name.encode("utf-8"), id)
            logging.LogInfoMessage(u"Товар '{}' ( {} ) деактивирован".format(name, id))

    #добавляем запись в updateProducts
    if len(productsForUpdate) != 0:
        productsIds = ",".join(productsForUpdate)
        prestashop_api.AddUpdateProducts(prestashop_api.siteUrl, productsIds)

    if machineName.find("hp") != -1:
        ssh.kill()

result = mails.SendInfoMail("Обновление товаров: Сититек", message)
