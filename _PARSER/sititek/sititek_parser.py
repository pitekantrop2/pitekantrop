﻿import requests
from lxml import etree
import urllib
import log
import datetime
import HTMLParser
import commonLibrary
import copy

def GetProductName(productPageContent):
    classes = ["roznica tabactive", "optom tabactive", "optom active"]
    for class_ in classes:
        titleElem = productPageContent.xpath("//div[@class = '{0}']//div".format(class_))
        if len(titleElem) != 0:
            break

    titleElem = titleElem[0].xpath(".//text()")
    productName = ""
    for part in titleElem:
        productName += unicode(part) + " "

    return productName.replace(u"(Оптом)","").strip()

def GetProductParams(productPageContent):
    productParams = {}

    #название
    productParams["name"] = GetProductName(productPageContent)

    #описание
    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["images"] = []
    imageSrc = productPageContent.xpath("//div[@class = 'col-sm-5 col-lg-3 text-center']//a/img")[0].attrib['src']
    if imageSrc.find("www.sititek.ru") == -1:
        imageSrc = "https://www.sititek.ru" + imageSrc
    productParams["images"].append(imageSrc)

    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productPageContentTmp = copy.copy(productPageContent)

    description = u""
    p = productPageContentTmp.xpath("//div[@itemprop = 'description']")[0]

    elements = productPageContentTmp.xpath("//a//img")
    for element in elements:
        bNeedUpdate = True
        a = element.getparent()
        pos = a.getparent().index(a)
        a.getparent().insert(pos, element)
        element.getparent().remove(a)

    aa = productPageContentTmp.xpath("//a")
    for a in aa:
        a.getparent().remove(a)

    isEnd = False

    while True:
        if len(p.xpath("./object")) == 0:
            description += etree.tostring(p)
        try:
            classAttr = p.getnext().xpath("./span")[0].attrib["class"]
            if classAttr == "manufacturer" or classAttr == "p_n warranty":
                break
        except:
            pass

        p = p.getnext()
        if p == None:
            break
        if len(p.xpath(".//form")) != 0:
            break

    description = commonLibrary.GetStringDescription(description)
    description = description.replace("src=\"/","src=\"http://www.sititek.ru/")
    description = description.replace("""<div class="hr">""", "")
    description = description.replace("""sititek.ru """, "")
    description = description.replace("""h2""", "h3")
##        prestashop_api.SetProductDescription(i, descr)
##    print description
    return description