﻿import requests
from lxml import etree
import urllib
import log
import datetime
import time
import HTMLParser
import PrestashopAPI
import commonLibrary
import SendMails
import subprocess
import sys

def GetProductTitle(productPageContent):
    nameElem = productPageContent.xpath("//div[@class = 'name']/strong")[0]
    productName = nameElem.text.strip()

    if nameElem.xpath("./span")[0].text != None:
        productName = productName + " " + nameElem.xpath("./span")[0].text

    return productName

def ProcessProductsOnPagePrestashop(products, categoryName):

    for product in products:

        time.sleep(5)
        print product.attrib['href']

##        if product.attrib['href'] != "/products/recorders/335/1183/":
##            continue

        productUrl = baseUrl + product.attrib['href']
        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        productName = GetProductTitle(productPageContent)

        ProcessProductPrestashop(productPageContent, categoryName, productUrl)

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = productPageContent.xpath("//div[@class = 'price']/text()")[0]
    productParams["title"] = GetProductTitle(productPageContent)

    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["html-title"] = shopName + u" - " + productParams["title"].lower()

    strproductPrice = productPrice.replace(" ","").replace(u"от","").replace(u"р.","")
    print strproductPrice
    if strproductPrice == "***":

        productParams["price"] = 0
        productParams["cost-price"] = 0
    else:
        price = int(10 * round(float(int(strproductPrice) * 1.05))/10)
        if price % 1000 == 0:
            price -= 10
        productParams["price"] = price

        productParams["cost-price"] = int(int(strproductPrice) * 0.8)

    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = ""

    aa = productPageContent.xpath("//a")
    for a in aa:
        a.getparent().remove(a)

    productDescription += etree.tostring(productPageContent.xpath("//div[@id = 'detail_text']")[0])

    characteristicsElem = productPageContent.xpath("//div[@id = 'detail_tech']")

    if len(characteristicsElem) > 0:
        productDescription += etree.tostring(characteristicsElem[0])

    complectElem = productPageContent.xpath("//div[@id = 'complect']")

    if len(complectElem) != 0:
        productDescription += etree.tostring(complectElem[0])

    productDescription = commonLibrary.GetStringDescription(productDescription)

    return productDescription

def ProcessProductPrestashop(productPageContent, categoryName, productUrl):

    try:
        productParams = GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product " + productUrl)
        return

    prodParams = {}

    prodParams["active"] = "1"
    prodParams["price"] = productParams["price"]
    prodParams["wholesale_price"] = productParams["cost-price"]
    prodParams["meta_title"] = shopName + " - " + productParams["title"]

    prodParams["name"] = productParams["title"]
    prodParams["description"] = productParams["description"]
    prodParams["images"] = []
    prodParams["category"] = categoryName

    prodParams["article"] = "ts"

    prestashop_api.Parser_ProcessProduct(prodParams, prodParams["category"], "telesys", shopName, productUrl)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\telesys\\log", "telesys")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()

mails = SendMails.SendMails()

sites = [
{"url": "http://safetus.ru", "ip" : "185.5.249.9", "prestashopKey" : "BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU", "dbName" : "safetus", "dbUser" : "safetus_user_ex", "dbPassword": "1qaz@WSX"},
{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX"},
]

for site in sites:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    shopName = ""
    if site["url"] == "http://omsk.knowall.ru.com":
        shopName = u"Новалл"
    else:
        shopName = u"Safetus"

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    prestashop_api.InitAllProducts()

    baseUrl = "http://telesys.ru"
    resp = requests.get(baseUrl + "/products/recorders")
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//div[@class = 'seria-description']//a")
    ##print len(categories)

    for category in categories:
        categoryName = category.xpath("./font")[0].text.strip()
        categoryUrl = category.attrib['href']
        if categoryUrl.find(baseUrl) == -1:
            categoryUrl = baseUrl + categoryUrl
##        print "category url {0}".format(categoryUrl)

        resp = requests.get(categoryUrl)
        categoryContent = etree.HTML(resp._content)

        products = categoryContent.xpath("//div[@class = 'elements-list']/table//h2/a")

        ProcessProductsOnPagePrestashop(products, categoryName)

##    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("ts")
    for id in ids:
        date = prestashop_api.GetProductUpdateDate(id)
        if date.date() < datetime.date.today() - datetime.timedelta(days = 1):
            prestashop_api.MakeProductUnavailableForAllShops(id)
            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    if machineName.find("hp") != -1:
        ssh.kill()

mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Телесистемы", "", [logFilePath])