﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys
import xlrd
import os
import myLibrary

def GetIntVolume(volume):
    if volume == "-":
        volume = 0
    elif volume.find("/") != -1:
        volume = sum( [ int(x) for x in volume.split(" ")[0].split("/")])
    else:
        volume = volume.split(" ")[0]
        volume = int(float(volume.replace(",",".")))

    return volume

def GetCategoryName(name):
    if name == u"ROSE OF BULGARIA FOODS & DRINKS":
        return u"Rose of Bulgaria Delight"
    if name == u"OLIVE OIL":
        return u"Olive Oil of Greece"

    marks = ["Maternea",
             "Bebble",
             "KiKi",
             "My Rose of Bulgaria",
             "Hands@Work",
             "Footness",
             "Rose of Bulgaria",
             "Rose of Bulgaria for men",
             "Regina Floris",
             "Yoghurt of Bulgaria",
             "Olive Oil of Greece",
             "Herbs of Bulgaria Lavender",
             "Hand made spa collection",
             "Footprim"

             ]

    for mark in marks:
        if name.lower().find(mark.lower()) != -1:
            return mark

def CorrectName(name,  code):
    bExact = False
    name = name.strip().split("(")[0]
    array = name.split(" ")
    if name.find("footness") == -1:
        if len(array) > 4:
            array = name.split(" ")[0:4]
    name = " ".join(array)
    if code == "3800200966079":
        name = u"Молочко для лица ультраделикатное"
    if code == "3800200961982":
        name = u"Соль для ванны Лаванда"
    if code == "3800200963184":
        name = u"Омолаживающий увлажняющий дневной крем"
    if code == "3800156001886":
        name = u"Экстра мягкий крем для"
    if code == "3800002300897":
        name = u"Body Oil Bebble"
    if code == "3800200963375":
        name = u"Крем для лица для любого типа кожи увлажняющий Rose of Bulgaria"
    if code == "3800200964105":
        name = u"Мыло ручной работы Цветок Розы бутон розы"
    if code == "3800200963382":
        name = u"Крем для лица Антивозрастной"
    if code == "3800200963191":
        name = u"Омолаживающий обновляющий ночной крем для лица Лаванда"
    if code == "3800200961951":
        name = u"Жидкое мыло Лаванда"
    if code == "3800200965638":
        name = u"Молочко для тела с лифтинг-эффектом Q10"
    if code == "3800200962651":
        name = u"Крем для лица дневной Rose of Bulgaria"
    if code == "3800200963108":
        name = u"Гель для душа для женщин"
    if code == "3800200968615":
        name = u"Крем для лица Probiotic Face Cream"
    if code == "3800156003057":
        name = u"Восстанавливающий шампунь Olive Oil of Greece"
    if code == "3800002300941":
        name = u"Крем для лица Facial Cream Bebble"
    if code == "3800200963153":
        name = u"Массажный крем Лаванда"
    if code == "3800002303126":
        name = u"Разогревающий антицеллюлитный крем Warming Anti-Cellulite Cream-Gel"
    if code == "3800002306806":
        name = u"Шампунь и гель для душа с ароматом клубники"
    if code == "3800002301047":
        name = u"Гель для мытья Wash Gel Bebble"
    if code == "3800002303140":
        name = u"Средство для мытья бутылочек, игрушек, посуды Bebble"
    if code == "3800002301214":
        name = u"Крем-мыло Cream-Soap Bebble"
    if code == "3800156002586":
        name = u"Очищающее молочко Olive Oil of Greece"
    if code == "3800002300835":
        name = u"Крем от опрелостей Nappy Rash Cream Bebble"
    if code == "3800200965690":
        name = u"Духи женские"
    if code == "3800002300910":
        name = u"Крем для тела Body Cream Bebble"
    if code == "3800002310254":
        name = u"Deo Foot spray"
    if code == "3800002306790":
        name = u"Шампунь и гель для душа с ароматом банана"
    if code == "3800200966048":
        name = u"Face Mask Ultra Detox Yoghurt of Bulgaria"
    if code == "3800200963122":
        name = u"Питающий крем для лица Лаванда"
    if code == "3800002300873":
        name = u"Body Milk Bebble"
    if code == "3800200962644":
        name = u"Крем для лица ночной Rose of Bulgaria"
    if code == "3800200966093":
        name = u"Гель для душа тонизирующий Probiotic Toning Shower Gel Yoghurt of Bulgaria"
    if code == "3800002301849":
        name = u"Шампунь для волос My Rose OF BULGARIA 250 мл"
    if code == "3800002310308":
        name = u"Крем для потрескавшейся кожи рук Hands@work intensive"
    if code == "3800002304390":
        name = u"Салфетки влажные Wet Wipes Bebble 64 pcs с клипом-крышкой"
    if code == "3800002303324":
        name = u"Мицеллярная розовая вода Micellar Rose"
    if code == "3800002310018":
        name = u"Маска для лица из бамбуковой ткани"
    if code == "3800002310285":
        name = u"Крем для рук увлажняющий Hands@work classic (D-пантенол)"
    if code == "3800002310292":
        name = u"Крем для защиты чувствительной кожи рук Hands@work soft (Витамин E)"
    if code == "3800002310315":
        name = u"SOS Крем для рук регенерирующий Hands@work"
    if code == "3800002310322":
        name = u"Влажные салфетки очищающие от агрессивных веществ"
    if code == "3800200965638":
        name = u"Молочко для тела с лифтинг-эффектом Q10  Regina Floris"
    if code == "3800200966086":
        name = u"Гель для умывания ультра мягкий с пробиотиком Yoghurt of Bulgaria"
    if code == "3800002300040":
        name = u"Успокаивающий крем для сосков Nutri-Calming Nipple Balm"

    name = name.replace(" ", "%20")

    return name

def GetProductName(productPageContent):
    name = productPageContent.xpath("//h1")[0].text.strip()
    return name


def ProcessProductsOnPage(products, priceProduct):
    for product in products:
        productUrl = product.attrib['href'].split("?")[0]
        print productUrl

        selenium.OpenUrl(productUrl)
        source =  selenium.GetSource()
        productPageContent = etree.HTML(source)

        productId = ProcessProductPrestashop(productPageContent, productUrl, priceProduct)
        if productId != -1:
            return productId
        else:
            time.sleep(1)

    return -1

def GetProductParams(productPageContent):
    productParams = {}
    productParams["name"] = GetProductName(productPageContent)
    price = productPageContent.xpath("//span[@class = 'price-new']")[0].text
    price = commonLibrary.GetIntPriceFromText(price)
    oldPrice = productPageContent.xpath("//span[@class = 'price-old']")
    productParams["prices"] = []
    if len (oldPrice) != 0:
        oldPrice = commonLibrary.GetIntPriceFromText(oldPrice[0].text)
        productParams["prices"].append( oldPrice)
    productParams["prices"].append( price)
    productSection = commonLibrary.GetStringDescription( productPageContent.xpath("//div[@class = 'product-section']")[0])
    p = re.compile(u'(?<=<span>Объем:</span>)(.*)(?=<br/>)')
    try:
        volume = int(p.findall(productSection)[0].strip())
    except:
        volume = 0
    productParams["volume"] = volume

    consistElem = productPageContent.xpath("//div[contains(@id, 'tab-consist')]")
    if len(consistElem) != 0:
        consistElem[0].attrib.pop("id")
        consistElem[0].attrib.pop("class")
        consistElem[0].attrib.pop("style")
        productParams["characteristics"] = commonLibrary.GetStringDescription(consistElem[0])
        productParams["characteristics"] = productParams["characteristics"].replace("h2", "h3")
##
    productParams["description"] = MakeProductDescription(productPageContent)
    productParams["images"] = [ x.attrib['href'] for x in productPageContent.xpath("//div[@id = 'full_gallery']//ul//li//a")]

    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = ""

    descriptionElem = productPageContent.xpath("//div[contains(@id, 'tab-description')]")[0]
    imgs = descriptionElem.xpath(".//img")

    tags = ["h2", "h1", "form", "iframe" ]
    for tag in tags:
        elems = descriptionElem.xpath(".//{0}".format(tag))
        for elem in elems:
            elem.getparent().remove(elem)

    productDescription += etree.tostring(descriptionElem)
    productDescription = commonLibrary.GetStringDescription(productDescription)

    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl, priceProduct):
    global message
    name, wholesale_price, price, avail, volume, code, categoryName = priceProduct
    try:
        productParams = GetProductParams(productPageContent)
    except Exception as exception:
        if exception.message != "":
            message = exception.message
        else:
            message = exception.args[1]
        logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        try:
            logging.LogErrorMessage("exception: {0}".format(message))
        except:
            chars = [u"\xab", u"\xbb"]
            for char in chars:
                message = message.replace(char, "")
            print message
        return

    intVolume = GetIntVolume(volume)

    if productParams["volume"] != 0 and intVolume != 0:
        if productParams["volume"] != intVolume:
            return -1

    productParams["wholesale_price"] = wholesale_price
    productParams["price"] = price = int(10 * round(float(float(price) * 1.05)/10))
    productParams["quantity"] = int(avail)

    productParams["original_name"] = code
    productParams["active"] = "1"
    productParams["meta_title"] = productParams["name"]
    productParams["article"] = "ic"

    #фичи
    productFeatures = {}
    productFeatures[u"Бренд"] = categoryName
    if intVolume != 0:
        if volume.find(u"г") != -1:
            productFeatures[u"Масса"] = volume
        elif volume.find(u"шт") != -1:
            productFeatures[u"Количество"] = volume
        else:
            productFeatures[u"Объем"] = volume

    fNames = prestashop_api.GetFeaturesNames()
    for propertyName in productFeatures.keys():
        propertyValue = productFeatures[propertyName]
        if propertyName not in fNames:
            prestashop_api.AddFeature(propertyName)
        fValues = prestashop_api.GetFeatureValues(propertyName)
        if propertyValue not in fValues:
            prestashop_api.AddFeatureValue2(propertyName,propertyValue)

    productParams["features"] = productFeatures

    productId, mes = prestashop_api.Parser_ProcessProduct(productParams, categoryName, u"Interscosmetics", "", productUrl,message=message)
    message = mes
    if productId != False:
        updatedProducts.append(int(productId))

    return productId

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\interscosmetics\\log")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()
message = ""

##priceProducts = {}
priceProducts = []

for file in os.listdir("."):
    if file.find(".xlsx") != -1:
        priceName = file

workbook = xlrd.open_workbook(priceName)
sheet = workbook.sheets()[-1]
num_rows = sheet.nrows - 1
for i in range(2, num_rows):
    rowValues = sheet.row_values(i, start_colx=0, end_colx=12)
    try:
        name = rowValues[3].strip()
    except:
        continue
    if name == "":
        categoryName = rowValues[0]
        continue
    price = int(float(rowValues[7]))
    wholesale_price = int(float(rowValues[6]))
    volume = unicode(rowValues[4]).strip()
    avail = rowValues[10].lower() == u"да"
    name = commonLibrary.DeleteQuotes( name.lower()).replace(" 0+", "").strip()
    categoryName = GetCategoryName(categoryName)
    code =  str(int(float(rowValues[1])))
    priceProducts.append( [name, wholesale_price, price, avail, volume,code , categoryName])

for site in commonLibrary.sitesParams:
    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    updatedProducts = []

    prestashop_api.InitAllProducts()

    baseUrl = "http://interscosmetics.ru"
    catName = ""
    selenium = myLibrary.Selenium()
    selenium.SetUp()

    #поиск
    for product in priceProducts:
        name, wholesale_price, price, avail, volume, code, categoryName = product
        logging.Log(name)
        if catName != categoryName:
            catName = categoryName
            logging.Log(u"============ {0} ===========".format(catName))
        correctedName = CorrectName(name,  code)
##        logging.Log(correctedName)
        url = baseUrl + u"/search&search={0}".format(correctedName)
        selenium.OpenUrl(url)
        source =  selenium.GetSource()
        tree = etree.HTML(source)
        products = tree.xpath("//li[.//div[contains(.,'{0}')]]//div[@class = 'name']//a".format(price))
        if len(products) == 0:
            products = tree.xpath("//li[contains(@class, 'col-sm-4')]//div[@class = 'name']//a")#собираем все товары, не сравнивая цены
            if len(products) == 0:
                logging.Log("Product '{0}' not found".format(code))
                continue
            if len(correctedName.split("%20")) <= 4:#если не задана конкретная поисковая фраза
                while len(products) > 0:
                    product_ = products[0]
                    prName = product_.text
                    prCat = GetCategoryName(prName)
                    priceProductCat = GetCategoryName(name)
                    if prCat != priceProductCat:
                        products.pop(0)
                    else:
                        break
        productId = ProcessProductsOnPage(products, product)
        if productId == -1:
            logging.Log("Product '{0}' not found".format(code))

    selenium.CleanUp()

    if machineName.find("hp") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Gemlux", "", [logFilePath])