﻿import requests
from lxml import etree
import urllib
import log
import datetime
import xlrd
import PrestashopAPI
import optimus_parser
import os

def GetProductId(name):

    for product in products:
        try:
            if product[4].find(name) != -1:
                return product[0]
        except:
            if product[4].find(str(name)) != -1:
                return product[0]

    return -1

def ProcessProductInsales(productLink,price, ourPrice, category):

    if productLink.find("avtocamera") != -1 or productLink.find("4455.ru") != -1:
        return

    ourPrice = int(ourPrice)
    newPrice = int(10 * round(float(int(price) * 1.05)/10))
    priceForSpycams = int(10 * round(float(int(price) * 1.07)/10))

    if productLink.find("spycams") != -1:
        spycams_parser.ProcessProduct(productLink, priceForSpycams, ourPrice,category)
    if productLink.find("podavitel") != -1:
        podavitel_parser.ProcessProduct(productLink,newPrice, ourPrice, category)
    if productLink.find("videogsm") != -1:
        videogsm_parser.ProcessProduct(productLink,newPrice, ourPrice, category)
    if productLink.find("domofons") != -1:
        domofons_parser.ProcessProduct(productLink,newPrice, ourPrice, category)

def ProcessProductPrestashop(productUrl, price, ourPrice, category):

    try:
        print productUrl, price, ourPrice, category
    except:
        pass

    if price == "" or ourPrice == "":
        return

    try:
        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)
        productParams = optimus_parser.GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        return

    try:
        price = str(price).replace(" ", "").replace(".0","")
    except:
        price = str(price.encode("cp1251").replace("\xa0", "").decode("cp1251")).replace(".0","")
    try:
        ourPrice = str(ourPrice).replace(" ", "").replace(".0","")
    except:
        ourPrice = str(ourPrice.encode("cp1251").replace("\xa0", "").decode("cp1251")).replace(".0","")

    newPrice = int(10 * round(float(int(price) * 1.00)/10))
    productParams["active"] = "1"
    productParams["price"] = newPrice
    productParams["wholesale_price"] = ourPrice
    productParams["meta_title"] = shopName + " - " + productParams["name"]

    prestashop_api.Parser_ProcessProduct(productParams, category, "Optimus", shopName)


#############################################################################################################
logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\optimus\\log")
logging = log.Log(logFilePath)

##prestashop_api = PrestashopAPI.PrestashopAPI("http://omsk.knowall.ru.com", 'KAR7M17CPPALJY38NKI521NNP96IH3Z7', "127.0.0.1", "knowall", "knowall_user_ex", "1qaz@WSX", 5555)
##shopName = u"Новалл"

prestashop_api = PrestashopAPI.PrestashopAPI("http://safetus.ru", 'BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU', "127.0.0.1", "safetus", "safetus_user_ex", "1qaz@WSX", 5555, logFilePath)
shopName = u"Safetus"

##prestashop_api = PrestashopAPI.PrestashopAPI("http://amazing-things.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", "amazing_things", "am_th_user_ex", "1qaz@WSX", 5555)
##shopName = "Amazing Things!"

logging.LogInfoMessage(prestashop_api.siteUrl)

for file in os.listdir("."):
    if file.endswith(".xls"):
        priceFile = file
        break

workbook = xlrd.open_workbook(priceFile, formatting_info=True)
products = prestashop_api.GetShopProducts(1)

for s in range(1, len(workbook.sheets()) - 1):
    worksheet = workbook.sheet_by_index(s)
    print worksheet.name
    urlColNum = 0
    priceCol = 4
    ourPriceCol = 8
    if s == 8 or s == 5:
        priceCol = 3
        ourPriceCol = 7

    num_rows = worksheet.nrows

    category = ""

    for i in range(num_rows):
        link = worksheet.hyperlink_map.get((i, urlColNum))
        url = '(No URL)' if link is None else link.url_or_path
        if url is "(No URL)":
            cellValue = worksheet.row_values(i, start_colx=0, end_colx=8)
            if cellValue[0] != "" and cellValue[1] == "":
                category = cellValue[0]
            continue

        rowValues = worksheet.row_values(i, start_colx=0, end_colx=14)
        ProcessProductPrestashop(url, rowValues[priceCol], rowValues[ourPriceCol],category)


