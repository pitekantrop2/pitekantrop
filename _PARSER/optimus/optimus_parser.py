﻿import requests
from lxml import etree
import urllib
import dbForParser
import InsalesForParser
import log
import datetime
import HTMLParser



def GetProductParams(productLink):

    resp = requests.get(productLink)
    productPageContent = etree.HTML(resp._content)

    productName = productPageContent.xpath("//div[@class = 'flypage']/h1")[0].text.strip()

    productParams = {}

    #название
    productParams["name"] = productName

    #описание
    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["images"] = []
    productParams["images"].append(productPageContent.xpath("//a[contains(@id, 'main_image_full')]")[0].attrib['href'])

    return productParams


def MakeProductDescription(productPageContent):

    description = ""
    h2Descr = productPageContent.xpath("//h2[@class = 'tabber_title' and contains(., '" + u"Описание" + "')]")[0]

    while h2Descr.getnext().tag != "div":
        description += etree.tostring(h2Descr.getnext())
        h2Descr = h2Descr.getnext()

    description += "<br/><br/>"

    try:
        description += etree.tostring(productPageContent.xpath("//div[@id = 'teh_har']/table")[0])
    except:
        pass

    description += "<br/><br/>"

    try:
        description += etree.tostring(productPageContent.xpath("//div[@class = 'video_full']/iframe")[0])
    except:
        pass

    description = description.replace("""justify""", """left""")

    h = HTMLParser.HTMLParser()
    description = h.unescape(description)

    return description

#===============================================================================

if __name__ == "__main__":
    logFilePath = InsalesForParser. GetLogFileName("c:\\programming\\_PARSER\\logos\\log")
    logging = log.Log(logFilePath)
    db = dbForParser.DB("knowall", logFilePath)
    insales_api = InsalesForParser.InsalesForParser('shop-65139', '0bb0ce959f0bb488968cd129e2c54435', 'b0fa3ea138286e59b76f922ad3ea7140',logFilePath)

