﻿import requests
from lxml import etree
import urllib
import log
import datetime
import xlrd
import spycams_parser
import domofons_parser
import videogsm_parser
import podavitel_parser
import shpionamnet_parser
import skrembler_parser
import PrestashopAPI
import os
from openpyxl import load_workbook
import openpyxl
import commonLibrary
import sys
import subprocess
import SendMails

excludedCategories = [
##                        u"IP камеры",
##                        u"AHD камеры",
##                        u"Проводные камеры",
                        u"Солнечные батареи",
                        u"Комплект системы IP видеонаблюдения с функцией распознаванием лиц",
                        u"Камеры 3G/4G с SIM-картой",
                        u"Видеорегистраторы SKY",
                        u"Видеорегистраторы (share-vision)",
                        u"Готовые решения",
                        u"Прочее (роутеры, провода, диски, карты и т.д)",
                        u"Камеры на солнечных батареях",
                        u"Видеоняни",
                        u"Биометрические замки",
                        u"Пластиковые кейсы",
                        u"Парктроники",
                        u"Прочее",

                        ]

excludedBigCategories = [u"СЧЕТЧИКИ ПОСЕТИТЕЛЕЙ",
            u"РАСПРОДАЖА" ,
            u"МАШИНКИ ДЛЯ СЧЕТА ДЕНЕГ",
            u"МЕТАЛЛОДЕТЕКТОРЫ",
            u"ИНВЕРТОРЫ"
            ]

def CorrectUrl(itemName, url):
    if itemName == u"Терминатор 300-16х101":
        return "http://www.podavitel.ru/terminator-300-16-101.php"
    if itemName == u"Терминатор 250-12х64":
        return "http://www.podavitel.ru/terminator-250-12-64.php"
    if itemName == u"Терминатор 200-8х42":
        return "http://www.podavitel.ru/terminator-200-8-42.php"
    if itemName == u"Терминатор 200-18х42":
        return "http://www.podavitel.ru/terminator-200-18-42.php"
    if itemName == u"Терминатор 33-5G":
        return "http://www.podavitel.ru/terminator-33-5g.php"
    if itemName == u"Терминатор 35-5G (16х12)":
        return "http://www.podavitel.ru/terminator-35-5g.php"
    if itemName == u"Amazon-217-AW2-8GS":
        return "http://www.spycams.ru/wi-fi-ip-camera-amazon-217-aw2-8gs.html"
    if itemName == u"C монитором iHome-GLAZ-М-Black ((DM28-W) monitor-black":
        return "http://www.domofons.info/videoglazok/s-monitorom/ihome-glaz-m-black.php"
    if itemName == u"Страж GSM-Din-rail 18 (GSM терм. на DIN-рейку на 18,5 кВт)":
        return "http://www.videogsm.ru/gsm-datchik-temperatury-s-upravleniem-strazh-gsm-din-rail-18-kvt.php"
    if itemName == u"Wi-Fi-розетка Лайт-WiFi":
        return "http://www.videogsm.ru/wifi-rozetka-lajt-sp1.php"
    if itemName == u"Ак. сейф  SPY-box Шкатулка-3 GSM-П":
        return "http://www.podavitel.ru/akusticheskij-sejf-spy-box-shkatulka-3-gsm-p.php"

    if url.find("http://") == -1:
        url = "http://" + url

    return url

def GetProductName(name, productUrl):
    if productUrl == "http://www.podavitel.ru/scorp-gsm-gps.php":
        return u"Скорпион GSM+GPS"
    if name == u"Подавитель Скорпион 5XL Интернет":
        return u"Скорпион 5XL Интернет"
    if name.find(u"iHome-4R") != -1 and name.find(u"бронза") != -1:
        return u"iHome-4R (бронза)"
    if name.find(u"iHome-4G") != -1 and name.find(u"бронза") != -1:
        return u"iHome-4G (бронза)"
    if name.find(u"iHome-4") != -1 and name.find(u"бронза") != -1:
        return u"iHome-4 (бронза)"
    if name.find(u"iHome-4") != -1 and name.find(u"серебро") != -1:
        return u"iHome-4 (серебро)"
    if name.find(u"Kvadro") != -1 and name.find(u"GSM-розетка") != -1:
        name = commonLibrary.CleanItemName(commonLibrary.ReplaceBadChars(name))
        return u"GSM-розетка {}".format(name)
    if name.find(u"Лайт") != -1 and name.find(u"GSM-розетка") != -1:
        name = commonLibrary.CleanItemName(commonLibrary.ReplaceBadChars(name))
        return u"GSM-розетка {}".format(name)
    if name.find(u"Квадро-А") != -1 and name.find(u"Wi-Fi-розетка") != -1:
        return u"Wi-Fi-розетка Квадро-А"
    name = commonLibrary.CleanItemName(commonLibrary.ReplaceBadChars(name))
    if name.find(u"WF91") != -1:
        return u"JMC-WF91"
    if name.find(u"iHome-2WG") != -1:
        return u"iHome-2WG"
    with open("marks.txt", "r") as f:
        marksFile = f.read()
    decoded = commonLibrary.ReplaceBadChars(marksFile.decode("utf-8"))
    marks = [ i for i in decoded.split("\n") if i != ""]
    for mark in marks:
        if name.lower().find(mark.lower()) != -1:
            return mark

    return name

def ProcessProductPrestashop(productUrl, price, ourPrice, category):
    global updatedProducts
    global message
    try:
        print productUrl
    except:
        pass

##    if category not in [u"Подавители связи_Мультичастотные подавители"]:
##        return

    if category == u"GSM охрана_Дополнительные датчики":
        ourPrice = str(int(10 * round(float(float(price) * 0.85)/10)))

##    if productUrl != "http://www.spycams.ru/2-kamera-micro-avtonom.html":
##        return

    try:
        if productUrl.find("spycams") != -1:
            productParams = spycams_parser.GetProductParams(productUrl)
        elif productUrl.find("domofons") != -1:
            productParams = domofons_parser.GetProductParams(productUrl)
        elif productUrl.find("videogsm") != -1:
            productParams = videogsm_parser.GetProductParams(productUrl)
        elif productUrl.find("shpionam") != -1:
            productParams = shpionamnet_parser.GetProductParams(productUrl)
        elif productUrl.find("podavitel") != -1:
            productParams = podavitel_parser.GetProductParams(productUrl)
        else:
            return
    except Exception as exception:
        if exception.message == "out of stock":
            logging.LogInfoMessage(u"Товар '{}' отсутствует".format(unicode( productUrl)))
        else:
            print exception.message
            logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        return

    calculatedPrice = int(10 * round(float(float(ourPrice) / 0.7)/10))

    if category not in [u"GSM охрана_Дополнительные датчики"]:
        newPrice = calculatedPrice
        if int(price) >  newPrice or int(price) - int(ourPrice) > 10000 :
            newPrice = int(price)
    else:
        newPrice = int(price)
        productParams["name"] = commonLibrary.DeleteQuotes(productParams["name"])

    if newPrice % 1000 == 0:
        if calculatedPrice < int(price):
            newPrice += 10
        else:
            newPrice -= 10

    productParams["active"] = "1"
    productParams["price"] = newPrice
    productParams["wholesale_price"] = ourPrice
    productParams["meta_title"] = productParams["name"]

    productParams["article"] = "lg"

##    print productParams["name"]
    productParams["name"] = GetProductName(productParams["name"], productUrl)
    print productParams["name"]
    productId, mes = prestashop_api.Parser_ProcessProduct(productParams, category, "logos", shopName, productUrl, message=message)
    message = mes
    print productId
    if productId != False:
        updatedProducts.append(int(productId))

#################################################################################################################

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\logos\\log", "logos")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()
message = ""


##for file in os.listdir("."):
##    if file.find(".xls") != -1:
##        os.remove(file)

priceFiles = ["price-logos.xlsx", "price-logos.xls"]
##priceFiles = commonLibrary.GetContractorPrice("lg")
##if priceFiles == -1:
##    sys.exit(0)

xlsxPriceName = priceFiles[0]

urls = {}
products = []

xlsPriceName = priceFiles[1]
workbook = xlrd.open_workbook(xlsPriceName, formatting_info=True)
worksheet = workbook.sheet_by_index(0)
num_rows = worksheet.nrows - 1

wb = load_workbook(filename = xlsxPriceName)
ws = wb.active

for row in ws.iter_rows():
    for cell in row:
        if type(cell) == openpyxl.cell.cell.MergedCell:
            continue
        if type(cell.internal_value) is unicode or type(cell.internal_value) is str:
            if cell.internal_value.find(u"=HYPERLINK") != -1:
                parts = cell.internal_value.split("\"")
                productName = parts[3]
                url = parts[1]
                url = CorrectUrl(productName, url)
                urls[productName] = url

            link = worksheet.hyperlink_map.get((row[0].row - 1, 1))
            url = '(No URL)' if link is None else link.url_or_path
            if url != "(No URL)":
                productName = row[1].internal_value
                url = CorrectUrl(url, productName)
                urls[productName] = url

category = ""
bigCategory = ""

for i in range(num_rows):
    rowValues = worksheet.row_values(i, start_colx=0, end_colx=10)
    if rowValues[1] != "" and rowValues[5] == "":
        if category == u"Дополнительные датчики":
            nextrowValues = worksheet.row_values(i + 1, start_colx=0, end_colx=10)
            if nextrowValues[0] != "":
                category = ""
                continue
        else:
            category = rowValues[1]
            prevRowValues = worksheet.row_values(i - 1, start_colx=0, end_colx=3)
            if prevRowValues[0] != "":
                bigCategory = prevRowValues[0]
##                print bigCategory
            continue

    if bigCategory in excludedBigCategories or category in excludedCategories:
        continue

    if (rowValues[1] != "" and rowValues[5] != "") or (category == u"Дополнительные датчики"):
        itemName = rowValues[1]
        if itemName.find("\"") != -1:
            itemName = itemName[0:itemName.find("\"")].strip()

        if urls.has_key(itemName) == False:
            continue

        url = urls[itemName]
        if url == 'http://www.shpionam.net/bluetooth-voice-changer.html':
            pass
        rowValues = worksheet.row_values(i, start_colx=0, end_colx=10)
        if category == u"Дополнительные датчики":
            products.append([url, rowValues[4], 0, bigCategory + "_" + category])
        else:
            if rowValues[4] == "" or rowValues[6] == "" :
                continue
            products.append([url, rowValues[4], rowValues[6], bigCategory + "_" + category])


##################################################

for site in commonLibrary.sitesParams:
    if site["url"] not in [
                            "http://safetus.ru",
                            "http://omsk.knowall.ru.com",
                            "http://glushilki.ru.com",
                            "http://usiliteli-svyazi.ru",
                            "http://mini-camera.ru.com"
                            ]:
        continue

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"], logFilePath)

    updatedProducts = []
    shopName = ""

    logging.LogInfoMessage("============== " + prestashop_api.siteUrl + " ==============")

    prestashop_api.InitAllProducts()

    for product in products:
        productUrl, price, wholesalePrice, category = product
        if productUrl.find("avtocamera.com") != -1 or productUrl.find("455") != -1 or productUrl.find("kopeyka.info") != -1:
            continue

        if site["url"]  == "http://glushilki.ru.com":
            if productUrl.find("shpionam") == -1 and productUrl.find("podavitel") == -1:
                continue
        if site["url"]  == "http://mini-camera.ru.com":
            if category.find(u"Видеонаблюдение") != -1:
                if productUrl.find("mini") == -1 and productUrl.find("micro") == -1 and productUrl.find("mikro") == -1:
                    continue
            else:
                continue

        if site["url"]  == "http://usiliteli-svyazi.ru" and category.find(u"Репиторы (усилители связи)") == -1:
            continue

        ProcessProductPrestashop (productUrl, price, wholesalePrice, category)

#####################################################################################

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("lg")
    for id in ids:
        if id not in updatedProducts:
            name = prestashop_api.GetProductOriginalName(id)
            active = prestashop_api.IsProductActive(id)
            if active:
                prestashop_api.MakeProductUnavailableForAllShops(id)
            else:
                prestashop_api.DeleteProduct(id)
            logging.LogInfoMessage(u"Товар '{}' ( {} ) деактивирован".format(name, id))

    if machineName.find("hp") != -1:
        ssh.kill()


result = mails.SendInfoMail("Обновление товаров: Логос", "", [logFilePath])
