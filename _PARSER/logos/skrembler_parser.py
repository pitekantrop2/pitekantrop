﻿import requests
from lxml import etree
import urllib
import PrestashopAPI
import log
import datetime
import HTMLParser
import re

def GetProductName(productPageContent):
    try:
        titleElem = productPageContent.xpath("//h1")[0].xpath(".//text()")
        productName = ""
        for i in range(len( titleElem)):
            if i == 1: break
            part = titleElem[i]
            productName += unicode(part) + " "

        return productName.strip()
    except:
        return None

def GetProductParams(productUrl):

    resp = requests.get(productUrl)
    productPageContent = etree.HTML(resp._content)
    print etree.tostring(productPageContent)
    productParams = {}

    productParams["images"] = []
    img = productPageContent.xpath("//table//table//*[@id = 'image_poster']")
##    productParams["images"].append("http://www.skrembler.ru" + img[0].attrib['href'])

    #название
    productParams["name"] = GetProductName(productPageContent)

    #описание
    productParams["description"] = MakeProductDescription(productPageContent, productParams["name"])

    return productParams


def MakeProductDescription(productPageContent, productName):

    description = ""

    scripts = productPageContent.xpath("//script")
    for script in scripts:
        script.getparent().remove(script)

    iframes = productPageContent.xpath("//iframe")
    for iframe in iframes:
        iframe.getparent().remove(iframe)

    aa = productPageContent.xpath("//a")
    for a in aa:
        if "onclick" in a.attrib.keys():
            a.getparent().remove(a)

    tds = productPageContent.xpath("//td")
    for td in tds:
        td.attrib['style'] = "border-width:0px;"

    div = productPageContent.xpath("//div[@id = 'podrobys']")
    if len(div) != 0:
        description += etree.tostring(div)

    div = productPageContent.xpath("//div[@id = 'osobenosys']")
    if len(div) != 0:
        description += etree.tostring(div)



    features = productPageContent.xpath("//p[contains(.,'" + u"Особенности" + "')]")
    params = productPageContent.xpath("//p[contains(.,'" + u"Технические"  + "')]")
    grade = productPageContent.xpath("//p[contains(.,'" + u"Комплект" + "') ]")
    princip = productPageContent.xpath("//p[contains(.,'" + u"Принцип работы" + "') ]")
    oblast = productPageContent.xpath("//p[contains(.,'" + u"Область применения" + "') ]")


    if len(features) != 0:
        description = description.replace(etree.tostring(features[0]),u"<p><br /><br /></p><h3>Спецификация товара \"" +  productName +  "\"</h3>")
    if len(params) != 0:
        description = description.replace(etree.tostring(params[0]),u"<p><br /><br /></p><h3>Технические характеристики товара \"" +  productName +  "\"</h3>")
    if len(grade) != 0:
        description = description.replace(etree.tostring(grade[0]),u"<p><br /><br /></p><h3>Комплектация товара \"" +  productName +  "\"</h3>")
    if len(princip) != 0:
        description = description.replace(etree.tostring(princip[0]),u"<p><br /><br /></p><h3>Принцип работы товара \"" +  productName +  "\"</h3>")
    if len(oblast) != 0:
        description = description.replace(etree.tostring(oblast[0]),u"<p><br /><br /></p><h3>Область применения товара \"" +  productName +  "\"</h3>")

    description = description.replace("""justify""", """left""")
    description = description.replace("<img","<img1")
    description = description.replace("<a","<a1")

    h = HTMLParser.HTMLParser()
    regexp = "&.+?;"
    list_of_html = re.findall(regexp, description) #finds all html entites in page
    for e in list_of_html:
        unescaped = h.unescape(e) #finds the unescaped value of the html entity
        description = description.replace(e, unescaped) #replaces html entity with unescaped value

    description = PrestashopAPI.DeleteCommentsFromHtml(description)

##    print description

    return description

#===============================================================================

