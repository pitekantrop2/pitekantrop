﻿import requests
from lxml import etree
import urllib
import PrestashopAPI
import log
import datetime
import HTMLParser
import re
import commonLibrary

def GetProductName(productPageContent):
    try:
        span = productPageContent.xpath("//h1//span")
        for s in span:
            s.getparent().remove(s)
        novelties = [u"новинка {0}".format(x) for x in range(2016,2025)]
        novelties += [u"новинка {0}!".format(x) for x in range(2016,2025)]
        novelties += [u"Новинка {0}!".format(x) for x in range(2016,2025)]
        novelties += [u"Новинка {0}".format(x) for x in range(2016,2025)]
        titleElem = productPageContent.xpath("//h1")[0].xpath(".//text()")
        parts = [unicode(x) for x in titleElem if x.lower() not in novelties]
        productName = ""
        for part in parts:
            productName += part + " "
            if part.find("\"") != -1 or part.find(u"«") != -1:
                break
        for novelty in novelties:
            productName = productName.replace(novelty, "")
        productName = productName.strip().strip("-").strip()
        productName = productName.replace("\r\n", " ")
        productName = productName.replace("  ", " ")
        return productName
    except:
        return None


def GetProductParams(productUrl):
    resp = requests.get(productUrl)
    if resp.status_code == 404:
        raise Exception('out of stock')

    productPageContent = etree.HTML(resp._content)

    productPageText = commonLibrary.UnescapeText(etree.tostring(productPageContent))
    if productPageText.find(u"Вы можете оформить предзаказ.") != -1 or productPageText.find(u"ТОВАР ОТСУТСТВУЕТ") != -1:
        raise Exception('out of stock')

    productParams = {}

    #название
    productParams["name"] = GetProductName(productPageContent)

    #описание
    productParams["description"] = MakeProductDescription(productPageContent, productParams["name"])

    productParams["images"] = []

    return productParams


def MakeProductDescription(productPageContent, productName):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    description = ""

    scripts = productPageContent.xpath("//script")
    for script in scripts:
        script.getparent().remove(script)

    td = productPageContent.xpath("//table/tbody[count(tr) = 2]//td[.//iframe]")
    if len(td) > 0:
        table = td[0].getparent().getparent().getparent()
        table.getparent().remove(table)

    iframes = productPageContent.xpath("//iframe")
    for iframe in iframes:
        iframe.getparent().remove(iframe)

    aa = productPageContent.xpath("//a")
    for a in aa:
        a.getparent().remove(a)

    tds = productPageContent.xpath("//td")
    for td in tds:
        td.attrib['style'] = "border-width:0px;"

    imgs = productPageContent.xpath("//img")
    for img in imgs:
        img.getparent().remove(img)

    startNode = productPageContent.xpath("//div[contains(@style, 'padding: 2px;')]")[0]
    while True:
        if "class" in startNode.attrib.keys() and startNode.attrib["class"] == "tabs":
            break
        description += etree.tostring(startNode)
        startNode = startNode.getnext()

    tds = productPageContent.xpath("//td")
    for td in tds:
        td.attrib['style'] = "border-width:0px;"

    tables = productPageContent.xpath("//table")
    for table in tables:
        table.attrib['style'] = "border-width:0px;"

    productPageContent = commonLibrary.MakeH3Titles(productPageContent)

    divs = productPageContent.xpath("//div[contains(@id, 'tab')]")
    for i in range(len(divs)):
        div = divs[i]
        titles_ = [u"Особенности", u"Область",  u"Характеристики", u"Компл", u"Технические" ]
        for title_ in titles_:
            elems = div.xpath(u".//*[starts-with(text(), '{}')]".format(title_))
            if len(elems) != 0:
                div.attrib['style'] = "display:block;"
                description += commonLibrary.GetStringDescription(div)
                break

    description = commonLibrary.GetStringDescription(description)
    description = commonLibrary.DeleteCommentsFromHtml(description)

##    print description

    return description

#===============================================================================

