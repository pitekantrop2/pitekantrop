﻿import requests
from lxml import etree
import urllib
import log
import datetime
import HTMLParser
import re
import PrestashopAPI
import commonLibrary

def GetProductName(productPageContent):
    try:
        span = productPageContent.xpath("//h1//span")
        for s in span:
            s.getparent().remove(s)
        novelties = [u"новинка {0}".format(x) for x in range(2016,2025)]
        novelties += [u"новинка {0}!".format(x) for x in range(2016,2025)]
        novelties += [u"Новинка {0}!".format(x) for x in range(2016,2025)]
        novelties += [u"Новинка {0}".format(x) for x in range(2016,2025)]
        titleElem = productPageContent.xpath("//h1")[0].xpath(".//text()")
        parts = [unicode(x) for x in titleElem if x.lower() not in novelties]
        productName = ""
        for part in parts:
            productName += part + " "
            if part.find("\"") != -1 or part.find(u"«") != -1:
                break
        for novelty in novelties:
            productName = productName.replace(novelty, "")
        productName = productName.strip().strip("-").strip()
        productName = productName.replace("\r\n", " ")
        productName = productName.replace("  ", " ")
        return productName
    except:
        return None

def GetProductParams(productUrl):
    resp = requests.get(productUrl)
    if resp.status_code == 404:
        raise Exception('out of stock')

    productPageContent = etree.HTML(resp.text)

    productParams = {}

    #название
    productParams["name"] = GetProductName(productPageContent)

    #описание
    productParams["description"] = MakeProductDescription(productPageContent, productParams["name"])

    productParams["images"] = []

    return productParams


def MakeProductDescription(productPageContent, productName):
    description = ""
    productPageContent = commonLibrary.CorrectHtml(productPageContent)

    scripts = productPageContent.xpath("//script")
    for script in scripts:
        script.getparent().remove(script)

    td = productPageContent.xpath("//table/tbody[count(tr) = 2]//td[.//iframe]")
    if len(td) > 0:
        table = td[0].getparent().getparent().getparent()
        table.getparent().remove(table)

    iframes = productPageContent.xpath("//iframe")
    for iframe in iframes:
        iframe.getparent().remove(iframe)

    aa = productPageContent.xpath("//a")
    for a in aa:
        a.getparent().remove(a)

    gift = productPageContent.xpath("//div[@class = 'alert-gift']")
    for g in gift:
        g.getparent().remove(g)

    elements = productPageContent.xpath("//ul//br")
    for element in elements:
        element.getparent().remove(element)

    imgs = productPageContent.xpath("//img")
    for img in imgs:
        img.getparent().remove(img)

    divs = productPageContent.xpath(u".//div[contains(@style, '{}') and contains(.,'{}')]".format("border-radius: 10px 10px 10px 10px;", u"Не хватает дальности"))
    for div in divs:
        div.getparent().remove(div)

    startP = productPageContent.xpath("//tr//td[@style = 'padding-bottom: 5px;']//descendant::*")[0]

    while startP != None and startP.tag != "div" :
        description += etree.tostring(startP)
        startP = startP.getnext()

    tds = productPageContent.xpath("//td")
    for td in tds:
        td.attrib['style'] = "border-width:0px;"

    productPageContent = commonLibrary.MakeH3Titles(productPageContent)

    divs = productPageContent.xpath("//div[contains(@id, 'tab')]")
    for i in range(len(divs)):
        div = divs[i]
        titles_ = [u"Особенности", u"Область",  u"Характеристики", u"Компл", u"Технические" ]
        for title_ in titles_:
            elems = div.xpath(u".//*[starts-with(text(), '{}')]".format(title_))
            if len(elems) != 0:
                div.attrib['style'] = "display:block;"
                tables = div.xpath(u".//table[not(contains(.,'{}'))]".format(u"Комплект"))
                for table in tables:
                    table.getparent().remove(table)
                description += commonLibrary.GetStringDescription(div)
                break

    description = description.replace("justify", "left")

    description = commonLibrary.UnescapeText(description)
    description = commonLibrary.DeleteCommentsFromHtml(description)

##    print description

    return description

#===============================================================================

