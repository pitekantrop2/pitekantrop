﻿import requests
import lxml
from lxml import etree
import urllib
import dbForParser
import InsalesForParser
import log
import datetime
import random
import PrestashopAPI


def ProcessProductsOnPage(products, categoryName):

    for product in products:

        productUrl = product.xpath(".//div[@class = 'prdbrief_name']/a")[0].attrib['href']
        print productUrl

        resp = requests.get(baseUrl + productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, categoryName)

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = productPageContent.xpath("//span[@class = 'totalPrice']")[0].text.strip()
    productParams["name"] = productPageContent.xpath("//div[@class = 'cpt_product_name']/h1")[0].text.strip().lower().capitalize() \
    .replace(u"оптом","").replace(u"!скидки не действуют!","").strip()

    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["images"] = []
    #main image
    productParams["images"].append(baseUrl + productPageContent.xpath("//img[@id = 'img-current_picture']")[0].attrib['src'])
    #other images
    images = productPageContent.xpath("//table[@id = 'box_product_thumbnails']//tr//td//a")
    for image in images:
        productParams["images"].append(baseUrl + image.attrib['img_picture'])

    strproductPrice = productPrice.split(" ")[0]
    productParams["wholesale_price"] = int(strproductPrice)
    productParams["price"] = int(productParams["wholesale_price"] * 1.8)
    productParams["price"] = int(10 * round(float(productParams["price"])/10))

    return productParams


def MakeProductDescription(productPageContent):
    productDescription = productPageContent.xpath("//div[@class = 'cpt_product_description']/div")[0]

    description =  etree.tostring(productDescription).replace(u"&#1086;&#1087;&#1090;&#1086;&#1084;","")

    return description


def ProcessProductPrestashop(productPageContent, categoryName):

    try:
        productParams = GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product")
        return

    productParams["active"] = "1"
    productParams["category"] = categoryName
    productParams["meta_title"] = shopName + " - " + productParams["name"]

    prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], "z29", shopName, False)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\z29\\log","z29")
logging = log.Log(logFilePath)


prestashop_api = PrestashopAPI.PrestashopAPI("http://amazing-things.ru", 'SPYWHH8AF494V4S96EKN1GGUTQSEBWJI', "127.0.0.1", "amazing_things", "am_th_user_ex", "1qaz@WSX", 5555, logFilePath)
shopName = "Amazing Things"

prestashop_api.InitAllProducts()

baseUrl = "http://z29.ru"
resp = requests.get(baseUrl)
tree = etree.HTML(resp._content)

categories = tree.xpath("//div[@class = 'cpt_category_tree']//ul/li/a[not(contains(.,'" + u"Все товары" + "'))]")

for category in categories:

    categoryName = category.text.strip()
    print categoryName
    categoryLink = category.attrib['href']

    url = baseUrl + categoryLink + "all"
    resp = requests.get(url)
    categoryContent = etree.HTML(resp._content)

    products = categoryContent.xpath("//form[@class = 'product_brief_block']")

    ProcessProductsOnPage(products, categoryName)
