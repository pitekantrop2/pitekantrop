﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys

def ProcessProductsOnPage(products):

    for product in products:

        productUrl = baseUrl + product.attrib['href']

        print productUrl

        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl)

def GetProductParams(productPageContent):

    productParams = {}
    productPrice = productPageContent.xpath("//div[@class = 'buy-block__price__new']")[0].text.strip().replace(u" руб.", "").replace(u" ", "")

    if productPrice[0].isdigit() == False:
        raise Exception('unknown price')

    productParams["name"] = productPageContent.xpath("//h1[@class = 'page__title']")[0].text.strip()

    productParams["categories"] = [ x.text.strip() for x in productPageContent.xpath("//div[@class = 'bx-breadcrumb-item']/a/span")]
    productParams["categories"].pop(0)
    productParams["categories"].pop(0)

##    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent, productParams["name"])

    images = productPageContent.xpath("//ul[@id = 'gallery__slider']//li//a/img")
    productParams["images"] = [ baseUrl + x.attrib['src'] for x in images]

    price = int(10 * round(float(float(productPrice) * 1.05)/10))
    if price % 1000 == 0:
        price -= 10

    productParams["price"] = price
    productParams["wholesale_price"] = int(float(productPrice) * 0.8)

    return productParams


def MakeProductDescription(productPageContent, productName):

    description = ""

    descr = productPageContent.xpath("//div[contains(@class, 'content__item c-desc')]")[0]
    description += etree.tostring(descr)

    description += u"<p><br /><br /></p><h3>Технические характеристики товара \"" +  productName +  "\"</h3><br />"

    params = productPageContent.xpath("//div[contains(@class, 'content__item c-params')]")[0]

    description += etree.tostring(params)

    description += u"<p><br /><br /></p><h3>Комплектация товара \"" +  productName +  "\"</h3><br />"

    complect = productPageContent.xpath("//div[contains(@class, 'content__item c-complect')]")[0]

    description += etree.tostring(complect)

    description = commonLibrary.UnescapeText(description)
    description = PrestashopAPI.DeleteCommentsFromHtml(description)
    description = description.replace("justify", "left")
    description = description.replace("<img","<img1")
    description = description.replace("<a","<a1")
    description = description.replace("<h2>","<h3>")
    description = description.replace("</h2>","</h3>")

    return description


def ProcessProductPrestashop(productPageContent, productUrl):

    try:
        productParams = GetProductParams(productPageContent)
    except Exception as exception:
        if exception.message == "unknown price":
            logging.LogErrorMessage("Product '{0}' has unknown price".format(productUrl))
        else:
            logging.LogErrorMessage("Unable to parse product {0}".format(productUrl))
        return

##    logging.Log(productParams["name"])

    if len(productParams["categories"]) > 1:
        parentCategoryName = productParams["categories"][0]
        categoryName = productParams["categories"][1]

        category = parentCategoryName + "_batman"

        categoryId = prestashop_api.GetCategoryID(category)
        if categoryId == -1:
            result = prestashop_api.AddCategoryDefault(category, "batman", 0, shopName)
            if result == False:
                logging.LogErrorMessage(u"Не удалось добавить категорию: " + category)
                return
            else:
                logging.LogInfoMessage(u"Добавлена категория: " + category)

    else:
        categoryName = productParams["categories"][0]
        parentCategoryName = "batman"

    productParams["active"] = "1"
    productParams["category"] = categoryName
    productParams["meta_title"] = shopName + " - " + productParams["name"]

    productParams["article"] = "mp"

    prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], category, shopName, productUrl, False)

#===============================================================================

commonLibrary.KillPutty()


logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\batman\\log", "batman")
logging = log.Log(logFilePath)
h = HTMLParser.HTMLParser()


sites = [
{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX"}
]

for site in sites:

    ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    shopName = ""
    if site["url"] == "http://omsk.knowall.ru.com":
        shopName = u"Новалл"
    else:
        shopName = u"Safetus"


    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    prestashop_api.InitAllProducts()

    baseUrl = "http://www.megaprosto.ru"

    resp = requests.get(baseUrl)
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//ul[@id = 'horizontal-multilevel-menu']/li")

    for category in categories:

        categoryUrl = category.xpath("./a")[0].attrib["href"]
        categoryName = category.xpath("./a")[0].text.strip()

        if categoryName not in [u"Радиостанции", u"Судовое оборудование", u"Туризм"]:
            continue

        subCategories = category.xpath("./ul//li")

        for subCategory in subCategories:

            subCategoryUrl = subCategory.xpath("./a")[0].attrib["href"]
            subCategoryName = subCategory.xpath("./a")[0].text.strip()

            if subCategoryName not in [u"Портативные рации", u"Эхолоты"]:
                continue

            subSubCategories = subCategory.xpath("./ul//li")
            sortParams = "/?sort=nalichie&lim=10000"

            if len(subSubCategories) == 0:

                if subCategoryName in [u"Портативные рации"]:
                    sortParams += "&set_filter=y&arrFilter_2_1925008924=Y&arrFilter_2_3095555881=Y&arrFilter_2_1356139133=Y&arrFilter_2_2574065104=Y&arrFilter_2_482495853=Y&arrFilter_2_1859602134=Y"

                if subCategoryName in [u"Эхолоты"]:
                    sortParams += "&set_filter=y&arrFilter_2_2562812255=Y&arrFilter_2_4250589019=Y&arrFilter_2_1090544110=Y"

                resp = requests.get(baseUrl + subCategoryUrl + sortParams)
                categoryContent = etree.HTML(resp._content)

                products = categoryContent.xpath("//div[@class = 'item__name-wrap']/a[@class = 'item__name']")

                ProcessProductsOnPage(products)

            else:

                for subSubCategory in subSubCategories:

                    subCategoryUrl = subSubCategory.xpath("./a")[0].attrib["href"]
                    subCategoryName = subSubCategory.xpath("./a")[0].text.strip()

                    resp = requests.get(baseUrl + subCategoryUrl + sortParams)
                    categoryContent = etree.HTML(resp._content)

                    products = categoryContent.xpath("//div[@class = 'item__name-wrap']/a[@class = 'item__name']")

                    ProcessProductsOnPage(products)


    ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: batman", "", [logFilePath])