﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import re
import SendMails
import subprocess
import time
import commonLibrary
import os
import xlrd

articles = ["DX", "LS", "UP", "CH", "AN", "AR", "GB", "GF", "GC1", "GC2", "GP2", "EGS", "GH", "SM", "GE4", "D30"]

def GetProductName(productPageContent):
    name = productPageContent.xpath("//h1")[0].text.strip()
    if name.find(u"GC1-40 с пультом ДУ") != -1:
        return u"GC1-40 с пультом ДУ"
    parts = name.split(" ")
    for article in articles:
        for part in parts:
            if part.startswith(article):
                return part
    print u"art not found: " + name
    return -1

def MakeProductName(article):
    if article == "LS-107":
        article = "LS107"
    if article == "LS-977":
        article = "LS-977S"
    if article == "LS-925":
        return u"ЭкоСнайпер - LS-925"

    productName = ""
    names = [u"ЭкоСнайпер", u"Экоснайпер", ""]
    for name in names:
        if name != "":
            tempName = u"{0} {1}".format(name, article)
        else:
            tempName = article
        id_product = prestashop_api.GetProductIdQuick(tempName)
        if id_product != -1:
            productName = tempName
            break

    if productName == "":
        print u"Новый товар"
        productName = u"Экоснайпер {0}".format(article)

    return productName

def ProcessProductsOnPage(products, categoryName):
    for product in products:
        productUrl = baseUrl + product.attrib['href']

        print productUrl

        resp = requests.get(productUrl, verify=False)
        productPageContent = etree.HTML(resp.text)

        ProcessProductPrestashop(productPageContent, productUrl, categoryName)

def GetProductParams(productPageContent):
    if len(productPageContent.xpath("//span[@id = 'price-field']")) > 1:
        raise Exception('out of stock')
    productParams = {}
    price = productPageContent.xpath("//span[@id = 'price-field']")[0].text.split(" ")[0]
    productParams["price"] = commonLibrary.GetIntPriceFromText(price)
    productParams["name"] = GetProductName(productPageContent)
    if productParams["name"] == -1:
        raise Exception()

    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)
    productParams["images"] = [ x.attrib['href'] for x in productPageContent.xpath("//div[@class = 'preview-images']/a")]

    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = ""

    descriptionElem = productPageContent.xpath("//div[contains(@class, 'description')]")[0]
    imgs = descriptionElem.xpath(".//img")
    for img in imgs:
        img.attrib["src"] = baseUrl + img.attrib["src"]

    tags = ["h2", "a", "form", "iframe" ]
    for tag in tags:
        elems = descriptionElem.xpath(".//{0}".format(tag))
        for elem in elems:
            elem.getparent().remove(elem)

    texts = [u'Гарантия 5 лет', u'Сертифицированные отпугиватели', u"Обзор отпугивателя"]
    for text in texts:
        elems = descriptionElem.xpath(u".//p[contains(., '{0}')]".format(text))
        for elem in elems:
            elem.getparent().remove(elem)

    productDescription += etree.tostring(descriptionElem)
    productDescription = commonLibrary.GetStringDescription(productDescription)
    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl, categoryName):
##    if productUrl not in ["https://www.eco-sniper.ru/product/universalnyy-otpugivatel-letuchih-myshey-ptits-kunits-ls-928"]:
##        return
    if prestashop_api.siteUrl in ["http://insect-killers.ru"]:
        if categoryName not in [
                            u"Уничтожители летающих насекомых"
                            ]:
            return

    if prestashop_api.siteUrl in ["http://otpugivateli-grizunov.ru"]:
        if categoryName not in [u"Отпугиватели мышей, крыс",
                            u"Отпугиватели кротов",
                            u"Отпугиватели тараканов, муравьев, пауков, мух, клопов, блох, моли",
                            u"Отпугиватели змей"
                            ]:
            return

    if prestashop_api.siteUrl in ["http://otpugivateli-krotov.ru"]:
        if categoryName not in [u"Отпугиватели кротов",
                                u"Отпугиватели змей"
                            ]:
            return

    if prestashop_api.siteUrl in ["http://otpugivateli-ptic.ru"]:
        if categoryName not in [u"Отпугиватели птиц"
                            ]:
            return

    if prestashop_api.siteUrl in ["http://otpugivateli-sobak.ru"]:
        if categoryName not in [u"Отпугиватели собак"
                            ]:
            return

    try:
        productParams = GetProductParams(productPageContent)
    except Exception as exception:
        if exception.message == "out of stock":
            logging.LogErrorMessage("Product '" + productUrl + "' out of stock")
        else:
            print exception.message
            logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        return

    if productParams["name"] in priceProducts.keys():
        wholesale_price = priceProducts[productParams["name"]]
    else:
        print "price not found: " + productParams["name"]
        return

    productParams["name"] = MakeProductName(productParams["name"])

    productParams["active"] = "1"
    price = int(10 * round(float(float(wholesale_price) / 0.70)/10))
    if price > productParams["price"]:
        productParams["price"] = price
    if productParams["price"] % 1000 == 0:
        productParams["price"] -= 10
    productParams["wholesale_price"] = wholesale_price
    productParams["meta_title"] = productParams["name"]
    productParams["article"] = "v"

    print productParams["name"], productParams["price"]
##    return

    productId = prestashop_api.Parser_ProcessProduct(productParams, categoryName, u"Ecosniper", "", productUrl)
    if productId != False:
        updatedProducts.append(int(productId))

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\31vek\\log", "31vek")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()
prestashop_api = PrestashopAPI.PrestashopAPI()
mails = SendMails.SendMails()

params = {"FromAddress" : u"feedback@otpugivatel.com", "Password" : "Tabulfsam198"}

emails = mails.GetMailsWithNums(params, u"прайс", "man1@eco-sniper.ru")
for email in emails:
    num, msg = email
    files = mails.GetMailAttachments(msg, ".")
    for fileName in files:
        if fileName.endswith(".xls"):
            price = fileName
            mails.DeleteEmail(params, num)
        else:
            os.unlink(fileName)

priceProducts = {}

workbook = xlrd.open_workbook(price, formatting_info=True)
sheet = workbook.sheets()[0]
num_rows = sheet.nrows - 1
for i in range(6, num_rows):
    rowValues = sheet.row_values(i, start_colx=0, end_colx=6)
    try:
        name = rowValues[0].strip()
    except:
        pass
    price = rowValues[4]
    if price != "" and name not in priceProducts.keys():
        priceProducts[name] = int(float(price))

for site in commonLibrary.sitesParams:
    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"], logFilePath)

    logging.LogInfoMessage("============" + prestashop_api.siteUrl + "=================")

    updatedProducts = []

    prestashop_api.InitAllProducts()

    baseUrl = "https://www.eco-sniper.ru"
    resp = requests.get(baseUrl, verify=False)
    tree = etree.HTML(resp.text)

    categories = tree.xpath("//div[@class = 'categories']//a")

    for category in categories:
        categoryUrl = category.attrib['href']
        categoryName = category.xpath(".//text()")[0]

        if categoryUrl.split("/")[-1] in [u"dlya-dachnikov", u"antimoskitnaya-setka", u"soputstvuyushie-tovary", u"dlya-mangala", u"sadovyy-inventar", u"dachnyy-otdyh", u"korziny-yaschiki"]:
            continue

        url = (baseUrl + categoryUrl).strip()
        resp = requests.get(url, verify=False)
        categoryContent = etree.HTML(resp.text)
        products = categoryContent.xpath("//div[@class = 'product-block']/a[1]")
        ProcessProductsOnPage(products, categoryName)

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("v")
    for id in ids:
        if id not in updatedProducts:
            name = prestashop_api.GetProductOriginalName(id)
            active = prestashop_api.IsProductActive(id)
            if active:
                prestashop_api.MakeProductUnavailableForAllShops(id)
            else:
                prestashop_api.DeleteProduct(id)
            logging.LogInfoMessage(u"Товар '{}' ( {} ) деактивирован".format(name, id))

    if machineName.find("hp") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: 31 век", "", [logFilePath])