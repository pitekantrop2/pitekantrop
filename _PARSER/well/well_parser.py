﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys

def ProcessProductsOnPage(products, categoryName):
    for product in products:
        productUrl = baseUrl + product.attrib['href']

        print productUrl

        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl, categoryName)

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = productPageContent.xpath("//span[@itemprop = 'price']")[0].text.strip()
    productParams["name"] = productPageContent.xpath("//h1")[0].text.strip()

##    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)
    productParams["images"] = [ baseUrl + x.attrib['style'].split(")")[0].replace("background:url(", "").replace("resize_cache/", "").replace("40_40_1/","") for x in productPageContent.xpath("//div[@class = 'd_mf']/a")]

    productParams["price"] = productPrice
    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = ""

    descriptionElem = productPageContent.xpath("//div[contains(@class, 'd_desc')]")[0]
    imgs = descriptionElem.xpath(".//img")
    for img in imgs:
        img.attrib["src"] = baseUrl + img.attrib["src"]

    iframes = descriptionElem.xpath(".//iframe")
    for iframe in iframes:
        if "style" in iframe.attrib.keys():
            del iframe.attrib["style"]

    forms = descriptionElem.xpath(".//form")
    for form in forms:
        form.getparent().remove(form)

    aa = descriptionElem.xpath(".//a")
    for a in aa:
        a.getparent().remove(a)

    productDescription += etree.tostring(descriptionElem)
    propertiesTable = productPageContent.xpath("//div[@class = 'd_props first']//table")[0]
    productDescription += etree.tostring(propertiesTable)

    productDescription = commonLibrary.GetStringDescription(productDescription)

    productDescription = productDescription.replace("'","")
    productDescription = productDescription.replace(u"\xa0","")

    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl, categoryName):
    try:
        productParams = GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product")
        return

##    if productUrl.find("https://well-we.ru/catalog/oskolkobezopasnye-lampy/oskolkobezopasnaya-20w-220/") == -1:
##        return

##    print productParams["name"]
    if productParams["name"].find(u"осколкозазищенная") != -1:
        productParams["name"] = productParams["name"].replace(u"осколкозазищенная", u"осколкозащищенная")

    productParams["wholesale_price"] = int(float(productParams["price"]) * 0.85)
    price = int(10 * round(float(float(productParams["price"]) * 1.214)/10))

    productParams["active"] = "1"
    productParams["category"] = categoryName
    if categoryName in [u"Промышленные высоковольтные", u"Бытовые ловушки"]:
        productParams["name"] = u"Уничтожитель насекомых (комаров, мух, мошек) \"{0}\"".format( productParams["name"])
    if categoryName in [u"Декоративные с клеевой пластиной", u"Промышленные с клеевой основой"]:
        productParams["name"] = u"Ловушка насекомых (мух, комаров, мошек) \"{0}\"".format( productParams["name"])

    productParams["meta_title"] = productParams["name"]

    if price % 1000 == 0:
        price -= 10

    productParams["price"] = price
    productParams["article"] = "ww"

    productId = prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], "well", "", productUrl)
    if productId != False:
        updatedProducts.append(int(productId))

#===============================================================================
prestashop_api = PrestashopAPI.PrestashopAPI()
logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\well\\log", "well")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:
    if site["url"] != "http://insect-killers.ru":
        continue

    prestashop_api, ssh = commonLibrary.CreatePrestashopSession(site["url"],logFilePath)

    logging.LogInfoMessage("=================" + prestashop_api.siteUrl + "===================")

    updatedProducts = []

    prestashop_api.InitAllProducts()

    baseUrl = "https://well-we.ru"
    resp = requests.get(baseUrl)
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//div[@class = 'main-nav__item']/a")

    for category in categories:
        categoryName = u""
        nameArr = category.xpath(".//text()")
        for namePart in nameArr:
            categoryName += u"{0} ".format(namePart)
        categoryName = categoryName.strip()
##        print categoryName
        categoryUrl = category.attrib['href']

        url = (baseUrl + categoryUrl).strip()
        resp = requests.get(url)
        categoryContent = etree.HTML(resp._content)

        products = categoryContent.xpath("//div[@class = 'p_name' or @class = 'name']/a")

        ProcessProductsOnPage(products, categoryName)

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("ww")
    for id in ids:
        if id not in updatedProducts:
            name = prestashop_api.GetProductOriginalName(id)
            active = prestashop_api.IsProductActive(id)
            if active:
                prestashop_api.MakeProductUnavailableForAllShops(id)
            else:
                prestashop_api.DeleteProduct(id)
            logging.LogInfoMessage(u"Товар '{}' ( {} ) деактивирован".format(name, id))

    if machineName.find("hp") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Велл", "", [logFilePath])