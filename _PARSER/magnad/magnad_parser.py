﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys
import copy

def GetProductName(productPageContent):
    try:
        span = productPageContent.xpath("//h1[@class = 'hdr1']/span")
        for s in span:
            s.getparent().remove(s)
        titleElem = productPageContent.xpath("//h1[@class = 'hdr1']")[0].xpath(".//text()")
        productName = ""
        for part in titleElem:
            productName += part + " "
            if part.find("\"") != -1 or part.find(u"«") != -1:
                break
        chars = [u" - ", u" - ", u": ", u" (", u" – "]
        for char in chars:
            productName = productName.split(char)[0]
        productName = productName.strip().strip("-").strip()
        productName = productName.replace("\r\n", " ")
        productName = productName.replace("  ", " ")
        productName = productName.replace(u"  –", "")
        return productName
    except:
        return -1

def ProcessProductsOnPage(products, prices):
    for i in range(0, len(products)):
        product = products[i]
        productUrl = (baseUrl + product.attrib['href'])
##        if productUrl != "https://www.magnad.ru/fantom-12-new-k125p1.html":
##            continue
        print productUrl
        time.sleep(1)
        resp = requests.get(productUrl, verify=False)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl, prices[i])

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = commonLibrary.GetIntPriceFromText(productPageContent.xpath("//div[@class = 'cena_cur_0']/span[@class = 'cena_price']")[0].text)
    name = GetProductName(productPageContent)
    if name == -1:
        raise Exception('invalid name')
    productParams["name"] = name
    productParams["price"] = productPrice

##    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["images"] = []
    #main image
##    productParams["images"].append(baseUrl + productPageContent.xpath("//div[@id = 'pp_product_image']//a")[0].attrib['href'])

    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = u""
    productPageContentTmp = copy.copy(productPageContent)

    lists = productPageContentTmp.xpath("//div[@class='main_area_text']//ul")
    if len(lists) > 0:
        productDescription += u"<h3>Спецификация электрошокера</h3>"
        productDescription += commonLibrary.UnescapeText(etree.tostring(lists[0]))
        productDescription += u"<br/>"

    tables = productPageContentTmp.xpath("//table[@class='desc_table']")
    if len(tables) > 0:
        productDescription += u"<h3>Технические характеристики электрошокера</h3>"
        productDescription += commonLibrary.UnescapeText(etree.tostring(tables[0]))

    div = productPageContentTmp.xpath("//div[@class='main_area_text']")[0]

    noindex = div.xpath(".//noindex")[0]
    nextElem = noindex.getnext()
    while nextElem != None:
        tmpnextElem = nextElem
        nextElem = nextElem.getnext()
        tmpnextElem.getparent().remove(tmpnextElem)

    texts = [u"На настоящий день", u"Подарок"]
    for text in texts:
        gift = div.xpath(u".//*[contains(.,'{0}')]".format(text))
        if len(gift) > 0:
            gitf = gift[-1]
            nextElem = gitf.getnext()
            while nextElem != None:
                tmpnextElem = nextElem
                nextElem = nextElem.getnext()
                tmpnextElem.getparent().remove(tmpnextElem)
            gitf.getparent().remove(gitf)

    tags = ["ul", "table", "hr", "h2" , "h1", "noindex"]
    for tag in tags:
        elems = div.xpath(".//{0}".format(tag))
        for elem in elems:
            elem.getparent().remove(elem)

    elems = div.xpath(".//div[contains(@id, 'item_fly')]")
    for elem in elems:
        elem.getparent().remove(elem)

    elems = div.xpath(".//div[.//h3]")
    if len(elems) != 0:
        elem = elems[-1]
        elem.getparent().remove(elem)

    elems = div.xpath(u".//span[contains(., '{0}')]".format(u"класс"))
    for elem in elems:
        elem.getparent().remove(elem)

    imgs = ['40e-gb', '119e-shok', "87e-kevlar", "105e-brasletv2x.jpg"]
    for img in imgs:
        elems = div.xpath(u".//img[contains(@src, '{0}')]".format(img))
        for elem in elems:
            elem.getparent().remove(elem)

    elems = div.xpath(u".//img")
    for elem in elems:
        src = elem.attrib["src"]
        if src.find("magnad") == -1:
            elem.attrib["src"] = "http://www.magnad.ru/" + src

    productDescription = commonLibrary.UnescapeText(etree.tostring(div)) + productDescription
##    print productDescription
    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl, wholesalePrice):
    try:
        productParams = GetProductParams(productPageContent)
    except Exception as exception:
        if exception.message == "invalid name":
            logging.LogErrorMessage("Product '" + productUrl + "' has incorrect name")
        else:
            print exception.message
            logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        return

##    logging.Log(productParams["name"])

    category = u"электрошокеры"

    categoryId = prestashop_api.GetCategoryID(category)
    if categoryId == -1:
        result = prestashop_api.AddCategoryDefault(category, "Magnad", 0, shopName)
        if result == False:
            logging.LogErrorMessage(u"Не удалось добавить категорию: " + category)
            return
        else:
            logging.LogInfoMessage(u"Добавлена категория: " + category)

    productParams["active"] = "1"
    productParams["category"] = category
    productParams["meta_title"] = productParams["name"]
    productParams["wholesale_price"] = wholesalePrice

    productParams["article"] = "md"

    prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], "Magnad", shopName, productUrl, False)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\magnad\\log", "magnad")
logging = log.Log(logFilePath)
h = HTMLParser.HTMLParser()
machineName = commonLibrary.GetMachineName()
shopName = ""

for site in commonLibrary.sitesParams:
    if machineName == "hp" or machineName.find("pitekantrop") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    prestashop_api.InitAllProducts()

    baseUrl = "https://www.magnad.ru"
    resp = requests.get(baseUrl + "/shokeri-optom-k49p1.html", verify=False)
    tree = etree.HTML(resp._content)

    products = tree.xpath("//a[@class = 'list_item_n_header_link']")
    prices = [commonLibrary.GetIntPriceFromText(x.text) for x in tree.xpath("//span[@class = 'cena_price']")]

    ProcessProductsOnPage(products, prices)

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("md")
    for id in ids:
        date = prestashop_api.GetProductUpdateDate(id)
        if date.date() < datetime.date.today() - datetime.timedelta(days = 1):
##            prestashop_api.MakeProductUnavailableForAllShops(id)
            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    if machineName == "hp" or machineName.find("pitekantrop") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Кварц", "", [logFilePath])