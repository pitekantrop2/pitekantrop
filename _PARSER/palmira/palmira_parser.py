﻿import requests
from lxml import etree
import urllib
import PrestashopAPI
import log
import datetime
import HTMLParser
import subprocess
import commonLibrary
import myLibrary
import xlrd


def ProcessProductsOnPage(products):
    for product in products:
        productUrl = baseUrl + product.attrib['href']
        print productUrl
##        if productUrl != "http://31vek.com/katalog/internet-magazin/podarki-na-23-fevralja/budilnik-s-mishenju-bf1821-908":
##            continue

        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl)


def GetProductParams(productPageContent):
    productParams = {}
    productParams["name"] = productPageContent.xpath("//h1")[0].text.strip().replace("\"","").replace(u"»","").replace(u"«","")
    productParams["price"] = int(productPageContent.xpath("//div[@class = 'title']/span")[0].text.strip().replace(u" руб.", ""))
    productParams["categories"] = [ x.text.strip() for x in productPageContent.xpath("//div[@class = 'breadcrumbs']//a")]
    productParams["categories"].pop(0)
    productParams["categories"].pop(0)

    productParams["description"] = MakeProductDescription(productPageContent, productParams["name"])
    productParams["images"] = []
    productParams["images"].append( baseUrl + productPageContent.xpath("//img[@class = 'thumble']")[0].attrib['src'])

    return productParams


def MakeProductDescription(productPageContent, productName):
    productDescription = ""

    descriptionElem = productPageContent.xpath("//div[contains(@class, 'tbox clearfix')]")[0]
    imgs = descriptionElem.xpath(".//img")
    for img in imgs:
        img.attrib["src"] = baseUrl + img.attrib["src"]

    iframes = descriptionElem.xpath(".//iframe")
    for iframe in iframes:
        if "style" in iframe.attrib.keys():
            del iframe.attrib["style"]

    forms = descriptionElem.xpath(".//form")
    for form in forms:
        form.getparent().remove(form)

    socials = descriptionElem.xpath(".//div[@class = 'social']")
    for social in socials:
        social.getparent().remove(social)

    productDescription += etree.tostring(descriptionElem)
    properties = productPageContent.xpath("//table[@class='harakteristika']")
    if len(properties) != 0:
        productDescription += etree.tostring(properties[0])
        productDescription += "<p></br/><br/></p>"
    try:
        video = productPageContent.xpath("//div[contains(@class, 'tbox clearfix')]")[4]
        productDescription += etree.tostring(video)
    except:
        pass

    productDescription = commonLibrary.UnescapeText(productDescription)

    productDescription = productDescription.replace("'","")
    productDescription = productDescription.replace(u"\xa0","")
    productDescription = productDescription.replace("allowfullscreen=\"\"/>","allowfullscreen=\"\"></iframe>")

    return productDescription

def ProcessProductPrestashop(productPageContent, productUrl):
    try:
        productParams = GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product {0}".format(productUrl))
        return

    categoriesNames = []

    if len(productParams["categories"]) > 1:
        for i in range(0, len(productParams["categories"])):
            if i == len(productParams["categories"]) - 1:
                break

            if i == 0:
                parentCategoryName = "palmira"
            else:
                parentCategoryName = productParams["categories"][i - 1] + "_palmira"

            categoryName = productParams["categories"][i] + "_" + parentCategoryName.lower()
            categoriesNames.append(categoryName)

            categoryId = prestashop_api.GetCategoryID(categoryName)
            if categoryId == -1:
                result = prestashop_api.AddCategoryDefault(categoryName, parentCategoryName, 0, "")
                if result == False:
                    logging.LogErrorMessage(u"Не удалось добавить категорию: " + categoryName)
                    return
                else:
                    logging.LogInfoMessage(u"Добавлена категория: " + categoryName)

        categoryName = productParams["categories"][-1]
        parentCategoryName = categoriesNames[-1]
    else:
        categoryName = productParams["categories"][0]
        parentCategoryName = "palmira"

    productParams["active"] = "1"
    if site["url"] == "http://saltlamp.su":
        productParams["wholesale_price"] = int(10 * round(float(float(productParams["price"]) * 0.65)/10))
    if site["url"] == "http://sushilki.ru.com":
        productParams["wholesale_price"] = int(10 * round(float(float(productParams["price"]) * 0.75)/10))
##        productParams["price"] = int(10 * round(float(float(productParams["price"]) * 1.05)/10))
    productParams["meta_title"] = productParams["name"]
    productParams["article"] = "pm"

    prestashop_api.Parser_ProcessProduct(productParams, categoryName, parentCategoryName, "", productUrl, False)


#############################################################################################################
logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\palmira\\log","palmira")
logging = log.Log(logFilePath)
machineName = commonLibrary.GetMachineName()

prestashop_api = PrestashopAPI.PrestashopAPI()

for site in commonLibrary.sitesParams:

    if site["url"] not in ["http://saltlamp.su", "http://sushilki.ru.com"]:
        continue

    if machineName == "hp":
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        try:
            ssh.kill()
        except:
            pass
        continue

    prestashop_api.InitAllProducts()
    baseUrl = "http://tornado-spb.ru"
    resp = requests.get(baseUrl)
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//div[@class = 'lmenu']//li//a")

    for category in categories:
        categoryName = category.text.strip()

        if categoryName not in [u"СОЛЯНЫЕ ЛАМПЫ"] and site["url"] == "http://saltlamp.su":
            continue

        if categoryName not in [u"ТОВАРЫ ДЛЯ ДОМА"] and site["url"] == "http://sushilki.ru.com":
            continue

        categoryUrl = category.attrib['href']

        url = (baseUrl + categoryUrl).strip()
        resp = requests.get(url)
        categoryContent = etree.HTML(resp._content)

        subCategories = categoryContent.xpath("//div[contains( @class, 'catalog-list2')]//a[@style = 'color:green;']")

        for subCategory in subCategories:
            subCategoryName = subCategory.text.strip()
            if subCategoryName not in [u"Товары для кухни"] and site["url"] == "http://sushilki.ru.com":
                continue

            subCategoryUrl = subCategory.attrib['href']

            url = (baseUrl + subCategoryUrl).strip()
            resp = requests.get(url)
            subCategoryContent = etree.HTML(resp._content)

            subsubCategories = subCategoryContent.xpath("//ul[@class = 'folder clearfix']//a")
            if len(subsubCategories) != 0:
                for subsubCategory in subsubCategories:
                    subsubCategoryName = subsubCategory.text.strip()
                    if subsubCategoryName not in [u"ЭЛЕКТРОСУШИЛКИ ДЛЯ ГРИБОВ И ЯГОД"] and site["url"] == "http://sushilki.ru.com":
                        continue
                    subsubCategoryUrl = subsubCategory.attrib['href']
                    url = (baseUrl + subsubCategoryUrl).strip()
                    resp = requests.get(url)
                    subsubCategoryContent = etree.HTML(resp._content)
                    products = subsubCategoryContent.xpath("//div[@class = 'good-list clearfix']/ul//a[@style = 'color:green;']")
                    ProcessProductsOnPage(products)
            else:
                products = subCategoryContent.xpath("//div[@class = 'good-list clearfix']/ul//a[@style = 'color:green;']")
                ProcessProductsOnPage(products)

##    #деактивируем устаревшие товары
##    ids = prestashop_api.GetContractorProductsIds("pm")
##    for id in ids:
##        date = prestashop_api.GetProductUpdateDate(id)
##        if date.date() < datetime.date.today() - datetime.timedelta(days = 1):
##            prestashop_api.MakeProductUnavailableForAllShops(id)
####            prestashop_api.SetProductActive(id, 0)
##            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    if machineName == "hp":
        ssh.kill()