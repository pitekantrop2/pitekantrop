﻿import requests
from lxml import etree
import urllib
import log
import datetime
import xlrd
import PrestashopAPI
import spectr_parser
import os
import commonLibrary
import sys
import SendMails
import subprocess

def ProcessProductsOnPage(products):

    for product in products:

        productUrl = product.attrib['href']
##        print productUrl

        productUrl = baseUrl + productUrl

        ProcessProductPrestashop(productUrl)


def ProcessProductPrestashop(productUrl):

    print productUrl

    try:
        productParams = spectr_parser.GetProductParams(productUrl)
    except:
        logging.LogErrorMessage("Unable to parse product '" + productUrl + "'")
        return

    if prestashop_api.GetCategoryID(productParams["categories"][-1] + "_spectr") == -1:

        #добавляем категории, если необходимо
        for i in range(0, len(productParams["categories"])):

            category = productParams["categories"][i] + "_spectr"
            categoryId = prestashop_api.GetCategoryID(category)
            if categoryId == -1:
                if i == 0:
                    result = prestashop_api.AddCategoryDefault(category, "Spectr", 0, shopName)
                else:
                    result = prestashop_api.AddCategoryDefault(category, productParams["categories"][i - 1] + "_spectr", 0, shopName)
                if result == False:
                    logging.LogErrorMessage(u"Не удалось добавить категорию: " + category)
                    return
                else:
                    logging.LogInfoMessage(u"Добавлена категория: " + category)


    categoryName = productParams["categories"][-1]
    parentCategoryName = "Spectr"

    productParams["category"] = categoryName
    productParams["meta_title"] = shopName + " - " + productParams["name"]

    productParams["article"] = "sks"

    prestashop_api.Parser_ProcessProduct(productParams, categoryName, parentCategoryName, shopName, productUrl, False)


#############################################################################################################
commonLibrary.KillPutty()

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\spectr\\log","spectr")
logging = log.Log(logFilePath)
mails = SendMails.SendMails()

sites = [
##{"url": "http://safetus.ru", "ip" : "185.5.249.9", "prestashopKey" : "BBJJE12T4BBU5HNB1ZTPBJEB6HGREGDU", "dbName" : "safetus", "dbUser" : "safetus_user_ex", "dbPassword": "1qaz@WSX"},
{"url": "http://omsk.knowall.ru.com", "ip" : "185.58.207.82", "prestashopKey" : "KAR7M17CPPALJY38NKI521NNP96IH3Z7", "dbName" : "knowall", "dbUser" : "knowall_user_ex", "dbPassword": "1qaz@WSX"},
]

for site in sites:

    ssh = subprocess.Popen([commonLibrary.puttyPath, '-v', '-ssh', '-2', '-P', '22', '-C', '-l', 'root', '-pw', 'IiRMBuAiJTLv', '-L', '5555:127.0.0.1:3306', site["ip"]])

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    shopName = ""
    if site["url"] == "http://omsk.knowall.ru.com":
        shopName = u"Новалл"
    else:
        shopName = u"Safetus"

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    prestashop_api.InitAllProducts()

    baseUrl = "http://www.spectr-sks.ru/"
    resp = requests.get(baseUrl + "secure")
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//div[@class = 'catalogue-sections']//a")

    for category in categories:

        categoryUrl = category.attrib['href']

        if categoryUrl in ["sustoms-devices",
                        "audio-record-systems",
                        "attestation-devices",
                        "metal-detectors"
                        ]:
            continue

        url = baseUrl + categoryUrl
        resp = requests.get(url)
        categoryContent = etree.HTML(resp._content)

        categories = categoryContent.xpath("//div[@class = 'catalogue-sections']//a")

        for category in categories:

            categoryUrl = category.attrib['href']

            if categoryUrl in ["nonlinear-locators",
                            "telephone-line-and-wire-communic",
                            "radio-monitoring-complex",
                            "low_electricity_protection",
                            "nsd-protection",
                            "noise-generators",
                            "cryptography-protection",
                            "information-destroyers",
                            "devices-electric-220V-protection"
                            ]:
                continue

            subCategoryContent = etree.HTML(requests.get(baseUrl + categoryUrl)._content)

            products = subCategoryContent.xpath("//td[@class = 'name']//a")

            if len(products) > 0: #категория без подкатегорий

                ProcessProductsOnPage(products)

            else: #категория с подкатегориями

                subCategories = subCategoryContent.xpath("//div[@class = 'catalogue-sections']//a")

                for subCategory in subCategories:

                    subCategoryUrl = subCategory.attrib['href']

                    if subCategoryUrl in ["vibro-noise-systems",
                            ]:
                        continue

                    subCategoryContent = etree.HTML(requests.get(baseUrl + subCategoryUrl)._content)
                    products = subCategoryContent.xpath("//td[@class = 'name']//a")

                    ProcessProductsOnPage(products)

##    #деактивируем устаревшие товары
##    ids = prestashop_api.GetContractorProductsIds("sks")
##    for id in ids:
##        date = prestashop_api.GetProductUpdateDate(id)
##        if date.date() < datetime.date.today() - datetime.timedelta(days = 1):
##            prestashop_api.MakeProductUnavailableForAllShops(id)
##            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    ssh.kill()

result = mails.SendInfoMail("Обновление товаров: Spectr", "", [logFilePath])