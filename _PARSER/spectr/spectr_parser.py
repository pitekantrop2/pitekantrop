﻿import requests
from lxml import etree
import urllib
import dbForParser
import InsalesForParser
import log
import datetime
import HTMLParser
import re


def GetProductParams(productLink):

    resp = requests.get(productLink)
    productPageContent = etree.HTML(resp._content)

    productName = productPageContent.xpath("//div[@class = 'h']//h1")[0].text.split(",")[0].strip()

    productParams = {}

    #категории
    productCategories = productPageContent.xpath("//div[@class = 'navigation-path']//a")
    productParams["categories"] = [ x.text.strip() for x in productCategories]
    productParams["categories"].pop(0)

    #название
    productParams["name"] = productParams["categories"][-1].capitalize() + " " + productName
    print productParams["name"]

    #цена
    productPrice = productPageContent.xpath("//td[@class = 'price-value']")[0].text.strip().replace(u"\xa0", "")
    price = int(10 * round(float(float(productPrice) * 1.2)/10))
    if price % 1000 == 0:
        price -= 10
    productParams["price"] = price
    productParams["wholesale_price"] = int(float(productPrice) * 0.9)

    #описание
    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["images"] = []
    productParams["images"].append( productPageContent.xpath("//a[@class = 'gallery']/img")[0].attrib['src'])

    return productParams


def MakeProductDescription(productPageContent):

    description = ""
    try:
        description += etree.tostring(productPageContent.xpath("//div[@id = 'content']")[0])
    except:
        pass

    description = description.replace("""justify""", """left""")

    h = HTMLParser.HTMLParser()
    regexp = "&.+?;"
    list_of_html = re.findall(regexp, description) #finds all html entites in page
    for e in list_of_html:
        unescaped = h.unescape(e) #finds the unescaped value of the html entity
        description = description.replace(e, unescaped)

    return description

