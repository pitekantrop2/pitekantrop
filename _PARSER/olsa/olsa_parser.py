﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys

def ProcessProductsOnPage(products, categoryName):
    for product in products:
        productUrl = product.attrib['href']

        print productUrl

        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl, categoryName)

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = "1000"
    productParams["name"] = productPageContent.xpath("//div[@class = 'card__title']")[0].text.strip() + " " + productPageContent.xpath("//div[@class = 'card__head']/p")[0].text.strip()
    productParams["name"] = productParams["name"].replace(u"Инкубатор", u"Инкубатор Золушка").replace(u".", u"")
##    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)
    productParams["images"] = [ x.attrib['src'] for x in productPageContent.xpath("//div[@class = 'slider__pager']/a/img")]

    productParams["price"] = productPrice
    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productDescription = ""

    descriptionElem = productPageContent.xpath("//div[contains(@class, 'card__info')]/p")[0]
    productDescription += etree.tostring(descriptionElem)
    descriptionElem = productPageContent.xpath("//div[contains(@class, 'card__description')]/p")[0]
    productDescription += etree.tostring(descriptionElem)

    propertiesTable = productPageContent.xpath("//div[@class = 'card__chars']//table")[0]
    productDescription += etree.tostring(propertiesTable)

    productDescription = commonLibrary.GetStringDescription(productDescription)

    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl, categoryName):
    try:
        productParams = GetProductParams(productPageContent)
    except:
        logging.LogErrorMessage("Unable to parse product")
        return

##    logging.Log(productParams["name"])
##    if productUrl.find("kamera_videonablyudeniya_ahd_kurato_vr_815_ahd_720p_0141_white/") == -1:
##        return
    productParams["wholesale_price"] = int(float(productParams["price"]) * 0.85)
    price = int(10 * round(float(float(productParams["price"]) * 1.214)/10))


    productParams["active"] = "1"
    productParams["category"] = categoryName

    productParams["meta_title"] = productParams["name"]

    if price % 1000 == 0:
        price -= 10

    productParams["price"] = price
    productParams["article"] = "os"

    productId = prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], "olsa", "", productUrl, False)
    if productId != False:
        updatedProducts.append(productId)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\olsa\\log")
logging = log.Log(logFilePath)
h = HTMLParser.HTMLParser()
machineName = commonLibrary.GetMachineName()

for site in commonLibrary.sitesParams:

    if site["url"] != "http://incubators.shop":
        continue

    if machineName.find("hp") != -1:
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        mails.SendInfoMail("Ошибка соединения", "Не удалось соединиться с " + site["url"])
        ssh.kill()
        continue

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    updatedProducts = []

    prestashop_api.InitAllProducts()

    baseUrl = "http://www.olsa-s.ru"
    resp = requests.get(baseUrl + "/nasha-produktsiya")
    tree = etree.HTML(resp._content)

    products = tree.xpath("//a[@class = 'entry__btn']")

    ProcessProductsOnPage(products, u"инкубаторы")

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("ww")
    for id in ids:
        if id not in updatedProducts:
##            prestashop_api.SetProductActive(id, 0)
            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    if machineName.find("hp") != -1:
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Beevid", "", [logFilePath])