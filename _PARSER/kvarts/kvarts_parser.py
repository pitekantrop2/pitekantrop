﻿import requests
import lxml
from lxml import etree
import urllib
import log
import datetime
import random
import PrestashopAPI
import HTMLParser
import re
import SendMails
import subprocess
import time
import commonLibrary
import sys
import copy

def ProcessProductsOnPage(products):
    for product in products:
        productUrl = (baseUrl + product.attrib['href'])
##        if productUrl != "http://konnektim.ru/equipment/internet-signal-amplification/3g-amplifiers/43":
##            continue
        print productUrl
        time.sleep(5)
        resp = requests.get(productUrl)
        productPageContent = etree.HTML(resp._content)

        ProcessProductPrestashop(productPageContent, productUrl)

def GetProductParams(productPageContent):
    productParams = {}
    productPrice = productPageContent.xpath("//div[@id = 'pp_productprice']")[0].text.strip().replace(u" руб.", "")
    if productPrice == "":
        productPrice = productPageContent.xpath("//span[@class = 'price_action']")[0].text.strip()
    productParams["name"] = productPageContent.xpath("//div[@id = 'mc_descriprion']/div")[0].text.strip()

##    print productParams["name"]

    productParams["description"] = MakeProductDescription(productPageContent)

    productParams["images"] = []
    #main image
    productParams["images"].append(baseUrl + productPageContent.xpath("//div[@id = 'pp_product_image']//a")[0].attrib['href'])

    price = int(10 * round(float(float(productPrice) * 1.05)/10))
    if price % 1000 == 0:
        price -= 10
    productParams["price"] = price

    productParams["wholesale_price"] = int(float(productPrice) * 0.75)

    return productParams


def MakeProductDescription(productPageContent):
    productPageContent = commonLibrary.CorrectHtml(productPageContent)
    productPageContentTmp = copy.copy(productPageContent)
    productPageContentTmp = productPageContentTmp.xpath("//div[@id = 'pp_chartable']")[0]

    divs = productPageContentTmp.xpath(u"//div[contains(.,'{0}') or contains(.,'{1}') or @style = 'line-height: 14px !important;']".format(u"ЭКОНОМИЯ при", u"покупке компонентов из комплекта отдельно"))
    for div in divs:
        div.getparent().remove(div)

    scripts = productPageContentTmp.xpath(u"//script")
    for script in scripts:
        script.getparent().remove(script)

    aa = productPageContentTmp.xpath(u".//table//a")
    replaces = []
    for a in aa:
        if a.text not in [None, ""]:
            replaces.append([a.text, commonLibrary.UnescapeText(etree.tostring(a))])

    aa = productPageContentTmp.xpath(u".//a[contains(.,'{0}')]".format(u"Комплект"))
    for a in aa:
        a.getparent().remove(a)

    aa = productPageContentTmp.xpath("//img[not(contains(@src,'pdf.jpg'))]")
    for a in aa:
        if "alt" in a.attrib.keys():
            if a.attrib["alt"].find(u"Комплект") != -1:
                a.getparent().remove(a)

    pp = productPageContentTmp.xpath("//p[not(.//img) and not(.//strong) and not(.//br)]")
    for p in pp:
        try:
            if p.text.strip() == "":
                p.getparent().remove(p)
        except:
            pass

    divs = productPageContentTmp.xpath(u"//div")
    for div in divs:
        try:
            if div.attrib["style"] == "text-align: center;":
                div.getparent().remove(div)
        except:
            pass

    productDescription = commonLibrary.UnescapeText(etree.tostring(productPageContentTmp))
    for replace in replaces:
        productDescription = productDescription.replace(replace[1], replace[0])

    productDescription = productDescription.replace("src=\"/","src=\"http://konnektim.ru/")
    productDescription = productDescription.replace("h1","h3").replace("h2","h3")
##    print productDescription
    return productDescription


def ProcessProductPrestashop(productPageContent, productUrl):
##    try:
    productParams = GetProductParams(productPageContent)
##    except:
##        logging.LogErrorMessage("Unable to parse product " + productUrl)
##        return

##    logging.Log(productParams["name"])

    parentCategoryName = productPageContent.xpath("//div[@id = 'desc_lead']")[0].text.replace(">", "").strip()
    categoryName = productPageContent.xpath("//div[@id = 'desc_lead']/span/a")[0].text.strip()

    category = parentCategoryName + "_kvarts"

    categoryId = prestashop_api.GetCategoryID(category)
    if categoryId == -1:
        result = prestashop_api.AddCategoryDefault(category, "Kvarts", 0, shopName)
        if result == False:
            logging.LogErrorMessage(u"Не удалось добавить категорию: " + category)
            return
        else:
            logging.LogInfoMessage(u"Добавлена категория: " + category)

    productParams["active"] = "1"
    productParams["category"] = categoryName
    productParams["meta_title"] = productParams["name"]

    productParams["article"] = "kv"

    prestashop_api.Parser_ProcessProduct(productParams, productParams["category"], category, shopName, productUrl, False)

#===============================================================================

logFilePath = log.GetLogFileName("c:\\programming\\_PARSER\\kvarts\\log", "kvarts")
logging = log.Log(logFilePath)
h = HTMLParser.HTMLParser()
machineName = commonLibrary.GetMachineName()
shopName = ""

for site in commonLibrary.sitesParams:
    if machineName == "hp":
        ssh = subprocess.Popen(commonLibrary.GetPuttyParams(site))

    prestashop_api = commonLibrary.GetPrestashopInstance(site, logFilePath)
    if prestashop_api == False:
        ssh.kill()
        continue

    logging.LogInfoMessage("================================" + prestashop_api.siteUrl + "================================")

    prestashop_api.InitAllProducts()

    baseUrl = "http://konnektim.ru"
    resp = requests.get(baseUrl)
    tree = etree.HTML(resp._content)

    categories = tree.xpath("//a[@class = 'mc_six_record']")

    for category in categories:
        categoryUrl = category.attrib['href']

        url = (baseUrl + categoryUrl).strip()
        resp = requests.get(url)
        categoryContent = etree.HTML(resp._content)

        products = categoryContent.xpath("//div[@class = 'cl_productname']/a")

        ProcessProductsOnPage(products)

    #деактивируем устаревшие товары
    ids = prestashop_api.GetContractorProductsIds("kv")
    for id in ids:
        date = prestashop_api.GetProductUpdateDate(id)
        if date.date() < datetime.date.today() - datetime.timedelta(days = 1):
##            prestashop_api.MakeProductUnavailableForAllShops(id)
            logging.LogInfoMessage("Product " + str(id) + " has been deactivated")

    if machineName == "hp":
        ssh.kill()


mails = SendMails.SendMails()
result = mails.SendInfoMail("Обновление товаров: Кварц", "", [logFilePath])