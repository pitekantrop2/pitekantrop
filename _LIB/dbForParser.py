﻿import MySQLdb
import ntplib
import datetime
import log

class DB:

    dbConnector = None
    logging = None
    tableName = None

    def __init__(self,tableName, logFilePath):
        self.dbConnector = MySQLdb.connect(host='127.0.0.1', user='root', passwd='123123', db='Parser', charset='utf8')
        self.logging = log.Log(logFilePath)
        self.tableName = tableName

    def GetProductId(self, productName):
        datetimeNow = datetime.datetime.now()

        cursor = self.dbConnector.cursor()

        sql = "SELECT * FROM %(tableName)s WHERE ProductName = '%(productName)s'"%{"tableName":self.tableName,"productName":productName}
        cursor.execute(sql)

        data =  cursor.fetchall()
        if len(data) == 0:
            return -1
        else:
            vendor, productName, productId, updated, deleted = data[0]

            sql = """UPDATE %(tableName)s
                SET Updated = '%(updated)s'
                WHERE ProductName = '%(productName)s'"""%{"tableName":self.tableName,"updated":datetimeNow,"productName":productName }
            cursor.execute(sql)
            self.dbConnector.commit()

            if deleted == 1:
                return "deleted"

            return productId

    def AddProduct(self, productName, productId, vendor):
        datetimeNow = datetime.datetime.now()
        sql = """INSERT INTO %(tableName)s
            VALUES ('%(Vendor)s', '%(ProductName)s', '%(ProductId)s', '%(Updated)s', 0)
            """%{"tableName":self.tableName,
                "Vendor":vendor,
                "ProductName":productName,
                 "ProductId":productId,
                 "Updated":datetimeNow}
        cursor = self.dbConnector.cursor()
        cursor.execute(sql)
        self.dbConnector.commit()

    def GetAllProductsIds(self):
        sql = "SELECT ProductId FROM %(tableName)s"%{"tableName":self.tableName}
        cursor = self.dbConnector.cursor()
        cursor.execute(sql)

        data =  cursor.fetchall()
        arr = []
        for row in data:
            arr.append(int(list(row)[0]))

        return arr

    def GetActiveProductsIds(self):
        sql = """SELECT ProductId FROM %(tableName)s
                 WHERE Deleted = 0"""%{"tableName":self.tableName}
        cursor = self.dbConnector.cursor()
        cursor.execute(sql)

        data =  cursor.fetchall()
        arr = []
        for row in data:
            arr.append(int(list(row)[0]))

        return arr

    def GetProductsIdsByDate(self, updateDate):
        sql = """SELECT ProductId FROM %(tableName)s
                 WHERE Updated > '%(Updated)s'"""%{"tableName":self.tableName,"Updated":updateDate }
        cursor = self.dbConnector.cursor()
        cursor.execute(sql)

        data =  cursor.fetchall()
        arr = []
        for row in data:
            arr.append(int(list(row)[0]))

        return arr

    def GetProductsIdsByVendor(self, vendorName, isActive = False):
        sql = """SELECT ProductId FROM %(tableName)s
                 WHERE Vendor = '%(Vendor)s'"""%{"tableName":self.tableName,"Vendor":vendorName }

        if isActive == True:
            sql += " and Deleted = 0"
        cursor = self.dbConnector.cursor()
        cursor.execute(sql)

        data =  cursor.fetchall()
        arr = []
        for row in data:
            arr.append(int(list(row)[0]))

        return arr

    def UpdateProductDate(self, productId):
        datetimeNow = datetime.datetime.now()

        cursor = self.dbConnector.cursor()

        sql = """UPDATE %(tableName)s
            SET Updated = '%(updated)s'
            WHERE ProductId = '%(productId)s'"""%{"tableName":self.tableName,"updated":datetimeNow,"productId":productId }
        cursor.execute(sql)
        self.dbConnector.commit()

    def MarkProductAsDeleted(self, productId):

        cursor = self.dbConnector.cursor()

        sql = """UPDATE %(tableName)s
            SET Deleted = 1
            WHERE ProductId = %(productId)s"""%{"tableName":self.tableName,"productId":productId }
        cursor.execute(sql)
        self.dbConnector.commit()
