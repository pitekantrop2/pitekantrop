﻿import insales
import log
import time
import datetime

def GetLogFileName(basePath):
    Now = datetime.datetime.now()
    sToday = "%(d)s_%(m)s_%(h)s_%(min)s"%{"m":Now.month ,"d":Now.day, "h":Now.hour, "min":Now.minute }
    logFilePath = basePath + "\\" + sToday + ".log"
    return logFilePath

class InsalesForParser:

    insales_api = None
    parentCategoryId = -1
    logging = None

    def __init__(self, accountName, login, password ,logFilePath):
        self.insales_api = insales.InSalesApi
        self.insales_api = self.insales_api.from_credentials(accountName, login, password)
        self.parentCategoryId = self.GetCategoryId(u"Склад")
        self.logging = log.Log(logFilePath)

    # === Получить категории на сайте ====
    def GetCollections(self):
        #insales_api = insales.InSalesApi
        insales_api = self.insales_api
        try:
            return insales_api.get_collections()

        except Exception, e:
            print e
            self.logging.Log(u"Не удалось получить список категорий по причине")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetCollections()
            raise Exception("Failure")

    # === Получить id категории ====
    def GetCategoryId(self, categoryName):

        insales_api = self.insales_api
        try:
            categories =  insales_api.get_categories()
            for category in categories:
                if category["title"] == categoryName.strip():
                    return category["id"]
        except Exception, e:
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetCategoryId(categoryName)
            raise Exception("Failure")

        return -1

    # === Добавить категорию ====
    def AddCategory(self, categoryName, parentCategoryId = -1):

        insales_api = self.insales_api
        if parentCategoryId == -1: parentCatId = self.parentCategoryId
        else: parentCatId = parentCategoryId
        item =  {
           'parent-id': parentCatId,
           'title' : categoryName
             }
        try:
            catId = insales_api.add_category(item)["id"]
            self.logging.Log(u"Добавлена категория '" + categoryName + "'")
            return catId
        except Exception, e:
            print e
            self.logging.Log(u"Не удалось добавить категорию '" + categoryName + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.AddCategory(categoryName, parentCategoryId )
            raise Exception("Failure")

    # === Добавить товар ====
    def AddProduct(self, item, productImages = []):
        insales_api = self.insales_api
        try:
            addedItem = insales_api.add_product(item)
            for productImage in productImages:
                imageData = {'src': productImage, 'filename' : addedItem["title"], 'title' : addedItem["title"],}
                insales_api.add_product_image(addedItem["id"],imageData)

            self.logging.Log(u"Добавлен товар '" + item["title"] + "'")
            return addedItem["id"]

        except Exception, e:
            print e
            self.logging.Log(u"Не удалось добавить товар '" + item["title"] + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.AddProduct(item, productImages)
            return None

    # === Обновить товар ====
    def UpdateProductVariant(self, variantData):
        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            insales_api.update_product_variant(variantData["product-id"],variantData["id"], variantData)
            self.logging.Log(u"Обновлен товар '" + str(variantData["product-id"]) + "'")
        except Exception, e:
            self.logging.Log(u"Не удалось обновить товар '" + str(variantData["product-id"]) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.UpdateProductVariant(variantData)

    # === Получить информацию о товаре ====
    def GetProduct(self, productId):

        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            item =  insales_api.get_product(productId)
            return item
        except Exception, e:
            self.logging.Log(u"Не удалось получить информацию по товару '" + str(productId) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetProduct(productId)
            return None

    # === Получить информацию о варианте товара ====
    def GetProductVariant(self, productId, variantId):

        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            item =  insales_api.get_product_variant(productId, variantId)
            return item
        except Exception, e:
            self.logging.Log(u"Не удалось получить информацию по варианту товара '" + str(productId) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetProductVariant(productId, variantId)
            return None

    # === Добавить аналогичный товар ====
    def AddSimilarProduct(self, productId, similarProductId):
        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        similar_data = {'similar_ids' : similarProductId}

        try:
            insales_api.add_similar_product(productId, similar_data)
            self.logging.Log(u"Добавлен аналогичный товар '" + str(similarProductId) + u"' для '" + str(productId) + "'")
        except Exception, e:
            self.logging.Log(u"Не удалось добавить аналогичный товар для '" + str(productId) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.AddSimilarProduct(productId, similarProductId)

    # === Получить список аналогичных товаров ====
    def GetSimilarProducts(self, productId):
        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            similar_products = insales_api.get_similar_products(productId)
            return similar_products
        except Exception, e:
            self.logging.Log(u"Не удалось получить список аналогичных товаров для '" + str(productId) + u"'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetSimilarProducts(productId)
            return None

    # === Получить список товаров категории ====
    def GetCollectionProducts(self, collectionId, active = False):
        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            collectionsProducts = insales_api.get_collects(collection_id = collectionId)
            return collectionsProducts
        except Exception, e:
            self.logging.Log(u"Не удалось получить список товаров для категории " + str(collectionId) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetCollectionProducts(collectionId)
            return None

    # === Получить список товаров категории ====
    def GetProducts(self, categoryId):
        #insales_api = self.insales_api
        insales_api = insales.InSalesApi

        try:
            collectionsProducts = insales_api.get_products( )
            return collectionsProducts
        except Exception, e:
            self.logging.Log(u"Не удалось получить список товаров для категории'" + str(collectionId))
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetCollectionProducts(collectionId)
            raise Exception("Failure")

    # === Получить информацию о параметре ====
    def GetParameterInfo(self, parameterId):
        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            parameterInfo = insales_api.get_option_name()
            return parameterInfo
        except Exception, e:
            self.logging.Log(u"Не удалось получить список товаров для категории'" + str(collectionId) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetCollectionProducts(collectionId)
            raise Exception("Failure")

    # === Получить список товаров категории ====
    def AddParameterToProduct(self, productId, productData):
        insales_api = self.insales_api
        #insales_api = insales.InSalesApi

        try:
            insales_api.update_product(productId, productData)
        except Exception, e:
            self.logging.Log(u"Не удалось получить список товаров для категории'" + str(collectionId) + u"' по причине '" + str(e) + "'")
            if e.code == 503:
                self.WaitForApiEnable()
                return self.GetCollectionProducts(collectionId)
            return None


    def WaitForApiEnable(self):
        insales_api = self.insales_api

        while True:
            try:
                insales_api.get_categories()
                return
            except Exception, e:
                self.logging.Log(u"Ждем доступность API")
                time.sleep(10)



