﻿import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
from BeautifulSoup import BeautifulStoneSoup as Soup
import MySQLdb
import io
import mimetypes
import json
import hashlib
import time
from lxml.builder import E
from datetime import date
import commonLibrary
from xml.etree import ElementTree
import datetime
import PrestashopAPI
import os

class DellinAPI:
    apiKey = "5EAC8185-B653-4DB1-901A-39C90BBBE680"
    loginUrl = "https://api.dellin.ru/v1/customers/login.xml"
    calculatorUrl = " https://api.dellin.ru/v1/public/calculator.json"
    placesUrl = "https://api.dellin.ru/v1/public/places.xml"
    terminalsUrl = "https://api.dellin.ru/v3/public/terminals.json"
    terminalsJson = None
    login = None
    password = None
    sessionID = None
    presta_api = None

    def __init__(self, login = "", password = ""):
        self.login = login
        self.password = password
        self.presta_api = PrestashopAPI.PrestashopAPI()

    def GetCityTerminals(self, city):
        self.GetTerminalsJson()
        if self.terminalsJson == None:
            return -1

        for city_ in self.terminalsJson["city"]:
            if city_["name"] == city:
                return [x for x in city_["terminals"]["terminal"]]

        return []


    def GetTermHash(self):
        presta_api = PrestashopAPI.PrestashopAPI()
        sql = u"""SELECT hash FROM dellin_term_hash"""
        try:
            hash_ = presta_api.MakeLocalDbGetInfoQueue(sql)[0][0]
        except:
            hash_ = ""
        return hash_

    def SetTermHash(self, hash_):
        presta_api = PrestashopAPI.PrestashopAPI()
        sql = u"""UPDATE dellin_term_hash
                  SET hash = '{0}'""".format(hash_)
        presta_api.MakeLocalDbUpdateQueue(sql)

    def GetCityCLADRCode(self, city):
        self.GetTerminalsJson()
        if self.terminalsJson == None:
            return -1

        for city_ in self.terminalsJson["city"]:
            if city_["name"].lower() == city.lower():
                return city_["code"]

        return -1

    def GetTerminalsJson(self):
        if self.terminalsJson != None:
            return

        data = {}
        data["appkey"] = self.apiKey

        response = self.SendRequest(self.terminalsUrl, data)
        resp = json.loads(response)
        try:
            hash_ = resp["hash"]
            url = resp["url"]
        except:
            return

        oldHash = self.GetTermHash()
        if oldHash != hash_ or os.path.isfile("terminals_v3.json") == False:
            presta_api = PrestashopAPI.PrestashopAPI()
            self.SetTermHash(hash_)
            presta_api.DownloadFile(url, "terminals_v3.json")

        with open("terminals_v3.json", "r") as f:
            jsonContent = f.read()

        self.terminalsJson = json.loads(jsonContent)

    def GetBaseXml(self, sessionid = True):
        requestElem = etree.Element('request')
        doc = etree.ElementTree(requestElem)
        appkey = etree.SubElement(requestElem, 'appkey')
        appkey.text = self.apiKey
        if sessionid == True:
            sessionid = etree.SubElement(requestElem, 'sessionid')
            sessionid.text = self.sessionID
        return [requestElem, doc]

    def Calculate(self, data):
        jData = {}
        jData["appkey"] = self.apiKey
        jData["derivalPoint"] = self.GetCityCLADRCode(data["derivalCity"])
        arrivalPoint = self.GetCityCLADRCode(data["arrivalCity"])
        if arrivalPoint == -1:
            return -1
        jData["arrivalPoint"] = arrivalPoint
        sizedVolume = float(float( data["width"] * data["height"] * data["depth"])/ float(1000000))
        jData["sizedVolume"] = sizedVolume
        jData["sizedWeight"] = data["sizedWeight"]

        response = self.SendRequest(self.calculatorUrl, jData)
        resp = json.loads(response)
        if "errors" in resp.keys():
            for key in resp["errors"].keys():
                print resp["errors"][key]
                return -1
        return resp



    def Login(self):
        requestElem, doc = self.GetBaseXml(False)
        login = etree.SubElement(requestElem, 'login')
        login.text = self.login
        password = etree.SubElement(requestElem, 'password')
        password.text = self.password
        request = """<?xml version="1.0" encoding="UTF-8" ?>""" + etree.tostring(doc)

        response = self.SendRequest(self.loginUrl, request )

        try:
            self.sessionID = etree.HTML(response).xpath("//sessionid")[0].text
        except:
            return -1

        return self.sessionID


    def GetPlaces(self):
        requestElem, doc = self.GetBaseXml(False)
        request = """<?xml version="1.0" encoding="UTF-8" ?>""" + etree.tostring(doc)
        try:
            response = self.SendRequest(self.placesUrl, request )
            with open("places.xml","w") as f:
                f.write(response)
        except:
            return -1

    def SendRequest(self, url, request):
        if url.split(".")[-1] == "xml":
            headers = {'Content-type': 'text/xml'}
            response = requests.post(url, data=request, headers=headers)
        else:
            headers = {'Content-type': 'application/json'}
            response = requests.post(url, json=request, headers=headers)

        return response.text


