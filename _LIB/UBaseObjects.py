
def GetLinkByTitle(title):
    return ["xpath" , u"//a[@title = '" + title + "']"]

def GetLinkByText(text):
    return ["xpath" , u"//a[contains(.,'" + text + "')]"]

def GetButtonByText(text):
    return ["xpath" , u"//button[contains(.,'" + text + "')]"]

def GetElementByText(element, text):
    return ["xpath" , u"//{0}[contains(.,'{1}')]".format(element, text)]

class UBaseObjects:

    Img = ["tagname","img"]
    Ul = ["tagname","ul"]
    Li = ["tagname","li"]
    Span = ["tagname","span"]
    Td = ["tagname","td"]
    Tr = ["tagname","tr"]
    A = ["tagname","a"]
    Textarea = ["tagname","textarea"]