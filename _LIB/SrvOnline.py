﻿from selenium import webdriver
import myLibrary
import USrvOnlineObjects
import datetime
##import win32api
##import win32com.client
import time

login = "pitekantrop@list.ru"
password = "1qaz@WSX1qaz@WSX"
baseUrl = "https://srv-online.ru/"
objects = USrvOnlineObjects.USrvOnlineObjects
today = datetime.date.today()

class SrvOnline:

##    shell = win32com.client.Dispatch("WScript.Shell")
    selenium = None
##    selenium = myLibrary.Selenium()

    def __init__(self):
        pass


    def Login(self):
        if self.selenium == None:
            self.selenium = myLibrary.Selenium()
            self.selenium.SetUp()
        else:
            return

        self.selenium.MaximizeWindow()
        self.selenium.OpenUrl("https://srv-online.ru/lk/")
        self.selenium.SendKeys(objects.login, login)
        self.selenium.SendKeys(objects.password, password)
        self.selenium.Click(objects.loginButton)
        self.selenium.WaitForText("Личный кабинет")

    def SaveData(self):
        self.selenium.Click(objects.saveButton)
        time.sleep(1)
        if self.selenium.IsTextPresent("Данные сохранены"):
            return 0
        else:
            return -1

    def OpenInvoicesList(self, isNovallOrder):
        if self.selenium == None:
            self.selenium = myLibrary.Selenium()
            self.selenium.SetUp()

        self.selenium.OpenUrl(baseUrl + "desktop/docs/list.2013.html")
        self.selenium.WaitFor(objects.invoicesNumbers)
        if self.selenium.IsVisible(objects.invoicesSeller) == False:
            self.selenium.Click(objects.invoicesFilter)
            time.sleep(0.5)
            self.selenium.Click(objects.invoicesSeller)
            self.selenium.WaitFor(objects.SelectOption)
            if isNovallOrder:
                self.selenium.Click(self.selenium.MyFindElements(objects.SelectOption)[1])
            else:
                self.selenium.Click(objects.SelectOption)
            time.sleep(1)
            self.selenium.Click(objects.invoicesSearch)
            self.selenium.WaitForText("Найдено")


    def OpenInvoiceDetails(self, invoiceNumber, isNovallOrder):
        self.OpenInvoicesList(isNovallOrder)
        invoiceLink = [x for x in self.selenium.MyFindElements(objects.invoicesNumbers) if self.selenium.GetText(x) == invoiceNumber][0]
        self.selenium.Click(invoiceLink)
        self.selenium.WaitFor(objects.sellerEdit)

    def GetInvoiceNumber(self, isNovallOrder):
        self.OpenInvoicesList(isNovallOrder)

        invoicesNumbersElems = self.selenium.MyFindElements(objects.invoicesNumbers)
        invoicesDates = self.selenium.MyFindElements(objects.invoicesDates)
        invoicesNumbers = [int(self.selenium.GetText(x).split("-")[0]) for x in invoicesNumbersElems]
        invoicesDates = [ datetime.datetime.strptime(self.selenium.GetText(x).replace(u"от ",""), '%d.%m.%Y') for x in invoicesDates]
        try:
            lastInvoiceDate = list(reversed(sorted(invoicesDates)))[0]
            lastInvoiceDateIndex = list(reversed(sorted([i for i, x in enumerate(invoicesDates) if x == lastInvoiceDate])))[0]
            invoicesArr = invoicesNumbers[0:lastInvoiceDateIndex + 1]
            invoiceNumber = list(reversed(sorted(invoicesArr)))[0] + 1
        except:
            invoiceNumber = 1

        while invoiceNumber in invoicesNumbers:
            invoiceNumber += 1

        return str(invoiceNumber)

    def GetInvoiceDocNumber(self, invoiceNumber, isNovallOrder):
        self.OpenInvoicesList(isNovallOrder)
        invoicesLinks = self.selenium.MyFindElements(objects.invoicesNumbersLinks)
        href = [self.selenium.GetElementAttribute(x, "href") for x in invoicesLinks if self.selenium.GetText(x) == invoiceNumber][0]
        return href.split(".")[-2]

    def SendToEmail(self, email, stamp = True):
        for i in range(0, 10):
            try:
                self.selenium.Click(objects.sendByEmailButton)
                if self.selenium.IsVisible(objects.setAnotherEmail) == True:
                    break
            except:
                self.shell.SendKeys("{UP}")
                time.sleep(1)
        self.selenium.WaitFor(objects.setAnotherEmail)
        if stamp == False:
            self.selenium.Click(objects.stampCheckbox)
        self.selenium.Click(objects.setAnotherEmail)
        self.selenium.WaitFor(objects.anotherEmailEdit)
        self.selenium.SendKeys(objects.anotherEmailEdit, email)
        self.selenium.Click(objects.sendButton)
        self.selenium.WaitForText("E-Mail отправлен")

    def CreateInvoice(self, orderData, contractorData):
        result = self.AddNewContractor(contractorData)
        if result != 0:
            return result

        try:
            invoiceNumber = self.GetInvoiceNumber(orderData["Novall"])

            self.selenium.OpenUrl(baseUrl + "desktop/docs/bill-edit.2013.html")
            self.selenium.WaitFor(objects.sellerEdit)
            self.selenium.SendKeys(objects.invoiceNumber, invoiceNumber)
            self.selenium.SendKeys(objects.invoiceDate, orderData["date"])
            self.selenium.Click(objects.invoiceNumber)
            self.selenium.Sleep(1)
            self.selenium.Click(objects.sellerEdit)
            self.selenium.WaitFor(objects.SelectOption)
            if orderData["Novall"]:
                self.selenium.Click(self.selenium.MyFindElements(objects.SelectOption)[1])
            else:
                self.selenium.Click(objects.SelectOption)
            if "INN" in contractorData:
                self.selenium.SendKeys(objects.contractorEdit, contractorData["INN"])
            else:
                self.selenium.SendKeys(objects.contractorEdit, contractorData["OGRN"])
            self.selenium.WaitFor(objects.SelectOption)
            self.selenium.Sleep(2)
            self.selenium.Click(objects.SelectOption)
            # if orderData["Novall"]:
            #     self.selenium.SetSelectOptionByText(objects.NDSSelect, "в т. ч. НДС 20%")
            # else:
            #     self.selenium.SetSelectOptionByText(objects.NDSSelect, "без НДС")
            self.selenium.Click(objects.deleteItemButton)
            self.selenium.Sleep(1)

            for orderItem in orderData["orderItems"]:
                self.selenium.Click(objects.addItemButton)
                self.selenium.Sleep(1)
                self.selenium.SendKeys(self.selenium.MyFindElements(objects.itemName)[-1], orderItem["Name"])
                self.selenium.SendKeys(self.selenium.MyFindElements(objects.itemPrice)[-1], orderItem["Price"])
                self.selenium.SendKeys(self.selenium.MyFindElements(objects.itemAmount)[-1], orderItem["Amount"])

            if orderData["postageIncluded"] == False:
                if orderData["orderDeliveryCost"] != 0:
                    self.selenium.Click(objects.addItemButton)
                    self.selenium.Sleep(1)
                    self.selenium.SendKeys(self.selenium.MyFindElements(objects.itemName)[-1], u"Доставка " + orderData["orderDeliveryDescription"])
                    self.selenium.SendKeys(self.selenium.MyFindElements(objects.itemPrice)[-1], str(orderData["orderDeliveryCost"]))
                    self.selenium.SendKeys(self.selenium.MyFindElements(objects.itemAmount)[-1], "1")
            else:
                self.selenium.SendKeys(objects.invoiceAdditionalInfo, u"В цену товара включена стоимость доставки " + orderData["orderDeliveryDescription"])

            while self.SaveData() == -1:
                invoiceNumber = str(int(invoiceNumber) + 1)
                self.selenium.SendKeys(objects.invoiceNumber, invoiceNumber)
                time.sleep(1)

            docNumber = self.GetInvoiceDocNumber(invoiceNumber, orderData["Novall"])
            return [invoiceNumber, docNumber]
        except:
            self.CloseSelenium()
            return -1

    def CreatePackingList(self, invoiceNumber, isNovallOrder ):
        try:
            self.OpenInvoiceDetails(invoiceNumber,isNovallOrder)
            self.selenium.Click(objects.contextMenu)
            self.selenium.WaitFor(USrvOnlineObjects.GetLinkByText(u"Товарная накладная"))
            self.selenium.Click(USrvOnlineObjects.GetLinkByText(u"Товарная накладная"))
            self.selenium.WaitFor(objects.packingListNumber)
            self.selenium.SendKeys(objects.packingListNumber, invoiceNumber)
            date = today.strftime('%d.%m.%Y')
            self.selenium.SendKeys(objects.packingListDate, date)

            self.SaveData()

        except:
            self.CloseSelenium()
            return -1

    def CreateContract(self, invoiceNumber, orderDeliveryDescription, isNovallOrder):
        try:
            self.OpenInvoiceDetails(invoiceNumber, isNovallOrder)
            self.selenium.Click(objects.contextMenu)
            self.selenium.WaitFor(USrvOnlineObjects.GetLinkByText(u"Договор"))
            self.selenium.Click(USrvOnlineObjects.GetLinkByText(u"Договор"))
            self.selenium.WaitFor(objects.contractNumber)
            self.selenium.SendKeys(objects.contractNumber, invoiceNumber)
            self.selenium.SendKeys(objects.contractDate, today.strftime('%d.%m.%Y'))
            if isNovallOrder:
                self.selenium.SetSelectOptionByIndex(objects.contractPrintForm, 7)
            else:
                self.selenium.SetSelectOptionByIndex(objects.contractPrintForm, 6)

            if orderDeliveryDescription != "":
                self.selenium.SendKeys(objects.contractAdditionalInfo, u"4. В цену товара включена стоимость доставки " + orderDeliveryDescription)

            self.SaveData()
            return invoiceNumber

        except:
            self.CloseSelenium()
            return -1

    def AddNewContractor(self, contractorData):
        try:
            self.selenium.OpenUrl(baseUrl + "desktop/docs/org-edit.2013.html")
            self.selenium.WaitFor(objects.INNEdit)

            self.selenium.SendKeys(objects.INNEdit, contractorData["INN"])
            self.selenium.Sleep(1)
            if self.selenium.IsVisible(objects.SelectOption):
                return 0
                self.selenium.Click(objects.KPPEdit)

            self.selenium.Click(objects.fillByINNButton)
            self.selenium.Sleep(2)
            self.selenium.WaitForNotVisible(objects.loaderElement)
            self.selenium.Sleep(2)
            if self.selenium.GetElementText( objects.contractorEdit) == "":
                self.CloseSelenium()
                return -3
            if "KPP" in contractorData.keys():
                self.selenium.SendKeys(objects.KPPEdit, contractorData["KPP"])
            if "OGRN" in contractorData.keys():
                self.selenium.SendKeys(objects.OGRNEdit, contractorData["OGRN"])

            self.selenium.Click(objects.bankRequisitesLink)
            self.selenium.WaitFor(objects.BIKEdit)
            if "BIK" in contractorData.keys():
                self.selenium.SendKeys(objects.BIKEdit, contractorData["BIK"])
                time.sleep(4)
                if self.selenium.IsVisible(objects.SelectOption) == False:
                    self.CloseSelenium()
                    return -2
                self.selenium.Click(objects.SelectOption)
                self.selenium.SendKeys(objects.RSEdit, contractorData["RS"])

            self.SaveData()
            return 0

        except:
            self.CloseSelenium()
            return -1



    def CloseSelenium(self):
        try:
            self.selenium.CleanUp()
        except:
            pass
        self.selenium = None

