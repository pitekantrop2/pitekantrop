﻿# -*- coding: utf-8 -*-

import subprocess
import PrestashopAPI
import time
import psutil
import ftplib
import requests
import HTMLParser
import html2text
import re, urlparse
import datetime
import socket
from lxml import etree
import random
from datetime import date
import paramiko
import urllib
import WebasystAPI
import ftplib
import shutil

puttyPath = 'c:\\temp\\plink.exe'
sshLogin = "root"
sshPassword = "IiRMBuAiJTLv"

basePath = ""
wa_basePath = "webasyst/"

wa_sitesParams = [
{"url":"http://mini-camera.ru.com", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://insect-killers.ru", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugivateli-sobak.ru", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugivateli-ptic.ru", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugivateli-grizunov.ru", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugiwatel.com", "ip":"95.216.145.123", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://knowall.ru.com", "ip":"95.216.145.123", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://safetus.ru", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugivateli-krotov.ru", "ip":"65.108.89.105", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugivatel.com", "ip":"65.21.1.62", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://glushilki.ru.com", "ip":"65.21.1.62", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://incubators.shop", "ip":"65.21.1.62", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://saltlamp.su", "ip":"65.21.1.62", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"},
{"url":"http://otpugivatel.spb.ru", "ip":"65.21.1.62", "access_token":"d2d9c1716c77444abf034df4e77fb1d3"}
]

labels = ["SDEKMoscow",
"SititekMoscow",
"SititekIzhevsk",
"SititekSpb",
"SDEKSpb",
"Logos",
"CNT",
"ATEK",
"Telesys",
"Kvarts",
"31Vek",
"Polikon",
"Chiston",
"Well",
"Bereg",
##"Ust",
"Almaz",
"Palmira",
"Spectr",
"Bios",
"Alex",
"Kvazar",
"Diktos",
"Eco"
]

contractorsLabels = {
"Logos":u"Логос",
"SititekMoscow":u"Сититек",
"Telesys":u"Телесистемы",
"31Vek":u"31 Век",
"Kvarts":u"Кварц",
"CNT":u"Торнадо",
"Chiston":u"Чистон",
"Beevid":u"Beevid",
"Polikon":u"Поликон",
"ATEK":u"Атек",
"Well":u"Велл",
"Ust":u"ЮСТ",
"Spectr":u"Спектр-прибор",
"Bereg":u"Берег мечты",
"Bios":u"Биос",
"Palmira":u"Пальмира",
"Alex":u"Алекс",
"Almaz":u"Алмаз",
"Kvazar":u"Квазар",
"Diktos":u"Диктос"
}

mainCities1 = [
u"Санкт-Петербург",
u"Москва",
u"Новосибирск",
u"Краснодар",
u"Ростов-на-Дону",
u"Нижний Новгород",
u"Пермь",
u"Казань",
u"Воронеж",
u"Уфа",
u"Севастополь",
u"Екатеринбург",
u"Самара",
u"Тула",
u"Челябинск",
u"Красноярск",
u"Зеленоград",
u"Рязань",
u"Пенза",
u"Калининград",
u"Волгоград",
u"Саратов",
u"Омск",
u"Тюмень",
u"Симферополь",
u"Калуга",
u"Сочи",
u"Ижевск",
u"Липецк",
u"Тольятти",
u"Анапа",
u"Тверь",
u"Брянск",
u"Ялта",
u"Оренбург",
u"Иваново",
u"Кемерово",
u"Хабаровск",
u"Белгород",
u"Новороссийск"
]

mainCities2 = [
u"Иркутск",
u"Ярославль",
u"Ставрополь",
u"Курск",
u"Владимир",
u"Псков",
u"Смоленск",
u"Вологда",
u"Дмитров",
u"Мытищи",
u"Подольск",
u"Барнаул",
u"Череповец",
u"Одинцово",
u"Томск",
u"Коломна",
u"Новокузнецк",
u"Набережные Челны",
u"Тамбов",
u"Астрахань",
u"Абакан",
u"Чебоксары",
u"Чехов",
u"Великий Новгород",
u"Ульяновск",
u"Феодосия",
u"Сургут",
u"Сергиев Посад",
u"Таганрог",
u"Мурманск",
u"Королев",
u"Геленджик",
u"Серпухов",
u"Петрозаводск",
u"Обнинск",
u"Киров",
u"Дзержинск",
u"Раменское",
u"Саранск",
u"Евпатория"
]

backofficeUrls = {
"http://safetus.ru":"http://webasyst2.ru/webasyst/shop/",
"http://knowall.ru.com":"http://webasyst1.ru/webasyst/shop/",
"http://otpugiwatel.com":"http://webasyst1.ru/webasyst/shop/",
"http://mini-camera.ru.com":"http://webasyst2.ru/webasyst/shop/",
"http://otpugivatel.com":"http://webasyst3.ru/webasyst/shop/",
"http://saltlamp.su":"http://webasyst3.ru/webasyst/shop/",
"http://insect-killers.ru":"http://webasyst2.ru/webasyst/shop/",
"http://glushilki.ru.com":"http://webasyst3.ru/webasyst/shop/",
"http://otpugivateli-grizunov.ru":"http://webasyst2.ru/webasyst/shop/",
"http://otpugivateli-krotov.ru":"http://webasyst2.ru/webasyst/shop/",
"http://otpugivateli-ptic.ru":"http://webasyst2.ru/webasyst/shop/",
"http://otpugivateli-sobak.ru":"http://webasyst2.ru/webasyst/shop/",
"http://incubators.shop":"http://webasyst3.ru/webasyst/shop/",
"http://otpugivatel.spb.ru":"http://webasyst3.ru/webasyst/shop/"
}

contractorsInn = {
"9701143437":"31Vek",
"183102871461":"SititekMoscow",
"6322025466":"CNT",
"132302532487":"SititekSpb",
"6345025310":"Alex"
}

excludedCities = u"""Брест,Минск,Ереван, Восточный мкр., Купино, Баган, Пограничный, Рассказово, Могилев, Киржач, Ордынское, Кочки, Карасук, Нижняя Тура,
Пекин, Гуанчжоу, Шанхай, Иу, Атырау, Гомель, Хэйхэ, Шэньчжэнь, Бишкек, Цзинань, Маньчжурия, Красная Поляна Кольцово Дрожжино Шаховская Куровское
Суйфэньхэ, Урумчи, Чулым, Чистоозерное, Сузун, Петропавловск, Москва,Киселёвск,Воскресенское поселение,ВНИИССОК,Химки Новые,
Довольное,  Северное, Убинское, Болотное, Елизово,  Баодин, Харбин, Кыштовка, Коротчаево Строитель Сколково инновационный центр Десеновское
Каргат, Здвинск, Учалы, Чаны, Гродно, Венгерово, Тогучин, Борисов, Цзиньхуа, Омск Чаочжоу Бобруйск Витебск  Наньтун  Паттайя Актобе
Уральск, Усть-Тарка, Ханчжоу, Чжэнчжоу, Нинбо, Талдыкорган Донецк"""

fragileItems = [u"Экоснайпер GC1-40", u"Экоснайпер GC1-16", u"GC1-60", u"Экоcнайпер GF-4WB", u"Экоснайпер LS-217", u"МТ-200", u"МТ64", u"MT100"]

def GetWebasystInstance(site, logFilePath = None):
    success = False
    counter = 0
    while success == False:
        if counter > 5:
            break
        try:
            print time.strftime("%H:%M:%S") + " connecting to " + site["url"]
            if GetMachineName().find("hp") != -1 or GetMachineName().find("pitekantrop") != -1:
                wa_api = WebasystAPI.WebasystAPI(site["url"], site["ip"], site["access_token"], logFilePath)
            else:
                wa_api = WebasystAPI.WebasystAPI(site["url"], site["ip"], site["access_token"], logFilePath)
            success = True
        except Exception as e:
            print str(e)
            time.sleep(2)
            counter += 1

    if success == False:
        return False

    return wa_api

def GetPrestashopInstance(site, logFilePath = None):
    success = False
    counter = 0
    while success == False:
        if counter > 5:
            break
        try:
            print time.strftime("%H:%M:%S") + " connecting to " + site["url"]
            if GetMachineName().find("hp") != -1 or GetMachineName().find("pitekantrop") != -1:
                prestashop_api = PrestashopAPI.PrestashopAPI(site["url"], site["apiKey"], site["ip"], site["dbName"], site["dbUser"], site["dbPassword"], 3306, logFilePath)
            else:
                prestashop_api = PrestashopAPI.PrestashopAPI(site["url"], site["apiKey"], site["ip"], site["dbName"], site["dbUser"], site["dbPassword"], 3306, logFilePath)
            success = True
        except Exception as e:
            print str(e)
            time.sleep(2)
            counter += 1
    if success == False:
        return False
    return prestashop_api

def IsPuttyRunning():
    for process in psutil.process_iter():
        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "putty.exe":
            return True

    return False

def GetContractorName(article):
    if article == -1:
        return -1

    if article.startswith("lg"):
        return u"Логос"
    if article.startswith("st"):
        return u"Сититек"
    if article.startswith("ts"):
        return u"Телесистемы"
    if article.startswith("v"):
        return u"31 Век"
    if article.startswith("kv"):
        return u"Кварц"
    if article.startswith("cnt"):
        return u"Торнадо"
    if article.startswith("chs"):
        return u"Чистон"
    if article.startswith("bv"):
       return u"Beevid"
    if article.startswith("me"):
        return u"Поликон"
    if article.startswith("at"):
        return u"Атек"
    if article.startswith("nt"):
        return u"Наш товар"
    if article.startswith("sp"):
        return u"Спектр-прибор"
    if article.startswith("ap"):
        return u"Агропласт"
    if article.startswith("pm"):
        return u"Берег мечты"
    if article.startswith("ww") or article.startswith("pg"):
        return u"Велл"
    if article.startswith("ust"):
        return u"ЮСТ"
    if article.startswith("bs"):
        return u"Биос"
    if article.startswith("ax"):
        return u"Алекс"
    if article.startswith("te"):
        return u"Техноэкспорт"
    if article.startswith("bb"):
        return u"Велл"
    if article.startswith("kz"):
        return u"Квазар"
    if article.startswith("bz"):
        return u"Алмаз"
    if article.startswith("dt"):
        return u"Диктос"
    if article.startswith("ld"):
        return u"Ладья"

    return -1

def GetOrderSender(orderNote):
    for label in labels:
        if orderNote.lower().find(label.lower()) != -1:
            return label
    return -1

def GetCityDeclension(name):
    sql = u"""SELECT * FROM cities
              WHERE city_I = '{0}'""".format(name)
    try:
        return MakeLocalDbGetInfoQueue(sql)[0]
    except:
        return -1

def KillPutty():
    for process in psutil.process_iter():

        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "putty.exe":
            process.kill()

def KillChromedriver():
    for process in psutil.process_iter():

        try:
            processName = process.name().lower()
        except:
            continue
        if processName in ["chromedriver.exe", "chrome.exe"] :
            try:
                process.kill()
            except:
                pass


def GetPid():
    for process in psutil.process_iter():
        try:
            processName = process.name().lower()
        except:
            continue
        if processName == "python.exe":
            return process.pid

def GetOrderIsAgreed(orderNote):
    if len(orderNote.strip()) < 2 \
    or orderNote.lower().find(u"звон") != -1 \
    or orderNote.lower().find(u"тправ") != -1 \
    or orderNote.lower().find(u"связа") != -1:
        return False

    return True

def GetOrderIsSingle(orderNote):
    if orderNote.lower().find(u"singleorder") != -1:
        return True

    return False

def GetOrderPlaces(orderNote):
    try:
        parts = orderNote.split("\n")
        for part in parts:
            if part.find("places") != -1:
                return part.replace("places", "")
    except:
        return -1

    return -1


def GetOrderPaid(orderNote):
    if orderNote.lower().find(u"оплачен") != -1:
        return True
    else:
        return False

def GetOrderLocked(orderNote):
    if orderNote.lower().find(u"locked") != -1:
        return True
    else:
        return False

def GetNovallOrder(orderNote):
    if orderNote.lower().find(u"novall") != -1:
        return True
    else:
        return False

def GetPvzCode(orderNote):
    try:
        parts = orderNote.split("\n")
        for part in parts:
            if part.find("pvzCode") != -1:
                return part.replace("pvzCode", "")
    except:
        return -1

    return -1

def GetTariffCode(orderNote):
    try:
        parts = orderNote.split("\n")
        for part in parts:
            if part.find("tariffCode") != -1:
                return part.replace("tariffCode", "")
    except:
        return -1

    return -1

def GetRequisition(orderNote):
    parts = orderNote.split(" ")

    for part in parts:
        if len(part) == 7:
            if part.isdigit() == True:
                return part

    parts = orderNote.split("\n")

    for part in parts:
        if len(part) == 7:
            if part.isdigit() == True:
                return part

    return -1

def GetInvoiceNumber(orderNote):
    parts = orderNote.split(" ")

    for part in parts:
        part = part.strip()
        if len(part) == 10:
            if part.isdigit() == True:
                return part

    parts = orderNote.split("\n")

    for part in parts:
        part = part.strip()
        if len(part) == 10:
            if part.isdigit() == True:
                return part

    return -1

def GetCourierRequestParams(contractor):
    if contractor not in labels:
        return -1

    today = datetime.date.today()
    sToday = today.strftime('%Y-%m-%d')
    sTomorrow = (today + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    now = datetime.datetime.now()

    data = {}
    CourierTimeBeg = "14:59"
    CourierTimeEnd = "17:59"
    if now.hour < 15:
        CourierDate = sToday
    else:
        CourierDate = sTomorrow

    if contractor == "SititekMoscow":
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"Окружной проезд"
        data["SendHouse"] = u"д.5, стр.1"
        data["SendFlat"] = "11"
        data["SendPhone"] = "89856463294"
        data["SenderName"] = u"Усов Иван Викторович"
        data["SenderCompany"] = u"ИП Березина Александра Владимировна"
        data["NeedPickup"] = True

    if contractor == "SDEKMoscow":
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"Энергетиков"
        data["SendHouse"] = u"22 корпус 2"
        data["SendFlat"] = "1"
        data["SendPhone"] = "88002500405"
        data["SenderName"] = u"Фулфилмент"
        data["NeedPickup"] = False

    if contractor == "SDEKSpb":
        data["SendCityCode"] = "137"
        data["SendStreet"] = u"Лиговский проспект"
        data["SendHouse"] = u"87"
        data["SendFlat"] = "621"
        data["SendPhone"] = "9697333555"
        data["SenderName"] = u"Усов Иван"
        data["NeedPickup"] = False

    if contractor == "SititekSpb":
        data["SendCityCode"] = "137"
        data["SendStreet"] = u"Лиговский проспект"
        data["SendHouse"] = u"50, к. 3"
        data["SendFlat"] = "1"
        data["SendPhone"] = "(812)2449474"
        data["SenderName"] = u"Нагибин Александр"
        data["NeedPickup"] = True

    if contractor == "SititekIzhevsk":
        CourierTimeBeg = "13:59"
        CourierTimeEnd = "16:59"
        data["SendCityCode"] = "224"
        if now.hour + 1 < 15:
            CourierDate = sToday
        else:
            CourierDate = sTomorrow
        data["SendStreet"] = u"Карла Маркса"
        data["SendHouse"] = u"1A"
        data["SendFlat"] = "414"
        data["SendPhone"] = "+7(922)511-55-50"
        data["SenderName"] = u"ИП Березина А.В."
        data["NeedPickup"] = True
        data["SenderCompany"] = u"ИП Березина А.В."

    if contractor == "Logos":
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"Коломенская"
        data["SendHouse"] = u"21/3"
        data["SendFlat"] = "455"
        data["SendPhone"] = "89645823161"
        data["SenderName"] = u"ИП Викулин А.М."
        data["NeedPickup"] = True

    if contractor == "CNT":
        data["SendCityCode"] = "431"
        if now.hour + 1 < 15:
            CourierDate = sToday
        else:
            CourierDate = sTomorrow
        data["SendStreet"] = u"Ярославская"
        data["SendHouse"] = u"12, стр.15"
        data["SendFlat"] = "1"
        data["SendPhone"] = "8-800-3333-900"
        data["SenderName"] = u"ООО МНПФ Центр Новые Технологии"
        data["NeedPickup"] = False

    if contractor == "Alex":
        data["SendCityCode"] = "431"
        data["SendStreet"] = u"Транспортная"
        data["SendHouse"] = u"19"
        data["SendFlat"] = "1"
        data["SendPhone"] = "+7-927-612-11-74"
        data["SenderName"] = u"ООО ТД Алекс"
        data["NeedPickup"] = False

    if contractor == "ATEK":
        data["SendCityCode"] = "474"
        data["SendStreet"] = u"ул. Спортивная"
        data["SendHouse"] = u"4А"
        data["SendFlat"] = "1"
        data["SendPhone"] = "+7(495)604-16-14"
        data["SenderName"] = u"Круглов Владимир Дмитриевич"
        data["NeedPickup"] = True

    if contractor == "Telesys":
        data["SendCityCode"] = "184"
        data["SendStreet"] = u"Сосновая аллея"
        data["SendHouse"] = u"10, стр. 1"
        data["SendFlat"] = "1"
        data["SendPhone"] = "89654119006"
        data["SenderName"] = u"ООО Телесистемы"
        data["SenderCompany"] = u"ООО Телесистемы"
        data["NeedPickup"] = True

    if contractor == "Kvarts":
        data["SendCityCode"] = "137"
        data["SendStreet"] = u"Зои Космодемьянской"
        data["SendHouse"] = u"10"
        data["SendFlat"] = "1"
        data["SendPhone"] = "89523501120"
        data["SenderName"] = u"Шигорин Алексей Александрович"
        data["NeedPickup"] = True

    if contractor == "31Vek":
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"Окружной проезд"
        data["SendHouse"] = u"6, стр.1"
        data["SendFlat"] = "1"
        data["SendPhone"] = "89255535601"
        data["SenderName"] = u"ООО Экоснайпер"
        data["SenderCompany"] = u"ООО Экоснайпер"
        data["NeedPickup"] = True

##    if contractor == "Polikon":
##        data["SendCityCode"] = "44"
##        data["SendStreet"] = u"Щелковское шоссе"
##        data["SendHouse"] = u"100, корпус 108"
##        data["SendFlat"] = "140"
##        data["SendPhone"] = "+74956656012"
##        data["SenderName"] = u"ООО Поликон"
##        data["NeedPickup"] = True

    if contractor == "Polikon":
        data["SendCityCode"] = "76"
        data["SendStreet"] = u"Щелковское шоссе"
        data["SendHouse"] = u"пр-т Мира, 29"
        data["SendFlat"] = "1"
        data["SendPhone"] = "+74956656012"
        data["SenderName"] = u"ООО Поликон"
        data["NeedPickup"] = False

    if contractor == "Bereg":
        data["SendCityCode"] = "250"
        data["SendStreet"] = u"ул. Монтерская"
        data["SendHouse"] = u"5"
        data["SendFlat"] = "1"
        data["SendPhone"] = "8(922)033-55-44"
        data["SenderName"] = u"Комлева Ольга Владимировна"
        data["SenderCompany"] = u"ООО Соль Земли"
        data["NeedPickup"] = False

    if contractor == "Well":
        data["SendCityCode"] = "45"
        data["SendStreet"] = u"Нагорное шоссе"
        data["SendHouse"] = u"2, кор.9"
        data["SendFlat"] = "305"
        data["SendPhone"] = "84954872067"
##        data["SendPhone"] = "+7 965 154 45 53"
        data["SenderName"] = u"ООО ВЕЛЛ-ВЕ"
        data["NeedPickup"] = True

    if contractor == "Ust":
        data["SendCityCode"] = "395"
        data["SendStreet"] = u"Рыленкова"
        data["SendHouse"] = u"61"
        data["SendFlat"] = "1"
        data["SendPhone"] = "+7 (958)756 87 04"
        data["SenderName"] = u"Людмила"
        data["NeedPickup"] = False

    if contractor == "Chiston":
        data["SendCityCode"] = "1022"
        data['tariffCode'] = "136"
        data["SendStreet"] = u"Кошевого"
        data["SendHouse"] = u"1"
        data["SendFlat"] = "1"
        data["SendPhone"] = "8 929 727 41 30"
        data["SenderName"] = u"Лилия"
        data["NeedPickup"] = False

    if contractor == "Bios":
        data["SendCityCode"] = "395"
        data["SendStreet"] = u"Верхне-Сенная"
        data["SendHouse"] = u"1"
        data["SendFlat"] = "1"
        data["SendPhone"] = "(4812) 24-02-54"
        data["SenderName"] = u"НПП БИОС"
        data["SenderCompany"] = u"НПП БИОС"
        data["NeedPickup"] = False

    if contractor == "Almaz":
        CourierTimeBeg = "13:59"
        CourierTimeEnd = "16:59"
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"2-й Хорошёвский проезд"
        data["SendHouse"] = u"7, стр.1А"
        data["SendFlat"] = "19"
        data["SendPhone"] = "(495)734-99-54"
        data["SenderName"] = u"ООО Алмаз"
        data["SenderCompany"] = u"ООО Алмаз"
        data["NeedPickup"] = True

    if contractor == "Kvazar":
        CourierTimeBeg = "13:59"
        CourierTimeEnd = "16:59"
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"Михайловский проезд"
        data["SendHouse"] = u"3с66"
        data["SendFlat"] = "1"
        data["SendPhone"] = "+7(903)004-08-00"
        data["SenderName"] = u"ООО КВАЗАР"
        data["SenderCompany"] = u"ООО КВАЗАР"
        data["NeedPickup"] = True


    if contractor == "Eco":
        CourierTimeBeg = "13:59"
        CourierTimeEnd = "16:59"
        data["SendCityCode"] = "44"
        data["SendStreet"] = u"Рябиновая улица"
        data["SendHouse"] = u"55с3"
        data["SendFlat"] = "5-6"
        data["SendPhone"] = "+7 (977) 885-87-95"
        data["SenderName"] = u"ООО «ЭКО ПЛЮС»"
        data["SenderCompany"] = u"ООО «ЭКО ПЛЮС»"
        data["NeedPickup"] = False

    data["CourierDate"] = CourierDate
    data["CourierTimeBeg"] = CourierTimeBeg
    data["CourierTimeEnd"] = CourierTimeEnd

    return data


def UnescapeText(text):
    h = HTMLParser.HTMLParser()
    regexp = "&.+?;"
    list_of_html = re.findall(regexp, text) #finds all html entites in page
    for e in list_of_html:
        unescaped = h.unescape(e) #finds the unescaped value of the html entity
        text = text.replace(e, unescaped) #replaces html entity with unescaped value

    return text

def ClearPhoneNumber(number):
    if number.startswith("8") or number.startswith("7"):
        number = number[1:]
    if number.startswith("+7"):
        number = number[2:]

    number = number.replace("(", "").replace(")", "").replace("-", "").replace(" ", "")

    return number

def GetVendorCode():
    prestashop_api = WebasystAPI.WebasystAPI()
    today = datetime.date.today()
    warehouseCodes = [x[3] for x in prestashop_api.GetWarehouseProducts("warehouse_moscow")]
    vendorCode = "1" + today.strftime('%d%m%Y')
    i = 1
    while vendorCode in warehouseCodes:
        i += 1
        vendorCode = str(i) + today.strftime('%d%m%Y')

    return vendorCode

def AddProductToWarehouse(warehouse, data):
    if warehouse == "warehouse_spb":
        sql = u"""INSERT INTO {0}(ItemName)
             VALUES ('{1}')""".format(warehouse, data["Name"])
    elif warehouse == "warehouse_moscow":
        sql = u"""INSERT INTO {2}(ItemName, WarehouseName, Sku_id)
             VALUES ('{0}', '{1}', {3})""".format(data["Name"], data["Article"], warehouse, data["Sku_id"])
    else:
        sql = u"""INSERT INTO {2}(ItemName, Sku_id)
             VALUES ('{0}', '{1}')""".format(data["Name"],  data["Sku_id"], warehouse)

    MakeLocalDbUpdateQueue(sql)

def CleanItemName(itemName):
    if itemName == -1:
        return ""
    name = itemName
    if name.find(u"«") != -1:
        name = name[name.index(u"«") + 1:name.index(u"»")]

    if name.find(u"\"") != -1:
        name = name.split(u"\"")[-2]

    return name

def GetInvoiceState(state):
    if state == "delivered":
        return u"Вручен"
    if state == "shipped":
        return u"Принят на склад"
    if state == "in_transit":
        return u"В пути"
    if state == "in_process":
        return u"Создан"

    return -1

def GetPostRefundState(states):
    if u"Возврат - Иные обстоятельства" not in states and u"Возврат - Истек срок хранения" not in states:
        return -1
    try:
        index = states.index(u"Возврат - Истек срок хранения")
    except:
        index = states.index(u"Возврат - Иные обстоятельства")
    states = states[0:index]
    if len(states) == 0:
        return u"Создан"
    if u"Вручение - Вручение адресату" in states:
        return u"Вручен"
    if u"Обработка - Прибыло в место вручения" in states:
        return u"Принят на склад"

    return u"В пути"


def ParseItems(items):
    itemsNames = [x.split("_")[0].strip() for x in items.split("/") ]
    itemsQuantities = [x.split("_")[1].strip() for x in items.split("/") ]

    return [itemsNames, itemsQuantities]

def GetLocalIpAddress():
    return socket.gethostbyname(socket.gethostname())

def GetMachineName():
    return socket.getfqdn()

def GetTariff(contractorDeliveriesNumber, city, contractor):
    tariff = ""
    if contractor == "SititekMoscow":
        tariff = u"Экспресс лайт"

    if contractor == "SititekIzhevsk":
        if city.lower() == u"москва":
            tariff = u"Магистральный экспресс"
        else:
            tariff = u"Экспресс лайт"

    if contractor == "Kvarts":
        if city.lower() == u"москва":
            tariff = u"Магистральный экспресс"
        else:
            tariff = u"Экспресс лайт"

    if contractor == "Logos":
        if city.lower() == u"москва":
            tariff = u"Экспресс лайт"
        else:
            tariff = u"Экспресс лайт"

    if contractor == "Telesys":
        if city.lower() == u"москва":
            tariff = u"Экспресс лайт"
        else:
            tariff = u"Экспресс лайт"

    if contractor == "Polikon":
        if city.lower() == u"москва":
            tariff = u"Экспресс лайт"
        else:
            tariff = u"Магистральный экспресс"

    if contractor == "CNT":
        if city.lower() == u"москва":
            tariff = u"Экспресс лайт"
        else:
            tariff = u"Экспресс лайт"

    if contractor == "31Vek":
        if city.lower() == u"москва":
            tariff = u"Экспресс лайт"
        else:
            tariff = u"Магистральный экспресс"

    if contractor == "Ust":
        tariff = u"Экспресс лайт склад-склад"

    if contractor == "ATEK":
        if city.lower() == u"москва":
            tariff = u"Экспресс лайт"
        else:
            tariff = u"Магистральный экспресс"

    if contractor == "Chiston":
        tariff = u"Магистральный экспресс"

    if contractor in ["Bereg", "SDEKMoscow", "SDEKSpb", "Spectr"]:
        tariff = u"Магистральный экспресс"

    if tariff == "":
        return -1

    if tariff == u"Магистральный экспресс":
        return tariff + u" склад-склад"

    if tariff.find(u"склад") == -1 and tariff.find(u"дверь") == -1:
        if contractorDeliveriesNumber != 1:
            tariff += u" склад-склад"
        else:
            tariff += u" дверь-склад"

    return tariff

def GetSdekCityCode(name):
    prestashop_api = WebasystAPI.WebasystAPI()
    sql = u"""SELECT id FROM sdek_cities_codes
              WHERE city_name = '{0}'""".format(name)
    data = MakeLocalDbGetInfoQueue(sql)
    if len(data) == 0:
        return -1
    return data[0][0]

def IsProductExpected(city, name, quantity):
    if city == u"Москва":
        sql = u"""SELECT items FROM deliveries
                 WHERE recipientCity = '{0}'""".format(u"Москва")
    else:
        sql = u"""SELECT items FROM deliveries
                 WHERE recipientCity <> '{0}'""".format(u"Москва")

    data = MakeLocalDbGetInfoQueue(sql)
    if len(data) == 0:
        return False
    allItems = "/".join( [x[0] for x in data])
    itemsNames, itemsQuantities = ParseItems(allItems)
    for i in range(0, len(itemsNames)):
        itemName = itemsNames[i]
        itemQty = int(itemsQuantities[i])
        if itemName == name:
            if itemQty >= quantity:
                return True

    return False

def GetContractorLabel(contractor):
    try:
        return [x for x in contractorsLabels.keys() if contractorsLabels[x] == contractor][0]
    except:
        return -1

def GetRequests():
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetRequests()

def GetDeliveriesByDate(date):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetDeliveriesByDate(date)

def GetShopName(shopName):
    if shopName in ["otpugiwatel", "Safetus.Ru", "AmazingThings", "Knowall"]:
        shopName = u"Москва"

    if shopName in [ "Novall"]:
        shopName = u"Омск"

    if shopName == u"Спб":
        shopName = u"Санкт-Петербург"

    return shopName


def GetSiteUrlScheme(site):
    if site in ["http://amazing-things.ru", "http://sushilki.ru.com", "http://rose-of-bulgaria.ru"]:
        return "http://"
    else:
        return "https://"

def GetStringDescription(description):
    if type(description) not in [type(u"123"), type("qqwe")] :
        description = etree.tostring(description)

    description = UnescapeText(description)
    description = description.replace("<html>","")
    description = description.replace("</html>","")
    description = description.replace("<body>","")
    description = description.replace("<p/>","")
    description = description.replace("</body>","")
    description = description.replace("<body/>","")
    description = description.replace("""<p align="justify"><strong/>""", """<p align="justify">""")
    description = description.replace("""<p style="text-align: left;"><strong/></p>""", """""")
    description = description.replace("""<p><br/><br/><br/><br/></p>""", """""")
    description = description.replace("""<p><strong/></p>""", """""")
    description = description.replace("""<p></p>""", """""")
    description = description.replace("""allowfullscreen"/>""","""allowfullscreen"></iframe>""")
    description = description.replace("""allowfullscreen=\"\""/>""","""allowfullscreen"></iframe>""")
    description = description.replace("""\"/>""","""\"></iframe>""")
    description = description.replace("width=\"420\"/>","width=\"420\"></iframe>")
    description = description.replace("""justify""", """left""")
    if description.find(u"</iframe>") != -1 and description.find(u"<iframe>") == -1:
        description = description.replace("""</iframe>""", """""")

    chars = [u"\xd8",u"\xba",u"\u2264",u"\u2212",u"\u02da",u"\xd7", u"\xf7", u"\u2219", u'\u25b7', u'\u20bd', u'\u2060']
    for char in chars:
        description = description.replace(char, " ")

    return description

notChangedWords = [u"интернета",u"домофона", u"навигация", u"Шкатулка", u"вызова", u"видеодомофона", u"тела", u"лица",u"телефона", u"голоса", u"дыма", u"замка", u"сигнала", u"клеща", u"хищника", u"дома", u"на", u"света", u"Ангара", u"(на", u"Хозяйка", u"крота", u"Золушка", u"Скала"]

def ChangeCase(text):
    text = text.strip()
    changedText = u""
    words = text.split(" ")
    bProductArticle = False
    for word in words:
        bNotChangeWord = False
        word_ = word
        if word_ == "":
            continue
        if word_[0] in ["\"", u"«"]:#начало наименования
            bProductArticle = True
        if word_ in notChangedWords or bProductArticle == True:
            bNotChangeWord = True
        if word_[-1] in ["\"", u"»"]:#конец наименования
            bProductArticle = False
        if bNotChangeWord == True:
            changedText += u" {0}".format(word_)
            continue
        if len(word_) > 2:
            wordParts = word_.split("-")
            for wordPart in wordParts:
                if wordPart in notChangedWords:
                    continue
                wordPart_ = wordPart
                if wordPart[-1] == u"а":
                    wordPart_ = wordPart[0:-1] + u"у"
                if wordPart[-2:] == u"ая":
                    wordPart_ = wordPart[0:-2] + u"ую"
                if wordPart[-3:] == u"ция":
                    wordPart_ = wordPart[0:-3] + u"цию"
                word_ = word_.replace(wordPart,wordPart_)
        changedText += u" {0}".format(word_)

    changedText = changedText.strip()
    return changedText


def CorrectHtml(descr):
    elements = descr.xpath("//table")
    for element in elements:
        if "border" in element.attrib.keys():
            element.attrib.pop("border")
        if "width" in element.attrib.keys():
            element.attrib.pop("width")
        if "align" in element.attrib.keys():
            element.attrib.pop("align")
        if "cellpadding" in element.attrib.keys():
            element.attrib.pop("cellpadding")
        if "cellspacing" in element.attrib.keys():
            element.attrib.pop("cellspacing")

    elements = descr.xpath("//ul/br")
    for element in elements:
        element.getparent().remove(element)

    elements = descr.xpath("//input")
    for element in elements:
        if "border" in element.attrib.keys():
            element.attrib.pop("border")

    elements = descr.xpath("//td")
    for element in elements:
        if "width" in element.attrib.keys():
            element.attrib.pop("width")

    return descr

def GetTariffNameFromId(tariffId):
    sql = """SELECT name FROM sdek_tariffs_codes
             WHERE code = {0}""".format(tariffId)
    try:
        return MakeLocalDbGetInfoQueue(sql)[0][0]
    except:
        return -1

def GetYandexLoginData(site):
    if site == "http://safetus.ru":
        return ["pitekantrop5-safetus", "3edc$RFV"]

    if site == "http://knowall.ru.com":
        return ["pitekantrop-knowall", "3edc$RFV"]

    if site == "http://otpugiwatel.com":
        return ["pitekantrop5", "2wsx#EDC"]

    if site == "http://tomsk.otpugivatel.com":
        return ["pitekantrop-otpugivatel", "1qaz@WSX"]

    if site == "http://amazing-things.ru":
        return ["pitekantrop-amth", "2wsx#EDC"]

    if site == "http://saltlamp.su":
        return ["feedback@saltlamp.su","Tabulfsam777"]

    if site == "http://insect-killers.ru":
        return ["feedback@insect-killers.ru", "Tabulfsam777"]

    if site == "http://sushilki.ru.com":
        return ["feedback@sushilki.ru.com", "Tabulfsam777"]

    if site == "http://glushilki.ru.com":
        return ["feedback@glushilki.ru.com", "Tabulfsam777"]

    if site == "http://mini-camera.ru.com":
        return ["feedback@mini-camera.ru.com", "Tabulfsam777"]

    if site == "http://otpugivateli-grizunov.ru":
        return ["feedback@otpugivateli-grizunov.ru", "Tabulfsam777"]

    if site == "http://otpugivateli-krotov.ru":
        return ["feedback@otpugivateli-krotov.ru", "Tabulfsam777"]

    if site == "http://otpugivateli-ptic.ru":
        return ["feedback@otpugivateli-ptic.ru", "Tabulfsam777"]

    if site == "http://otpugivateli-sobak.ru":
        return ["feedback@otpugivateli-sobak.ru", "Tabulfsam777"]

def GetGoogleLoginData(site):
    if site in ["http://sushilki.ru.com"]:
        return ["knowall.ru.com@gmail.com", "19842001"]
    elif site in ["http://otpugivateli-grizunov.ru"]:
        return ["otpugivateligrizunov@gmail.com", "19842001"]
    elif site in ["http://otpugiwatel.com", "http://safetus.ru", "http://saltlamp.su"]:
        return ["feedback@knowall.ru.com", "Tabulfsam"]
    elif site in ["http://otpugivateli-ptic.ru"]:
        return ["otpugivateliptic@gmail.com", "19842001"]
    elif site in ["http://insect-killers.ru"]:
        return ["insectkillersru@gmail.com", "19842001"]
    elif site in ["http://otpugivateli-sobak.ru", "http://mini-camera.ru.com"]:
        return ["otpugivatelisobak@gmail.com", "19842001"]
    elif site in ["http://glushilki.ru.com", "http://otpugivateli-krotov.ru"]:
        return ["glushilkirucom@gmail.com", "19842001"]
    else:
        return ["pitekantrop55@gmail.com", "Tabulfsam555"]

def GetSubfoldersSites():
    return ["http://glushilki.ru.com",
    "http://otpugivateli-ptic.ru",
    "http://otpugivateli-krotov.ru",
    "http://otpugivateli-sobak.ru",
    "http://mini-camera.ru.com",
    "http://sushilki.ru.com",
    "http://saltlamp.su",
    "http://safetus.ru",
    "http://incubators.shop",
    "http://usiliteli-svyazi.ru",
    "http://otpugiwatel.com",
    "http://insect-killers.ru",
    "http://gps-tracker.su"
    ]

def GetVariantFromTemplate(shopName, template):
    shopName = GetShopName(shopName)
    tmp_template = template
    if tmp_template.count("{") != tmp_template.count("}") or tmp_template.count("[") != tmp_template.count("]"):
        return False

    declensions = GetCityDeclension(shopName)
    columns = "city_I,city_R,city_D,city_V,city_T,city_P,city_pr_s_I,city_pr_s_R,city_pr_s_D,city_pr_s_V,city_pr_s_T,city_pr_s_P,city_pr_m_I,city_pr_m_R,city_pr_m_D,city_pr_m_V,city_pr_m_T,city_pr_m_P".split(",")
    for column in columns:
        tmp_template = tmp_template.replace("[{0}]".format(column), declensions[columns.index(column)])

    p = re.compile('\{([^\}\{]+)\}')
    while tmp_template.find("{") != -1:
        variants = p.findall(tmp_template)
        for variant in variants:
            phrase = random.choice(variant.split("|"))
            tmp_template = tmp_template.replace(u"{" + variant + "}", phrase)

    p = re.compile('\[([^\]\[]+)\]')
    while tmp_template.find("[") != -1:
        variants = p.findall(tmp_template)
        for variant in variants:
            array = variant.split("|")
            random.shuffle(array)
            phrase = u"".join(array)
            tmp_template = tmp_template.replace("[" + variant + "]", phrase)

    tmp_template = tmp_template.replace("  ", " ")
    return tmp_template

def GetPuttyParams(site):
    return [puttyPath, '-v', '-ssh', '-2', '-P', '22', '-C', '-l', 'root', '-L', '5555:127.0.0.1:3306', site["ip"]]

def GetCitiesTable():
    sql = """SELECT * FROM cities"""
    return MakeLocalDbGetInfoQueue(sql)

def MakeLocalDbUpdateQueue(sql):
    prestashop_api = WebasystAPI.WebasystAPI()
    prestashop_api.MakeLocalDbUpdateQueue(sql)

def MakeLocalDbGetInfoQueue(sql):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.MakeLocalDbGetInfoQueue(sql)

def ReplaceCityInText(city1, city2, text):
    if text == None:
        return ""

    city1Dec = GetCityDeclension(city1)
    city2Dec = GetCityDeclension(city2)
    for i in range (len(city1Dec) -1 , -1 ,-1 ):
        text = text.replace(city1Dec[i], city2Dec[i])

    return text

def GetShopMainDomain(url):
    if url.find(".ru.com") != -1:
        return url.split(".")[-3] + ".ru.com"
    else:
        parts = url.split(".")
        return parts[-2] + "." + parts[-1]

def GetSingleForm(name):
    uppercase = lambda s: s[:1].upper() + s[1:] if s else ''
    array = []
    array.append([u"карманные подавители (глушилки)", u"карманный подавитель (глушилку)"])
    array.append([u"мобильные подавители (глушилки)", u"мобильный подавитель (глушилку)"])
    array.append([u"комплекты усилителей связи", u"комплект усилителя связи"])
    array.append([u"стационарные глушилки", u"стационарную глушилку"])
    array.append([u"карманные отпугиватели собак", u"карманный отпугиватель собак"])
    array.append([u"карманные миникамеры", u"карманную миникамеру"])
    array.append([u"комплекты gsm-сигнализации", u"комплект gsm-сигнализации"])
    array.append([u"беспроводные домофоны", u"беспроводной домофон"])
    array.append([u"беспроводные аудиодомофоны", u"беспроводной аудиодомофон"])
    array.append([u"беспроводные видеоглазки", u"беспроводной видеоглазок"])
    array.append([u"уличные уничтожители насекомых", u"уличный уничтожитель насекомых"])
    array.append([u"отпугиватели", u"отпугиватель"])
    array.append([u"промышленные инкубаторы", u"промышленный инкубатор"])
    array.append([u"вызывные панели видеодомофонов", u"вызывную панель видеодомофона"])
    array.append([u"ловушки", u"ловушку"])
    array.append([u"глушилки", u"глушилку"])
    array.append([u"подавители", u"подавитель"])
    array.append([u"уничтожители", u"уничтожитель"])
    array.append([u"ультразвуковые", u"ультразвуковой"])
    array.append([u"лазерные", u"лазерный"])
    array.append([u"универсальные", u"универсальный"])
    array.append([u"антилаи", u"антилай"])
    array.append([u"визуальные", u"визуальный"])
    array.append([u"ошейники", u"ошейник"])
    array.append([u"стационарные", u"стационарный"])
    array.append([u"биоакустические", u"биоакустический"])
    array.append([u"громпушки", u"громпушку"])
    array.append([u"пропановые", u"пропановую"])
    array.append([u"пушки", u"пушку"])
    array.append([u"промышленные", u"промышленную"])
    array.append([u"бытовые", u"бытовую"])
    array.append([u"уличные", u"уличную"])
    array.append([u"электрические", u"электрическую"])
    array.append([u"клеевые", u"клеевую"])
    array.append([u"видеоглазки", u"видеоглазок"])
    array.append([u"домофоны", u"домофон"])
    array.append([u"видеодомофоны", u"видеодомофон"])
    array.append([u"системы", u"систему"])
    array.append([u"сигнализации", u"сигнализацию"])
    array.append([u"изменители", u"изменитель"])
    array.append([u"усилители", u"усилитель"])
    array.append([u"репитеры", u"репитер"])
    array.append([u"алкотестеры", u"алкотестер"])
    array.append([u"парктроники", u"парктроник"])
    array.append([u"ножеточки", u"ножеточку"])
    array.append([u"эндоскопы", u"эндоскоп"])
    array.append([u"инкубаторы", u"инкубатор"])
    array.append([u"индикаторы", u"индикатор"])
    array.append([u"детекторы", u"детектор"])
    array.append([u"камеры", u"камеру"])
    array.append([u"корпусные", u"корпусную"])
    array.append([u"поворотные", u"поворотную"])
    array.append([u"миниатюрные", u"миниатюрную"])
    array.append([u"купольные", u"купольную"])
    array.append([u"видеорегистраторы", u"видеорегистратор"])
    array.append([u"гибридные", u"гибридный"])
    array.append([u"цифровые", u"цифровой"])
    array.append([u"диктофоны", u"диктофон"])
    array.append([u"минидиктофоны", u"минидиктофон"])
    array.append([u"подарочные", u"подарочный"])
    array.append([u"цветные", u"цветной"])
    array.append([u"электронные", u"электронный"])
    array.append([u"дверные", u"дверной"])
    array.append([u"антижучки", u"антижучок"])
    array.append([u"блокираторы", u"блокиратор"])
    array.append([u"автоматические", u"автоматический"])
    array.append([u"обнаружители", u"обнаружитель"])
    array.append([u"охранные", u"охранную"])
    array.append([u"глазки", u"глазок"])
    array.append([u"беспроводные", u"беспроводную"])
    array.append([u"сушилки", u"сушилку"])
    array.append([u"электросушилки", u"электросушилку"])
    array.append([u"мобильные", u"мобильную"])
    array.append([u"аналоговые", u"аналоговую"])
    array.append([u"широкополосные", u"широкополосный"])
    array.append([u"солевые", u"солевую"])
    array.append([u"лампы", u"лампу"])
    array.append([u"соляные", u"соляную"])
    array.append([u"гималайские", u"гималайскую"])
    array.append([u"белые", u"белую"])
    array.append([u"шпионские", u"шпионскую"])
    array.append([u"маленькие", u"маленькую"])
    array.append([u"гусиные", u"гусиный"])
    array.append([u"утиные", u"утиный"])
    array.append([u"куриные", u"куриный"])
    array.append([u"перепелиные", u"перепелиный"])
    array.append([u"китайские", u"китайский"])
    array.append([u"звуковые", u"звуковой"])
    array.append([u"скрытые", u"скрытую"])
    array.append([u"дозиметры", u"дозиметр"])
    array.append([u"нитратомеры", u"нитратомер"])
    array.append([u"радиометры", u"радиометр"])
    array.append([u"вакуумные упаковщики", u"вакуумный упаковщик"])
    array.append([u"вафельницы", u"вафельницу"])
    array.append([u"вспениватели", u"вспениватель"])
    array.append([u"слайсеры (ломтерезки)", u"слайсер (ломтерезку)"])
    array.append([u"кухонные измельчители", u"кухонный измельчитель"])
    array.append([u"конвекционные печи", u"конвекционную печь"])
    array.append([u"кофемолки", u"кофемолку"])
    array.append([u"льдогенераторы", u"льдогенератор"])
    array.append([u"микроволновые печи", u"микроволновую печь"])
    array.append([u"миксеры", u"миксер"])
    array.append([u"холодильные шкафы", u"холодильный шкаф"])
    array.append([u"гомогенизаторы", u"гомогенизатор"])
    array.append([u"блендеры", u"блендер"])
    array.append([u"средства", u"средство"])
    array.append([u"погружные", u"погружной"])
    array.append([u"кипятильники, термопоты", u"кипятильник, термопот"])
    array.append([u"фритюрницы", u"фритюрницу"])
    array.append([u"печи для пиццы", u"печь для пиццы"])
    array.append([u"спиральные тестомесы", u"спиральный тестомес"])
    array.append([u"индукционные плиты", u"индукционную плиту"])
    array.append([u"капучинаторы", u"капучинатор"])
    array.append([u"кухонная посуда", u"кухонную посуду"])
    array.append([u"инсектицидные лампы", u"инсектицидную лампу"])
    array.append([u"карманные", u"карманный"])
    array.append([u"ручные", u"ручной"])
    array.append([u"инсектицидные", u"инсектицидную"])
    array.append([u"антимоскитные", u"антимоскитную"])
    array.append([u"клеевые", u"клеевую"])
    array.append([u"фотоловушки", u"фотоловушку"])
    array.append([u"антижучки", u"антижучок"])
    array.append([u"электромеханические замки", u"электромеханический замок"])
    array.append([u"электромагнитные замки", u"электромагнитный замок"])
    array.append([u"вызывные панели", u"вызывную панель"])
    array.append([u"розетки", u"розетку"])
    array.append([u"браслеты", u"браслет"])

    for entry in array:
        name = name.replace(entry[0], entry[1])
        name = name.replace(uppercase(entry[0]), uppercase(entry[1]))
    return name

def Dowmcase(text):
    downcase = lambda s: s[:1].lower() + s[1:] if s else ''
    bDowncase = True
    words = [u"GSM", u"P2P", u"MМS", u"IP", u"Wi"]
    for word in words:
        if text.startswith(word):
            bDowncase = False
            break
    if bDowncase:
        text = downcase(text)
    return text

def Uppercase(text):
    uppercase = lambda s: s[:1].upper() + s[1:] if s else ''
    return uppercase(text)

def GetLinkRewriteFromName(name, bTransliterate = True):
    link = name
    if bTransliterate:
        link = Transliterate(name)
    chars = ["(","?", ")","+",",",".","/","\\", u"«", u"»", u"\"", u":", u"%", u"&", u"@", u"–"]
    for char in chars:
        link = link.replace(char, "")
    link = link.replace(" ", "-").lower()
    return link

def GetCompanyName(siteUrl):
    if siteUrl == "http://safetus.ru":
        return u"Safetus"
    if siteUrl == "http://otpugiwatel.com":
        return u"Отпугиватель.Com"
    if siteUrl == "http://otpugivatel.com":
        return u"Отпугиватель.ком"
    if siteUrl == "http://saltlamp.su":
        return u"SaltLamp.Su"
    if siteUrl == "http://insect-killers.ru":
        return u"Insect Killers"
    if siteUrl == "http://sushilki.ru.com":
        return u"Сушилки"
    if siteUrl == "http://glushilki.ru.com":
        return u"Глушилки"
    if siteUrl == "http://incubators.shop":
        return u"Incubators"
    if siteUrl == "http://mini-camera.ru.com":
        return u"Мини камера"
    if siteUrl == "http://otpugivateli-grizunov.ru":
        return u"Отпугиватели грызунов"
    if siteUrl == "http://otpugivateli-krotov.ru":
        return u"Отпугиватели кротов"
    if siteUrl == "http://otpugivateli-ptic.ru":
        return u"Отпугиватели птиц"
    if siteUrl == "http://otpugivateli-sobak.ru":
        return u"Отпугиватели собак"
    if siteUrl == "http://knowall.ru.com":
        return u"Новалл"
    if siteUrl == "http://usiliteli-svyazi.ru":
        return u"Усилители связи"
    if siteUrl == "http://gemlux-shop.ru":
        return u"Gemlux-shop.ru"
    if siteUrl == "http://gastrorag-shop.ru":
        return u"Gastrorag-shop.ru"
    if siteUrl == "http://otpugivatel.spb.ru":
        return u"Отпугиватель.spb.ru"
    if siteUrl == "http://rose-of-bulgaria.ru":
        return u"Rose-of-Bulgaria.ru"

def GetSdekTariffId(tariff):
    prestashop_api = WebasystAPI.WebasystAPI()
    sql = u"""SELECT code FROM sdek_tariffs_codes
              WHERE name = '{0}'""".format(tariff)
    try:
        return prestashop_api.MakeLocalDbGetInfoQueue(sql)[0][0]
    except:
        return -1

def GetCityPvz(city):
    prestashop_api = WebasystAPI.WebasystAPI()
    sql = u"""SELECT * FROM sdek_pvz
              WHERE cityName = '{0}'""".format(city)
    return prestashop_api.MakeLocalDbGetInfoQueue(sql)

def GetCityPvzByCode(Code):
    prestashop_api = WebasystAPI.WebasystAPI()
    sql = u"""SELECT * FROM sdek_pvz
              WHERE cityCode = '{0}'""".format(Code)
    return prestashop_api.MakeLocalDbGetInfoQueue(sql)

def GetCitiesWithPoints():
    prestashop_api = WebasystAPI.WebasystAPI()
    sql = u"""SELECT distinct cityCode FROM sdek_pvz"""
    return [i[0] for i in prestashop_api.MakeLocalDbGetInfoQueue(sql)]

def GetCityRegion(cityName):
    prestashop_api = WebasystAPI.WebasystAPI()
    sql = u"""SELECT RegionName FROM sdek_pvz
              WHERE cityName = '{}'""".format(cityName)
    return prestashop_api.MakeLocalDbGetInfoQueue(sql)[0][0]

def GetIntPriceFromText(price):
    price = price.strip()
    chars = [u"₽", " ", u"&nbsp;", u" руб.", u" руб", u"\xa0", u"руб.", u"руб", u"р.",".00",".0"]
    for char in chars:
        price = price.replace(char, "")

    try:
        price = str(price).replace(",",".")
    except:
        try:
            price = str(price.encode("cp1251").replace("\xa0", "").decode("cp1251")).replace(".0","").replace(",",".")
        except:
            pass

    return int(float(price))

def TransliterateToEn(string):
    capital_letters = {u'А': u'A',
                   u'Б': u'B',
                   u'В': u'V',
                   u'Г': u'G',
                   u'Д': u'D',
                   u'Е': u'E',
                   u'Ё': u'E',
                   u'З': u'Z',
                   u'И': u'I',
                   u'Й': u'Y',
                   u'К': u'K',
                   u'Л': u'L',
                   u'М': u'M',
                   u'Н': u'N',
                   u'О': u'O',
                   u'П': u'P',
                   u'Р': u'R',
                   u'С': u'S',
                   u'Т': u'T',
                   u'У': u'U',
                   u'Ф': u'F',
                   u'Х': u'H',
                   u'Ы': u'Y',
                   u'Э': u'E',}

    capital_letters_transliterated_to_multiple_letters = {u'Ж': u'Zh',
                                                          u'Ц': u'Ts',
                                                          u'Ч': u'Ch',
                                                          u'Ш': u'Sh',
                                                          u'Щ': u'Sch',
                                                          u'Ю': u'Yu',
                                                          u'Я': u'Ya',}


    lower_case_letters = {u'а': u'a',
                       u'б': u'b',
                       u'в': u'v',
                       u'г': u'g',
                       u'д': u'd',
                       u'е': u'e',
                       u'ё': u'e',
                       u'ж': u'zh',
                       u'з': u'z',
                       u'и': u'i',
                       u'й': u'y',
                       u'к': u'k',
                       u'л': u'l',
                       u'м': u'm',
                       u'н': u'n',
                       u'о': u'o',
                       u'п': u'p',
                       u'р': u'r',
                       u'с': u's',
                       u'т': u't',
                       u'у': u'u',
                       u'ф': u'f',
                       u'х': u'h',
                       u'ц': u'ts',
                       u'ч': u'ch',
                       u'ш': u'sh',
                       u'щ': u'sch',
                       u'ы': u'y',
                       u'э': u'e',
                       u'ю': u'yu',
                       u'я': u'ya'}

    for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
        string = re.sub(ur"%s([a-z])" % latin_string, ur'%s\1' % cyrillic_string, string)

    for dictionary in (capital_letters, lower_case_letters):
        for cyrillic_string, latin_string in dictionary.iteritems():
            string = string.replace(latin_string, cyrillic_string)

    for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
        string = string.replace(latin_string, cyrillic_string )

    return string

def GetDiscount(note):
    rows = note.split("\n")
    for row in rows:
        if row.find(u"discount") != -1:
            p = re.compile('\d+')
            discount = p.findall(row)[0]
            return float(discount)

    return 0

def GetRate(currency = "euro"):
    if currency == "euro":
        id = 'R01239'
    if currency == "dollar":
        id = 'R01235'
    try:
        url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req={0}".format((date.today() + datetime.timedelta(days=0)).strftime('%d.%m.%Y'))
        resp = requests.get(url)
        tree = etree.XML(resp.text.replace("""<?xml version="1.0" encoding="windows-1251"?>""", ""))
        value = tree.xpath("//ValCurs//Valute[@ID = '{0}']/Value".format(id))[0]
        return float(value.text.replace(",","."))
    except Exception as e:
        print e.message.args[1]
        return -1

def DeleteQuotes(text):
    text = text.replace(u"«","").replace(u"»","").replace(u"\"","")
    return text

def ChangeProductName(oldName, name):
    article = CleanItemName(oldName)
    newName = name + u" \"{0}\"".format(article)
    return newName

def GetProductName(name):
    if name.find("\"") != -1:
        return name.split("\"")[0].strip()
    if name.find(u"«") != -1:
        return name.split(u"«")[0].strip()


def CreateSshSession(ip):
    ssh_ = paramiko.SSHClient()
    ssh_.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_.connect(ip, port=11111, username=sshLogin, password=sshPassword)
    sftp = ssh_.open_sftp()
    return [ssh_, sftp]

def CreateFtpSession(ip):
    ftp = ftplib.FTP(ip, "wa_ftp", "Tabulfsam555")
    return ftp

def GetTextFromHtml(text):
    text = html2text.html2text (text)
    for char in ["###", "**", "[", "]", "\\"]:
        text = text.replace(char, "")
    return text

def GetWaSiteParams(url):
    params = [x for x in wa_sitesParams if x["url"] == url][0]
    return params

def GetSiteParams(url):
    params = [x for x in sitesParams if x["url"] == url][0]
    return params

def GetReindexQuota(siteUrl):
    if siteUrl == "http://safetus.ru":
        return 100
    if siteUrl == "http://otpugiwatel.com":
        return 90
    if siteUrl == "http://tomsk.otpugivatel.com":
        return 150
    if siteUrl == "http://saltlamp.su":
        return 20
    if siteUrl == "http://insect-killers.ru":
        return 110
    if siteUrl == "http://sushilki.ru.com":
        return 20
    if siteUrl == "http://glushilki.ru.com":
        return 110
    if siteUrl == "http://incubators.shop":
        return 20
    if siteUrl == "http://mini-camera.ru.com":
        return 110
    if siteUrl == "http://otpugivateli-grizunov.ru":
        return 90
    if siteUrl == "http://otpugivateli-krotov.ru":
        return 20
    if siteUrl == "http://otpugivateli-ptic.ru":
        return 30
    if siteUrl == "http://otpugivateli-sobak.ru":
        return 30
    if siteUrl == "http://knowall.ru.com":
        return 150
    if siteUrl == "http://usiliteli-svyazi.ru":
        return 20

def CreateWebasystSession(url, logFilePath = None):
    siteParams = GetWaSiteParams(url)
    wa_api = GetWebasystInstance(siteParams, logFilePath)
    return wa_api

def CreatePrestashopSession(url, logFilePath = None):
    siteParams = GetSiteParams(url)
    prestashop_api = False
    for i in range(5):
##        ssh = subprocess.Popen(GetPuttyParams(siteParams))
        prestashop_api = GetPrestashopInstance(siteParams, logFilePath)
        if prestashop_api == False:
            time.sleep(0.5)
        else:
            return prestashop_api
    if prestashop_api == False:
        return -1

def GetContentmonsterCategoriesTexts(source, clearHtml = True):
    titleTag = "h2"
    texts = []
    tree = etree.HTML(source)
    titles = tree.xpath("//{0}".format(titleTag))

    for title in titles:
        topText = u""
        bottomText = u""
        text = u""
        title = title.getnext()
        bContinue = True
        while bContinue:
            if topText == u"":
                topText = GetStringDescription(title)
            else:
                bottomText += GetStringDescription(title)
            title = title.getnext()
            if title == None or title.tag == titleTag:
                bContinue = False

        if clearHtml:
            text = GetTextFromHtml(text)
        else:
            text = UnescapeText(text)

        texts.append([topText, bottomText])

    return texts

def GetContentmonsterProductsTexts(source):
    titleTag = "h1"
    texts = []
    tree = etree.HTML(source)
    titles = tree.xpath("//{0}".format(titleTag))

    for title in titles:
        descr = u""
        title = title.getnext()
        bContinue = True
        while bContinue:
            descr += GetStringDescription(title)
            title = title.getnext()
            if title == None or title.tag == titleTag:
                bContinue = False

        texts.append(descr)

    return texts

def GetPvzTable():
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetSdekPvzTable()

def GetCitySdekPvzList(pvzTable, city):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetCitySdekPvzList(pvzTable, city)

def GetCityFromCode(code):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetCityFromCode(code)

def GetCityFromId(code):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetCityFromId(code)

def GetPvzCityCode(code):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetPvzCityCode(code)

def GetPvzByCode(code):
    prestashop_api = WebasystAPI.WebasystAPI()
    return prestashop_api.GetPvzByCode(code)

def DeleteCommentsFromHtml(string):
    if string.find("<!--") != -1:
        while string.find("<!--") != -1:
            ind1 = string.find("<!--")
            ind2 = string.find("-->") + 3
            string = string.replace(string[ind1:ind2],"")

    return string

def ReplaceBadChars(text):
    chars = [u"\xa0", u'\xb2', u'\ufeff']
    for char in chars:
        text = text.replace(char, u"")
    return text.strip()

def MakeH3Titles(productPageContent):
    titles_ = [u"Особенности", u"Область",  u"Характеристики", u"Комплектация", u"Технические", u"Принцип", u"Целевая аудитория"  ]
    for title_ in titles_:
        elems = productPageContent.xpath(u"//*[starts-with(text(), '{}')]".format(title_))
        if len(elems) > 0:
            elem = elems[-1]
            elem.tag = "h3"
            if 'style' in elem.attrib.keys():
                elem.attrib.pop('style')

    return productPageContent

def iriToUri(iri):
    parts= urlparse.urlparse(iri)
    return urlparse.urlunparse(
        part.encode('idna') if parti==1 else urlEncodeNonAscii(part.encode('utf-8'))
        for parti, part in enumerate(parts)
    )

def GetPictureNameFromUrl(url):
    name = url.split("/")[-1]
    name = name.replace("." + name.split(".")[-1], "")
    ext = url.split(".")[-1]
    if ext == "" or len(ext) > 3:ext = "jpg"
    return u"{}.{}".format( name,ext )

def DownloadImage(originImagePath, destinationImagePath):
    picName = GetPictureNameFromUrl(originImagePath)
    if destinationImagePath != "":
        pic = u"{}\\{}".format( destinationImagePath, picName)
    else:
        pic = picName
    originImagePath = iriToUri(originImagePath)
    try:
##        urllib.urlretrieve(originImagePath, pic)
        response = requests.get(originImagePath, stream=True, verify=False)
        with open(pic, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
    except Exception as e:
        print str(e)
        return False
    return True

def GetContractorPrice(contractor):
    if contractor == "st":
        username = 'dealer'
        password = 'fLR7MRrI'
        priceName = "price.xls"

        url = 'http://www.sititek.ru/dealer/Price.xls'

        r = requests.get(url, auth=(username,password), verify=False)

        if r.status_code == 200:
            with open(priceName, 'wb') as out:
                for bits in r.iter_content():
                    out.write(bits)
            return priceName
        else:
            return -1

def DownloadFile(originPath, name = None, destinationPath = None ):
    if name == None:
        name = originPath.split("/")[-1]
    if destinationPath != None:
        if destinationPath[-1] != "\\":
            destinationPath += "\\"
        filePath = destinationPath + name
    else:
        filePath = name
    originPath = iriToUri(originPath)
    try:
        urllib.urlretrieve(originPath, filePath)
    except:
        return False

    return True

def urlEncodeNonAscii(b):
    return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

def SendSshCommandAndWait(ssh_, command):
    stdin, stdout, stderr = ssh_.exec_command(command)
    channel = stdout.channel
    status = channel.recv_exit_status()

def GetProductCategoriesWA(siteUrl, productName, contractorCategory):
    if contractorCategory in [u"Прочие устройства и комплектующие", u"Расходные материалы", u"Сопутствующие товары", u"Комплектующие"] or len([i for i in [u"ереходник", u"аксесс", u"ешний динамик", u"адаптер", u"ттрактан", u"тренога"] if productName.lower().find(i) != -1]) > 0:
        if siteUrl in ["http://otpugivateli-krotov.ru", "http://mini-camera.ru.com", "http://otpugivateli-ptic.ru","http://otpugivateli-grizunov.ru"]:
            return [95]
        if siteUrl in ["http://otpugivateli-sobak.ru"]:
            return [169]
        if siteUrl in ["http://insect-killers.ru"]:
            return [161]
        if siteUrl in ["http://otpugivatel.com"]:
            return [692]
        if siteUrl in ["http://otpugiwatel.com"]:
            return [284]
        if siteUrl in ["http://knowall.ru.com"]:
            return [349]
    ###########################################
    if contractorCategory in [u"Отпугиватели грызунов", u"Отпугиватели мышей, крыс"]:
        cats = []
        if productName.lower().find(u"насеком") != -1:
            if siteUrl == "http://otpugivateli-grizunov.ru":
                cats.append(203)
            if siteUrl == "otpugiwatel.com":
                cats.append(291)
            if siteUrl == "otpugivatel.com":
                cats.append(699)
            if siteUrl == "otpugivatel.spb.ru":
                cats.append(819)
        if siteUrl == "http://otpugivateli-grizunov.ru":
            cats += [187]
        if siteUrl == "http://otpugivatel.com":
            cats += [695, 694, 700]
        if siteUrl == "http://otpugivatel.spb.ru":
            cats += [821, 795, 700, 695, 694]
        if siteUrl == "http://otpugiwatel.com":
            cats += [289, 290, 292]
        if siteUrl == "http://knowall.ru.com":
            return [363]
        return cats
    ###########################################
    if contractorCategory == u"Отпугиватели клещей ультразвуковые":
        if siteUrl == "http://otpugiwatel.com":
            return [232, 230]
        if siteUrl == "http://otpugivatel.spb.ru":
            return [232, 230]
        if siteUrl == "http://otpugivatel.com":
            return [636, 634]
        if siteUrl == "http://knowall.ru.com":
            return [634]
    ###########################################
    if contractorCategory == u"Отпугиватели комаров":
        if siteUrl in ["http://insect-killers.ru"]:
            return [148]
        if siteUrl == "http://knowall.ru.com":
            return [364]
        if siteUrl == "http://otpugiwatel.com":
            return [213]
        if siteUrl == "http://otpugivatel.spb.ru":
            return [785]
        if siteUrl == "http://otpugivatel.com":
            return [617]
    ###########################################
    if contractorCategory in [u"Отпугиватели кротов и змей", u"Отпугиватели кротов", u"Отпугиватели змей"]:
        if productName.lower().find(u"кротов") == -1:
            if siteUrl == "http://otpugiwatel.com":
                return [209]
            if siteUrl == "http://otpugivatel.spb.ru":
                return [781]
            if siteUrl == "http://otpugivatel.com":
                return [613]
            if siteUrl == "http://otpugivateli-krotov.ru":
                return [379]
            if siteUrl == "http://otpugivateli-grizunov.ru":
                return [208]
        else:
            if siteUrl == "http://otpugiwatel.com":
                return [210]
            if siteUrl == "http://otpugivatel.spb.ru":
                return [782]
            if siteUrl == "http://otpugivatel.com":
                return [614]
            if siteUrl == "http://otpugivateli-krotov.ru":
                return [371, 380, 381]
            if siteUrl == "http://otpugivateli-grizunov.ru":
                return [205]
        if siteUrl == "http://knowall.ru.com":
            return [361]
    ###########################################
    if contractorCategory == u"Отпугиватели птиц":
        cats = []
        if len([i for i in [u"изуальный", u"шар", u"лента", u"светоотраж"] if productName.lower().find(i) != -1]) > 0:
            if siteUrl == "http://otpugiwatel.com":
                cats.append(236)
            if siteUrl == "http://otpugivatel.com":
                cats.append(642)
            if siteUrl in [ "http://otpugivateli-ptic.ru"]:
                cats.append(177)
            if siteUrl == "http://otpugivatel.spb.ru":
                cats.append(809)
        elif productName.lower().find(u"ультразвуковой") != -1:
            if siteUrl == "http://otpugiwatel.com":
                cats.append(236)
            if siteUrl == "http://otpugivatel.com":
                cats.append(641)
            if siteUrl == "http://otpugivateli-ptic.ru":
                cats.append(176)
            if siteUrl == "http://otpugivatel.spb.ru":
                cats.append(808)
        elif len([i for i in [u"звуковой", u"биоак"] if productName.lower().find(i) != -1]) > 0:
            if siteUrl == "http://otpugiwatel.com":
                cats.append(237)
            if siteUrl == "http://otpugivatel.com":
                cats.append(643)
            if siteUrl == "http://otpugivateli-ptic.ru":
                cats.append(178)
            if siteUrl == "http://otpugivatel.spb.ru":
                cats.append(810)
        elif len([i for i in [u"шипы", u"Барьер"] if productName.lower().find(i.lower()) != -1]) > 0:
            if siteUrl == "http://otpugiwatel.com":
                cats.append(234)
            if siteUrl == "http://otpugivatel.com":
                cats.append(640)
            if siteUrl == "http://otpugivateli-ptic.ru":
                cats.append(175)
            if siteUrl == "http://knowall.ru.com":
                cats.append(366)
            if siteUrl == "http://otpugivatel.spb.ru":
                cats.append(807)
        elif len([i for i in [u"гром", u"пушка"] if productName.lower().find(i) != -1]) > 0:
            if siteUrl == "http://otpugiwatel.com":
                cats.append(238)
            if siteUrl == "http://otpugivatel.com":
                cats.append(644)
            if siteUrl == "http://otpugivateli-ptic.ru":
                cats.append(179)
            if siteUrl == "http://otpugivatel.spb.ru":
                cats.append(811)
        if siteUrl == "http://otpugiwatel.com":
            cats.append(233)
        if siteUrl == "http://otpugivatel.com":
            cats.append(638)
        if siteUrl == "http://otpugivateli-ptic.ru":
            cats.append(174)
        if siteUrl == "http://knowall.ru.com":
            cats.append(365)
        if siteUrl == "http://otpugivatel.spb.ru":
            cats.append(806)
        return cats
    ###########################################
    if contractorCategory == u"Отпугиватели собак":
        cats = []
        if productName.lower().find(u"стационарн") != -1:
            if siteUrl == "http://otpugivateli-sobak.ru":
                cats += [166, 168]
            if siteUrl == "http://otpugivatel.com":
                cats += [625, 701]
            if siteUrl == "http://otpugiwatel.com":
                cats += [221, 293]
            if siteUrl == "http://otpugivatel.spb.ru":
                cats += [794, 820]
            if siteUrl == "http://knowall.ru.com":
                cats.append(360)
        else:
            if siteUrl == "http://otpugivateli-sobak.ru":
                cats.append(167)
            if siteUrl == "http://otpugivatel.com":
                cats.append(624)
            if siteUrl == "http://otpugiwatel.com":
                cats.append(220)
            if siteUrl == "http://otpugivatel.spb.ru":
                cats.append(793)
            if siteUrl == "http://knowall.ru.com":
                cats.append(359)
        if siteUrl == "http://otpugivateli-sobak.ru":
            cats.append(165)
        if siteUrl == "http://otpugivatel.com":
            cats.append(623)
        if siteUrl == "http://otpugiwatel.com":
            cats.append(219)
        if siteUrl == "http://otpugivatel.spb.ru":
            cats.append(792)
        if siteUrl == "http://knowall.ru.com":
            cats.append(358)
        return cats
    ###########################################
    if contractorCategory in [u"Истребители насекомых \"Баргузин\"", u"Пылевлагозащищенные", u"С высокой эффективностью", u"Уничтожители комаров"]:
        if siteUrl == "http://insect-killers.ru":
            return [149, 154]
        if siteUrl == "http://otpugivatel.com":
            return [618, 620]
        if siteUrl == "http://otpugiwatel.com":
            return [214, 216]
        if siteUrl == "http://otpugivatel.spb.ru":
            return [786, 787]
        if siteUrl == "http://knowall.ru.com":
            return [364]
    ###########################################
    if contractorCategory in [u"Уничтожители летающих насекомых"]:
        if siteUrl == "http://insect-killers.ru":
            return [147]
        if siteUrl == "http://otpugivatel.com":
            return [618]
        if siteUrl == "http://otpugiwatel.com":
            return [214]
        if siteUrl == "http://otpugivatel.spb.ru":
            return [786]
        if siteUrl == "http://knowall.ru.com":
            return [364]
    ###########################################
    if contractorCategory == u"Алкотестеры":
        if siteUrl == "http://knowall.ru.com":
            return [343]
    ###########################################
    if contractorCategory == u"Видеоаппаратура (видеонаблюдение)":
        if siteUrl == "http://knowall.ru.com":
            return [376]
        if siteUrl == "http://safetus.ru":
            return [240]
    ###########################################
    if contractorCategory in [u"Видеодомофоны и видеоглазки", u"Видеоглазки и видеодомофоны"]:
        if productName.lower().find(u"идеод") != -1 and productName.lower().find(u"беспро") != -1:
            if siteUrl == "http://knowall.ru.com":
                return [303]
            if siteUrl == "http://safetus.ru":
                return [234]
        else:
            if siteUrl == "http://knowall.ru.com":
                return [304]
            if siteUrl == "http://safetus.ru":
                return [231]
            if siteUrl == "http://mini-camera.ru.com":
                return [98]
    ###########################################
    if contractorCategory == u"Дозиметры":
        if siteUrl == "http://knowall.ru.com":
            return [347]
    ###########################################
    if contractorCategory == u"Индикаторы поля":
        if siteUrl == "http://knowall.ru.com":
            return [587, 584, 585, 586]
        if siteUrl == "http://safetus.ru":
            return [213]
        if siteUrl == "http://glushilki.ru.com":
            return [759, 758, 755]
    ###########################################
    if contractorCategory == u"Инкубаторы автоматические":
        if productName.lower().find(u"лотков") != -1:
            if siteUrl == "http://knowall.ru.com":
                return [351]
            if siteUrl == "http://incubators.shop":
                return [727]
        else:
            if siteUrl == "http://knowall.ru.com":
                return [351]
            if siteUrl == "http://incubators.shop":
                return [724, 722]
    ###########################################
    if contractorCategory == u"Ножеточки, ножи, ломтерезки":
        if siteUrl == "http://knowall.ru.com":
            return [348]
    ###########################################
    if contractorCategory == u"Обнаружители скрытых видеокамер":
        if siteUrl == "http://knowall.ru.com":
            return [373]
        if siteUrl == "http://safetus.ru":
            return [214]
        if siteUrl == "http://glushilki.ru.com":
            return [755]
    ###########################################
    if contractorCategory == u"Планетарии":
        if siteUrl == "http://knowall.ru.com":
            return [434]
    ###########################################
    if contractorCategory == u"Подавители микрофонов (диктофонов)":
        if len([i for i in [u"латч для подави", u"ешняя ультразвуко"] if productName.lower().find(i) != -1]) > 0:
            if siteUrl == "http://knowall.ru.com":
                return [346]
            if siteUrl == "http://safetus.ru":
                return [222]
            if siteUrl == "http://glushilki.ru.com":
                return [727]
        else:
            if siteUrl == "http://knowall.ru.com":
                return [370]
            if siteUrl == "http://safetus.ru":
                return [210]
            if siteUrl == "http://glushilki.ru.com":
                return [754]
    ###########################################
    if contractorCategory == u"Подавители сотовых телефонов":
        if siteUrl == "http://knowall.ru.com":
            return [372]
        if siteUrl == "http://safetus.ru":
            return [211]
        if siteUrl == "http://glushilki.ru.com":
            return [753]
    ###########################################
    if contractorCategory == u"Видеонаблюдение_Камеры с записью на SD карту".lower():
        if siteUrl == "http://knowall.ru.com":
            return [417]
        if siteUrl == "http://safetus.ru":
            return [302]
        if productName.lower().find(u"wi") == -1:
            if siteUrl == "http://mini-camera.ru.com":
                return [91, 90]
        else:
            if siteUrl == "http://mini-camera.ru.com":
                return [93, 90]
    ###########################################
    if contractorCategory == u"Видеонаблюдение_Аналоговые беспроводные камеры".lower():
        if siteUrl == "http://knowall.ru.com":
            return [389]
        if siteUrl == "http://safetus.ru":
            return [300]
        if siteUrl == "http://mini-camera.ru.com":
            return [92]
    ###########################################
    if contractorCategory == u"Домофония_Беспроводные видеодомофоны".lower():
        if siteUrl == "http://knowall.ru.com":
            return [312]
        if siteUrl == "http://safetus.ru":
            return [234]
    ###########################################
    if contractorCategory == u"Домофония_Беспроводные аудиодомофоны".lower():
        if siteUrl == "http://knowall.ru.com":
            return [312]
        if siteUrl == "http://safetus.ru":
            return [238]
    ###########################################
    if contractorCategory == u"Домофония_Видеоглазки".lower():
        if siteUrl == "http://knowall.ru.com":
            return [297]
        if siteUrl == "http://safetus.ru":
            return [235]
        if siteUrl == "http://mini-camera.ru.com":
            return [98]
    ###########################################
    if contractorCategory == u"домофония_ip вызывные панели":
        if siteUrl == "http://knowall.ru.com":
            return [311]
        if siteUrl == "http://safetus.ru":
            return [237]
    ###########################################
    if contractorCategory == u"Домофония_Электромеханические замки".lower():
        if siteUrl == "http://knowall.ru.com":
            return [310]
        if siteUrl == "http://safetus.ru":
            return [230]
    ###########################################
    if contractorCategory == u"Биометрические и электромагнитные замки".lower():
        if siteUrl == "http://knowall.ru.com":
            return [708]
        if siteUrl == "http://safetus.ru":
            return [657]
    ###########################################
    if contractorCategory in [u"GSM охрана_GSM сигнализации".lower(), u"GSM охрана_3G камеры и видеосигализации".lower()]:
        if siteUrl == "http://knowall.ru.com":
            return [317]
        if siteUrl == "http://safetus.ru":
            return [227]
    ###########################################
    if contractorCategory in [u"GSM охрана_MMS камеры и фотоловушки".lower()]:
        if productName.lower().find(u"камера") != -1:
            if siteUrl == "http://knowall.ru.com":
                return [422]
            if siteUrl == "http://safetus.ru":
                return [246]
        else:
            if siteUrl == "http://knowall.ru.com":
                return [709]
            if siteUrl == "http://safetus.ru":
                return [246]
            if siteUrl == "http://mini-camera.ru.com":
                return [97]
    ###########################################
    if contractorCategory == u"GSM охрана_Локальная охрана".lower():
        if siteUrl == "http://knowall.ru.com":
            return [319]
        if siteUrl == "http://safetus.ru":
            return [228]
    ###########################################
    if contractorCategory == u"видеонаблюдение_диктофоны":
        if siteUrl == "http://knowall.ru.com":
            return [425]
        if siteUrl == "http://safetus.ru":
            return [250]
    ###########################################
    if contractorCategory == u"GSM охрана_GSM розетки".lower():
        if siteUrl == "http://knowall.ru.com":
            return [344]
        if siteUrl == "http://safetus.ru":
            return [221]
    ###########################################
    if contractorCategory == u"Антишпионское оборудование_Антижучки".lower():
        if siteUrl == "http://knowall.ru.com":
            return [368, 584, 585]
        if siteUrl == "http://safetus.ru":
            return [213]
        if siteUrl == "http://glushilki.ru.com":
            return [755]
    ###########################################
    if contractorCategory == u"Антишпионское оборудование_Обнаружители камер".lower():
        if siteUrl == "http://knowall.ru.com":
            return [373, 588]
        if siteUrl == "http://safetus.ru":
            return [214]
        if siteUrl == "http://glushilki.ru.com":
            return [755]
    ###########################################
    if contractorCategory in [u"Подавители связи_Мультичастотные подавители".lower(), u"Подавители связи_Стационарные подавители".lower()]:
        if siteUrl == "http://knowall.ru.com":
            return [369, 596, 598]
        if siteUrl == "http://safetus.ru":
            return [212]
        if siteUrl == "http://glushilki.ru.com":
            return [752]
    ###########################################
    if contractorCategory in [u"Подавители связи_Мобильные подавители".lower(), u"Подавители связи_Антишансон - подавитель радио".lower()]:
        if siteUrl == "http://knowall.ru.com":
            return [372, 602, 607]
        if siteUrl == "http://safetus.ru":
            return [211]
        if siteUrl == "http://glushilki.ru.com":
            return [753]
    ###########################################
    if contractorCategory == u"Подавители связи_Подавители диктофонов".lower():
        if siteUrl == "http://knowall.ru.com":
            return [370]
        if siteUrl == "http://safetus.ru":
            return [210]
        if siteUrl == "http://glushilki.ru.com":
            return [754]
    ###########################################
    if contractorCategory in [u"Подавители связи_Аккустические сейфы - ПРОИЗВОДСТВО ЛОГОС".lower(), u"Подавители связи_Прочее".lower()]:
        if siteUrl == "http://knowall.ru.com":
            return [374]
        if siteUrl == "http://safetus.ru":
            return [216]
        if siteUrl == "http://glushilki.ru.com":
            return [760]
    ###########################################
    if contractorCategory == u"GSM охрана_Дополнительные датчики".lower():
        if siteUrl == "http://knowall.ru.com":
            return [318]
        if siteUrl == "http://safetus.ru":
            return [226]
    ###########################################
    if contractorCategory in [u"Видеонаблюдение_Диктофоны".lower()]:
        if siteUrl == "http://knowall.ru.com":
            return [425]
        if siteUrl == "http://safetus.ru":
            return [250]
    ###########################################
    if contractorCategory == u"домофония_проводные домофоны (аналог, ahd, full-hd, ip, мини)":
        if siteUrl == "http://knowall.ru.com":
            return [304]
        if siteUrl == "http://safetus.ru":
            return [231]
    ###########################################
    if contractorCategory in [u"Электроразрядные", u"Промышленные высоковольтные"]:
        if siteUrl == "http://insect-killers.ru":
            return [150, 154]
        if siteUrl == "http://otpugivatel.com":
            return [618, 620, 702]
        if siteUrl == "http://otpugiwatel.com":
            return [214, 216]
    ###########################################
    if contractorCategory in [u"Высокоэффективные клеевые", u"Клеевые", u"Интерьерные", u"Промышленные клеевые", u"Промышленные с клеевой основой", u"Декоративные с клеевой пластиной"]:
        if siteUrl == "http://insect-killers.ru":
            return [151, 154]
        if siteUrl == "http://otpugivatel.com":
            return [621, 618]
        if siteUrl == "http://otpugiwatel.com":
            return [217, 216, 214]
    ###########################################
    if contractorCategory in [u"Бытовые ловушки"]:
        if siteUrl == "http://insect-killers.ru":
            return [152]
        if siteUrl == "http://otpugivatel.com":
            return [619]
        if siteUrl == "http://otpugiwatel.com":
            return [215]
    ###########################################
    if contractorCategory in [u"Отпугиватели тараканов, муравьев, пауков, мух, клопов, блох, моли"]:
        if siteUrl in ["http://otpugivatel.com"]:
            return [634]
        if siteUrl in ["http://otpugiwatel.com"]:
            return [230]
        if siteUrl in ["http://knowall.ru.com"]:
            return [362]
    ###########################################
    if contractorCategory == u"Биоакустические приборы":
        if siteUrl == "http://otpugiwatel.com":
            return [237, 233]
        if siteUrl == "http://otpugivatel.com":
            return [643, 638]
        if siteUrl == "http://otpugivateli-ptic.ru":
            return [178, 174]
        if siteUrl == "http://otpugivatel.spb.ru":
            return [810, 806]
        if siteUrl in ["http://knowall.ru.com"]:
            return [614, 365]
    ###########################################
    if contractorCategory == u"Пропановые пушки":
        if siteUrl == "http://otpugiwatel.com":
            return []
        if siteUrl == "http://otpugivatel.com":
            return []
        if siteUrl == "http://otpugivateli-ptic.ru":
            return [179, 174]
        if siteUrl == "http://otpugivatel.spb.ru":
            return []
        if siteUrl in ["http://knowall.ru.com"]:
            return [615, 365]
    ###########################################
    if contractorCategory in [u"Динамические отпугиватели", u"Визуальные средства защиты"]:
        if siteUrl == "http://otpugiwatel.com":
            return []
        if siteUrl == "http://otpugivatel.com":
            return []
        if siteUrl == "http://otpugivateli-ptic.ru":
            return [177, 174]
        if siteUrl == "http://otpugivatel.spb.ru":
            return []
        if siteUrl in ["http://knowall.ru.com"]:
            return [618, 365]
    ###########################################
    if contractorCategory == u"Лазерные отпугиватели птиц":
        if siteUrl == "http://otpugiwatel.com":
            return []
        if siteUrl == "http://otpugivatel.com":
            return []
        if siteUrl == "http://otpugivateli-ptic.ru":
            return [180, 174]
        if siteUrl == "http://otpugivatel.spb.ru":
            return []
        if siteUrl in ["http://knowall.ru.com"]:
            return [617, 365]
    ###########################################
    if contractorCategory == u"Противоприсадные средства":
        if siteUrl == "http://otpugiwatel.com":
            return []
        if siteUrl == "http://otpugivatel.com":
            return []
        if siteUrl == "http://otpugivateli-ptic.ru":
            return [175, 174]
        if siteUrl == "http://otpugivatel.spb.ru":
            return []
        if siteUrl in ["http://knowall.ru.com"]:
            return [366, 365]
    ###########################################
    if contractorCategory == u"Профессиональные ультразвуковые отпугиватели птиц":
        if siteUrl == "http://otpugiwatel.com":
            return []
        if siteUrl == "http://otpugivatel.com":
            return []
        if siteUrl == "http://otpugivateli-ptic.ru":
            return [176, 174]
        if siteUrl == "http://otpugivatel.spb.ru":
            return []
        if siteUrl in ["http://knowall.ru.com"]:
            return [616, 365]
    return []


def GetProductNameVariants(categoryName, productName):
    if len([i for i in [u"ксессуар", u"отпугиватели и уничтожители", u"Ультразвуковые отпугиватели насекомых"] if categoryName.lower().find(i) != -1]) > 0:
        return False

    if categoryName in [u"Видеомониторы",
                u"Локальная охрана",
                u"Средства защиты от прослушивания через сотовый телефон",
                u"Средства защиты от прослушивания через телефон",
                u"Охранные датчики",
                u"GSM-розетки",
                u"Охранные датчики для GSM-сигнализаций",
                u"Ультразвуковые отпугиватели насекомых",
                u"Ультразвуковые отпугиватели ползающих насекомых",
                u"Отпугиватели ползающих насекомых",
                u"Отпугиватели насекомых",
                u"Детекторы скрытых камер и \"жучков\""]:
        return False

    if len([i for i in [u"Хамелеон ANG-4", u"UltraSonic-120-Рhantom", u"шансон",u"для фотоловуш", u"умный дом", u"монитор ", u"видеомонитор ", u"одуль сопряжения", u"блок питания", u"ветоотражающая", u"лотков", u"рудер", u"мундштук", u"диск"] if productName.lower().find(i) != -1]) > 0:
        return False

    if productName == u"SITITEK БИО-1":
        return [u"Отпугиватель крыс и мышей биологический", u"Биологический отпугиватель грызунов"]
    if productName == u"Weitech WK-3523":
        return [u"Комплект из 3 отпугивателей грызунов", u"Комплект из 3 ультразвуковых отпугивателей грызунов",
                u"Комплект из 3 отпугивателей крыс и мышей"   ]
    if productName == u"Weitech WK0206":
        return [u"Отпугиватель мух ультразвуковой", u"Отпугиватель мух"]
    if productName == u"Weitech WK-0432":
        return [u"Отпугиватель ос"]
    if productName == u"Weitech-WK2015":
        return [u"Комплект из 2 отпугивателей", u"Комплект из двух отпугивателей кротов", u"Комплект из 2 отпугивателей кротов"]
    if productName == "Weitech WK0071":
        return [u"Ракетка от комаров"]
    if productName == u"GRAD BLACK G2":
        return [u"Уличный уничтожитель комаров", u"Уничтожитель комаров для улицы", u"Уничтожитель комаров на открытом воздухе"]

    variants = []
    if categoryName in [
        u"Отпугиватели кротов, змей, медведок",
        u"Отпугиватели кротов",
        u"Отпугиватели кротов и змей",
        u"Отпугиватели кротов (антикроты)"]:
        variants = [u"Антикрот", u"Отпугиватель кротов", u"Электронный отпугиватель кротов", u"Ультразвуковой отпугиватель кротов"]

    if categoryName in [u"Отпугиватели змей"]:
        variants = [u"Отпугиватель змей", u"Ультразвуковой отпугиватель змей"]

    if categoryName in [u"Биоакустические отпугиватели птиц", u"Биоакустические (звуковые) отпугиватели птиц" ]:
        variants = [u"Биоакустический отпугиватель птиц", u"Звуковой отпугиватель птиц"]

    if categoryName in [u"Ультразвуковые отпугиватели птиц"]:
        variants = [u"Ультразвуковой отпугиватель птиц", u"Отпугиватель птиц"]

    if categoryName in [u"Противоприсадные шипы от птиц", u"Противоприсадные шипы", u"Противоприсадные (антиприсадные) шипы от птиц"]:
        variants = [u"Противоприсадные шипы от птиц",
                    u"Противоприсадные шипы",
                    u"Антиприсадные шипы от птиц",
                    u"Антиприсадные шипы",
                    u"Антиприсадочные шипы"]

    if categoryName in [u"Отпугиватели птиц"]:
        variants = [u"Отпугиватель птиц"]

    if categoryName in [u"Лазерные отпугиватели птиц"]:
        variants = [u"Лазерный отпугиватель птиц", u"Отпугиватель птиц"]

    if categoryName in [u"Визуальные отпугиватели птиц"]:
        variants = [u"Визуальный отпугиватель птиц", u"Отпугиватель птиц", u"Отпугиватель птиц визуальный"]

    if categoryName in [u"Пропановые пушки" , u"Пропановые пушки (громпушки)", u"Громпушки (пропановые пушки)"]:
        variants = [u"Пропановая пушка", u"Громпушка", u"Гром-пушка"]

    if categoryName in [u"Стационарные отпугиватели собак", u"Стационарные отпугиватели собак и кошек"]:
        variants = [u"Стационарный отпугиватель собак",
                    u"Отпугиватель собак стационарный",
                    u"Отпугиватель котов и кошек",
                    u"Отпугиватель животных",
                    u"Стационарный ультразвуковой отпугиватель собак",
                    u"Ультразвуковой отпугиватель животных"]

    if categoryName in [u"Ручные отпугиватели собак",
        u"Карманные отпугиватели собак",
        u"Ультразвуковые отпугиватели собак",
        u"Отпугиватели собак"]:
        variants = [
                    u"Отпугиватель собак",
                    u"Ультразвуковой отпугиватель собак",
                    u"Электронный отпугиватель собак"]

    if categoryName in [u"Ультразвуковые отпугиватели грызунов и насекомых", u"Ультразвуковой отпугиватель насекомых и грызунов"]:
        variants = [
                    u"Ультразвуковой отпугиватель грызунов и насекомых",
                    u"Отпугиватель грызунов и насекомых",
                    u"Отпугиватель грызунов и насекомых ультразвуковой"]

    if categoryName in [u"Ультразвуковые отпугиватели грызунов",
        u"Ультразвуковые отпугиватели мышей",
        u"Ультразвуковые отпугиватели крыс",
        u"Ультразвуковые отпугиватели крыс и мышей",
        u"Ультразвуковые отпугиватели грызунов (крыс и мышей)"
        ]:
        variants = [
                    u"Ультразвуковой отпугиватель грызунов",
                    u"Ультразвуковой отпугиватель мышей",
                    u"Отпугиватель мышей",
                    u"Отпугиватель грызунов",
                    u"Отпугиватель крыс",
                    u"Ультразвуковой отпугиватель мышей и крыс",
                    u"Ультразвуковой отпугиватель крыс"]

    if categoryName in [u"Отпугиватели комаров"]:
        variants = [u"Отпугиватель комаров", u"Ультразвуковой отпугиватель комаров"]

    if categoryName in [
        u"Средства от комаров, мух, мошек",
        u"Уничтожители и ловушки комаров, мух, мошек",
        u"Средства от насекомых (комаров, мух, мошек)",
        u"Ловушки и уничтожители летающих насекомых",
        u"Ловушки комаров, мух, мошек"
        ]:
        variants = [
                    u"Ловушка для комаров",
                    u"Лампа от комаров",
                    u"Электрическая ловушка для комаров",
                    u"Уничтожитель насекомых",
                    u"Уничтожитель комаров"]

    if categoryName in [u"Отпугиватели и уничтожители комаров"]:
        variants = [u"Средство от комаров", u"Средство от насекомых"]

    if categoryName in [u"Ультразвуковые отпугиватели клещей"]:
        variants = [u"Отпугиватель клещей", u"Отпугиватель клещей ультразвуковой", u"Ультразвуковой отпугиватель клещей"]

    if categoryName in [u"Алкотестеры"]:
        variants = [u"Алкометр", u"Алкотестер",u"Персональный алкотестер"]

    if categoryName in [u"Видеоглазки"]:
        if productName.lower().find(u"wi") != -1 and productName.lower().find(u"gsm") != -1:
            name_ = u"GSM/Wi-Fi видеоглазок"
        elif productName.lower().find(u"wi") != -1:
            name_ = u"Wi-Fi видеоглазок"
        elif productName.lower().find(u"gsm") != -1:
            name_ = u"GSM видеоглазок"
        else:
            name_ = u""

        variants = [u"Видеоглазок",
                    u"Видеоглазок в дверь",
                    u"Беспроводной видеоглазок",
                    u"Видеоглазок дверной",
                    u"Видеоглазок с монитором"]
        if name_ != "":
            variants = [i.replace(u"видеоглазок",name_) for i in variants]
            variants = [i.replace(u"Видеоглазок",name_) for i in variants]

    if categoryName in [u"Беспроводные домофоны"]:
        variants = [u"Беспроводной видеодомофон", u"Беспроводной домофон"]

    if categoryName in [u"Индикаторы (детекторы) поля"]:
        variants = [u"Детектор жучков", u"Индикатор поля", u"Антижучок", u"Детектор поля"]

    if categoryName in [u"Инкубаторы автоматические", u"Инкубаторы для яиц «Sititek»"]:
        variants = [u"Инкубатор для яиц автоматический",
                    u"Автоматический инкубатор для яиц",
                    u"Домашний инкубатор для яиц",
                    u"Инкубатор с автоматическим переворотом яиц"]

    if categoryName in [u"Ножеточки электрические"]:
        variants = [u"Электроножеточка",
                    u"Ножеточка электрическая",
                    u"Электрическая ножеточка",
                    u"Электрическая точилка для ножей",
                    u"Электроточилка для ножей"]

    if categoryName in [u"Детекторы скрытых камер", u"Обнаружители (детекторы) скрытых камер"]:
        variants = [u"Детектор скрытых камер",
                    u"Обнаружитель скрытых камер"]

    if categoryName == u"Планетарии":
        variants = [u"Домашний планетарий", u"Планетарий"]

    if categoryName == u"Подавители диктофонов":
        variants = [u"Подавитель диктофонов", u"Подавитель диктофонов и микрофонов", u"Подавитель микрофонов и диктофонов"]

    if categoryName in [
    u"Стационарные глушилки (подавители) связи",
    u"Стационарные подавители (глушилки)",
    u"Мобильные глушилки (подавители) связи",
    u"Мобильные подавители (глушилки)",
    u"Карманные подавители (глушилки)"
    ]:
        variants = [
                    u"Глушилка сотовой связи",
                    u"GSM-глушилка",
                    u"Подавитель сотовой связи",
                    u"Глушилка GSM-сигнала",
                    u"Глушилка сотовых телефонов",
                    u"Подавитель сигнала",
                    u"Глушилка связи",
                    u"Блокиратор сотовой связи",
                    u"Глушилка мобильной связи"]

    if categoryName in [u"Карманные миникамеры", u"Мини камеры видеонаблюдения", u"Wi-Fi мини камеры", u"Мини видеокамеры с записью"]:
        if productName.lower().find(u"wi") == -1:
            variants = [u"Мини камера",
                    u"Миниатюрная камера",
                    u"Мини камера наблюдения",
                    u"Мини видео камера",
                    u"Мини видеокамера наблюдения",
                    u"Миникамера"]
        else:
            variants = [u"Беспроводная мини камера видеонаблюдения",
                    u"Wi-Fi мини камера",
                    u"Мини IP-камера",
                    u"Миниатюрная IP-камера"]


    if categoryName in [u"Миниатюрные камеры", u"Аналоговые мини камеры наблюдения"]:
        variants = [u"Миниатюрная камера",
                    u"Мини камера наблюдения",
                    u"Мини видео камера",
                    u"Мини видеокамера наблюдения",
                    u"Миникамера"]

    if categoryName in [u"Видеодомофоны"]:
        variants = [u"Видеодомофон",
                    u"Видеодомофон для квартиры",
                    u"Видеодомофон для частного дома",
                    u"Цветной видеодомофон",
                    u"Видеодомофон с записью",
                    u"Проводной видеодомофон",
                    u"Цифровой видеодомофон"]

    if categoryName in [u"IP-видеодомофоны", u"IP-домофоны"]:
        variants = [u"IP-видеодомофон",
                    u"Видеодомофон с Wi-Fi",
                    u"Wi-Fi видеодомофон",
                    u"Видеодомофон с Wi-Fi-модулем"]

    if categoryName in [u"Беспроводные домофоны"]:
        variants = [u"Беспроводной видеодомофон",
                    u"Видеодомофон беспроводной",
                    u"Беспроводной видеодомофон для квартиры",
                    u"Беспроводной видеодомофон для частного дома"]

    if categoryName in [u"Беспроводные аудиодомофоны"]:
        variants = [u"Беспроводной аудиодомофон",
                    u"Аудиодомофон беспроводной"]

    if categoryName in [u"Электромеханические замки"]:
        variants = [u"Электромеханический замок",
                    u"Электромеханический замок на входную дверь",
                    u"Электромеханический замок на калитку",
                    u"Накладной электромеханический замок",
                    u"Замок дверной электромеханический",
                    u"Электромеханический замок на дверь",
                    u"Замок электромеханический уличный"]

    if categoryName in [u"Электромагнитные замки"]:
        variants = [u"Электромагнитный замок",
                    u"Электромагнитный замок на дверь",
                    u"Электромагнитный замок на входную дверь",
                    u"Дверной электромагнитный замок",
                    u"Уличный электромагнитный замок"]


    if categoryName in [u"GSM-сигнализации"]:
        variants = [u"GSM-сигнализация",
                    u"GSM-сигнализация для дома",
                    u"GSM-сигнализация для дачи",
                    u"Охранная GSM-сигнализация",
                    u"Охранная GSM-сигнализация для дома",
                    u"Беспроводная GSM-сигнализация",
                    u"Сигнализация для дачи с GSM-модулем",
                    u"GSM-сигнализация для квартиры"]
        if productName.lower().find("wi") != -1 and productName.lower().find("gsm") != -1:
            variants = [i.replace("GSM","GSM/Wi-Fi") for i in variants]
        if productName.lower().find("wi") != -1 and productName.lower().find("gsm") == -1:
            variants = [i.replace("GSM","Wi-Fi") for i in variants]

    if categoryName in [u"Вызывные панели", u"Вызывные панели видеодомофонов"]:
        variants = [u"Вызывная панель",
                        u"Вызывная панель домофона",
                        u"Вызывная панель видеодомофона",
                        u"Вызывная панель видеодомофона с камерой"]

    if categoryName in [u"Минидиктофоны"]:
        variants = [u"Миниатюрный диктофон",
                        u"Мини диктофон"]

    if categoryName in [u"Фотоловушки"]:
        variants = [u"Фотоловушка",
                    u"Фотоловушка для охоты",
                    u"Фотоловушка для охраны",
                    u"Фотоловушка для охраны дачи"]
        if productName.lower().find("4g") != -1:
            variants = [i.replace(u"Фотоловушка",u"4G фотоловушка") for i in variants]
        if productName.lower().find("3g") != -1:
            variants = [i.replace(u"Фотоловушка",u"3G MMS фотоловушка") for i in variants]

    if categoryName in [u"Инсектицидные лампы", u"Электрические ловушки-уничтожители насекомых"]:
        variants = [u"Инсектицидная лампа",
                    u"Электрическая ловушка для комаров",
                    u"Антимоскитная лампа",
                    u"Лампа от комаров",
                    u"Инсектицидная лампа от насекомых",
                    u"Электрический уничтожитель насекомых"]

    if categoryName in [
    u"Бытовые ловушки-уничтожители насекомых",
    u"Лампы от комаров для дома",
    u"Лампы от насекомых для дома"]:
        variants = [u"Ловушка для комаров",
                    u"Электрическая ловушка для комаров",
                    u"Уничтожитель насекомых",
                    u"Уничтожитель комаров",
                    u"Лампа от комаров для дома",
                    u"Уничтожитель летающих насекомых",
                    u"Электронная ловушка для комаров"]

    if categoryName in [
    u"Уличные ловушки-уничтожители насекомых",
    u"Уничтожители комаров, мух, мошек на улице",
    u"Уничтожители насекомых для улицы"
    ]:
        variants = [u"Лампа от комаров для улицы",
                    u"Уничтожитель комаров",
                    u"Уличная ловушка для комаров",
                    u"Уничтожитель комаров на открытом воздухе",
                    u"Ловушка для комаров на улице"]

    if categoryName in [u"Клеевые ловушки насекомых", u"Ловушки насекомых на клеевой основе"]:
        variants = [u"Ловушка для комаров",
                    u"Лампа от комаров",
                    u"Ловушка насекомых с клеевой основой"]

    return variants

def ProcessNewProduct(prestashop_api, productId, productName, category):
    if prestashop_api.siteUrl.find("http://") == -1:
        siteUrl = "http://" + prestashop_api.siteUrl
    else:
        siteUrl = prestashop_api.siteUrl
    categories = GetProductCategoriesWA(siteUrl, productName, category)
    if categories == []:
        return "не найдена категория"

    print categories
    categoryName = prestashop_api.GetCategoryName(categories[0])
    categoryId = prestashop_api.GetCategoryId(categoryName)
    if categoryId == -1:
        return "категория отсутствует в списке категорий данного сайта"

    prestashop_api.SetProductCategories(productId, categories)

    #меняем имя
    originalName = CleanItemName(prestashop_api.GetProductOriginalName(productId))
    categoryName = prestashop_api.GetCategoryName(categories[0])
    variants = GetProductNameVariants(categoryName, productName)
    if variants == []:
        return "не найдено новое наименование"
    if variants == False:
        newName = productName
    else:
        variant = random.choice(variants)
        newName = u"{} \"{}\"".format(variant, originalName)
    print newName
    prestashop_api.SetProductName(productId, newName)

    return True

def GetCategoryGender(siteUrl, categoryId):
    if siteUrl == "http://safetus.ru":
        return 20
    if siteUrl == "http://otpugiwatel.com":
        if categoryId in [1]:
            return 2
        else:
            return 1
    if siteUrl == "http://tomsk.otpugivatel.com":
        if categoryId in [136, 147, 95]:
            return 2
        elif categoryId in [107]:
            return 3
        else:
            return 1
    if siteUrl == "http://saltlamp.su":
        return 20
    if siteUrl == "http://insect-killers.ru":
        if categoryId in [135, 133, 154]:
            return 2
    if siteUrl == "http://sushilki.ru.com":
        return 20
    if siteUrl == "http://glushilki.ru.com":
        if categoryId in [145, 144]:
            return 2
        if categoryId in [147]:
            return 1
    if siteUrl == "http://incubators.shop":
        return 20
    if siteUrl == "http://mini-camera.ru.com":
        if categoryId in [144, 159]:
            return 2
        if categoryId in [160]:
            return 1
    if siteUrl == "http://otpugivateli-grizunov.ru":
        if categoryId in [99, 135]:
            return 1
    if siteUrl == "http://otpugivateli-krotov.ru":
        return 20
    if siteUrl == "http://otpugivateli-ptic.ru":
        if categoryId in [105]:
            return 1
        if categoryId in [107]:
            return 3
    if siteUrl == "http://otpugivateli-sobak.ru":
        if categoryId in [140, 143, 146]:
            return 1
    if siteUrl == "http://knowall.ru.com":
        return 20
    if siteUrl == "http://usiliteli-svyazi.ru":
        return u"Усилители связи"


def GetSiteArticlesCategories(siteUrl):
    if siteUrl == "http://safetus.ru":
        return [u"Видеоглазки"]
    if siteUrl in ["http://tomsk.otpugivatel.com"]:
        return [
                u"Отпугиватели птиц",
##                u"Отпугиватели ползающих насекомых",
                u"Отпугиватели кротов, змей, землероек, медведок",
                u"Отпугиватели и уничтожители летающих насекомых",
                u"Ультразвуковые отпугиватели собак",
                u"Ультразвуковые отпугиватели грызунов",
                u"Электроошейники для собак"
                ]
    if siteUrl in ["http://otpugiwatel.com"]:
        return [u"Ультразвуковые отпугиватели грызунов",
                u"Ультразвуковые отпугиватели собак",
##                u"Отпугиватели и уничтожители летающих насекомых",
                u"Отпугиватели кротов, змей, землероек, медведок",
##                u"Отпугиватели ползающих насекомых",
##                u"Отпугиватели птиц",
                u"Электронные ошейники для собак"
                ]
    if siteUrl == "http://insect-killers.ru":
        return [u"Ловушки и уничтожители насекомых (комаров, мух, мошек)"]
    if siteUrl == "http://sushilki.ru.com":
        return 20
    if siteUrl == "http://glushilki.ru.com":
        return [u"Глушилки cотовой связи и навигации",
                u"Детекторы скрытых камер и жучков"]
    if siteUrl == "http://incubators.shop":
        return 20
    if siteUrl == "http://mini-camera.ru.com":
        return [u"Мини камеры видеонаблюдения",
                u"Видеоглазки",
                u"Фотоловушки"]
    if siteUrl == "http://otpugivateli-grizunov.ru":
        return [u"Ультразвуковые отпугиватели грызунов",
                u"Отпугиватели кротов и змей"]
    if siteUrl == "http://otpugivateli-krotov.ru":
        return 20
    if siteUrl == "http://otpugivateli-ptic.ru":
        return [u"Отпугиватели птиц"]
    if siteUrl == "http://otpugivateli-sobak.ru":
        return [u"Ультразвуковые отпугиватели собак",
                u"Электроошейники (электронные ошейники)",
                u"Стационарные отпугиватели животных"]
    if siteUrl == "http://knowall.ru.com":
        return 20
    if siteUrl == "http://usiliteli-svyazi.ru":
        return u"Усилители связи"

def GetProductTextDescription(htmlDescription):
    try:
        ldElement = etree.HTML(htmlDescription)
    except Exception as e:
        print str(e)
        return False

    texts = [u"Требования к каналу Интернет" ,u"Выпущена новая версия прошивки", u"Как увеличить расстояние",  u"подарок" ]
    for text in texts:
        elements = ldElement.xpath(u"//div[contains(@style, 'background') and contains(., '{}')]".format(text))
        for element in elements:
            element.getparent().remove(element)

    tags = ["img", "a", "table"]
    for tag in tags:
        elements = ldElement.xpath("//{}".format(tag))
        for element in elements:
            element.getparent().remove(element)

    elements = ldElement.xpath("//h3")
    elements += ldElement.xpath("//h3/strong")
    elements += ldElement.xpath("//h2")
    elements += ldElement.xpath("//span[@style = 'font-size:16px;']/strong")
    for element in elements:
        if element.text not in [None,""]:
            element.text = "{h3} " + element.text + " {h3}"

    try:
        saleText = ldElement.xpath(u"//p[contains(.,'Москве')]")[0]
        saleText.getparent().remove(saleText)
    except:
        pass

    productDescr = GetStringDescription(ldElement)
    for title in [u'{h3} Техничекие характеристики', u'{h3} Характеристики', u'{h3} Характеристики', u'{h3} Комплектация', u'{h3} Технические  характеристики', u'{h3} Технические данные', u'{h3} Технические характеристики']:
        index = productDescr.find(title)
        if index != -1:
            productDescr = productDescr[0:index]

    try:
        productDescr = html2text.html2text (productDescr)
    except:
        return False

    chars = ["###", "_", "**", u'\u25cf', u'\u2264', u'\u2265', u'\xd7', u'\uff08', u'\u200b', u'\xbc', u'\u2015']
    for char in chars:
        productDescr = productDescr.replace(char, "")

    return productDescr


def GetTenderGroup(siteUrl, type_ = 0):
    if type_ == 0:
        if siteUrl in ["http://safetus.ru", "http://knowall.ru.com", "http://glushilki.ru.com", "http://mini-camera.ru.com"]:
            return 40039
        else:
            return 40038
    else:
        return 32050


def GetLogosProductName(productPageContent):
    try:
        span = productPageContent.xpath("//h1//span")
        for s in span:
            s.getparent().remove(s)
        novelties = [u"новинка {0}".format(x) for x in range(2016,2025)]
        novelties += [u"новинка {0}!".format(x) for x in range(2016,2025)]
        novelties += [u"Новинка {0}!".format(x) for x in range(2016,2025)]
        novelties += [u"Новинка {0}".format(x) for x in range(2016,2025)]
        titleElem = productPageContent.xpath("//h1")[0].xpath(".//text()")
        parts = [unicode(x) for x in titleElem if x.lower() not in novelties]
        productName = ""
        for part in parts:
            productName += part + " "
##            if part.find("\"") != -1 or part.find(u"«") != -1:
##                break
        for novelty in novelties:
            productName = productName.replace(novelty, "")
        productName = productName.strip().strip("-").strip(".").strip()
        productName = productName.replace("\r\n", " ")
        productName = productName.replace("  ", " ")
        return productName
    except:
        return None

def GetParamValueFromXml(xml, xpath, attrName = None):
    if type(xml) == type("123"):
        xml = etree.XML(xml)
    if attrName == None:
        value = xml.xpath(xpath)[0].text
        return value
    else:
        value = xml.xpath(xpath)[0].attrib[attrName]
        return value

def ReplaceLinksWithText(descr):
    tree = etree.HTML(descr)
    aa = tree.xpath(".//a")
    for a in aa:
        aText = a.text
        if aText == None:
            span = a.xpath(".//span")
            if len(span) == 0:
                continue
            else:
                span = span[0]
                aText = span.text
                aHtml = GetStringDescription(a)
                if aText == None:
                    continue
                descr = descr.replace( aHtml, aText)
                continue
        aHtml = GetStringDescription(a).split("</a>")[0] + "</a>"
        descr = descr.replace(aHtml, aText)
    return descr

def GetCategoryIdForPhrase( siteUrl, linkText):
    if siteUrl == "http://insect-killers.ru":
        if linkText.find(u"нсектицид") != -1:
            return 154
        if linkText.find(u"овушк") != -1 and linkText.find(u"комар") != -1 and linkText.find(u"личн") == -1:
            return 170
        if linkText.find(u"ничтож") != -1 and linkText.find(u"личн") == -1 and linkText.find(u"возд") == -1:
            return 171
        if linkText.find(u"амп") != -1 and linkText.find(u"комар") != -1:
            return 172
        if linkText.find(u"редств") != -1:
            return 152
    if siteUrl == "http://glushilki.ru.com":
        if linkText.find(u"нтижуч") != -1:
            return 177
        if linkText.find(u"ндикатор") != -1 or (linkText.find(u"етектор") != -1 and linkText.find(u"пол") != -1):
            return 178

    return -1

def DeleteSalesData(text):
    tree = etree.HTML(text)

    try:
        p = tree.xpath(u"//p[contains(.,'Москв') or contains(.,'Омск')]")[0]
        p.getparent().remove(p)
    except:
        pass

    description = GetStringDescription(tree)
    return description

def GetProductShortDescription(text):
    text = text.replace("<br/>", "")
    text = text.replace("<!--noindex-->", "")
    text = text.replace("<!--/noindex-->", "")
    tree = etree.HTML(text)

    tags_ = ["h3", "h2"]
    for tag in tags_:
        elems = tree.xpath("//{}".format(tag))
        for elem in elems:
            if "style" in elem.attrib.keys():
                elem.attrib.pop("style")
    description = GetStringDescription(tree)

    tags = ["h3", "h2"]
    for tag in tags:
        titles = [u"<{0}>Техническ".format(tag), u"<{0}>Комплект".format(tag),u"<{0}>Преимущества".format(tag)]
        for title in titles:
            if text.find(title) != -1:
                ind = text.find(title)
                descr = text[ind:].replace("<!--/noindex-->", "")
                return descr

    tree = etree.HTML(text)
    elem = tree.xpath(u"//p")[0]
    description = GetStringDescription(elem)
    return description


def GetIntValue(value):
    parts = value.split(" ")
    for part in parts:
        try:
            intv = int(part)
            return intv
        except:
            pass
    return -1

def Transliterate(string):
    capital_letters = {u'А': u'A',
                   u'Б': u'B',
                   u'В': u'V',
                   u'Г': u'G',
                   u'Д': u'D',
                   u'Е': u'E',
                   u'Ё': u'E',
                   u'З': u'Z',
                   u'И': u'I',
                   u'Й': u'Y',
                   u'К': u'K',
                   u'Л': u'L',
                   u'М': u'M',
                   u'Н': u'N',
                   u'О': u'O',
                   u'П': u'P',
                   u'Р': u'R',
                   u'С': u'S',
                   u'Т': u'T',
                   u'У': u'U',
                   u'Ф': u'F',
                   u'Х': u'H',
                   u'Ъ': u'',
                   u'Ы': u'Y',
                   u'Ь': u'',
                   u'Э': u'E',}

    capital_letters_transliterated_to_multiple_letters = {u'Ж': u'Zh',
                                                          u'Ц': u'Ts',
                                                          u'Ч': u'Ch',
                                                          u'Ш': u'Sh',
                                                          u'Щ': u'Sch',
                                                          u'Ю': u'Yu',
                                                          u'Я': u'Ya',}


    lower_case_letters = {u'а': u'a',
                       u'б': u'b',
                       u'в': u'v',
                       u'г': u'g',
                       u'д': u'd',
                       u'е': u'e',
                       u'ё': u'e',
                       u'ж': u'zh',
                       u'з': u'z',
                       u'и': u'i',
                       u'й': u'y',
                       u'к': u'k',
                       u'л': u'l',
                       u'м': u'm',
                       u'н': u'n',
                       u'о': u'o',
                       u'п': u'p',
                       u'р': u'r',
                       u'с': u's',
                       u'т': u't',
                       u'у': u'u',
                       u'ф': u'f',
                       u'х': u'h',
                       u'ц': u'ts',
                       u'ч': u'ch',
                       u'ш': u'sh',
                       u'щ': u'sch',
                       u'ъ': u'',
                       u'ы': u'y',
                       u'ь': u'',
                       u'э': u'e',
                       u'ю': u'yu',
                       u'я': u'ya',}

    for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
        string = re.sub(ur"%s([а-я])" % cyrillic_string, ur'%s\1' % latin_string, string)

    for dictionary in (capital_letters, lower_case_letters):
        for cyrillic_string, latin_string in dictionary.iteritems():
            string = string.replace(cyrillic_string, latin_string)

    for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
        string = string.replace(cyrillic_string, latin_string.upper())

    return string

def MakeShortDescription(text):
    text_ = ReplaceLinksWithText(text)
    ldElement = etree.HTML(text_)
    elements = ldElement.xpath(".//img")
    for element in elements:
        imgElem = GetStringDescription(element)
        imgElem = imgElem.split(">")[0] + ">"
        text_ = text_.replace(imgElem, "")
    array_ = GetTextFromHtml(text_)
    descr = ".".join(array_.split(".")[0:1]).strip()
    descr = u"<p>{0}</p>".format(descr)

    return descr

def ReplaceSpecialCharacters(text):
    ii = ["/", "\\", "*"]
    for i in ii:
        text = text.replace(i,"-")
    return text

def GetProductDimensions(name, descr):
    weight = 0
    dimensions = [0,0,0]
    root = etree.HTML(descr)
    elements = root.xpath("//ul//li")
    elements += root.xpath(".//tr")
    for elem in elements:
        text = u""
        if elem.tag == "tr":
            text = GetTextFromHtml(GetStringDescription(elem))
            text = text.replace("\n\n", "").replace("|"," : ")
        else:
            text = GetTextFromHtml(GetStringDescription(elem))
        text = text.replace(u"&nbsp;", " ").strip()
        text = text.replace(u"\xa0", " ")
        if (text.lower().find(u"вес") != -1 and text.lower().find(u"вест") == -1 and text.lower().find(u"весь") == -1 and text.lower().find(u"весн") == -1) or text.lower().find(u"масса") != -1:
            if weight != 0:
                continue
            words = text.split(u" ")
            for word in words:
                if word == '':
                    continue
                if word[0].isdigit():
                    word = word.split(u"\xa0")[0]
                    word = word.replace(",", ".").replace(u"грамм;", "").replace(u"кг", "").replace(u"кг.", "").replace(u";", "").replace(u"гр.", "").replace(u"г.", "").replace(u"г", "")
                    try:
                        weight = float(word)
                    except Exception as e:
                        continue
                    if weight > 15 or text.lower().find(u"кг") == -1 or name.lower().find("edic") != -1 or name.lower().find("soroka") != -1:
                        weight = float(weight) / float(1000)#переводим в кг
                    break
        if text.lower().find(u"размер") != -1 or text.lower().find(u"габарит") != -1:
            if dimensions[0] != 0:
                continue
            delimiters = [u"X", "x", "*", u"х"]
            text_ = text
            for delimiter in delimiters:
                if text.find(delimiter) == -1:
                    continue
                text = text.replace(u"—", ":")
                text = text.split(":")[-1].split("(")[0].strip().replace(",", ".")
                words = [u"Длина", u"(Д)", u"(Ш)", u"(В)", u"без антенны",u"без антенн",u"Габаритные размеры", u"Габариты", u"* Размеры", u"Размеры -", u"* Размер", u"см.",u"см;",u"мм;",u"мм.",u"см.",u"mm",u"мм", u"см",u"м", u"-", u"*"]
                for word in words:
                    text = text.replace(word, "")
                if text.find(u" {} ".format(delimiter)) == -1:
                    parts = text.split(delimiter)
                else:
                    parts = text.split(u" {} ".format(delimiter))
                if len(parts) != 3:
                    continue
                try:
                    dimensions = [float(i) for i in parts]
                except Exception as e:
                    pass
                if len(dimensions) != 3:
                    continue
                break
            if text_.lower().find(u"мм") != -1 or text_.lower().find(u"mm") != -1 or len([i for i in dimensions if i >= 100]) > 0:
                dimensions = [float(i / 10) for i in dimensions]#переводим в см

    return [weight, dimensions]

