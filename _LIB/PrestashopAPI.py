﻿# -*- coding: utf-8 -*-
import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import MySQLdb
import io
import shutil
import mimetypes
import datetime
import log
import commonLibrary

class PrestashopAPI():

    siteUrl = None
    key = None
    dbConnector = None
    localDbConnector = None
    dbName = None
    dbPassword = None
    dbUser = None
    dbHost = None
    dbPort = None
    logging = None
    allProducts = None
    message = None

    def __init__(self, siteUrl = None, key = None, dbHost = None, dbName = None, dbUser = None, dbPassword = None, dbPort = 3306, logFilePath = None):
        if dbUser == "":
            dbUser = dbName
        self.siteUrl = siteUrl
        self.key = key
        self.dbName = dbName
        self.dbPassword = dbPassword
        self.dbUser = dbUser
        self.dbHost = dbHost
        self.dbPort = dbPort
        if siteUrl != None:
            self.InitConnetcor()
        self.InitLocalConnector()
        if logFilePath != None:
            self.logging = log.Log(logFilePath)


    ######## Методы работы с БД ###########################################

    def InitConnetcor(self):
        counter = 1
        while True:
            if counter > 5:
                raise Exception("")
                return
            try:
                self.dbConnector = MySQLdb.connect(host=self.dbHost, port=self.dbPort, user=self.dbUser, passwd=self.dbPassword, db=self.dbName, charset='utf8')
                self.dbConnector.autocommit(True)
                return
            except Exception as error:
                if error.args[1].find("127.0.0.1") == -1:
                    print error.args[1]
                print str(error)
                counter += 1

    def InitLocalConnector(self):
        counter = 1
        while True:
            if counter > 1:
##                raise Exception("")
                return
            try:
                self.localDbConnector = MySQLdb.connect(host="65.108.89.105", port=3306, user="knowall_user_ex", passwd="1qaz@WSX", db="prestashop", charset='utf8')
                self.localDbConnector.autocommit(True)
                return
            except Exception as e:
                print str(e)
                counter += 1

    def MakeUpdateQueue(self, sql, breakIfError = False):
        for i in range(0, 10):
            try:
                cursor = self.dbConnector.cursor()
                resp = cursor.execute(sql)
                cursor.close()
                self.dbConnector.commit()
                break
            except Exception, e:
                if breakIfError == True:
                    self.InitConnetcor()
                    return
                with open("sql_err.txt", "w") as log:
                    log.write(str(e))
                    log.write(sql.encode('cp1251'))
                print e
##                print sql
                print "reconnect to database"
                self.InitConnetcor()

    def MakeGetInfoQueue(self, sql):
        for i in range(0, 10):
            try:
                cursor = self.dbConnector.cursor()
                cursor.execute(sql)
                return cursor.fetchall()
            except Exception, e:
                print e
                print sql
                print "reconnect to database"
                self.InitConnetcor()

    def MakeLocalDbGetInfoQueue(self, sql):
        for i in range(0, 10):
            try:
                cursor = self.localDbConnector.cursor()
                cursor.execute(sql)
                return cursor.fetchall()
            except Exception, e:
                print e
                print sql
                print "reconnect to database"
                self.InitLocalConnector()

    def MakeLocalDbUpdateQueue(self, sql, breakIfError = False):
        for i in range(0, 10):
            try:
                cursor = self.localDbConnector.cursor()
                resp = cursor.execute(sql)
                cursor.close()
                self.localDbConnector.commit()
                break
            except Exception, e:
                if breakIfError == True:
                    self.InitLocalConnector()
                    return
##                with open("sql_err.txt", "w") as log:
##                    log.write(str(e))
##                    log.write(sql)
                print e
                print sql
                print "reconnect to database"
                self.InitLocalConnector()

    def GetTodayDate(self):
        today = datetime.date.today()
        return today.strftime('%Y-%m-%d')

    def Transliterate(self, string):

        capital_letters = {u'А': u'A',
                       u'Б': u'B',
                       u'В': u'V',
                       u'Г': u'G',
                       u'Д': u'D',
                       u'Е': u'E',
                       u'Ё': u'E',
                       u'З': u'Z',
                       u'И': u'I',
                       u'Й': u'Y',
                       u'К': u'K',
                       u'Л': u'L',
                       u'М': u'M',
                       u'Н': u'N',
                       u'О': u'O',
                       u'П': u'P',
                       u'Р': u'R',
                       u'С': u'S',
                       u'Т': u'T',
                       u'У': u'U',
                       u'Ф': u'F',
                       u'Х': u'H',
                       u'Ъ': u'',
                       u'Ы': u'Y',
                       u'Ь': u'',
                       u'Э': u'E',}

        capital_letters_transliterated_to_multiple_letters = {u'Ж': u'Zh',
                                                              u'Ц': u'Ts',
                                                              u'Ч': u'Ch',
                                                              u'Ш': u'Sh',
                                                              u'Щ': u'Sch',
                                                              u'Ю': u'Yu',
                                                              u'Я': u'Ya',}


        lower_case_letters = {u'а': u'a',
                           u'б': u'b',
                           u'в': u'v',
                           u'г': u'g',
                           u'д': u'd',
                           u'е': u'e',
                           u'ё': u'e',
                           u'ж': u'zh',
                           u'з': u'z',
                           u'и': u'i',
                           u'й': u'y',
                           u'к': u'k',
                           u'л': u'l',
                           u'м': u'm',
                           u'н': u'n',
                           u'о': u'o',
                           u'п': u'p',
                           u'р': u'r',
                           u'с': u's',
                           u'т': u't',
                           u'у': u'u',
                           u'ф': u'f',
                           u'х': u'h',
                           u'ц': u'ts',
                           u'ч': u'ch',
                           u'ш': u'sh',
                           u'щ': u'sch',
                           u'ъ': u'',
                           u'ы': u'y',
                           u'ь': u'',
                           u'э': u'e',
                           u'ю': u'yu',
                           u'я': u'ya',}

        for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
            string = re.sub(ur"%s([а-я])" % cyrillic_string, ur'%s\1' % latin_string, string)

        for dictionary in (capital_letters, lower_case_letters):

            for cyrillic_string, latin_string in dictionary.iteritems():
                string = string.replace(cyrillic_string, latin_string)

        for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
            string = string.replace(cyrillic_string, latin_string.upper())

        return string

    def GetTableEntriesNumber(self, tableName):
        sql = "SELECT COUNT(*) FROM %(tableName)s"%{"tableName":tableName}
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetLastInsertedId(self, tableName, columnName):
        sql = """SELECT %(columnName)s FROM %(tableName)s
                ORDER BY %(columnName)s DESC
                LIMIT 1"""%{"columnName":columnName, "tableName":tableName}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return 0

        return data[0][0]

    def GetStoreID(self, address):
        sql = "SELECT id_store FROM ps_store WHERE address1 = '%(address)s'"%{"address":address}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetShopStores(self, shopName):
        sql = u"""SELECT name, address1 FROM ps_store
                 WHERE city = '{0}'""".format(shopName)

        data = self.MakeGetInfoQueue(sql)

        return data

    def DeleteStore(self, city, address1):
        sql = u"""DELETE FROM ps_store
                 WHERE city = '{0}' AND address1 = '{1}'""".format(city, address1)
        self.MakeUpdateQueue(sql)

    def GetIdCmsByLinkRewrite(self, region):
        sql = """SELECT id_cms
                 FROM ps_cms_lang
                 WHERE link_rewrite like '%""" + region + "%'"
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetIdCmsByTitle(self, title):
        sql = """SELECT id_cms FROM ps_cms_lang
                 WHERE meta_title like '%""" + title + "%'"
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetSSLEnabled(self):
        sql = """SELECT value FROM ps_configuration
                 WHERE name = 'PS_SSL_ENABLED'"""
        return int(self.MakeGetInfoQueue(sql)[0][0])

    def GetSiteUrlScheme(self):
        if self.GetSSLEnabled():
            return "https://"
        else:
            return "http://"

    ######################################################################
    ############################### SHOPS ################################
    ######################################################################

    def GetShopID(self, name):
        sql = "SELECT id_shop FROM ps_shop WHERE name = '%(name)s'"%{"name":name}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetShopIdByUrl(self, url):
        sql = "SELECT id_shop FROM ps_shop_url WHERE domain = '%(url)s'"%{"url":url}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetShopIdByName(self, name):
        sql = "SELECT id_shop FROM ps_shop WHERE name = '%(name)s'"%{"name":name}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def DeleteShop(self,id_shop):
        sTables = """ps_shop_url,
ps_category_lang,
ps_category_shop,
ps_shop,
ps_cms_block_shop,
ps_cms_shop,
ps_configuration,
ps_country_shop,
ps_currency_shop,
ps_employee_shop,
ps_feature_shop,
ps_group_shop,
ps_hook_module,
ps_image_shop,
ps_info,
ps_iqitmegamenu_tabs_shop,
ps_lang_shop,
ps_layered_category,
ps_layered_filter_shop,
ps_layered_price_index,
ps_meta_lang,
ps_module_country,
ps_module_currency,
ps_module_group,
ps_module_shop,
ps_product_lang,
ps_product_media,
ps_product_shop,
ps_stock_available,
ps_reinsurance,
ps_reinsurance_lang,
ps_tax_rules_group_shop,
ps_webservice_account_shop,
ps_zone_shop"""
        tables = [x.replace("\n","") for x in sTables.split(",")]
        for table in tables:
            sql = """DELETE FROM {0}
                      WHERE id_shop = {1}""".format(table, id_shop)
            self.MakeUpdateQueue(sql)

    def AddShopUrl(self, shopID, url):
        sql = """INSERT INTO ps_shop_url(id_shop, domain, domain_ssl, physical_uri, virtual_uri, main, active)
                VALUES (%(shopID)s,'%(url)s','%(url)s','/','',1,1)"""%{"shopID":shopID, "url":url}
        self.MakeUpdateQueue(sql)

    def GetShopProducts(self, shopID):
        sql = """SELECT id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description
                FROM ps_product_lang
                WHERE id_shop = %(shopID)s"""%{"shopID":shopID}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetShopActiveProducts(self, shopID):
        sql = """SELECT pl.id_product, link_rewrite, meta_title, description, name, meta_keywords, meta_description FROM ps_product_lang as pl
                LEFT JOIN ps_product
                ON pl.id_product = ps_product.id_product
                WHERE pl.id_shop = %(shopID)s AND ps_product.active = 1"""%{"shopID":shopID}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetActiveProducts(self):
        sql = """SELECT pl.id_product, pl.name, pl.original_name FROM ps_product_lang as pl
                LEFT JOIN ps_product
                ON pl.id_product = ps_product.id_product
                WHERE ps_product.active = 1"""
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetShopProductShortDescription(self, shopID, productId):
        sql = """SELECT description_short
                FROM ps_product_lang
                WHERE id_shop = %(shopID)s and id_product = %(productId)s"""%{"shopID":shopID,
                "productId":productId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetShopProductDescription(self, shopID, productId):
        sql = """SELECT description
                FROM ps_product_lang
                WHERE id_shop = %(shopID)s and id_product = %(productId)s"""%{"shopID":shopID,
                "productId":productId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetShopProductMetaTitle(self, shopID, productId):
        sql = """SELECT meta_title
                FROM ps_product_lang
                WHERE id_shop = %(shopID)s and id_product = %(productId)s"""%{"shopID":shopID,
                "productId":productId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def SetShopProductShortDescription(self, shopID, productId, description_short):
        sql = """UPDATE ps_product_lang
                SET description_short = '%(description_short)s'
                WHERE id_shop = %(shopID)s and id_product = %(productId)s"""%{"shopID":shopID,
                "productId":productId, "description_short":description_short}

        data = self.MakeUpdateQueue(sql)

    def SetProductShortDescription(self, productId, description_short):
        sql = """UPDATE ps_product_lang
                SET description_short = '%(description_short)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "description_short":description_short}
        data = self.MakeUpdateQueue(sql)

    def SetShopProductDescription(self, shopID, productId, description):
        sql = """UPDATE ps_product_lang
                SET description = '%(description)s'
                WHERE id_shop = %(shopID)s and id_product = %(productId)s"""%{"shopID":shopID,
                "productId":productId, "description":description}

        data = self.MakeUpdateQueue(sql)

    def SetShopProductMetaTitle(self, shopId, productId, meta_title):
        sql = u"""UPDATE ps_product_lang
                 SET meta_title = '{0}'
                 WHERE id_shop = {1} and id_product = {2}""".format(meta_title, shopId, productId)

        data = self.MakeUpdateQueue(sql)

    def SetShopProductLinkRewrite(self, shopId, productId, link_rewrite):
        sql = u"""UPDATE ps_product_lang
                 SET link_rewrite = '{0}'
                 WHERE id_shop = {1} and id_product = {2}""".format(link_rewrite, shopId, productId)

        data = self.MakeUpdateQueue(sql)

    def GetShopProductsOriginalNames(self, shopID):
        sql = """SELECT id_product, original_name
                FROM ps_product_lang
                WHERE id_shop = %(shopID)s"""%{"shopID":shopID}
        data = self.MakeGetInfoQueue(sql)

        return data

    def GetShopCategories(self, shopID):
        sql = """SELECT id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description
                FROM ps_category_lang
                WHERE id_shop = %(shopID)s"""%{"shopID":shopID}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetShopsActiveCategories(self, shopID):
        sql = """SELECT ps_category_lang.id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description FROM ps_category_lang
                LEFT JOIN ps_category
                ON ps_category.id_category = ps_category_lang.id_category
                WHERE ps_category_lang.id_shop = %(shopID)s and ps_category.active = 1"""%{"shopID":shopID}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetAllShops(self):
        sql = """SELECT ps_shop_url.id_shop, domain, name FROM ps_shop_url
                 LEFT JOIN ps_shop
                 ON ps_shop.id_shop = ps_shop_url.id_shop"""

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetInactiveShops(self):
        sql = """SELECT ps_shop_url.id_shop, domain, name FROM ps_shop_url
                 LEFT JOIN ps_shop
                 ON ps_shop.id_shop = ps_shop_url.id_shop
                 WHERE ps_shop.active = 0"""

        return self.MakeGetInfoQueue(sql)

    def GetShopsUrls(self):
        sql = """SELECT * FROM ps_shop_url"""
        data = self.MakeGetInfoQueue(sql)
        return data

    def SetShopDomain(self, id_shop, domain):
        sql = """UPDATE ps_shop_url
                 SET domain = '{0}', domain_ssl  = '{0}'
                 WHERE id_shop = {1}""".format(domain, id_shop)
        data = self.MakeGetInfoQueue(sql)
        return data

    def GetShops(self):
        sql = """SELECT * FROM ps_shop"""
        data = self.MakeGetInfoQueue(sql)
        return data

    def GetShopPostcode(self, shopName):
        sql = """SELECT DISTINCT postcode FROM ps_store
                 WHERE city = '%(shopName)s'"""%{"shopName":shopName}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetShopActiveCarriersNames(self, shopId):
        sql = """SELECT name FROM ps_carrier
                 LEFT JOIN ps_carrier_shop
                 ON ps_carrier.id_carrier = ps_carrier_shop.id_carrier
                 WHERE ps_carrier.active = 1 AND ps_carrier.deleted = 0 AND ps_carrier_shop.id_shop = %(shopId)s """%{"shopId":shopId}

        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetShopActiveCarriersIds(self, shopId):
        sql = """SELECT ps_carrier.id_carrier FROM ps_carrier
                 LEFT JOIN ps_carrier_shop
                 ON ps_carrier.id_carrier = ps_carrier_shop.id_carrier
                 WHERE ps_carrier.active = 1 AND ps_carrier.deleted = 0 AND ps_carrier_shop.id_shop = %(shopId)s """%{"shopId":shopId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return [x[0] for x in data]

    def GetActiveCarriersIds(self):
        sql = """SELECT id_carrier FROM ps_carrier
                 WHERE active = 1 AND deleted = 0"""

        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetShopCarrierIdByName(self, shopId, carrierName):
        sql = u"""SELECT ps_carrier.id_carrier FROM ps_carrier
                 LEFT JOIN ps_carrier_shop
                 ON ps_carrier.id_carrier = ps_carrier_shop.id_carrier
                 WHERE ps_carrier.name like '%{1}%'
                 AND ps_carrier_shop.id_shop = {0}
                 AND ps_carrier.active = 1 AND ps_carrier.deleted = 0""".format(shopId,carrierName)

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetCarrierNameById(self, carrierId):
        sql = """SELECT name FROM ps_carrier
                 WHERE id_carrier = {0}""".format(carrierId)

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetCarriersIdsByName(self, carrierName):
        sql = u"""SELECT id_carrier FROM ps_carrier
                 WHERE name = '{}'""".format(carrierName)

        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def SetCarrierWorktime(self, carrierId, worktime):
        sql = u"""UPDATE ps_carrier_lang
                  SET worktime = '{}'
                  WHERE id_carrier = {}""".format(worktime, carrierId)
        self.MakeUpdateQueue(sql)

    def GetCarrierWorktime(self, carrierId):
        sql = u"""SELECT worktime FROM ps_carrier_lang
                  WHERE id_carrier = {}""".format( carrierId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def DeleteShopsCarriers(self, id_shop):
        sql = """DELETE FROM ps_carrier_lang
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}

        self.MakeUpdateQueue(sql)

        sql = """DELETE FROM ps_carrier_shop
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}

        self.MakeUpdateQueue(sql)

        sql = """DELETE FROM ps_carrier_tax_rules_group_shop
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}

        self.MakeUpdateQueue(sql)

        sql = """DELETE FROM ps_carrier_tax_rules_group_shop
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}

        self.MakeUpdateQueue(sql)

    def GetShopName(self, id_shop):
        sql = """SELECT name FROM ps_shop
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetShopDomain(self, id_shop):
        sql = """SELECT domain FROM ps_shop_url
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}
        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetShopMainDomain(self, id_shop):
        domain = self.GetSiteUrlScheme() + self.GetShopDomain(id_shop)
        if not self.IsSubfolderSite():
            return domain

        if id_shop == 1:
            return domain

        virtual_uri = self.GetShopVirtualUri(id_shop).replace("/", "")
        return "{0}/{1}".format(domain, virtual_uri)

    def SetShopName(self, shopId, name):
        sql = u"""UPDATE ps_shop
                 SET name = '{0}'
                 WHERE id_shop = {1}""".format(name, shopId)
        self.MakeUpdateQueue(sql)

    def SetShopActive(self,shopId, active):
        sql = u"""UPDATE ps_shop
                 SET active = '{0}'
                 WHERE id_shop = {1}""".format(active, shopId)
        self.MakeUpdateQueue(sql)

    def SetShopDomainActive(self,shopId, active):
        sql = u"""UPDATE ps_shop_url
                 SET active = '{0}'
                 WHERE id_shop = {1}""".format(active, shopId)
        self.MakeUpdateQueue(sql)

    def GetShopVirtualUri(self, id_shop):
        sql = """SELECT virtual_uri FROM ps_shop_url
                 WHERE id_shop = %(id_shop)s"""%{"id_shop":id_shop}
        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def IsSubfolderSite(self):
        try:
            return self.GetShopsUrls()[1][5] != ""
        except:
            return False

    def GetAllShopsFull(self):
        sql = """SELECT ps_shop_url.id_shop, domain, name, virtual_uri FROM ps_shop_url
                 LEFT JOIN ps_shop
                 ON ps_shop.id_shop = ps_shop_url.id_shop"""

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    #########################################################################
    ############################### PRODUCTS ################################
    #########################################################################

    def GetProductID(self, name, bLike = False):
        if bLike == False:
            sql = u"SELECT id_product FROM ps_product_lang WHERE original_name = '%(catName)s'"%{"catName":name}
        else:
            sql = u"SELECT id_product FROM ps_product_lang WHERE original_name LIKE '%" + name + "%'"
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetProductIdByName(self, name, bLike = False):
        if bLike == False:
            sql = u"SELECT id_product FROM ps_product_lang WHERE name = '{0}'".format(name)
        else:
            sql = u"SELECT id_product FROM ps_product_lang WHERE name LIKE '%{0}%'".format(name)
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetProductIDs(self, name, bLike = False):
        if bLike == False:
            sql = u"SELECT id_product FROM ps_product_lang WHERE original_name = '%(catName)s'"%{"catName":name}
        else:
            sql = u"SELECT id_product FROM ps_product_lang WHERE original_name LIKE '%" + name + "%'"
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return [x[0] for x in data ]

    def GetShopProductLangTable(self,shopId):
        sql = """SELECT * from ps_product_lang
                 WHERE id_shop = {0}""".format(shopId)

        data = self.MakeGetInfoQueue(sql)

        return data


    def GetProductDetailsById(self, productId):
        sql = """SELECT  pl.name, ps.price, ps.wholesale_price, pl.description, pl.link_rewrite, pl.meta_title
                FROM ps_product_lang AS pl
                LEFT JOIN ps_product_shop AS ps
                ON pl.id_product = ps.id_product
                WHERE pl.id_product = %(productId)s LIMIT 1"""%{"productId":productId}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0]

    def GetShopProductDetailsById(self, productId, shopId):
        sql = """SELECT  pl.name, ps.price, ps.wholesale_price, pl.description, pl.link_rewrite, pl.meta_title
                FROM ps_product_lang AS pl
                LEFT JOIN ps_product_shop AS ps
                ON pl.id_product = ps.id_product
                WHERE pl.id_product = %(productId)s AND pl.id_shop = %(shopId)s LIMIT 1"""%{"productId":productId, "shopId":shopId}

        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0]

    def GetShopProductMetaTagsById(self, productId, shopId):
        sql = """SELECT  pl.meta_title, pl.meta_description, pl.meta_keywords
                FROM ps_product_lang AS pl
                LEFT JOIN ps_product_shop AS ps
                ON pl.id_product = ps.id_product
                WHERE pl.id_product = {} AND pl.id_shop = {} LIMIT 1""".format( productId, shopId)

        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0]

    def GetProductDetailsByName(self, productName):
        sql = """SELECT  pl.name, ps.price, ps.wholesale_price, pl.description, pl.link_rewrite, pl.meta_title
                FROM ps_product_lang AS pl
                LEFT JOIN ps_product_shop AS ps
                ON pl.id_product = ps.id_product
                WHERE pl.name LIKE '%""" + productName + """%'  LIMIT 1"""
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0]

    def GetProductsIDs(self):
        sql = """SELECT DISTINCT id_product FROM ps_product_lang"""
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetProductsIds(self):
        sql = """SELECT DISTINCT id_product FROM ps_product_lang"""
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return [x[0] for x in data]

    def GetActiveProductsIDs(self):
        sql = """SELECT DISTINCT id_product FROM ps_product
                 WHERE active = 1"""
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return [x[0] for x in data]

    def GetProductsNames(self):
        sql = """SELECT DISTINCT name FROM ps_product_lang"""
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def GetProductImagesIds(self, productID):
        sql = """SELECT  id_image FROM ps_image
                 WHERE id_product = %(productID)s"""%{"productID":productID}

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def DeleteProductImages(self, productID):
        sql = """DELETE FROM ps_image
                 WHERE id_product = %(productID)s"""%{"productID":productID}

        data = self.MakeUpdateQueue(sql)

    def GetProductUrl(self, productId, shopId):
        sql = """SELECT link_rewrite FROM ps_category_lang
                 LEFT JOIN ps_product
                 ON ps_category_lang.id_category = ps_product.id_category_default
                 WHERE id_product = %(productId)s and id_shop = %(shopId)s"""%{"productId":productId, "shopId":shopId}

        try:
            productUrl = self.MakeGetInfoQueue(sql)[0][0].lower() + "/"
        except:
            return -1

        sql = """SELECT link_rewrite FROM ps_product_lang
                 WHERE id_product = %(productId)s and id_shop = %(shopId)s"""%{"productId":productId, "shopId":shopId}

        try:
            productUrl += str(productId) + "-" + self.MakeGetInfoQueue(sql)[0][0] + ".html"
        except:
            return -1

        return productUrl

    def GetProductFullUrl(self, productId, shopId):
        url = self.GetProductUrl(productId, shopId)
        if url == -1:
            return -1

        mainDomain = self.GetShopMainDomain(shopId)
        fullUrl = u"{}/{}".format(mainDomain, url)
        return fullUrl

    def SetProductMinQuantity(self, productId, minimal_quantity):
        sql = """UPDATE ps_product
                SET minimal_quantity = {}
                WHERE id_product = {}""".format(minimal_quantity, productId)
        self.MakeUpdateQueue(sql)
        sql = """UPDATE ps_product_shop
                SET minimal_quantity = {}
                WHERE id_product = {}""".format(minimal_quantity, productId)
        self.MakeUpdateQueue(sql)

    def SetProductQuantityForAllShops(self, productId, quantity):
        sql = """UPDATE ps_stock_available
                SET quantity = %(quantity)s
                WHERE id_product = %(productId)s"""%{"quantity":quantity,
               "productId":productId}

        self.MakeUpdateQueue(sql)

    def MakeProductAttributesEnabledForAllShops(self, productId):
        ids = self.GetProductAttributesIds(productId)
        for id in ids:
            shops = self.GetAllShops()
            for shop in shops:
                shopId, shopDomen, shopName = shop
                sql = """INSERT INTO ps_stock_available(id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)
                        VALUES ({}, {}, {}, 0, 3000, 0, 2)""".format(productId,id, shopId)
                self.MakeUpdateQueue(sql)

    def MakeProductEnabledForAllShops(self, productId):
        self.MakeProductUnavailableForAllShops(productId)

        shops = self.GetAllShops()
        for shop in shops:
            shopId, shopDomen, shopName = shop
            sql = """INSERT INTO ps_stock_available(id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)
                    VALUES (%(productId)s,0,%(shopId)s, 0, 0, 0, 2)"""%{"productId":productId, "shopId":shopId}
            self.MakeUpdateQueue(sql)

        sql = u"""UPDATE ps_product_lang
                SET available_now = 'В наличии'
                WHERE id_product = %(productId)s"""%{"productId":productId}
        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product_shop
                SET active = 1, available_for_order = 1, show_price = 1
                WHERE id_product = %(productId)s"""%{"productId":productId}
        self.MakeUpdateQueue(sql)


    def MakeProductUnavailableForAllShops(self, productId):
        sql = """DELETE FROM ps_stock_available
                 WHERE id_product = %(productId)s"""%{"productId":productId}
        self.MakeUpdateQueue(sql)


    def SetProductUpdateDate(self, productId, date):
        sql = """UPDATE ps_product_shop
                SET date_upd = '%(date)s'
                WHERE id_product = %(productId)s"""%{"date":date,"productId":productId}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product
                SET date_upd = '%(date)s'
                WHERE id_product = %(productId)s"""%{"date":date,"productId":productId}

        self.MakeUpdateQueue(sql)

    def SetProductsUpdateDate(self, date):
        sql = """UPDATE ps_product_shop
                SET date_upd = '%(date)s'"""%{"date":date}
        self.MakeUpdateQueue(sql)
        sql = """UPDATE ps_product
                SET date_upd = '%(date)s'"""%{"date":date}
        self.MakeUpdateQueue(sql)

    def GetProductUpdateDate(self, productId):
        sql = """SELECT date_upd FROM ps_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetProductDescription(self, productId, shopId = False):
        try:
            if shopId == False:
                return self.GetProductDetailsById(productId)[3]
            else:
                return self.GetShopProductDetailsById(productId, shopId)[3]
        except:
            return -1

    def GetProductShortDescription(self, productId):
        sql = """SELECT description_short FROM ps_product_lang
                WHERE id_product = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductArticle(self, productId):
        sql = """SELECT reference FROM ps_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetProductCategoryDefault(self, productId):
        sql = """SELECT id_category_default FROM ps_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetProductCategoryDefaultName(self, productId):
        sql = """SELECT name FROM ps_category_lang
                LEFT JOIN ps_product
                ON ps_category_lang.id_category = ps_product.id_category_default
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetProductCategories(self, productId):
        sql = """SELECT id_category FROM ps_category_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetProductCategoryName(self, productId):
        sql = """SELECT name FROM ps_category_lang
                LEFT JOIN ps_product
                ON ps_product.id_category_default = ps_category_lang.id_category
                WHERE ps_product.id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def SetProductPriceForAllShops(self, productId,  wholesalePrice = -1, price = -1):
        if wholesalePrice == -1 and price == -1:
            return

        if wholesalePrice != -1:
            if price != -1:
                sql = """UPDATE ps_product_shop
                        SET price = %(price)s, wholesale_price = %(wholesalePrice)s
                        WHERE id_product = %(productId)s"""%{"price":price,"wholesalePrice":wholesalePrice,
                       "productId":productId}
            else:
                sql = """UPDATE ps_product_shop
                    SET wholesale_price = %(wholesalePrice)s
                    WHERE id_product = %(productId)s"""%{"wholesalePrice":wholesalePrice,"productId":productId}

        else:
            sql = """UPDATE ps_product_shop
                    SET price = %(price)s
                    WHERE id_product = %(productId)s"""%{"price":price,"productId":productId}

        self.MakeUpdateQueue(sql)

    def SetProductPriceForShop(self, productId, shopId, wholesalePrice, price):

        if wholesalePrice != -1:
            sql = """UPDATE ps_product_shop
                    SET price = %(price)s, wholesale_price = %(wholesalePrice)s
                    WHERE id_product = %(productId)s AND id_shop = %(shopId)s"""%{"price":price,"wholesalePrice":wholesalePrice,
                   "productId":productId, "shopId":shopId}

        else:
            sql = """UPDATE ps_product_shop
                    SET price = %(price)s
                    WHERE id_product = %(productId)s AND id_shop = %(shopId)s"""%{"price":price,
                   "productId":productId, "shopId":shopId}

        self.MakeUpdateQueue(sql)

    def SetProductCategories(self, productId, categoriesIds):
        sql = """DELETE FROM ps_category_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product
                SET id_category_default = %(defaultCategoryId)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "defaultCategoryId":categoriesIds[0]}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product_shop
                SET id_category_default = %(defaultCategoryId)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "defaultCategoryId":categoriesIds[0]}

        self.MakeUpdateQueue(sql)

        for categoryId in categoriesIds:
            sql = """INSERT INTO ps_category_product(id_category, id_product, position)
                VALUES (%(id_category)s,%(id_product)s,1)"""%{"id_category":categoryId, "id_product":productId}

            self.MakeUpdateQueue(sql)

    def AddProductToCategory(self, productId, categoryId):
        sql = """INSERT INTO ps_category_product(id_category, id_product, position)
                VALUES (%(id_category)s,%(id_product)s,1)"""%{"id_category":categoryId, "id_product":productId}

        self.MakeUpdateQueue(sql)

    def DeleteProductFromCategory(self, productId, categoryId):
        sql = """DELETE FROM ps_category_product
                WHERE id_category = {0} AND id_product = {1}""".format(categoryId, productId)

        self.MakeUpdateQueue(sql)

    def UpdateShopProductData(self, shopID, productID, newProductsData):
        sql = """UPDATE ps_product_lang
                SET link_rewrite = '%(link_rewrite)s', meta_title = '%(meta_title)s', description = '%(description)s',
                meta_keywords = '%(meta_keywords)s', meta_description = '%(meta_description)s'
               WHERE id_shop = %(shopID)s AND id_product = %(productID)s"""%{"shopID":shopID,
               "productID":productID,
               "link_rewrite":newProductsData[0],
               "meta_title":newProductsData[1],
               "description":newProductsData[2],
               "meta_keywords":newProductsData[3],
               "meta_description":newProductsData[4]
               }

        self.MakeUpdateQueue(sql)


    def SetProductOnSale(self, productId, onSale):
        sql = """UPDATE ps_product
                SET on_sale = %(onSale)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "onSale":int(onSale)}
        self.MakeUpdateQueue(sql)
        sql = """UPDATE ps_product_shop
                SET on_sale = %(onSale)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "onSale":int(onSale)}
        self.MakeUpdateQueue(sql)

    def GetProductOnSale(self, productId):
        sql = """SELECT on_sale FROM ps_product
                 WHERE id_product = {}""".format(productId)
        return self.MakeGetInfoQueue(sql)[0][0]

    def SetProductActive(self, productId, active):
        sql = """UPDATE ps_product
                SET active = %(active)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "active":int(active)}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product_shop
                SET active = %(active)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "active":int(active)}

        self.MakeUpdateQueue(sql)

    def SetProductArticle(self, productId, article):
        sql = """UPDATE ps_product
                SET reference = '%(article)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "article":article}

        self.MakeUpdateQueue(sql)

    def SetProductEan13(self, productId, ean13):
        sql = """UPDATE ps_product
                SET ean13 = '%(ean13)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "ean13":ean13}

        self.MakeUpdateQueue(sql)

    def SetProductUpc(self, productId, upc):
        sql = """UPDATE ps_product
                SET upc = '%(upc)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "upc":upc}

        self.MakeUpdateQueue(sql)

    def GetProductUpc(self, productId):
        sql = """SELECT upc FROM ps_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def SetProductUnity(self, productId, unity):
        sql = """UPDATE ps_product
                SET unity = '%(unity)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "unity":unity}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product_shop
                SET unity = '%(unity)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "unity":unity}

        self.MakeUpdateQueue(sql)

    def SetProductUnitPriceRatio(self, productId, unit_price_ratio):
        sql = """UPDATE ps_product_shop
                SET unit_price_ratio = %(unit_price_ratio)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "unit_price_ratio":unit_price_ratio}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product
                SET unit_price_ratio = %(unit_price_ratio)s
                WHERE id_product = %(productId)s"""%{"productId":productId, "unit_price_ratio":unit_price_ratio}

        self.MakeUpdateQueue(sql)

    def SetProductOriginalName(self, productId, originalName):
        sql = """UPDATE ps_product_lang
                SET original_name = '%(originalName)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "originalName":originalName}

        self.MakeUpdateQueue(sql)

    def SetProductName(self, productId, name):
        sql = """UPDATE ps_product_lang
                SET name = '%(name)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "name":name}

        self.MakeUpdateQueue(sql)

    def SetProductMetaTitle(self, productId, meta_title):
        sql = """UPDATE ps_product_lang
                SET meta_title = '%(meta_title)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "meta_title":meta_title}

        self.MakeUpdateQueue(sql)

    def SetProductDescription(self, productId, description):
        sql = """UPDATE ps_product_lang
                SET description = '%(description)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "description":description}

        self.MakeUpdateQueue(sql)

    def SetProductShortDescription(self, productId, description):
        sql = """UPDATE ps_product_lang
                SET description_short = '%(description)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "description":description}

        self.MakeUpdateQueue(sql)

    def SetProductLinkRewrite(self, productId, link_rewrite):
        sql = """UPDATE ps_product_lang
                SET link_rewrite = '%(link_rewrite)s'
                WHERE id_product = %(productId)s"""%{"productId":productId, "link_rewrite":link_rewrite}

        self.MakeUpdateQueue(sql)

    def GetProductOriginalName(self, productId):
        sql = """SELECT original_name, name FROM ps_product_lang
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        try:
            if data[0][0] == "":
                return data[0][1]
            return data[0][0]
        except:
            return -1

    def GetProductName(self, productId):
        sql = """SELECT name FROM ps_product_lang
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductPrice(self, productId):
        sql = """SELECT price FROM ps_product
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetProductMetaTitle(self, productId):
        sql = """SELECT meta_title FROM ps_product_lang
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetProductLinkRewrite(self, productId):
        sql = """SELECT link_rewrite FROM ps_product_lang
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetContractorProductsIds(self, contractorCode):
        sql = """SELECT id_product FROM ps_product
                WHERE reference LIKE '""" + contractorCode + "%'"

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetProductWarehouseName(self, productName):
        productName = self.CleanItemName(productName)

        sql = """SELECT WarehouseName FROM warehouse_moscow
                 WHERE ItemName = '%(productName)s'"""%{"productName":productName}

        data = self.MakeLocalDbGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetProductItemName(self, WarehouseName):
        sql = """SELECT ItemName FROM warehouse_moscow
                 WHERE WarehouseName = '%(WarehouseName)s'"""%{"WarehouseName":WarehouseName}

        data = self.MakeLocalDbGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def IsProductActive(self, productId):
        sql = """SELECT active FROM ps_product_shop
                WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return 0

        return data[0][0]

    def IsProductInBackoffice(self, productId):
        sql = """SELECT * FROM ps_product_shop
                WHERE id_product = %(productId)s and id_shop = 1"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) > 0:
            return True
        else:
            return False

    def IsProductInStock(self, productId):
        sql = """SELECT * FROM ps_stock_available
                 WHERE id_product = %(productId)s"""%{"productId":productId}

        data = self.MakeGetInfoQueue(sql)

        return len(data) > 0

    def GetProductShopQuantity(self, productId, shopId):
        sql = """SELECT quantity FROM ps_stock_available
                WHERE id_product = %(productId)s AND id_shop = %(shopId)s"""%{"productId":productId,
                "shopId":shopId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return 0


    def AddProductAccessory(self, productId, accessoryId):
        sql = """INSERT INTO ps_accessory (id_product_1, id_product_2)
                VALUES (%(productId)s,%(accessoryId)s)"""%{"productId":productId,
                "accessoryId":accessoryId}

        self.MakeUpdateQueue(sql)

    def GetProductAccessory(self, productId):
        sql = """SELECT id_product_2 FROM ps_accessory
                WHERE id_product_1 = {}""".format(productId)
        return [i[0] for i in self.MakeGetInfoQueue(sql)]

    def DeleteProductAccessory(self, productId, accessoryId):
        sql = """DELETE FROM ps_accessory
                WHERE id_product_1 = %(productId)s AND id_product_2 = %(accessoryId)s"""%{"productId":productId,
                "accessoryId":accessoryId}

        self.MakeUpdateQueue(sql)

    def DeleteAllProductAccessories(self, productId):
        sql = """DELETE FROM ps_accessory
                WHERE id_product_1 = %(productId)s"""%{"productId":productId}

        self.MakeUpdateQueue(sql)

    def GetProductMainImage(self, productId):
        sql = """SELECT id_image FROM ps_image
                 WHERE id_product = {} and cover = 1""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetProductMainImage(self, productId, imageId):
        sql = """UPDATE ps_image
                 SET cover = 1
                 WHERE id_product = {1} AND id_image = {0}""".format(imageId, productId)
        self.MakeUpdateQueue(sql)
        sql = """UPDATE ps_image
                 SET cover = 0
                 WHERE id_product = {1} AND id_image <> {0}""".format(imageId, productId)
        self.MakeUpdateQueue(sql)
        images = self.GetProductImagesIds(productId)
        for imageid in images:
            if imageid == imageId:
                sql = """UPDATE ps_image_shop
                         SET cover = 1
                         WHERE id_image = {0}""".format(imageid)
                self.MakeUpdateQueue(sql)
            else:
                sql = """UPDATE ps_image_shop
                         SET cover = 0
                         WHERE id_image = {0}""".format(imageid)
                self.MakeUpdateQueue(sql)

    def SetProductRedirectType(self, productId, redirect):
        sql = """UPDATE ps_product
                 SET redirect_type = '{}'
                 WHERE id_product = {}""".format(redirect, productId)
        self.MakeUpdateQueue(sql)
        sql = """UPDATE ps_product_shop
                 SET redirect_type = '{}'
                 WHERE id_product = {}""".format(redirect, productId)
        self.MakeUpdateQueue(sql)

    def SetProductRedirectedId(self, productId, redirectedId):
        sql = """UPDATE ps_product
                 SET id_product_redirected = {}
                 WHERE id_product = {}""".format(redirectedId, productId)
        self.MakeUpdateQueue(sql)
        sql = """UPDATE ps_product_shop
                 SET id_product_redirected = {}
                 WHERE id_product = {}""".format(redirectedId, productId)
        self.MakeUpdateQueue(sql)

    ###################################
    ######## specific price ###########
    ###################################

    def AddProductSpecificPrice(self, productId, id_specific_price_rule, from_quantity,reduction ):
        if self.siteUrl not in ["http://omsk.knowall.ru.com", "http://safetus.ru"]:
            sql = """INSERT INTO ps_specific_price ( id_specific_price_rule, id_cart, id_product, id_shop, id_shop_group, id_currency, id_country, id_group, id_customer, id_product_attribute, price, from_quantity, reduction, reduction_tax, reduction_type, from, to)
                     VALUES ( {}, 0, {}, 0, 0, 0, 0, 0, 0, 0, -1, {}, {}, 0, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00')""".format(id_specific_price_rule, productId,  from_quantity,reduction)
        else:
            sql = """INSERT INTO ps_specific_price ( id_specific_price_rule, id_cart, id_product, id_shop, id_shop_group, id_currency, id_country, id_group, id_customer, id_product_attribute, price, from_quantity, reduction, reduction_type, from, to)
                     VALUES ( {}, 0, {}, 0, 0, 0, 0, 0, 0, 0, -1, {}, {},'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00')""".format(id_specific_price_rule, productId,  from_quantity,reduction)

        self.MakeUpdateQueue(sql)

    def DeleteProductSpecificPrice(self, productId):
        sql = """DELETE FROM ps_specific_price
                WHERE id_product = %(productId)s"""%{"productId":productId}

        self.MakeUpdateQueue(sql)

    def AddSpecificPriceRule(self, name, from_quantity, reduction):
        if self.siteUrl not in ["http://omsk.knowall.ru.com", "http://safetus.ru"]:
            sql = u"""INSERT INTO ps_specific_price_rule (name, id_shop, id_currency, id_country, id_group, from_quantity, price, reduction, reduction_tax, reduction_type, from,to)
                     VALUES ('{}', 0, 0, 0, 0, {}, -1, {}, 0, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00')""".format(name, from_quantity, reduction)
        else:
            sql = u"""INSERT INTO ps_specific_price_rule (name, id_shop, id_currency, id_country, id_group, from_quantity, price, reduction, reduction_type, from,to)
                     VALUES ('{}', 0, 0, 0, 0, {}, -1, {}, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00')""".format(name, from_quantity, reduction)
        self.MakeUpdateQueue(sql)


    ###################################
    ######### attributes ###########
    ###################################

    def AddProductAttribute(self, id_product, attributeName, attributeValue, wholesale_price, price, default_on, original_name):
        id_attribute = self.GetAttributeId(attributeName, attributeValue)
        if id_attribute == -1:
            return -1
        name, base_price, base_wholesale_price, description, link_rewrite, meta_title = self.GetProductDetailsById(id_product)
        wprice_ = wholesale_price - base_wholesale_price
        price_ = price - base_price
        sql = u"""INSERT INTO ps_product_attribute( id_product, wholesale_price, price, quantity, default_on, original_name)
                  VALUES ({},{},{},3000, {}, '{}')""".format(id_product,wprice_ , price_, default_on, original_name)
        self.MakeUpdateQueue(sql)
        id_product_attribute = self.GetLastInsertedId("ps_product_attribute", "id_product_attribute")
        sql = u"""INSERT INTO ps_product_attribute_combination(id_attribute, id_product_attribute)
                  VALUES ({},{})""".format(id_attribute, id_product_attribute)
        self.MakeUpdateQueue(sql)
        shops = self.GetAllShops()
        for shop in shops:
            id_shop = shop[0]
            sql = u"""INSERT INTO ps_product_attribute_shop(id_product_attribute, id_shop, wholesale_price, price, default_on, available_date)
                      VALUES ({},{},{},{},{},'{}')""".format(id_product_attribute, id_shop, wprice_, price_, default_on, self.GetTodayDate())
            self.MakeUpdateQueue(sql)

    def GetProductAttributesIds(self, id_product):
        sql = u"""SELECT id_product_attribute FROM ps_product_attribute
                  WHERE id_product = {}""".format(id_product)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def DeleteProductAttributes(self, id_product):
        attrsIds = self.GetProductAttributesIds(id_product)
        sql = u"""DELETE FROM ps_product_attribute
                  WHERE id_product = {}""".format(id_product)
        self.MakeUpdateQueue(sql)
        for attrId in attrsIds:
            sql = u"""DELETE FROM ps_product_attribute_shop
                      WHERE id_product_attribute = {}""".format(attrId)
            self.MakeUpdateQueue(sql)

    def GetAttributeId(self, attributeName, attributeValue):
        sql = u"""SELECT ps_attribute.id_attribute FROM ps_attribute
                  LEFT JOIN ps_attribute_group_lang
                  ON ps_attribute_group_lang.id_attribute_group = ps_attribute.id_attribute_group
                  LEFT JOIN ps_attribute_lang
                  ON ps_attribute_lang.id_attribute = ps_attribute.id_attribute
                  WHERE ps_attribute_lang.name = '{}' AND ps_attribute_group_lang.name = '{}'""".format(attributeValue, attributeName)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductAttributeId(self, id_product, attributeId):
        sql = u"""SELECT ps_product_attribute_combination.id_product_attribute FROM ps_product_attribute_combination
                  LEFT JOIN ps_product_attribute
                  ON ps_product_attribute.id_product_attribute = ps_product_attribute_combination.id_product_attribute
                  WHERE ps_product_attribute_combination.id_attribute = {} AND ps_product_attribute.id_product = {}""".format(attributeId, id_product)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductAttributeOriginalName(self, attributeId ):
        sql = u"""SELECT original_name FROM ps_product_attribute
              WHERE id_product_attribute = {}""".format(attributeId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetProductAttributeOriginalName(self, id_product, attributeName, attributeValue, original_name ):
        id_product_attribute = self.GetProductAttributeIdByData(id_product,attributeName, attributeValue)
        if id_product_attribute == -1:
            return False

        sql = u"""UPDATE ps_product_attribute
                  SET original_name = '{1}'
              WHERE id_product_attribute = {0}""".format(id_product_attribute, original_name)
        self.MakeGetInfoQueue(sql)
        return True

    def GetProductAttributeIdByData(self, id_product, attributeName, attributeValue ):
        id_attribute = self.GetAttributeId(attributeName, attributeValue)
        if id_attribute == -1:
            return False
        id_product_attribute = self.GetProductAttributeId(id_product,id_attribute)
        if id_product_attribute == -1:
            return False
        return id_product_attribute

    def SetProductAttributePrice(self, id_product, attributeName, attributeValue, wholesale_price, price):
        id_product_attribute = self.GetProductAttributeIdByData(id_product, attributeName, attributeValue)
        if id_product_attribute == -1:
            return False
        name, base_price, base_wholesale_price, description, link_rewrite, meta_title = self.GetProductDetailsById(id_product)
        wprice_ = wholesale_price - base_wholesale_price
        price_ = price - base_price
        sql = u"""UPDATE ps_product_attribute
                  SET price = {}, wholesale_price = {}
                  WHERE id_product_attribute = {}""".format(price_,wprice_ ,  id_product_attribute)
        self.MakeUpdateQueue(sql)
        sql = u"""UPDATE ps_product_attribute_shop
                  SET price = {}, wholesale_price = {}
                  WHERE id_product_attribute = {}""".format(price_,wprice_ ,  id_product_attribute)
        self.MakeUpdateQueue(sql)

    ######################
    ###### features ######
    ######################

    def DeleteFeature(self, featureId):
        tables = [
        "ps_feature",
        "ps_feature_lang",
        "ps_feature_product",
        "ps_feature_shop",
        "ps_feature_value",
        "ps_layered_indexable_feature"
        ]
        for table in tables:
            sql = """DELETE FROM {}
                     WHERE id_feature = {}""".format(table, featureId)
            self.MakeUpdateQueue(sql)

    def GetFeaturesNames(self):
        sql = """SELECT name FROM ps_feature_lang"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetFeatureId(self, featureName):
        sql = """SELECT id_feature FROM ps_feature_lang
                WHERE name = '%(featureName)s'"""%{"featureName":featureName}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetFeatureName(self, featureId):
        sql = """SELECT name FROM ps_feature_lang
                WHERE id_feature = {0}""".format(featureId)

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetFeatureValues(self, featureName):
        sql = u"""SELECT value FROM ps_feature_value_lang
                LEFT JOIN ps_feature_value
                ON ps_feature_value_lang.id_feature_value = ps_feature_value.id_feature_value
                LEFT JOIN ps_feature_lang
                ON ps_feature_lang.id_feature = ps_feature_value.id_feature
                WHERE name = '{0}'""".format(featureName)

        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetFeatureValueName(self, valueId):
        sql = """SELECT value FROM ps_feature_value_lang
                WHERE id_feature_value = {0}""".format(valueId)

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetFeatureValueId(self, featureId, featureValueName):
        sql = """SELECT ps_feature_value_lang.id_feature_value FROM ps_feature_value_lang
                LEFT JOIN ps_feature_value
                ON ps_feature_value.id_feature_value = ps_feature_value_lang.id_feature_value
                WHERE value = '%(featureValueName)s' AND ps_feature_value.id_feature = %(featureId)s"""%{"featureId":featureId,
                "featureValueName":featureValueName}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def AddProductFeatureValue(self, id_product, featureName, featureValueName):
        id_feature = self.GetFeatureId(featureName)
        id_feature_value = self.GetFeatureValueId(id_feature, featureValueName)

        if id_feature == -1 or id_feature_value == -1:
            return -1

        sql = """INSERT INTO ps_feature_product(id_feature, id_product, id_feature_value)
                 VALUES (%(id_feature)s,%(id_product)s,%(id_feature_value)s)"""%{"id_feature":id_feature,
                "id_product":id_product,"id_feature_value":id_feature_value}

        self.MakeUpdateQueue(sql)

    def AddProductFeatureValueById(self, id_product, id_feature, id_feature_value):
        sql = """SELECT * FROM ps_feature_product
                 WHERE id_product = %(id_product)s
                        AND id_feature_value = %(id_feature_value)s
                        AND id_feature = %(id_feature)s """%{"id_product":id_product,
                        "id_feature":id_feature, "id_feature_value":id_feature_value}

        data = self.MakeGetInfoQueue(sql)

        if len(data) != 0:
            return

        sql = """INSERT INTO ps_feature_product(id_feature, id_product, id_feature_value)
                 VALUES (%(id_feature)s,%(id_product)s,%(id_feature_value)s)"""%{"id_feature":id_feature,
                "id_product":id_product,"id_feature_value":id_feature_value}

        self.MakeUpdateQueue(sql)

    def GetProductFeatures(self, id_product):
        features = {}
        sql = """SELECT DISTINCT id_feature FROM ps_feature_product
                 WHERE id_product = {0} and id_feature <> 0""".format(id_product)
        featuresIds = [x[0] for x in self.MakeGetInfoQueue(sql)]
        for featureId in featuresIds:
            sql = """SELECT id_feature_value FROM ps_feature_product
                 WHERE id_product = {0} and id_feature = {1}""".format(id_product, featureId)
            values = [self.GetFeatureValueName(x[0]) for x in self.MakeGetInfoQueue(sql)]
            features[self.GetFeatureName(featureId)] = values
        return features

    def DeleteProductFeatureValue(self, id_product, featureName, featureValueName):
        id_feature = self.GetFeatureId(featureName)
        id_feature_value = self.GetFeatureValueId(id_feature, featureValueName)

        if id_feature == -1 or id_feature_value == -1:
            return

        sql = """DELETE FROM ps_feature_product
                WHERE id_product = %(id_product)s AND id_feature = %(id_feature)s AND id_feature_value = %(id_feature_value)s"""%{"id_product":id_product,
                "id_feature":id_feature, "id_feature_value":id_feature_value}

        self.MakeUpdateQueue(sql)

    def DeleteProductFeature(self, id_product, featureName):
        id_feature = self.GetFeatureId(featureName)

        if id_feature == -1:
            return

        sql = """DELETE FROM ps_feature_product
                WHERE id_product = %(id_product)s AND id_feature = %(id_feature)s"""%{"id_product":id_product,
                "id_feature":id_feature}
        self.MakeUpdateQueue(sql)

    def DeleteProductFeatureValueById(self, id_product, id_feature, id_feature_value):
        sql = """DELETE FROM ps_feature_product
                WHERE id_product = %(id_product)s AND id_feature = %(id_feature)s AND id_feature_value = %(id_feature_value)s"""%{"id_product":id_product,
                "id_feature":id_feature, "id_feature_value":id_feature_value}

        self.MakeUpdateQueue(sql)

    def DeleteProductFeatures(self, id_product):
        sql = """DELETE FROM ps_feature_product
                WHERE id_product = %(id_product)s"""%{"id_product":id_product}

        self.MakeUpdateQueue(sql)

    def GetAllFeaturesIds(self):
        sql = """SELECT id_feature FROM ps_feature"""

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def AddFeature(self, name):
        pos = self.GetLastInsertedId("ps_feature", "position") + 1
        sql = """INSERT INTO ps_feature( position)
                 VALUES ({0})""".format(pos)
        self.MakeUpdateQueue(sql)
        id_feature = self.GetLastInsertedId("ps_feature", "id_feature")
        sql = u"""INSERT INTO ps_feature_lang(id_feature, id_lang, name)
                  VALUES ({0},1,'{1}')""".format(id_feature, name)
        self.MakeUpdateQueue(sql)
        shopsIds = [x[0] for x in self.GetShops()]
        for shopId in shopsIds:
            sql = """INSERT INTO ps_feature_shop(id_feature, id_shop)
                     VALUES ({0},{1})""".format(id_feature, shopId)
            self.MakeUpdateQueue(sql)
        return id_feature

    def AddFeatureValue(self, id_feature, value):
        sql = """INSERT INTO ps_feature_value(id_feature, custom)
                 VALUES ({0},0)""".format(id_feature)
        self.MakeUpdateQueue(sql)
        id_feature_value = self.GetLastInsertedId("ps_feature_value", "id_feature_value")
        sql = u"""INSERT INTO ps_feature_value_lang(id_feature_value, id_lang, value)
                  VALUES ({0},1,'{1}')""".format(id_feature_value, value)
        self.MakeUpdateQueue(sql)

    def AddFeatureValue2(self, featureName, value):
        id_feature = self.GetFeatureId(featureName)
        if id_feature == -1:
            return -1
        sql = """INSERT INTO ps_feature_value(id_feature, custom)
                 VALUES ({0},0)""".format(id_feature)
        self.MakeUpdateQueue(sql)
        id_feature_value = self.GetLastInsertedId("ps_feature_value", "id_feature_value")
        sql = u"""INSERT INTO ps_feature_value_lang(id_feature_value, id_lang, value)
                  VALUES ({0},1,'{1}')""".format(id_feature_value, value)
        self.MakeUpdateQueue(sql)

    #########################
    ###### attachments ######
    #########################

    def AddProductAttachment(self, id_product, id_attachment):
        sql = """INSERT INTO ps_product_attachment(id_attachment, id_product)
                 VALUES (%(id_attachment)s,%(id_product)s)"""%{"id_attachment":id_attachment,
                "id_product":id_product}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product
                 SET cache_has_attachments = 1
                 WHERE id_product = %(id_product)s"""%{"id_product":id_product}

        self.MakeUpdateQueue(sql)

    def DeleteProductAttachment(self, id_product, id_attachment):
        sql = """DELETE FROM ps_product_attachment
                 WHERE id_product = %(id_product)s AND id_attachment = %(id_attachment)s"""%{"id_attachment":id_attachment,
                "id_product":id_product}

        self.MakeUpdateQueue(sql)

    def DeleteProductAttachments(self, id_product):
        sql = """DELETE FROM ps_product_attachment
                 WHERE id_product = %(id_product)s"""%{"id_product":id_product}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product
                 SET cache_has_attachments = 0
                 WHERE id_product = %(id_product)s"""%{"id_product":id_product}

        self.MakeUpdateQueue(sql)


    def AddAttachment(self, file, name, fileSize, mime ):
        sql = u"""INSERT INTO ps_attachment (file, file_name, file_size, mime)
                 VALUES ('{0}','{1}', {2}, '{3}')""".format(file, file, fileSize, mime)
        self.MakeUpdateQueue(sql)
        id_attachment = self.GetLastInsertedId("ps_attachment", "id_attachment")
        sql = u"""INSERT INTO ps_attachment_lang (id_attachment, id_lang, name, description)
                 VALUES ({0},{1}, '{2}', '{3}')""".format(id_attachment, 1, name, "")
        self.MakeUpdateQueue(sql)

        return id_attachment

    #########################
    ###### search index #####
    #########################

    def GetShopProductIndexed(self,id_shop, id_product):
        sql = """SELECT indexed FROM ps_product_shop
                WHERE id_product = %(id_product)s AND id_shop = %(id_shop)s"""%{"id_shop":id_shop,
                "id_product":id_product}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetShopProductActive(self,id_shop, id_product):
        sql = """SELECT active FROM ps_product_shop
                WHERE id_product = %(id_product)s AND id_shop = %(id_shop)s"""%{"id_shop":id_shop,
                "id_product":id_product}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetShopProductPrice(self,id_shop, id_product):
        sql = """SELECT price FROM ps_product_shop
                WHERE id_product = {} AND id_shop = {}""".format(id_shop, id_product)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductIndexed(self, id_product):
        sql = """SELECT indexed FROM ps_product
                WHERE id_product = %(id_product)s"""%{"id_product":id_product}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def SetShopProductIndexed(self,id_shop, id_product, indexed):
        sql = """UPDATE ps_product_shop
                SET indexed = %(indexed)s
                WHERE id_product = %(id_product)s AND id_shop = %(id_shop)s"""%{"id_shop":id_shop,
                "id_product":id_product, "indexed":indexed}

        data = self.MakeUpdateQueue(sql)

    def SetProductIndexed(self, id_product, indexed):
        sql = """UPDATE ps_product
                SET indexed = %(indexed)s
                WHERE id_product = %(id_product)s"""%{"id_product":id_product, "indexed":indexed}

        data = self.MakeUpdateQueue(sql)

    def SetProductIndexedForAllShops(self, id_product, indexed):
        sql = """UPDATE ps_product
                SET indexed = %(indexed)s
                WHERE id_product = %(id_product)s"""%{"id_product":id_product, "indexed":indexed}

        data = self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_product_shop
                SET indexed = %(indexed)s
                WHERE id_product = %(id_product)s"""%{"id_product":id_product, "indexed":indexed}

        data = self.MakeUpdateQueue(sql)

    def IsProductInSearchIndex(self, id_product):
        sql = """SELECT * FROM  ps_search_index
                WHERE id_product = %(id_product)s"""%{"id_product":id_product}

        data = self.MakeGetInfoQueue(sql)

        if len(data) > 0:
            return True
        else:
            return False

    def GetIndexedProducts(self):
        sql = """SELECT distinct id_product FROM ps_search_index"""

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def AddVideoToProductDescription(self, id_product, videoCode):
        if videoCode.find("youtube.com") != -1:
            videoCode = videoCode.replace("https://www.youtube.com/watch?v=", "")
        descr = self.GetProductDescription(id_product)
        if descr.find("videos_1") == -1:
            descr += "<p><br/><br/></p>"
        code = """<p class="videos_1" style="text-align: center;"><iframe width="560" height="315" src="https://www.youtube.com/embed/{0}?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></p>""".format(videoCode)
        descr += code
        self.SetProductDescription(id_product, descr)

    def AddMediaToProduct(self, id_product, id_shop, label, url_media, Type):
        sql = u"""INSERT INTO ps_product_media(id_product, id_shop, label, url_media, type)
                 VALUES ({0},{1},'{2}','{3}','{4}')""".format(id_product, id_shop, label, url_media, Type)

        self.MakeUpdateQueue(sql)

    def DeleteVideoFromProductDescription(self, id_product, videoCode):
        if videoCode.find("youtube.com") != -1:
            videoCode = videoCode.replace("https://www.youtube.com/watch?v=", "")
        description = self.GetProductDescription(id_product)
        if description.find("videos_1") == -1:
            return

        ldElement = etree.HTML(description)
        elems = ldElement.xpath("//p[.//iframe[contains(@src, '{0}')]]".format(videoCode))
        for element in elems:
            try:
                element.getparent().remove(element)
            except:
                pass

        description = etree.tostring(ldElement)
        description = description.replace("<html>","")
        description = description.replace("</html>","")
        description = description.replace("<body>","")
        description = description.replace("</body>","")
        description = description.replace("allowfullscreen=\"\"/>","allowfullscreen=\"\"></iframe>")
        self.SetProductDescription(id_product, description)

    def DeleteAllVideosFromProductDescription(self, id_product):
        description = self.GetProductDescription(id_product)
        if description.find("videos_1") == -1:
            return

        ldElement = etree.HTML(description)
        elems = ldElement.xpath("//p[.//iframe]")
        for element in elems:
            try:
                element.getparent().remove(element)
            except:
                pass

        description = etree.tostring(ldElement)
        description = description.replace("<html>","")
        description = description.replace("</html>","")
        description = description.replace("<body>","")
        description = description.replace("</body>","")
        description = description.replace("allowfullscreen=\"\"/>","allowfullscreen=\"\"></iframe>")
        self.SetProductDescription(id_product, description)

    def GetProductWholesalePrice(self, name):
        sql = u"""SELECT wholesale_price FROM products
                  WHERE name = '{0}'""".format(name)

        data = self.MakeLocalDbGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return int(data[0][0])

    def DeleteProduct(self, id):
        sqls = []
        sqls.append("""DELETE FROM ps_category_product
                 WHERE id_product = {0}""".format(id))
        sqls.append("""DELETE FROM ps_feature_product
                 WHERE id_product = {0}""".format(id))
        sqls.append("""DELETE FROM ps_product
                 WHERE id_product = {0}""".format(id))
        sqls.append("""DELETE FROM ps_product_attribute
                 WHERE id_product = {0}""".format(id))
        sqls.append("""DELETE FROM ps_product_lang
                 WHERE id_product = {0}""".format(id))
        sqls.append("""DELETE FROM ps_product_shop
                 WHERE id_product = {0}""".format(id))
        sql = """SELECT id_image FROM ps_image
                 WHERE id_product = {0}""".format(id)
        pics = [x[0] for x in self.MakeGetInfoQueue(sql)]
        for pic in pics:
            sqls.append( """DELETE FROM ps_image
                     WHERE id_image = {0}""".format(pic))
            sqls.append( """DELETE FROM ps_image_shop
                     WHERE id_image = {0}""".format(pic))
            sqls.append( """DELETE FROM ps_image_lang
                     WHERE id_image = {0}""".format(pic))

        for sql in sqls:
            self.MakeUpdateQueue(sql)

    def SetProductVideo(self, id, video):
        sql = """UPDATE ps_product_lang
                 SET video = '{0}'
                 WHERE id_product = {1}""".format(video, id)
        self.MakeUpdateQueue(sql)

    def SetProductEquipment(self, id, equipment):
        sql = """UPDATE ps_product_lang
                 SET equipment = '{0}'
                 WHERE id_product = {1}""".format(equipment, id)
        self.MakeUpdateQueue(sql)

    def SetProductCharacteristics(self, id, characteristics):
        sql = u"""UPDATE ps_product_lang
                 SET characteristics = '{0}'
                 WHERE id_product = {1}""".format(characteristics, id)
        self.MakeUpdateQueue(sql)

    def GetProductVideo(self, id):
        sql = """SELECT video FROM ps_product_lang
                 WHERE id_product = {0}""".format(id)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductEquipment(self, id):
        sql = """SELECT equipment FROM ps_product_lang
                 WHERE id_product = {0}""".format(id)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductCharacteristics(self, id):
        sql = """SELECT characteristics FROM ps_product_lang
                 WHERE id_product = {0}""".format(id)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetProductData(self, shopId, id, newProductData):
        sql = """UPDATE ps_product_lang
                SET link_rewrite = '%(link_rewrite)s', meta_title = '%(meta_title)s',
                meta_keywords = '%(meta_keywords)s', meta_description = '%(meta_description)s'
               WHERE id_shop = %(shopID)s AND id_product = %(productID)s"""%{"shopID":shopId,
               "productID":id,
               "link_rewrite":newProductData[0],
               "meta_title":newProductData[1],
               "meta_keywords":newProductData[2],
               "meta_description":newProductData[3]
               }

        self.MakeUpdateQueue(sql)

    ###########################################################################
    ############################### CATEGORIES ################################
    ###########################################################################

    def GetCategoryID(self, name):
        sql = "SELECT id_category FROM ps_category_lang WHERE name = '%(catName)s'"%{"catName":name}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def GetCategoryId(self, name, parentId):
        sql = u"""SELECT ps_category.id_category FROM ps_category
                  LEFT JOIN ps_category_lang
                  ON ps_category.id_category = ps_category_lang.id_category
                  WHERE ps_category_lang.name = '{0}' AND ps_category.id_parent = {1}""".format(name, parentId)
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def UpdateCategoryData(self, shopID, categoryID, newCategoryData):
        if len(newCategoryData) == 6:
            sql = """UPDATE ps_category_lang
                SET link_rewrite = '%(link_rewrite)s', meta_title = '%(meta_title)s',
                description = '%(description)s',long_description = '%(long_description)s',
                meta_keywords = '%(meta_keywords)s', meta_description = '%(meta_description)s'
               WHERE id_shop = %(shopID)s AND id_category = %(categoryID)s"""%{"shopID":shopID,
               "categoryID":categoryID,
               "link_rewrite":newCategoryData[0],
               "meta_title":newCategoryData[1],
               "description":newCategoryData[2],
               "long_description":newCategoryData[3],
               "meta_keywords":newCategoryData[4],
               "meta_description":newCategoryData[5]
               }
        else:
            sql = """UPDATE ps_category_lang
                SET link_rewrite = '%(link_rewrite)s', meta_title = '%(meta_title)s',
                description = '%(description)s',long_description = '%(long_description)s',
                meta_keywords = '%(meta_keywords)s', meta_description = '%(meta_description)s',
                articles = '%(articles)s'
               WHERE id_shop = %(shopID)s AND id_category = %(categoryID)s"""%{"shopID":shopID,
               "categoryID":categoryID,
               "link_rewrite":newCategoryData[0],
               "meta_title":newCategoryData[1],
               "description":newCategoryData[2],
               "long_description":newCategoryData[3],
               "meta_keywords":newCategoryData[4],
               "meta_description":newCategoryData[5],
               "articles":newCategoryData[6]
               }

        self.MakeUpdateQueue(sql)

    def SetCategoryData(self, shopId, categoryId, newCategoryData):
        sql = """UPDATE ps_category_lang
                SET link_rewrite = '%(link_rewrite)s', meta_title = '%(meta_title)s',
                meta_keywords = '%(meta_keywords)s', meta_description = '%(meta_description)s'
               WHERE id_shop = %(shopID)s AND id_category = %(categoryID)s"""%{"shopID":shopId,
               "categoryID":categoryId,
               "link_rewrite":newCategoryData[0],
               "meta_title":newCategoryData[1],
               "meta_keywords":newCategoryData[2],
               "meta_description":newCategoryData[3]
               }

        self.MakeUpdateQueue(sql)

    def GetCategoryProductsIds(self, categoryId):
        sql = """SELECT id_product FROM ps_category_product
                WHERE id_category = %(categoryId)s"""%{"categoryId":categoryId}

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetCategoryActiveProductsIds(self, categoryId):
        sql = """SELECT ps_category_product.id_product FROM ps_category_product
                LEFT JOIN ps_product
                ON ps_product.id_product = ps_category_product.id_product
                WHERE ps_category_product.id_category = %(categoryId)s and ps_product.active = 1"""%{"categoryId":categoryId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return []

        return [x[0] for x in data]

    def GetCategorySubcategoriesIds(self, categoryId):
        sql = """SELECT id_category FROM ps_category
                WHERE id_parent = {0}""".format(categoryId)

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetCategoryActiveSubcategoriesIds(self, categoryId):
        sql = """SELECT id_category FROM ps_category
                WHERE id_parent = {0} and active = 1""".format(categoryId)

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetShopMainCategories(self, shopId):
        sql = """SELECT ps_category_lang.id_category, ps_category_lang.name, ps_category_lang.link_rewrite FROM ps_category_lang
                LEFT JOIN ps_category
                 ON ps_category.id_category = ps_category_lang.id_category
                WHERE ps_category_lang.id_shop = %(shopId)s AND ps_category.id_parent = 2 AND ps_category.active = 1"""%{"shopId":shopId}

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetShopActiveCategories(self, shopId):
        sql = """SELECT ps_category_lang.id_category, ps_category_lang.name, ps_category_lang.link_rewrite FROM ps_category_lang
                LEFT JOIN ps_category
                 ON ps_category.id_category = ps_category_lang.id_category
                WHERE ps_category_lang.id_shop = %(shopId)s AND ps_category.active = 1"""%{"shopId":shopId}

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetActiveCategoriesIds(self):
        sql = """SELECT id_category FROM ps_category
                 WHERE active = 1"""

        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetShopActiveGoogleCategories(self, shopId):
        sql = u"""SELECT ps_category_lang.id_category, ps_category_lang.name, ps_category_lang.link_rewrite FROM ps_category_lang
                LEFT JOIN ps_category
                 ON ps_category.id_category = ps_category_lang.id_category
                WHERE ps_category_lang.id_shop = {0} AND ps_category.active = 1 AND ps_category_lang.link_rewrite like '%-купить-%'""".format(shopId)

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetShopActiveNotGoogleCategories(self, shopId):
        sql = u"""SELECT ps_category_lang.id_category, ps_category_lang.name, ps_category_lang.link_rewrite FROM ps_category_lang
                LEFT JOIN ps_category
                 ON ps_category.id_category = ps_category_lang.id_category
                WHERE ps_category_lang.id_shop = {0} AND ps_category.active = 1 AND ps_category_lang.link_rewrite not like '%-купить-%'""".format(shopId)

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetShopCategoryDetails(self, shopId, categoryId):
        sql = """SELECT id_category, link_rewrite, meta_title, description, name, long_description, meta_keywords, meta_description
                FROM ps_category_lang
                WHERE id_shop = %(shopId)s AND id_category = %(categoryId)s """%{"shopId":shopId, "categoryId":categoryId}
        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data

    def IsCategoryActive(self, categoryId):
        sql = """SELECT active
                FROM ps_category
                WHERE id_category = %(categoryId)s """%{"categoryId":categoryId}
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetCategoryParentsNames(self, categoryId):
        parents = []
        parentId = self.GetCategoryParent(categoryId)
        while parentId != 0:
            parents.append(parentId)
            parentId = self.GetCategoryParent(parentId)
        parentsNames = [self.GetCategoryName(i) for i in parents]
        return parentsNames

    def GetCategoryArticles(self, shopId, categoryId):
        sql = """SELECT articles
                FROM ps_category_lang
                WHERE id_category = %(categoryId)s AND id_shop = %(shopId)s"""%{"categoryId":categoryId, "shopId":shopId}

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return ""

        return data[0][0]

    def GetCategoryDescription(self, categoryId):
        sql = """SELECT description FROM ps_category_lang
                WHERE id_category = {0}""".format(categoryId)

        data = self.MakeGetInfoQueue(sql)

        if len(data) == 0:
            return -1

        return data[0][0]

    def SetCategoryDescription(self, categoryId, description):
        sql = u"""UPDATE ps_category_lang
                SET description = '{0}'
                WHERE id_category = {1}""".format(description, categoryId)

        data = self.MakeUpdateQueue(sql)

    def SetCategoryLongDescription(self, categoryId, long_description):
        sql = u"""UPDATE ps_category_lang
                SET long_description = '{0}'
                WHERE id_category = {1}""".format(long_description, categoryId)

        data = self.MakeUpdateQueue(sql)

    def SetShopCategoryDescription(self, shopId, categoryId, description):
        sql = u"""UPDATE ps_category_lang
                SET description = '{0}'
                WHERE id_category = {1} and id_shop = {2}""".format(description, categoryId, shopId)

        data = self.MakeUpdateQueue(sql)

    def GetShopCategoryLongDescription(self, shopId, categoryId):
        sql = u"""SELECT long_description FROM ps_category_lang
                WHERE id_category = {0} and id_shop = {1}""".format(categoryId, shopId)

        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return ""

    def GetShopCategoryLinkRewrite(self, shopId, categoryId):
        sql = u"""SELECT link_rewrite FROM ps_category_lang
                WHERE id_category = {0} and id_shop = {1}""".format(categoryId, shopId)

        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return ""

    def GetShopCategoryDescription(self, shopId, categoryId):
        sql = u"""SELECT description FROM ps_category_lang
                WHERE id_category = {0} and id_shop = {1}""".format(categoryId, shopId)

        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return ""

    def SetShopCategoryLongDescription(self, shopId, categoryId, long_description):
        sql = u"""UPDATE ps_category_lang
                SET long_description = '{0}'
                WHERE id_category = {1} and id_shop = {2}""".format(long_description, categoryId, shopId)

        data = self.MakeUpdateQueue(sql)

    def SetShopCategoryMetaTitle(self, shopId, categoryId, meta_title):
        sql = u"""UPDATE ps_category_lang
                SET meta_title = '{0}'
                WHERE id_category = {1} and id_shop = {2}""".format(meta_title, categoryId, shopId)

        data = self.MakeUpdateQueue(sql)

    def SetShopCategoryLinkRewrite(self, shopId, categoryId, link_rewrite):
        sql = u"""UPDATE ps_category_lang
                SET link_rewrite = '{0}'
                WHERE id_category = {1} and id_shop = {2}""".format(link_rewrite, categoryId, shopId)

        data = self.MakeUpdateQueue(sql)

    def SetCategoryArticles(self,shopId,  categoryId, articles):
        sql = """UPDATE ps_category_lang
                SET articles = '%(articles)s'
               WHERE id_shop = %(shopId)s AND id_category = %(categoryId)s"""%{"shopId":shopId, "categoryId":categoryId,"articles":articles}

        self.MakeUpdateQueue(sql)

    def SetCategoryName(self, categoryId, name):
        sql = """UPDATE ps_category_lang
                SET name = '%(name)s'
               WHERE id_category = %(categoryId)s"""%{"categoryId":categoryId,"name":name}

        self.MakeUpdateQueue(sql)

    def SetCategoryActive(self, categoryId, active):
        sql = """UPDATE ps_category
                SET active = {1}
                WHERE id_category = {0}""".format(categoryId, active)

        self.MakeUpdateQueue(sql)

    def SetCategoryParent(self, categoryId, parentId):
        sql = """UPDATE ps_category
                SET id_parent = {1}
                WHERE id_category = {0}""".format(categoryId, parentId)

        self.MakeUpdateQueue(sql)

    def SetCategoryLinkRewrite(self, categoryId, link_rewrite):
        sql = u"""UPDATE ps_category_lang
                SET link_rewrite = '{0}'
               WHERE id_category = {1}""".format(link_rewrite, categoryId)

        self.MakeUpdateQueue(sql)

    def SetCategoryMetaTitle(self, categoryId, meta_title):
        sql = u"""UPDATE ps_category_lang
                SET meta_title = '{0}'
               WHERE id_category = {1}""".format(meta_title, categoryId)

        self.MakeUpdateQueue(sql)

    def GetCategoryName(self, categoryId):
        sql = """SELECT name FROM ps_category_lang
                 WHERE id_category = %(categoryId)s"""%{"categoryId":categoryId}

        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetCategoryLinkRewrite(self, categoryId):
        sql = """SELECT link_rewrite FROM ps_category_lang
                 WHERE id_category = %(categoryId)s"""%{"categoryId":categoryId}

        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def DeleteCategory(self, categoryId):
        sql = """DELETE FROM ps_category
                 WHERE id_category = {0}""".format(categoryId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_category_lang
                 WHERE id_category = {0}""".format(categoryId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_category_product
                 WHERE id_category = {0}""".format(categoryId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_category_shop
                 WHERE id_category = {0}""".format(categoryId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_layered_category
                 WHERE id_category = '{0}'""".format(categoryId)
        self.MakeUpdateQueue(sql)

    def DeleteCategoryWithSubcategories(self, categoryId):
        subcats = self.GetCategorySubcategoriesIds(categoryId)
        self.DeleteCategory(categoryId)
        for subcat in subcats:
            self.DeleteCategoryWithSubcategories(subcat)

    def GetCategoryUrl(self, categoryId, shopId):
        sql = """SELECT link_rewrite FROM ps_category_lang
                 WHERE id_category = %(categoryId)s and id_shop = %(shopId)s"""%{"categoryId":categoryId, "shopId":shopId}

        try:
            return str(categoryId) + "-" + self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetCategoryFullUrl(self, categoryId, shopId):
        catUrl = self.GetCategoryUrl(categoryId, shopId)
        if catUrl == -1:
            return -1
        url = u"{}/{}".format(self.GetShopMainDomain(shopId), catUrl)
        return url

    def SetCategoryUpdateDate(self, categoryId, date):
        sql = """UPDATE ps_category
                SET date_upd = '{0}'
                WHERE id_category = {1}""".format(date, categoryId)

        self.MakeUpdateQueue(sql)

    def SetCategoriesUpdateDate(self, date):
        sql = """UPDATE ps_category
                SET date_upd = '{0}'""".format(date)

        self.MakeUpdateQueue(sql)

    def GetCategoryParent(self, categoryId):
        sql = u"""SELECT id_parent FROM ps_category
                  WHERE id_category = {0}""".format(categoryId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1



    ##########################################################################
    ############################### CUSTOMERS ################################
    ##########################################################################

    def GetCustomerCoupons(self, customerId, couponName):
        sql = """SELECT ps_cart_rule_lang.name, ps_cart_rule.code, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount, ps_cart_rule.active, ps_cart_rule.date_to FROM ps_cart_rule
                 LEFT JOIN ps_cart_rule_lang
                 ON ps_cart_rule.id_cart_rule = ps_cart_rule_lang.id_cart_rule
                 WHERE id_customer = """ + str(customerId) + """ AND ps_cart_rule_lang.name LIKE '%""" + couponName + """%'"""

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetCustomerInfo(self, customerId):
        sql = """SELECT firstname, lastname FROM ps_customer
                 WHERE id_customer = %(customerId)s"""%{"customerId":customerId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0]
        except:
            return -1

    def GetCustomerOrdersIds(self, customerId):
        sql = """SELECT id_order FROM ps_orders
                 WHERE id_customer = %(customerId)s"""%{"customerId":customerId}
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetCustomersForPeriod(self, startDate, endDate):
        sql = """SELECT id_customer, firstname, lastname, id_shop FROM ps_customer
                 WHERE date_add < '%(endDate)s' AND date_add > '%(startDate)s'"""%{"startDate":startDate,
                 "endDate":endDate}

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetCustomerAddressData(self, customerId):
        sql = """SELECT id_address, city, phone, phone_mobile FROM ps_address
                 WHERE id_customer = %(customerId)s"""%{"customerId":customerId}
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0]

    def GetCustomerId(self, email):
        sql = """SELECT id_customer FROM ps_customer
                 WHERE email = '{}'""".format(email)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetCartProductsIdsByAddressId(self, addressId):
        sql = """SELECT id_product, quantity FROM ps_cart_product
                 WHERE id_address_delivery = %(addressId)s"""%{"addressId":addressId}

        data = self.MakeGetInfoQueue(sql)

        return data

    def DeleteCustomer(self, customerId):
##        sql = """SELECT id_customer_thread FROM ps_customer_thread
##                 WHERE id_customer = {0}""".format(customerId)
##        data = self.MakeGetInfoQueue(sql)
##        if len(data) > 0:
##            id_customer_thread = data[0][0]
##            sql = """DELETE FROM ps_customer_message
##                    WHERE id_customer_thread = {0}""".format(id_customer_thread)
##            self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer
                 WHERE id_customer = {0}""".format(customerId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_address
                 WHERE id_customer = {0}""".format(customerId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer_group
                 WHERE id_customer = {0}""".format(customerId)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer_thread
                 WHERE id_customer = {0}""".format(customerId)
        self.MakeUpdateQueue(sql)

    def DeleteAllCustomers(self):
        sql = """DELETE FROM ps_customer"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer_group"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer_message"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer_thread"""
        self.MakeUpdateQueue(sql)

    def GetCustomerThreads(self, id_customer, date_add):
        sql = """SELECT * FROM ps_customer_thread
                 WHERE id_customer = {0} AND date_add < '{1}'""".format(id_customer, date_add)
        data = self.MakeGetInfoQueue(sql)
        return data

    def DeleteCustomerThread(self, id_customer_thread):
        sql = """DELETE FROM ps_customer_message
                 WHERE id_customer_thread = {0}""".format(id_customer_thread)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_customer_thread
                 WHERE id_customer_thread = {0}""".format(id_customer_thread)
        self.MakeUpdateQueue(sql)

    def DeleteGuests(self):
        sql = """DELETE FROM ps_guest"""
        self.MakeUpdateQueue(sql)

    def SetCustomerEmail(self, customerId, email):
        sql = u"""UPDATE ps_customer
                  SET email = '{}'
                  WHERE id_customer = {}""".format(email, customerId)
        self.MakeUpdateQueue(sql)

    def GetAllCustomers(self):
        sql = """SELECT id_customer FROM ps_customer"""
        return [i[0] for i in self.MakeGetInfoQueue(sql)]

    def GetCustomerData(self, customerId):
        sql = u"""SELECT ps_customer.firstname, ps_customer.lastname, email, phone_mobile, ps_shop.name FROM ps_customer
                  LEFT JOIN ps_shop
                  ON ps_shop.id_shop = ps_customer.id_shop
                  LEFT JOIN ps_address
                  ON ps_address.id_customer = ps_customer.id_customer
                  WHERE ps_customer.id_customer = {}""".format(customerId)
        return self.MakeGetInfoQueue(sql)[0]

    #######################################################################
    ############################### ORDERS ################################
    #######################################################################

    def GetOrderData(self, orderId):
        sql = u"""SELECT """
        return self.MakeGetInfoQueue(sql)[0]


    def GetOrderState(self, orderId):
        sql = """SELECT osl.name FROM ps_order_state_lang as osl
                 LEFT JOIN ps_orders as ord
                 ON ord.current_state = osl.id_order_state
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetOrderStates(self, orderId):
        sql = """SELECT id_order_state FROM ps_order_history
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return [x[0] for x in data]
        except:
            return -1

    def GetOrderStatesNames(self, orderId):
        sql = """SELECT name FROM ps_order_history
                LEFT JOIN ps_order_state_lang
                ON ps_order_history.id_order_state =  ps_order_state_lang.id_order_state
                WHERE id_order = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return [x[0] for x in data]
        except:
            return -1

    def GetOrderStateDate(self, orderId, state):
        stateId = self.GetOrderStateId(state)
        sql = """SELECT date_add FROM ps_order_history
                 WHERE id_order = %(orderId)s AND id_order_state = %(id_order_state)s"""%{"orderId":orderId,
                 "id_order_state":stateId}
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[-1][0]
        except:
            return -1

    def GetOrderShopName(self, orderId):
        sql = """SELECT ps.name FROM ps_shop as ps
                 LEFT JOIN ps_order_detail as od
                 ON ps.id_shop = od.id_shop
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetOrderShop(self, orderId, shopName):
        shopId = self.GetShopID(shopName)

        if shopId == -1:
            return -1

        sql = """UPDATE ps_orders
                 SET id_shop = {0}
                 WHERE id_order = {1}""".format(shopId, orderId)

        data = self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_order_detail
                 SET id_shop = {0}
                 WHERE id_order = {1}""".format(shopId, orderId)

        data = self.MakeUpdateQueue(sql)

    def GetOrderShopUrl(self, orderId):
        sql = """SELECT * FROM ps_shop_url as psu
                 LEFT JOIN ps_order_detail as od
                 ON psu.id_shop = od.id_shop
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)
        if data[0][5] != "":
            return data[0][2] + "/" + data[0][5]

        return data[0][2]

    def GetOrderShopId(self, orderId):
        sql = """SELECT psu.id_shop FROM ps_shop_url as psu
                 LEFT JOIN ps_order_detail as od
                 ON psu.id_shop = od.id_shop
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetOrderCustomer(self, orderId):
        sql = """SELECT ps_customer.firstname, ps_customer.lastname, email, ps_customer.id_customer FROM ps_customer
                 LEFT JOIN ps_orders
                 ON ps_customer.id_customer = ps_orders.id_customer
                 WHERE ps_orders.id_order = %(orderId)s"""%{"orderId":orderId}
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0]
        except:
            return -1

    def GetOrderDeliveryData(self, orderId):
        sql = """SELECT address1, address2, city, phone, phone_mobile, other, id_address FROM ps_address
                 LEFT JOIN ps_orders
                 ON ps_address.id_address = ps_orders.id_address_delivery
                 WHERE ps_orders.id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0]
        except:
            return -1

    def GetOrderDeliveryCost(self, orderId):
        sql = """SELECT total_shipping FROM ps_orders
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetOrderTotalCost(self, orderId):
        sql = """SELECT total_paid FROM ps_orders
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetOrderCarrierName(self, orderId):
        sql = """SELECT pc.name FROM ps_carrier as pc
                 LEFT JOIN ps_order_carrier as oc
                 ON pc.id_carrier = oc.id_carrier
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetOrderCarrierDelay(self, orderId):
        sql = """SELECT pc.delay FROM ps_carrier_lang as pc
                 LEFT JOIN ps_order_carrier as oc
                 ON pc.id_carrier = oc.id_carrier
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetOrderPaymentMethod(self, orderId):
        order_reference = self.GetOrderReference(orderId)

        sql = """SELECT payment_method FROM ps_order_payment
                 WHERE order_reference = '%(order_reference)s'"""%{"order_reference":order_reference}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetOrderItems(self, orderId):
        sql = """SELECT product_id, product_name, unit_price_tax_incl, product_quantity, purchase_supplier_price, product_attribute_id  FROM ps_order_detail
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}
        data = self.MakeGetInfoQueue(sql)
        return data

    def GetOrderNote(self, orderId):
        sql = """SELECT c.note FROM ps_customer as c
                 LEFT JOIN ps_orders as ord
                 ON ord.id_customer = c.id_customer
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            note = data[0][0]
        except:
            return ""

        if note == None:
            return ""

        return note

    def SetCustomerNote(self, customerId, note):
        sql = """UPDATE ps_customer
                 SET note = '%(note)s'
                 WHERE id_customer = %(customerId)s"""%{"customerId":customerId, "note":note}

        self.MakeUpdateQueue(sql)

    def GetOrderReference(self, orderId):
        sql = """SELECT reference FROM ps_orders
                 WHERE ps_orders.id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def SetOrderTrackingNumber(self, orderId, trackingNumber):
        sql = """UPDATE ps_order_carrier
                 SET tracking_number = %(trackingNumber)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId, "trackingNumber":trackingNumber}

        self.MakeUpdateQueue(sql)

    def GetOrderTrackingNumber(self, orderId):
        sql = """SELECT tracking_number FROM ps_order_carrier
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0].strip()
        except:
            return -1

    def GetOrderIdByTrackingNumber(self, trackingNumber):
        sql = """SELECT id_order FROM ps_order_carrier
                 WHERE tracking_number = '%(tracking_number)s'"""%{"tracking_number":trackingNumber}

        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetOrderStateId(self, stateName):
        sql = u"""SELECT id_order_state FROM ps_order_state_lang
                 WHERE name = '%(stateName)s'"""%{"stateName":stateName}
        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def SetOrderState(self, orderId, stateName):
        stateId = self.GetOrderStateId(stateName)
        now = datetime.datetime.now()

        sql = """UPDATE ps_orders
                 SET current_state = %(stateId)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId,
                 "stateId":stateId}

        self.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_order_history(id_employee, id_order, id_order_state, date_add)
                 VALUES (0,%(orderId)s,%(id_order_state)s,'%(now)s')"""%{"orderId":orderId,
                 "id_order_state":stateId,
                 "now":now}

        self.MakeUpdateQueue(sql)

    def GetOrdersIds(self, states, startDate, endDate, notInState = False):
        if notInState == False:

            statesCond = "("

            for state in states:
                stateId = self.GetOrderStateId(state)
                statesCond += "current_state = " + str(stateId) + " or "

        else:

            statesCond = "NOT ("

            for state in states:
                stateId = self.GetOrderStateId(state)
                statesCond += "current_state = " + str(stateId) + " or "

        statesCond = statesCond[0:len(statesCond)-3] + ")"

        sql = """SELECT id_order FROM ps_orders
                 WHERE date_add > '%(startDate)s' AND date_add < '%(endDate)s'
                 AND %(statesCond)s ORDER BY date_add"""%{"startDate":startDate, "endDate":endDate, "statesCond":statesCond }

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetAllOrdersIds(self):
        sql = """SELECT id_order FROM ps_orders"""
        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]

    def GetOrderCreationDate(self, orderId):
        sql = """SELECT date_add FROM ps_orders
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        return data[0][0]

    def GetOrderDiscountSum(self, orderId):
        sql = """SELECT value FROM ps_order_cart_rule
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        discountSum = 0
        for discount in data:
            discountSum += int(discount[0])

        return discountSum

    def SetOrderCarrier(self, orderId, carrierId):
        sql = """UPDATE ps_orders
                 SET id_carrier = %(carrierId)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId,
                 "carrierId":carrierId}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_order_carrier
                 SET id_carrier = %(carrierId)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId,
                 "carrierId":carrierId}

        self.MakeUpdateQueue(sql)

    def SetOrderCarrierByName(self, orderId, carrierName):
        shopId = self.GetOrderShopId(orderId)

        carrierId = self.GetShopCarrierIdByName(shopId, carrierName)

        if carrierId == -1:
            return False

        sql = """UPDATE ps_orders
                 SET id_carrier = %(carrierId)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId,
                 "carrierId":carrierId}

        self.MakeUpdateQueue(sql)

        sql = """UPDATE ps_order_carrier
                 SET id_carrier = %(carrierId)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId,
                 "carrierId":carrierId}

        self.MakeUpdateQueue(sql)

        return True

    def SetOrderDeliveryCost(self, orderId, deliveryCost):

        sql = """SELECT total_products FROM ps_orders
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId}

        data = self.MakeGetInfoQueue(sql)

        total_products = data[0][0]
        total_sum = deliveryCost + total_products

        sql = """UPDATE ps_orders
                 SET total_paid = %(total_sum)s,
                     total_paid_tax_incl = %(total_sum)s,
                     total_paid_tax_excl = %(total_sum)s,
                     total_paid_real = %(total_sum)s,
                     total_shipping = %(deliveryCost)s,
                     total_shipping_tax_incl = %(deliveryCost)s,
                     total_shipping_tax_excl = %(deliveryCost)s
                 WHERE id_order = %(orderId)s"""%{"orderId":orderId, "deliveryCost":deliveryCost,
                 "total_sum":total_sum}

        self.MakeUpdateQueue(sql)

    def SetOrderAddressCity(self, orderId, city):
        sql = u"""UPDATE ps_address
                 LEFT JOIN ps_orders
                 ON ps_orders.id_address_delivery = ps_address.id_address
                 SET ps_address.city = '{1}'
                 WHERE ps_orders.id_order = {0}""".format(orderId, city)

        self.MakeUpdateQueue(sql)

    def DeleteOrder(self, id):
        reference = self.GetOrderReference(id)
        sql = """DELETE FROM ps_orders
                 WHERE id_order = {0}""".format(id)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_carrier
                 WHERE id_order = {0}""".format(id)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_detail
                 WHERE id_order = {0}""".format(id)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_history
                 WHERE id_order = {0}""".format(id)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_payment
                 WHERE order_reference = '{0}'""".format(reference)
        self.MakeUpdateQueue(sql)

    def DeleteAllOrders(self):
        sql = """DELETE FROM ps_orders"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_carrier"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_detail"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_history"""
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_order_payment"""
        self.MakeUpdateQueue(sql)

    def GetOrderCustomerMessage(self, orderId):
        sql = """SELECT message FROM ps_customer_message
                 LEFT JOIN ps_customer_thread
                 ON ps_customer_thread.id_customer_thread = ps_customer_message.id_customer_thread
                 WHERE ps_customer_thread.id_order = {0}""".format(orderId)

        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return ""

    def AddToOrderNote(self, orderId, text):
        customerId = self.GetOrderCustomer(orderId)[3]
        note = self.GetOrderNote(orderId)
        note += text
        self.SetCustomerNote(customerId, note)

    def DeleteFromOrderNote(self, orderId, text):
        customerId = self.GetOrderCustomer(orderId)[3]
        note = self.GetOrderNote(orderId)
        note = note.replace(text,"")
        self.SetCustomerNote(customerId, note)


    ######################################################################
    ############################### OTHER ################################
    ######################################################################

    def CreateCartRule(self, cartRuleParams):
        today = datetime.date.today()

        if "reduction_amount" in cartRuleParams.keys():
            cartRuleParams["reduction_percent"] = 0
        else:
            cartRuleParams["reduction_amount"] = 0

        sql = """INSERT INTO ps_cart_rule(id_customer, date_from, date_to, description, quantity, quantity_per_user, priority, partial_use, code, minimum_amount, minimum_amount_tax, minimum_amount_currency, minimum_amount_shipping, country_restriction, carrier_restriction, group_restriction, cart_rule_restriction, product_restriction, shop_restriction, free_shipping, reduction_percent, reduction_amount, reduction_tax, reduction_currency, reduction_product, gift_product, gift_product_attribute, highlight, active, date_add, date_upd)
                VALUES (%(id_customer)s,'%(date_from)s','%(date_to)s',%(description)s,1,1,1,0,'%(code)s',0,0,1,0,0,0,0,0,0,0,0,%(reduction_percent)s,%(reduction_amount)s,0,1,0,0,0,0,%(active)s,'%(date)s','%(date)s')"""%{"id_customer":cartRuleParams["id_customer"],
                "date_from":cartRuleParams["date_from"],
                "date_to":cartRuleParams["date_to"],
                "description":"description",
                "code":cartRuleParams["code"],
                "reduction_percent":cartRuleParams["reduction_percent"],
                "reduction_amount":cartRuleParams["reduction_amount"],
                "active":cartRuleParams["active"],
                "date":today,
                }

        self.MakeUpdateQueue(sql)

        sql = """INSERT INTO ps_cart_rule_lang(id_lang, name)
                VALUES (1,'%(ruleName)s')"""%{"ruleName":cartRuleParams["name"]}

        self.MakeUpdateQueue(sql)

    def DeactivateCarrier(self, shopId, carrierName):
        id_carrier = self.GetShopCarrierIdByName(shopId, carrierName)
        sql = """UPDATE ps_carrier
                 SET active = 0
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)

    def DeactivateCarrierById(self, id_carrier):
        sql = """UPDATE ps_carrier
                 SET active = 0
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)

    def ActivateCarrier(self, shopId, carrierName):
        id_carrier = self.GetShopCarrierIdByName(shopId, carrierName)
        sql = """UPDATE ps_carrier
                 SET active = 1
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)


    def DeleteCarrier(self, shopId, carrierName):
        id_carrier = self.GetShopCarrierIdByName(shopId, carrierName)
        sql = """DELETE FROM ps_carrier
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_carrier_lang
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_carrier_shop
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_carrier_shop
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)

    def DeleteCarrierById(self, id_carrier):
        sql = """DELETE FROM ps_carrier
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_carrier_lang
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_carrier_shop
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_carrier_shop
                 WHERE id_carrier = {0}""".format(id_carrier)
        self.MakeUpdateQueue(sql)

    def GetUnpublishedReviewsCustomers(self):
        sql = """SELECT id_customer FROM ps_product_comment
                 WHERE validate = 0"""

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetTodayReviewsCustomers(self):
        sql = """SELECT id_customer FROM ps_product_comment
                 WHERE date_add > '%(today)s'"""%{"today":datetime.date.today() - datetime.timedelta(days=1)}

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetUnvalidatedReviewsCustomers(self):
        sql = """SELECT id_customer FROM ps_product_comment
                 WHERE validate = 0"""

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetValidatedReviewsCustomers(self):
        sql = """SELECT id_customer FROM ps_product_comment
                 WHERE validate = 1"""

        data = self.MakeGetInfoQueue(sql)

        return data

    def ActivateCoupon(self, coupon):
        sql = """UPDATE ps_cart_rule
                 SET active = 1
                 WHERE code = '%(coupon)s'"""%{"coupon":coupon}

        self.MakeUpdateQueue(sql)

        sql = """INSERT INTO activated_coupons(siteUrl, coupon)
                 VALUES ('%(siteUrl)s','%(coupon)s')"""%{"siteUrl":self.siteUrl,"coupon":coupon}

        self.MakeLocalDbUpdateQueue(sql)

    def GetIsCouponActivated(self, coupon ):
        sql = """SELECT coupon FROM activated_coupons
                 WHERE coupon = '%(coupon)s' AND siteUrl = '%(siteUrl)s'"""%{"coupon":coupon, "siteUrl":self.siteUrl}

        data = self.MakeLocalDbGetInfoQueue(sql)

        if len(data) == 0:
            return False
        else:
            return True

    def GetActiveCoupons(self):
        sql = """SELECT code, id_customer, date_to, reduction_amount, reduction_percent, date_from FROM ps_cart_rule
                 WHERE active = 1"""

        data = self.MakeGetInfoQueue(sql)

        return data

    def GetCouponReminderDate(self, code):
        sql = """SELECT date FROM emails
                 WHERE siteUrl = '%(site)s' AND code = '%(code)s'"""%{"site":self.siteUrl,
                  "code":code}

        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def SetCouponReminderDate(self, code, date):
        couponDate = self.GetCouponReminderDate(code)

        if couponDate != -1:

            sql = """UPDATE emails
                     SET date = '%(date)s'
                     WHERE siteUrl = '%(siteUrl)s' AND code = '%(code)s'"""%{"siteUrl":self.siteUrl,"date":date,"code":code}

            self.MakeLocalDbUpdateQueue(sql)

        else:

            sql = """INSERT INTO emails(siteUrl, mode, code, date)
                     VALUES ('%(siteUrl)s','coupon_reminder', '%(code)s', '%(date)s')"""%{"siteUrl":self.siteUrl,
                     "code": code, "date":date}

            self.MakeLocalDbUpdateQueue(sql)


    def SetOrderSMSDate(self, orderId, mode, date):
        smsDate = self.GetOrderSMSDate(orderId, mode)

        if smsDate != -1:

            sql = """UPDATE sms
                     SET lastSMSDate = '%(today)s'
                     WHERE siteUrl = '%(siteUrl)s' AND orderId = %(orderId)s AND mode = '%(mode)s'"""%{"today":date,
                     "siteUrl":self.siteUrl, "orderId":orderId, "mode":mode}

            self.MakeLocalDbUpdateQueue(sql)

        else:

            sql = """INSERT INTO sms(siteUrl, orderId, mode, lastSMSDate)
                     VALUES ('%(siteUrl)s',%(orderId)s, '%(mode)s', '%(lastSMSDate)s')"""%{"siteUrl":self.siteUrl,
                     "orderId":orderId, "mode": mode, "lastSMSDate":date}

            self.MakeLocalDbUpdateQueue(sql)

    def GetOrderSMSDate(self, orderId, mode):
        sql = """SELECT lastSMSDate FROM sms
                 WHERE siteUrl = '%(site)s' AND orderId = %(orderId)s AND mode = '%(mode)s'"""%{"site":self.siteUrl,
                  "orderId":orderId, "mode":mode}

        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetMailingIgnoreCustomers(self):
        sql = """SELECT customerId FROM ignored_customers
                 WHERE siteUrl = '%(siteUrl)s'"""%{"siteUrl":self.siteUrl}

        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            return [x[0] for x in data]
        except:
            return []

    def AddCustomerToIgnoreList(self, customerId):
        sql = """INSERT INTO ignored_customers (siteUrl, customerId)
                 VALUES ( '{0}', {1})""".format( self.siteUrl,customerId)

        data = self.MakeLocalDbUpdateQueue(sql)

    def GetWarehouseProductQuantity(self, warehouse, productName):
        itemName = self.CleanItemName(productName)

        sql = u"""SELECT ItemQuantity FROM %(warehouse)s
                 WHERE ItemName = '%(itemName)s'"""%{"warehouse":warehouse,
                  "itemName":itemName}
        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            num = data[0][0]
            if num == -1:
                num = 0
            return num
        except:
            return -1

    def GetItemNameBySkuId(self, Sku_id):
        sql = """SELECT ItemName FROM warehouse_moscow
                 WHERE Sku_id = '%(Sku_id)s'"""%{"Sku_id":Sku_id}

        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetProductContractor(self, name):
        sql = u"""SELECT contractor FROM products
                 WHERE name = '{0}'""".format(name)
        data = self.MakeLocalDbGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetSkuIdByItemName(self, itemName):
        sql = """SELECT Sku_id FROM warehouse_moscow
                 WHERE ItemName = '%(itemName)s'"""%{"itemName":itemName}

        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetWarehouseProducts(self, warehouse):
        sql = """SELECT * FROM %(warehouse)s"""%{"warehouse":warehouse}
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def DecreaseWarehouseProductQuantity(self, warehouse, productName, quantity):
        itemName = self.CleanItemName(productName)
        itemQuantity = self.GetWarehouseProductQuantity(warehouse,itemName)
        qty = itemQuantity - quantity
        if warehouse == "warehouse_spb" and qty < 0:
            qty = 0

        sql = """UPDATE %(warehouse)s
                 SET ItemQuantity = %(quantity)s
                 WHERE ItemName = '%(itemName)s'"""%{"warehouse":warehouse,
                  "itemName":itemName, "quantity": qty }

        self.MakeLocalDbUpdateQueue(sql)

    def IncreaseWarehouseProductQuantity(self, warehouse, productName, quantity):
        itemName = self.CleanItemName(productName)
        itemQuantity = self.GetWarehouseProductQuantity(warehouse,itemName)

        sql = """UPDATE %(warehouse)s
                 SET ItemQuantity = %(quantity)s
                 WHERE ItemName = '%(itemName)s'"""%{"warehouse":warehouse,
                  "itemName":itemName, "quantity":itemQuantity + quantity }

        self.MakeLocalDbUpdateQueue(sql)

    def SetWarehouseProductQuantity(self, warehouse, sku, quantity):
        sql = """UPDATE %(warehouse)s
                 SET ItemQuantity = %(quantity)s
                 WHERE WarehouseName = '%(sku)s'"""%{"warehouse":warehouse,
                  "sku":sku, "quantity":quantity }

        self.MakeLocalDbUpdateQueue(sql)

    def SetProductSkuId(self, warehouseName, Sku_id):
        sql = """UPDATE warehouse_moscow
                 SET Sku_id = '{0}'
                 WHERE WarehouseName = '{1}'""".format(Sku_id, warehouseName)
        self.MakeLocalDbUpdateQueue(sql)


    def GetPostings(self, date = None):
        sql = """SELECT * FROM postings"""
        if date != None:
            sql = """SELECT * FROM postings
                     WHERE date > '{0}'""".format(date)

        data = self.MakeLocalDbGetInfoQueue(sql)

        return data

    def AddPosting(self, postingId, items):
        sql = u"""INSERT INTO postings(date, postingId, items)
                 VALUES ('{0}', '{1}', '{2}')""".format(datetime.date.today(), postingId, items)

        data = self.MakeLocalDbUpdateQueue(sql)

    def GetRegistries(self):
        sql = """SELECT * FROM registries"""

        data = self.MakeLocalDbGetInfoQueue(sql)

        return [x[1] for x in data]

    def AddRegistryId(self, registryId):
        sql = """INSERT INTO registries(registryId)
                 VALUES ('%(id)s')"""%{"id":registryId}

        data = self.MakeLocalDbUpdateQueue(sql)

    def GetIsRefundProcessed(self, statementCode):
        sql = u"""SELECT * FROM refunds
                 WHERE statementCode = '{0}'""".format(statementCode)

        data = self.MakeLocalDbGetInfoQueue(sql)
        return len(data) > 0

    def AddRefund(self, params):
        sql = u"""INSERT INTO refunds(date, statementCode, items, invoice)
                 VALUES ('{0}', '{1}', '{2}', '{3}')""".format(params["date"], params["statementCode"],params["items"],params["invoice"])

        self.MakeLocalDbUpdateQueue(sql)

    def GetRefundsParams(self, date):
        sql = """SELECT * FROM refunds
                 WHERE date >= '{0}'""".format(date)

        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def SetRefundReplySended(self, statementCode, sended):
        sql = u"""UPDATE refunds
                 SET replySended = {1}
                 WHERE statementCode = '{0}'""".format(statementCode, sended )

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundPostingId(self, statementCode, postingId):
        sql = u"""UPDATE refunds
                 SET postingId = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, postingId )

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundReturnInvoice(self, statementCode, returnInvoice):
        sql = u"""UPDATE refunds
                 SET returnInvoice = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, returnInvoice )

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundState(self, statementCode, state):
        sql = u"""UPDATE refunds
                 SET state = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, state)

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundPostingId(self, statementCode, postingId):
        sql = u"""UPDATE refunds
                 SET postingId = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, postingId )

        self.MakeLocalDbUpdateQueue(sql)

    def DeleteRefund(self, statementCode):
        sql = u"""DELETE FROM refunds
                 WHERE statementCode = '{0}'""".format(statementCode)

        self.MakeLocalDbUpdateQueue(sql)

    def GetDeliveries(self):
        sql = """SELECT * FROM deliveries"""

        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def GetDeliveriesByDate(self, date):
        sql = """SELECT * FROM deliveries
                 WHERE date > '{0}'""".format(date)

        data = self.MakeLocalDbGetInfoQueue(sql)
        return data


    def SetDeliveryState(self, id, state):
        sql = u"""UPDATE deliveries
                 SET state = '{0}'
                 WHERE id = {1}""".format(state, id)

        self.MakeLocalDbUpdateQueue(sql)

    def SetDeliveryInvoice(self, id, invoice):
        sql = u"""UPDATE deliveries
                 SET invoice = '{0}'
                 WHERE id = {1}""".format(invoice, id)

        self.MakeLocalDbUpdateQueue(sql)

    def SetDeliveryPostingId(self, id, postingId):
        sql = u"""UPDATE deliveries
                 SET postingId = '{1}'
                 WHERE id = {0}""".format(id, postingId )

        self.MakeLocalDbUpdateQueue(sql)

    def SetDeliveryAcceptance(self, id, acceptance):
        sql = u"""UPDATE deliveries
                 SET acceptance = {1}
                 WHERE id = {0}""".format(id, acceptance )

        self.MakeLocalDbUpdateQueue(sql)

    def DeleteDelivery(self, id):
        sql = u"""DELETE FROM deliveries
                 WHERE id = {0}""".format(id)

        self.MakeLocalDbUpdateQueue(sql)

    def GetRussianPostRefunds(self):
        sql = """SELECT * FROM post_refunds"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def AddRussianPostRefund(self, params):
        sql = u"""INSERT INTO post_refunds(date, orderReference, items, trackingNumber, state)
                  VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')""".format(params["date"], params["orderReference"],params["items"], params["trackingNumber"],params["state"])

        self.MakeLocalDbUpdateQueue(sql)

    def SetRussianPostRefundState(self, trackingNumber, state):
        sql = u"""UPDATE post_refunds
                 SET state = '{0}'
                 WHERE trackingNumber = '{1}'""".format(state, trackingNumber)

        self.MakeLocalDbUpdateQueue(sql)

    def IsPageExists(self, link_rewrite):
        sql = """SELECT id_cms FROM ps_cms_lang
                 WHERE link_rewrite = '%(link_rewrite)s'"""%{"link_rewrite":link_rewrite}

        data = self.MakeGetInfoQueue(sql)

        if len(data) > 0:
            return True
        else:
            return False

    def AddMainPageText(self, id_shop, content):
        sql = """INSERT INTO ps_info(id_shop)
                 VALUES ({0})""".format(id_shop)
        self.MakeUpdateQueue(sql)

        sql = u"""INSERT INTO ps_info_lang(id_lang, text)
                 VALUES (1,'{0}')""".format(content)
        self.MakeUpdateQueue(sql)

    def GetPageContent(self, link_rewrite):
        sql = """SELECT content FROM ps_cms_lang
                 WHERE link_rewrite = '%(link_rewrite)s'"""%{"link_rewrite":link_rewrite}

        data = self.MakeGetInfoQueue(sql)

        if len(data) > 0:
            return data[0][0]
        else:
            return False

    def GetPageParams(self, id_cms):
        sql = """SELECT * FROM ps_cms_lang
                 WHERE id_cms = {0}""".format(id_cms)

        return self.MakeGetInfoQueue(sql)[0]

    def SetPageContent(self, link_rewrite, content):
        sql = """UPDATE ps_cms_lang
                SET content = '%(content)s'
                 WHERE link_rewrite = '%(link_rewrite)s'"""%{"content":content, "link_rewrite":link_rewrite}

        data = self.MakeUpdateQueue(sql)

    def GetShopPageContent(self, id_shop, link_rewrite):
        sql = """SELECT ps_cms_lang.content FROM ps_cms_lang
                 LEFT JOIN ps_cms_shop
                 ON ps_cms_shop.id_cms = ps_cms_lang.id_cms
                 WHERE ps_cms_lang.link_rewrite = '%(link_rewrite)s'"""%{"link_rewrite":link_rewrite}

        data = self.MakeGetInfoQueue(sql)

        if len(data) > 0:
            return data[0][0]
        else:
            return -1

    def GetShopPages(self, id_shop):
        sql = """SELECT meta_title, meta_description, meta_keywords, content, link_rewrite FROM ps_cms_lang
                 LEFT JOIN ps_cms_shop
                 ON ps_cms_shop.id_cms = ps_cms_lang.id_cms
                 LEFT JOIN ps_cms
                 ON ps_cms_lang.id_cms = ps_cms.id_cms
                 WHERE ps_cms_shop.id_shop = {} AND active = 1""".format(id_shop)

        data = self.MakeGetInfoQueue(sql)
        if len(data) > 0:
            return data
        else:
            return -1

    def SetShopPageContent(self, id_shop, link_rewrite, content):
        sql = """SELECT ps_cms_lang.id_cms FROM ps_cms_lang
                 LEFT JOIN ps_cms_shop
                 ON ps_cms_shop.id_cms = ps_cms_lang.id_cms
                 WHERE ps_cms_lang.link_rewrite = '%(link_rewrite)s'"""%{"link_rewrite":link_rewrite}

        data = self.MakeGetInfoQueue(sql)

        if len(data) > 0:
            id_cms = data[0][0]
        else:
            return -1

        sql = """UPDATE ps_cms_lang
                SET content = '%(content)s'
                 WHERE id_cms = '%(id_cms)s'"""%{"content":content, "id_cms":id_cms}

        data = self.MakeUpdateQueue(sql)

    def GetShopDefaultPageTags(self, id_shop, page):
        sql = """SELECT title, description, keywords, url_rewrite FROM ps_meta_lang
                 LEFT JOIN ps_meta
                 ON ps_meta.id_meta = ps_meta_lang.id_meta
                 WHERE id_shop = {0} and page = '{1}'""".format(id_shop, page)

        try:
            return self.MakeGetInfoQueue(sql)[0]
        except:
            return -1

    def SetShopDefaultPageTag(self, id_shop, page, tag, value):
        sql = u"""UPDATE ps_meta_lang
                 LEFT JOIN ps_meta
                 ON ps_meta.id_meta = ps_meta_lang.id_meta
                 SET {0} = '{1}'
                 WHERE id_shop = {2} and ps_meta.page = '{3}'""".format(tag, value, id_shop, page)
        self.MakeUpdateQueue(sql)

    ####################################
    ########### ARTIСLES ###############
    ####################################

    def GetArticle(self, articleId):
        sql = """SELECT ps_smart_blog_post_lang.meta_title,
                        ps_smart_blog_post_lang.short_description,
                        ps_smart_blog_post_lang.content,
                        ps_smart_blog_post_lang.link_rewrite,
                        ps_smart_blog_post.id_category FROM ps_smart_blog_post_lang
                LEFT JOIN ps_smart_blog_post
                ON ps_smart_blog_post_lang.id_smart_blog_post = ps_smart_blog_post.id_smart_blog_post
                 WHERE ps_smart_blog_post_lang.id_smart_blog_post = %(articleId)s"""%{"articleId":articleId}

        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0]
        except:
            return -1

    def GetArticleData(self, articleId):
        sql = """SELECT ps_smart_blog_post_lang.id_smart_blog_post,
                        ps_smart_blog_post_lang.meta_title,
                        ps_smart_blog_post_lang.content,
                        ps_smart_blog_post_lang.link_rewrite,
                        ps_smart_blog_post_lang.short_description,
                        ps_smart_blog_post.created
                        FROM ps_smart_blog_post_lang
                LEFT JOIN ps_smart_blog_post
                ON ps_smart_blog_post_lang.id_smart_blog_post = ps_smart_blog_post.id_smart_blog_post
                 WHERE ps_smart_blog_post_lang.id_smart_blog_post = {}""".format(articleId)

        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0]
        except:
            return -1

    def GetShopCategoriesArticles(self, id_shop):
        sql = """SELECT id_category, articles FROM ps_category_lang
                 WHERE id_shop = {0}""".format(id_shop)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data
        except:
            return -1

    def GetShopArticles(self, id_shop):
        sql = """SELECT id_smart_blog_post FROM ps_smart_blog_post_shop
                 WHERE id_shop = {0}""".format(id_shop)
        data = self.MakeGetInfoQueue(sql)
        try:
            return [x[0] for x in data]
        except:
            return -1

    def GetArticleContent(self, articleId):
        sql = """SELECT content FROM ps_smart_blog_post_lang
                 WHERE id_smart_blog_post = %(articleId)s"""%{"articleId":articleId}
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetArticleContentById(self, articleId, content):
        sql = u"""UPDATE ps_smart_blog_post_lang
                 SET content = '{0}'
                 WHERE id_smart_blog_post = {1}""".format(content, articleId)
        self.MakeUpdateQueue(sql)

    def GetArticlesIds(self):
        sql = """SELECT id_smart_blog_post
                 FROM ps_smart_blog_post_lang"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def SetArticleContent(self, articleId, content):
        sql = u"""UPDATE ps_smart_blog_post_lang
                 SET content = '{0}'
                 WHERE id_smart_blog_post = {1}""".format(content, articleId)
        self.MakeUpdateQueue(sql)

    def DeleteArticleFromShop(self, articleId, shopId):
        sql = """DELETE FROM ps_smart_blog_post_shop
                WHERE id_shop = %(shopId)s AND id_smart_blog_post = %(articleId)s"""%{"articleId":articleId, "shopId":shopId}
        self.MakeUpdateQueue(sql)

    def DeleteArticle(self, articleId):
        sql = """DELETE FROM ps_smart_blog_post_shop
                WHERE id_smart_blog_post = %(articleId)s"""%{"articleId":articleId}
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_smart_blog_post
                WHERE id_smart_blog_post = %(articleId)s"""%{"articleId":articleId}
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_smart_blog_post_lang
                WHERE id_smart_blog_post = %(articleId)s"""%{"articleId":articleId}
        self.MakeUpdateQueue(sql)

    def GetArticleId(self, title, id_shop):
        sql = u"""SELECT ps_smart_blog_post_shop.id_smart_blog_post FROM ps_smart_blog_post_lang
                  LEFT JOIN ps_smart_blog_post_shop
                  ON ps_smart_blog_post_lang.id_smart_blog_post = ps_smart_blog_post_shop.id_smart_blog_post
                  WHERE id_shop = {0} AND meta_title = '{1}'""".format(id_shop, title)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetArticleUpdateDate(self, articleId, date):
        sql = """UPDATE ps_smart_blog_post
                 SET modified = '{0}'
                 WHERE id_smart_blog_post = {1}""".format(date, articleId)
        self.MakeUpdateQueue(sql)

    def GetSmartBlogCategoryId(self, name):
        sql = u"""SELECT id_smart_blog_category FROM ps_smart_blog_category_lang
                  WHERE meta_title = '{0}'""".format(name)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetSmartBlogCategoriesNames(self):
        sql = u"""SELECT meta_title FROM ps_smart_blog_category_lang"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetSmartBlogCategoryUrl(self, name):
        sql = u"""SELECT * FROM ps_smart_blog_category_lang
                  WHERE meta_title = '{0}'""".format(name)
        data = self.MakeGetInfoQueue(sql)[0]
        url = u"{0}/smartblog/category/{1}_{2}.html".format(self.GetShopMainDomain(1),data[0], data[-1])
        return url

    def GetArticleUrl(self, shopId, theme):
        try:
            articleId = self.GetArticleId(theme, shopId)
            link_rewrite = self.GetArticle(articleId)[3]
            return u"smartblog/{0}_{1}.html".format(articleId, link_rewrite)
        except:
            return -1

    def GetArticleUrlById(self, articleId):
        try:
            link_rewrite = self.GetArticle(articleId)[3]
            return u"smartblog/{0}_{1}.html".format(articleId, link_rewrite)
        except:
            return -1

    def GetArticleUrl(self, shopId, theme):
        try:
            articleId = self.GetArticleId(theme, shopId)
            link_rewrite = self.GetArticle(articleId)[3]
            return u"smartblog/{0}_{1}.html".format(articleId, link_rewrite)
        except:
            return -1

    def GetArticleViews(self, articleId):
        sql = u"""SELECT viewed
                  FROM ps_smart_blog_post
                  WHERE id_smart_blog_post = {}""".format(articleId)
        data = self.MakeGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    ####################################
    ########### Block layered  #########
    ####################################

    def GetLayeredCategoriesIds(self):
        sql = """SELECT distinct id_category FROM ps_layered_category"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def DeleteLayeredCategories(self):
        sql = """DELETE FROM ps_layered_category"""
        self.MakeUpdateQueue(sql)

    def DeleteLayeredFilterShops(self):
        sql = """DELETE FROM ps_layered_filter_shop"""
        self.MakeUpdateQueue(sql)

    def GetShopLayeredFilterPriceCategories(self, shopId):
        sql = """SELECT id_category FROM ps_layered_category
                 WHERE id_shop = {} AND type = 'price'""".format(shopId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def GetShopLayeredFilterFeatures(self, shopId, id_category):
        sql = """SELECT id_value FROM ps_layered_category
                 WHERE id_shop = {} AND id_category = {} AND type = 'id_feature'""".format(shopId, id_category)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    ####################################
    ########### Sdek ###################
    ####################################

    def GetSdekCityCode(self, name):
        sql = u"""SELECT id FROM sdek_cities_codes
                  WHERE city_name = '{0}'""".format(name)
        data = self.MakeLocalDbGetInfoQueue(sql)

        if len(data) == 0:
            sql = u"""SELECT id FROM sdek_cities_codes
                  WHERE full_name like '%{0}%'""".format(name)
            data = self.MakeLocalDbGetInfoQueue(sql)
        return [str(x[0]) for x in data]

    def GetCityBySdekCode(self, code):
        sql = u"""SELECT city_name FROM sdek_cities_codes
                  WHERE id = {0}""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetCityFromId(self, id):
        sql = """SELECT city_name FROM sdek_cities_codes
                 WHERE id = {0}""".format(id)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    ####################################
    ########### Requests ###############
    ####################################

    def GetRequests(self):
        sql = u"""SELECT * FROM requests"""
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def AddRequest(self, contractor):
        sql = u"""INSERT INTO requests(contractor)
                  VALUES ('{0}')""".format(contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def SetRequestSended(self, contractor, sended):
        sql = u"""UPDATE requests
                  SET emailSended = {0}
                  WHERE contractor = '{1}'""".format(sended, contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def GetRequestPaid(self, contractor):
        sql = u"""SELECT paid FROM requests
                  WHERE contractor = '{1}'""".format(paid, contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def SetRequestPaid(self, contractor, paid):
        sql = u"""UPDATE requests
                  SET paid = {0}
                  WHERE contractor = '{1}'""".format(paid, contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteAllRequests(self):
        sql = u"""DELETE FROM requests"""
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteRequest(self, id):
        sql = u"""DELETE FROM requests
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)


    ####################################
    ########### Emloyees ###############
    ####################################

    def AddEmployee(self, firstname, lastname, email):
        if self.siteUrl not in ["http://omsk.knowall.ru.com", "http://safetus.ru"]:
            passwd = 'fb20eb33e9e8d5beb5e620a9e2fadd15'
            sql = u"""INSERT INTO ps_employee(id_profile, id_lang, lastname, firstname, email, passwd, last_passwd_gen, stats_date_from, stats_date_to, stats_compare_from, stats_compare_to, stats_compare_option, preselect_date_range, bo_color, bo_theme, bo_css, default_tab, bo_width, bo_menu, active, optin, id_last_order, id_last_customer_message, id_last_customer, last_connection_date)
            VALUES (4,1,'{0}','{1}','{2}','{3}','2019-09-16 03:58:17','2019-08-16','2019-08-16','0000-00-00','0000-00-00',1,'','','default','admin-theme.css',1,0,1,1,1,0,0,0,'2019-09-16')""".format(lastname,firstname, email, passwd)
        else:
            passwd = '075f13a3db4329ce02a089cd3ce8261e'
            sql = u"""INSERT INTO ps_employee(id_profile, id_lang, lastname, firstname, email, passwd, last_passwd_gen, stats_date_from, stats_date_to, stats_compare_from, stats_compare_to, stats_compare_option, preselect_date_range, bo_color, bo_theme, bo_css, default_tab, bo_width, bo_menu, active, optin, id_last_order, id_last_customer_message, id_last_customer)
            VALUES (4,1,'{0}','{1}','{2}','{3}','2019-09-16 03:58:17','2019-08-16','2019-08-16','0000-00-00','0000-00-00',1,'','','default','admin-theme.css',1,0,1,1,1,0,0,0)""".format(lastname,firstname, email, passwd)
        self.MakeUpdateQueue(sql)
        id_employee = self.GetLastInsertedId('ps_employee', 'id_employee')
        shops = [x[0] for x in self.GetAllShops()]
        for id_shop in shops:
            sql = """INSERT INTO ps_employee_shop (id_employee, id_shop)
                     VALUES ({0}, {1})""".format(id_employee, id_shop)
            self.MakeUpdateQueue(sql)

    def DeleteEmployee(self, email):
        sql = """SELECT id_employee FROM ps_employee
                 WHERE email = '{0}'""".format(email)
        try:
            id_employee = self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1
        sql = """DELETE FROM ps_employee
                 WHERE id_employee = {0}""".format(id_employee)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM ps_employee_shop
                 WHERE id_employee = {0}""".format(id_employee)
        self.MakeUpdateQueue(sql)

        return True

    ####################################
    ########### Aliexpress ###############
    ####################################

    def GetAliexpressOrders(self):
        sql = u"""SELECT * FROM aliexpress"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetAliexpressOrderState(self, trackNum, state):
        sql = u"""UPDATE aliexpress
                  SET state = '{0}'
                  WHERE trackingCode = '{1}'""".format(state, trackNum)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteAliexpressOrder(self, trackNum):
        sql = u"""DELETE FROM aliexpress
                  WHERE trackingCode = '{0}'""".format(trackNum)
        self.MakeLocalDbUpdateQueue(sql)


    ####################################
    ########### Reviews ################
    ####################################

    def GetLocalReviews(self):
        sql = """SELECT * FROM reviews"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetProductLocalReviews(self, name):
        sql = u"""SELECT * FROM reviews
                 WHERE productName = '{0}'""".format(name)
        return self.MakeLocalDbGetInfoQueue(sql)

    def AddLocalReview(self, review):
        sql = u"""INSERT INTO reviews(productName, site, author, text, grade, title, date)
              VALUES ('{0}','{1}','{2}','{3}',{4},'{5}','{6}')""" \
            .format(review["name"], review["site"], review["author"], review["content"],review["grade"], review["title"], review["date"] )
        self.MakeLocalDbUpdateQueue(sql)

    def GetProductReviews(self, name):
        id_product = self.GetProductIdQuick(name)
        if id_product == -1:
            return -1
        sql = u"""SELECT * FROM ps_product_comment
                  WHERE id_product = {0}""".format(id_product)
        return self.MakeGetInfoQueue(sql)

    def GetProductReviewsById(self, id_product):
        sql = u"""SELECT * FROM ps_product_comment
                  WHERE id_product = {0}""".format(id_product)
        return self.MakeGetInfoQueue(sql)

    def AddProductReview(self, id_product, review):
        id, productName, site, author, text,grade, title, date = review
        sql = u"""INSERT INTO ps_product_comment(id_product, id_customer, id_guest, title, content, customer_name, grade, validate, deleted, date_add)
                  VALUES ({0},0,0,'{1}','{2}','{3}',{4},1,0, '{5}')""" \
                  .format(id_product, title,text, u"{0} ({1})".format( author, site),grade, date)
        self.MakeUpdateQueue(sql)

    def IsExistingProductReview(self, id_product, review):
        id, productName, site, author, text,grade, title, date = review
        author_ = u"{0} ({1})".format( author, site)
        sql = u"""SELECT * FROM ps_product_comment
                  WHERE customer_name = '{0}' AND id_product = {1} AND date_add = '{2}'""".format(author_, id_product, date)
        return len(self.MakeGetInfoQueue(sql)) > 0

    def IsExistingLocalProductReview(self, review):
        productName, site, author, text, grade, title, date = review
        sql = u"""SELECT * FROM reviews
                  WHERE productName = '{0}' AND site = '{1}' AND author = '{2}'""".format(productName, site, author)
        return len(self.MakeLocalDbGetInfoQueue(sql)) > 0

    ####################################
    ########### Megamenu  ##############
    ####################################

    def GetMegaMenuTab(self,id_shop, id_url ):
        sql = """SELECT ps_iqitmegamenu_tabs.id_tab, submenu_content FROM ps_iqitmegamenu_tabs
                 LEFT JOIN ps_iqitmegamenu_tabs_shop
                 ON ps_iqitmegamenu_tabs.id_tab = ps_iqitmegamenu_tabs_shop.id_tab
                 WHERE ps_iqitmegamenu_tabs_shop.id_shop = {0} AND id_url = '{1}'""".format(id_shop, id_url)

        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1

        return data[0]

    def IsMegaMenuCustomLinkExists(self, linkParam):
        sql = u"""SELECT id_iqitmenulinks FROM ps_iqitmenulinks_lang
                 WHERE id_shop = {0} and label = '{1}'""".format(linkParam["id_shop"], linkParam["label"])
        data = self.MakeGetInfoQueue(sql)
        return len(data) > 0

    def AddMegaMenuCustomLink(self, linkParam):
        sql= """INSERT INTO ps_iqitmenulinks( id_shop, new_window)
                VALUES ({0}, {1})""".format(linkParam["id_shop"], linkParam["new_window"])
        self.MakeUpdateQueue(sql)
        id_iqitmenulinks = self.GetLastInsertedId("ps_iqitmenulinks", "id_iqitmenulinks")
        sql = u"""INSERT INTO ps_iqitmenulinks_lang(id_iqitmenulinks, id_lang, id_shop, label, link)
                 VALUES ({0},1,{1},'{2}','{3}')""".format(id_iqitmenulinks, linkParam["id_shop"],linkParam["label"],linkParam["link"] )
        self.MakeUpdateQueue(sql)
        return id_iqitmenulinks

    def AddMegaMenuTab(self, tabParam):
        sql = u"""INSERT INTO ps_iqitmegamenu_tabs(menu_type, active, active_label, position, url_type, id_url,  submenu_content, submenu_type, submenu_width, group_access)
                  VALUES ({0},1,0,{1},2,'HOME0','{2}','{3}','{4}','_group_access')""".format(tabParam["menu_type"], tabParam["position"], tabParam["submenu_content"], tabParam["submenu_type"], tabParam["submenu_width"]).replace("_group_access", "a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}")
        self.MakeUpdateQueue(sql)
        id_tab = self.GetLastInsertedId("ps_iqitmegamenu_tabs", "id_tab")
        sql = u"""INSERT INTO ps_iqitmegamenu_tabs_lang(id_tab, id_lang, title)
                 VALUES ({0},1,'{1}')""".format(id_tab, tabParam["title"])
        self.MakeUpdateQueue(sql)
        return id_tab

    def SetMegaMenuTabSubmenuContent(self, id_tab, submenu_content):
        sql = u"""UPDATE ps_iqitmegamenu_tabs
                  SET submenu_content = '{1}'
                  WHERE id_tab = {0}""".format(id_tab, submenu_content)
        self.MakeUpdateQueue(sql)

    def AddMegaMenuTabToShop(self, id_tab, id_shop):
        sql = """INSERT INTO ps_iqitmegamenu_tabs_shop(id_tab, id_shop)
                 VALUES ({0},{1})""".format(id_tab, id_shop)
        self.MakeUpdateQueue(sql)

    def GetMegaMenuMobileMenuValue(self, id_shop):
        sql = u"""SELECT value FROM ps_configuration
                 where name = 'iqitmegamenu_mobile_menu' and id_shop = {0}""".format(id_shop)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def AddMegaMenuMobileMenuValue(self, id_shop, value):
        today = self.GetTodayDate()
        sql = u"""INSERT INTO ps_configuration( id_shop_group, id_shop, name, value, date_add, date_upd)
                  VALUES (1,{0},'iqitmegamenu_mobile_menu','{1}','{2}','{3}')""".format(id_shop, value, today, today)
        self.MakeUpdateQueue(sql)

    def SetMegaMenuMobileMenuValue(self, id_shop, value):
        sql = u"""UPDATE ps_configuration
                  SET value = '{1}'
                  where name = 'iqitmegamenu_mobile_menu' and id_shop = {0}""".format(id_shop, value)
        self.MakeUpdateQueue(sql)

    def GetMegaMenuShopLinksIds(self, id_shop):
        sql = u"""SELECT id_iqitmenulinks FROM ps_iqitmenulinks
                  where id_shop = {0}""".format(id_shop)
        return [x[0] for x in self.MakeGetInfoQueue(sql)]


    def GetMegaMenuTabsIds(self):
        sql = """SELECT id_tab FROM ps_iqitmegamenu_tabs"""
        data = self.MakeGetInfoQueue(sql)

        return [x[0] for x in data]


    ####################################
    ########### Contentmonster #########
    ####################################

    def GetContentmonsterCategoriesOrders(self):
        sql = """SELECT * FROM contentmonster"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetContentmonsterOrderState(self, orderId, state):
        sql = u"""UPDATE contentmonster
                  SET state = '{0}'
                  WHERE orderId = {1}""".format(state, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderId(self, id, orderId):
        sql = u"""UPDATE contentmonster
                  SET orderId = {0}
                  WHERE id = {1}""".format(orderId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderText(self, orderId, text):
        sql = u"""UPDATE contentmonster
                  SET text = '{0}'
                  WHERE orderId = {1}""".format(text, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderUniqueness(self, orderId, uniqueness):
        sql = u"""UPDATE contentmonster
                  SET uniqueness = {0}
                  WHERE orderId = {1}""".format(uniqueness, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderDate(self, orderId, date):
        sql = u"""UPDATE contentmonster
                  SET date = '{0}'
                  WHERE orderId = {1}""".format(date, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteContentmonsterOrder(self, orderId):
        sql = u"""DELETE FROM contentmonster
                  WHERE orderId = {0}""".format(orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def AddContentmonsterOrder(self, data):
        if "orderId" not in data.keys():
            data["orderId"] = "NULL"
        sql = u"""INSERT INTO contentmonster(site, cityId, cityName, categories, orderId)
                  VALUES ('{0}',{1},'{2}','{3}','{4}')""".format(
                  data["site"], data["cityId"],data["cityName"],data["categories"],data["orderId"])
        self.MakeLocalDbUpdateQueue(sql)

    ####### статьи #######

    def GetContentmonsterArticlesOrders(self):
        sql = """SELECT * FROM contentmonster_articles"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetContentmonsterArticleState(self, orderId, state):
        sql = u"""UPDATE contentmonster_articles
                  SET state = '{0}'
                  WHERE orderId = {1}""".format(state, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleDate(self, orderId, date):
        sql = u"""UPDATE contentmonster_articles
                  SET date = '{0}'
                  WHERE orderId = {1}""".format(date, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleText(self, orderId, text):
        sql = u"""UPDATE contentmonster_articles
                  SET text = '{0}'
                  WHERE orderId = {1}""".format(text, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleTheme(self, orderId, theme):
        sql = u"""UPDATE contentmonster_articles
                  SET theme = '{0}'
                  WHERE orderId = {1}""".format(theme, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleOrderId(self, id, orderId):
        sql = u"""UPDATE contentmonster_articles
                  SET orderId = {0}
                  WHERE id = {1}""".format(orderId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddContentmonsterArticleOrder(self, data):
        sql = u"""INSERT INTO contentmonster_articles(site, category, date)
                  VALUES ('{0}','{1}','{2}','{3}','{4}')""".format(
                  data["site"], data["category"],data["cityName"],data["categories"],data["orderId"])
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteContentmonsterArticleOrder(self, orderId):
        sql = u"""DELETE FROM contentmonster_articles
                  WHERE orderId = {0}""".format(orderId)
        self.MakeLocalDbUpdateQueue(sql)

    ####### товары #######

    def GetContentmonsterProductsOrders(self):
        sql = """SELECT * FROM contentmonster_products"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetContentmonsterProductsSiteOrders(self, siteUrl):
        sql = """SELECT * FROM contentmonster_products
                 WHERE site = '{}'""".format(siteUrl)
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetContentmonsterProductsSiteUnprocessedOrders(self, siteUrl):
        sql = """SELECT * FROM contentmonster_products
                 WHERE site = '{}' AND (orderId is NULL or orderId = '')""".format(siteUrl)
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetContentmonsterProductsOrderId(self, id, orderId):
        sql = u"""UPDATE contentmonster_products
                  SET orderId = {}
                  WHERE id = {}""".format(orderId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddContentmonsterProductsOrder(self, siteUrl, products, shortDescriptions = 0):
        sql = u"""INSERT INTO contentmonster_products(site, products, shortDescriptions)
                  VALUES ('{}','{}', {})""".format(siteUrl, products, shortDescriptions)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterProductsOrderState(self, orderId, state):
        sql = u"""UPDATE contentmonster_products
                  SET state = '{0}'
                  WHERE orderId = {1}""".format(state, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteContentmonsterProductsOrder(self, id):
        sql = u"""DELETE FROM contentmonster_products
                  WHERE id = {}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## Reindex #############
    ####################################

    def AddReindex(self, data):
        values = u""
        if "categories" not in data.keys():
            values += "NULL"
        else:
            values += u"'{0}'".format(data["categories"])
        values += ","
        if "products" not in data.keys():
            values += "NULL"
        else:
            values += u"'{0}'".format(data["products"])
        values += ","
        if "otherPages" not in data.keys():
            values += "NULL"
        else:
            values += u"'{0}'".format(data["otherPages"])

        sql = u"""INSERT INTO reindex(site, shopName, categories, products, otherPages)
                  VALUES ('{0}', '{1}', {2})""".format(data["site"], data["shopName"], values)

        self.MakeLocalDbUpdateQueue(sql)

    def GetReindexData(self):
        sql = u"""SELECT * FROM reindex"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetReindexLastLaunchDate(self, id, date):
        sql = u"""UPDATE reindex
                  SET lastLaunchDate = '{0}'
                  WHERE id = {1}""".format(date, id)
        self.MakeLocalDbUpdateQueue(sql)

    def SetReindexLastShopId(self, id, lastShopId):
        sql = u"""UPDATE reindex
                  SET lastShopId = {0}
                  WHERE id = {1}""".format(lastShopId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteReindex(self, id):
        sql = u"""DELETE FROM reindex
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ################ Feeds #############
    ####################################
    def GetFeedsData(self):
        sql = u"""SELECT * FROM feeds"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetFeedDate(self, id, date):
        sql = u"""UPDATE feeds
                  SET date = '{}'
                  WHERE id = {}""".format(date, id)
        return self.MakeLocalDbUpdateQueue(sql)

    def SetFeedCity(self, id, city):
        sql = u"""UPDATE feeds
                  SET city = '{}'
                  WHERE id = {}""".format(city, id)
        return self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## СДЭК ################
    ####################################

    def GetSdekPvzTable(self):
        sql = """SELECT * FROM sdek_pvz
                 WHERE Type = 'PVZ'"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetCitySdekPvzList(self, pvzTable, city):
        city = city.lower()
        pvzList = [x for x in pvzTable if (x[3].lower().startswith(city) and x[3].lower().find(",") != -1) or (x[3].lower() == city and x[3].lower().find(",") == -1)]
        return pvzList

    def GetCityFromCode(self, code):
        sql = """SELECT cityName FROM sdek_pvz
                 WHERE cityCode = {0}""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    def GetCityFromId(self, id):
        sql = """SELECT city_name FROM sdek_cities_codes
                 WHERE id = {0}""".format(id)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    def GetPvzCityCode(self, code):
        sql = """SELECT cityCode FROM sdek_pvz
                 WHERE pvzCode = '{0}'""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    def GetPvzByCode(self, code):
        sql = """SELECT * FROM sdek_pvz
                 WHERE pvzCode = '{0}'""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data[0]

    ####################################
    ######## Update products ###########
    ####################################

    def GetUpdateProducts(self):
        sql = u"""SELECT * FROM updateProducts"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def DeleteUpdateProducts(self, id):
        sql = u"""DELETE FROM updateProducts
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddUpdateProducts(self, site, productsIds):
        sql = u"""INSERT INTO updateProducts(site, productsIds)
                  VALUES ('{}', '{}')""".format(site, productsIds)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## Features ############
    ####################################

    def GetFeatures(self):
        sql = u"""SELECT * FROM features"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetDoneFeatures(self):
        sql = u"""SELECT * FROM features
                  WHERE done = 1"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def DeleteFeatureRow(self, id):
        sql = u"""DELETE FROM features
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddFeatureRow(self, productName, category, features, url):
        sql = u"""INSERT INTO features(productName, category, features, url)
                  VALUES ('{}', '{}', '{}', '{}')""".format(productName, category, features, url)
        self.MakeLocalDbUpdateQueue(sql)

    def GetAllFeatures(self):
        sql = u"""SELECT * FROM allFeatures"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def AddFeatureToAllFeatures(self, name, values):
        sql = u"""INSERT INTO allFeatures(name, featureValues)
                  VALUES ('{}', '{}')""".format(name, values)
        self.MakeLocalDbUpdateQueue(sql)

    def SetFeatureValues(self, name, values):
        sql = u"""UPDATE allFeatures
                  SET featureValues = '{}'
                  WHERE name = '{}'""".format(values, name)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## States ##############
    ####################################

    def AddOrderState(self, name, color, unremovable):
        id_order_state = self.GetOrderStateId(name)
        if id_order_state != -1:
            return id_order_state
        sql = u"""INSERT INTO ps_order_state(color, unremovable)
                VALUES ('{}', {})""".format(color, unremovable)
        self.MakeUpdateQueue(sql)
       	id_order_state = self.GetLastInsertedId("ps_order_state","id_order_state")
        sql = u"""INSERT INTO ps_order_state_lang(id_order_state, id_lang, name, template)
                  VALUES ({}, 1, '{}', '')""".format(id_order_state, name)
        self.MakeUpdateQueue(sql)
        return id_order_state


    def GetOrderStateId(self, name):
        sql = u"""SELECT id_order_state FROM ps_order_state_lang
                  WHERE name = '{}'""".format(name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    ####################################
    ############## New orders ############
    ####################################

    def AddNewOrder(self, siteUrl, orderId):
        sql = u"""INSERT INTO newOrders(siteUrl, orderId)
                VALUES ('{}', '{}')""".format(siteUrl, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def GetNewOrders(self, siteUrl):
        sql = u"""SELECT orderId FROM newOrders
                  WHERE siteUrl = '{}'""".format(siteUrl)
        data = self.MakeLocalDbGetInfoQueue(sql)
        return [i[0] for i in data]

    ####################################
    ######## Products Uniqueness #######
    ####################################

    def AddProductUniqueness(self, data):
        sql = u"""INSERT INTO products_uniqueness(site, productId, name, url, description, uniquenessInfo)
                  VALUES ('{}',{},'{}','{}','{}','{}')""".format(data["site"], data["productId"], data["name"], data["url"],data["description"],data["uniquenessInfo"])
        self.MakeLocalDbUpdateQueue(sql)

    def GetProductsUniqueness(self):
        sql = u"""SELECT * FROM products_uniqueness"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetNonuniqueSiteProducts(self, siteUrl):
        sql = u"""SELECT * FROM products_uniqueness
                  WHERE site = '{}' AND uniquenessInfo <> ''""".format(siteUrl)
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetSiteNeedToRewriteProducts(self, siteUrl):
        sql = u"""SELECT * FROM products_uniqueness
                  WHERE site = '{}' AND needToRewrite = 1 AND processed = 0""".format(siteUrl )
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetProductProcessed(self, siteUrl, productId):
        sql = u"""UPDATE products_uniqueness
                  SET processed = 1
                  WHERE site = '{}' AND productId = {}""".format(siteUrl, productId)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## Other #############
    ####################################

    def GetCurrencyId(self, name):
        sql = u"""SELECT id_currency FROM ps_currency
                  WHERE name = '{}'""".format(name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetActiveCountryId(self):
        sql = u"""SELECT * FROM ps_country
                  WHERE active = 1"""
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def AddItemToThemeConfigurator(self, id_shop, data):
        item_order, title, title_use, hook, url, target, image, image_w, image_h, html, active = data
        sql = u"""INSERT INTO ps_themeconfigurator( id_shop, id_lang, item_order, title, title_use, hook, url, target, image, image_w, image_h, html, active)
                 VALUES ({0},1,{1},'{2}',{3},'{4}','{5}',{6},'{7}',{8},{9},'{10}',{11})""".format(id_shop, item_order, title, title_use, hook, url, target, image, image_w, image_h, html, active)

        self.MakeUpdateQueue(sql)

    def GetTableData(self, tableName, shopId = 1):
        sql = """SELECT * FROM {0}
                 WHERE id_shop = {1}""".format(tableName, shopId)
        return self.MakeGetInfoQueue(sql)

    def GetTexts(self):
        sql = u"""SELECT * FROM  texts"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def UpdateAddressData(self, addressId, newAddressData):
        sql = """UPDATE ps_address
                SET address1 = '%(address1)s'
               WHERE id_address = %(addressId)s"""%{
               "addressId":addressId,
               "address1":newAddressData[0]
               }

        self.MakeUpdateQueue(sql)

    def GetSdekPvzTable(self):
        sql = """SELECT * FROM sdek_pvz"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetCitySdekPvzList(self, pvzTable, city):
        city = city.lower()
        pvzList = [x for x in pvzTable if (x[3].lower().startswith(city) and x[3].lower().find(",") != -1) or (x[3].lower() == city and x[3].lower().find(",") == -1)]
        return pvzList

    def GetConfigurationValue(self, name):
        sql = u"""SELECT value FROM ps_configuration
                  WHERE name = '{}'""".format(name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) > 0:
            return data[0][0]

        return -1

    def SetConfigurationValue(self, name, value):
        sql = u"""UPDATE ps_configuration
                  SET value = "{}"
                  WHERE name = "{}\"""".format(value, name)
        data = self.MakeUpdateQueue(sql)

    def GetModuleId(self, name):
        sql = """SELECT id_module FROM ps_module
                 WHERE name = '{}'""".format(name)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    ############################################################################
    ################################    API    #################################
    ############################################################################

    def InitAllProducts(self):
        self.allProducts = self.GetShopProductsOriginalNames(1)
##        self.allProducts = self.GetShopProducts(1)

    def CleanItemName(self, itemName):
        name = itemName
        if name.find(u"«") != -1:
            name = name[name.index(u"«") + 1:name.index(u"»")]

        if name.find(u"\"") != -1:
            name = name.split(u"\"")[-2]

        return name

    def GetProductIdQuick(self, itemName, cleanItemName = True):
        if cleanItemName == True:
            itemName = self.CleanItemName(itemName)

        for product in self.allProducts:
            productId = product[0]
            try:
                productName = product[1].lower()
            except:
                continue
            if productName in ["", None]:
                productName = self.GetProductName(productId)
            if cleanItemName == True:
                productName = self.CleanItemName(productName)
            if productName.lower() == itemName.lower():
                return productId

        return -1

    def Parser_ProcessProduct(self, productParams, productCategory, warehouseCategory, shopName, productUrl, cleanItemName = True, message = ""):
        self.message = message
        category = productCategory + "_" + warehouseCategory.lower()
        productParams["categories"] = category
        productParams["productUrl"] = productUrl

        categoryId = self.GetCategoryID(category)
        if categoryId == -1:
            #добавляем категорию
            result = self.AddCategoryDefault(category, warehouseCategory, 0, shopName)
            if result == False:
                self.logging.LogErrorMessage(u"Не удалось добавить категорию: " + category)
                self.message += "Не удалось добавить категорию '{}'\n".format(category.encode("utf-8"))
                return [False, self.message]
            else:
                self.message += "Добавлена категория '{}'\n".format(category.encode("utf-8"))
                self.logging.LogInfoMessage(u"Добавлена категория: " + category)

        if "original_name" in productParams.keys():
            productId = self.GetProductIdQuick(productParams["original_name"], cleanItemName)
        else:
            productId = self.GetProductIdQuick(productParams["name"], cleanItemName)

        if productId != -1:#обновление товара
            article = self.GetProductArticle(productId)
            contractor = commonLibrary.GetContractorName(article)
            if contractor in [u"Наш товар", u"Диктос"]:
                return [productId, self.message]

            quantity = 1

            productName = commonLibrary.CleanItemName(productParams["name"])
            if "quantity" in productParams.keys():
                quantity = productParams["quantity"]

            if self.IsProductInStock(productId) == False:
                if quantity > 0:
                    self.MakeProductEnabledForAllShops(productId)
                    self.SetProductQuantityForAllShops(productId, 30)
                    self.logging.LogInfoMessage(u"Товар '{}' ({}) активирован".format(productName, productId))
                    self.message += "Товар '{}' ({}) активирован\n".format(productName.encode("utf-8"), productId)
            else:
                if quantity == 0:
                    self.MakeProductUnavailableForAllShops(productId)
                    self.logging.LogInfoMessage(u"Товар '{}' ({}) деактивирован".format(productName, productId))
                    self.message += "Товар '{}' ({}) деактивирован\n".format(productName.encode("utf-8"), productId)

            if "characteristics" in productParams.keys():
                self.SetProductCharacteristics(productId, productParams["characteristics"])

            productDetails = self.GetShopProductDetailsById(productId, 1)
            if productDetails == -1:
                self.logging.Log("Product with id " + str(productId) + " must be deleted")
                return False
##                productDetails = self.GetProductDetailsById(productId)
            if float(productDetails[2]) != float(productParams["wholesale_price"]) or float(productDetails[1]) != float(productParams["price"]):
                self.SetProductPriceForAllShops(productId, productParams["wholesale_price"], productParams["price"])
                self.logging.Log(u"""Товар '{}' ( {} ) обновлен: старая закупочная цена = {}, новая закупочная цена = {}, старая розничная цена = {}, новая розничная цена = {}""".
                format( productName, productId, productDetails[2], productParams["wholesale_price"], productDetails[1],productParams["price"]))
                self.message += """Товар '{}' ( {} ) обновлен: старая закупочная цена = {}, новая закупочная цена = {}, старая розничная цена = {}, новая розничная цена = {}\n""". \
                format( productName.encode("utf-8"), productId, productDetails[2], productParams["wholesale_price"], productDetails[1],productParams["price"])
            return [productId, self.message]

        else:#добавление товара
            contractor = commonLibrary.GetContractorName(productParams["article"])
            if contractor in [u"Логос"]:
                return [False, self.message]
            result = self.AddProductAllShops(productParams, shopName)
            if result == False:
                self.logging.LogErrorMessage(u"Ошибка добавления товара '{}'\n".format(productParams["name"]))
                self.message +=  "Ошибка добавления товара '{}'\n".format(productParams["name"].encode("utf-8"))
                return [False, self.message]
            else:
                try:
                    self.logging.Log(u"Добавлен товар '{0}' ({1})".format(productParams["name"], str(result)))
                    self.message += "Добавлен товар '{0}' ({1})\n".format(productParams["name"].encode("utf-8"), str(result))
                except:
                    self.logging.Log(u"Добавлен товар {0}".format(result))
                productId = result
                self.SetProductArticle(result, productParams["article"] + str(productId))
                if "original_name" in productParams.keys():
                    self.SetProductOriginalName(productId, productParams["original_name"])
                else:
                    if cleanItemName:
                        self.SetProductOriginalName(productId, commonLibrary.CleanItemName( productParams["name"]))
                    else:
                        self.SetProductOriginalName(productId, productParams["name"])
                self.SetProductUpc(productId, productUrl)
                self.SetProductActive(productId, 0)
                self.SetProductIndexedForAllShops(productId, 0)
                if "features" in productParams.keys():
                    self.DeleteProductFeatures(productId)
                    for feature in productParams["features"]:
                        self.AddProductFeatureValue(productId, feature, productParams["features"][feature])
                if "id_attachment" in productParams.keys():
                    self.AddProductAttachment(productId,productParams["id_attachment"] )
                return [productId, self.message]

    def GetProductIdFromXml(self, xml):
        xml = etree.XML(xml)
        try:
            id = xml.xpath("//product//id")[0].text
            return id
        except:
            return -1

    def AddProductAllShops(self, productData, shopName):
        data = {}

        if "description_short" not in productData.keys():
            productData["description_short"] = None
        if "meta_title" not in productData.keys():
            productData["meta_title"] = None
        if "link_rewrite" not in productData.keys():
            productData["link_rewrite"] = None

        data["active"] = "0"
        data["name"] = productData["name"]
        data["categories"] = productData["categories"]
        data["price"] = str(productData["price"])
        data["id_tax_rules_group"] = "0"
        data["wholesale_price"] = str(productData["wholesale_price"])
        data["on_sale"] = "0"
        data["quantity_discount"] = "0"
        data["reference"] = ""
        data["supplier_reference"] = ""
        data["id_supplier"] = "0"
        data["manufacturer_name"] = ""
        data["ean13"] = "0"
        data["upc"] = "0"
        data["ecotax"] = "0"
        data["width"] = "0"
        data["height"] = "0"
        data["depth"] = "0"
        data["weight"] = "0"
        data["quantity"] = "0"
        data["visibility"] = "both"
        data["additional_shipping_cost"] = "0"
        data["unity"] = ""
        data["unit_price_ratio"] = "0"
        if productData["description_short"] != None: data["description_short"] = productData["description_short"]
        else: data["description_short"] = data["name"]
        if productData["description"] != None: data["description"] = commonLibrary.UnescapeText (productData["description"])
        else:data["description"] = data["name"]
        data["tags"] = ""
        if productData["meta_title"] == None:
            data["meta_title"] = commonLibrary.CleanItemName(data["name"])
        else:
            data["meta_title"] = productData["meta_title"]

        if productData["link_rewrite"] == None:
            data["link_rewrite"] = ""
        else:
            data["link_rewrite"] = productData["link_rewrite"]

        chars = ["(",")","+",",",".","-","/","\\"]
        for char in chars:
            data["link_rewrite"] = data["link_rewrite"].replace(char, "")

        data["meta_keywords"] = data["name"].replace(" ",",")
        data["meta_description"] = data["name"]
        data["available_now"] = u"В наличии"
        data["available_later"] = u"Под заказ"
        data["available_for_order"] = "1"
        date_add = self.GetTodayDate()
        data["available_date"] = date_add
        data["date_add"] = date_add
        data["date_upd"] = date_add
        data["show_price"] = "1"
        data["images"] = []
        for image in productData["images"]:
            data["images"].append(self.iriToUri(image))
        data["product_features"] = ""
        data["online_only"] = "0"
        data["condition"] = "new"
        data["customizable"] = "0"
        data["uploadable_files"] = "0"
        data["text_fields"] = "0"

        xml = ""
        try:
            xml = self.AddProduct(data)
        except Exception as exception:
            with open("exception.message" + data["price"] + ".txt", "w") as ef:
                ef.write(str(exception.message))
            return False
        if type(xml) == int:
            self.logging.Log("Product with url '{0}' exists, id {1}".format(productData["productUrl"], xml))
            return False
        if xml.find("xception") != -1 or xml.find("<errors>") != -1:
            with open("err" + data["price"] + ".xml", "w") as ef:
                ef.write(xml)
            return False
##        with open("xml.xml", "w") as ef:
##            ef.write(xml)
        productId = self.GetProductIdFromXml(xml)
        print productId

        #добавляем картинки
        for image in productData["images"]:
            try:
                self.AddImage("products", productId, image)
            except:
                self.logging.LogErrorMessage(u"Unable to add picture {} to product {}".format(image, productId))
                self.message += "Unable to add picture {} to product {}\n".format(image.encode("utf-8"), productId)

        shops = self.GetAllShops()
        if len(shops) > 1:
            #добавляем во все магазины
            categoryId = str(self.GetCategoryID(data["categories"]))

            #добавляем картинки
            print u"добавляем картинки"
            imagesIds =  self.GetProductImagesIds(productId)

            for imageId in imagesIds:
                sql = """INSERT INTO ps_image_lang(id_image, id_lang, legend)
                    VALUES (%(id_image)s,1,'')"""%{"id_image":imageId}

                self.MakeUpdateQueue(sql, True)

                for shop in shops:
                    shopId = shop[0]
                    if shopId == 1:
                        if self.siteUrl != "http://tomsk.otpugivatel.com":
                            continue

                    sql = """INSERT INTO ps_image_shop(id_image, id_shop, cover)
                         VALUES (%(id_image)s,%(shopId)s,1)"""%{"id_image":imageId,"shopId":shopId}
                    self.MakeUpdateQueue(sql)

            sql = """DELETE FROM ps_product_shop
                     WHERE id_product = %(productId)s"""%{"productId":productId}
            self.MakeUpdateQueue(sql)
            print u"добавляем для всех магазинов"

            for shop in shops:
                shopId = shop[0]

                sql = """INSERT INTO ps_product_shop(id_product, id_shop, id_category_default, id_tax_rules_group, on_sale, online_only, ecotax, minimal_quantity, price, wholesale_price, unity, unit_price_ratio, additional_shipping_cost, customizable, uploadable_files, text_fields, active, redirect_type, id_product_redirected, available_for_order, available_date, `condition`, show_price, indexed, visibility, cache_default_attribute, advanced_stock_management, date_add, date_upd)
                VALUES (%(productId)s, %(shopId)s, %(categoryId)s,0, 0, 0, 0, 0, %(price)s, %(wholesale_price)s,'',0, 0, 0, 0, 0, 1, '', 0, 1, '%(available_date)s', 'new', 1, 1, 'both', 0, 0, '%(date)s', '%(date)s')"""%{"productId":productId,
                "shopId":shopId,
                "categoryId":categoryId,
                "price":data["price"],
                "wholesale_price":data["wholesale_price"],
                "date":date_add,"available_date":date_add}
                self.MakeUpdateQueue(sql)

        print u"изменяем количество"
        self.MakeProductEnabledForAllShops(productId)
        self.SetProductQuantityForAllShops(productId, 30)

        return productId


    def AddProduct(self, productData):
        productId = self.GetProductID(productData["name"])
        if productId != -1:
            self.SetProductOriginalName(productId, productData["name"])
            return int(productId)

        xml = self.POST("products", self.GetProductXml(productData))
        return xml

    def DownloadImage(self, originImagePath, destinationImagePath):
        name = originImagePath.split("/")[-1]
        name = name.replace("." + name.split(".")[-1], "")
        ext = originImagePath.split(".")[-1]
        if ext == "" or len(ext) > 3:ext = "jpg"
        pic = destinationImagePath + name + "." + ext
        originImagePath = self.iriToUri(originImagePath)
        try:
            urllib.urlretrieve(originImagePath, pic)
        except:
            return False

        return True

    def DownloadFile(self, originPath, name = None, destinationPath = None ):
        if name == None:
            name = originPath.split("/")[-1]
        if destinationPath != None:
            if destinationPath[-1] != "\\":
                destinationPath += "\\"
            filePath = destinationPath + name
        else:
            filePath = name
        originPath = self.iriToUri(originPath)
        try:
            urllib.urlretrieve(originPath, filePath)
        except:
            return False

        return True

    def AddImage(self, category, id, image):
        ext = image.split(".")[-1]
        if ext == "" or len(ext) > 3:ext = "jpg"
        pic = "pic." + ext
        urllib.urlretrieve(image, pic)
        fd        = io.open(pic, "rb")
        content   = fd.read()
        fd.close()
        xml = self.POST("images/" + category + "/" + id, files = [('image', pic, content)])
        if xml != None:
            if xml.find("errors") != -1:
                name = image.split("/")[-2]
                with open(name, "w") as f:
                    f.write(xml)
                    f.close()

    def GetImage(self, category, id, imagePath = "pic.jpg"):
        xml = self.GET("images/" + category + "/" + str(id))
        if xml == "":
            return False

        xml = etree.XML(xml)
        imagePath = xml.xpath("//image//declination")[0].text
        imageId = imagePath.split("\"")[-2].split("/")[-1]
        content = self.GET("images/products/" + str(id) + "/" + imageId, True)
        with open(imagePath, 'wb') as f:
            for chunk in content:
                f.write(chunk)

        return True

    def UpdateProduct(self, productData):
        xml = self.GET("products/1")
        xml = self.GetProductXml(productData,xml )
        return self.PUT("products/1", xml)

##    def DeleteProduct(self, productId):
##        xml = self.DELETE("products/" + str(productId))
##        return xml

    def AddCategoryDefault(self, name, parentCategory, enabled, shopName):
        data = {}

        data["name"] = name

        if self.GetCategoryID(name) != -1:
            return

        data["parent"] = parentCategory
        data["active"] = str(enabled)
        data["is_root_category"] = "0"
        data["description"] = name
        data["meta_title"] = name + u" купить недорого"
        data["meta_description"] = name
        data["meta_keywords"] = name.replace(" ",",")
        data["images"] = []

        xml = self.AddCategory(data)

        if xml != None:
            if xml.find("errors") != -1:
                with open(name, "w") as f:
                    f.write(xml)
                    f.close()
                return False
        else:
            return True

    def AddCategory(self,categoryData):
        xml = self.POST("categories", self.GetCategoryXml(categoryData))
        if xml.find("errors") != -1:
            return xml
        xml = etree.XML(xml)
        id = xml.xpath("//category//id")[0].text
        for image in categoryData["images"]:
            self.AddImage("categories", id, image)

        return id

    def GetProductXml(self, productData, xml = None):
        if xml == None:
            xml = self.GET("products?schema=synopsis")

        xml = etree.XML(xml)
        root =  xml.getroottree().getroot()
        productNode = root.find("product")
        productNode.remove(productNode.find("manufacturer_name"))
        productNode.remove(productNode.find("quantity"))

        productNode.find("name").find("language").text = productData["name"]
        categoryId = str(self.GetCategoryID(productData["categories"]))
        productNode.find("id_category_default").text = categoryId
        if self.siteUrl in ["http://safetus.ru", "http://omsk.knowall.ru.com"]:
            productNode.find("associations").find("categories").find("categories").find("id").text = categoryId
        else:
            productNode.find("associations").find("categories").find("category").find("id").text = categoryId
        productNode.find("link_rewrite").find("language").text = self.Transliterate(commonLibrary.CleanItemName(productData["name"].replace(" ","-")))
        productNode.find("active").text = productData["active"]
        productNode.find("available_for_order").text = productData["active"]
        productNode.find("show_price").text = productData["show_price"]
        productNode.find("description_short").find("language").text = productData["description_short"]
        productNode.find("description").find("language").text = productData["description"]
        productNode.find("price").text = productData["price"]
        productNode.find("wholesale_price").text = productData["wholesale_price"]
        productNode.find("id_tax_rules_group").text = productData["id_tax_rules_group"]
        productNode.find("meta_title").find("language").text = productData["meta_title"]
        productNode.find("meta_description").find("language").text = productData["meta_description"]
        productNode.find("meta_keywords").find("language").text = productData["meta_keywords"]
        productNode.find("width").text = productData["width"]
        productNode.find("height").text = productData["height"]
        productNode.find("depth").text = productData["depth"]
        productNode.find("weight").text = productData["weight"]
        productNode.find("on_sale").text = productData["on_sale"]
        productNode.find("date_add").text = productData["date_add"]
        productNode.find("available_date").text = productData["date_add"]
        return etree.tostring(xml)

    def GetCategoryXml(self, categoryData, xml = None):
        if xml == None:
            xml = self.GET("categories?schema=synopsis")

        xml = etree.XML(xml)
        root =  xml.getroottree().getroot()
        productNode = root.find("category")
        productNode.remove(productNode.find("level_depth"))
        productNode.remove(productNode.find("nb_products_recursive"))

        productNode.find("name").find("language").text = categoryData["name"]
        productNode.find("is_root_category").text = "0"

        if categoryData["is_root_category"] == "1":productNode.find("id_parent").text = "2"
        else:
            if str(categoryData["parent"])[0].isdigit() == True:
                parentCategoryId = str(categoryData["parent"])
            else:
                parentCategoryId = self.GetCategoryID(categoryData["parent"])
            productNode.find("id_parent").text = str(parentCategoryId)

        productNode.find("active").text = categoryData["active"]

        productNode.find("link_rewrite").find("language").text = commonLibrary.GetLinkRewriteFromName(categoryData["name"])

        productNode.find("description").find("language").text = categoryData["description"]
        productNode.find("meta_title").find("language").text = categoryData["meta_title"]
        productNode.find("meta_description").find("language").text = categoryData["meta_description"]
        productNode.find("meta_keywords").find("language").text = categoryData["meta_keywords"]

        return etree.tostring(xml)

    def GET(self, path, returnResponseObject = False):
        xml = requests.get(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "") + "/api/" + path, auth=requests.auth.HTTPBasicAuth(self.key, ''), verify=False)
        if returnResponseObject == False:
            return xml._content
        else:
            return xml

    def POST(self, path, xml=None, files=None):
        if files is not None:
            headers, body = self.encode_multipart_formdata(files)
            xml = requests.post(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "") + "/api/" + path,  data=body, headers=headers,auth=requests.auth.HTTPBasicAuth(self.key, ''), verify=False)
        else:
            xml = requests.post(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "")  + "/api/" + path, data=xml,auth=requests.auth.HTTPBasicAuth(self.key, ''), verify=False)
        return xml._content

    def PUT(self, path, xml):
        xml = requests.put(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "") + "/api/" + path, data=xml,auth=requests.auth.HTTPBasicAuth(self.key, ''))
        return xml._content

    def DELETE(self, path):
        xml = requests.delete(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "")  + "/api/" + path, auth=requests.auth.HTTPBasicAuth(self.key, ''))

    def encode_multipart_formdata(self, files):
        """
        Encode files to an http multipart/form-data.

        @param files: a sequence of (type, filename, value) elements for data to be uploaded as files.
        @return: headers and body.
        """
        BOUNDARY = '----------ThIs_Is_tHe_bouNdaRY_$'
        CRLF     = '\r\n'
        L        = []
        for (key, filename, value) in files:
            L.append('--' + BOUNDARY)
            L.append('Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename))
            L.append('Content-Type: %s' % self.get_content_type(filename))
            L.append('')
            L.append(value)
        L.append('--' + BOUNDARY + '--')
        L.append('')
        body     = CRLF.join(L)
        headers  = {'Content-Type': 'multipart/form-data; boundary=%s' % BOUNDARY}
        return headers, body

    def get_content_type(self, filename):
        """
        Retrieve filename mimetype.

        @param filename: file name.
        @return: mimetype.
        """
        return mimetypes.guess_type(filename)[0] or 'application/octet-stream'

    def urlEncodeNonAscii(self, b):
        return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

    def iriToUri(self, iri):
        parts= urlparse.urlparse(iri)
        return urlparse.urlunparse(
            part.encode('idna') if parti==1 else self.urlEncodeNonAscii(part.encode('utf-8'))
            for parti, part in enumerate(parts)
        )

    def DeleteCommentsFromHtml(self, string):
        if string.find("<!--") != -1:
            while string.find("<!--") != -1:
                ind1 = string.find("<!--")
                ind2 = string.find("-->") + 3
                string = string.replace(string[ind1:ind2],"")

        return string

    def IsUrlExists(self, url):
        try:
            resp = requests.get(url)
            return resp.status_code == 200
        except:
            return False
