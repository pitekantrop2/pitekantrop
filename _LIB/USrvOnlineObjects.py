﻿from UBaseObjects import *

class USrvOnlineObjects(UBaseObjects):


    #авторизация
    login = ["name", "auth1"]
    password = ["name", "auth2"]
    loginButton = GetButtonByText(u"Вход")

    #создание счета
    invoiceNumber = ["name" , u"СчетНомер"]
    invoiceDate = ["name" , u"СчетДата"]
    sellerEdit = ["name" , u"ОргНаименование"]
    contractorEdit = ["name" , u"КонтрагентНаименование"]
    itemName = ["name" , u"ТоварНаименование[]"]
    itemPrice = ["name" , u"ТоварЦена[]"]
    itemAmount = ["name" , u"ТоварКоличество[]"]
    addItemButton = ["xpath" , "//a[@title = '" + u"Добавить строку" + "']"]
    deleteItemButton = ["xpath" , "//a[@title = '" + u"Удалить позицию" + "']"]
    NDSSelect = ["name" , u"НДС"]
    invoiceAdditionalInfo = ["name" , u"СчетИнфо"]
##    sendByEmailButton = GetLinkByTitle(u"Отправить по электронной почте")
    sendByEmailButton = ["class", "mr05"]
    setAnotherEmail = GetElementByText(u"label", u"Ввести другой e-mail")
    anotherEmailEdit = ["name" , "q[emailother]"]
    pageBottom = ["class", "online-copy"]

    #Товарные накладные
    packingListNumber = ["name" , u"ТоргНомер"]
    packingListDate = ["name" , u"ТоргДата"]

    #договоры
    contractNumber = ["name" , u"ДоговорНомер"]
    contractDate = ["name" , u"ДоговорДата"]
    contractAdditionalInfo = ["name" , u"ДоговорИнфо"]
    contractPrintForm = ["name", "prnDoc1"]

    #добавление контрагента
    INNEdit = ["name" , u"КонтрагентИНН"]
    KPPEdit = ["name" , u"КонтрагентКПП"]
    OGRNEdit = ["name" , u"КонтрагентОГРН"]
    BIKEdit = ["name" , u"КонтрагентБИК[]"]
    RSEdit = ["name" , u"КонтрагентСчет[]"]
    NameEdit = ["name" , u"КонтрагентСчет[]"]
    fillByINNButton = GetLinkByText(u"Заполнить по ИНН / ОГРН")
    bankRequisitesLink = GetElementByText(u"label", u"Банковские реквизиты")

    #список счетов
    invoicesNumbers = ["xpath" , "//div[@class = 'relative pl2']/a/strong"]
    invoicesNumbersLinks = ["xpath" , "//div[@class = 'relative pl2']/a"]
    invoicesDates = ["xpath" , "//div[@class = 'relative pl2']/small"]
    invoicesSeller = ["name", "seller"]
    invoicesSearch = GetLinkByText(u"Поиск")
    invoicesFilter = GetElementByText("label", u"Фильтр")

    #кнопки
    saveDocumentButton = GetLinkByTitle(u"Сохранить документ")
    sendButton = GetButtonByText(u"Отправить")
    saveButton = GetButtonByText(u"Сохранить")

    #общее
    loaderElement = ["xpath" , "//p[@class = 'preloader-local mt2 mb1']"]
    SelectOption = ["class", "ui-menu-item"]
    contextMenu = ["xpath" , "//span[@class = 'online-i i-contextmenu']"]
    stampCheckbox = ["xpath" , "//label[@class = 'box border gradient-silver']"]
