﻿# -*- coding: utf-8 -*-
import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import MySQLdb
import io
import shutil
import mimetypes
import datetime
import log
import json
import commonLibrary
import os

class WebasystAPI():
    siteUrl = None
    typeId = None
    key = None
    dbConnector = None
    localDbConnector = None
    dbName = None
    dbPassword = None
    dbUser = None
    dbHost = None
    dbPort = None
    logging = None
    allProducts = None
    message = None
    access_token = None
    apiUrl = None

    ordersStates = {
    u"Новый":"new",
    u"Подтвержден":"processing",
    u"Оплачен":"paid",
    u"Ждет отправки":"zhdet-otpravki",
    u"Отправлен":"shipped",
    u"Доставлен в пункт выдачи":"dostavlen",
    u"Доставлен в город-получатель":"dostavlen-v-goro",
    u"Доставлен в почтовое отделение":"dostavlen-v-poch",
    u"Вручен":"vruchen",
    u"Доставлен и оплачен":"completed",
    u"Возврат":"refunded",
    u"В ожидании оплаты по счету":"ozhidanie-oplaty",
    u"Данного товара нет на складе":"tovara-net",
    u"Удален":"deleted"
    }

    def __init__(self, siteUrl = None, serverIP = None, access_token = None, logFilePath = None):
        if serverIP == "95.217.128.60":
            self.access_token = "4edba8c784ecaf90191531fddde5c27a"
            self.apiUrl = ""
        if serverIP == "65.108.89.105":
            self.apiUrl = "https://webasyst2.ru/api.php"
        if serverIP == "95.216.145.123":
            self.apiUrl = "http://webasyst1.ru/api.php"
        if serverIP == "65.21.1.62":
            self.apiUrl = "http://webasyst3.ru/api.php"

        self.access_token = access_token

        if serverIP != None:
            self.siteUrl = siteUrl.replace("http://", "")
            self.dbName = "webassyst"
            self.dbPassword = "1qaz@WSX"
            self.dbUser = "webassyst_user"
            self.dbHost = serverIP
            self.InitConnetcor()
            if self.siteUrl == "auto-hands.ru":
                self.typeId = self.GetProductsTypeId("http://saltlamp.su")
            else:
                self.typeId = self.GetProductsTypeId(siteUrl)

        self.InitLocalConnector()
        if logFilePath != None:
            self.logging = log.Log(logFilePath)

    ######## Методы работы с БД ###########################################

    def InitConnetcor(self):
        counter = 1
        while True:
            if counter > 5:
                raise Exception("")
                return
            try:
                self.dbConnector = MySQLdb.connect(host=self.dbHost, port=3306, user=self.dbUser, passwd=self.dbPassword, db=self.dbName, charset='utf8')
                self.dbConnector.autocommit(True)
                return
            except Exception as error:
                if error.args[1].find("127.0.0.1") == -1:
                    print error.args[1]
                print str(error)
                counter += 1

    def InitLocalConnector(self):
        counter = 1
        while True:
            if counter > 1:
##                raise Exception("")
                return
            try:
                self.localDbConnector = MySQLdb.connect(host="65.108.89.105", port=3306, user="knowall_user_ex", passwd="1qaz@WSX", db="prestashop", charset='utf8')
                self.localDbConnector.autocommit(True)
                return
            except Exception as e:
                print str(e)
                counter += 1

    def MakeUpdateQueue(self, sql, breakIfError = False):
        for i in range(0, 10):
            try:
                cursor = self.dbConnector.cursor()
                resp = cursor.execute(sql)
                cursor.close()
                self.dbConnector.commit()
                break
            except Exception, e:
                if breakIfError == True:
                    self.InitConnetcor()
                    return
                with open("sql_err.txt", "w") as log:
                    log.write(str(e))
                    log.write(sql.encode('cp1251'))
                print e
##                print sql
                print "reconnect to database"
                self.InitConnetcor()

    def MakeGetInfoQueue(self, sql):
        for i in range(0, 10):
            try:
                cursor = self.dbConnector.cursor()
                cursor.execute(sql)
                return cursor.fetchall()
            except Exception, e:
                print e
                print sql
                print "reconnect to database"
                self.InitConnetcor()

    def MakeLocalDbGetInfoQueue(self, sql):
        for i in range(0, 10):
            try:
                cursor = self.localDbConnector.cursor()
                cursor.execute(sql)
                return cursor.fetchall()
            except Exception, e:
                print e
                print sql
                print "reconnect to database"
                self.InitLocalConnector()

    def MakeLocalDbUpdateQueue(self, sql, breakIfError = False):
        for i in range(0, 10):
            try:
                cursor = self.localDbConnector.cursor()
                resp = cursor.execute(sql)
                cursor.close()
                self.localDbConnector.commit()
                break
            except Exception, e:
                if breakIfError == True:
                    self.InitLocalConnector()
                    return
##                with open("sql_err.txt", "w") as log:
##                    log.write(str(e))
##                    log.write(sql)
                print e
                print sql
                print "reconnect to database"
                self.InitLocalConnector()

    def GetTodayDate(self):
        today = datetime.date.today()
        return today.strftime('%Y-%m-%d')

    def GetTableEntriesNumber(self, tableName):
        sql = "SELECT COUNT(*) FROM %(tableName)s"%{"tableName":tableName}
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetLastInsertedId(self, tableName, columnName):
        sql = """SELECT {0} FROM {1}
                ORDER BY {0} DESC
                LIMIT 1""".format(columnName, tableName)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return 0
        return data[0][0]

    def IsSubfolderSite(self):
        storefrontsUrls = self.GetStorefrontsUrls()
        try:
            return storefrontsUrls[4].count("/") > 1
        except:
            return True

    def SetOrderSMSDate(self, orderId, mode, date):
        smsDate = self.GetOrderSMSDate(orderId, mode)
        if smsDate != -1:
            sql = """UPDATE sms
                     SET lastSMSDate = '%(today)s'
                     WHERE siteUrl = '%(siteUrl)s' AND orderId = %(orderId)s AND mode = '%(mode)s'"""%{"today":date,
                     "siteUrl":self.siteUrl, "orderId":orderId, "mode":mode}
            self.MakeLocalDbUpdateQueue(sql)
        else:
            sql = """INSERT INTO sms(siteUrl, orderId, mode, lastSMSDate)
                     VALUES ('%(siteUrl)s',%(orderId)s, '%(mode)s', '%(lastSMSDate)s')"""%{"siteUrl":self.siteUrl,
                     "orderId":orderId, "mode": mode, "lastSMSDate":date}
            self.MakeLocalDbUpdateQueue(sql)

    def GetOrderSMSDate(self, orderId, mode):
        sql = """SELECT lastSMSDate FROM sms
                 WHERE siteUrl = '%(site)s' AND orderId = %(orderId)s AND mode = '%(mode)s'"""%{"site":self.siteUrl,
                  "orderId":orderId, "mode":mode}

        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            return data[0][0]
        except:
            return -1

    def GetMailingIgnoreCustomers(self):
        sql = """SELECT customerId FROM ignored_customers
                 WHERE siteUrl = '%(siteUrl)s'"""%{"siteUrl":self.siteUrl}
        data = self.MakeLocalDbGetInfoQueue(sql)
        return [x[0] for x in data]

    def AddCustomerToIgnoreList(self, customerId):
        sql = """INSERT INTO ignored_customers (siteUrl, customerId)
                 VALUES ( '{0}', {1})""".format( self.siteUrl,customerId)

        data = self.MakeLocalDbUpdateQueue(sql)

    def GetWarehouseProductQuantity(self, warehouse, productName):
        sql = u"""SELECT ItemQuantity FROM %(warehouse)s
                 WHERE ItemName = '%(itemName)s'"""%{"warehouse":warehouse,
                  "itemName":productName}
        data = self.MakeLocalDbGetInfoQueue(sql)

        try:
            num = data[0][0]
            if num == -1:
                num = 0
            return num
        except:
            return -1

    def GetItemNameBySkuId(self, Sku_id):
        sql = """SELECT ItemName FROM warehouse_moscow
                 WHERE Sku_id = '%(Sku_id)s'"""%{"Sku_id":Sku_id}
        data = self.MakeLocalDbGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductContractor(self, name):
        sql = u"""SELECT contractor FROM products
                 WHERE name = '{0}'""".format(name)
        data = self.MakeLocalDbGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetSkuIdByItemName(self, itemName):
        sql = """SELECT Sku_id FROM warehouse_moscow
                 WHERE ItemName = '%(itemName)s'"""%{"itemName":itemName}
        data = self.MakeLocalDbGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetWarehouseProductData(self, warehouse, productName):
        sql = u"""SELECT * FROM {}
                 WHERE ItemName = '{}'""".format(warehouse, productName)
        try:
            return self.MakeLocalDbGetInfoQueue(sql)[0]
        except:
            return -1

    def GetWarehouseProducts(self, warehouse):
        sql = """SELECT * FROM %(warehouse)s"""%{"warehouse":warehouse}
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def GetProductWholesalePrice(self, name):
        sql = u"""SELECT wholesale_price FROM products
                  WHERE name = '{0}'""".format(name)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return int(data[0][0])

    def DecreaseWarehouseProductQuantity(self, warehouse, productName, quantity):
        itemQuantity = self.GetWarehouseProductQuantity(warehouse,productName)
        qty = itemQuantity - quantity
        if warehouse == "warehouse_spb" and qty < 0:
            qty = 0
        sql = """UPDATE %(warehouse)s
                 SET ItemQuantity = %(quantity)s
                 WHERE ItemName = '%(itemName)s'"""%{"warehouse":warehouse,
                  "itemName":productName, "quantity": qty }
        self.MakeLocalDbUpdateQueue(sql)

    def IncreaseWarehouseProductQuantity(self, warehouse, productName, quantity):
        itemQuantity = self.GetWarehouseProductQuantity(warehouse,productName)
        sql = """UPDATE %(warehouse)s
                 SET ItemQuantity = %(quantity)s
                 WHERE ItemName = '%(itemName)s'"""%{"warehouse":warehouse,
                  "itemName":productName, "quantity":itemQuantity + quantity }
        self.MakeLocalDbUpdateQueue(sql)

    def SetWarehouseProductQuantity(self, warehouse, sku, quantity):
        sql = """UPDATE %(warehouse)s
                 SET ItemQuantity = %(quantity)s
                 WHERE WarehouseName = '%(sku)s'"""%{"warehouse":warehouse,
                  "sku":sku, "quantity":quantity }

        self.MakeLocalDbUpdateQueue(sql)

    def SetProductSkuId(self, warehouseName, Sku_id):
        sql = """UPDATE warehouse_moscow
                 SET Sku_id = '{0}'
                 WHERE WarehouseName = '{1}'""".format(Sku_id, warehouseName)
        self.MakeLocalDbUpdateQueue(sql)


    def GetPostings(self, date = None):
        sql = """SELECT * FROM postings"""
        if date != None:
            sql = """SELECT * FROM postings
                     WHERE date > '{0}'""".format(date)
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def GetRegistries(self):
        table = "wa_registries"
        sql = """SELECT * FROM {}""".format(table)
        data = self.MakeLocalDbGetInfoQueue(sql)
        return [x[1] for x in data]

    def AddRegistryId(self, registryId):
        table = "wa_registries"
        sql = """INSERT INTO {}(registryId)
                 VALUES ('{}')""".format(table, registryId)
        data = self.MakeLocalDbUpdateQueue(sql)

    def GetIsRefundProcessed(self, statementCode):
        sql = u"""SELECT * FROM refunds
                 WHERE statementCode = '{0}'""".format(statementCode)

        data = self.MakeLocalDbGetInfoQueue(sql)
        return len(data) > 0

    def AddRefund(self, params):
        sql = u"""INSERT INTO refunds(date, statementCode, items, invoice)
                 VALUES ('{0}', '{1}', '{2}', '{3}')""".format(params["date"], params["statementCode"],params["items"],params["invoice"])

        self.MakeLocalDbUpdateQueue(sql)

    def GetRefundsParams(self, date):
        sql = """SELECT * FROM refunds
                 WHERE date >= '{0}'""".format(date)

        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def SetRefundReplySended(self, statementCode, sended):
        sql = u"""UPDATE refunds
                 SET replySended = {1}
                 WHERE statementCode = '{0}'""".format(statementCode, sended )

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundPostingId(self, statementCode, postingId):
        sql = u"""UPDATE refunds
                 SET postingId = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, postingId )

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundReturnInvoice(self, statementCode, returnInvoice):
        sql = u"""UPDATE refunds
                 SET returnInvoice = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, returnInvoice )

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundState(self, statementCode, state):
        sql = u"""UPDATE refunds
                 SET state = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, state)

        self.MakeLocalDbUpdateQueue(sql)

    def SetRefundPostingId(self, statementCode, postingId):
        sql = u"""UPDATE refunds
                 SET postingId = '{1}'
                 WHERE statementCode = '{0}'""".format(statementCode, postingId )

        self.MakeLocalDbUpdateQueue(sql)

    def DeleteRefund(self, statementCode):
        sql = u"""DELETE FROM refunds
                 WHERE statementCode = '{0}'""".format(statementCode)

        self.MakeLocalDbUpdateQueue(sql)

    def GetDeliveries(self):
        sql = """SELECT * FROM deliveries"""

        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def GetDeliveriesByDate(self, date):
        sql = """SELECT * FROM deliveries
                 WHERE date > '{0}'""".format(date)

        data = self.MakeLocalDbGetInfoQueue(sql)
        return data


    def SetDeliveryState(self, id, state):
        sql = u"""UPDATE deliveries
                 SET state = '{0}'
                 WHERE id = {1}""".format(state, id)

        self.MakeLocalDbUpdateQueue(sql)

    def SetDeliveryInvoice(self, id, invoice):
        sql = u"""UPDATE deliveries
                 SET invoice = '{0}'
                 WHERE id = {1}""".format(invoice, id)

        self.MakeLocalDbUpdateQueue(sql)

    def SetDeliveryPostingId(self, id, postingId):
        sql = u"""UPDATE deliveries
                 SET postingId = '{1}'
                 WHERE id = {0}""".format(id, postingId )

        self.MakeLocalDbUpdateQueue(sql)

    def SetDeliveryAcceptance(self, id, acceptance):
        sql = u"""UPDATE deliveries
                 SET acceptance = {1}
                 WHERE id = {0}""".format(id, acceptance )

        self.MakeLocalDbUpdateQueue(sql)

    def DeleteDelivery(self, id):
        sql = u"""DELETE FROM deliveries
                 WHERE id = {0}""".format(id)

        self.MakeLocalDbUpdateQueue(sql)

    def GetRussianPostRefunds(self):
        sql = """SELECT * FROM post_refunds"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def AddRussianPostRefund(self, params):
        sql = u"""INSERT INTO post_refunds(date, orderReference, items, trackingNumber, state)
                  VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')""".format(params["date"], params["orderReference"],params["items"], params["trackingNumber"],params["state"])

        self.MakeLocalDbUpdateQueue(sql)

    def SetRussianPostRefundState(self, trackingNumber, state):
        sql = u"""UPDATE post_refunds
                 SET state = '{0}'
                 WHERE trackingNumber = '{1}'""".format(state, trackingNumber)

        self.MakeLocalDbUpdateQueue(sql)


    ####################################
    ########### Sdek ###################
    ####################################

    def GetSdekCityCode(self, name):
        sql = u"""SELECT id FROM sdek_cities_codes
                  WHERE city_name = '{0}'""".format(name)
        data = self.MakeLocalDbGetInfoQueue(sql)

        if len(data) == 0:
            sql = u"""SELECT id FROM sdek_cities_codes
                  WHERE full_name like '%{0}%'""".format(name)
            data = self.MakeLocalDbGetInfoQueue(sql)
        return [str(x[0]) for x in data]

    def GetCityBySdekCode(self, code):
        sql = u"""SELECT city_name FROM sdek_cities_codes
                  WHERE id = {0}""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetCityFromId(self, id):
        sql = """SELECT city_name FROM sdek_cities_codes
                 WHERE id = {0}""".format(id)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    ####################################
    ########### Requests ###############
    ####################################

    def GetRequests(self):
        sql = u"""SELECT * FROM requests"""
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data

    def AddRequest(self, contractor):
        sql = u"""INSERT INTO requests(contractor)
                  VALUES ('{0}')""".format(contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def SetRequestSended(self, contractor, sended):
        sql = u"""UPDATE requests
                  SET emailSended = {0}
                  WHERE contractor = '{1}'""".format(sended, contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def GetRequestPaid(self, contractor):
        sql = u"""SELECT paid FROM requests
                  WHERE contractor = '{1}'""".format(paid, contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def SetRequestPaid(self, contractor, paid):
        sql = u"""UPDATE requests
                  SET paid = {0}
                  WHERE contractor = '{1}'""".format(paid, contractor)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteAllRequests(self):
        sql = u"""DELETE FROM requests"""
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteRequest(self, id):
        sql = u"""DELETE FROM requests
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ########### Aliexpress ###############
    ####################################

    def GetAliexpressOrders(self):
        sql = u"""SELECT * FROM aliexpress"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetAliexpressOrderState(self, trackNum, state):
        sql = u"""UPDATE aliexpress
                  SET state = '{0}'
                  WHERE trackingCode = '{1}'""".format(state, trackNum)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteAliexpressOrder(self, trackNum):
        sql = u"""DELETE FROM aliexpress
                  WHERE trackingCode = '{0}'""".format(trackNum)
        self.MakeLocalDbUpdateQueue(sql)


    ####################################
    ########### Reviews ################
    ####################################

    def GetLocalReviews(self):
        sql = """SELECT * FROM reviews"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetProductLocalReviews(self, name):
        sql = u"""SELECT * FROM reviews
                 WHERE productName = '{0}'""".format(name)
        return self.MakeLocalDbGetInfoQueue(sql)

    def AddLocalReview(self, review):
        sql = u"""INSERT INTO reviews(productName, site, author, text, grade, title, date)
              VALUES ('{0}','{1}','{2}','{3}',{4},'{5}','{6}')""" \
            .format(review["name"], review["site"], review["author"], review["content"],review["grade"], review["title"], review["date"] )
        self.MakeLocalDbUpdateQueue(sql)

    def GetProductReviews(self, name):
        id_product = self.GetProductIdQuick(name)
        if id_product == -1:
            return -1
        sql = u"""SELECT * ps_product_comment
                  WHERE id_product = {0}""".format(id_product)
        return self.MakeGetInfoQueue(sql)

    def IsExistingProductReview(self, id_product, review):
        id, productName, site, author, text,grade, title, date = review
        author_ = u"{0} ({1})".format( author, site)
        sql = u"""SELECT * FROM ps_product_comment
                  WHERE customer_name = '{0}' AND id_product = {1} AND date_add = '{2}'""".format(author_, id_product, date)
        return len(self.MakeGetInfoQueue(sql)) > 0

    def IsExistingLocalProductReview(self, review):
        productName, site, author, text, grade, title, date = review
        sql = u"""SELECT * FROM reviews
                  WHERE productName = '{0}' AND site = '{1}' AND author = '{2}'""".format(productName, site, author)
        return len(self.MakeLocalDbGetInfoQueue(sql)) > 0

    ####################################
    ########### Contentmonster #########
    ####################################

    def GetContentmonsterCategoriesOrders(self):
        sql = """SELECT * FROM contentmonster"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetContentmonsterOrderState(self, orderId, state):
        sql = u"""UPDATE contentmonster
                  SET state = '{0}'
                  WHERE orderId = {1}""".format(state, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderId(self, id, orderId):
        sql = u"""UPDATE contentmonster
                  SET orderId = {0}
                  WHERE id = {1}""".format(orderId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderText(self, orderId, text):
        sql = u"""UPDATE contentmonster
                  SET text = '{0}'
                  WHERE orderId = {1}""".format(text, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderUniqueness(self, orderId, uniqueness):
        sql = u"""UPDATE contentmonster
                  SET uniqueness = {0}
                  WHERE orderId = {1}""".format(uniqueness, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterOrderDate(self, orderId, date):
        sql = u"""UPDATE contentmonster
                  SET date = '{0}'
                  WHERE orderId = {1}""".format(date, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteContentmonsterOrder(self, orderId):
        sql = u"""DELETE FROM contentmonster
                  WHERE orderId = {0}""".format(orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def AddContentmonsterOrder(self, data):
        if "orderId" not in data.keys():
            data["orderId"] = "NULL"
        sql = u"""INSERT INTO contentmonster(site, storefrontId, cityName, categories, orderCategories, orderId)
                  VALUES ('{}',{},'{}','{}','{}','{}')""".format(
                  data["site"], data["storefrontId"],data["cityName"],data["categories"], data["orderCategories"], data["orderId"])
        self.MakeLocalDbUpdateQueue(sql)

    ####### статьи #######

    def GetContentmonsterArticlesOrders(self):
        sql = """SELECT * FROM contentmonster_articles"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetContentmonsterArticleState(self, orderId, state):
        sql = u"""UPDATE contentmonster_articles
                  SET state = '{0}'
                  WHERE orderId = {1}""".format(state, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleDate(self, orderId, date):
        sql = u"""UPDATE contentmonster_articles
                  SET date = '{0}'
                  WHERE orderId = {1}""".format(date, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleText(self, orderId, text):
        sql = u"""UPDATE contentmonster_articles
                  SET text = '{0}'
                  WHERE orderId = {1}""".format(text, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleTheme(self, orderId, theme):
        sql = u"""UPDATE contentmonster_articles
                  SET theme = '{0}'
                  WHERE orderId = {1}""".format(theme, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterArticleOrderId(self, id, orderId):
        sql = u"""UPDATE contentmonster_articles
                  SET orderId = {0}
                  WHERE id = {1}""".format(orderId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddContentmonsterArticleOrder(self, data):
        sql = u"""INSERT INTO contentmonster_articles(site, category, date)
                  VALUES ('{0}','{1}','{2}','{3}','{4}')""".format(
                  data["site"], data["category"],data["cityName"],data["categories"],data["orderId"])
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteContentmonsterArticleOrder(self, orderId):
        sql = u"""DELETE FROM contentmonster_articles
                  WHERE orderId = {0}""".format(orderId)
        self.MakeLocalDbUpdateQueue(sql)

    ####### товары #######

    def GetContentmonsterProductsOrders(self):
        sql = """SELECT * FROM contentmonster_products"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetContentmonsterProductsSiteOrders(self, siteUrl):
        sql = """SELECT * FROM contentmonster_products
                 WHERE site = '{}'""".format(siteUrl)
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetContentmonsterProductsSiteUnprocessedOrders(self, siteUrl):
        sql = """SELECT * FROM contentmonster_products
                 WHERE site = '{}' AND (orderId is NULL or orderId = '')""".format(siteUrl)
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetContentmonsterProductsOrderId(self, id, orderId):
        sql = u"""UPDATE contentmonster_products
                  SET orderId = {}
                  WHERE id = {}""".format(orderId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddContentmonsterProductsOrder(self, siteUrl, products, shortDescriptions = 0):
        sql = u"""INSERT INTO contentmonster_products(site, products, shortDescriptions)
                  VALUES ('{}','{}', {})""".format(siteUrl, products, shortDescriptions)
        self.MakeLocalDbUpdateQueue(sql)

    def SetContentmonsterProductsOrderState(self, orderId, state):
        sql = u"""UPDATE contentmonster_products
                  SET state = '{0}'
                  WHERE orderId = {1}""".format(state, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteContentmonsterProductsOrder(self, id):
        sql = u"""DELETE FROM contentmonster_products
                  WHERE id = {}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## Reindex #############
    ####################################

    def AddReindex(self, data):
        values = u""
        if "categories" not in data.keys():
            values += "NULL"
        else:
            values += u"'{0}'".format(data["categories"])
        values += ","
        if "products" not in data.keys():
            values += "NULL"
        else:
            values += u"'{0}'".format(data["products"])
        values += ","
        if "otherPages" not in data.keys():
            values += "NULL"
        else:
            values += u"'{0}'".format(data["otherPages"])

        sql = u"""INSERT INTO reindex(site, shopName, categories, products, otherPages)
                  VALUES ('{0}', '{1}', {2})""".format(data["site"], data["shopName"], values)

        self.MakeLocalDbUpdateQueue(sql)

    def GetReindexData(self):
        sql = u"""SELECT * FROM reindex"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetReindexLastLaunchDate(self, id, date):
        sql = u"""UPDATE reindex
                  SET lastLaunchDate = '{0}'
                  WHERE id = {1}""".format(date, id)
        self.MakeLocalDbUpdateQueue(sql)

    def SetReindexLastShopId(self, id, lastShopId):
        sql = u"""UPDATE reindex
                  SET lastStorefrontId = {0}
                  WHERE id = {1}""".format(lastShopId, id)
        self.MakeLocalDbUpdateQueue(sql)

    def DeleteReindex(self, id):
        sql = u"""DELETE FROM reindex
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ################ Feeds #############
    ####################################

    def GetFeedsData(self):
        sql = u"""SELECT * FROM feeds"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetFeedDate(self, id, date):
        sql = u"""UPDATE feeds
                  SET date = '{}'
                  WHERE id = {}""".format(date, id)
        return self.MakeLocalDbUpdateQueue(sql)

    def SetFeedCity(self, id, city):
        sql = u"""UPDATE feeds
                  SET city = '{}'
                  WHERE id = {}""".format(city, id)
        return self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## СДЭК ################
    ####################################

    def GetSdekPvzTable(self):
        sql = """SELECT * FROM sdek_pvz
                 WHERE Type = 'PVZ'"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetCitySdekPvzList(self, pvzTable, city):
        city = city.lower().strip()
        pvzList = [x for x in pvzTable if (x[3].lower().startswith(city) and x[3].lower().find(",") != -1) or (x[3].lower() == city and x[3].lower().find(",") == -1)]
        return pvzList

    def GetCityFromCode(self, code):
        sql = """SELECT cityName FROM sdek_pvz
                 WHERE cityCode = {0}""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    def GetCityFromId(self, id):
        sql = """SELECT city_name FROM sdek_cities_codes
                 WHERE id = {0}""".format(id)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    def GetPvzCityCode(self, code):
        sql = """SELECT cityCode FROM sdek_pvz
                 WHERE pvzCode = '{0}'""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        else:
            return data[0][0]

    def GetPvzByCode(self, code):
        sql = """SELECT * FROM sdek_pvz
                 WHERE pvzCode = '{0}'""".format(code)
        data = self.MakeLocalDbGetInfoQueue(sql)
        return data[0]

    ####################################
    ######## Update products ###########
    ####################################

    def GetUpdateProducts(self):
        sql = u"""SELECT * FROM updateProducts"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def DeleteUpdateProducts(self, id):
        sql = u"""DELETE FROM updateProducts
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddUpdateProducts(self, site, productsIds):
        sql = u"""INSERT INTO updateProducts(site, productsIds)
                  VALUES ('{}', '{}')""".format(site, productsIds)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## Features ############
    ####################################

    def GetFeatures(self):
        sql = u"""SELECT * FROM features"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetDoneFeatures(self):
        sql = u"""SELECT * FROM features
                  WHERE done = 1"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def DeleteFeatureRow(self, id):
        sql = u"""DELETE FROM features
                  WHERE id = {0}""".format(id)
        self.MakeLocalDbUpdateQueue(sql)

    def AddFeatureRow(self, productName, category, features, url):
        sql = u"""INSERT INTO features(productName, category, features, url)
                  VALUES ('{}', '{}', '{}', '{}')""".format(productName, category, features, url)
        self.MakeLocalDbUpdateQueue(sql)

    def GetAllFeatures(self):
        sql = u"""SELECT * FROM allFeatures"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def AddFeatureToAllFeatures(self, name, values):
        sql = u"""INSERT INTO allFeatures(name, featureValues)
                  VALUES ('{}', '{}')""".format(name, values)
        self.MakeLocalDbUpdateQueue(sql)

    def SetFeatureValues(self, name, values):
        sql = u"""UPDATE allFeatures
                  SET featureValues = '{}'
                  WHERE name = '{}'""".format(values, name)
        self.MakeLocalDbUpdateQueue(sql)


    ####################################
    ############## New orders ############
    ####################################

    def AddNewOrder(self, siteUrl, orderId):
        sql = u"""INSERT INTO newOrders(siteUrl, orderId)
                VALUES ('{}', '{}')""".format(siteUrl, orderId)
        self.MakeLocalDbUpdateQueue(sql)

    def GetNewOrders(self, siteUrl):
        sql = u"""SELECT orderId FROM newOrders
                  WHERE siteUrl = '{}'""".format(siteUrl)
        data = self.MakeLocalDbGetInfoQueue(sql)
        return [i[0] for i in data]

    ####################################
    ######## Products Uniqueness #######
    ####################################

    def AddProductUniqueness(self, data):
        sql = u"""INSERT INTO products_uniqueness(site, productId, name, url, description, uniquenessInfo)
                  VALUES ('{}',{},'{}','{}','{}','{}')""".format(data["site"], data["productId"], data["name"], data["url"],data["description"],data["uniquenessInfo"])
        self.MakeLocalDbUpdateQueue(sql)

    def GetProductsUniqueness(self):
        sql = u"""SELECT * FROM products_uniqueness"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetNonuniqueSiteProducts(self, siteUrl):
        sql = u"""SELECT * FROM products_uniqueness
                  WHERE site = '{}' AND uniquenessInfo <> ''""".format(siteUrl)
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetSiteNeedToRewriteProducts(self, siteUrl):
        sql = u"""SELECT * FROM products_uniqueness
                  WHERE site = '{}' AND needToRewrite = 1 AND processed = 0""".format(siteUrl )
        return self.MakeLocalDbGetInfoQueue(sql)

    def SetProductProcessed(self, siteUrl, productId):
        sql = u"""UPDATE products_uniqueness
                  SET processed = 1
                  WHERE site = '{}' AND productId = {}""".format(siteUrl, productId)
        self.MakeLocalDbUpdateQueue(sql)

    ####################################
    ############## Other #############
    ####################################

    def GetSdekPvzTable(self):
        sql = """SELECT * FROM sdek_pvz"""
        return self.MakeLocalDbGetInfoQueue(sql)

    def GetCitySdekPvzList(self, pvzTable, city):
        city = city.lower()
        pvzList = [x for x in pvzTable if (x[3].lower().startswith(city) and x[3].lower().find(",") != -1) or (x[3].lower() == city and x[3].lower().find(",") == -1)]
        return pvzList

    def GetConfigurationValue(self, name):
        sql = u"""SELECT value FROM ps_configuration
                  WHERE name = '{}'""".format(name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) > 0:
            return data[0][0]

        return -1

    def SetConfigurationValue(self, name, value):
        sql = u"""UPDATE ps_configuration
                  SET value = "{}"
                  WHERE name = "{}\"""".format(value, name)
        data = self.MakeUpdateQueue(sql)

    def GetModuleId(self, name):
        sql = """SELECT id_module FROM ps_module
                 WHERE name = '{}'""".format(name)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    ############################################################################
    ################################    API    #################################
    ############################################################################

    def Parser_ProcessProduct(self, productParams, productCategory, warehouseCategory, productUrl, cleanItemName = True, message = ""):
        self.message = message
        categoryName = productCategory + "_" + warehouseCategory.lower()
        productParams["categories"] = categoryName
        productParams["productUrl"] = productUrl

        if "original_name" in productParams.keys():
            productId = self.GetProductIdQuick(productParams["original_name"], cleanItemName)
        else:
            productId = self.GetProductIdQuick(productParams["name"], cleanItemName)

        if productId != -1:#обновление товара
            skus = self.GetProductSkusIds(productId)
            article = self.GetSkuArticle(skus[0])
            contractor = commonLibrary.GetContractorName(article)
            if contractor in [u"Наш товар", u"Диктос"]:
                return [productId, self.message]

            quantity = 1
            productName = commonLibrary.CleanItemName(productParams["name"])
            if "quantity" in productParams.keys():
                quantity = productParams["quantity"]

            if quantity == -1:
                self.logging.LogInfoMessage(u"Товар '{}' ({}) удален".format(productName, productId))
                self.message += "Товар '{}' ({}) удален\n".format(productName.encode("utf-8"), productId)
                self.DeleteProduct(productId)
                return [False, self.message]

            if self.IsProductInStock(productId) == False:
                if quantity > 0:
                    self.SetProductAvailableForOrder(productId, True)
                    self.logging.LogInfoMessage(u"Товар '{}' ({}) активирован".format(productName, productId))
                    self.message += "Товар '{}' ({}) активирован\n".format(productName.encode("utf-8"), productId)
            else:
                if quantity == 0:
                    self.logging.LogInfoMessage(u"Товар '{}' ({}) деактивирован".format(productName, productId))
                    self.message += "Товар '{}' ({}) деактивирован\n".format(productName.encode("utf-8"), productId)
                    self.SetProductAvailableForOrder(productId, False)
##            if "characteristics" in productParams.keys():
##                self.SetProductCharacteristics(productId, productParams["characteristics"])
            if productParams["wholesale_price"] != 0:
                name, price, wholesale_price, description, url = self.GetProductDetails(productId)
                if int(wholesale_price) != int(productParams["wholesale_price"]) or int(price) != int(productParams["price"]):
                    self.SetProductPrices(productId, productParams["wholesale_price"], productParams["price"])
                    self.logging.Log(u"""Товар '{}' ( {} ) обновлен: старая закупочная цена = {}, новая закупочная цена = {}, старая розничная цена = {}, новая розничная цена = {}""".
                    format( productName, productId, wholesale_price, productParams["wholesale_price"], price, productParams["price"]))
                    self.message += """Товар '{}' ( {} ) обновлен: старая закупочная цена = {}, новая закупочная цена = {}, старая розничная цена = {}, новая розничная цена = {}\n""". \
                    format( productName.encode("utf-8"), productId, wholesale_price, productParams["wholesale_price"], price,productParams["price"])
            return [productId, self.message]

        else:#добавление товара
            if "quantity" in productParams.keys() and productParams["quantity"] <= 0:
                self.logging.Log(u"Товара '{0}' нет в наличии".format(productParams["name"]))
                self.message += "Товара '{0}' нет в наличии\n".format(productParams["name"].encode("utf-8"))
                return [False, self.message]
            contractor = commonLibrary.GetContractorName(productParams["article"])
##            if contractor in [u"Логос"]:
##                return [False, self.message]
            categoryId = self.GetCategoryId(categoryName)
            if categoryId == -1:#добавляем категорию
                data = {}
                data["name"] = categoryName
                data["parentName"] = warehouseCategory
                data["url"] = commonLibrary.GetLinkRewriteFromName(categoryName)
                data["description"] = ""
                data["include_sub_categories"] = "1"
                data["status"] = 0
                catId = self.AddCategory(data)
                if catId == -1:
                    self.message += "[ERROR] Ошибка добавления категории '{}'\n".format(categoryName.encode("utf-8"))
                    self.logging.LogErrorMessage(u"Ошибка добавления категории: " + categoryName)
                    return [False, self.message]
                storefrontsUrls = self.GetStorefrontsUrls()
                for storefrontUrl in storefrontsUrls:
                    self.AddCategoryRoute(catId, storefrontUrl)
                self.message += "Добавлена категория '{}'\n".format(categoryName.encode("utf-8"))
                self.logging.LogInfoMessage(u"Добавлена категория: " + categoryName)

            originalName = commonLibrary.CleanItemName( productParams["name"])
            productParams["summary"] = productParams["name"]
            url = commonLibrary.GetLinkRewriteFromName(originalName)
            i = 1
            while True:
                sql = u"""select id from shop_product
                         where url = '{}'""".format(url)
                rows = self.MakeGetInfoQueue(sql)
                if len(rows) == 0:
                    break
                url += "-{}".format(i)
                i += 1
            productParams["url"] = url
##            print productParams["url"]
            productParams["status"] = 0
            productParams["sku_type"] = 1
##            productParams["skus"] = "[{'sku': '', 'name': '', 'price': 1, 'available': 1}]"
##            print productParams["skus"]
            productId = self.AddProduct(productParams)
            if productId == -1:
                self.logging.LogErrorMessage(u"Ошибка добавления товара '{}'\n".format(productParams["name"]))
                self.message +=  "Ошибка добавления товара '{}'\n".format(productParams["name"].encode("utf-8"))
                return [False, self.message]

            try:
                self.logging.Log(u"Добавлен товар '{0}' ({1})".format(productParams["name"], str(productId)))
                self.message += "Добавлен товар '{0}' ({1})\n".format(productParams["name"].encode("utf-8"), str(productId))
            except:
                self.logging.Log(u"Добавлен товар {0}".format(productId))

            self.SetProductPrices(productId, productParams["wholesale_price"], productParams["price"])
            self.SetProductArticle(productId, productParams["article"] + str(productId))
            self.SetProductSkuType(productId, 0)

            if "original_name" in productParams.keys():
                self.SetProductOriginalName(productId, productParams["original_name"])
            else:
                self.SetProductOriginalName(productId, originalName)

            if "videoContent" in productParams.keys():
                self.AddProductVideoPage(productId, productParams["videoContent"])

            if "features" in productParams.keys():
                self.DeleteProductFeatures(productId)
                for feature in productParams["features"]:
                    self.AddProductFeatureValue(productId, feature, productParams["features"][feature])

            for imgUrl in productParams["images"]:
                result = commonLibrary.DownloadImage(imgUrl, "")
                if result:
                    picName = commonLibrary.GetPictureNameFromUrl(imgUrl)
                    pictureId = self.AddProductImage(productId, picName)
                    os.unlink(picName)

            self.SetProductTypeId(productId)
            self.SetProductBadge(productId, 'new')
            return [productId, self.message]

#################################################################################

    ############################
    ######### разное #########
    ############################

    def GetCitiesWithCourierDelivery(self):
        sql = """SELECT shop_plugin_settings.value FROM shop_plugin_settings
                 LEFT JOIN shop_plugin
                 ON shop_plugin.id = shop_plugin_settings.id
                 WHERE type = 'shipping' AND plugin = 'courier' AND shop_plugin_settings.name = 'rate_zone'"""
        list_ = [json.loads( i[0])["city"] for i in self.MakeGetInfoQueue(sql)]
        return list_

    def GetAddedSdekPvzCodes(self):
        sql = u"""SELECT value FROM shop_plugin_settings
                  WHERE name = 'additional'"""
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def GetRegions(self):
        sql = u"""SELECT code, name FROM wa_region
                  WHERE country_iso3 = 'rus'"""
        return self.MakeGetInfoQueue(sql)

    def GetRegionCode(self, region):
        regions = self.GetRegions()
        words = [u"респ.", u"край", u"обл.", u"область", u"авт. округ", u"- Кузбасс"]
        for word in words:
            region = region.replace(word, u"")
        region = region.replace("  ", " ").strip()
        if region == u"Удмуртия":
            region = u"Удмуртская"
        if region == u"Чувашия":
            region = u"Чувашская"
        for r in regions:
            code, name = r
            if name.lower().startswith(region.lower()):
                return code

        return -1

    ############################
    ######### Plugin #########
    ############################

    def AddPlugin(self, data):
        sort = self.GetLastInsertedId("shop_plugin", "sort") + 1
        sql = u"""INSERT INTO shop_plugin(type, plugin, name, description, logo, status, sort, options)
                  VALUES ('{}', '{}', '{}', '{}', '{}', {}, {}, '{}')""".format(
                  data["type"], data["plugin"], data["name"], data["description"], data["logo"], data["status"], sort, data["options"] )
        self.MakeUpdateQueue(sql)
        id = self.GetLastInsertedId("shop_plugin", "id")
        return id

    def UpdatePluginSetting(self, id, name, value):
        pass

    def AddPluginSetting(self, id, name, value):
        sql = u"""INSERT INTO shop_plugin_settings(id, name, value)
                  VALUES ({},'{}','{}')""".format(id, name, value)
        self.MakeUpdateQueue(sql)

    def DeletePlugin(self, id):
        sql = """DELETE FROM shop_plugin
                 WHERE id = {}""".format(id)
        self.MakeUpdateQueue(sql)
        sql = """DELETE FROM shop_plugin_settings
                 WHERE id = {}""".format(id)
        self.MakeUpdateQueue(sql)

    def GetPluginValue(self, id, name ):
        sql = """SELECT value FROM shop_plugin_settings
                 WHERE id = {} AND name = '{}'""".format(id, name)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetPluginStatus(self, id, status):
        sql = """UPDATE shop_plugin
                 SET status = {}
                 WHERE id = {}""".format(status, id)
        self.MakeUpdateQueue(sql)

    def GetPluginIdByPVZCode(self, code ):
        sql = u"""SELECT shop_plugin.id FROM shop_plugin
                 LEFT JOIN shop_plugin_settings
                 ON shop_plugin.id = shop_plugin_settings.id
                 WHERE shop_plugin_settings.value = '{}' AND shop_plugin_settings.name = 'additional'""".format(code)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    ############################
    ######### заказы #########
    ############################

    def GetOrderIdByTrackingNumber(self, trackingNumber):
        sql = """SELECT order_id FROM shop_order_params
                 WHERE name = 'tracking_number' AND value = '{}'""".format(trackingNumber)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetOrderStateDate(self, orderId, state):
        stateId = self.ordersStates[state]
        sql = """SELECT datetime FROM shop_order_log
                 WHERE order_id = {0} AND after_state_id = '{1}' AND before_state_id <> '{1}' ORDER BY datetime DESC""".format(orderId, stateId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1


    def GetOrderTrackingNumber(self, orderId):
        sql = """SELECT value FROM shop_order_params
                where order_id = {} and name = 'tracking_number'""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def SetOrderTrackingNumber(self, orderId, trackingNumber):
        value = self.GetOrderTrackingNumber(orderId)
        if value != -1:
            sql = """UPDATE shop_order_params
                     SET value = '{}'
                     WHERE order_id = {} and name = 'tracking_number'""".format(trackingNumber, orderId)
        else:
            sql = u"""INSERT INTO shop_order_params(order_id, name, value)
                    VALUES ({},'tracking_number','{}')""".format(orderId, trackingNumber)
        self.MakeUpdateQueue(sql)

    def GetOrdersIds(self, states, startDate, endDate, notInState = False):
        states_ = ["'{}'".format( self.ordersStates[i]) for i in states]
        strStates = "({})".format(",".join(states_))
        if notInState == False:
            flag = ''
        else:
            flag = 'NOT'
        sql = """SELECT distinct id FROM shop_order
                 LEFT JOIN shop_order_params
                 ON shop_order.id = shop_order_params.order_id
                 WHERE shop_order_params.name IN ('storefront_decoded', 'storefront') AND shop_order_params.value like '%{}%' AND shop_order.create_datetime > '{}' AND shop_order.create_datetime < '{}'
                 AND shop_order.state_id {} IN {} ORDER BY shop_order.create_datetime""".format(self.siteUrl, startDate, endDate, flag, strStates)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def GetOrderComment(self, orderId):
        sql = """SELECT comment FROM shop_order
                 WHERE id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        note = data[0][0]
        if note == None:
            return ""
        return note

    def GetOrderParamValue(self, orderId, paramName):
        sql = """SELECT value FROM shop_order_params
                 WHERE order_id = {} AND name = '{}'""".format(orderId, paramName)
        data = self.MakeGetInfoQueue(sql)
        if len(data) > 0:
            return data[0][0]
        return -1

    def SetOrderComment(self, orderId, comment):
        sql = u"""UPDATE shop_order
                 SET comment = '{}'
                 WHERE id = {}""".format(comment, orderId)
        self.MakeUpdateQueue(sql)

    def GetAllOrdersIds(self, bCurrentDomain = True):
        if bCurrentDomain:
            sql = """SELECT id FROM shop_order
                     LEFT JOIN shop_order_params
                     ON shop_order.id = shop_order_params.order_id
                     WHERE shop_order_params.name = 'storefront' AND shop_order_params.value like '{}%'""".format(self.siteUrl)
        else:
            sql = """SELECT id FROM shop_order"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def DeleteOrder(self, orderId):
        tables = ["shop_order",
                  "shop_order_items",
                  "shop_order_item_codes",
                  "shop_order_log",
                  "shop_order_log_params",
                  "shop_order_params",
                  "shop_promo_orders"]
        for table in tables:
            if table == "shop_order":
                field = "id"
            else:
                field = "order_id"
            sql = """DELETE FROM {}
                     WHERE {} = {}""".format(table, field, orderId)
            self.MakeUpdateQueue(sql)

    def AddOrder(self, orderData, orderParams):
        sql = u"""INSERT INTO shop_order( contact_id, create_datetime, update_datetime, state_id, total, currency)
        VALUES ({}, '{}', '{}', '{}', {}, 'RUB')""".format(
        orderData["contact_id"], orderData["datetime"], orderData["datetime"],orderData["state_id"], orderData["total"])
        self.MakeUpdateQueue(sql)
        orderId = self.GetLastInsertedId("shop_order", "id")

        for product in orderData["products"]:
            productId, name, sku_id, sku_code, quantity, price, purchase_price = product
            sql = u"""INSERT INTO shop_order_items(order_id, name, product_id, sku_id, sku_code, type, price, quantity, purchase_price)
                      VALUES ({}, '{}', {}, {}, '{}', 'product', {}, {}, {})""".format(
                    orderId, name, productId, sku_id, sku_code, price, quantity, purchase_price)
            self.MakeUpdateQueue(sql)

        for key in orderParams.keys():
            value = orderParams[key]
            sql = u"""INSERT INTO shop_order_params(order_id, name, value)
                         VALUES ({}, '{}', '{}') """.format(orderId, key, value)
            self.MakeUpdateQueue(sql)

        return orderId

    def GetOrderCreationDate(self, orderId):
        sql = """SELECT create_datetime FROM shop_order
                 WHERE id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetOrderDiscountSum(self, orderId):
        sql = """SELECT total_discount FROM shop_order_items
                 WHERE order_id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        discountSum = 0
        for discount in data:
            discountSum += int(discount[0])
        return discountSum

    def SetOrderPaid(self, orderId, date):
        if date == None:
            sql = """UPDATE shop_order
                     SET paid_year = NULL, paid_quarter = NULL, paid_month = NULL, paid_date = NULL
                     WHERE id = {}""".format(orderId)
        else:
            if type(date) != type("123"):
                date = date.strftime('%Y-%m-%d')
            year, month, day = date.split("-")
            month = int(month)
            if month in [1,2,3]:
                quarter = 1
            if month in [4,5,6]:
                quarter = 2
            if month in [7,8,9]:
                quarter = 3
            if month in [10,11,12]:
                quarter = 4

            sql = """UPDATE shop_order
                     SET paid_year = {}, paid_quarter = {}, paid_month = {}, paid_date = '{}'
                     WHERE id = {}""".format(year, quarter, month, date, orderId)
        self.MakeUpdateQueue(sql)

    def GetOrderTotalCost(self, orderId):
        sql = """SELECT total FROM shop_order
                 WHERE id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetOrderStateId(self, orderId):
        sql = """SELECT state_id FROM shop_order
                 WHERE id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetOrderStateName(self, orderId):
        stateId = self.GetOrderStateId(orderId)
        for stateName in self.ordersStates.keys():
            if self.ordersStates[stateName] == stateId:
                return stateName
        return -1

    def GetOrderCustomerData(self, orderId):
        sql = """SELECT wa_contact.firstname, wa_contact.lastname, wa_contact_data.value, email, wa_contact.id FROM wa_contact
                 LEFT JOIN shop_order
                 ON wa_contact.id = shop_order.contact_id
                 LEFT JOIN wa_contact_data
                 ON wa_contact.id = wa_contact_data.contact_id
                 LEFT JOIN wa_contact_emails
                 ON wa_contact.id = wa_contact_emails.contact_id
                 WHERE shop_order.id = {} AND wa_contact_data.field = 'phone'""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0]
        except:
            return -1

    def GetOrderStatesNames(self, orderId):
        sql = """SELECT after_state_id FROM shop_order_log
                WHERE order_id = {} AND after_state_id <> before_state_id order BY datetime""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return [x[0] for x in data]
        except:
            return -1

    def GetOrderDeliveryCost(self, orderId):
        sql = """SELECT shipping FROM shop_order
                 WHERE id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetOrderDeliveryData(self, orderId):
        sql = """SELECT value FROM shop_order_params
                 WHERE order_id = {} AND name IN ('shipping_address.city', 'shipping_address.street', 'shipping_id', 'shipping_name')""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0].replace("_","") for i in data]

    def GetOrderItems(self, orderId):
        sql = """SELECT product_id, sku_id, name, price, quantity, purchase_price  FROM shop_order_items
                 WHERE order_id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        data1 = []
        for i in range(0, len(data)):
            product_id, sku_id, name, price, quantity, purchase_price = data[i]
            quantity = int(quantity)
            data1.append([product_id, sku_id, name, price, quantity, purchase_price])
        return data1

    def ApplyActionToOrder(self, orderId, action):
        data = {"id":orderId, "action":action}
        result = self.POST("shop.order.action", data)
        json_ = json.loads( result.text)
##        print result.text
        return json_

    def IsOrderPaid(self, orderId):
        sql = """SELECT paid_date FROM shop_order
                 WHERE id = {}""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0] != None

    def GetOrderShippingCity(self, orderId):
        sql = """SELECT value FROM shop_order_params
                 WHERE order_id = {} AND name = 'shipping_address.city'""".format(orderId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def SetOrderState(self, orderId, state):
        stateId = self.ordersStates[state]
        sql = """UPDATE shop_order
                 SET state_id = '{}'
                 WHERE id = {}""".format(stateId, orderId)
        self.MakeUpdateQueue(sql)

    def GetOrderState(self, orderId):
        sql = """SELECT state_id FROM shop_order
                 WHERE id = {}""".format(orderId)
        stateId = self.MakeGetInfoQueue(sql)[0][0]
        return [i for i in self.ordersStates if self.ordersStates[i] == stateId][0]

    ############################
    ######### Вывод записей блога #########
    ############################

    def AddPostToCategory(self, categoryId, postId):
        sql = u"""INSERT INTO shop_pposts_category(category_id, post_id)
                  VALUES ({}, {})""".format(categoryId, postId)
        self.MakeUpdateQueue(sql)

    def GetCategoryPosts(self, categoryId):
        sql = u"""SELECT post_id FROM shop_pposts_category
                  WHERE category_id = {}""".format(categoryId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    ############################
    ######### покупатели #########
    ############################

    def AddCustomer(self, data):
        sql = u"""INSERT INTO wa_contact(name, firstname, lastname, create_datetime, create_app_id)
                  VALUES ('{}', '{}', '{}', '2015-07-10 09:12:5', 'shop')""".format(
                  u"{} {}".format(data["lastname"],data["firstname"] ), data["firstname"],data["lastname"])
        self.MakeUpdateQueue(sql)
        id = self.GetLastInsertedId("wa_contact", "id")
        sql = u"""INSERT INTO wa_contact_categories(category_id, contact_id)
                  VALUES (1, {})""".format(id)
        self.MakeUpdateQueue(sql)
        sql = u"""INSERT INTO wa_contact_data(contact_id, field, ext, value)
                  VALUES ({}, '{}', '{}', '{}')""".format(id, "phone", "", data["phone_mobile"])
        self.MakeUpdateQueue(sql)
        sql = u"""INSERT INTO wa_contact_data(contact_id, field, ext, value)
                  VALUES ({}, '{}', '{}', '{}')""".format(id, "address:city", "shipping", data["city"])
        self.MakeUpdateQueue(sql)
        sql = u"""INSERT INTO wa_contact_emails(contact_id, email)
                  VALUES ({}, '{}')""".format(id, data["email"])
        self.MakeUpdateQueue(sql)
        return id

    def DeleteCustomer(self, customerId):
        tables = ["wa_contact",
                  "wa_contact_categories",
                  "wa_contact_data",
                  "wa_contact_data_text",
                  "wa_contact_emails",
                  "wa_contact_events",
                  "wa_contact_settings",
                  "wa_contact_waid"]
        for table in tables:
            if table == "wa_contact":
                field = "id"
            else:
                field = "contact_id"
            sql = """DELETE FROM {}
                     WHERE {} = {}""".format(table, field, customerId)
            self.MakeUpdateQueue(sql)

    def GetCustomerId(self, phone, email = None):
        if email == None:
            sql = u"""SELECT contact_id FROM wa_contact_data
                      WHERE field = 'phone' and value = '{}'""".format(phone)
        else:
            sql = u"""SELECT wa_contact_data.contact_id FROM wa_contact_data
                      LEFT JOIN wa_contact_emails
                      ON wa_contact_data.contact_id = wa_contact_emails.contact_id
                      WHERE wa_contact_data.field = 'phone' and wa_contact_data.value = '{}' and wa_contact_emails.email = '{}'""".format(phone, email)
        try:
            data = self.MakeGetInfoQueue(sql)
            if len(data) == 1:
                return data[0][0]
            else:
                return[i[0] for i in data]
        except:
            return -1

    def GetCustomerOrdersIds(self, customerId ):
        sql = """SELECT id FROM shop_order
                 WHERE contact_id = {}""".format(customerId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def IsCustomerInShop(self, customerId):
        sql = """SELECT * FROM shop_customer
                 WHERE contact_id = {}""".format(customerId)
        data = self.MakeGetInfoQueue(sql)
        return len(data) > 0

    def AddCustomerToShop(self, customerId, data):
        sql = u"""INSERT INTO shop_customer(contact_id, total_spent,  number_of_orders, last_order_id)
                  VALUES ({}, {}, {}, {})""".format(
                    customerId, data["total_spent"], data["number_of_orders"],data["last_order_id"])
        self.MakeUpdateQueue(sql)

    def GetContactsIds(self, app):
        sql = """SELECT id FROM wa_contact
                 WHERE create_app_id = '{}'""".format(app)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def SetCustomerEmail(self, customerId, email):
        sql = u"""UPDATE wa_contact_emails
                  SET email = '{}'
                  WHERE contact_id = {}""".format(email, customerId)
        self.MakeUpdateQueue(sql)

    ######################
    ###### товары #######
    ######################

    def GetProductCategories(self, productId):
        sql = """SELECT category_id FROM shop_category_products
                WHERE product_id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def SetProductName(self, productId, name):
        sql = u"""UPDATE shop_product
                SET name = '{}'
                WHERE id = {}""".format(name, productId)
        self.MakeUpdateQueue(sql)

    def SetProductAvailableForOrder(self, productId, available):
        if available == True:
            count = "NULL"
        else:
            count = 0
        sql = """UPDATE shop_product
                 SET `count` = {}
                 WHERE id = {}""".format(count, productId)
        self.MakeUpdateQueue(sql)
        sql = """UPDATE shop_product_skus
                 SET `count` = {}
                 WHERE product_id = {}""".format(count, productId)
        self.MakeUpdateQueue(sql)


    def SetProductActive(self, productId, active):
        sql = """UPDATE shop_product
                SET status = {}
                WHERE id = {}""".format(active, productId)
        self.MakeUpdateQueue(sql)
        sql = """UPDATE shop_product_skus
                SET status = {}
                WHERE product_id = {}""".format(active, productId)
        self.MakeUpdateQueue(sql)

    def IsProductInStock(self, productId):
        sql = """SELECT `count` FROM shop_product
                 WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0] != 0
        except:
            return False

    def IsProductActive(self, productId):
        sql = """SELECT status FROM shop_product
                 WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0] != 0

    def GetProductOriginalName(self, productId):
        sql = """SELECT original_name FROM shop_product
                WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def GetProductCategoryName(self, productId):
        sql = u"""SELECT shop_category.name FROM shop_category
                  LEFT JOIN shop_product
                  ON shop_category.id = shop_product.category_id
                  WHERE shop_product.id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetSkuOriginalName(self, id):
        sql = """SELECT original_name FROM shop_product_skus
                WHERE id = {}""".format(id)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetActiveProductsIDs(self):
        sql = """SELECT DISTINCT id FROM shop_product
                 WHERE status = 1 and type_id = {} ORDER BY id ASC""".format(self.typeId)
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetAllProductsIDs(self, bCurrentDomain = True):
        if bCurrentDomain:
            sql = """SELECT DISTINCT id FROM shop_product
                     WHERE type_id = {}""".format(self.typeId)
        else:
            sql = """SELECT DISTINCT id FROM shop_product"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetProductData(self, productId):
        sql = u"""SELECT sp.name, sp.sku_id, sps.sku
                  FROM shop_product AS sp
                  LEFT JOIN shop_product_skus AS sps
                  ON sp.id = sps.product_id
                  WHERE sp.id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)[0]
        return data

    def GetProductDetails(self, productId):
        sql = u"""SELECT sp.name, sps.price, sps.purchase_price, sp.description, sp.url
                  FROM shop_product AS sp
                  LEFT JOIN shop_product_skus AS sps
                  ON sp.id = sps.product_id
                  WHERE sp.id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)[0]
        return data

    def GetProductSkuDetails(self, skuId):
        sql = u"""SELECT name, original_name, price, purchase_price
                  FROM shop_product_skus
                  WHERE id = {}""".format(skuId)
        data = self.MakeGetInfoQueue(sql)[0]
        return data

    def SetProductSkuPrices(self, skuId,  wholesalePrice = -1, price = -1):
        if wholesalePrice != -1:
            sql = """UPDATE shop_product_skus
                        SET purchase_price = {}
                        WHERE id = {}""".format(wholesalePrice, skuId)
            self.MakeUpdateQueue(sql)
        if price != -1:
            sql = """UPDATE shop_product_skus
                        SET price = {0}, primary_price = {0}
                        WHERE id = {1}""".format(price, skuId)
            self.MakeUpdateQueue(sql)
##            sql = """UPDATE shop_product
##                        SET price = {0}, min_price = {0}, max_price = {0}
##                        WHERE id = {1}""".format(price, skuId)
##            self.MakeUpdateQueue(sql)

    def SetProductDescription(self, productId, description):
        sql = u"""UPDATE shop_product
                    SET description = '{}'
                    WHERE id = {}""".format(description, productId)
        self.MakeUpdateQueue(sql)

    def SetProductSkuType(self, productId, sku_type):
        sql = """UPDATE shop_product
                    SET sku_type = {}
                    WHERE id = {}""".format(sku_type, productId)
        self.MakeUpdateQueue(sql)

    def SetProductPrices(self, productId,  wholesalePrice = -1, price = -1):
        if wholesalePrice != -1:
            sql = """UPDATE shop_product_skus
                        SET purchase_price = {}
                        WHERE product_id = {}""".format(wholesalePrice, productId)
            self.MakeUpdateQueue(sql)
        if price != -1:
            sql = """UPDATE shop_product_skus
                        SET price = {0}, primary_price = {0}
                        WHERE product_id = {1}""".format(price, productId)
            self.MakeUpdateQueue(sql)
            sql = """UPDATE shop_product
                        SET price = {0}, min_price = {0}, max_price = {0}
                        WHERE id = {1}""".format(price, productId)
            self.MakeUpdateQueue(sql)

    def SetProductCategories(self, productId, categoriesIds):
        sql = """DELETE FROM shop_category_products
                WHERE product_id = {}""".format(productId)
        self.MakeUpdateQueue(sql)

        sql = """UPDATE shop_product
                SET category_id = {}
                WHERE id = {}""".format(  categoriesIds[0], productId)
        self.MakeUpdateQueue(sql)

        for categoryId in categoriesIds:
            sql = """INSERT INTO shop_category_products(category_id, product_id)
                VALUES ({},{})""".format(categoryId, productId)
            self.MakeUpdateQueue(sql)

    def SetProductOriginalName(self, productId, originalName):
        sql = u"""UPDATE shop_product
                SET original_name = '{}'
                WHERE id = {}""".format(originalName, productId)
        self.MakeUpdateQueue(sql)

        sql = u"""UPDATE shop_product_skus
                SET original_name = '{}'
                WHERE product_id = {}""".format(originalName, productId)
        self.MakeUpdateQueue(sql)

    def SetProductSkuOriginalName(self, skuId, originalName):
        sql = u"""UPDATE shop_product_skus
                SET original_name = '{}'
                WHERE id = {}""".format(originalName, skuId)
        self.MakeUpdateQueue(sql)

    def SetProductUrl(self, productId, url):
        sql = u"""UPDATE shop_product
                SET url = '{}'
                WHERE id = {}""".format(url, productId)
        self.MakeUpdateQueue(sql)

    def SetProductTypeId(self, productId):
        sql = """UPDATE shop_product
                SET type_id = {}
                WHERE id = {}""".format(self.typeId, productId)
        self.MakeUpdateQueue(sql)

    def SetProductArticle(self, productId, article):
        sql = """UPDATE shop_product_skus
                SET sku = '{}'
                WHERE product_id = {}""".format(article, productId)
        self.MakeUpdateQueue(sql)

    def GetSkuArticle(self, skuId):
        sql = """SELECT sku FROM shop_product_skus
                WHERE id = {}""".format(skuId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetSkuName(self, skuId):
        sql = """SELECT name FROM shop_product_skus
                WHERE id = {}""".format(skuId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductSkusIds(self, productId):
        sql = """SELECT id FROM shop_product_skus
                WHERE product_id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return [i[0] for i in data]
        except:
            return -1

    def GetProductName(self, productId):
        sql = """SELECT name FROM shop_product
                WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def InitAllProducts(self):
        self.allProducts = self.GetProductsOriginalNames()

    def GetProductsOriginalNames(self):
        sql = """SELECT id, original_name
                FROM shop_product
                WHERE type_id = {}""".format(self.typeId)
        data = self.MakeGetInfoQueue(sql)
        return data

    def GetProductIdQuick(self, itemName, cleanItemName = True):
        if itemName == None:
            return -1
        if cleanItemName == True:
            itemName = commonLibrary.CleanItemName(itemName)

        for product in self.allProducts:
            productId = product[0]
            if product[1] == None:
                continue
            productName = product[1].lower()
            if cleanItemName == True:
                productName = commonLibrary.CleanItemName(productName)
            if productName.lower() == itemName.lower():
                return productId

        return -1

    def AddProduct(self, productData):
        result = self.POST("shop.product.add", productData)
        try:
            json_ = json.loads( result.text)
            return json_["id"]
        except:
##            print result.text
            return -1

    def GetProductsTypes(self):
        sql = """SELECT * FROM shop_type"""
        return self.MakeGetInfoQueue(sql)

    def GetProductsTypeId(self, siteUrl):
        sql = """SELECT id FROM shop_type
                 WHERE name = '{}'""".format(siteUrl)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def AddProductReview(self, productId, data):
        sql = u"""INSERT INTO shop_product_reviews( product_id, datetime, title, text, rate, name)
                  VALUES ( {}, '{}', '{}', '{}', {},  '{}')""".format(
                  productId, data[-1], data[4], data[5], data[7],data[6])
        self.MakeUpdateQueue(sql)

    def AddProductImage(self, productId, imgFilePath):
        files = {'file': open(imgFilePath, 'rb')}
        data = {"productId" : productId}
        result = self.POST("shop.product.images.add", data, files)
        try:
            json_ = json.loads( result.text)
            return json_["id"]
        except:
##            print result.text
            return -1

    def GetProductImagesIds(self, productId):
        sql = """SELECT id FROM shop_product_images
                 WHERE product_id = {}""".format(productId)
        return [i[0] for i in self.MakeGetInfoQueue(sql)]

    def GetProductImagesUrls(self, productId):
        data = {"productId" : productId}
        result = self.GET("shop.product.images.getList", data)
        arr = json.loads(result.text)
        return [i["url_thumb"] for i in arr]

    def SetProductVideo(self, productId, video):
        if video == None:
            sql = """UPDATE shop_product
                     SET video_url = NULL
                     WHERE id = {}""".format(productId)
        else:
            sql = """UPDATE shop_product
                     SET video_url = '{0}'
                     WHERE id = {1}""".format(video, productId)
        self.MakeUpdateQueue(sql)

    def SetProductBadge(self, productId, badge):
        if badge == None:
            sql = """UPDATE shop_product
                     SET badge = NULL
                     WHERE id = {}""".format( productId)
        else:
            sql = """UPDATE shop_product
                     SET badge = '{0}'
                     WHERE id = {1}""".format(badge, productId)
        self.MakeUpdateQueue(sql)

    def GetProductBadge(self, productId):
        sql = """SELECT badge FROM shop_product
                 WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def SetProductRating(self, productId, rating):
        sql = """UPDATE shop_product
                 SET rating = {0}
                 WHERE id = {1}""".format(rating, productId)
        self.MakeUpdateQueue(sql)

    def SetProductComparePrice(self, productId, compare_price):
        sql = """UPDATE shop_product
                 SET compare_price = {0}
                 WHERE id = {1}""".format(compare_price, productId)
        self.MakeUpdateQueue(sql)

    def SetProductRelatedProducts(self, productId, type_, related_product_id):
        sql = """INSERT INTO shop_product_related(product_id, type, related_product_id)
                 VALUES ({},'{}',{})""".format(productId, type_, related_product_id)
        self.MakeUpdateQueue(sql)

    def SetProductCrossSelling(self, productId, cross_selling):
        sql = """UPDATE shop_product
                 SET cross_selling = {}
                 WHERE id = {}""".format(cross_selling, productId)
        self.MakeUpdateQueue(sql)

    def GetProductRelatedProducts(self, productId, type_):
        sql = """SELECT related_product_id FROM shop_product_related
                 WHERE product_id = {} AND type = '{}'""".format(productId, type_)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def GetProductLinkRewrite(self, productId):
        sql = """SELECT url FROM shop_product
                WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductUpdateDate(self, productId):
        sql = """SELECT edit_datetime FROM shop_product
                WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductCreateDate(self, productId):
        sql = """SELECT create_datetime FROM shop_product
                WHERE id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetProductUrl(self, productId, city):
        link_rewrite = self.GetProductLinkRewrite(productId)
        storefrontUrl = self.GetStorefrontUrl(city).replace("*", "")
        if storefrontUrl.count("/") > 1:#подпапки
            productUrl = u"{}product/{}/".format(storefrontUrl.replace(self.siteUrl, ""), link_rewrite)
        else:
            productUrl = u"/product/{}/".format(link_rewrite)
        return productUrl

    def GetProductFullUrl(self, productId, city):
        link_rewrite = self.GetProductLinkRewrite(productId)
        storefrontUrl = self.GetStorefrontUrl(city).replace("*", "")
        productUrl = "https://{}product/{}/".format(storefrontUrl, link_rewrite)
        return productUrl

    def DeleteProduct(self, productId):
        result = self.POST("shop.product.delete", {"id" : productId})
        if result.text.find("invalid_request") != -1:
            return False
        else:
            return True

    def GetContractorProductsIds(self, contractorCode):
        sql = """SELECT product_id FROM shop_product_skus
                 LEFT JOIN shop_product
                 ON shop_product_skus.product_id = shop_product.id
                WHERE sku LIKE '{}%' and shop_product.type_id = {}""".format(contractorCode, self.typeId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def AddProductToCategory(self, productId, categoryId):
        sql = u"""INSERT INTO shop_category_products(product_id, category_id)
                  VALUES ({}, {})""".format(productId, categoryId)
        self.MakeUpdateQueue(sql)

    def AddProductPage(self, productId, data):
        today = self.GetTodayDate()
        sql = u"""INSERT INTO shop_product_pages( product_id, name, title, url, content, create_datetime, update_datetime, create_contact_id, status)
                  VALUES ({}, '{}', '', '{}', '{}', '{}','{}', 1, 1)""".format(productId, data["name"], data["url"], data["content"], today,today)
        self.MakeUpdateQueue(sql)

    def SetProductPageContent(self, productId, pageName, content):
        sql = u"""UPDATE shop_product_pages
                  SET content = '{}'
                  WHERE product_id = {} and name = '{}'""".format(content, productId, pageName)
        self.MakeUpdateQueue(sql)

    def DeleteProductFromCategory(self, productId, categoryId):
        sql = u"""DELETE FROM shop_category_products
                  WHERE product_id = {} and category_id = {}""".format(productId, categoryId)
        self.MakeUpdateQueue(sql)

    def AddProductVideoPage(self, productId, content):
        today = self.GetTodayDate()
        sql = u"""INSERT INTO shop_product_pages( product_id, name, title, url, content, create_datetime, update_datetime, create_contact_id, status)
                  VALUES ({}, 'Видео', '', 'video', '{}', '{}','{}', 1, 1)""".format(productId, content, today,today)
        self.MakeUpdateQueue(sql)

    def GetProductPagesNames(self, productId):
        sql = """SELECT name FROM shop_product_pages
                WHERE product_id = {}""".format(productId)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def GetProductPageContent(self, productId, pageName):
        sql = u"""SELECT content FROM shop_product_pages
                WHERE product_id = {} and name = '{}'""".format(productId, pageName)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def DeleteProductPage(self, productId, name):
        sql = u"""DELETE FROM shop_product_pages
                  WHERE product_id = {} AND name = '{}'""".format(productId, name)
        self.MakeUpdateQueue(sql)

    def GetProductsIdsWithUnvalidatedReviews(self):
        sql = """SELECT product_id FROM shop_product_reviews
                 WHERE status = 'moderation'"""
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    ######################
    ###### категории ######
    ######################

    def GetCategoryParent(self, categoryId):
        sql = u"""SELECT parent_id FROM shop_category
                  WHERE id = {}""".format(categoryId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetCategoryLinkRewrite(self, categoryId):
        sql = """SELECT url FROM shop_category
                 WHERE id = {}""".format(categoryId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def SetCategoryLinkRewrite(self, categoryId, link_rewrite):
        sql = u"""UPDATE shop_category
                SET url = '{0}', full_url = '{0}'
               WHERE id = {1}""".format(link_rewrite, categoryId)
        self.MakeUpdateQueue(sql)

    def SetCategoryActive(self, categoryId, status):
        sql = u"""UPDATE shop_category
                SET status = {}
               WHERE id = {}""".format(status, categoryId)
        self.MakeUpdateQueue(sql)

    def SetCategoryName(self, categoryId, name):
        sql = u"""UPDATE shop_category
                SET name = '{0}'
               WHERE id = {1}""".format(name, categoryId)
        self.MakeUpdateQueue(sql)

    def GetCategoryUrl(self, categoryId, city):
        link_rewrite = self.GetCategoryLinkRewrite(categoryId)
        storefrontUrl = self.GetStorefrontUrl(city).replace("*", "")
        if storefrontUrl.count("/") > 1:#подпапки
            productUrl = u"{}category/{}/".format(storefrontUrl.replace(self.siteUrl, ""), link_rewrite)
        else:
            productUrl = u"/category/{}/".format(link_rewrite)
        return productUrl

    def GetCategoryFullUrl(self, categoryId, city):
        link_rewrite = self.GetCategoryLinkRewrite(categoryId)
        storefrontUrl = self.GetStorefrontUrl(city).replace("*", "")
        categoryUrl = u"https://{}category/{}/".format(storefrontUrl, link_rewrite)
        return categoryUrl

    def GetCategoriesIds(self, onlyActive = 0):
        if onlyActive == 0:
            sql = """SELECT DISTINCT category_id FROM shop_category_routes
                     WHERE route like '%{}%'""".format(self.siteUrl)
        else:
            sql = """SELECT DISTINCT category_id FROM shop_category_routes
                    LEFT JOIN shop_category
                    ON shop_category.id = shop_category_routes.category_id
                    WHERE route like '%{}%' and shop_category.status = 1""".format(self.siteUrl)
        ids = [i[0] for i in self.MakeGetInfoQueue(sql)]
        return ids

    def GetAllCategoriesIds(self):
        sql = """SELECT DISTINCT id FROM shop_category"""
        ids = [i[0] for i in self.MakeGetInfoQueue(sql)]
        return ids

    def GetCategoryWithParentId(self, name, parentCategoryName):
        parentCategoryId = self.GetCategoryId(parentCategoryName)
        sql = u"""SELECT id FROM shop_category
                  WHERE name = '{0}' and parent_id = {}""".format(name, parentCategoryId)

    def IsCategoryCommon(self, categoryId):
        sql = """SELECT route FROM shop_category_routes
                 WHERE category_id = {}""".format(categoryId)
        data = self.MakeGetInfoQueue(sql)
        return len(data) == 0

    def GetCategoryId(self, name):
        sql = u"""SELECT DISTINCT category_id FROM shop_category_routes
                LEFT JOIN shop_category
                ON shop_category.id = shop_category_routes.category_id
                WHERE route like '%{}%' and shop_category.name = '{}'""".format(self.siteUrl, name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def GetCommonCategoryId(self, name):
        sql = u"""SELECT id FROM shop_category
                WHERE name = '{}'""".format(name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def DeleteCategory(self, categoryId):
        tables = ["shop_category",
                  "shop_category_og",
                  "shop_category_params",
                  "shop_category_products",
                  "shop_category_routes",
                  "shop_seo_category_settings",
                  ]
        for table in tables:
            if table == "shop_category":
                field = "id"
            else:
                field = "category_id"
            sql = """DELETE FROM {}
                     WHERE {} = {}""".format(table, field, categoryId)
            self.MakeUpdateQueue(sql)

    def AddCategoryToDB(self, categoryData):
        if self.GetCategoryId(categoryData["name"]) != -1:
            return
        if "parentName" in categoryData.keys():
            parent_id = self.GetCategoryId(categoryData["parentName"])
            categoryData["parent_id"] = parent_id
        else:
            categoryData["parent_id"] = 0
        sql = u""" INSERT INTO shop_category(parent_id, name, url, description, create_datetime, include_sub_categories, status)
                   VALUES ({}, '{}', '{}', '{}','{}',{},{})""".format(
                   categoryData["parent_id"],
                   categoryData["name"],
                   categoryData["url"],
                   categoryData["description"],
                   self.GetTodayDate() ,
                   categoryData["include_sub_categories"],
                   categoryData["status"])
        self.MakeUpdateQueue(sql)
        return self.GetLastInsertedId("shop_category", "id")

    def AddCategory(self, categoryData):
        if "parentName" in categoryData.keys():
            parent_id = self.GetCategoryId(categoryData["parentName"])
            categoryData["parent_id"] = parent_id
        else:
            categoryData["parent_id"] = 0
        result = self.POST("shop.category.add", categoryData)
        try:
            json_ = json.loads( result.text)
            return json_["id"]
        except:
##            print result.text
            return -1

    def GetCategoriesUrls(self):
        sql = u"""SELECT distinct url FROM shop_category"""
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def SetCategoryFilter(self, categoryId, catFilter):
        sql = """UPDATE shop_category
                 SET filter = '{}'
                 WHERE id = {}""".format(catFilter, categoryId)
        self.MakeUpdateQueue(sql)

    def AddCategoryRoute(self, categoryId, route):
        sql = """INSERT INTO shop_category_routes(category_id, route)
                 VALUES ({},'{}')""".format(categoryId, route)
        self.MakeUpdateQueue(sql)

    def SetCategorySortProducts(self, categoryId, sortProducts):
        sql = """UPDATE shop_category
                 SET sort_products = '{}'
                 WHERE id = {}""".format(sortProducts, categoryId)
        self.MakeUpdateQueue(sql)

    def AddCategoryParams(self, categoryId, name, value):
        sql = """INSERT INTO shop_category_params (category_id, name, value)
                 VALUES ({},'{}','{}')""".format(categoryId, name, value)
        self.MakeUpdateQueue(sql)

    def GetCategoryName(self, categoryId):
        sql = """SELECT name FROM shop_category
                 WHERE id = {}""".format(categoryId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetCategoryUpdateDate(self, categoryId):
        sql = """SELECT edit_datetime FROM shop_category
                 WHERE id = {}""".format(categoryId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def SetCategoryUpdateDate(self, categoryId, date):
        sql = """UPDAT shop_category
                 SET edit_datetime = '{}'
                 WHERE id = {}""".format(date, categoryId)

    def GetCategoryCreateDate(self, categoryId):
        sql = """SELECT create_datetime FROM shop_category
                 WHERE id = {}""".format(categoryId)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetCategoryProducts(self, categoryId, onlyActive = True):
        if onlyActive == False:
            sql = """SELECT product_id FROM shop_category_products
                     WHERE category_id = {}""".format(categoryId)
        else:
            sql = """SELECT product_id FROM shop_category_products
                     LEFT JOIN shop_product
                     ON shop_product.id = shop_category_products.product_id
                     WHERE shop_category_products.category_id = {} AND status = 1""".format(categoryId)
        return [i[0] for i in self.MakeGetInfoQueue(sql)]


    def IsCategoryActive(self, categoryId):
        sql = """SELECT status
                FROM shop_category
                WHERE id = {}""".format(categoryId)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    ############################
    ######### страницы #########
    ############################

    def GetDomainPages(self):
        sql = """SELECT * FROM shop_page
                 WHERE domain = '{}' and route = '*'""".format(self.siteUrl)
        return self.MakeGetInfoQueue(sql)

    def AddPageToDomain(self, data):
        sql = u"""INSERT INTO shop_page(parent_id, domain, route, name, title, url, full_url, content, create_datetime, update_datetime, create_contact_id, sort, status)
                  VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', {}, {}, {})""".format(
                  "NULL",data["domain"],data["route"],data["name"],"",data["url"],data["url"],data["content"],self.GetTodayDate(),self.GetTodayDate(),1,data["sort"],1)
        self.MakeUpdateQueue(sql)

    def SetPageContent(self, name, content):
        bSubfolderSite = self.IsSubfolderSite()
        if bSubfolderSite:
            sql = u"""UPDATE shop_page
                      SET content = '{}'
                      WHERE domain = '{}' and name = '{}'""".format(content, self.siteUrl, name)
        else:
            sql = u"""UPDATE shop_page
                      SET content = '{}'
                      WHERE domain like '%{}%' and name = '{}'""".format(content, self.siteUrl, name)
        self.MakeUpdateQueue(sql)

    def GetPageContent(self, name):
        sql = """SELECT * FROM shop_page
                 WHERE domain = '{}' and route = '*' and name = '{}'""".format(self.siteUrl, name)
        return self.MakeGetInfoQueue(sql)

    def SetPageSort(self, name, sort):
        bSubfolderSite = self.IsSubfolderSite()
        if bSubfolderSite:
            sql = u"""UPDATE shop_page
                      SET sort = {}
                      WHERE domain = '{}' and name = '{}'""".format(sort, self.siteUrl, name)
        else:
            sql = u"""UPDATE shop_page
                      SET sort = '{}'
                      WHERE domain like '%{}%' and name = '{}'""".format(sort, self.siteUrl, name)
        self.MakeUpdateQueue(sql)

    ############################
    ########### блог ###########
    ############################

    def GetBlogId(self, contact_name):
        sql = u"""SELECT blog_id FROM blog_post
                 WHERE contact_name = '{}'""".format(contact_name)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetBlogArticles(self, blogId):
        sql = """SELECT * FROM blog_post
                 WHERE blog_id = {}""".format(blogId)
        return self.MakeGetInfoQueue(sql)

    def AddArticle(self, data):
        sql = u"""INSERT INTO blog_post(blog_id, contact_id, contact_name, datetime, update_datetime, title, status, text, text_before_cut, cut_link_label, url, comments_allowed, meta_title, meta_keywords, meta_description)
                  VALUES ({}, {}, '{}', '{}', '{}', '{}', 'published', '{}', '{}', NULL, '{}', 0, '{}', '{}', '{}')""".format(
                  data["blog_id"],
                  data["contact_id"],
                  data["contact_name"],
                  data["datetime"],
                  data["datetime"],
                  data["title"],
                  data["text"],
                  data["text_before_cut"],
                  data["link_rewrite"],
                  data["meta_title"],
                  data["meta_keywords"],
                  data["meta_description"])
##        print sql
        self.MakeUpdateQueue(sql)

    def SetPostShortDescription(self, postId, description):
        sql = u"""UPDATE blog_post
                  SET text_before_cut = '{}'
                  WHERE id = {}""".format(description, postId)
        self.MakeUpdateQueue(sql)

    def GetPostId(self, title):
        sql = u"""SELECT id FROM blog_post
                  WHERE title = '{}'""".format(title)
        data = self.MakeGetInfoQueue(sql)
        if len(data) > 0:
            return data[0][0]
        else:
            return -1

    def SetPostUrl(self, id, url):
        sql = u"""UPDATE blog_post
                  SET url = '{}'
                  WHERE id = {}""".format(url, id)
        self.MakeUpdateQueue(sql)

    def GetPostUpdateDate(self, postId):
        sql = """SELECT update_datetime FROM blog_post
                WHERE id = {}""".format(postId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def SetPostParams(self, postId, title, description):
        sql = u"""UPDATE blog_post
                  SET meta_title = '{}', meta_description = '{}'
                  WHERE id = {}""".format(title, description, postId)
        self.MakeUpdateQueue(sql)


    ############################
    ######## витрины ###########
    ############################

    def GetStorefrontUrl(self, city):
        sql = u"""SELECT storefront FROM shop_seo_group_storefront_storefront
                  LEFT JOIN shop_seo_group_storefront
                  ON shop_seo_group_storefront.id = shop_seo_group_storefront_storefront.group_id
                  WHERE shop_seo_group_storefront.name = '{}' and storefront like '%{}%'""".format(city, self.siteUrl)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def GetStorefrontUrlById(self, id):
        sql = u"""SELECT storefront FROM shop_seo_group_storefront_storefront
                  WHERE group_id = {}""".format(id)
        try:
            return self.MakeGetInfoQueue(sql)[0][0].replace("/*", "")
        except:
            return -1

    def GetStorefrontsUrls(self):
        sql = u"""SELECT storefront FROM shop_seo_group_storefront_storefront
                  WHERE storefront like '%{}%'""".format(self.siteUrl)
        return [i[0] for i in self.MakeGetInfoQueue(sql)]

    def GetStorefrontsIds(self):
        sql = u"""SELECT group_id FROM shop_seo_group_storefront_storefront
                  WHERE storefront like '%{}%'""".format(self.siteUrl)
        return [i[0] for i in self.MakeGetInfoQueue(sql)]

    def GetStorefrontLatinCity(self, storefrontUrl):
        bSubfolders = storefrontUrl.count("/") > 1
        domain = storefrontUrl.replace("/*", "")
        if domain == self.siteUrl:
            city = ""
        else:
            if bSubfolders:
                city = domain.split("/")[-1]
            else:
                city = domain.split(".")[0]
        return city

    def GetStorefrontName(self, storefrontId):
        sql = u"""SELECT name FROM shop_seo_group_storefront
                  WHERE id = {}""".format(storefrontId)
        return self.MakeGetInfoQueue(sql)[0][0]

    ############################
    ###### характеристики ######
    ############################

    def GetFeaturesNames(self):
        sql = """SELECT name FROM shop_feature"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetDomainFeaturesIds(self):
        sql = """SELECT feature_id FROM shop_type_features
                 WHERE type_id = {}""".format(self.typeId)
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def GetFeatureId(self, featureName):
        sql = u"""SELECT id FROM shop_feature
                WHERE name = '{}'""".format(featureName)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetFeatureName(self, featureId):
        sql = """SELECT name FROM shop_feature
                WHERE id = {0}""".format(featureId)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetFeatureTypeByName(self, featureName):
        sql = """SELECT type FROM shop_feature
                WHERE name = '{}'""".format(featureName)
        data = self.MakeGetInfoQueue(sql)
        try:
            return data[0][0]
        except:
            return -1

    def GetFeatureTypeById(self, featureId):
        sql = """SELECT type FROM shop_feature
                WHERE id = '{}'""".format(featureId)
        data = self.MakeGetInfoQueue(sql)
        try:
            type_ = data[0][0]
            if type_.find("dimension") != -1:
                type_ = "dimension"
            return type_
        except:
            return -1

    def GetFeatureValues(self, featureName):
        values = []
        id = self.GetFeatureId(featureName)
        tables = ["shop_feature_values_color",
                  "shop_feature_values_dimension",
                  "shop_feature_values_double",
                  "shop_feature_values_text",
                  "shop_feature_values_varchar"]
        for table in tables:
            sql = u"""SELECT value FROM {}
                    WHERE feature_id = {}""".format(table, id)

            data = self.MakeGetInfoQueue(sql)
            values += [x[0] for x in data]
        return values

    def GetAllFeaturesIds(self):
        sql = """SELECT id FROM shop_feature"""
        data = self.MakeGetInfoQueue(sql)
        return [x[0] for x in data]

    def AddFeatureToSite(self, featureName):
        id = self.GetFeatureId(featureName)
        sql = """INSERT INTO shop_type_features(type_id, feature_id)
                 VALUES ({}, {})""".format(self.typeId, id)
        self.MakeUpdateQueue(sql)

    def AddFeatureToSiteById(self, featureId):
        sql = """INSERT INTO shop_type_features(type_id, feature_id)
                 VALUES ({}, {})""".format(self.typeId, featureId)
        self.MakeUpdateQueue(sql)

    def IsFeatureAddedToSite(self, featureId):
        sql = """SELECT * FROM shop_type_features
                 WHERE feature_id = {} and type_id = {}""".format(featureId, self.typeId)
        data = self.MakeGetInfoQueue(sql)
        return len(data) > 0

    def AddFeature(self, data):
        sql = u"""INSERT INTO shop_feature(code, name, type, selectable, multiple, available_for_sku)
                 VALUES ('{}','{}','{}',{},{},1)""".format(
                 data["code"],
                 data["name"],
                 data["type"],
                 data["selectable"],
                 data["multiple"]
                 )
        self.MakeUpdateQueue(sql)
        id = self.GetLastInsertedId('shop_feature', 'id')
        return id

    def AddFeatureValue(self, id_feature, value):
        feature_type = self.GetFeatureTypeById(id_feature)
        sort = self.GetLastInsertedId("shop_feature_values_" + feature_type, "id") + 1
        sql = u"""INSERT INTO shop_feature_values_{}(feature_id, sort, value)
                 VALUES ({},{}, '{}')""".format(feature_type, id_feature, sort, value)
        self.MakeUpdateQueue(sql)

    def AddFeatureValue2(self, featureName, value):
        id_feature = self.GetFeatureId(featureName)
        if id_feature == -1:
            return -1
        feature_type = self.GetFeatureTypeById(id_feature)
        sort = self.GetLastInsertedId("shop_feature_values_" + feature_type, "id") + 1
        if feature_type not in ["dimension"]:
            sql = u"""INSERT INTO shop_feature_values_{}(feature_id, sort, value)
                     VALUES ({},{}, '{}')""".format(feature_type, id_feature, sort, value)
        else:
            value_base_unit = value
            if featureName == u"Вес":
                type_ = "weight"
                unit = "kg"
            else:
                type_ = "length"
                unit = "cm"
                value /=  100
            sql = """INSERT INTO shop_feature_values_dimension(feature_id, sort, value, unit, type, value_base_unit)
                     VALUES ({}, {}, {}, '{}', '{}', {})""".format(id_feature, sort, value_base_unit, unit, type_, value)
        self.MakeUpdateQueue(sql)

    def GetFeatureValueId(self,id_feature, featureValueName):
        feature_type = self.GetFeatureTypeById(id_feature)
        if feature_type == "boolean":
            if featureValueName == u"да":
                return 1
            else:
                return 0
        if feature_type in ["double", "dimension"]:
            sql = u"""SELECT id FROM shop_feature_values_{}
                      WHERE feature_id = {} and value = {}""".format(feature_type, id_feature, featureValueName)
        else:
            sql = u"""SELECT id FROM shop_feature_values_{}
                      WHERE feature_id = {} and value = '{}'""".format(feature_type, id_feature, featureValueName)
        data = self.MakeGetInfoQueue(sql)
        return data[0][0]

    def AddProductFeatureValue(self, id_product, featureName, featureValueName):
        id_feature = self.GetFeatureId(featureName)
        id_feature_value = self.GetFeatureValueId(id_feature, featureValueName)
        if id_feature == -1 or id_feature_value == -1:
            return -1
        sql = """INSERT INTO shop_product_features(product_id, feature_id, feature_value_id)
                 VALUES ({},{},{})""".format(id_product, id_feature, id_feature_value)
        self.MakeUpdateQueue(sql)

    def AddProductFeatureValueById(self, id_product, id_feature, id_feature_value):
        sql = """SELECT * FROM ps_feature_product
                 WHERE id_product = %(id_product)s
                        AND id_feature_value = %(id_feature_value)s
                        AND id_feature = %(id_feature)s """%{"id_product":id_product,
                        "id_feature":id_feature, "id_feature_value":id_feature_value}

        data = self.MakeGetInfoQueue(sql)
        if len(data) != 0:
            return
        sql = """INSERT INTO ps_feature_product(id_feature, id_product, id_feature_value)
                 VALUES (%(id_feature)s,%(id_product)s,%(id_feature_value)s)"""%{"id_feature":id_feature,
                "id_product":id_product,"id_feature_value":id_feature_value}
        self.MakeUpdateQueue(sql)

    def GetFeatureValueName(self, featureId, feature_value_id):
        fType = self.GetFeatureTypeById(featureId)
        if fType == "boolean":
            if feature_value_id == 0:
                return u"нет"
            else:
                return u"да"
        table = "shop_feature_values_" + fType
        sql = """SELECT value FROM {}
                 WHERE feature_id = {} AND id = {}""".format(table, featureId, feature_value_id)
        return self.MakeGetInfoQueue(sql)[0][0]

    def GetProductFeatures(self, id_product):
        features = {}
        sql = """SELECT DISTINCT feature_id FROM shop_product_features
                 WHERE product_id = {0}""".format(id_product)
        featuresIds = [x[0] for x in self.MakeGetInfoQueue(sql)]
        for featureId in featuresIds:
            sql = """SELECT feature_value_id FROM shop_product_features
                 WHERE product_id = {0} and feature_id = {1}""".format(id_product, featureId)
            values = [self.GetFeatureValueName(featureId, x[0]) for x in self.MakeGetInfoQueue(sql)]
            features[self.GetFeatureName(featureId)] = values
        return features

    def DeleteProductFeatureValue(self, id_product, featureName, featureValueName):
        id_feature = self.GetFeatureId(featureName)
        id_feature_value = self.GetFeatureValueId(id_feature, featureValueName)
        if id_feature == -1 or id_feature_value == -1:
            return
        sql = """DELETE FROM ps_feature_product
                WHERE id_product = %(id_product)s AND id_feature = %(id_feature)s AND id_feature_value = %(id_feature_value)s"""%{"id_product":id_product,
                "id_feature":id_feature, "id_feature_value":id_feature_value}

        self.MakeUpdateQueue(sql)

    def DeleteProductFeature(self, id_product, featureName):
        id_feature = self.GetFeatureId(featureName)
        if id_feature == -1:
            return
        sql = """DELETE FROM shop_product_features
                WHERE product_id = %(id_product)s AND feature_id = %(id_feature)s"""%{"id_product":id_product,
                "id_feature":id_feature}
        self.MakeUpdateQueue(sql)

    def DeleteProductFeatureValueById(self, id_product, id_feature, id_feature_value):
        sql = """DELETE FROM ps_feature_product
                WHERE id_product = %(id_product)s AND id_feature = %(id_feature)s AND id_feature_value = %(id_feature_value)s"""%{"id_product":id_product,
                "id_feature":id_feature, "id_feature_value":id_feature_value}
        self.MakeUpdateQueue(sql)

    def DeleteProductFeatures(self, id_product):
        sql = """DELETE FROM shop_product_features
                WHERE product_id = {}""".format(id_product)
        self.MakeUpdateQueue(sql)

    ############################
    ####### уведомления  #######
    ############################

    def GetActiveNotificationsIds(self, siteUrl):
        sql = """SELECT DISTINCT id FROM shop_notification
                 LEFT JOIN shop_notification_sources
                 ON shop_notification.id = shop_notification_sources.notification_id
                 WHERE status = 1 AND shop_notification_sources.source LIKE '%{}%'""".format(siteUrl)
        data = self.MakeGetInfoQueue(sql)
        return [i[0] for i in data]

    def GetNotificationData(self, id):
        sql = """SELECT name, event, transport, status FROM shop_notification
                 WHERE id = {}""".format(id)
        data = self.MakeGetInfoQueue(sql)
        return data[0]

    def GetNotificationParams(self, id):
        sql = """SELECT name, value FROM shop_notification_params
                 WHERE notification_id = {}""".format(id)
        data = self.MakeGetInfoQueue(sql)
        return data

    def AddNotification(self, name, event):
        sql = u"""INSERT INTO shop_notification(name, event)
                 VALUES ('{}', '{}')""".format(name, event)
        self.MakeUpdateQueue(sql)
        return self.GetLastInsertedId("shop_notification", "id")

    def AddNotificationSource(self, notification_id, source):
        sql = u"""INSERT INTO shop_notification_sources(notification_id, source)
                     VALUES ({},'{}')""".format(notification_id, source)
        self.MakeUpdateQueue(sql)

    def AddNotificationParam(self, notification_id, name, value):
        sql = u"""INSERT INTO shop_notification_params(notification_id, name, value)
                 VALUES ({}, '{}', '{}')""".format(notification_id, name, value)
        self.MakeUpdateQueue(sql)

    ############################
    ########### сео  ###########
    ############################

    def AddStorefront(self, name, url, storefront_select_rule_type):
        id_ = self.GetLastInsertedId("shop_seo_group_storefront", "id") + 1
        sql = u"""INSERT INTO shop_seo_group_storefront(id, name, storefront_select_rule_type, sort)
                 VALUES ({}, '{}', '{}', {})""".format(id_, name, storefront_select_rule_type, id_)
        self.MakeUpdateQueue(sql)
        sql = u"""INSERT INTO shop_seo_group_storefront_storefront(group_id, storefront)
                 VALUES ({}, '{}')""".format(id_, url)
        self.MakeUpdateQueue(sql)
        return id_

    def GetStorefrontId(self, domain, name):
        sql = u"""SELECT id FROM shop_seo_group_storefront
                  LEFT JOIN shop_seo_group_storefront_storefront
                  ON shop_seo_group_storefront.id = shop_seo_group_storefront_storefront.group_id
                  WHERE name = '{}' AND storefront LIKE '%{}%'""".format(name, domain)
        try:
            return self.MakeGetInfoQueue(sql)[0][0]
        except:
            return -1

    def AddStorefrontSettings(self, group_id, name, value):
        sql = u"""INSERT INTO shop_seo_storefront_settings(group_id, name, value)
                 VALUES ({}, '{}', '{}')""".format(group_id, name, value)
        self.MakeUpdateQueue(sql)

    def AddStorefrontCategorySettings(self, group_storefront_id, category_id, name, value):
        sql = u"""INSERT INTO shop_seo_category_settings(group_storefront_id, category_id, name, value)
                 VALUES ({}, {}, '{}', '{}')""".format(group_storefront_id, category_id, name, value)
        self.MakeUpdateQueue(sql)

    def UpdateStorefrontCategorySettings(self, group_storefront_id, category_id, name, value):
        sql = u"""UPDATE shop_seo_category_settings
                  SET value = '{}'
                  WHERE group_storefront_id = {} AND category_id = {} AND name = '{}'""".format(value, group_storefront_id, category_id, name )
        self.MakeUpdateQueue(sql)

    def GetStorefrontCategorySettings(self, group_storefront_id, category_id, name):
        sql = u"""SELECT value FROM shop_seo_category_settings
                  WHERE group_storefront_id = {} and category_id = {} and name = '{}'""".format(group_storefront_id, category_id, name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def SetStorefrontCategorySettings(self, group_storefront_id, category_id, name, newValue):
        sql = u"""UPDATE shop_seo_category_settings
                  SET value = '{}'
                  WHERE group_storefront_id = {} and category_id = {} and name = '{}'""".format(newValue, group_storefront_id, category_id, name)
        data = self.MakeUpdateQueue(sql)

    def AddStorefrontProductSettings(self, group_storefront_id, product_id, name, value):
        sql = u"""INSERT INTO shop_seo_product_settings(group_storefront_id, product_id, name, value)
                 VALUES ({}, {}, '{}', '{}')""".format(group_storefront_id, product_id, name, value)
        self.MakeUpdateQueue(sql)

    def GetStorefrontProductSettings(self, group_storefront_id, product_id, name):
        sql = u"""SELECT value FROM shop_seo_product_settings
                  WHERE group_storefront_id = {} and product_id = {} and name = '{}'""".format(group_storefront_id, product_id, name)
        data = self.MakeGetInfoQueue(sql)
        if len(data) == 0:
            return -1
        return data[0][0]

    def DeleteStorefrontProductSettings(self, group_storefront_id, product_id):
        sql = u"""DELETE FROM shop_seo_product_settings
                  WHERE group_storefront_id = {} AND product_id = {}""".format(group_storefront_id, product_id)
        self.MakeUpdateQueue(sql)

    def SetStorefrontProductSettings(self, group_storefront_id, product_id, name, newValue):
        sql = u"""UPDATE shop_seo_product_settings
                  SET value = '{}'
                  WHERE group_storefront_id = {} and product_id = {} and name = '{}'""".format(newValue, group_storefront_id, product_id, name)
        data = self.MakeUpdateQueue(sql)

    ############################
    ######### редиректы ########
    ############################

    def AddRedirect(self, data):
        sort = self.GetLastInsertedId('shop_seoredirect_redirect', 'sort') + 1
        sql = u"""INSERT INTO shop_seoredirect_redirect(hash, domain, url_from, url_to, param, type, status, sort, code_http, create_datetime, edit_datetime, comment)
                  VALUES ('{}', '{}', '{}', '{}', 0, 1, 1, {}, {}, '{}', '{}', comment)""".format(
                  data["hash"], data["domain"], data["url_from"], data["url_to"], sort, 301, data["date"], data["date"])
        self.MakeUpdateQueue(sql)

    def GetAllRedirects(self):
        sql = """select * from shop_seoredirect_redirect"""
        return self.MakeGetInfoQueue(sql)

    def GetAllDomainRedirects(self, domain):
        sql = """select * from shop_seoredirect_redirect
                 where domain like '{}'""".format(domain)
        return self.MakeGetInfoQueue(sql)

    def GetDomain404ErrorUrls(self, domain):
        sql = """select url from shop_seoredirect_errors
                 where domain like '{}'""".format(domain)
        return [i[0] for i in self.MakeGetInfoQueue(sql)]

#################################################################################

    def GET(self, path, data=None):
        url = "{}/{}".format(self.apiUrl, path) + "?access_token={}".format(self.access_token)
        if path == "shop.product.images.getList":
            url += "&product_id={}".format(data["productId"])
        result = requests.get(url, verify=False)
        return result

    def POST(self, path, data=None, files=None):
        url = "{}/{}".format(self.apiUrl, path) + "?access_token={}".format(self.access_token)
        if path == "shop.product.images.add":
            url += "&product_id={}".format(data["productId"])
##        print url
        if files is not None:
            result = requests.post(url, files=files, verify=False)
        else:
            result = requests.post(url, data=data, verify=False, allow_redirects=False)
        return result

    def PUT(self, path, xml):
        xml = requests.put(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "") + "/api/" + path, data=xml,auth=requests.auth.HTTPBasicAuth(self.login, self.key))
        return xml.text

    def DELETE(self, path):
        xml = requests.delete(self.GetSiteUrlScheme() + self.siteUrl.replace("http://", "")  + "/api/" + path, auth=requests.auth.HTTPBasicAuth(self.login, self.key))

    def urlEncodeNonAscii(self, b):
        return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

    def iriToUri(self, iri):
        parts= urlparse.urlparse(iri)
        return urlparse.urlunparse(
            part.encode('idna') if parti==1 else self.urlEncodeNonAscii(part.encode('utf-8'))
            for parti, part in enumerate(parts)
        )

    def DeleteCommentsFromHtml(self, string):
        if string.find("<!--") != -1:
            while string.find("<!--") != -1:
                ind1 = string.find("<!--")
                ind2 = string.find("-->") + 3
                string = string.replace(string[ind1:ind2],"")

        return string

    def IsUrlExists(self, url):
        try:
            resp = requests.get(url)
            return resp.status_code == 200
        except:
            return False
