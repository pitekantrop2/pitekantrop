﻿from UBaseObjects import *

class UGoogleObjects(UBaseObjects):

    #авторизация
##    emailEdit = ["id", "Email"]
##    passwordEdit = ["id", "Passwd"]
##    nextButton = ["name", "signIn"]
    emailEdit = ["id", "identifierId"]
    passwordEdit = ["name", "password"]
    nextButton = GetElementByText("span", u"Далее")

    #добавление url'ов
    URLEdit = ["name" , "urlnt"]
    captchaCh = ["class", "recaptcha-checkbox-checkmark"]
    submitUrlButton = ["id", "save-input-button"]
    recaptchaIframe = ["xpath", "//iframe[@title = '" + u"виджет reCAPTCHA" + "']"]
    recaptchaAria = ["id", "rc-imageselect"]
    confirmButton = ["xpath", "//div[@aria-disabled = 'false']"]
    imageSelectDiv = ["id", "recaptcha-verify-button"]

    #добавление ресурса
    addResource_addUrlButton = ["id", "gwt-uid-38"]
    addResource_urlEdit = ["xpath", "//div[@class = 'JX0GPIC-h-a']//input[contains(@class, '" + u"gwt-TextBox jfk-textinput JX0GPIC-h-B" + "')]"]
    addResource_nextButton = ["xpath", "//button[contains(@id, 'gwt-uid') and contains(@class, 'JX0GPIC-k-a JX0GPIC-k-l')]"]
    addResource_confirmButton = ["xpath", "//div[contains(@class, '" + u"goog-inline-block jfk-button jfk-button-primary" + "')]"]
    addResource_continueButton = ["linktext", "Продолжить"]

    #sitemaps
    sitemaps_addButton = GetElementByText("button", u"Добавление/проверка файла Sitemap")
    sitemaps_addButton = GetElementByText("button", u"Добавление/проверка файла Sitemap")
    sitemaps_sendButton = ["xpath", "//div[@class = 'JX0GPIC-k-h' and contains(.,'" + u"Отправить" + "')]"]
    sitemaps_resendButton = ["xpath", "//div[@class = 'JX0GPIC-k-h' and contains(.,'" + u"Отправить еще раз" + "')]"]
    sitemaps_urlEdit = ["xpath", "//input[@class = 'gwt-TextBox jfk-textinput']"]
    sitemaps_checkbox = ["xpath", "//input[@type = 'checkbox']"]
    sitemaps_deleteButton = ["xpath", u"//div[@class = 'JX0GPIC-k-h' and contains(.,'{0}')]".format(u"Удалить")]
    sitemaps_confirmationIframe = ["id","com.google.crawl.wmconsole.fe.feature.gwt.wmxclient"]
    sitemaps_YesButton = ["xpath", u"//div[@class = 'JX0GPIC-k-h' and contains(.,'{0}')]".format(u"Да")]
    sitemaps_CloseButton = ["xpath", u"//div[@class = 'JX0GPIC-k-h' and contains(.,'{0}')]".format(u"Закрыть")]

    #удаление url'ов
    deleteUrls_hideButton = ["xpath", "//div[@id = 'create-removal_button']/div"]
    deleteUrls_urlEdit = ["id", "urlt"]
    deleteUrls_continueButton = ["name", "urlt.submitButton"]
    deleteUrls_sendRequestButton = ["id", "submit-button"]

    #переобход
    reindex_urlEdit = ["id", "path-input"]
    reindex_scanButton = ["xpath", u"//div[@role = 'button' and contains(.,'{0}')]".format(u"Сканировать")]
    reindex_requestButton = ["xpath", u"//div[@role = 'button' and contains(.,'{0}')]".format(u"Запросить индексирование")]
    reindex_allLinksRadio = ["id", u"verified-addurl-dialog-radio-site"]
    reindex_sendButton = ["name", u"go"]
    reindex_recaptchaDiv = ["class", u"rc-imageselect-payload"]
    reindex_recaptchaIframe = ["xpath", "//iframe[contains(@src, 'recaptcha')]"]
