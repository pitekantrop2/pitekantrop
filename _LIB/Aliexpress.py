﻿from selenium import webdriver
import myLibrary
import urllib
import ftplib
import UAliexpressObjects
import time

objects = UAliexpressObjects.UAliexpressObjects

class Aliexpress:

    selenium = myLibrary.Selenium()

    def __init__(self):
        self.selenium.SetUp()

    def Login(self, Login, Password):
        self.selenium.OpenUrl('https://login.aliexpress.ru')
##        self.selenium.SwitchToFrame("alibaba-login-box")
        self.selenium.WaitFor(objects.login)
        self.selenium.SendKeys(objects.login, Login)
        self.selenium.SendKeys(objects.password, Password)
        self.selenium.Click(objects.loginButton)
        self.selenium.SwitchToBaseWindow()
        self.selenium.WaitFor(objects.userNameSpan)


    def OpenSendedOrderList(self):
        self.selenium.OpenUrl("https://trade.aliexpress.ru/orderList.htm")
        self.selenium.WaitFor(objects.ordersList_SendedOrdersLink)
        self.selenium.Click(objects.ordersList_SendedOrdersLink)
        self.selenium.WaitFor(objects.ordersList_CheckOrderTrackButton)


    def CloseSelenium(self):
        try:
            self.selenium.CleanUp()
        except:
            pass
        self.selenium = None

