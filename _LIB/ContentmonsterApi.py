﻿import requests
import log
import urllib
import json
import sys
import xmlrpclib

orderStates = {
0 : u"черновик",
2 : u"прием предложений",
3 : u"выбор копирайтера",
4 : u"отправлен копирайтеру",
5 : u"в работе",
7 : u"проверка уникальности",
8 : u"на проверку",
9 : u"доработка",
10 : u"просрочен",
12 : u"закончен",
20 : u"архив",
21 : u"удален"
}

sitesIds = {
34506 : "http://mini-camera.ru.com",
34507 : "http://otpugivateli-ptic.ru",
34509 : "http://otpugivatel.com",
34510 : "http://otpugiwatel.com",
34963 : "http://otpugivateli-sobak.ru",
35208 : "http://incubators.shop",
35478 : "http://usiliteli-svyazi.ru",
35531 : "http://knowall.ru.com",
35532 : "http://glushilki.ru.com",
35533 : "http://otpugivateli-grizunov.ru",
36780 : "http://insect-killers.ru",
37020 : "http://otpugivateli-krotov.ru",
38437 : "http://otpugivatel.spb.ru",
38724 : "http://safetus.ru"
}

class ContentmonsterApi():
    apikey = "c863492d73f28d020fd7f02bc19a4b8d"
    apiUrl = "https://contentmonster.ru/api/xmlrpc/"
    client = None

    def __init__(self):
        self.client = xmlrpclib.ServerProxy(self.apiUrl, encoding='UTF-8')

    def GetBalance(self):
        try:
            resp = self.client.contentmonster.balance(self.apikey)
        except Exception as e:
            print e.faultString
            return -1

        return resp["wmr"]

    def GetOrderStatus(self, orderId):
        try:
            resp = self.client.contentmonster.getOrderStatus(self.apikey, int(orderId))
        except Exception as e:
            print e.faultString
            return -1

        return orderStates[int(resp)]

    def ApproveOrder(self, orderId, ctype = 1,rate = 5 ):
        try:
            resp = self.client.contentmonster.approveOrder(self.apikey, int(orderId), ctype=ctype,rate=rate )
        except Exception as e:
            print e.faultString
            return -1

    def CreateOrder(self, data):
        try:
            orderId = self.client.contentmonster.createOrder(self.apikey, data)
        except Exception as e:
            print e.faultString
            return -1

        return orderId

    def GetOrderText(self, orderId):
        try:
            resp = self.client.contentmonster.export(self.apikey, int(orderId))
        except Exception as e:
            print e.faultString
            return -1

        return resp["text"]

    def GetSiteId(self, siteUrl):
        for key in sitesIds.keys():
            if sitesIds[key] == siteUrl:
                return key
        return -1






