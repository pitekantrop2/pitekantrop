﻿import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
import MySQLdb
import io
import mimetypes
import json
import hashlib
import time
import myLibrary
import URussianpostcalcObjects
import requests
from suds.client import Client

objects = URussianpostcalcObjects.URussianpostcalcObjects()

class RussianPostAPI:

    calculateUrl = "http://api.postcalc.ru/"
##    getStateUrl = "http://gdeposylka.ru/api/v3/jsonrpc"
    getStateUrl = "https://tracking.pochta.ru/rtm34?wsdl"
    getIndexUrl = "http://post-api.ru/api/v2/ibc.php"
    getBlankUrl = "http://russianpostcalc.ru/blank/"
    fromCity = None
    selenium = None
    rpApiLogin = "TfTibweoPXZMVe"
    rpApiPass = "9yD0Q7Tqy2o3"
##    selenium = myLibrary.Selenium()


    def __init__(self):
        self.fromCity = "Санкт-Петербург"

    def Calculate(self, toCity):
        toCity = toCity.encode("utf8")
        data = {}
        data["f"] = self.fromCity
        data["t"] = toCity
        data["o"] = "xml"
        resp = requests.get(self.calculateUrl + "?" + urllib.urlencode(data))
        return resp._content

    def GetDays(self, toCity):

        try:
            pvzXml = etree.XML( self.Calculate (toCity))
            root =  pvzXml.getroottree().getroot()
            pvzNodes = root.xpath("//var[@name = '" + u"СрокДоставки" + "']/string")
            days = []
            for day in pvzNodes:
                if day.text != None:
                    days.append(int(day.text.split("-")[0]))

            maxD = max(days)
            return maxD
        except:
            return 8

    def GetIndex(self, city, street):

        if city == u"Братск":return "665717"
        if city == u"Горно-Алтайск":return "649000"
        if city == u"Евпатория":return "297493"
        if city == u"Керчь":return "298309"
        if city == u"Комсомольск-на-Амуре":return "681000"
        if city == u"Нарьян-Мар":return "166000"
        if city == u"Нижнекамск":return "423570"
        if city == u"Новый Уренгой":return "629300"
        if city == u"Севастополь":return "299055"
        if city == u"Симферополь":return "295044"
        if city == u"Ханты-Мансийск":return "628000"
        if city == u"Южно-Сахалинск":return "693000"
        if city == u"Ялта":return "298637"


        data = {}
        data["c"] = city.encode('utf8')
        data["s"] = street.encode('utf8')
        data["apikey"] = "lfpb6m47upd4asuy"

        indexesJson = []
        indexes = requests.get( self.getIndexUrl + "?" + urllib.urlencode(data))._content
        try:
            indexesJson = json.loads(indexes)["content"]

            if len(indexesJson) == 0:
                data["s"] = u"ленин".encode('utf8')
                indexes = requests.get( self.getIndexUrl + "?" + urllib.urlencode(data))._content
                indexesJson = json.loads(indexes)["content"][0]
            else:
                indexesJson = indexesJson[0]
                if len(indexesJson["indexes"]) == 0:
                    data["s"] = u"ленин".encode('utf8')
                    indexes = requests.get( self.getIndexUrl + "?" + urllib.urlencode(data))._content
                    indexesJson = json.loads(indexes)["content"][0]
            try:
                ind = indexesJson["indexes"][-1]
            except:
                ind = indexesJson["indexes"]["0"]
        except:
            return -1

        return ind

    def GetOrderStates(self, trackingNumber):
        orderStates = []

        xml = """
                <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:oper="http://russianpost.org/operationhistory" xmlns:data="http://russianpost.org/operationhistory/data" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header/>
                <soap:Body>
                   <oper:getOperationHistory>
                      <data:OperationHistoryRequest>
                         <data:Barcode>""" + trackingNumber + """</data:Barcode>
                         <data:MessageType>0</data:MessageType>
                         <data:Language>RUS</data:Language>
                      </data:OperationHistoryRequest>
                      <data:AuthorizationHeader soapenv:mustUnderstand="1">
                         <data:login>"""+ self.rpApiLogin +"""</data:login>
                         <data:password>""" + self.rpApiPass + """</data:password>
                      </data:AuthorizationHeader>
                   </oper:getOperationHistory>
                </soap:Body>
             </soap:Envelope>"""
##        print xml
        try:
            client = Client(self.getStateUrl,headers={'Content-Type': 'application/soap+xml; charset=utf-8'})
            result = client.service.getOperationHistory(__inject={'msg':xml})
            for rec in result.historyRecord:
                try:
                    orderStates.append(unicode(rec.OperationParameters.OperAttr.Name))
                except Exception as e:
                    pass
            return orderStates
        except Exception as e:
            print str(e)
            return False


    def GetBlanks(self, orderData):

        if self.selenium == None:
            self.selenium = myLibrary.Selenium()
            self.selenium.SetUp()

        try:
            self.selenium.OpenUrl(self.getBlankUrl)
            self.selenium.WaitFor(objects.from_fio)
            if orderData["nalogka_rub"] != "0":
                self.selenium.Click(objects.print112)
            self.selenium.Click(objects.print7p)
            self.selenium.SendKeys(objects.from_fio, orderData["from_fio"])
            self.selenium.SendKeys(objects.from_index, orderData["from_index"])
            self.selenium.WaitFor(objects.from_selectOption)
            self.selenium.Click(objects.from_selectOption)
            self.selenium.WaitForNotVisible(objects.from_selectOption)
            self.selenium.SendKeys(objects.from_addr, orderData["from_addr"])
##            self.selenium.SendKeys(objects.p_nomer, orderData["p_nomer"])
##            self.selenium.SendKeys(objects.p_seriya, orderData["p_seriya"])
##            self.selenium.SendKeys(objects.p_vidal, orderData["p_vidal"])
##            self.selenium.SendKeys(objects.p_vidan, orderData["p_vidan"])
            self.selenium.SendKeys(objects.to_fio, orderData["to_fio"])
            self.selenium.SendKeys(objects.to_tel, orderData["to_tel"])
            self.selenium.SendKeys(objects.to_index, orderData["to_index"])
            self.selenium.WaitFor(objects.to_selectOption)
            self.selenium.Click(objects.to_selectOption)
            self.selenium.WaitForNotVisible(objects.to_selectOption)
            self.selenium.SendKeys(objects.to_addr, orderData["to_addr"])
            self.selenium.SendKeys(objects.ob_cennost_rub, orderData["ob_cennost_rub"])
            self.selenium.SendKeys(objects.nalogka_rub, orderData["nalogka_rub"])
            self.selenium.Click(objects.calcbutton)
            self.selenium.WaitFor(objects.downloadLink)
            return [self.selenium.GetElementAttribute(x, "href") for x in self.selenium.MyFindElements(objects.downloadLink)]

        except:
            self.CloseSelenium()
            return -1

    def CloseSelenium(self):
        try:
            self.selenium.CleanUp()
        except:
            pass
        self.selenium = None




