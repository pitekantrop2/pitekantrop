﻿from selenium import webdriver
import myLibrary
import urllib
import ftplib
import UYandexObjects
import time
import Captcha
import commonLibrary
import os

antigateKey = '0d841d49f0062d8940c1ab538e9b0f98'

def GetAntigate(pictureName):
    gate = AntiGate(antigateKey,pictureName)
    return gate.captcha_key


accountName = "pitekantrop5"
objects = UYandexObjects.UYandexObjects


class Yandex:
    mode = True
    selenium = myLibrary.Selenium()
    captcha = None
    ssh_ = None
    sftp = None

    def __init__(self, mode, ip):
        self.mode = mode
        if self.mode == True:
            self.selenium.SetUp()
        else:
            self.selenium.driver = webdriver.Remote("http://localhost:4444/wd/hub", webdriver.DesiredCapabilities.HTMLUNITWITHJS)
        self.captcha = Captcha.Captcha(True)
        self.ssh_, self.sftp = commonLibrary.CreateSshSession(ip)


    def Login(self, Login, Password, bWaitForUserMenu = True):
        self.Logout()
        self.selenium.OpenUrl("https://passport.yandex.ru/passport?mode=auth&from=mail&retpath=https%3A%2F%2Fmail.yandex.ru&origin=hostroot_ru_nol_mobile_enter")
        self.selenium.WaitFor(objects.auth_login, Login)
        self.selenium.SendKeys(objects.auth_login, Login)
        self.selenium.Click(objects.auth_loginButton)
        self.selenium.WaitFor(objects.auth_password)
        self.selenium.SendKeys(objects.auth_password, Password)
        self.selenium.Click(objects.auth_loginButton)
        if bWaitForUserMenu == True:
            try:
                self.selenium.WaitFor(objects.letters_messages)
                if self.selenium.IsVisible(objects.auth_closePopup) == True:
                    self.selenium.Click(objects.auth_closePopup)
            except:
                print "FinishDomenRegistration for " + Login
                self.FinishDomenRegistration()
        else:
            self.selenium.Sleep(10)

    def Logout(self):
        self.selenium.OpenUrl('https://mail.yandex.ru')
        self.selenium.Sleep(2)
        if self.selenium.IsVisible(objects.auth_closePopup) == True:
                 self.selenium.Click(objects.auth_closePopup)
        if self.selenium.IsVisible(objects.reg_userMenu) == True or self.selenium.IsVisible(objects.reg_userMenuName) == True:
            self.selenium.WaitFor(objects.letters_messages)
            if self.selenium.IsVisible(objects.auth_closePopup) == True:
                 self.selenium.Click(objects.auth_closePopup)

            if self.mode == True:
                self.selenium.Click(objects.reg_userMenu)
                self.selenium.WaitFor(objects.reg_logoutItem)

            self.selenium.Click(objects.reg_logoutItem)


    def AddDomenAccount(self, domen, newAccName):
        if self.selenium.IsTextPresent(accountName) == False:
            self.Login(accountName, "2wsx#EDC")

        self.selenium.OpenUrl("https://pdd.yandex.ru/domain/" + domen)
        self.selenium.WaitFor(objects.reg_newAccButton)
        self.selenium.Click(objects.reg_newAccButton)
        self.selenium.Sleep(1)
        self.selenium.SendKeys(objects.auth_login, newAccName)
        self.selenium.SendKeys(objects.auth_accPassword, "123qweasd")
        self.selenium.SendKeys(objects.auth_accPasswordAgain, "123qweasd")
        self.selenium.Click(self.selenium.MyFindElementInElement(objects.reg_addPopup, objects.reg_addAccButton))
        self.Logout()
        self.Login(newAccName + "@" + domen, "123qweasd", False)
        #self.selenium.WaitFor(finishReg_endRegistraionButton)
        self.FinishDomenRegistration()
        self.Login(newAccName + "@" + domen, "123qweasd")

    def FinishDomenRegistration(self):
        self.selenium.SendKeys(objects.finishReg_accName, getRandomData.GetRandomFemaleRandomName() )
        self.selenium.SendKeys(objects.finishReg_accLastname, getRandomData.GetRandomMaleRandomLastname()   )
        self.selenium.SetSelectOptionByIndex(objects.finishReg_controlQuestion, 1)
        self.selenium.SendKeys(objects.finishReg_controlAnswer, "qwerty")
        self.selenium.Click(objects.finishReg_endRegistraionButton)
        self.selenium.WaitFor(objects.letters_messages)

    def ResendLettersToEmail(self, fromEmail, ToEmail):
        isSuccess = False
        while isSuccess == False:
            try:
                self.Login(fromEmail, "123qweasd")
                self.selenium.Click(objects.resend_settings)
                self.selenium.WaitFor(objects.resend_rules)
                self.selenium.Click(objects.resend_rules)
                self.selenium.WaitFor(objects.resend_newRuleButton)
                self.selenium.Click(objects.resend_newRuleButton)
                self.selenium.Click(objects.resend_condition)
                self.selenium.Click(objects.resend_contains)
                self.selenium.SendKeys(objects.resend_condField, "@")
                self.selenium.Click(objects.resend_continueLink)
                self.selenium.WaitFor(objects.resend_enterPassword)
                self.selenium.SendKeys(objects.resend_enterPassword, "123qweasd")
                self.selenium.Click(objects.resend_confirm)
                self.selenium.WaitForNotVisible(objects.resend_enterPassword)
                self.selenium.Click(objects.resend_resendCh)
                self.selenium.SendKeys(objects.resend_mailAddress, ToEmail)
                self.selenium.Click(objects.resend_safeCopyCh)
                self.selenium.Click(objects.resend_createRuleButton)
                self.selenium.Sleep(10)
                self.Logout()
                self.Login(ToEmail, "123qweasd")
                self.selenium.Click(self.selenium.MyFindElements(objects.letters_letters_links)[0])
                self.selenium.WaitFor(objects.letters_confirm_link)
                href = self.selenium.GetElementAttribute(lobjects.etters_confirm_link, "href")
                self.selenium.Click(objects.letters_confirm_link)
                self.selenium.SwitchToBaseWindow()
                self.Logout()
                self.Login(fromEmail, "123qweasd")
                self.selenium.OpenUrl(href)
                self.selenium.WaitFor(objects.resend_turnonRule)
                self.selenium.Click(objects.resend_turnonRule)
                self.Logout()
                isSuccess = True
            except:
                pass

    def AddAccount(self, name):
        isSuccess = False
        while isSuccess == False:
            try:
                self.selenium.OpenUrl("https://passport.yandex.ru/registration/mail?from=mail&require_hint=1&origin=hostroot_reliable_l&retpath=https%3A%2F%2Fpassport.yandex.ru%2Fpassport%3Fmode%3Dsubscribe%26from%3Dmail%26retpath%3Dhttps%253A%252F%252Fmail.yandex.ru")
                self.selenium.WaitFor(objects.finishReg_accName)
                self.selenium.SendKeys(objects.finishReg_accName, getRandomData.GetRandomFemaleRandomName().decode('utf-8'))
                self.selenium.SendKeys(objects.finishReg_accLastname, getRandomData.GetRandomFemaleRandomLastname().decode('utf-8'))
                self.selenium.SendKeys(objects.finishReg_login, name)
                self.selenium.SendKeys(objects.finishReg_password, "123qweasd")
                self.selenium.SendKeys(objects.finishReg_confirmPassword, "123qweasd")
                self.selenium.SetSelectOptionByIndex(objects.finishReg_controlQuestion, 1)
                self.selenium.SendKeys(objects.finishReg_controlAnswer, "qwerty")
                imgPath = self.selenium.GetElementAttribute(objects.finishReg_captchaImg, "src")
                urllib.urlretrieve(imgPath, "securecode.jpg")
                captcha = GetAntigate("securecode.jpg")
                self.selenium.SendKeys(objects.finishReg_captchaAnswer, captcha)
                self.selenium.Click(objects.finishReg_endRegistraionButton)
                self.selenium.WaitFor(objects.letters_messages)
                self.Logout()
                isSuccess = True
                return name
            except:
                pass

    def DeleteUrl(self, url):
        self.selenium.WaitFor(objects.delete_url_url)
        self.selenium.SendKeys(objects.delete_url_url, url)
        self.selenium.Click(objects.delete_url_delete_button)
        #self.selenium.WaitForText("добавлен в очередь на удаление")

    def AddRights(self, ftpServer, ftpDir, bWaitForIcon = True):
        try:
            self.selenium.Click(objects.addSite_addSiteButton_HtmlFileTab)
            self.selenium.WaitFor(objects.addSite_downloadFileLink)
            filename = self.selenium.GetText(objects.addSite_downloadFileLink)
            fileUrl = self.selenium.GetElementAttribute(objects.addSite_downloadFileLink, "href")
            urllib.urlretrieve(fileUrl, filename)

            self.sftp.put(filename, "{0}/{1}".format( ftpDir, filename))
            os.remove(filename)

            success = False
            counter = 0
            while success == False:
                if counter >= 2:
                    break
                self.selenium.Click(objects.addSite_CheckFile)
                time.sleep(1)
                bError = False
                while self.selenium.IsVisible(objects.captchaImg):
                    if bError == True:
                        self.captcha.ReportIncorrectImageCaptcha(captchaText[1])
                    imgPath = self.selenium.GetElementAttribute(objects.captchaImg, "src")
                    captchaText = self.captcha.SolveTextCaptcha(imgPath)
                    self.selenium.SendKeys(objects.captchaValueEdit, captchaText[0])
                    self.selenium.Click(objects.sendCaptchaButton)
                    self.selenium.Sleep(1)
                    bError = True
                try:
                    time.sleep(2)
                    if self.selenium.IsTextsPresents(["потому что не нашли сайт в DNS.", "код ответа сервера не 200"]) == True:
                        success = True
                        return -1
                    if bWaitForIcon == True:
                        self.selenium.WaitForText(objects.addSite_Icon)
                    else:
                        self.selenium.WaitFor(objects.addSite_dropRights)

                    success = True
                    return 0
                except:
                    counter += 1
        except:
            return -1

    def AddSiteToWebmaster(self, siteUrl, ftpServer, ftpDir):
        self.selenium.OpenUrl("https://webmaster.yandex.ru/sites/add/")
        self.selenium.WaitFor(objects.addSite_addSiteButton)
        self.selenium.SendKeys(objects.addSite_addSiteInput, siteUrl)
        self.selenium.Sleep(1)
        try:
            self.selenium.Click(objects.addSite_addSiteButton)
            self.selenium.Sleep(2)
            if self.selenium.IsTextsPresents(["уже добавлен.", "вам не принадлежит. "]) == True:
                return
        except:
            return -1

        try:
            self.selenium.WaitFor(objects.addSite_addSiteButton_HtmlFileTab)
        except:
            return -1

        return self.AddRights(ftpServer, ftpDir, False)

    def Destructor(self):
        self.ssh_.close()
        self.selenium.CleanUp()


