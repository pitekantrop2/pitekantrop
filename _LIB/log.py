﻿import datetime
import os.path

def GetLogFileName(basePath, descr = ""):
    Now = datetime.datetime.now()
    sToday = "%(d)s_%(m)s_%(h)s_%(min)s"%{"m":Now.month ,"d":Now.day, "h":Now.hour, "min":Now.minute }
    logFilePath = basePath + "\\" + descr + "_" + sToday + ".log"
    return logFilePath

class Log():

    logFilePath = None

    def __init__(self, logFilePath):
        self.logFilePath = logFilePath

    def AddRecord(self,text):
        with open(self.logFilePath, "a") as myfile:
            myfile.write(text + "\n")

    def Log(self,text, timeStamp = False):
        if type(text) in [type(long(1)), type(int(1))]:
            text = str(text)
        Now = datetime.datetime.now()
        sNow = "%(h)s:%(m)s:%(s)s "%{"h":Now.hour ,"m":Now.minute ,"s":Now.second}
        if timeStamp == False:
            sNow = ""
        with open(self.logFilePath, "a") as myfile:
            myfile.write(sNow + text.encode('cp1251') + "\n")

    def LogInfoMessage(self,text):
        self.Log(" [Info] " + text)

    def LogWarningMessage(self,text):
        self.Log(" [Warning] " + text)

    def LogErrorMessage(self,text):
        self.Log(" [Error] " + text)




