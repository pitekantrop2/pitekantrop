﻿# -*- coding: utf-8 -*-
import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
import MySQLdb
import io
import mimetypes
import json
import hashlib
import time
from lxml.builder import E
from datetime import date
from selenium import webdriver
import myLibrary
import USDEKObjects
import commonLibrary
##import win32api
##import win32com.client
from xml.etree import ElementTree
import datetime

class SdekAPI:
    calculateUrl = "http://api.cdek.ru/calculator/calculate_price_by_json.php"
    newOrderUrl = "https://integration.cdek.ru/new_orders.php"
    newDeliveryUrl = "https://integration.cdek.ru/addDelivery"
    deleteOrderUrl = "https://integration.cdek.ru/delete_orders.php"
    printFormUrl = "https://integration.cdek.ru/orders_print.php"
    callCourierUrl = "https://integration.cdek.ru/call_courier.php"
    infoReportUrl = "https://integration.cdek.ru/info_report.php"
    pvzUrl = "https://integration.cdek.ru/pvzlist.php"
    getOrderStatusUrl = "https://integration.cdek.ru/status_report_h.php"
    requisitionUrl = "http://lkff.cdek.ru/SOAP/soap.php"
    fulfilmentLKUrl = "http://GTN01:AAbb77dd@cdekff.ddns.net:40300/"
    LKUrl = "http://lknew.cdek.ru/"

    login = None
    password = None
    test = None
    selenium = None
    pvzTable = None
    fulfilmentObjects = USDEKObjects.USDEKObjects()
    todayFormatDate = date.today().strftime('%Y-%m-%d')
    pvzXml = None
    apiUser = "Onwer54408"
    apiSecure = "871ECC0A-A08C-4819-A0CE-0435758888A9"
    login_1_5 = "iix55tq8ldhr9fysxg2380l84hpzxsy4"
    password_1_5 = "ddt6w6jfkps7tdzvljq52v2x3ulbexre"

    posylkaCodes = {
    "SS" : "136",
    "SD" : "137",
    "DS" : "138" ,
    "DD" : "139"
    }

    economyPosylkaCodes = {
    "SS" : "234",
    "SD" : "233"
    }

    orderStates = {
    1:	u"Создан",
    2:	u"Удален",
    3:	u"Принят на склад отправителя",
    6:	u"Выдан на отправку в г. отправителе",
    16:	u"Возвращен на склад отправителя",
    7:	u"Сдан перевозчику в г. отправителе",
    21:	u"Отправлен в г. транзит",
    22:	u"Встречен в г. транзите",
    13:	u"Принят на склад транзита",
    17:	u"Возвращен на склад транзита",

    19:	u"Выдан на отправку в г. транзите",
    20:	u"Сдан перевозчику в г. транзите",
    8:	u"Отправлен в г. получатель",
    9:	u"Встречен в г. получателе",
    10:	u"Принят на склад доставки",
    12:	u"Принят на склад до востребования",
    11:	u"Выдан на доставку",
    18:	u"Возвращен на склад доставки",

    4:	u"Вручен",
    5:	u"Не вручен"
    }

    def __init__(self, test = False, login = "8a4a7f3c6834f7378154db9c1380e968", password = "29f3888fc6ffc5556ae5865a193bf188"):
        self.login = login
        self.password = password
        self.test = test
        self.pvzTable = commonLibrary.GetPvzTable()

    def Calculate(self, data):
        data['authLogin'] = self.login
        m = hashlib.md5()
        m.update(data['dateExecute'] + "&" + self.password)
        data['secure'] = m.hexdigest()
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

        bSuccess = False
        while bSuccess == False:
            try:
                resp = requests.post(self.calculateUrl,  json=data, headers=headers)
                bSuccess = True
            except:
                time.sleep(1)
        return json.loads(resp._content)

    def GetTariffByTariffId(self, tariffId):
        if tariffId in [234, 136, 10, 62]:
            return "SS"
        if tariffId in [137, 233, 11]:
            return "SD"
        if tariffId in [138, 12]:
            return "DS"
        if tariffId in [139, 1]:
            return "DD"

    def GetTariffId(self, cityId, tariff, sendCityCode):
        tariffId = -1
        if tariff == "SS":
            tariffs = [234, 136, 10, 62]
        if tariff == "SD":
            tariffs = [137, 233, 11]
        if tariff == "DS":
            tariffs = [138, 12]
        if tariff == "DD":
            tariffs = [139, 1]

        minCost = 100000
        deliveryCost = 0
        calcResult = None

        for tariffid in tariffs:
            requestData = {}
            requestData['version'] = "1.0"
            requestData['dateExecute'] = datetime.date.today().strftime('%Y-%m-%d')
            requestData['senderCityId'] = sendCityCode
            requestData['receiverCityId'] = int(cityId)
            requestData['tariffId'] = tariffid
            requestData['goods'] = [{'weight':1, 'length':20, 'width':20,'height':20}]

            try:
                calcResult_ = self.Calculate(requestData)
                if "error" in calcResult_.keys():
##                    print calcResult_["error"][0]["text"]
                    continue

                deliveryCost = int(calcResult_["result"]["price"])
                if deliveryCost < minCost:
                    minCost = deliveryCost
                    calcResult = calcResult_
                    tariffId = tariffid
            except:
                continue

        return [tariffId, minCost]

    def CreateDelivery(self, data):
        DeliveryRequest = etree.Element('DeliveryRequest')
        doc = etree.ElementTree(DeliveryRequest)

        data["Number"] = str(datetime.datetime.now().microsecond)

        DeliveryRequest.attrib["Number"] = data["Number"]
        DeliveryRequest.attrib["Date"] = self.todayFormatDate
        DeliveryRequest.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        DeliveryRequest.attrib["Secure"] = m.hexdigest()
        DeliveryRequest.attrib["OrderCount"] = "1"
        DeliveryRequest.attrib["ForeignDelivery"] = "false"
        Order = etree.SubElement(DeliveryRequest, 'Order')
        Order.attrib["ClientSide"] = data["ClientSide"]
        Order.attrib["Number"] = data["Number"]
        Order.attrib["SendCityCode"] = data["SendCityCode"]
        Order.attrib["RecCityCode"] = data["RecCityCode"]
        Sender = etree.SubElement(Order, 'Sender')
        Sender.attrib["Name"] = "sender_name"
        Sender.attrib["Company"] = "sender_company"
        if data["NeedPickup"] == True:
            SendAddress = etree.SubElement(Sender, 'Address')
            SendAddress.attrib["Street"] = data["SendStreet"]
            SendAddress.attrib["House"] = data["SendHouse"]
            SendAddress.attrib["Flat"] = data["SendFlat"]
        SendPhone = etree.SubElement(Sender, 'Phone')
        SendPhone.text = data["SendPhone"]
        Order.attrib["RecipientCompany"] = u"recipient_company"
        Order.attrib["RecipientName"] = u"recipient_name"
        Order.attrib["Phone"] = data["RecipientPhone"]
        Order.attrib["TariffTypeCode"] = data["TariffTypeCode"]

        Address = etree.SubElement(Order, 'Address')
        Address.attrib["PvzCode"] = data["RecPVZ"]

        counter = 1
        for place in data["Places"].split("/"):
            Package = etree.SubElement(Order, 'Package')
            Package.attrib["Number"] = str(counter)
            Package.attrib["BarCode"] = str(counter)
            placeParam = place.strip().split("_")
            Package.attrib["Weight"] = str( int(placeParam[0].replace(".",",")) * 1000)
            Package.attrib["SizeA"] = placeParam[1]
            Package.attrib["SizeB"] = placeParam[2]
            Package.attrib["SizeC"] = placeParam[3]
            Package.attrib["Comment"] = u"Komment{0}".format(counter)
            counter += 1

        AddService = etree.SubElement(Order, 'AddService')
        AddService.attrib["ServiceCode"] = "2"
        AddService.attrib["Sum"] = str(data["itemsCost"])

        request = """<?xml version="1.0" encoding="UTF-8" ?>""" + etree.tostring(doc)
        request = request.replace("recipient_company", data["RecipientCompany"])
        request = request.replace("recipient_name", data["RecipientName"])
        request = request.replace("sender_name", data["SenderName"])
        request = request.replace("sender_company", data["SenderCompany"])

        for i in range(1, counter):
            request = request.replace("Komment{0}".format(i), u"Техника")
            i += 1

        resp = self.SendRequest(self.newDeliveryUrl, request)

        with open("resp.xml","w") as f:
            f.write(resp)

        with open("request.xml","w") as f:
            f.write(request.encode('cp1251'))

        if resp.find("ErrorCode") != -1:
            return -1

##        try:
##            dispatchNumber = Soup(resp).response.order.attrs[1][0]
##            if data["NeedPickup"] == True:
##                callNumber = Soup(resp).response.call.attrs[1][1]
##                return [dispatchNumber, callNumber]
##        except:
##            dispatchNumber = resp

        return dispatchNumber

    def CreateOrder(self, data):
        DeliveryRequest = etree.Element('DeliveryRequest')
        doc = etree.ElementTree(DeliveryRequest)

        if self.test == True:
            data["Number"] = str(datetime.datetime.now().microsecond)

        DeliveryRequest.attrib["Number"] = data["Number"]
        DeliveryRequest.attrib["Date"] = self.todayFormatDate
        DeliveryRequest.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        DeliveryRequest.attrib["Secure"] = m.hexdigest()
        DeliveryRequest.attrib["OrderCount"] = "1"

        Order = etree.SubElement(DeliveryRequest, 'Order')
        Order.attrib["Number"] = data["Number"]
        if "DeliveryRecipientCost" in data.keys():
            Order.attrib["DeliveryRecipientCost"] = data["DeliveryRecipientCost"]
            if self.login == "8a4a7f3c6834f7378154db9c1380e968":
                Order.attrib["DeliveryRecipientVATRate"] = "VATX"
            else:
                Order.attrib["DeliveryRecipientVATRate"] = "VAT18"
        Order.attrib["SendCityCode"] =  data["SendCityCode"]

        if data["RecCityName"][0].isdigit() == True:
            recCityCode = data["RecCityName"]
        else:
            recCityCode = str(self.GetCityCode(data["RecCityName"]))
##            print data["RecCityName"]
##            Order.attrib["RecCityCode"] = str(commonLibrary.GetSdekCityCode(data["RecCityName"]))

        Order.attrib["RecCityCode"] = recCityCode
        Order.attrib["RecipientName"] = "RecName"
##        if "RecipientEmail" in data.keys():
##            Order.attrib["RecipientEmail"] = data["RecipientEmail"]
        Order.attrib["Phone"] = data["RecipientPhone"]
        Order.attrib["Comment"] = ""
        Order.attrib["SellerName"] = "SelName"

        if "TariffCode" in data.keys():
            Order.attrib["TariffTypeCode"] = data["TariffCode"]
        elif "TariffId" in data.keys():
            Order.attrib["TariffTypeCode"] = str(data["TariffId"])
        else:
            tariffId, deliveryCost = self.GetTariffId(recCityCode,data["Tariff"], data["SendCityCode"] )
            if tariffId == -1:
                return [-1, -1]
            Order.attrib["TariffTypeCode"] = str(tariffId)

        if "Tariff" not in data.keys():
            data["Tariff"] = self.GetTariffByTariffId(data["TariffId"])

        Address = etree.SubElement(Order, 'Address')
        if data["Tariff"] == "DS" or data["Tariff"] == "SS":
            if "PvzCode" in data.keys():
                Address.attrib["PvzCode"] = str(data["PvzCode"])
                cityCode = self.GetPvzCityCode(data["PvzCode"])
                Order.attrib["RecCityCode"] = cityCode
            elif "RecPVZName" in data.keys():
                Address.attrib["PvzCode"] = data["RecPVZName"]
            else:
                Address.attrib["PvzCode"] = str(self.GetPVZCode(data["RecCityName"], data["RecPointName"]))
        else:
            Address.attrib["Street"] = "RecStreet"
            Address.attrib["House"] = "RecHouse"
            Address.attrib["Flat"] = "RecFlat"
            if "RecComment" in data.keys():
                Address.attrib["Comment"] = "RecComment"

        if "Places" not in data.keys():#доставка до клиента
            Package = etree.SubElement(Order, 'Package')
            Package.attrib["Number"] = "1"
            Package.attrib["BarCode"] = "1"
            if "Items" in data.keys():
                totalWeight = 0
                i = 1
                for item in data["Items"]:
                    Item = etree.SubElement(Package, 'Item')
                    Item.attrib["WareKey"] = item["WareKey"]
                    Item.attrib["Cost"] = item["Cost"]
                    Item.attrib["Payment"] = item["Payment"]
                    if self.login == "8a4a7f3c6834f7378154db9c1380e968":
                        Item.attrib["PaymentVATRate"] = "VATX"
                    else:
                        Item.attrib["PaymentVATRate"] = "VAT20"
                    Item.attrib["Amount"] = item["Amount"]
                    if item["Comment"].lower().find(u"шипы") != -1:
                        Item.attrib["Weight"] = "20"
                        weight = 20
                    elif item["Comment"].lower().find(u"ундшту") != -1:
                        weight = 5
                        Item.attrib["Weight"] = "5"
                    else:
                        Item.attrib["Weight"] = "300"
                        weight = 300
                    totalWeight += weight * int(item["Amount"])
                    Item.attrib["Comment"] = "Comment" + str(i)
                    i += 1

                Package.attrib["Weight"] = str(totalWeight)
                data["Weight"] = str(totalWeight)

            else:
                Package.attrib["Weight"] = data["Weight"]
                Item = etree.SubElement(Package, 'Item')
                Item.attrib["WareKey"] = "1"
                Item.attrib["Cost"] = data["PackageCost"]
                Item.attrib["Payment"] = "0"
                Item.attrib["Weight"] = data["Weight"]
                Item.attrib["Amount"] = "1"
                Item.attrib["Comment"] = "PackageDescription"

        else:#доставка нам
            totalWeight = 0
            if data["Contractor"] in ["SDEKMoscow"]:
                Package = etree.SubElement(Order, 'Package')
                Package.attrib["Number"] = "1"
                Package.attrib["BarCode"] = "1"
            places = data["Places"].split("/")
            if "Items" not in data.keys():
                data["Items"] = []
            for i in range(0, len(data["Items"])):
                item = data["Items"][i]
                if data["Contractor"] != "SDEKMoscow":
                    Package = etree.SubElement(Order, 'Package')
                    Package.attrib["Number"] = str(i + 1)
                    Package.attrib["BarCode"] = str(i + 1)
                    Package.attrib["Weight"] = str(int (places[i].split("_")[0]) * 1000)
                Item = etree.SubElement(Package, 'Item')
                Item.attrib["WareKey"] = item["WareKey"]
                Item.attrib["Cost"] = item["Cost"]
                Item.attrib["Payment"] = item["Payment"]
                if self.login == "8a4a7f3c6834f7378154db9c1380e968":
                    Item.attrib["PaymentVATRate"] = "VATX"
                else:
                    Item.attrib["PaymentVATRate"] = "VAT20"
                Item.attrib["Amount"] = item["Amount"]
                itemWeight = str (int(float(item["Weight"])*1000))
                Item.attrib["Weight"] =  itemWeight
                totalWeight += int(itemWeight)
                Item.attrib["Comment"] = "Comment" + str(i+1)

            if data["Contractor"] == "SDEKMoscow":
                Package.attrib["Weight"] = str(totalWeight)

        if data["Tariff"][0] == "D":
            CallCourier  = etree.SubElement(DeliveryRequest, 'CallCourier')
            CallCourier.attrib["Date"] = self.todayFormatDate
            Call = etree.SubElement(CallCourier, 'Call')
            Call.attrib["Date"] = data["CourierDate"]
            Call.attrib["TimeBeg"] = data["CourierTimeBeg"]
            Call.attrib["TimeEnd"] = data["CourierTimeEnd"]
            Call.attrib["SendCityCode"] = data["SendCityCode"]
            Call.attrib["SendPhone"] = data["SendPhone"]
            Call.attrib["SenderName"] = u"SenName"
            Call.attrib["Comment"] = "SendComment"

            Address = etree.SubElement(Call, 'SendAddress')
            Address.attrib["Street"] = "SendStreet"
            Address.attrib["House"] = "SendHouse"
            Address.attrib["Flat"] = data["SendFlat"]

        request = """<?xml version="1.0" encoding="UTF-8" ?>""" + etree.tostring(doc)
        request = request.replace("RecName", data["RecipientName"].replace("\"", ""))
        if self.login == "8a4a7f3c6834f7378154db9c1380e968":
            request = request.replace("SelName", u"ИП Усов Иван Викторович")
        else:
            request = request.replace("SelName", u"ООО Новалл")

        if data["Tariff"][-1] == "D":
            request = request.replace("RecStreet", data["RecStreet"])
            request = request.replace("RecHouse", data["RecHouse"])
            request = request.replace("RecFlat", data["RecFlat"])
            if "RecComment" in data.keys():
                request = request.replace("RecComment", data["RecComment"].replace("\"",""))

        if "Items" in data.keys():
            for i in range(0, len(data["Items"])):
                item = data["Items"][i]
                request = request.replace ("Comment" + str(i + 1), item["Comment"].replace("\"",""))
        else:
            request = request.replace ("PackageDescription", data["PackageDescription"].replace("\"",""))

        if data["Tariff"][0] == "D":
            request = request.replace("SenName", data["SenderName"])
            request = request.replace("SendStreet", data["SendStreet"])
            request = request.replace("SendHouse", data["SendHouse"])

        AddService = etree.SubElement(Order, 'AddService')
        AddService.attrib["ServiceCode"] = "37"
        AddService = etree.SubElement(Order, 'AddService')
        AddService.attrib["ServiceCode"] = "36"

        resp = self.SendRequest(self.newOrderUrl, request)

##        if self.test == True:
##        with open("resp.xml","w") as f:
##            f.write(resp.encode('utf-8'))
##
##        with open("request.xml","w") as f:
##            f.write(request.encode('utf-8'))

        xml = etree.XML(resp)
        try:
            dispatchNumber = commonLibrary.GetParamValueFromXml(xml, "//Order", "DispatchNumber")
        except Exception as e:
            dispatchNumber = resp

        if dispatchNumber.lower().find("error") != -1:
            with open("resp.xml","w") as f:
                f.write(resp.encode('utf-8'))
            return [-1, -1]

        if data["Tariff"][0] == "D":
            try:
                courierRequestNumber = commonLibrary.GetParamValueFromXml(xml, "//Call", "Number")
            except:
                courierRequestNumber = -1
            return [dispatchNumber, deliveryCost, courierRequestNumber]

        if 'deliveryCost' not in locals():
            deliveryCost = 0

        return [dispatchNumber, deliveryCost]

    def DeleteOrder(self, number):
        DeleteRequest = etree.Element('DeleteRequest')
        doc = etree.ElementTree(DeleteRequest)

        DeleteRequest.attrib["Number"] = number
        DeleteRequest.attrib["Date"] = self.todayFormatDate
        DeleteRequest.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        DeleteRequest.attrib["Secure"] = m.hexdigest()
        DeleteRequest.attrib["OrderCount"] = "1"

        Order = etree.SubElement(DeleteRequest, 'Order')
        Order.attrib["Number"] = number

        request = """<?xml version="1.0" encoding="UTF-8" ?>""" + etree.tostring(doc)
        resp = self.SendRequest(self.deleteOrderUrl, request)

        if self.test == True:
            with open("resp.xml","w") as f:
                f.write(resp)

            with open("request.xml","w") as f:
                f.write(request.encode('cp1251'))

        if resp.find("ErrorCode") != -1:
            mes = commonLibrary.GetParamValueFromXml(resp, "//Order", "Msg")
            return mes
        else:
            return True

        return dispatchNumber

    def CallCourier(self, data):
        CallCourier = etree.Element('CallCourier')
        doc = etree.ElementTree(CallCourier)

        CallCourier.attrib["Date"] = self.todayFormatDate

        CallCourier.attrib["Account"] = self.login
        CallCourier.attrib["CallCount"] = "1"
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        CallCourier.attrib["Secure"] = m.hexdigest()

        Call = etree.SubElement(CallCourier, 'Call')
        Call.attrib["Date"] = data["CourierDate"]
        Call.attrib["TimeBeg"] = data["CourierTimeBeg"]
        Call.attrib["TimeEnd"] = data["CourierTimeEnd"]
        Call.attrib["SendCityCode"] = data["SendCityCode"]
        Call.attrib["SendPhone"] = data["SendPhone"]
        Call.attrib["SenderName"] = u"SenName"
        Call.attrib["Weight"] = "10000"
        Call.attrib["Comment"] = "SendComment"

        Address = etree.SubElement(Call, 'Address')
        Address.attrib["Street"] = "SendStreet"
        Address.attrib["House"] = "SendHouse"
        Address.attrib["Flat"] = data["SendFlat"]

        request = """<?xml version="1.0" encoding="UTF-8" ?>""" + etree.tostring(doc)
        request = request.replace("SenName", data["SenderName"])
        request = request.replace("SendStreet", data["SendStreet"])
        request = request.replace("SendHouse", data["SendHouse"])
        request = request.replace("SendComment", data["SendComment"])

        resp = self.SendRequest(self.callCourierUrl, request)

        try:
            callNumber = commonLibrary.GetParamValueFromXml(resp, "//Call", "Number" )
        except:
            callNumber = resp

        return callNumber

    def GetPrintForm(self, dispatchNumber):
        OrdersPrint = etree.Element('OrdersPrint')
        doc = etree.ElementTree(OrdersPrint)

        OrdersPrint.attrib["OrderCount"] = "1"
        OrdersPrint.attrib["Date"] = self.todayFormatDate
        OrdersPrint.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        OrdersPrint.attrib["Secure"] = m.hexdigest()

        Order = etree.SubElement(OrdersPrint, 'Order')
        Order.attrib["DispatchNumber"] = dispatchNumber
        Order.attrib["CopyCount"] = "4"
        content = self.SendRequest(self.printFormUrl, etree.tostring(doc))

        words = ["danger", "ERR"]
        for word in words:
            if content.find(word) != -1:
                return content

        filePath = "printforms\\" + str(dispatchNumber) + '.pdf'

        try:
            FILE = open(filePath, "wb")
            FILE.write(content)
            FILE.close()
        except Exception as e:
            print str(e)
            return -1

        return filePath

    def GetOrderState(self, dispatchNumber):
        orderStatesCodes = self.orderStates.keys()
        StatusReport = etree.Element('StatusReport')
        doc = etree.ElementTree(StatusReport)

        StatusReport.attrib["ShowHistory"] = "1"
        StatusReport.attrib["Date"] = self.todayFormatDate
        StatusReport.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        StatusReport.attrib["Secure"] = m.hexdigest()

        Order = etree.SubElement(StatusReport, 'Order')
        Order.attrib["DispatchNumber"] = dispatchNumber

        try:
            response = self.SendRequest(self.getOrderStatusUrl, etree.tostring(doc))
##            with open("response.xml", "w") as log:
##                log.write(response.encode("utf-8"))
            xml = etree.XML(response)
            root =  xml.getroottree().getroot()
        except Exception as e:
            with open("error.txt", "w") as log:
                log.write(str( e).encode("utf-8"))
            return -1

        if response.lower().find("error") != -1:
##            print response
##            with open("response.xml", "w") as log:
##                log.write(response.encode("cp1251"))
            return -1

        statesCodes = [ int(i.attrib["Code"]) for i in root.xpath("//State")]

        statesCodes = list( set(statesCodes))

        if 4 in statesCodes:
            return "delivered"

        if 12 in statesCodes or 11 in statesCodes or 10 in statesCodes:
            return "shipped"

        if len(statesCodes) > 1:
            return "in_transit"

        if 1 in statesCodes and len(statesCodes) == 1:
            return "in_process"

        return -1

    def GetOrders(self, startDate, endDate):
        orders = []
        InfoRequest = etree.Element('InfoRequest')
        doc = etree.ElementTree(InfoRequest)

        InfoRequest.attrib["Date"] = self.todayFormatDate
        InfoRequest.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        InfoRequest.attrib["Secure"] = m.hexdigest()

        ChangePeriod = etree.SubElement(InfoRequest, 'ChangePeriod')
        ChangePeriod.attrib["DateBeg"] = (datetime.date.today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
        ChangePeriod.attrib["DateEnd"] = (datetime.date.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d')

##        Order = etree.SubElement(InfoRequest, 'Order')
##        Order.attrib["Number"] = orderNumber
##        Order.attrib["Date"] = self.todayFormatDate

        try:
            response = self.SendRequest(self.infoReportUrl, etree.tostring(doc))
##            print response
            xml = etree.XML(response)
            root =  xml.getroottree().getroot()
            orders_ = root.xpath("//Order")
            for order in orders_:
                orders[order.attrib["Number"]] = order.attrib["DispatchNumber"]
            return dispatchNumber
        except:
            return -1



    def GetOrderReturnInvoiceNumber(self, dispatchNumber):
        StatusReport = etree.Element('StatusReport')
        doc = etree.ElementTree(StatusReport)

        StatusReport.attrib["ShowReturnOrder"] = "1"
        StatusReport.attrib["Date"] = self.todayFormatDate
        StatusReport.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        StatusReport.attrib["Secure"] = m.hexdigest()

        Order = etree.SubElement(StatusReport, 'Order')
        Order.attrib["DispatchNumber"] = dispatchNumber

        try:
            response = self.SendRequest(self.getOrderStatusUrl, etree.tostring(doc))
##            with open("response.txt", "w") as log:
##                log.write(response)

            states = []

            xml = etree.XML(response)
            root =  xml.getroottree().getroot()
        except:
            return -1

        try:
            number = root.xpath("//Order")[0].attrib["ReturnDispatchNumber"]
            return number
        except:
            return -1


    def GetPVZCode(self, city, name):
        name = name.strip()
        pvzList = commonLibrary.GetCitySdekPvzList(self.pvzTable, city)
        for pvz in pvzList:
            if pvz[1].find(name) != -1:
                return pvz[0]
        return -1

    def GetCityCode(self, city):
        try:
            pvzList = commonLibrary.GetCitySdekPvzList(self.pvzTable, city)
            return pvzList[0][2]
        except:
            return -1

    def GetCityFromCode(self, code):
        return commonLibrary.GetCityFromCode(code)

    def GetCityFromId(self, code):
        return commonLibrary.GetCityFromId(code)

    def GetPvzCityCode(self, code):
        return str(commonLibrary.GetPvzCityCode(code))

    def GetPVZDetails(self, city, name):
        try:
            if city != "":
                pvz = [i for i in commonLibrary.GetCitySdekPvzList(self.pvzTable, city) if i[1].find(name) != -1][0]
            else:
                pvz = commonLibrary.GetPvzByCode(name)
            return pvz
        except:
            return -1

    def GetChangedOrdersForPeriod(self, dateStart, dateEnd, tariffTypeCodes = ["136", "62"]):
        InfoRequest = etree.Element('InfoRequest')
        doc = etree.ElementTree(InfoRequest)

        InfoRequest.attrib["Date"] = self.todayFormatDate
        InfoRequest.attrib["Account"] = self.login
        m = hashlib.md5()
        m.update(self.todayFormatDate + "&" + self.password)
        InfoRequest.attrib["Secure"] = m.hexdigest()

        ChangePeriod = etree.SubElement(InfoRequest, 'ChangePeriod')

        ChangePeriod.attrib["DateBeg"] = dateStart.strftime('%Y-%m-%d')
        ChangePeriod.attrib["DateEnd"] = dateEnd.strftime('%Y-%m-%d')

        cont = self.SendRequest(self.infoReportUrl, etree.tostring(doc))

        xml = etree.XML(cont)
        root =  xml.getroottree().getroot()

        typeCodesCondition = ""

        if len(tariffTypeCodes) != 0:
            typeCodesCondition = " and contains('{0}', @TariffTypeCode)".format(" ".join(tariffTypeCodes))

        orders = root.xpath("//Order[@Date = '{0}' or @Date = '{1}' {2}]"
        .format(dateStart.strftime('%Y-%m-%d'), dateEnd.strftime('%Y-%m-%d'), typeCodesCondition))

        dispatchNumbers = {}

        for order in orders:
            dispatchNumbers[order.attrib["DispatchNumber"]] = order.xpath("./SendCity")[0].attrib["Code"]

        return dispatchNumbers


    def LoginInFulfilmentLK(self):
        objects = self.fulfilmentObjects

        if self.selenium == None:
            self.selenium = myLibrary.Selenium()
            self.selenium.SetUp()

        self.selenium.OpenUrl( self.fulfilmentLKUrl + "C5O_Login")

        self.selenium.SendKeys(objects.login, u"Onwer54408")
        self.selenium.SendKeys(objects.password, u"7F25F369")
        self.selenium.Click(objects.loginButton)
        self.selenium.WaitFor(objects.userProfileMenu)

    def LoginInLK(self):
        objects = self.fulfilmentObjects

        if self.selenium == None:
            self.selenium = myLibrary.Selenium()
            self.selenium.SetUp()

        if self.login == "8a4a7f3c6834f7378154db9c1380e968":
            contractNumber = u"ИМ10465"
        else:
            contractNumber = u"ИМ12369564"

        self.selenium.OpenUrl( self.LKUrl + "?language-picker-language=ru-RU")
        self.selenium.WaitFor(objects.lKlogin)

        if self.login == "8a4a7f3c6834f7378154db9c1380e968":
            self.selenium.SendKeys(objects.lKlogin, contractNumber)
            self.selenium.SendKeys(objects.lKpassword, u"1qaz@WSX3edc")
        else:
            self.selenium.SendKeys(objects.lKlogin, contractNumber)
            self.selenium.SendKeys(objects.lKpassword, u"1qaz@WSX3edc")
        self.selenium.Click(objects.lKloginButton)
        time.sleep(5)

    def CloseSelenium(self):
        try:
            self.selenium.CleanUp()
        except:
            pass
        self.selenium = None

    def CorrectSendCity(self, city):
        if city == u"Москва":
            return u"Москва, Москва, Россия"
        if city == u"Химки":
            return u"Химки Новые, Московская обл., Россия"

        return city

    def CreateInvoice(self, data):
        shell = win32com.client.Dispatch("WScript.Shell")
        returnData = []
        objects = self.fulfilmentObjects
        bToClientDelivery = data["RecipientName"] not in [u"Фулфилмент", u"Усов Иван", u"Расконсолидация"]
        bIMDelivery = "Items" in data.keys()
        bToOfficeDelivery = data["RecipientName"] != u"Фулфилмент"
        self.LoginInLK()
        self.selenium.MaximizeWindow()
        self.selenium.OpenUrl(self.LKUrl + "order/new-order?language-picker-language=ru-RU")
##        self.selenium.WaitFor(objects.createInvoice_calculateButton)

        #выбор города
        sendCity = self.GetCityFromCode(data["SendCityCode"])
        if bIMDelivery:
            self.selenium.SetSelectOptionByIndex(objects.createInvoice_deliveryType, 1)
            if bToClientDelivery or not bToOfficeDelivery:
                self.selenium.SetSelectOptionByIndex(objects.createInvoice_payerType, 2)
            else:
                self.selenium.SetSelectOptionByIndex(objects.createInvoice_payerType, 1)
        else:
            self.selenium.SetSelectOptionByIndex(objects.createInvoice_deliveryType, 0)
            if bToOfficeDelivery and data["Tariff"] != u"Забор для ИМ дверь-склад":
                self.selenium.SetSelectOptionByIndex(objects.createInvoice_payerType, 1)
            else:
                self.selenium.SetSelectOptionByIndex(objects.createInvoice_payerType, 2)
        city = self.CorrectSendCity(sendCity)
        self.selenium.SendKeys(objects.createInvoice_senderCity,city )
        self.selenium.WaitFor(objects.createInvoice_selectOption)
        self.selenium.SetListIndexByText(objects.createInvoice_sendCitiesList,city )

        if data["RecCityName"][0].isdigit() == True:
            recCity = self.GetCityFromId(data["RecCityName"])
            cityId = data["RecCityName"]
        else:
            recCity = data["RecCityName"]
            cityId = self.GetCityCode(recCity)
        city = self.CorrectSendCity(recCity)
        self.selenium.SendKeys(objects.createInvoice_recipientCity,city )
        self.selenium.WaitFor(objects.createInvoice_selectOption)
        self.selenium.SetListIndexByText(objects.createInvoice_recCitiesList,city )

        #заполнение мест
        if bToClientDelivery and "Places" not in data.keys():
            if data["Items"][0]["Comment"].lower().find(u"шипы") != -1:
                itemWeight = 0.05
            else:
                itemWeight = 0.3
            itemsWeight = sum(int(item["Amount"]) for item in data["Items"]) * itemWeight
            data["Places"] = "{0}_10_10_10".format(itemsWeight)
        counter = 1
        while self.selenium.IsVisible(objects.createInvoice_removePlace):
            self.selenium.Click(self.selenium.MyFindElements( objects.createInvoice_removePlace)[-1])
            time.sleep(0.5)
        for place in data["Places"].split("/"):
            placeParam = place.strip().split("_")
            if counter != 1:
                self.selenium.Click(objects.createInvoice_addPlace)
            if not self.selenium.IsVisible( ["id", "wd{0}".format(counter)]):
                counter += 1
            self.selenium.Click(["id", "wd{0}".format(counter)])
            time.sleep(0.5)
            shell.SendKeys("{END}")
            time.sleep(0.5)
            shell.SendKeys("{BACKSPACE}")
            time.sleep(0.5)
            shell.SendKeys("{BACKSPACE}")
            time.sleep(0.5)
            shell.SendKeys("{BACKSPACE}")
##            time.sleep(1)
            self.selenium.SendKeys(["id", "wd{0}".format(counter)], placeParam[0].replace(".",","))
            self.selenium.SendKeys(["id", "l{0}".format(counter)], placeParam[1])
            self.selenium.SendKeys(["id", "w{0}".format(counter)], placeParam[2])
            self.selenium.SendKeys(["id", "h{0}".format(counter)], placeParam[3])
            if data["RecipientName"] == u"Расконсолидация":
                self.selenium.SendKeys(["id", "d{0}".format(counter)], u"Техника")
            counter += 1
            time.sleep(1)
##                self.selenium.WaitFor(["id", "wd{0}".format(counter)])

        if bIMDelivery == False:
            self.selenium.Click(objects.createInvoice_payer)
            if self.login == "8a4a7f3c6834f7378154db9c1380e968":
                contractNumber = u"ИМ10465"
            else:
                contractNumber = u"ИМ12369564"
            if self.selenium.IsVisible(objects.createInvoice_docNumber):
                self.selenium.SendKeys(objects.createInvoice_docNumber, contractNumber)
        self.selenium.Click(objects.createInvoice_calculateButton)
        self.selenium.WaitFor(objects.tariffRow)

        #выбор тарифа
        tariffsRows = self.selenium.MyFindElements(objects.tariffRow)
        if "TariffId" in data.keys():
            sTariff = commonLibrary.GetTariffNameFromId(data["TariffId"])
        else:
            if bToClientDelivery:
                tariffId, deliveryCost = self.GetTariffId(cityId,data["Tariff"], data["SendCityCode"] )
                if tariffId == -1:
                    return -1
                sTariff = commonLibrary.GetTariffNameFromId(tariffId)
            else:
                sTariff = data["Tariff"]
        tariffRow = [x for x in tariffsRows if self.selenium.GetText(x).find(sTariff) != -1]
        if len(tariffRow) != 0:
            tariffRow = tariffRow[0]
            returnData.append(sTariff)
        else:
            self.CloseSelenium()
            return -1
        self.selenium.Click( self.selenium.MyFindElementInElement(tariffRow, ["name", "ServiceID"]))

        time.sleep(4)
        #страховка
        if bToClientDelivery:
            itemsCost = sum(int(float(item["Cost"]) * float(item["Amount"])) for item in data["Items"])
        else:
            itemsCost = data["itemsCost"]
        if itemsCost != 0:
            time.sleep(1)
            self.selenium.Click(objects.createInvoice_additionalServices)
            time.sleep(1)
            self.selenium.WaitFor(objects.insuranceEdit)
            if bIMDelivery == False:
                for i in range(0, 20):
                    try:
                        self.selenium.Click(objects.insuranceCh)
                        break
                    except:
                        shell.SendKeys("{DOWN}")
                        time.sleep(0.5)
            time.sleep(1)
            self.selenium.Click(objects.insuranceEdit)
            shell.SendKeys("{END}")
            for i in range(0, 6):
                shell.SendKeys("{BACKSPACE}")
                time.sleep(0.5)
            self.selenium.SendKeys(objects.insuranceEdit, str(itemsCost))
            time.sleep(2)
##        totalDeliveryCost = self.selenium.GetText(objects.totalDeliveryCosts).replace(u"Итоговая сумма, с учётом всех доп. сборов и налогов: ", "").replace(u" руб", "")
##        returnData.append(totalDeliveryCost)

        #отправитель
        if sTariff.find(u"дверь-") != -1:
            courierDay = data["CourierDate"].split("-")[2]
            if courierDay.startswith("0"):
                courierDay = courierDay.replace("0","")
            self.selenium.Click(objects.createInvoice_courierDate)
            self.selenium.WaitFor(objects.createInvoice_calendar)
            dayElement = ["xpath", "//a[text() = '{0}']".format(courierDay)]
            self.selenium.Click(dayElement)
            self.selenium.WaitForNotVisible(objects.createInvoice_calendar)
            self.selenium.SendKeys(objects.createInvoice_timeFrom, data["CourierTimeBeg"])
            self.selenium.SendKeys(objects.createInvoice_timeTo, data["CourierTimeEnd"])
        self.selenium.SendKeys(objects.createInvoice_senderFio, data["SenderName"])
        self.selenium.SendKeys(objects.createInvoice_senderStreet, data["SendStreet"])
        self.selenium.SendKeys(objects.createInvoice_senderHouse, data["SendHouse"])
        self.selenium.SendKeys(objects.createInvoice_senderFlat, data["SendFlat"])
        if "SenderCompany" in data.keys():
            self.selenium.SendKeys(objects.createInvoice_senderCompany, data["SenderCompany"])
        if "SendComment" in data.keys():
            self.selenium.SendKeys(objects.createInvoice_senderComment, data["SendComment"])

        self.selenium.SendKeys(self.selenium.MyFindElements(objects.createInvoice_Phone)[0], commonLibrary.ClearPhoneNumber(data["SendPhone"]))

        #получатель
        self.selenium.SendKeys(objects.createInvoice_recipientFio, data["RecipientName"])
        if sTariff.find(u"-дверь") != -1:
            self.selenium.SendKeys(objects.createInvoice_recipientStreet, data["RecStreet"])
            self.selenium.SendKeys(objects.createInvoice_recipientHouse, data["RecHouse"])
            self.selenium.SendKeys(objects.createInvoice_recipientFlat, data["RecFlat"])
        else:
            if "RecPointName" in data.keys():
                pointName = data["RecPointName"]
            else:
                pointName = data["RecPVZName"]
            pointName = pointName.replace("  ", " ")
            pointElem = ["xpath", u"//ul[@id='pvz-select-items']//li[contains(.,'{0}')]".format(pointName)]
            point = self.selenium.MyFindElements(pointElem)
            if len(point) == 0:
                self.CloseSelenium()
                return -1
            else:
                self.selenium.Click(point[0])
                self.selenium.WaitFor(objects.createInvoice_recipientChoosePoint)
                self.selenium.Click(objects.createInvoice_recipientChoosePoint)
        self.selenium.SendKeys(self.selenium.MyFindElements(objects.createInvoice_Phone)[1], commonLibrary.ClearPhoneNumber(data["RecipientPhone"]))

        #товары
        if bIMDelivery:
            self.selenium.SendKeys(objects.createInvoice_imOrderNum, data["Number"])
            self.selenium.SendKeys(objects.createInvoice_ttn, data["Number"])
            self.selenium.SendKeys(objects.createInvoice_additionalPayment, data["DeliveryRecipientCost"])
            if self.login != "8a4a7f3c6834f7378154db9c1380e968":
                self.selenium.SetSelectOptionByIndex(objects.createInvoice_additionalPaymentVatRate, 3)
            if self.login == "8a4a7f3c6834f7378154db9c1380e968":
                self.selenium.SendKeys(objects.createInvoice_trueSeller, u"ИП Усов Иван Викторович")
            else:
                self.selenium.SendKeys(objects.createInvoice_trueSeller, u"ООО Новалл")
            counter = 0
            for item in data["Items"]:
                self.selenium.SendKeys(["id", "shop_cod_goods_{0}".format(counter)], item["WareKey"] )
                self.selenium.SendKeys(["id", "shop_name_goods_{0}".format(counter)], item["Comment"] )
                self.selenium.SendKeys(["id", "shop_cost_goods_{0}".format(counter)], str(int(float(item["Cost"]))) )
                if data["orderPaid"] == True:
                    self.selenium.SendKeys(["id", "shop_payment_recipient_{0}".format(counter)], "0" )
                else:
                    self.selenium.SendKeys(["id", "shop_payment_recipient_{0}".format(counter)], str(int(float(item["Payment"]))))
                self.selenium.SendKeys(["id", "shop_number_goods_{0}".format(counter)], item["Amount"])
                if "Weight" in item.keys():
                    self.selenium.SendKeys(["id", "shop_weight_goods_{0}".format(counter)], item["Weight"])
                else:
                    self.selenium.SendKeys(["id", "shop_weight_goods_{0}".format(counter)], str(itemWeight).replace(".",","))
                if self.login != "8a4a7f3c6834f7378154db9c1380e968":
                    self.selenium.SetSelectOptionByIndex(["id", "shop_vat_rate_goods_{0}".format(counter)], 3)
                counter += 1
                if counter < len(data["Items"]):
                    self.selenium.Click(objects.createInvoice_addItem)
                    self.selenium.WaitFor(["id", "shop_cod_goods_{0}".format(counter)])

        #завершение
        self.selenium.Click(objects.createInvoice_previewButton)
        self.selenium.WaitFor(objects.createInvoice_createOrderButton)
        for i in range(0, 20):
            try:
                self.selenium.Click(objects.createInvoice_createOrderButton)
                break
            except:
                shell.SendKeys("{DOWN}")
                time.sleep(0.5)
        self.selenium.WaitFor(objects.createInvoice_invoiceInfo)
        invoiceInfo = self.selenium.GetText(objects.createInvoice_invoiceInfo).split("\n")
        returnData.append(invoiceInfo[0].replace(u"Накладная успешно создана с номером: ", ""))
        try:
            returnData.append(invoiceInfo[1].replace(u"Заявка на вызов курьера с номером: ", ""))
        except:
            pass
        self.CloseSelenium()
        return returnData

    def GetPass(self):
        m = hashlib.md5()
        m.update(self.apiSecure + "|" + date.today().strftime('%Y-%m-%d'))
        return m.hexdigest()

    def GetBalance(self):
        products = []
        xml = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
 <soap:Body>
 <Stock_GetList xmlns="http://cowms.ru/reports">
 <User>[User]</User>
 <Pass>[Pass]</Pass>
 </Stock_GetList>
 </soap:Body>
</soap:Envelope>"""
        res = self.SendSoapRequest("http://cowms.ru/reports/Stock_GetList", xml)
        ind1 = res.find("<WarehouseStock>")
        ind2 = res.find("<ErrorMessage")
        res = res[ind1:ind2]
        root = etree.XML(res)
        skus = root.xpath("//WarehouseStock//WarehouseStock")
        for i in range(0, len(skus)):
            sku = skus[i]
            Sku_id = sku.xpath(".//Sku_id")[0].text
            StockType_id = sku.xpath(".//StockType_id")[0].text
            Qty = sku.xpath(".//Qty")[0].text
            products.append([Sku_id, int(float(StockType_id)), int(float(Qty))])

        return products

    def CreateClient(self, data):
        address = ""
        if "RecStreet" not in data.keys():
            data["RecStreet"] = u"Ленина"
            data["RecHouse"] = "1"
            data["RecFlat"] = "1"
        xml = u"""<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <Client_CreateUpdate xmlns="http://cowms.ru/outbound">
                      <User>[User]</User>
                      <Pass>[Pass]</Pass>
                      <ClientCode>{0}</ClientCode>
                      <LastName>{1}</LastName>
                      <Phone>{2}</Phone>
                      <Street>{3}</Street>
                      <House>{4}</House>
                      <App>{5}</App>
                    </Client_CreateUpdate>
                  </soap:Body>
                </soap:Envelope>""".format(
                time.strftime("%y%m%d%H%M%S") + str(datetime.datetime.now().microsecond),
                data["RecipientName"],
                data["RecipientPhone"], data["RecStreet"],data["RecHouse"],data["RecFlat"]
                )

        res = self.SendSoapRequest("http://cowms.ru/outbound/Client_CreateUpdate", xml)
        if res == -1:
            return -1
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/outbound',
        }
        try:
            id = root.findall('./soap:Body'
            '/a:Client_CreateUpdateResponse'
            '/a:Client_CreateUpdateResult'
            , namespaces,)[0].text
            return id
        except:
            return -1

    def CreateContractor(self, data):
        address = ""
        if "RecStreet" in data.keys():
            address = u"{0} {1} {2}".format(data["RecStreet"],data["RecHouse"],data["RecFlat"])
        xml = u"""<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <Client_CreateUpdate xmlns="http://cowms.ru/outbound">
                      <User>[User]</User>
                      <Pass>[Pass]</Pass>
                      <ClientCode>{0}</ClientCode>
                      <LastName>{1}</LastName>
                      <Phone>{2}</Phone>
                      <AddressString>{3}</AddressString>
                    </Client_CreateUpdate>
                  </soap:Body>
                </soap:Envelope>""".format(
                str(datetime.datetime.now().microsecond),
                data["RecipientName"],
                data["RecipientPhone"], address
                )

        res = self.SendSoapRequest("http://cowms.ru/outbound/Client_CreateUpdate", xml)
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/outbound',
        }
        try:
            id = root.findall('./soap:Body'
            '/a:Client_CreateUpdateResponse'
            '/a:Client_CreateUpdateResult'
            , namespaces,)[0].text
            return id
        except:
            return -1

    def CreateOrderApi(self, data):
        xml = """<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <ClientOrder_CreateUpdate xmlns="http://cowms.ru/outbound">
                    <User>[User]</User>
                    <Pass>[Pass]</Pass>
                    {0}
                    </ClientOrder_CreateUpdate>
                  </soap:Body>
                </soap:Envelope>"""

        clientId = self.CreateClient(data)
        if clientId == -1:
            return [-1, -1]

        rootElem = etree.Element('root')
        doc = etree.ElementTree(rootElem)
        ClientOrderCode = etree.SubElement(rootElem, 'ClientOrderCode')
        ClientOrderCode.text = data["Number"]
##        ClientOrderCode.text = data["Number"] + "3"
##        ClientOrderCode.text = "12345"
        OrderDate = etree.SubElement(rootElem, 'OrderDate')
        OrderDate.text = str(date.today())
        ExpectedDeliveryDate = etree.SubElement(rootElem, 'ExpectedDeliveryDate')
        ExpectedDeliveryDate.text = str(date.today())
        ExpectedShipmentDate = etree.SubElement(rootElem, 'ExpectedShipmentDate')
        ExpectedShipmentDate.text = str(date.today())
        OutcomeWarehouse_id = etree.SubElement(rootElem, 'OutcomeWarehouse_id')
        OutcomeWarehouse_id.text = "48"
        Owner_id = etree.SubElement(rootElem, 'Owner_id')
        Owner_id.text = "54408"
        Client_id = etree.SubElement(rootElem, 'Client_id')
        Client_id.text = clientId
        Contractor_id = etree.SubElement(rootElem, 'Contractor_id')
        Contractor_id.text = "0"
        Agent_id = etree.SubElement(rootElem, 'Agent_id')
        Agent_id.text = "0"
        DeliveryType_id = etree.SubElement(rootElem, 'DeliveryType_id')
        DeliveryType_id.text = "13"
        DeliveryMode_id = etree.SubElement(rootElem, 'DeliveryMode_id')
        if data["Tariff"] == "SS":
            DeliveryMode_id.text = "0"
        elif data["Tariff"] == "SD":
            DeliveryMode_id.text = "1"
        elif data["Tariff"] == "DS":
            DeliveryMode_id.text = "2"
        else:
            DeliveryMode_id.text = "3"

        DeliveryType_Parameters = etree.SubElement(rootElem, 'DeliveryType_Parameters')

        if data["Tariff"][-1] == "S":
            DeliveryType_Parameters1 = etree.SubElement(DeliveryType_Parameters, 'DeliveryType_Parameters')
            ParameterName = etree.SubElement(DeliveryType_Parameters1, 'ParameterName')
            ParameterName.text = "CdekPVZCode"
            if "PvzCode" in data.keys():
                pvzCode = data["PvzCode"]
                data["RecCityName"] = self.GetPvzCityCode(pvzCode)
            else:
                pvzCode = str(self.GetPVZCode(data["RecCityName"], data["RecPointName"]))
            ParameterValue = etree.SubElement(DeliveryType_Parameters1, 'ParameterValue')
            ParameterValue.text = pvzCode

        if data["RecCityName"][0].isdigit() == True:
            cityCode = data["RecCityName"]
        else:
            cityCode = str(self.GetCityCode(data["RecCityName"]))
        DeliveryType_Parameters1 = etree.SubElement(DeliveryType_Parameters, 'DeliveryType_Parameters')
        ParameterName = etree.SubElement(DeliveryType_Parameters1, 'ParameterName')
        ParameterName.text = "CdekCityCode"
        ParameterValue = etree.SubElement(DeliveryType_Parameters1, 'ParameterValue')
        ParameterValue.text = cityCode

        DeliveryType_Parameters1 = etree.SubElement(DeliveryType_Parameters, 'DeliveryType_Parameters')
        ParameterName = etree.SubElement(DeliveryType_Parameters1, 'ParameterName')
        ParameterName.text = "CdekTariff"
        if "TariffCode" in data.keys():
             tariffCode = data["TariffCode"]
        else:
             tariffCode, deliveryCost = self.GetTariffId(cityCode, data["Tariff"], data["SendCityCode"] )
             if tariffCode == -1:
                return [-1, -1]
        ParameterValue = etree.SubElement(DeliveryType_Parameters1, 'ParameterValue')
        ParameterValue.text = str(tariffCode)

        DeliveryPayment = etree.SubElement(rootElem, 'DeliveryPayment')
        DeliveryPayment.text = data["DeliveryRecipientCost"]
        PayType = etree.SubElement(rootElem, 'PayType')
        PayType.text = "0"

        declaredAmount = 0
        sumToPay = 0

        ClientOrder_Positions = etree.SubElement(rootElem, 'ClientOrder_Positions')
        for i in range(0, len(data["Items"])):
            item = data["Items"][i]
            ClientOrder_Positions_1 = etree.SubElement(ClientOrder_Positions, 'ClientOrder_Positions')
            ClientOrder_PositionCode = etree.SubElement(ClientOrder_Positions_1, 'ClientOrder_PositionCode')
            ClientOrder_PositionCode.text = data["Number"] + str(i)
            Sku_id = etree.SubElement(ClientOrder_Positions_1, 'Sku_id')
            Sku_id.text = item["Name"]
            Unit_id = etree.SubElement(ClientOrder_Positions_1, 'Unit_id')
            Unit_id.text = "784"
            StockType_id = etree.SubElement(ClientOrder_Positions_1, 'StockType_id')
            StockType_id.text = "0"
            Price = etree.SubElement(ClientOrder_Positions_1, 'Price')
            Price.text = item["Cost"]
            Payment = etree.SubElement(ClientOrder_Positions_1, 'Payment')
            Payment.text = item["Payment"]
            VAT = etree.SubElement(ClientOrder_Positions_1, 'VAT')
            if self.login == "8a4a7f3c6834f7378154db9c1380e968":
                VAT.text = "0"
            else:
                VAT.text = "18"
            Qty = etree.SubElement(ClientOrder_Positions_1, 'Qty')
            Qty.text = item["Amount"]
            Declared = etree.SubElement(ClientOrder_Positions_1, 'Declared')
            Declared.text = item["Cost"]
            declaredAmount += int(item["Amount"]) * float(item["Cost"])
            sumToPay += int(item["Amount"]) * float(item["Payment"])

        DeclaredAmount = etree.SubElement(rootElem, 'DeclaredAmount')
        DeclaredAmount.text = str(declaredAmount)
        SumToPay = etree.SubElement(rootElem, 'SumToPay')
        SumToPay.text = str(sumToPay)
        Weight = etree.SubElement(rootElem, 'Weight')
        Weight.text = "0.3"
        Width = etree.SubElement(rootElem, 'Width')
        Width.text = "10"
        Height = etree.SubElement(rootElem, 'Height')
        Height.text = "10"
        Length = etree.SubElement(rootElem, 'Length')
        Length.text = "10"

        xmlPart = etree.tostring(doc).replace("<root>","").replace("</root>","")
        xml = xml.replace("{0}", xmlPart)
        res = self.SendSoapRequest("http://cowms.ru/outbound/ClientOrder_CreateUpdate", xml)
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/outbound',
        }
        try:
            id = root.findall('./soap:Body'
            '/a:ClientOrder_CreateUpdateResponse'
            '/a:RPO_Created'
            , namespaces,)[0].text
            return [id, deliveryCost]
        except:
            return [-1, -1]

    def UpdateProduct(self, data):
        xml = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SKU_CreateUpdate xmlns="http://cowms.ru/service">
      <User>[User]</User>
      <Pass>[Pass]</Pass>
      {0}
    </SKU_CreateUpdate>
  </soap:Body>
</soap:Envelope>"""
        rootElem = etree.Element('root')
        doc = etree.ElementTree(rootElem)
        Code = etree.SubElement(rootElem, 'Code')
        Code.text = data["Article"]
        Article = etree.SubElement(rootElem, 'Article')
        Article.text = data["Article"]
        FullName = etree.SubElement(rootElem, 'FullName')
        FullName.text = data["Name"]
        BaseUnit_id = etree.SubElement(rootElem, 'BaseUnit_id')
        BaseUnit_id.text = "784"
        Owner_id = etree.SubElement(rootElem, 'Owner_id')
        Owner_id.text = "54408"
        PackUnit_id = etree.SubElement(rootElem, 'PackUnit_id')
        PackUnit_id.text = "0"
        PackKoeff = etree.SubElement(rootElem, 'PackKoeff')
        PackKoeff.text = "0"
        BoxUnit_id = etree.SubElement(rootElem, 'BoxUnit_id')
        BoxUnit_id.text = "0"
        BoxKoeff = etree.SubElement(rootElem, 'BoxKoeff')
        BoxKoeff.text = "0"
        Width = etree.SubElement(rootElem, 'Width')
        if "Width" in data.keys():
            Width.text = data["Width"]
        else:
            Width.text = "10"
        Height = etree.SubElement(rootElem, 'Height')
        if "Height" in data.keys():
            Height.text = data["Height"]
        else:
            Height.text = "10"
        Length = etree.SubElement(rootElem, 'Length')
        if "Length" in data.keys():
            Length.text = data["Length"]
        else:
            Length.text = "10"
        Weight = etree.SubElement(rootElem, 'Weight')
        if "Weight" in data.keys():
            Weight.text = data["Weight"]
        else:
            Weight.text = "0.3"

        if "Barcode" in data.keys():
            Barcode = etree.SubElement(rootElem, 'Barcode')
            Barcode.text = data["Barcode"]
        Volume = etree.SubElement(rootElem, 'Volume')
        Volume.text = "0"
        Life = etree.SubElement(rootElem, 'Life')
        Life.text = "0"
        IsMan = etree.SubElement(rootElem, 'IsMan')
        IsMan.text = "0"
        IsWoman = etree.SubElement(rootElem, 'IsWoman')
        IsWoman.text = "0"
        IsChild = etree.SubElement(rootElem, 'IsChild')
        IsChild.text = "0"

        xmlPart = etree.tostring(doc).replace("<root>","").replace("</root>","")
        xml = xml.replace("{0}", xmlPart)
        res = self.SendSoapRequest("http://cowms.ru/service/SKU_CreateUpdate", xml)
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/service',
        }
        try:
            sku = root.findall('./soap:Body'
            '/a:SKU_CreateUpdateResponse'
            '/a:SKU_CreateUpdateResult'
            , namespaces,)[0].text
            return sku
        except:
            return -1

    def GetPosting(self, incomeRequestId, invoice):
        xml = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
 <soap:Body>
 <Income_GetFact xmlns="http://cowms.ru/inbound">
 <User>[User]</User>
 <Pass>[Pass]</Pass>
 <IncomeRequestCode>{1}</IncomeRequestCode>
 <IncomeRequest_id>{0}</IncomeRequest_id>
 </Income_GetFact>
 </soap:Body>
</soap:Envelope>""".format(incomeRequestId, invoice)
        res = self.SendSoapRequest("http://cowms.ru/inbound/Income_GetFact", xml)
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/inbound',
        }
        try:
            positions = root.findall('./soap:Body'
            '/a:Income_GetFactResponse'
            '/a:Income_Positions'
            '/a:Income_Positions'
            , namespaces,)
            items = []
            for position in positions:
                Sku_id = position.getchildren()[1].text
                Qty = position.getchildren()[4].text
                items.append([Sku_id, Qty])
            return items
        except:
            return -1

    def GetDeliveryMode(self):
        xml = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
 <soap:Body>
 <DeliveryMode_GetList xmlns="http://cowms.ru/service">
 <User>[User]</User>
 <Pass>[Pass]</Pass>
 </DeliveryMode_GetList>
 </soap:Body>
</soap:Envelope>"""
        res = self.SendSoapRequest("http://cowms.ru/service/DeliveryMode_GetList", xml)
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/service',
        }
        try:
            positions = root.findall('./soap:Body'
            '/a:Income_GetFactResponse'
            '/a:Income_Positions'
            '/a:Income_Positions'
            , namespaces,)
            items = []
            for position in positions:
                Sku_id = position.getchildren()[1].text
                Qty = position.getchildren()[4].text
                items.append([Sku_id, Qty])
            return items
        except:
            return -1

    def CreatePostingApi(self, items, invoice = "0"):
        xml = """<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <IncomeRequest_CreateUpdate xmlns="http://cowms.ru/inbound">
                    <User>[User]</User>
                    <Pass>[Pass]</Pass>
                    {0}
                    </IncomeRequest_CreateUpdate>
                  </soap:Body>
                </soap:Envelope>"""

        rootElem = etree.Element('root')
        doc = etree.ElementTree(rootElem)
        IncomeRequestCode = etree.SubElement(rootElem, 'IncomeRequestCode')
        IncomeRequestCode.text = invoice
        IncomeWarehouse_id = etree.SubElement(rootElem, 'IncomeWarehouse_id')
        IncomeWarehouse_id.text = "48"
        Owner_id = etree.SubElement(rootElem, 'Owner_id')
        Owner_id.text = "54408"
        Supplier_id = etree.SubElement(rootElem, 'Supplier_id')
        Supplier_id.text = "54408"
        ReturnClientOrder_id = etree.SubElement(rootElem, 'ReturnClientOrder_id')
        ReturnClientOrder_id.text = "0"
        Amount = etree.SubElement(rootElem, 'Amount')
        Amount.text = "0"
        IncomeRequest_Positions = etree.SubElement(rootElem, 'IncomeRequest_Positions')
        for i in range(0,len(items)):
            item = items[i]
            IncomeRequest_Positions1 = etree.SubElement(IncomeRequest_Positions, 'IncomeRequest_Positions')
            IncomeRequest_PositionCode = etree.SubElement(IncomeRequest_Positions1, 'IncomeRequest_PositionCode')
            IncomeRequest_PositionCode.text = str(i)
            Sku_id = etree.SubElement(IncomeRequest_Positions1, 'Sku_id')
            Sku_id.text = item["SkuId"]
            Unit_id = etree.SubElement(IncomeRequest_Positions1, 'Unit_id')
            Unit_id.text = "784"
            StockType_id = etree.SubElement(IncomeRequest_Positions1, 'StockType_id')
            StockType_id.text = "0"
            Price = etree.SubElement(IncomeRequest_Positions1, 'Price')
            Price.text = "0"
            VAT = etree.SubElement(IncomeRequest_Positions1, 'VAT')
            VAT.text = "0"
            Qty = etree.SubElement(IncomeRequest_Positions1, 'Qty')
            Qty.text = item["Amount"]

        xmlPart = etree.tostring(doc).replace("<root>","").replace("</root>","")
        xml = xml.replace("{0}", xmlPart)
        res = self.SendSoapRequest("http://cowms.ru/inbound/IncomeRequest_CreateUpdate", xml)
        root = ElementTree.fromstring(res)
        namespaces = {
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'a': 'http://cowms.ru/inbound',
        }
        try:
            id = root.findall('./soap:Body'
            '/a:IncomeRequest_CreateUpdateResponse'
            '/a:IncomeRequest_CreateUpdateResult'
            , namespaces,)[0].text
            return id
        except:
            return -1

    def SendRequest(self, url, request):
        f = {}
        f["xml_request"] = request

        xml = requests.post(url, data=f, verify=False)
        if url == self.printFormUrl:
            return xml._content
        try:
            return xml._content.split("?>")[1]
        except:
            return xml._content

    def SendSoapRequest(self, SOAPAction, xml):
        if SOAPAction in ["http://cowms.ru/reports/Stock_GetList"]:
            asmxUrl = "http://cdekff.ddns.net:40100/reports.asmx"
        if SOAPAction in ["http://cowms.ru/outbound/Client_CreateUpdate", "http://cowms.ru/outbound/ClientOrder_CreateUpdate"]:
            asmxUrl = "http://92.242.40.125:40100/outbound.asmx"
##            asmxUrl = "http://cdekff.ddns.net:40100/outbound.asmx"
        if SOAPAction in ["http://cowms.ru/service/SKU_CreateUpdate", "http://cowms.ru/service/DeliveryMode_GetList"]:
            asmxUrl = "http://cdekff.ddns.net:40100/service.asmx"
        if SOAPAction in ["http://cowms.ru/inbound/Income_GetFact", "http://cowms.ru/inbound/IncomeRequest_CreateUpdate"]:
            asmxUrl = "http://cdekff.ddns.net:40100/inbound.asmx"
##        reportsUrl = "http://cdekff.ddns.net:40100/reports.asmx"
##        inboundUrl = "http://cdekff.ddns.net:40100/service.asmx"
##        outboundUrl = "http://cdekff.ddns.net:40100/outbound.asmx"
##        deliverycalcUrl = "http://cdekff.ddns.net:40100/service.asmx"
        xml = xml.replace("[User]", self.apiUser).replace("[Pass]", self.GetPass()).encode('utf-8')
##        print xml
        headers = {
         "Content-Type": "text/xml; charset=utf-8",
         "Content-Length": str(len(xml)),
         "SOAPAction": SOAPAction}
        try:
            res = requests.post(asmxUrl, data=xml, headers=headers)
            return res._content
        except:
            return -1

