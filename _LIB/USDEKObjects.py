﻿from UBaseObjects import *

class USDEKObjects(UBaseObjects):


    ################### фулфилмент ##################

    login = ["id", "id_sc_field_login"]
    password = ["id", "id_sc_field_pswd"]
    loginButton = ["id", "sub_form_b"]

    userProfileMenu = ["id", "item_42"]

    #страница заявки
    date = ["id", "id_sc_field_orderdate"]
    sender = ["id", "id_sc_field_sender"]
    receiver = ["id", "id_sc_field_name"]
    phones = ["id", "id_sc_field_phones"]
    address = ["id", "id_sc_field_addressstring"]
    address = ["id", "id_sc_field_addressstring"]
    city = ["id", "id_ac_city"]
    tariffSelect = ["id", "id_sc_field_deliverytariff_id"]
    rqTypeSelect = ["id", "id_sc_field_deliverytype_id"]
    warehouseSelect = ["id", "id_sc_field_warehouse_id"]
    OkButton = ["id", "sub_form_b"]
    comment = ["id", "id_sc_field_comment"]

    #список заявок
    lastPageButton = ["id", "last_bot"]
    nextPageButton = ["id", "id_img_forward_bot"]
    rqNumberLink = ["xpath" , "//a[contains(@onmouseover, '" + u"перейти к заказу" + "')]"]
    searchEdit = ["name", "sc_clone_nmgp_arg_fast_search"]
    searchButton = ["id", "SC_fast_search_submit_top"]
    clearSearchEditButton = ["id", "SC_fast_search_close_top"]
##    rqCheckbox = ["id", "NM_ck_run100"]
    rqCheckbox = ["id", "NM_ck_run1"]
    reservButton = ["id", "sc_SC_btn_1_top"]
    confirmButton = ["id", "sc_SC_btn_0_top"]
    executeButton = ["id", "sc_Execute_top"]
    OKButton = ["name", "nmgp_bok"]
    sortByDokumentIdButton = ["xpath", "//a[contains(@href,'Document_id')]"]

    #список товаров
    addNewItemButton = ["id", "sc_b_new_b"]
    insertRowButton = ["xpath" , "//img[contains(@id, 'id_img_sc_ins_line')]"]
    itemNameEdit = ["xpath" , "//input[contains(@id, 'id_ac_sku_id')]"]
    itemSkuSelect = ["xpath" , "//select[contains(@id, 'id_sc_field_skuunit_id')]"]
    itemQtyEdit = ["xpath" , "//input[contains(@id, 'id_sc_field_qty')]"]
    backButton = ["id", "sc_b_sai_t"]
    itemVariant = ["xpath" , "//div[@class = 'ac_results']/ul/li[contains(@class, 'ac_')]"]
    waitElement = ["id", "id_div_process"]

    #список приемок
    postingIdLink = ["xpath" , "//a[contains(@onmouseover, '" + u"состав приемки" + "')]"]
    itemSku = ["xpath" , "//span[contains(@id, 'id_sc_field_sku_id')]"]
    itemQty = ["xpath" , "//span[contains(@id, 'id_sc_field_qty')]"]

    #отчет по остаткам
    balance_findButton = ["id" , "sc_b_pesq_bot"]
    balance_toolbar = ["xpath" , "//span[contains(@class, 'css_toolbar_obj')]"]
    balance_itemSkuCell = ["xpath", "//table[@class = 'scGridTabela']//td[@class = 'scGridBlock']//td[3]"]
    balance_itemQtyCell = ["xpath", "//span[contains(@id, 'id_sc_field_qty')]"]

    #заявка на приемку
    posting_purchaseDate = ["id", "id_sc_field_purchasedate"]
    posting_planDate = ["id", "id_sc_field_planincomedate"]
    posting_supplier = ["id", "id_ac_supplier_id"]
    posting_postingIdLink = ["xpath" , "//a[contains(@onmouseover, '" + u"перейти к составу документа" + "')]"]

    #справочник товаров
##    directory_category = ["id", "id_sc_field_skucategory_id"]
##    directory_unit = ["id", "id_ac_baseunit_id"]
##    directory_vendorCode = ["id", "id_sc_field_article"]
##    directory_name = ["id", "id_sc_field_fullname"]
##    directory_description = ["id", "id_sc_field_description"]
##    directory_unitOption = ["xpath", "//div[@class = 'ac_results']//li"]
##    directory_addNewItem = ["id", "sc_CreateNew_top"]
    directory_code = ["xpath", "//span[contains(@id, 'id_sc_field_tid')]"]
    directory_article = ["xpath", "//a[contains(@href, 'grid_dbo_WMS_SkuUnits_portal')]"]

    #################### ЛК ####################
    #вход
    lKlogin = ["id", "loginform-login"]
    lKpassword = ["id", "loginform-password"]
    lKloginButton = ["name" , "login-button"]

    #список реестров
    registries_searchButton = GetElementByText("button", u"Поиск")
    registries_paidFilter = ["id", "registrymodel-paid"]
    registries_dateFrom = ["id", "registrymodel-datefrom"]
    registries_previousMonth = GetElementByText("span", u"Пред. месяц")
    registries_firstDay = ["xpath", "//a[contains(@class, 'ui-state-default') and text() = '1']"]
    registries_registryRow = ["xpath", "//table[contains(@class, 'registry-table')]//tr"]
    registries_nextPage = ["xpath" , "//li[@class = 'next']/a"]
    invoices_rows = ["xpath" , "//table[contains(@class, 'invoice-table')]//tr"]
    invoices_page = ["xpath" , "//li/a[contains(@href, 'get-invoice-list')]"]
    invoices_back = ["id" , "registry-back-to-list"]

    #создание заказа
    #шаг 1
    createInvoice_deliveryType = ["id", "dOrderType"]
    createInvoice_payerType = ["id", "dPayer"]
    createInvoice_payer = ["id", "payer-type-order"]
    createInvoice_docNumber = ["id", "payer-order-number"]
    createInvoice_senderCity = ["id", "dFromCity"]
    createInvoice_recipientCity = ["id", "dToCity"]
    createInvoice_addPlace = GetElementByText(u"a", u"Добавить ещё место")
    createInvoice_removePlace = ["xpath" , "//a[contains(@class, 'glyphicon-remove-circle remove')]"]
    createInvoice_calculateButton = ["id", "tarif-calc"]
    createInvoice_sendCitiesList = ["id", "ui-id-1"]
    createInvoice_recCitiesList = ["id", "ui-id-2"]
    createInvoice_selectOption = ["xpath" , "//div[contains(@id, 'ui-id')]"]
    tariffRow = ["xpath", "//tbody[@id = 'tarif-list']/tr"]
    tariffSum = ["class", "calc-content-mid2"]
    createInvoice_additionalServices = GetElementByText(u"a", u"Дополнительные услуги")
    insuranceCh = ["id", "binsurance"]
    insuranceEdit = ["id", "insurance"]
    totalDeliveryCosts = ["id", "total-amount"]

    #шаг 2
    createInvoice_courierDate =["id", "deliver-date2"]
    createInvoice_calendar =["class", "ui-datepicker-calendar"]
    createInvoice_timeFrom =["id", "CourierTimeFrom"]
    createInvoice_timeTo =["id", "CourierTimeTo"]
    createInvoice_courierLunchStart =["id", "CourierLunchStart"]
    createInvoice_senderFio = ["id", "dSenderName"]
    createInvoice_senderStreet = ["id", "dSenderStreet"]
    createInvoice_senderHouse = ["id", "dSenderHouse"]
    createInvoice_senderFlat = ["id", "dSenderRoom"]
    createInvoice_Phone = ["xpath", "//input[contains(@class, 'form-control phone-main')]"]
    createInvoice_senderCompany = ["id", "dSenderCompany"]
    createInvoice_senderComment = ["id", "dSenderNotes"]

    #шаг 3
    createInvoice_recipientFio = ["id", "dReceiverName"]
    createInvoice_recipientStreet = ["id", "dReceiverStreet"]
    createInvoice_recipientHouse = ["id", "dReceiverHouse"]
    createInvoice_recipientFlat = ["id", "dReceiverRoom"]
    createInvoice_recipientPointSelect = ["id", "dPvz"]
    createInvoice_recipientPhone = ["id", "dReceiverPhone"]
    createInvoice_recipientComment = ["id", "dReceiverNotes"]
    createInvoice_recipientChoosePoint = GetButtonByText(u"Выбрать")

    #шаг 4
    createInvoice_imOrderNum = ["id", "dshop_number_dep"]
    createInvoice_ttn = ["id", "dshop_number_act"]
    createInvoice_additionalPayment = ["id", "dshop_additional_collection"]
    createInvoice_trueSeller = ["id", "dshop_true_seller"]
    createInvoice_addItem = ["xpath", "//button[@class = 'btn btn-default btn-add']"]
    createInvoice_previewButton = ["id", "create-order-form-confirm"]
    createInvoice_additionalPaymentVatRate = ["id", "dshop_additional_collection_vat_rate"]


    #завершение
    createInvoice_createOrderButton = ["id", "create-order-form-send"]
    createInvoice_invoiceInfo = ["class", "text-success"]
    createInvoice_pageBottom = ["class", "pull-left"]














