﻿from UBaseObjects import *

class URussianpostcalcObjects(UBaseObjects):

    print7p = ["name", "print7p"]
    print112 = ["name", "print112"]
    from_fio = ["name", "from_fio"]
    from_index = ["name", "from_index"]
    from_addr = ["name", "from_addr"]
    from_selectOption = ["xpath", "//div[@class = 'ac_results'][2]/ul/li"]
    to_selectOption = ["xpath", "//div[@class = 'ac_results'][4]/ul/li"]
    p_seriya = ["name", "p_seriya"]
    p_nomer = ["name", "p_nomer"]
    p_vidal = ["name", "p_vidal"]
    p_vidan = ["name", "p_vidan"]
    to_fio = ["name", "to_fio"]
    to_tel = ["name", "to_tel"]
    to_index = ["name", "to_index"]
    to_addr = ["name", "to_addr"]
    ob_cennost_rub = ["name", "ob_cennost_rub"]
    nalogka_rub = ["name", "nalogka_rub"]
    calcbutton = ["class", "calcbutton"]
    downloadLink = GetLinkByText(u"скачать PDF")

