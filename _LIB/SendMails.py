﻿import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders
from email.mime.application import MIMEApplication
from email.MIMEImage import MIMEImage
import os
from email.header import Header
import poplib
import imaplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.message import MIMEMessage
import commonLibrary

robotParams = {"FromAddress" : u"feedback@knowall.ru.com", "From" : u"Робот","Password" : "Va17yAjvdikXGs4WxJey"}
knowallParams = {"FromAddress" : u"feedback@knowall.ru.com", "From" : u"Компания \"Новалл\"","Password" : "Va17yAjvdikXGs4WxJey"}

templateMessage = """<html>
      <head></head>
      <body>
        <p>{message}</p>
      </body>
    </html>"""

class SendMails:

    SmtpServer = None
    ImapServer = None
    logging = None

    def __init__(self, logFilePath = None):
        self.SmtpServer = "smtp.mail.ru"
        self.ImapServer = "imap.mail.ru"
        if logFilePath != None:
            self.logging = log.Log(logFilePath)

    def GetSenderData(self, siteUrl):
        data = {}

        if siteUrl == "http://knowall.ru.com":
            data["images"] = ["mails/novall-1423403145.jpg"]
            data["params"] = {"FromAddress" : u"feedback@knowall.ru.com", "Password" : "Va17yAjvdikXGs4WxJey"}
        if siteUrl in ["http://otpugiwatel.com"]:
            data["images"] = ["mails/otpugiwatel-1419615610.jpg"]
            data["params"] = {"FromAddress" : u"feedback@otpugiwatel.com", "Password" : "Tabulfsam777"}
        if siteUrl in ["http://otpugivatel.com"]:
            data["images"] = ["mails/otpugiwatel-1419615610.jpg"]
            data["params"] = {"FromAddress" : u"feedback@otpugivatel.com", "Password" : "Tabulfsam777"}			
        if siteUrl == "http://safetus.ru":
            data["images"] = ["mails/safetus.png"]
            data["params"] = {"FromAddress" : u"feedback@safetus.ru", "Password" : "Tabulfsam777"}
        if siteUrl == "http://amazing-things.ru":
            data["images"] = ["mails/amazingthings-1428236814.jpg"]
            data["params"] = {"FromAddress" : u"feedback@amazing-things.ru", "Password" : "Tabulfsam777"}
        if siteUrl == "http://saltlamp.su":
            data["images"] = ["mails/saltlamp.jpg"]
            data["params"] = {"FromAddress" : u"feedback@saltlamp.su", "Password" : "Tabulfsam777"}
        if siteUrl == "http://insect-killers.ru":
            data["images"] = ["mails/ic-logo.jpg"]
            data["params"] = {"FromAddress" : u"feedback@insect-killers.ru", "Password" : "Tabulfsam777"}
        if siteUrl == "http://sushilki.ru.com":
            data["images"] = ["mails/sushilki.png"]
            data["params"] = {"FromAddress" : u"feedback@sushilki.ru.com", "Password" : "Tabulfsam777"}
        if siteUrl == "http://glushilki.ru.com":
            data["images"] = ["mails/gl_logo.png"]
            data["params"] = {"FromAddress" : u"feedback@glushilki.ru.com", "Password" : "Tabulfsam777"}
        if siteUrl == "http://mini-camera.ru.com":
            data["images"] = ["mails/minicamera.png"]
            data["params"] = {"FromAddress" : u"feedback@mini-camera.ru.com", "Password" : "Tabulfsam777"}
        if siteUrl == "http://otpugivateli-grizunov.ru":
            data["images"] = ["mails/ot-grizunov.jpg"]
            data["params"] = {"FromAddress" : u"feedback@otpugivateli-grizunov.ru", "Password" : "Tabulfsam777"}
        if siteUrl == "http://otpugivateli-krotov.ru":
            data["images"] = ["mails/ot-krotov.png"]
            data["params"] = {"FromAddress" : u"feedback@otpugivateli-krotov.ru", "Password" : "Tabulfsam777"}
        if siteUrl == "http://otpugivateli-ptic.ru":
            data["images"] = ["mails/ot-ptic.png"]
            data["params"] = {"FromAddress" : u"feedback@otpugivateli-ptic.ru",  "Password" : "DVkWpTNNz7Ufm3EkRxGD"}
        if siteUrl == "http://incubators.shop":
            data["images"] = ["mails/incub.png"]
            data["params"] = {"FromAddress" : u"feedback@incubators.shop",  "Password" : "Tabulfsam555"}
        if siteUrl == "http://otpugivateli-sobak.ru":
            data["images"] = ["mails/ot-sobak.jpg"]
            data["params"] = {"FromAddress" : u"feedback@otpugivateli-sobak.ru",  "Password" : "Tabulfsam777"}
        if siteUrl == "http://usiliteli-svyazi.ru":
            data["images"] = ["mails/usiliteli.png"]
            data["params"] = {"FromAddress" : u"feedback@usiliteli-svyazi.ru",  "Password" : "Tabulfsam777"}
        if siteUrl == "http://otpugivatel.spb.ru":
            data["images"] = ["mails/otp-spb.png"]
            data["params"] = {"FromAddress" : u"feedback@otpugivatel.spb.ru",  "Password" : "Tabulfsam777"}
        if siteUrl == "http://gemlux-shop.ru":
            data["images"] = ["mails/gemlux.jpg"]
            data["params"] = {"FromAddress" : u"feedback@gemlux-shop.ru",  "Password" : "1qaz@WSX"}

        data["params"]["From"] = u"Интернет-магазин «{0}»".format(commonLibrary.GetCompanyName(siteUrl))
        data["shop_name"] = u"«{0}»".format(commonLibrary.GetCompanyName(siteUrl))
        return data



    def SendInfoMail(self, subject, message, files = None):
        text = templateMessage
        message = message.replace("\n","<br/>")
        text = text.replace("{message}", message)

        try:
            return self.SendMail(robotParams, "feedback@knowall.ru.com", subject, text, files)
        except:
            return False

    def SendMailToPartner(self, to, subject, message, files = None,cc = []):
        text = templateMessage
        message = message.replace("\n","<br/>")
        text = text.replace("{message}", message)

        try:
            return self.SendMail(knowallParams, to, subject, text, files, [], cc)
        except:
            return False

    def SendMailToCustomer(self, template, data):
        orderName = data["order_name"].encode("cp1251")

        if template == "in_transit":
            mail = open("mails/in_transit.html","r").read()
            mail = mail.replace("{followup}", data["followup"])
            mail = mail.replace("{tracking_number}", str(data["tracking_number"]))
            subject = "Сообщение о Вашем заказе " + orderName

        if template == "shipped":
            mail = open("mails/shipped.html","r").read()
            mail = mail.replace("{description}", data["description"])
            subject = "Сообщение о Вашем заказе " + orderName

        if template == "review":
            mail = open("mails/review.html","r").read()
            subject = "Получите 100 рублей на телефон за 5 минут!"
            mail = mail.replace("{item_link}", data["itemUrl"].encode("utf-8"))
            mail = mail.replace("{date}", data["date"])
            mail = mail.replace("{item_name}", data["item_name"].encode("utf-8"))

        if template == "coupon":
            mail = open("mails/coupon.html","r").read()
            subject = "Купон на скидку"
            mail = mail.replace("{code}", data["code"].encode("utf-8"))
            mail = mail.replace("{percent}", data["reduction_percent"])
            mail = mail.replace("{dateEnd}", data["dateEnd"])
            mail = mail.replace("{pageUrl}", data["pageUrl"].encode("utf-8"))
            mail = mail.replace("{item_name}", data["item_name"].encode("utf-8"))

        if template == "coupon_activation":
            mail = open("mails/coupon_activation.html","r").read()
            mail = mail.replace("{amount}", data["reduction_amount"])
            mail = mail.replace("{code}", data["code"].encode("utf-8"))
            mail = mail.replace("{dateEnd}", data["dateEnd"])
            subject = "Активация купона"

        if template == "coupon_reminder":
            mail = open("mails/coupon_reminder.html","r").read()
            mail = mail.replace("{code}", data["code"].encode("utf-8"))
            mail = mail.replace("{dateTo}", data["dateTo"])
            if data["reduction_percent"] != 0:
                mail = mail.replace("{value}", data["reduction_percent"])
                mail = mail.replace("{unit}", "%")
            else:
                mail = mail.replace("{value}", data["reduction_amount"])
                mail = mail.replace("{unit}", " руб.")
            mail = mail.replace("{dateTo}", data["dateTo"])
            mail = mail.replace("{categories}", data["categories"].encode("utf-8"))

            subject = "Срок действия купона истекает"


        mail = mail.replace("{shop_name}", data["shop_name"].encode("utf-8"))
        mail = mail.replace("{shop_url}", str(data["shop_url"]))
        mail = mail.replace("{firstname}", data["firstname"].encode("utf-8"))
        mail = mail.replace("{lastname}", data["lastname"].encode("utf-8"))
        mail = mail.replace("{order_name}", orderName)
        shopUrl = str(data["shop_url"])
        mail = mail.replace("{my_account_url}", shopUrl + "/my-account")
        mail = mail.replace("{history_url}", shopUrl + "/order-history")
        mail = mail.replace("{guest_tracking_url}", shopUrl + "/guest-tracking?id_order=" + orderName)

        try:
            return self.SendMail(data["params"], data["email"], subject, mail, None, data["images"])
        except:
            return False

    def SendMail(self, params, to, subject, htmlText, files = None, images = [], cc = []):
        msg = MIMEMultipart()

        msg['Subject'] =  "%s" % Header(subject, 'utf-8')
        msg['From'] = "\"%s\" <%s>" % (Header(params["From"], 'utf-8'), params["FromAddress"])
        msg['CC'] = ", ".join(cc)

        text = MIMEText(htmlText, 'html','utf-8')

        msg.attach(text)

        for f in files or []:
            part = MIMEBase('application', "octet-stream")
            part.set_payload( open(f,"rb").read() )
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"'
                           % os.path.basename(f))
            msg.attach(part)

        i = 1
        for pic in images:
            fp = open(pic, 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            msgImage.add_header('Content-ID', "<image%s>" % i)
            msg.attach(msgImage)
            i += 1

        s = smtplib.SMTP_SSL()
        try:
            s.connect(self.SmtpServer, 465)
            s.login(params["FromAddress"], params["Password"])
        except Exception, e:
           return e.args[1]

        if to.find(",") != -1:
            to = [x.strip() for x in to.split(",")]
        else:
            to = [to]

        msg['To'] = ", ".join(to)

        to += cc

        try:
            s.sendmail(params["FromAddress"], to, msg.as_string())
            return True
        except Exception, e:
            s.quit()
            try:
                return e.args[0][e.args[0].keys()[0]][1]
            except:
                return "Error"


    def GetMails(self, params, theme = ""):
        emails = []
        conn = imaplib.IMAP4_SSL(self.ImapServer)
        conn.login(params["FromAddress"], params["Password"])
        status, messages = conn.select('INBOX')
        if status != "OK":
            return -1

        (retcode, messages) = conn.search('utf-8', 'subject', theme.encode('utf-8'))
        for num in messages[0].split(' '):
            try:
                typ, data = conn.fetch(num,'(RFC822)')
                msg = email.message_from_string(data[0][1])
                emails.append(msg)
            except:
                pass

        return emails

    def GetMailsAttachments(self, emails, attachmentsDir):
        attachmentsNames = []
        for msg in emails:
            for part in msg.walk():
                if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is None:
                    continue

                filename =  part.get_filename()
                if filename == None:
                    continue
                nameParts = email.header.decode_header(filename.encode("utf-8"))
                try:
                    filename = nameParts[0][0].decode("utf-8") + " " + nameParts[1][0]
                except:
                    filename = nameParts[0][0].decode("utf-8")
                attachmentsNames.append(filename)
                att_path = os.path.join(attachmentsDir, filename)

                if not os.path.isfile(att_path):
                    fp = open(att_path, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()

        return attachmentsNames

    def ReplyToEmail(self, params, mail, message):
        message = message.encode('windows-1251')
        oldMessage = ""
        if mail.get_content_maintype() == 'multipart':
            for part in mail.walk():
                if part.get_content_type() == "text/html":
                    oldMessage += part.get_payload(decode=True)
                else:
                    continue
        oldMessage = "<br/><br/>-------------------<br/><br/>" + oldMessage
        msg = MIMEMultipart("mixed")
        body = MIMEMultipart("alternative")
        body.attach( MIMEText(message + oldMessage, "plain",'windows-1251') )
        body.attach( MIMEText("<html>{0}</html>".format(message + oldMessage), "html",'windows-1251') )
        msg.attach(body)

        msg["Message-ID"] = email.utils.make_msgid()
        msg["In-Reply-To"] = mail["Message-ID"]
        msg["References"] = mail["Message-ID"]
        msg["Subject"] = "Re: "+mail["Subject"]
        msg["To"] = mail["Reply-To"] or mail["From"]
##        msg["To"] = params["FromAddress"]
        msg["From"] = params["FromAddress"]
        s = smtplib.SMTP_SSL()
        try:
            s.connect(self.SmtpServer, 465)
            s.login(params["FromAddress"], params["Password"])
        except Exception, e:
           return e.args[1]

        try:
            s.sendmail(params["FromAddress"], msg["To"], msg.as_string())
            return True
        except Exception, e:
            s.quit()
            try:
                return e.args[0][e.args[0].keys()[0]][1]
            except:
                return "Error"

    def GetUnreadedMails(self, params):
        if params["FromAddress"] in ["feedback@knowall.ru.com", "feedback@otpugivateli-ptic.ru"]:
            conn = imaplib.IMAP4_SSL("imap.mail.ru")
        else:
            conn = imaplib.IMAP4_SSL("imap.yandex.com")
        conn.login(params["FromAddress"], params["Password"])
        status, messages = conn.select('INBOX')
        if status != "OK":
            return -1

        retcode, messages = conn.search(None, '(UNSEEN)')
        return messages