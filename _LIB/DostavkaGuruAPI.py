﻿import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
from BeautifulSoup import BeautifulStoneSoup as Soup
import MySQLdb
import io
#import httplib2
import mimetypes
import json
import hashlib
import time

class DostavkaGuruAPI:

    calculateUrl = "http://api.dostavka.guru/client/calc_guru_main_2_0.php"
    checkAddressUrl = "http://api.dostavka.guru/client/check_addr.php"
    addOrderUrl = "http://api.dostavka.guru/client/in_up_2_0.php"
    getDateUrl = "http://api.dostavka.guru/client/date_dost.php"
    getOrderInfoUrl = "http://api.dostavka.guru/client/order_info_post.php"

    login = None
    password = None

    def __init__(self, test = False, login = "2127", password = u"c2643f0c3f7d3d75f580c5e5d5cde18b"):
        self.login = login
        self.password = password
        self.test = test
        if self.test == True:
            self.login = "9999"
            self.password = "827ccb0eea8a706c4c34a16891f84e7b"

    def Calculate(self, data):
        f = {}
        f["method"] = u"Почта"
        f["weight"] = "0.5"
        f["zip_1"] = "115738"
        f["zip_2"] = data["zip_2"]
        f["post_type_method"] = data["post_type_method"]
        f["post_type_otp"] = "1"

        resp = self.SendRequest(self.calculateUrl, f)
        return resp

    def GetDate(self):

        resp = self.SendRequest(self.getDateUrl)
        date = etree.HTML(resp).xpath("//day[2]")[0].text
        return date

    def AddOrder(self, data):

        f = {}
        f["sposob_dostavki"] = u"Почта"
        f["ves_kg"] = "0.5"
        f["mesta"] = "1"
        f["usluga"] = u"ДОСТАВКА"
        f["region_iz"] = u"Москва"
        f["date_dost"] = self.GetDate()

        f["zip_v"] = data["zip_v"]
        f["order_number"] = data["order_number"]
        f["cont_name"] = data["cont_name"]
        f["tip_otpr"] = data["tip_otpr"]
        f["cont_tel"] = data["cont_tel"]
        f["comment"] = data["comment"]
        f["adres"] = data["adres"]
        f["delivery_time_from"] = data["delivery_time_from"]
        f["delivery_time_to"] = data["delivery_time_to"]
        f["nal_plat"] = data["nal_plat"]
        f["ocen_sum"] = data["ocen_sum"]

        resp = self.SendRequest(self.addOrderUrl, f)
        return resp

    def CheckAddress (self, address):

        f = {}
        f["addr"] = address

        try:
            resp = self.SendRequest(self.checkAddressUrl, f)
            json_ = json.loads(resp)
            return json_["data"][0][0]
        except:
            return -1

    def GetOrderTrackingCode (self, orderNumber):

        f = {}
        f["order_number"] = orderNumber
        resp = self.SendRequest(self.getOrderInfoUrl, f)
        try:
            return Soup(resp).root.order.track_number.text
        except:
            return -1

    def SendRequest(self, url, f = {}):

        f["partner_id"] = self.login
        f["client"] = self.login
        f["key"] = self.password
        xml = requests.post(url, data=f)
        return xml._content
