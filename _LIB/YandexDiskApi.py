﻿import requests
import log
import urllib
import json
import sys

class YandexDiskApi():

    access_token = "AQAEA7qgtdoVAAV7Q-HV_oh2C03xgnp4A-HcB_U"
    mainUrl = "https://cloud-api.yandex.net/v1/disk/resources"


    def __init__(self):
        pass

    def CreateDir(self, dirName):
        data = {"path":dirName}
        params = urllib.urlencode( data)
        path = "https://cloud-api.yandex.net/v1/disk/resources?{0}".format(params)
        resp = self.Put(path)
        if "error" in resp.keys():
            return resp["error"]
        else:
            return True

    def SendFile(self, filePath, diskPath):
        fileName = filePath.split("\\")[-1]
        data = {"path":diskPath + "/" + fileName, "overwrite":"true"}
        params = urllib.urlencode( data)
        path = "https://cloud-api.yandex.net/v1/disk/resources/upload?{0}".format(params)
        resp =  self.Get(path)
        if "error" in resp.keys():
            return resp["error"]
        href = resp["href"]
        resp =  self.Put(href, filePath)
        if "error" in resp.keys():
            return resp["error"]
        else:
            return True

    def GetSubdirectories(self, dirPath):
        data = {"path":dirPath}
        params = urllib.urlencode(data)
        path = "https://cloud-api.yandex.net/v1/disk/resources?{0}".format(params)
        resp = self.Get(path)
        if "error" in resp.keys():
            return resp["error"]
        else:
            subdirs = [x["name"] for x in resp["_embedded"]["items"]]
            return subdirs

    def DeleteResource(self, resourcePath):
        data = {"path":resourcePath, "permanently":"true"}
        params = urllib.urlencode( data)
        path = "https://cloud-api.yandex.net/v1/disk/resources?{0}".format(params)
        resp = self.Delete(path)
        if "error" in resp.keys():
            return resp["error"]
        else:
            return True

    def Put(self, path, file = None):
        headers = { 'Authorization' : 'OAuth {0}'.format(self.access_token) }
        if file == None:
            response = requests.put(path, headers=headers, verify=False)
        else:
            with open(file, "rb") as fh:
                mydata = fh.read()
            response = requests.put(path, headers=headers, data=mydata, verify=False)
        if response.content != "":
            resp = json.loads(response.content)
        else:
            resp = {}
        return resp

    def Get(self, path):
        headers = { 'Authorization' : 'OAuth {0}'.format(self.access_token) }
        response = requests.get(path, headers=headers, verify=False)
        resp = json.loads(response.content)
        return resp

    def Delete(self, path):
        headers = { 'Authorization' : 'OAuth {0}'.format(self.access_token) }
        response = requests.delete(path, headers=headers, verify=False)
        if response.content != "":
            resp = json.loads(response.content)
        else:
            resp = {}
        return resp
