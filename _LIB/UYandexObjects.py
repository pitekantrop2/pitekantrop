﻿from UBaseObjects import *

class UYandexObjects(UBaseObjects):


    #Авторизация
    auth_loginButton = ["xpath", "//a[@class = ' nb-button _nb-large-action-button new-hr-auth-Form_Button-enter'] and contains(.,'" + u"Войти в Яндекс.Почту" + "')]"]
    auth_login = ["name", "login"]
    auth_password = ["name", "passwd"]
    auth_accPassword = ["name", "password"]
    auth_accPasswordAgain = ["name", "passwordagain"]
    ##auth_loginButton = ["xpath","//button[@class = 'nb-button _init nb-button_size_m  nb-button_theme_action new-auth-form-button']"]
    auth_loginButton = ["xpath","//*[@type = 'submit']"]
    auth_closePopup = ["xpath", "//div[@class = 'b-popup__close daria-action']"]

    #Регистрация
    reg_addPopup = ["xpath", "//div[@class = 'b-expand__body']"]
    reg_addAccButton = ["xpath", "//button[contains(@class, 'js-expand__save')]"]
    reg_newAccButton = ["xpath", "//span[contains(@class, 'js-domain__link_add-email')]"]
    reg_userMenu = ["xpath", "//span[contains(@class, 'header-user-pic')]"]
    reg_userMenuName = ["xpath", "//span[contains(@class, 'header-user-name')]"]
    reg_logoutItem = ["linktext", "Выход"]
    reg_logoutLink = ["xpath", "//a[@class = 'b-mail-dropdown__item__content Выход daria-action']"]


    #Завершение регистрации.
    finishReg_endRegistraionButton = ["xpath", "//button[@type = 'submit']"]
    finishReg_accName = ["id", "firstname"]
    finishReg_accLastname = ["id", "lastname"]
    finishReg_controlQuestion = ["id", "hint_question_id"]
    finishReg_controlAnswer = ["id", "hint_answer"]
    finishReg_login = ["id", "login"]
    finishReg_password = ["id", "password"]
    finishReg_confirmPassword = ["id", "password_confirm"]
    finishReg_captchaAnswer = ["id", "answer"]
    finishReg_captchaImg = ["class", "captcha__captcha__text"]

    #перенаправление почты
    resend_settings = ["linktext", "настроить…"]
    resend_rules = ["linktext", "Правила обработки почты"]
    resend_newRuleButton = ["xpath", "//button[contains(.,'" + u"Создать" + "')]"]
    resend_condition = ["xpath", "//span[contains(.,'" + u"совпадает" + "')]"]
    resend_contains = ["xpath", "//span[contains(.,'" + u"содержит" + "') and @class = 'nb-select__text']"]
    resend_condField = ["name", "field3"]
    resend_continueLink = ["xpath", "//span[@action = 'filters.enable-forwarding-options']"]
    resend_enterPassword = ["name", "password"]
    resend_confirm = ["xpath", "//button[@class = 'nb-button _init nb-button_size_s  nb-button_theme_normal daria-action ']"]
    resend_resendCh = ["xpath", "//span[contains(.,'" + u"Переслать по адресу" + "')]"]
    resend_mailAddress = ["name", "forward_address"]
    resend_safeCopyCh = ["xpath", "//span[contains(.,'" + u"сохранить копию" + "')]"]
    resend_createRuleButton = ["xpath", "//button[@class = 'nb-button _init nb-button_size_l  nb-button_theme_action']"]
    resend_turnonRule =["xpath", "//button[@class = 'nb-button _init nb-button_size_m  nb-button_theme_normal']"]

    #cообщения
    letters_messages = ["xpath", "//div[contains(@class, 'b-messages')]"]
    letters_letters_links = ["xpath", "//div[@class = 'b-messages']/div[@data-action='mail.message.show-or-select']//a[@class = 'b-messages__message__link daria-action' and contains(.,'" + u"Подтверждение" + "')]"]
    letters_confirm_link = ["xpath", "//a[contains(@href, 'filters-confirm' )]"]


    #удаление url
    delete_url_url = ["name", "url"]
    delete_url_delete_button = ["xpath", "//td[2]/span"]


    #добавление сайта в webmaster
    addSite_addSiteInput = ["name", "hostName"]
    addSite_addSiteButton = ["xpath", "//button[contains(@class, 'one-line-submit__submit')]"]
    addSite_addSiteButton_HtmlFileTab = ["id", "verification-tabs-tab-1"]
    addSite_downloadFileLink = ["xpath", "//a[contains(@href, '/downloads/verification')]"]
    addSite_CheckFile = ["xpath", "//button[contains(@class, 'verification__verify')]"]
    addSite_Icon = ["xpath", "//i[@class = 'b-icon b-icon_custom']"]
    addSite_dropRights = ["xpath", "//span[contains(.,'" + u"Сбросить права" + "')]"]

    #список сайтов
    pageLink = ["class", "b-pager__page"]
    deleteSiteButton = ["xpath", "//td[@class = 'cell close b-sites__close']"]
    nextPageLink = ["class", "b-pager__next"]
    siteLink = ["xpath", "//td[@class = 'b-sites__name cell']//a"]
    confirmRightsLink = ["xpath", "//td[@class = 'cell b-sites__long-text']//a"]

    #подтверждение прав
    htmlFileTab = ["xpath", "//a[contains(.,'" + u"html-файл" + "')]"]

    #добавление регионов
    region_RegionEdit = ["xpath", "//span[contains(@class, 'region-select__search-input-control')]//input"]
    region_UrlEdit = ["xpath", "//div[contains(@class, 'regions-webmaster__evidence')]//input"]
    region_SetRegionButton = GetButtonByText(u"Сохранить")
    region_RegionVariant = ["xpath", "//li[contains(@class, 'suggest2-item')]"]
    region_NoRegion = ["xpath", "//div[@class = 'suggest2__not-found']"]
    region_noRegionButton = GetButtonByText(u"регион сайта не задан")
    region_editRegionButton = ["xpath", "//button[contains(@class, 'regions-webmaster__edit-button')]"]
    region_addRegionButton = GetButtonByText(u"Добавить регион")

    #переезд
    mirrors_HttpsCh = GetElementByText("label", u"Добавить HTTPS")
    mirrors_SaveButton = ["xpath", "//button[contains(@class, 'button_theme_action')]"]

    #справочник
    directory_name =  ["name", "name"]
    directory_addressString =  ["name", "address_string"]
    directory_phones =  ["name", "phones"]
    directory_urls =  ["name", "urls"]
    directory_workingDays = GetElementByText("span", u"ежедневно")
    directory_activity = ["xpath", "//input[contains(@placeholder,'" + u"Начните печатать и выберите из списка" + "')]"]
    directory_menuItem = ["xpath", "//div[@role = 'menuitem']"]
    directory_AddOrganization = GetButtonByText(u"Добавить организацию")
    directory_Add = GetButtonByText(u"Добавить")
    directory_ConfirmAdding = GetButtonByText(u"Добавить")
    directory_Follow = GetElementByText("span",u"Перейти")
    directory_AddLink = GetLinkByText(u"Добавить")

    #sitemaps
    sitemaps_Add = GetButtonByText(u"Добавить")
    sitemaps_sitemapEdit = ["name", "sitemapUrl"]
    sitemaps_deleteButton = ["xpath", u"//button[@aria-label = '{0}']".format(u"Удалить")]
    sitemaps_reindex = ["xpath", u"//button[@aria-label = '{0}']".format(u"восстановить")]

    #reindex
    reindex_urls = ["xpath", "//div[@class = 'CodeMirror-lines']/div/div"]
    reindex_sendButton = GetButtonByText(u"Отправить")
    reindex_quota = ["class", "add-url__quota"]
    reindex_closeButton = ["xpath", u"//button[@aria-label = '{0}']".format(u"Удалить")]

    #market
    market_frame = ["class", "iframe-content__iframe"]
    market_activateShopButton = ["xpath", "//input[@value = '" + u"Включить магазин" + "']"]
    market_deactivateShopButton = ["xpath", "//input[@value = '" + u"Выключить магазин" + "']"]

    market_agreeButton = GetElementByText("span",u"Согласен")
    market_changeLink = GetElementByText("span",u"изменить")
    market_shopRegionName = ["name", "own-region-name"]
    market_shopRegionVariant = ["xpath", "//span[@class = 'b-suggest-elem b-suggest-elem-link']"]
    market_shopShippingRegion = ["name", "shipping-region-name"]
    market_shopShippingInfo = ["class", "wsw-type-textarea"]
    market_shopOnlineOrderSelect = ["class", "wsw-type-select"]
    market_shopPhone = ["class", "wsw-type-phone"]
    market_shopPageUrl = ["class", "wsw-type-url"]
    market_shopNextButton = ["id", "next-button"]

    market_ULForm = ["class", "wsw-type-select"]
    market_ULName = ["class", "wsw-type-text"]
    market_ULOGRN = ["xpath", "//div[@class = 'wsw-field wsw-field-name-ogrn']//input"]
    market_ULPageUrl = ["class", "wsw-type-url"]
    market_ULPostAddressCountry = ["xpath", "//input[contains(@id, 'country-field-input')]"]
    market_ULPostAddressCountry = ["xpath", "//input[contains(@id, 'country-field-input')]"]
    market_ULPostAddressCity = ["xpath", "//input[contains(@id, 'locality-name-field')]"]
    market_ULPostAddressStreet = ["xpath", "//input[contains(@id, 'street-field-input')]"]
    market_ULPostAddressHome = ["xpath", "//input[contains(@id, 'home-field-input')]"]
    market_ULPostAddressOffice = ["xpath", "//input[contains(@id, 'office-field-input')]"]

    market_dataUrl = ["name", "url"]
    market_dataAddButton = ["xpath", "//input[@value = '" + u"Добавить" + "']"]
    market_dataRefresh = GetElementByText("span", u"обновить")
    market_dataActivateFile = ["class", "feed-activate-button"]
    market_dataSaveButton = ["id", "save-button"]
    market_dataNextButton = ["xpath", "//input[@value = '" + u"далее..." + "']"]

    #captcha
    captchaValueEdit = ["name", "captcha-value"]
    captchaImg = ["xpath", "//img[contains(@class, 'service-captcha__image')]"]
    sendCaptchaButton = GetElementByText("button", u"Отправить")

    #фиды
    feeds_feedUrl = ["xpath", u"//input[@class = 'application-feed__input' and @placeholder = 'Ссылка на feed']"]
    feeds_feedState = ["class", u"application-feed__status_text"]
    feeds_feedRemoveButton = ["class", u"application-feed__remove-button"]
    feeds_feedRegion = ["xpath", u"//input[@class = 'application-feed__input' and @placeholder = 'Регион']"]
    feeds_addFeedButton = GetButtonByText(u"Добавить еще один feed")
    feeds_checkFeedButton = GetButtonByText(u"Проверить")
    feeds_ListItem = ["class", "application-feed__region-suggest_item"]





