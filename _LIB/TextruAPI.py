﻿import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
import MySQLdb
import io
import mimetypes
import json
import hashlib
import time
from lxml.builder import E
import commonLibrary
from xml.etree import ElementTree
import datetime

class TextruAPI:

    key = "52063b1649d44e28563614f025a85cb5"
    url = "http://api.text.ru/post"

    def __init__(self):
        pass

    def GetTextUniqueness(self, text):
        data = {}
        data["text"] = text
        responseJson = self.SendRequest(data)
        if responseJson == -1 or "text_uid" not in responseJson.keys():
            return -1
        uid = responseJson["text_uid"]
        data = {"uid" : uid}
        while True:
            responseJson = self.SendRequest(data)
            if responseJson == -1:
                return -1
            if "error_code" in responseJson.keys():
                time.sleep(5)
            else:
                urls = json.loads(responseJson["result_json"])["urls"]
                return [responseJson["text_unique"],urls ]


    def SendRequest(self, data):
        try:
            data["userkey"] = self.key
            resp = requests.post(self.url, data=data)
            responseJson = json.loads(resp.text)
            return responseJson
        except:
            return -1

