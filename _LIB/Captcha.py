﻿import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
import io
import mimetypes
import json
import hashlib
import time
import myLibrary
import requests
from suds.client import Client
import base64

class Captcha:

    key = "346b5e2221aa78f7e27c0dee07550a56"
    createTaskUrl = "https://api.anti-captcha.com/createTask"
    getTaskResultUrl = "https://api.anti-captcha.com/getTaskResult"
    reportIncorrectImageCaptchaUrl = "https://api.anti-captcha.com/reportIncorrectImageCaptcha"
    phrase = False
    case = False
    numeric = False

    def __init__(self, phrase = False, case = False, numeric = False):
        self.phrase = phrase
        self.case = case
        self.numeric = numeric

    def iriToUri(self, iri):
        parts= urlparse.urlparse(iri)
        return urlparse.urlunparse(
            part.encode('idna') if parti==1 else self.urlEncodeNonAscii(part.encode('utf-8'))
            for parti, part in enumerate(parts)
        )

    def urlEncodeNonAscii(self, b):
        return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

    def GetImage(self, image):
        if image.find("http") != -1:
            originImagePath = self.iriToUri(image)
            try:
                image = "captcha.jpg"
                urllib.urlretrieve(originImagePath, image)
            except:
                return False

        imageFile = open(image, 'rb').read()
        encodedFile = base64.encodestring(imageFile)
        return encodedFile.replace("\n","")


    def SolveTextCaptcha(self, image):
        task= {}
        task["type"] = "ImageToTextTask"
        task["phrase"] = self.phrase
        image_ = self.GetImage(image)
        if image_ == False:
            return False
        task["body"] = image_
        task["case"] = self.case
        task["numeric"] = self.numeric
        taskId = self.CreateTask(task)
        return [ self.GetTaskResult(taskId)['text'], taskId]

    def SolveRecaptcha(self, url, websiteKey = "6LeJtBcTAAAAANfu_uoIFPytpr_JASQ1E6NHLWE5"):
        task= {}
        task["type"] = "NoCaptchaTaskProxyless"
        task["websiteURL"] = url
        task["websiteKey"] = websiteKey
        taskId = self.CreateTask(task)
        return [ self.GetTaskResult(taskId, 10)['text'], taskId]

    def CreateTask (self, task):
        data = {}
        data["clientKey"] = self.key
        data["task"] = task
        resp = requests.post (self.createTaskUrl, data=json.dumps(data))
        responseJson = json.loads(resp._content)
        return responseJson['taskId']

    def GetTaskResult(self, taskId, timeout = 2):
        time.sleep(timeout)
        data = {}
        data["clientKey"] = self.key
        data["taskId"] = taskId
        while True:
            resp = requests.post (self.getTaskResultUrl, data=json.dumps(data))
            responseJson = json.loads(resp._content)
            if responseJson['status'] == "ready":
                break
            time.sleep(2)
        return responseJson['solution']

    def ReportIncorrectImageCaptcha(self, taskId):
        data = {}
        data["clientKey"] = self.key
        data["taskId"] = taskId
        resp = requests.post (self.reportIncorrectImageCaptchaUrl, data=json.dumps(data))
        responseJson = json.loads(resp._content)
