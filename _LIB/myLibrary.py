﻿from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import time
import sys
import os


class Selenium:

    driver = None

    def SetUp(self, browser = "Chrome"):
        try:
            if browser == "Firefox":
                self.driver = webdriver.Firefox()
            if browser == "Chrome":
                self.driver = webdriver.Chrome()
            if browser == "Headless":
                self.driver = webdriver.Remote("http://localhost:4444/wd/hub", webdriver.DesiredCapabilities.HTMLUNITWITHJS)
        except Exception, e:
            print e
            sys.exit()


    def CleanUp(self):
        for windowHandle in self.driver.window_handles:
            self.driver.switch_to_window(windowHandle)
            self.driver.close()
        #os.system("taskkill /f /im chromedriver.exe")

    def SendKeys(self, element, text):
        if type(element) == type([]) :
            element = self.MyFindElement(element)
        element.clear()
        element.send_keys(text)

    def DragAndDrop(self, element, x, y):
        if type(element) == type([]) :
            element = self.MyFindElement(element)
        actionChains = ActionChains(self.driver)
        actionChains.drag_and_drop_by_offset(element,x,y)

    def MouseMoveAt(self, element, x, y):
        if type(element) == type([]) :
            element = self.MyFindElement(element)
        actionChains = ActionChains(self.driver)
        actionChains.move_to_element_with_offset(element,x,y)

    def OpenUrl(self, url):
        self.driver.get(url)

    def GetTitle(self):
        return self.driver.title

    def GetCurrentUrl(self):
        return self.driver.current_url

    def GetSource(self):
        return self.driver.page_source

    def Click(self, element):
        if type(element) == type([]):
            element = self.MyFindElement(element)
        element.click()

    def Sleep(self, time_):
        time.sleep(time_)

    def SetSelectOptionByText(self, element, text):
        options = self.MyFindElementsInElement(element,["tagname","option"])
        for option in options:
            optext = self.GetText(option).encode('utf-8')
            if optext.find(text) != -1 and optext != "":
                self.Click(option)
                return

        self.Exit(u"Не найдено значение '" + text.data().decode('utf-8') + u"' в списке")

    def SetSelectOptionByIndex(self, element, index):
        options = self.MyFindElementsInElement(element,["tagname","option"])
        self.Click(options[index])
        return


    def SetListIndexByText(self, element, text):
        options = self.MyFindElementsInElement(element,["tagname","li"])
        for option in options:
            optext = self.GetText(option).encode('utf-8')
            if optext.startswith(text) == True:
                self.Click(option)
                return
        self.Exit(u"Не найдено значение '" + text.data().decode('utf-8') + u"' в списке")

    def SetInputChecked(self, element, bMustBeChecked):
        if type(element) == type([]):
            element = self.MyFindElement(element)

        if bMustBeChecked == True:
            if element.is_selected() != True:
                self.Click(element)
        else:
            if element.is_selected() != False:
                self.Click(element)

    def FindElement(self, element):
        if type(element) != type([]):
            return element

        if element[0] == "id":
            return self.GetFirstVisible(self.driver.find_elements_by_id(element[1]))
        elif element[0] == "class":
            return self.GetFirstVisible(self.driver.find_elements_by_class_name(element[1]))
        elif element[0] == "cssselector":
            return self.GetFirstVisible(self.driver.find_elements_by_css_selector(element[1]))
        elif element[0] == "name":
            return self.GetFirstVisible(self.driver.find_elements_by_name(element[1]))
        elif element[0] == "xpath":
            return self.GetFirstVisible(self.driver.find_elements_by_xpath(element[1]))
        elif element[0] == "tagname":
            return self.GetFirstVisible(self.driver.find_elements_by_tag_name(element[1]))
        elif element[0] == "linktext":
            return self.GetFirstVisible(self.driver.find_elements_by_link_text(element[1]))
        else:
            return self.GetFirstVisible(self.driver.find_elements_by_partial_link_text(element[1]))

    def FindElements(self, element):
        if element[0] == "id":
            return self.driver.find_elements_by_id(element[1])
        elif element[0] == "class":
            return self.driver.find_elements_by_class_name(element[1])
        elif element[0] == "cssselector":
            return self.driver.find_elements_by_css_selector(element[1])
        elif element[0] == "name":
            return self.driver.find_elements_by_name(element[1])
        elif element[0] == "xpath":
            return self.driver.find_elements_by_xpath(element[1])
        elif element[0] == "tagname":
            return self.driver.find_elements_by_tag_name(element[1])
        elif element[0] == "linktext":
            return self.driver.find_elements_by_link_text(element[1])
        else:
            return self.driver.find_elements_by_partial_link_text(element[1])

    def MyFindElement(self, element):
        try:
            return self.FindElement(element)
        except NoSuchElementException:
##            Dialogs.ShowErrorMessage(u"Отсутствует элемент '" + element[1].decode('utf-8') + "'")
##            self.CleanUp()
            self.Exit(u"Отсутствует элемент '" + element[1].decode('utf-8') + "'")

    def MyFindElements(self, element):
        try:
            return self.FindElements(element)
        except NoSuchElementException:
##            Dialogs.ShowErrorMessage(u"Отсутствует элемент '" + element[1] + "'")
##            self.CleanUp()
            self.Exit(u"Отсутствует элемент '" + element[1] + "'")

    def MyFindElementsInElement(self, element1, element2):
        if type(element1) == type([]):
            elem1 = self.MyFindElement(element1)
        else:
            elem1 = element1
        try:
            if element2[0] == "id":
                return elem1.find_elements_by_id(element2[1])
            elif element2[0] == "class":
                return elem1.find_elements_by_class_name(element2[1])
            elif element2[0] == "cssselector":
                return elem1.find_elements_by_css_selector(element2[1])
            elif element2[0] == "name":
                return elem1.find_elements_by_name(element2[1])
            elif element2[0] == "xpath":
                return elem1.find_elements_by_xpath(element2[1])
            elif element2[0] == "tagname":
                return elem1.find_elements_by_tag_name(element2[1])
            elif element2[0] == "linktext":
                return elem1.find_elements_by_link_text(element2[1])
            else:
                return elem1.find_elements_by_partial_link_text(element2[1])
        except NoSuchElementException:
##            Dialogs.ShowErrorMessage(u"Отсутствует элемент '" + element2[1] + u"'")
##            self.CleanUp()
            self.Exit(u"Отсутствует элемент '" + element2[1] + u"'")

    def FindElementInElement(self, element1, element2):
        if type(element1) == type([]):
            elem1 = self.FindElement(element1)
        else:
            elem1 = element1

        if element2[0] == "id":
            return self.GetFirstVisible(elem1.find_elements_by_id(element2[1]))
        elif element2[0] == "class":
            return self.GetFirstVisible(elem1.find_elements_by_class_name(element2[1]))
        elif element2[0] == "cssselector":
            return self.GetFirstVisible(elem1.find_elements_by_css_selector(element2[1]))
        elif element2[0] == "name":
            return self.GetFirstVisible(elem1.find_elements_by_name(element2[1]))
        elif element2[0] == "xpath":
            return self.GetFirstVisible(elem1.find_elements_by_xpath(element2[1]))
        elif element2[0] == "tagname":
            return self.GetFirstVisible(elem1.find_elements_by_tag_name(element2[1]))
        elif element2[0] == "linktext":
            return self.GetFirstVisible(elem1.find_elements_by_link_text(element2[1]))
        else:
            return self.GetFirstVisible(elem1.find_elements_by_partial_link_text(element2[1]))

    def MyFindElementInElement(self, element1, element2):
        try:
            return self.FindElementInElement(element1, element2)
        except NoSuchElementException:
            self.Exit(u"Отсутствует элемент '" + element2[1] + u"'")

    def GetElementAttribute(self, element, attributeName):
        if type(element) == type([]):
            element = self.MyFindElement(element)
        return element.get_attribute(attributeName)

    def GetElementText(self, element):
        elem = self.MyFindElement(element)
        elemText = self.GetText(element)
        if elemText == "":
            return self.driver.execute_script("return arguments[0].value",elem)
        else:
            return elemText

    def GetText(self, element):
        if type(element) == type([]):
            element = self.MyFindElement(element)
        return element.text

    def GetValue(self, element):
        if type(element) == type([]):
            element = self.MyFindElement(element)
        return self.driver.execute_script("return arguments[0].value",element)

    def GetElement(self, element):
        try:
            elem = self.FindElement(element)
            return elem
        except NoSuchElementException:
            return None

    def IsVisible(self, element):
        if type(element) == type([]):
            elem = self.GetElement(element)
        else:
            elem = element

        if elem != None:
            try:
                return self.GetElement(element).is_displayed()
            except:
                return False
        else:
            return False

    def WaitFor(self, element, time = 160):
        counter = 0
        while self.IsVisible(element) == False:
            self.Sleep(1)
            counter += 1
            if counter > time:
                #self.Exit(u"Не дождались появления элемента '" + element[1].decode('utf-8') + "'")
                raise Exception (u"Не дождались появления элемента '" + element[1].decode('utf-8') + "'")
                return

    def WaitForText(self, text):
        counter = 0
        while self.IsTextPresent(text) == False:
            self.Sleep(1)
            counter += 1
            if counter > 30:
                #self.Exit(u"Не дождались появления элемента '" + element[1].decode('utf-8') + "'")
                raise Exception (u"Не дождались появления текста '" + text.decode('utf-8') + "'")
                return

    def WaitForTexts(self, texts):
        counter = 0
        isPresent = False
        while isPresent == False:
            self.Sleep(1)
            counter += 1
            for text in texts:
                if self.IsTextPresent(text) == True:
                    isPresent = True
                    break
            if counter > 30:
                #self.Exit(u"Не дождались появления элемента '" + element[1].decode('utf-8') + "'")
                raise Exception (u"Не дождались появления текста '" + text.decode('utf-8') + "'")
                return

    def WaitForTextNotVisible(self, text):
        self.Sleep(1)
        element = ["xpath", "//*[contains(., '" + text + "')]"]
        counter = 0
        while self.IsVisible(element) == True:
            self.Sleep(1)
            counter += 1
            if counter > 30:
                raise Exception (u"Не дождались исчезновения текста '" + text.decode('utf-8') + "'")
                return

    def WaitForNotVisible(self, element):
        counter = 0
        self.Sleep(1)
        while self.IsVisible(element) == True:
            self.Sleep(1)
            counter += 1
            if counter > 30:
                raise Exception (u"Не дождались исчезновения элемента '" + element[1].decode('utf-8') + "'")
                return

    def WaitForElementInElement(self, element1, element2):
        counter = 0
        while self.IsVisibleElementInElement(element1,element2) == False:
            self.Sleep(1)
            counter += 1
            if counter > 30:
                self.Exit(u"Не дождались появления элемента '" + element1[1].decode('utf-8') + "'")
                return

    def IsVisibleElementInElement(self, element1,element2):
        try:
            elem = self.FindElementInElement(element1,element2)
        except NoSuchElementException:
            return False

        return elem.is_displayed()

    def SwitchToFrame(self, frameId):
        self.driver.switch_to_frame(frameId)

    def SwitchToNewWindow(self):
        self.driver.switch_to_window(self.driver.window_handles[len(self.driver.window_handles) - 1])

    def SwitchToBaseWindow(self):
        count = len(self.driver.window_handles)
        for wn in range(1,count):
            self.driver.switch_to_window(self.driver.window_handles[1])
            self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def Back(self):
        self.driver.back()
        self.Sleep(1)

    def Refresh(self):
        self.driver.refresh()
        self.Sleep(1)

    def IsTextPresent(self, text):
        return self.driver.page_source.find(text.decode('utf-8')) >= 0

    def IsTextsPresents(self, texts):
        for text in texts:
            if self.driver.page_source.find(text.decode('utf-8')) >= 0:
                return True
        return False

    def Exit(self, message):
        self.CleanUp()

    def GetFirstVisible(self, elemsArray):
        for i in range(0, len(elemsArray)):
            if self.IsVisible(elemsArray[i]) == True:
                return elemsArray[i]

    def SetElementValue(self, elem, value):
        element = self.MyFindElement(elem)
        self.driver.execute_script("return arguments[0].value = '" + value + "';", element)

    def GetScreenshotAsPng(self):
        return self.driver.get_screenshot_as_png()

    def GetScreenshotAsFile(self, fileName):
        return self.driver.get_screenshot_as_file(fileName)

    def AcceptAlert(self):
        try:
            alert = self.driver.switch_to.alert
            alert.accept()
        except:
            pass

    def MaximizeWindow(self):
        self.driver.maximize_window()
