﻿from selenium import webdriver
import myLibrary
import urllib
import ftplib
import UGoogleObjects

objects = UGoogleObjects.UGoogleObjects


class Google:

    mode = True
    selenium = myLibrary.Selenium()

    def __init__(self, mode):
        self.mode = mode
        if self.mode == True:
            self.selenium.SetUp()
        else:
            self.selenium.driver = webdriver.Remote("http://localhost:4444/wd/hub", webdriver.DesiredCapabilities.HTMLUNITWITHJS)


    def Login(self, Login, Password):
        self.Logout()
        self.selenium.OpenUrl('https://accounts.google.com/ServiceLogin')
        self.selenium.WaitFor(objects.emailEdit)
        self.selenium.SendKeys(objects.emailEdit, Login)
        self.selenium.Click(objects.nextButton)
        self.selenium.WaitFor(objects.passwordEdit)
        self.selenium.SendKeys(objects.passwordEdit, Password)
        self.selenium.Click(objects.nextButton)
        self.selenium.WaitForText("Добро пожаловать")


    def Logout(self):

        pass


