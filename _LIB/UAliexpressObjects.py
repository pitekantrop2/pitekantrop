﻿from UBaseObjects import *

class UAliexpressObjects(UBaseObjects):

    #авторизация
    loginLink = GetLinkByText(u"Войти")
    login = ["id", "fm-login-id"]
    password = ["id", "fm-login-password"]
    loginButton = GetButtonByText(u"Войти")
    userNameSpan = GetLinkByText(u"Заказы")

    #список заказов
    ordersList_SendedOrdersLink = GetElementByText("p", u"Заказ отправлен")
    ordersList_CheckOrderTrackButton = GetLinkByText(u"Проверить отслеживание")

    #заказ
    order_StatesList = ["xpath", "//div[contains(@class, 'Tracking_TrackingGraph__wrapper')]"]
    order_States = ["xpath", "//div[contains(@class, 'Tracking_TrackingStep__description')]/span[1]"]
    order_Dates = ["xpath", "//div[contains(@class, 'Tracking_TrackingStep__description')]/span[2]"]
    order_States2 = ["xpath", "//ul[@class = 'ship-steps']/li/p[1]"]
    order_Dates2 = ["xpath", "//ul[@class = 'ship-steps']/li/p[2]"]
    order_TrackingDetail = ["class", "tracking-detail"]
    order_TrackNum = ["xpath", "//span[contains(@class,'Tracking_ListElement__sub')]"]
    order_TrackNum2 = ["xpath", "//div[contains(.,'Tracking number:')]/p"]






