﻿import requests
import urllib

class SMSAPI:

    smsUrl = "http://sms.ru/sms/send"

    apiId = None
    test = None
    dbLocalConnector = None

    def __init__(self, test = False, apiId = "093d3e2c-6267-a214-41a9-6cdba83279dd"):
        self.apiId = apiId
        self.test = test

    def SendSMS(self, to, message):

        params = {}

        params["to"] = to
        params["text"] = message
        params["test"] = int(self.test)
        params["api_id"] = self.apiId

        response = requests.get(self.smsUrl + "?" + urllib.urlencode(params))
        return response._content


