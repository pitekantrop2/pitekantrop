﻿import requests
import lxml
from lxml import etree
import urllib
import re, urlparse
import HTMLParser
import MySQLdb
import io
import mimetypes
import json
import hashlib
import time
from lxml.builder import E
import datetime

class ModulbankAPI:

    accessToken = "NzM0YTYzMGQtNjQ1Mi00YWZjLWFlZjQtZGQ5ZjllNTQ2YWY5NmY2NzJiNTctYzRiYi00NmRjLTg1MjItNDc5NWI3NWM2NTRi"
    accountId = "5b7ff29f-0ca5-4c64-b7d5-a6c8035b883d"
    getOperationsUrl = "https://api.modulbank.ru/v1/operation-history/"
    getAccountInfoUrl = "https://api.modulbank.ru/v1/account-info/"


    def __init__(self, bNovall = False):
        if bNovall == True:
            self.accountId = "11e8e2fd-be3f-4ceb-9a81-a7c30442785e"
        self.getOperationsUrl += self.accountId

    def GetOperationsHistory(self, date, category):
        data = {}
        data['category'] = category
        data['from'] = date
        data['records'] = 100

        return self.SendRequest(self.getOperationsUrl, data)

    def GetAccountInfo(self):
        data = {}
        return self.SendRequest(self.getAccountInfoUrl, data)

    def SendRequest(self, url, data):
        headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + self.accessToken}
        try:
            resp = requests.post(url,  json=data, headers=headers)
        except Exception as e:
            print str(e)
            return -1
        if resp.status_code == 200:
            return json.loads(resp.text)
        else:
            return -1


