﻿import sendmail
import random
import getRandomData

##<p><img height="49" src="cid:image3" style="float: left; margin-left: 0px; margin-right: 40px; margin-top: 10px; margin-bottom: 10px;" width="210" /></a></p>
##<br/><p>Здравствуйте, {name} !</p>
##<br/>
##<p></p>
##<p><span style="background-color: #ffffff; color: #ff0000;"><strong><span style="font-size: 14px;">Эффективная защита от комаров на природе без вреда для здоровья!</span></strong></span></p>
##<p>Интеллектуальная система Electrofrog &ndash; это <span style="text-decoration: underline;">новейшее средство защиты от комаров</span>, разработанное российскими инженерами при содействии лучших энтомологов страны. Его главная область применения &ndash; леса, дачи, сады, огороды, пасеки, летние кафе и другие открытые участки, где невозможно использование химических и прочих уничтожителей комаров, способных нанести вред человеку, домашним животным, пчёлам или растениям.</p>
##<p></p>
##<p style="text-align: justify;"><img src="cid:image1" style="float: left; margin-left: 20px; margin-right: 20px; margin-top: 10px; margin-bottom: 10px;" /></a><img src="cid:image2" /><strong>&nbsp;</strong>Предназначен для&nbsp;лесов, дач, садов, огородов, пасек, летних кафе и других открытых участков</p>
##<p><img src="cid:image2" />&nbsp;Радиус действия устройства &ndash; 40 метров. Благодаря мобильной стойке его легко перемещать с места на место, в зависимости от ваших потребностей.</p>
##<p><img src="cid:image2" />&nbsp;&laquo;Электрофрог&raquo; не отпугивает гнус, а сокращает его популяцию.&nbsp;При использовании &laquo;Электрофрога&raquo; все кровососущие на вашем участке сразу же ликвидируются.</p>
##<p><img src="cid:image2" />&nbsp;Использует безопасный для людей, животных и растений октенол</p>
##<p></p>
##<p></p>
##<p>Главное преимущество Electrofrog перед другими средствами против комаров &ndash; использование безопасного для людей, животных и растений октенола. Если аналогичные устройства для уничтожения комаров используют накапливающиеся на коже и в тканях хлорорганические соединения, а спреи и фумигаторы &ndash; вызывающие раздражение кожи и аллергию инстектициды, то Электрофрог полностью безвреден и не оказывает никакого воздействия на окружающую среду.<br /><br />Даже распространенные ультразвуковые отпугиватели комаров ему проигрывают: они нисколько не сокращают численность кровососов на местности и, помимо этого, вызывают стресс у домашних животных.</p>
##<p></p>
##<p style="text-align: left;"><span style="color: #000000;">С уважением<span style="font-size: 16px;">, </span></span><span style="color: #000000;">коллектив компании&nbsp;&laquo;Отпугиватель.Com&raquo;<span style="font-size: 16px;"><br /></span></span></p>
##<p style="text-align: left;">Тел.: 8(3812)21-25-89<strong><span style="color: #ff0000; font-size: 16px;"><br /></span></strong></p>
##<p style="text-align: left;"><strong><span style="color: #ff0000; font-size: 16px;">&nbsp;</span></strong></p>
##<p></p>


dbFile = "database_omsk_other"
#dbFile = "database_omsk_test"

message = u'''
<p><img height="49" src="cid:image3" style="float: left; margin-left: 0px; margin-right: 40px; margin-top: 10px; margin-bottom: 10px;" width="210" /></a></p>
<br/><p>Здравствуйте, <strong>{name} </strong>!</p>
<br/>
<p></p>
<p><span style="background-color: #ffffff; color: #ff0000;"><strong><span style="font-size: 14px;">Эффективная защита от комаров на природе без вреда для здоровья!</span></strong></span></p>
<p>Интеллектуальная система Electrofrog &ndash; это <span style="text-decoration: underline;">новейшее средство защиты от комаров</span>, разработанное российскими инженерами при содействии лучших энтомологов страны. Его главная область применения &ndash; леса, дачи, сады, огороды, пасеки, летние кафе и другие открытые участки, где невозможно использование химических и прочих уничтожителей комаров, способных нанести вред человеку, домашним животным, пчёлам или растениям.</p>
<p></p>
<p style="text-align: justify;"><a href="http://otpugivatel.com/page/sistema-unichtozheniya-komarov-electrofrog" target="_blank"><img src="cid:image1" style="float: left; margin-left: 20px; margin-right: 20px; margin-top: 10px; margin-bottom: 10px;" /></a><img src="cid:image2" /><strong>&nbsp;Предназначен для&nbsp;лесов, дач, садов, огородов, пасек, летних кафе и других открытых участков</strong></p>
<p><img src="cid:image2" /><strong>&nbsp;Радиус действия устройства &ndash; 40 метров.</strong> Благодаря мобильной стойке его легко перемещать с места на место, в зависимости от ваших потребностей.</p>
<p><img src="cid:image2" /><strong>&nbsp;&laquo;Электрофрог&raquo; не отпугивает гнус, а сокращает его популяцию.</strong>&nbsp;При использовании &laquo;Электрофрога&raquo; все кровососущие на вашем участке сразу же ликвидируются.</p>
<p><img src="cid:image2" /><strong>&nbsp;Использует безопасный для людей, животных и растений октенол</strong></p>
<p></p>
<p></p>
<p>Главное преимущество Electrofrog перед другими средствами против комаров &ndash; использование безопасного для людей, животных и растений октенола. Если аналогичные устройства для уничтожения комаров используют накапливающиеся на коже и в тканях хлорорганические соединения, а спреи и фумигаторы &ndash; вызывающие раздражение кожи и аллергию инстектициды, то Электрофрог полностью безвреден и не оказывает никакого воздействия на окружающую среду.
<p><br/></p>
<p style="text-align: center;"><span style="color:black; background-color: black;"><a href="http://otpugivatel.com/page/sistema-unichtozheniya-komarov-electrofrog" target="_blank"><span style="color: black; background-color: #ffffff;"><strong><span style="color: black; font-size: 16px;"><span style="color: black;">Получить больше информации</span></span></strong></span></a></span><br /><br /><strong><span style="color: #ff0000; font-size: 16px;"><br /></span></strong></p>
<p style="text-align: left;"><span style="color: #000000;">С уважением<span style="font-size: 16px;">, </span></span><span style="color: #000000;">коллектив компании&nbsp;&laquo;Отпугиватель.Com&raquo;<span style="font-size: 16px;"><br /></span></span></p>
<p style="text-align: left;">Тел.: 8(3812)21-25-89<strong><span style="color: #ff0000; font-size: 16px;"><br /></span></strong></p>
<p style="text-align: left;"><a href="http://otpugivatel.com" target="_blank">http://otpugivatel.com</a></p>
<p style="text-align: left;"><strong><span style="color: #ff0000; font-size: 16px;">&nbsp;</span></strong></p>
<p></p>
        '''

#message = "234234"
images = ["elfrog1.png","gal.jpg", "logo_small.png"]
#images = []
mails = []
subject = "Комфортная жизнь без летающих насекомых для {name}"
#subject = "привет!"

##for i in range(1,31):
##    mails.append("adv" + str(i) + "@otpugivatel.com")

for i in range(1,31):
    mails.append("adv" + str(i) + "@otpugivatel.com")

sendmail.SendEmails("c:\\_MAIL_SENDER\\otpugivatel\\",dbFile,mails,message,subject, images)