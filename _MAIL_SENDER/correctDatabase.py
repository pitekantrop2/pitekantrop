﻿import os
import re

errorMailsFile = "errorMails.txt"
csvFileName = "database_omsk.csv"

with open(csvFileName, "r") as myfile:
        emails = myfile.readlines()

em = open(errorMailsFile, "r")
errorEmails = em.read()
em.close()

newEmails = []

for i in range(0, len(emails)):
    entry = emails[i].split(';')[1].replace("\n","")
    if errorEmails.find(entry) == -1:
        newEmails.append(emails[i])

print len(newEmails)
with open("corrected_database_omsk.csv", "a") as myfile:
    for entry in newEmails:
        myfile.write(entry)
