﻿import requests
from lxml import etree
import urllib

def AddRecord(text):
##    with open(csvFileName, "r") as myfile:
##        if myfile.read().find(text.split(';')[1]) != -1 :
##            return
    with open(csvFileName, "a") as myfile:
            myfile.write(text + "\n")

csvFileName = "database_omsk.csv"
category = "ресторан"

baseUrl = "http://old.gorod55.ru"
searchUrl = baseUrl + "/catalog"

if category == "all":
    resp = requests.get(searchUrl)
    tree = etree.HTML(resp._content)
    subsections = tree.xpath("//a[contains(@id, 'ctl00_ctl00_body_body_catalogTree_tree1t')]")

    for i in range(16,len(subsections)):

        subsectionLink = subsections[i].attrib['href']
        subsectionName = subsections[i].text
        subsectionFirmsCount = int (subsectionName.split('(')[1].replace(")",""))

        pageNumber = subsectionFirmsCount / 20

        for j in range(1, pageNumber + 1):

            subsectionCurP = requests.get(baseUrl + subsectionLink + "&page=" + str(j))
            subsectionCurPage = etree.HTML(subsectionCurP._content)

            rows = subsectionCurPage.xpath("//table[@class = 'find_table']//tr")

            for k in range(1,len(rows)):
                if len(rows[k].xpath("./td/a[contains(@href, 'mailto')]")) != 0:
                    try:
                        name = rows[k].xpath("./td[2]/b/a")[0].text.strip()
                        email = rows[k].xpath("./td/a[contains(@href, 'mailto')]")[0].attrib['href'].replace("mailto:","")
                        AddRecord(name + ";" + email)
                    except:
                        continue

else:
    resp = requests.get(searchUrl + "/company/?inName=False&inDescription=False&page=1&text=" + urllib.quote(category))
    tree = etree.HTML(resp._content)

    slastPageNumber = tree.xpath("//a[contains(., ' >>')]")[0].attrib['href'][-2:].replace("=","")
    lastPageNumber = int(slastPageNumber)

    for i in range(1, lastPageNumber):
        resp = requests.get(searchUrl + "/company/?inName=False&inDescription=False&page=" + str(i) + "&text=" + urllib.quote(category))
        tree = etree.HTML(resp._content)
        rows = tree.xpath("//table[@class = 'find_table']//tr")
        for k in range(1,len(rows)):
            try:
                name = rows[k].xpath("./td[2]/b/a")[0].text.strip()
                descr = rows[k].xpath("./td[3]")[0].xpath(".//text()")
                info = descr[1].replace("\r\n", "").replace("\t", "").replace("                                ","").replace("\n", "")
                AddRecord(name.encode("Windows-1251") + ";" + info.encode("Windows-1251"))
            except:
                continue