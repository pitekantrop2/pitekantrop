﻿import requests
import random
import time
import getRandomData
from lxml import etree
import urllib
from antigate import AntiGate

antigateKey = '0d841d49f0062d8940c1ab538e9b0f98'

def GetAntigate(pictureName):
    gate = AntiGate(antigateKey,pictureName)
    return gate

#0.9534
#print gate
#gate = GetAntigate("securecode.php.png")
gate = AntiGate(antigateKey)
print gate.balance()

url = "http://voronej.oborudunion.ru/mailer.php?to=7c9b66867ac2fc3a1470971f32cf0001&mod=2"

while(True):

    #item = GetRandomItem()
    resp = requests.get(url)
    tree = etree.HTML(resp._content)
    hashId = tree.xpath("//input[@name = 'hashid']")[0].attrib['value']
    imgPath = tree.xpath("//img[contains(@src, 'securecode')]")[0].attrib['src']

    urllib.urlretrieve(imgPath, "securecode.jpg")

    f = {}
    f['hashid'] = hashId
    f['r_name'] = "Босфор, ООО"
    f['title'] = random.choice(["Запрос информации", "Заказ товара"])
    f['text'] = getRandomData.GetRandomStringWithSpaces(200)
    f['MAX_FILE_SIZE'] = "10485760"
    f['file'] = ""
    fio = getRandomData.GetRandomMaleRandomLastname() + " " + getRandomData.GetRandomMaleRandomName() + " " + getRandomData.GetRandomMaleRandomMiddleName()
    f['s_name'] = fio
    f['s_company'] = ""
    f['s_email'] = getRandomData.GetRandomEmail()
    f['s_tel'] = "8" + getRandomData.GetRandomPhone()
    f['s_country'] = getRandomData.GetRandomRussianCity()
    f['securecode'] = GetAntigate("securecode.jpg")
    f['refferer'] = ""

    resp = requests.post(url, data=f)
    time.sleep(240)