﻿from antigate import AntiGate
import datetime
import time
import requests
from lxml import etree
import urllib
import log

antigateKey = '0d841d49f0062d8940c1ab538e9b0f98'

def GetAntigate(pictureName):
    config = {'is_russian': '1'}
    gate = AntiGate(antigateKey,pictureName, send_config=config)
    return gate


gate = AntiGate(antigateKey)

logFilePath = log.GetLogFileName("c:\\_HTTP")
logging = log.Log(logFilePath)
csvFileName = "urls.csv"

with open(csvFileName, "r") as myfile:
        urls = myfile.readlines()

for url in urls:

    url = url.decode('windows-1251')
    resp = requests.get("http://webmaster.yandex.ru/addurl.xml")
    tree = etree.HTML(resp._content)
    key = tree.xpath("//input[@name = 'key']")[0].attrib['value']
    sk = tree.xpath("//input[@name = 'sk']")[0].attrib['value']
    imgPath = tree.xpath("//div[@class = 'captcha']//img")[0].attrib['src']

    urllib.urlretrieve(imgPath, "yacode.jpg")

    postData = {}

    postData["do"] = "add"
    code = GetAntigate("yacode.jpg")
    postData["rep"] = code
    postData["key"] = key
    postData["sk"] = sk
    postData["url"] = url

    resp = requests.post("http://webmaster.yandex.ru/addurl.xml", data=postData)

    if resp.status_code == 200:
        logging.LogInfoMessage(u"Добавлен урл '" + url + "'")
    else:
        logging.LogErrorMessage(u"Не удалось добавить урл '" + url + "'")


