﻿import requests
import random
import time
import getRandomData
from lxml import etree
import urllib


def GetRandomItem():
    products = ["Топливные гранулы (Пеллеты)",
            "топливные брикеты",
            "Браслеты от комаров Bugs Lock",
            "Ультразвуковой отпугиватель Weitech WK-0600 CIX2",
            "Ультразвуковой отпугиватель птиц и грызунов для складов WK 052 SD",
            "биоакустический отпугиватель птиц Bird Gard PRO",
            "Отпугиватель птиц QB 4",
            "Стационарный лазерный прибор для отпугивания птиц WK-0062",
            "Отпугиватель птиц ULTRASON X",
            "Отпугиватель ос WK-0432",
            "Ручной лазерный прибор для отпугивания птиц Avian Dissuader",
            ]

    item = products[random.randint(0, len(products) - 1)]
    return item

companyId = "199082"

mesUrl = "http://" + companyId + ".ru.all.biz/contacts?pmn="
postUrl = "http://" + companyId + ".ru.all.biz/message"

while(True):
    r = requests.get(mesUrl)
    tree = etree.HTML(r._content)

    check = tree.xpath("//input[@name = 'check']")[1].attrib['value']
    p_id = tree.xpath("//input[@name = 'p_id']")[0].attrib['value']

    f = {}
    f['check'] = check
    f['p_id'] = p_id
    f['country_tel_code'] = "7"
    f['email'] = ""
    f['email'] = getRandomData.GetRandomEmail()
    f['message_country'] = "136"
    fio = getRandomData.GetRandomMaleRandomLastname() + " " + getRandomData.GetRandomMaleRandomName() + " " + getRandomData.GetRandomMaleRandomMiddleName()
    f['message_fio'] = fio
    f['message_phone'] = getRandomData.GetRandomPhoneNumber()
    message_subject ="Запрос по " + GetRandomItem()
    f['message_subject'] = message_subject
    f['message_text'] = getRandomData.GetRandomStringWithSpaces(202)
    f['passwd'] = ""
    f['show_other_ent_mess'] = "0"
    operatorCode = getRandomData.GetRandomOperatorCode()
    f['tel_code'] = operatorCode
    f['type'] = ""
    f['user_agreement'] = "on"

    resp = requests.post(postUrl, data=f)
    time.sleep(60)