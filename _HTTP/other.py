﻿import requests
from lxml import etree
import urllib

def AddRecord(text):
    with open(csvFileName, "r") as myfile:
        if myfile.read().find(text.split(';')[1]) != -1 :
            return
    with open(csvFileName, "a") as myfile:
            myfile.write(text + "\n")

csvFileName = "database_voronezh.csv"

compCount = 6081
pagesCount = 6081 / 50

baseUrl = "http://www.eprussia.ru"
searchUrl = "http://www.eprussia.ru/com/com.cgi?com=com&sort=date"

resp = requests.get(searchUrl)
tree = etree.HTML(resp._content)

pageNum = 346

for i in range(0,pageNum):

    resp = requests.get(searchUrl + "&startrec=" + str(i * 10))
    tree = etree.HTML(resp._content)

    compLinks = tree.xpath("//a[@class = 'menu8' and contains(@href, '.htm')]")

    for cCount in range(0, len(compLinks)):

        compLink = compLinks[cCount]
        compHref = compLink.attrib['href']
        #name = compLink.text.strip().encode('utf-8')

        resp = requests.get(compHref)
        tree = etree.HTML(resp._content)

        contTd =  tree.xpath( "//a[contains(@href, 'mailto')]")
        if (len(contTd) == 2):
            email = contTd[0].attrib['href'].replace("mailto:","")
            name = tree.xpath("//h1")[0].text.strip().encode('utf-8')

            try:
                AddRecord(name + ";" + email)
            except:
                pass
